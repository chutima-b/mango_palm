/*
 * jQuery Validation Plugin用カスタムスクリプト
 *
 * @author Takahiro Kimura
 * @version 1.0.1
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */
$.validator.setDefaults({
	// エラーメッセージのBootstrap3: popover表示
	showErrors: function(errorMap, errorList) {
		// validationに引っかからなかった要素はtooltip非表示にする
		$.each(this.successList, function(index, value) {
			//console.log('success', index, value, $(value).attr('id'));
			var _element = $(value);
			if ($(value).attr('id') == 'courseedit_description'
			 || $(value).attr('id') == 'notice_content'
			 || $(value).attr('id') == 'theme_description'
			 || $(value).attr('id') == 'introduction') {
				// summernoteの要素の場合は、親要素を操作する
				_element = $(value).parent();
			}
			_element.tooltip('hide');
		});

		// validationに引っかかった要素はtooltip表示する
		$.each(errorList, function(index, value) {
			//console.log('error', index, value, value.element.id);
			var _element = $(value.element);
			var _addClass = "";
			if (value.element.id == 'courseedit_description'
			 || value.element.id == 'notice_content'
			 || value.element.id == 'theme_description'
			 || value.element.id == 'introduction'
			 || value.element.name == 'task_radio'
			 || value.element.name == 'task_checkbox[]') {
				// summernoteの要素の場合または、
				// 課題の単一選択、複数選択の場合は、親要素を操作する
				_element = _element.parent();
				//
				if (value.element.name == 'task_radio'
					 || value.element.name == 'task_checkbox[]') {
					switch ($('#task_type').val()) {
					case '0':
					case '1':
					case '4':
					case '5':
					case '6':
						// 課題の単一選択、複数選択の場合は、CSSクラスを追加する
						_addClass = "task_valid_tooltip";
						break;
					}
				}
			}

			var _tooltip = _element.tooltip({
				trigger: 'manual',
				placement: 'auto',
				title: value.message,
				template: '<div class="tooltip top tooltip-validation ' + _addClass + '" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
			});
			_tooltip.data('bs.tooltip').options.title = value.message; // tooltip要素のテキストを更新する
			_element.tooltip('show');
		});
	}
});
