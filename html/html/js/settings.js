/*
 * システム管理（設定）画面JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.2
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */
var pathname = location.pathname.replace(/\/regist/, '');
var pageurl = location.protocol+'//'+location.hostname+pathname;
var params = location.search;

$(function() {
	var introductionElement = $('#introduction');
	// イントロダクションのWYSIWYG適用
	introductionElement.summernote({
		lang: summernoteLang,
		toolbar: [
			['style', ['style', 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
			['fonts', ['fontsize', 'fontname']],
			['color', ['color']],
//			['undo', ['undo', 'redo', 'help']],
			['undo', ['undo', 'redo']],
			['ckMedia', ['ckImageUploader', 'ckVideoEmbeeder']],
			['misc', ['link', 'picture', 'video', 'table', 'hr', 'codeview', 'fullscreen']],
			['para', ['ul', 'ol', 'paragraph', 'leftButton', 'centerButton', 'rightButton', 'justifyButton', 'outdentButton', 'indentButton']],
			['height', ['lineheight']],
		],
		height: 300,
		fontNames: ["ＭＳ Ｐゴシック", "ＭＳ Ｐ明朝","メイリオ", "YuGothic","Yu Gothic","Hiragino Kaku Gothic Pro","sans-serif", "Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana"],
		callbacks: {
			onImageUpload: function(files, editor, welEditable) {
				$().introductionImgUpload(files[0], editor, welEditable);
			},
			// jquery-validationとsummernote併用時の不具合回避
			onChange: function(contents, $editable) {
				introductionElement.val(introductionElement.summernote('isEmpty') ? "" : contents);
				validateResult.element(introductionElement);
			}
		},
	});

	// カラーピッカー表示
	$('#background_color').simplecolorpicker({
		picker: true, theme: 'glyphicons'
	});

	// ロゴ画像ボタンクリック
	$('#btn_logo_imgupload').click(function() {
		$('#logo_img').click();
		return false;
	});

	// ロゴ画像選択
	$('#logo_img').change(function() {
		if (this.files.length > 0) {
			// 選択されたファイル情報を取得
			var file = this.files[0];
			// readerのresultプロパティに、データURLとしてエンコードされたファイルデータを格納
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function() {
				$('#thumb_logo').attr('src', reader.result);
			}
		}
	});

	// 壁紙ボタンクリック
	$('#btn_background_img').click(function() {
		$('#background_img').click();
		return false;
	});

	// 壁紙画像選択
	$('#background_img').change(function() {
		if (this.files.length > 0) {
			// 選択されたファイル情報を取得
			var file = this.files[0];
			// readerのresultプロパティに、データURLとしてエンコードされたファイルデータを格納
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function() {
				$('#thumb_background').attr('src', reader.result);
			}
			$('#validate_background_img').val(file.name);
			$('#msg_validate_background_img').text('');
		}
	});

	// 登録ボタンクリック
	$('#btn_regist').click(function() {
		if (introductionElement.summernote('codeview.isActivated')) {
			introductionElement.summernote('codeview.deactivate');
		}
		if ($('#use_background_img').prop('checked')
		 && $('#validate_background_img').val() == "") {
			// 壁紙が選択されてない場合はエラーメッセージを表示
			$('#msg_validate_background_img').text(errMsgBackgroundimgRequired);
		} else {
			$('#setting_form').submit();
		}
	});

	// フォームバリデーション
	// jquery-validationとsummernote併用時の不具合回避のため、
	// 戻り値を保持しておく
	var validateResult = $('#setting_form').validate({
		rules: {
			title :{
				maxlength: titleMaxLength
			}
		},
		messages: {
			title :{
				maxlength: titleMaxLength + errMsgTitleMaxlength
			}
		},
		// jquery-validationとsummernote併用時の不具合回避
		ignore: ":hidden:not(#introduction),.note-editable.panel-body"
	});

});

//イントロダクション画像ファイルアップロード
$.fn.introductionImgUpload = function(file, editor, welEditable) {
	data = new FormData();
	data.append("introimg", file);
	$.ajax({
		data: data,
		type: "POST",
		url: pageurl + "/introimg" + params,
		cache: false,
		contentType: false,
		processData: false,
		success: function(url) {
//			editor.insertImage(welEditable, url);
			$('#introduction').summernote('editor.insertImage', url);
		},
		error: function(response){
			$('#error').html(response.responseText);
		},
		complete: function() {
		},
	});
};
