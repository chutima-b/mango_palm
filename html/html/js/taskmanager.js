/*
 * 課題管理画面JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.1
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
$(function() {
	// フィードバックボタンクリック
	$("[id^=taskmng_btn_feedback_]").click(function() {
		var id = $(this).attr("id");
		var splitId = id.split('_');
		location.href = feedbackUrl + '?cid=' + splitId[3] + '&thid=' + splitId[4] + '&uid=' + splitId[5];
		return false;
	});

	// ダウンロードボタンクリック
	$("[id^=taskmng_btn_download_]").click(function() {
		var id = $(this).attr("id");
		var splitId = id.split('_');
		location.href = downloadUrl + '?cid=' + splitId[3] + '&thid=' + splitId[4];
		return false;
	});

});
