/*
 * 教材動画再生画面JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.2
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

$(function() {
	$('[data-toggle="tooltip"]').tooltip();

	var videoElement = $("#video");
	var video = videoElement.get(0);
	video.src = videoSrc;

	var isPlayTouched = false;

	videoElement.on("timeupdate", function() {
		if (video.currentTime > playTime) {
			playTime = video.currentTime;
		}
		$("#timeDisplay").text($().formatDisplayTime(video.currentTime));
	});

	videoElement.on("ended", function() {
		$().sendMaterialPlayTime(true);
		$("#play").html('<span class="glyphicon glyphicon-play" aria-hidden="true"></span>');
	});

	videoElement.on("loadedmetadata", function() {
		video.currentTime = playTime;
		if (!isSmartPhone) {
			if (!video.paused) {
				video.pause();
				$("#play").html('<span class="glyphicon glyphicon-play" aria-hidden="true"></span>');
			} else {
				video.play();
				$("#play").html('<span class="glyphicon glyphicon-pause" aria-hidden="true"></span>');
			}
		} else {
			$("#play").html('<span class="glyphicon glyphicon-play" aria-hidden="true"></span>');
		}
		totalTime = video.duration;
		$("#totalTime").text($().formatDisplayTime(totalTime));
	});

	videoElement.on("canplaythrough", function() {
		if (isAndroidChrome) {
			if (video.paused) {
				$("#play").html('<span class="glyphicon glyphicon-play" aria-hidden="true"></span>');
			} else {
				$("#play").html('<span class="glyphicon glyphicon-pause" aria-hidden="true"></span>');
			}
		}
	});

	$('[data-toggle="tooltip"]').tooltip().on("show.bs.tooltip", function() {
		// ツールチップが自動で閉じない場合があるので自動で閉じるようにしておく
		setTimeout(function () {
			$(".tooltip").fadeOut('fast', function() {
				$(this).remove();
			});
		}, 5000);
	});

	$("#play").on("click touchstart", function(e) {
		if (video.paused) {
			video.play();
			$("#play").html('<span class="glyphicon glyphicon-pause" aria-hidden="true"></span>');
		} else {
			$().sendMaterialPlayTime(true);
			video.pause();
			$("#play").html('<span class="glyphicon glyphicon-play" aria-hidden="true"></span>');
		}
		if (isAndroidChrome) {
			$("#play").blur(); // Androidはフォーカスが外れない時があるので外しておく
			if (isPlayTouched) {
				// Androidは最初のタッチ時以外はイベントをキャンセル
				e.preventDefault(); // touchstart と clickが重複発生しないように抑止
			}
		} else {
			e.preventDefault(); // touchstart と clickが重複発生しないように抑止
		}
		isPlayTouched = true;
	});

	$("#restart").on("click touchstart", function(e) {
		$().sendMaterialPlayTime(true);
		video.currentTime = 0;
		e.preventDefault();
	});

	$("#rew").on("click touchstart", function(e) {
		video.currentTime += $(this).data("skip");
		e.preventDefault(); // touchstart と clickが重複発生しないように抑止
	});

	$("#fastFwd").on("click touchstart", function(e) {
		if (!isPreview
		 && video.currentTime + $(this).data("skip") > playTime) {
			// プレビュー以外の場合で、
			// 早送り後の時間が教材再生時間を超えてしまう場合は
			// 早送り後の時間を教材再生時間にする
			video.currentTime = playTime;
		} else {
			video.currentTime += $(this).data("skip");
		}
		e.preventDefault(); // touchstart と clickが重複発生しないように抑止
	});

});
