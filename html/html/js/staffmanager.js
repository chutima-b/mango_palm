/*
 * 講師運用管理者登録画面JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
$(function() {

	// 「講師登録」ボタンクリック
	$("#staffmanager_btn_regist_teacher").click(function() {
		location.href = registTeacherUrl;
		return false;
	});

	// 「運用管理者登録」ボタンクリック
	$("#staffmanager_btn_regist_op_manager").click(function() {
		location.href = registOpManagerUrl;
		return false;
	});

});
