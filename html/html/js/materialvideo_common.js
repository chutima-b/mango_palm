/*
 * 教材動画再生画面共通JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.3
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */
var sendUrl = location.protocol+'//'+location.hostname+location.pathname + '/registtime' + location.search;
var ua = navigator.userAgent;
var isSmartPhone = false;
var isAndroidChrome = false;
var isAndroidStandard = false;

if (ua.indexOf('iPhone') > 0 || ua.indexOf('iPad') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0) {
	isSmartPhone = true;
	if (ua.indexOf('Android') > 0) {
		if ((/Linux; U;/.test(ua) && !/Chrome/.test(ua))
		 || (/Chrome/.test(ua) && /Version/.test(ua))
		 || (/Chrome/.test(ua) && /SamsungBrowser/.test(ua))) {
			// Android標準ブラウザ
			var isAndroidStandard = true;
		} else {
			// Android Chromeブラウザ
			isAndroidChrome = true;
		}
	}

}

$(function() {
	if (isSmartPhone) {
		window.addEventListener("pagehide", function() {
			// スマホではbeforeunloadイベントを取得できないため
			// pagehideイベントを使う
			$().sendMaterialPlayTime(false); // 非同期だとページが閉じられて正常に送信できない可能性があるので同期にする
		}, false);
	} else {
		$(window).bind("beforeunload", function() {
			$().sendMaterialPlayTime(false); // 非同期だとページが閉じられて正常に送信できない可能性があるので同期にする
		});
	}
});

// 秒数を表示用の時間にフォーマット（時:分:秒）
$.fn.formatDisplayTime = function(seconds) {
	var hour = Math.floor((seconds / 60) / 60);
	var min = Math.floor((seconds / 60) % 60);
	var sec = Math.floor(seconds % 60);
	return $().zeroPadding(hour) + ':' + $().zeroPadding(min) + ':' + $().zeroPadding(sec);
};

// ゼロパディング
$.fn.zeroPadding = function(data) {
	if (data < 10) {
		data = '0' + data;
	}
	return data;
};

// 教材再生時間送信
$.fn.sendMaterialPlayTime = function(isAsync) {
	if (evaluationUse == 0
	 || !isStudents
	 || isPreview
	 || isWatchEnd) {
		// 課題を作成しないテーマ
		// または
		// 受講者ではない場合
		// まはた
		// プレビューの場合
		// または
		// サーバー側で最後まで再生済みと判定された動画は送信しない
		console.log("Evaluation use is 'OFF' or not students or preview mode or watche ended.");
		return;
	}

	if (playTime <= sendTime) {
		// 再生時間が送信した教材再生時間以下の場合は送信しない
		console.log("playTime <= sendTime");
		return;
	}

	try {
		console.log('sendUrl:'+sendUrl, 'play_time:'+playTime, 'total_time:'+ totalTime);
		$.ajax({
			url: sendUrl,
			type: 'post',
			async: isAsync,
			timeout: 10000,
			cache: false,
			data: {
				'play_time': playTime,
				'total_time': totalTime
			}
		}).done(function(data, textStatus, jqXHR) {
			// 通信成功
			console.log('SUCCESS', data);
			if (data != null) {
				var parseData = JSON.parse(JSON.stringify(data));
				if (parseData.percentage == 100) {
					isWatchEnd = true;
				}
			}
			// 送信した教材再生時間を再生時間に更新しておく
			sendTime = playTime;
		}).fail(function(jqXHR, textStatus, errorThrown) {
			// 通信失敗
			console.log('ERROR', jqXHR, textStatus, errorThrown);
		}).always(function(data_jqXHR, textStatus, jqXHR_errorThrown) {
			// 通信完了（※引数はdoneの時はdoneと同じ、failの時はfailと同じ）
		});
	} catch (e) {
		console.log(e);
	}
	return;
};
