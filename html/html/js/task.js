/*
 * 課題画面JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.2
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
$(function() {
	// 画像ファイル選択時にファイル名を表示する
	$('#task_file').change(function() {
		if (this.files.length > 0) {
			// 選択されたファイル情報を取得
			var file = this.files[0];
			// readerのresultプロパティに、データURLとしてエンコードされたファイルデータを格納
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function() {
				$('#thumbnail').attr('src', reader.result);
			}
		}
	});

	// 無解答ボタン押下
	$('#btn_noanswer').click(function() {
		$('#task_is_noanswer').val('1');
		$('#task_form').submit();
	});

	// 解答ボタン押下
	$('#btn_submit').click(function() {
		var taskType = $('#task_type').val();
		valid[taskType]();
		$('#task_form').submit();
	});

	// 解答ボタン押下時のバリデーション
	var valid = {};
	valid['0'] = function() {
		// 単一選択の場合
		$('#task_form').validate({
			rules: {
				task_radio :{
					required: true
				}
			},
			messages: {
				task_radio :{
					required: validMsgRequireChoose
				}
			}
		});
	};
	valid['1'] = function() {
		// 複数選択の場合
		$('#task_form').validate({
			rules: {
				"task_checkbox[]" :{
					required: true
				}
			},
			messages: {
				"task_checkbox[]" :{
					required: validMsgRequireChoose
				}
			}
		});
	};
	valid['2'] = function() {
		// テキストの場合
		$('#task_form').validate({
			rules: {
				task_text :{
					required: true
				}
			},
			messages: {
				task_text :{
					required: validMsgRequireText
				}
			}
		});
	};
	valid['3'] = function() {
		// ファイルの場合は何もしない
	};
	valid['4'] = function() {
		// クイズ（単一選択）の場合
		$('#task_form').validate({
			rules: {
				task_radio :{
					required: true
				}
			},
			messages: {
				task_radio :{
					required: validMsgRequireChoose
				}
			}
		});
	};
	valid['5'] = function() {
		// アンケート（単一選択）の場合
		$('#task_form').validate({
			rules: {
				task_radio :{
					required: true
				}
			},
			messages: {
				task_radio :{
					required: validMsgRequireChoose
				}
			}
		});
	};
	valid['6'] = function() {
		// アンケート（複数選択）の場合
		$('#task_form').validate({
			rules: {
				"task_checkbox[]" :{
					required: true
				}
			},
			messages: {
				"task_checkbox[]" :{
					required: validMsgRequireChoose
				}
			}
		});
	};
});
