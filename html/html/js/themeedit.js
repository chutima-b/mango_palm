/*
 * テーマ編集画面JavaScript
 *
 * @author Mitsuharu Shimizu
 * @version 1.0.12
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
var pageurl = location.protocol+'//'+location.hostname+location.pathname;
var params = location.search;
var isQuizChangeModalShow = true;
$(function() {
	var descriptionElement = $('#theme_description');
	// テーマ説明テキストエリアのWYSIWYG適用
	descriptionElement.summernote({
		lang: summernoteLang,
		toolbar: [
			['style', ['style', 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
			['fonts', ['fontsize', 'fontname']],
			['color', ['color']],
//			['undo', ['undo', 'redo', 'help']],
			['undo', ['undo', 'redo']],
			['ckMedia', ['ckImageUploader', 'ckVideoEmbeeder']],
			['misc', ['link', 'picture', 'video', 'table', 'hr', 'codeview', 'fullscreen']],
			['para', ['ul', 'ol', 'paragraph', 'leftButton', 'centerButton', 'rightButton', 'justifyButton', 'outdentButton', 'indentButton']],
			['height', ['lineheight']],
		],
		height: 300,
		fontNames: ["ＭＳ Ｐゴシック", "ＭＳ Ｐ明朝","メイリオ", "YuGothic","Yu Gothic","Hiragino Kaku Gothic Pro","sans-serif", "Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana"],
		callbacks: {
			onImageUpload: function(files, editor, welEditable) {
				$().descriptImgUpload(files[0], editor, welEditable);
			},
			// jquery-validationとsummernote併用時の不具合回避
			onChange: function(contents, $editable) {
				descriptionElement.val(descriptionElement.summernote('isEmpty') ? "" : contents);
				validateResult.element(descriptionElement);
			}
		},
	});

	// 公開する/非公開ボタン押下
	$('#btn_open').click(function() {
		$('#themeopen').submit();
	});

	// 公開終了ボタン押下
	$('#btn_end').click(function() {
		$('#theme_public_end').submit();
	});

	// スイッチ風のスタイル適用
	$('.themeedit_switch').bootstrapSwitch();

	// 課題を作成する/作成しない設定
	$().setupEvaluationUse(true);

	$('[data-toggle="tooltip"]').tooltip();

	$('#themeedit_form_guide_help').on('click', function () {
		$('#themeedit_form_guide').toggle();
	});

	$('#themeedit_evaluation_use_guide_help').on('click', function () {
		$('#themeedit_evaluation_use_guide').toggle();
	});

	$('#themeedit_quiz_guide_help').on('click', function () {
		$('#themeedit_quiz_guide').toggle();
	});

	$('#themeedit_btn_reg_del_guide_help').on('click', function () {
		$('#themeedit_btn_reg_del_guide').toggle();
	});

	$('#themeedit_btn_open_guide_help').on('click', function () {
		$('#themeedit_btn_open_guide').toggle();
	});

	$('#themeedit_btn_new_material_guide_help').on('click', function () {
		$('#themeedit_btn_new_material_guide').toggle();
	});

	$('#themeedit_btn_new_task_guide_help').on('click', function () {
		$('#themeedit_btn_new_task_guide').toggle();
	});

	// 日付選択
	$('#themedate').datepicker({
		language: datepickerLang,
		format: "yyyy/mm/dd",
		startDate: $('#start_date').val(),
		endDate: $('#end_date').val(),
		autoclose: true
	});

	// 期限選択
	$('#time_limit').datepicker({
		language: datepickerLang,
		format: "yyyy/mm/dd",
		startDate: $('#start_date').val(),
		endDate: $('#end_date').val(),
		autoclose: true
	});

	// 「課題を作成する」のスイッチ変更
	$('#evaluation_use').on('switchChange.bootstrapSwitch', function(event, state) {
		// 課題を作成する/作成しない設定
		$().setupEvaluationUse(false);
		// 警告ポップアップの非表示設定を解除しておく
		isQuizChangeModalShow = true;
		if ($('#evaluation_use').prop('checked')) {
			// 「課題を作成する」をYESにした場合、
			// 公開ボタンを無効にする
			$("#btn_open").prop('disabled', true);
		}
	});

	// 「クイズ形式にする」のチェックボックス変更
	$('#quiz').on('switchChange.bootstrapSwitch', function(event, state) {
		if ($('#is_new').val() == '0') {
			if (isQuizChangeModalShow) {
				// 「クイズ形式にする」の状態を変更した場合は、作成できる課題も変わるため、
				// 「更新」を押下して保存するまで課題を作成できないようにする。
				// また、メッセージダイアログを表示してユーザーに保存するように促す。
				$('#quiz_change_modal').modal('show');
			}
			// 「新しい課題」ボタンを無効にする
			$('#btn_new_task').prop("disabled", true);
		}
	});

	// 登録ボタンクリック
	$('#themeedit_btn_entry').click(function() {
		if (descriptionElement.summernote('codeview.isActivated')) {
			descriptionElement.summernote('codeview.deactivate');
		}
	});

	// 「課題のリセット」クリック
	$('#btn_reset_task_result').click(function() {
		// 確認ダイアログ表示
		$('#reset_task_result_modal').modal('show');
	});

	// テーマ削除ダイアログ「削除」クリック
	$('#btn_modal_delete_theme').click(function() {
		// テーマ削除ダイアログを閉じる
		$('#delete_theme_modal').modal('hide');
		// ローディング表示
		$().showLoading();
	});

	// 「新しい教材」クリック
	$('#new_material_modal').on('show.bs.modal', function (event) {
		$('#play_end_area').hide();
		var modal = $(this)										//モーダルを取得
		modal.find('.modal-body input#description').val("")		//inputタグにも表示
		modal.find('.modal-body input#resource').val("")		//inputタグにも表示
		modal.find('.modal-body input#upload_file').val("")		//inputタグにも表示
	});

	// 課題「削除」クリック
	$('#delete_task_modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget)						//モーダルを呼び出すときに使われたボタンを取得
		var taid = button.data('taid')							//data-taid の値を取得
		var modal = $(this)										//モーダルを取得
		modal.find('.modal-body input#taid').val(taid.toString())	//inputタグにも表示
	});

	// 教材「削除」クリック
	$('#delete_material_modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget)						//モーダルを呼び出すときに使われたボタンを取得
		var mid = button.data('mid')							//data-mid の値を取得
		var modal = $(this)										//モーダルを取得
		modal.find('.modal-body input#mid').val(mid.toString())	//inputタグにも表示
	});

	// 教材画像選択時、ファイル名表示
	$('#upload_file').change(function() {
		$('#play_end_area').hide();
		var fileName = $('#upload_file')[0].files[0].name;
		if (evaluationUse) {
			// 「課題を作成する」の場合
			var fileInfo = fileName.split('.');
			var ext = fileInfo[fileInfo.length - 1];
			$.each(materialUploadExtensions, function(index, val) {
				// 拡張子が動画の場合は「最後まで再生する」スイッチを表示する
				if (ext.toLowerCase() == val.toLowerCase()) {
					$('#play_end_area').show();
					return false;
				}
			});
		}
		$('#resource').val(fileName);
	});

	// テーマフォームバリデーション
	// jquery-validationとsummernote併用時の不具合回避のため、
	// 戻り値を保持しておく
	var validateResult = $('#themeedit').validate({
		rules: {
			title :{
				required: true,
				maxlength: titleMax
			},
			goal :{
				required: true,
				maxlength: goalMax
			},
			themedate :{
				required: true
			},
			description :{
				required: true
			},
			time_limit :{
				required: true
			}
		},
		messages: {
			title :{
				required: titleRequired,
				maxlength: titleMax + errMsgTitleMaxlength
			},
			goal :{
				required: goalRequired,
				maxlength: goalMax + errMsgGoalMaxlength
			},
			themedate :{
				required: dateRequired
			},
			description :{
				required: descriptionRequired
			},
			time_limit :{
				required: timelimitRequired
			}
		},
		// jquery-validationとsummernote併用時の不具合回避
		ignore: ":hidden:not(#theme_description),.note-editable.panel-body"
	});

	// 教材フォームバリデーション
	$('#add_material_form').validate({
		rules: {
			description :{
				maxlength: materialDescriptionMax,
				required: true
			},
			resource :{
				maxlength: materialResourceMax,
				required: true
			}

		},
		messages: {
			description :{
				maxlength: materialDescriptionMax + errMsgMaterialDescriptionMaxlength,
				required: errMsgMaterialDescriptionRequired
			},
			resource :{
				maxlength: materialResourceMax + errMsgMaterialResourceMaxlength,
				required: errMsgMaterialResourceRequired
			}
		}
	});

	// 公開中、または公開終了の場合は、
	// summernoteを無効にする
	if ($('#theme_description').prop('readonly')) {
		$('#theme_description').summernote('disable');
	}

});

// 課題を作成する/作成しない設定
$.fn.setupEvaluationUse = function(isOnload) {
	if ($('#evaluation_use').prop('checked')) {
		// 「YES」の場合
		// 「評価点」を有効にする
		$('.evaluation_points_area').show();
	} else {
		// 「NO」の場合
		// クイズ形式をNOにしても警告ポップアップは表示しないようにしておく
		isQuizChangeModalShow = false;
		// 「クイズ形式にする」をNOにする
		$('#quiz').bootstrapSwitch('state', false);
		// 「評価点」を無効にする
		$('.evaluation_points_area').hide();
		// 「新しい課題」ボタンを無効にする
		$('#btn_new_task').prop("disabled", true);
	}
	if (!isOnload) {
		// 課題を作成する/作成しないの状態を変更したら、
		// 保存を促すメッセージを表示
		$('#evaluation_use_yes_modal').modal('show');
	}
};

// テーマ説明画像ファイルアップロード
$.fn.descriptImgUpload = function(file, editor, welEditable) {
	data = new FormData();
	data.append("descriptimg", file);
	$.ajax({
		data: data,
		type: "POST",
		url: pageurl + "/descriptimg" + params,
		cache: false,
		contentType: false,
		processData: false,
		success: function(url) {
//			editor.insertImage(welEditable, url);
			$('#theme_description').summernote('editor.insertImage', url);
		},
		error: function(response){
			$('#error').html(response.responseText);
		},
		complete: function() {
		},
	});
};

