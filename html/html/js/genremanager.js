/*
 * システム管理画面（ジャンル）JavaScript
 *
 * @author Mitsuharu Shimizu
 * @version 1.0.2
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
var isSending = false;
var tmpScrollTop = 0;
$(function() {
	$(window).scrollTop(1); // scrollTopが0だと上スクロールイベントが発生しないので、1pxスクロールしておく

	$(window).on("scroll", function() {
		var nowScrollTop = $(window).scrollTop();
		var isDown = false;
		if (tmpScrollTop < nowScrollTop) {
			isDown = true;
		}
		tmpScrollTop = nowScrollTop;

		var scrollHeight = $(document).height(); // ページの高さ
		var scrollPosition = $(window).height() + $(window).scrollTop(); // 表示領域の高さ + スクロール位置
		if ((scrollHeight - scrollPosition) / scrollHeight <= 0.05) {
			// スクロール位置がページ下部の5%の範囲に入った場合
			if (isSending || !nextPageUrl) {
				// 次ページ取得処理中
				// あるいは、
				// 次ページが存在しない場合は何もしない
				return false;
			}
			isSending = true;
			$().showLoading(); // ローディング表示
			$.ajax({
				url: nextPageUrl, // 次のページのHTMLを取得
				method: 'get',
			}).done(function(data, status, xhr) {
				// 通信成功
//				console.log('SUCCESS', data);
				if (!$(data).find('#next_page').attr("href")) {
					// 取得したHTMLに、次のページが無ければnullを設定しておく
					nextPageUrl = null;
				} else {
					nextPageUrl = $(data).find('#next_page').attr("href"); // 取得したHTMLから、次のページのURLを取得して設定し直す
				}
				$(data).find('#genre_list').each(function() { // 取得したHTMLからジャンルリストの部分を抜き出す
					$(this).find('tr:first').remove(); // 取得したHTMLからカラム名の部分を削除
					$('#genre_list').append($(this).html()); // 取得したHTMLをジャンルリストの最後に追加
					return false;
				});
			}).fail(function(xhr, status, error) {
				// 通信失敗
				console.log('ERROR', xhr, status, error);
			}).always(function(arg1, status, arg2) {
				// 通信完了
				isSending = false;
				// ローディング非表示
				$().hideLoading();
			});
		} else if ($(window).scrollTop() <= 100 && !isDown) {
			// スクロール位置が100以下で上スクロールの場合
			if (isSending || !beforePageUrl) {
				// 前ページ取得処理中
				// あるいは、
				// 前ページが存在しない場合は何もしない
				return false;
			}
			isSending = true;
			$().showLoading(); // ローディング表示
			$.ajax({
				url: beforePageUrl, // 前のページのHTMLを取得
				method: 'get',
			}).done(function(data, status, xhr) {
				// 通信成功
//				console.log('SUCCESS', data);
				if (!$(data).find('#before_page').attr("href")) {
					// 取得したHTMLに、前のページが無ければnullを設定しておく
					beforePageUrl = null;
				} else {
					beforePageUrl = $(data).find('#before_page').attr("href"); // 取得したHTMLから、前のページのURLを取得して設定し直す
				}
				$('#genre_list').find('tr:first').remove(); // 挿入元HTMLからカラム名の部分を削除
				$('#genre_list').prepend($(data).find('#genre_list').html()); // 取得したHTMLをジャンルリストの先頭に追加
				if (tmpScrollTop == 0) {
					// scrollTopが0だと次の上スクロールイベントが発生しないので、1pxスクロールしておく
					// （ただし、現在位置がスクロールしている場合は、現在位置以上に上に戻すと、
					// 　連続して上スクロールイベントが発生してしまうのでなにもしない
					$(window).scrollTop(1);
				}
			}).fail(function(xhr, status, error) {
				// 通信失敗
				console.log('ERROR', xhr, status, error);
			}).always(function(arg1, status, arg2) {
				// 通信完了
				isSending = false;
				// ローディング非表示
				$().hideLoading();
			});
		}
	});

	// 「新しいジャンル」「編集」ダイアログデフォルトアイコン画像ロード
	$('#icon_thumb').attr('src', defaultIcon);
	$('#icon_thumb').css({'width':defaultIconMaxLength});

	// 「新しいジャンル」「編集」ダイアログ画像選択ボタンクリック
	$('#btn_icon').click(function() {
		$('#icon').click();
		return false;
	});

	// 「新しいジャンル」「編集」ダイアログの画像選択時にサムネイル表示
	$('#icon').change(function() {
		if (this.files.length > 0) {
			// 選択されたファイル情報を取得
			var file = this.files[0];
			// readerのresultプロパティに、データURLとしてエンコードされたファイルデータを格納
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function() {
				$('#icon_thumb').attr('src', reader.result);
				$('#icon_thumb').css({'width':defaultIconMaxLength});
			}
		}
	});

	// 「新しいジャンル」「編集」クリック
	$('#new_genre_modal').on('show.bs.modal', function (event) {
		// 表示されているエラーのツールチップを非表示にする
		$('.tooltip-validation').hide();
		var button = $(event.relatedTarget)										// モーダルを呼び出すときに使われたボタンを取得
		var isnew = button.data('isnew')										// data-isnew の値を取得
		var gid = button.data('gid')											// data-gid の値を取得
		var genreid = button.data('genreid')									// data-genreid の値を取得
		var genrename = button.data('genrename')								// data-genrename の値を取得
		var icon = button.data('icon')											// data-icon の値を取得
		var modal = $(this)														// モーダルを取得
		if (isnew) {
			$('#delete_icon_area').hide();
		} else {
			$('#delete_icon_area').show();
		}
		modal.find('.modal-body input#gid').val(gid.toString())					// ジャンルテーブルIDを設定
		modal.find('.modal-body input#genreid').val(genreid.toString())			// ジャンルIDを設定
		modal.find('.modal-body input#genrename').val(genrename.toString())		// ジャンル名を設定
		modal.find('.modal-body img#icon_thumb').attr('src', icon);				// アイコンサムネイルを設定
		modal.find('.modal-body input#delete_icon').prop('checked', false);		// アイコンを削除のチェックボックス初期化
	})

	// 「削除」クリック
	$('#delete_genre_modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget)						//モーダルを呼び出すときに使われたボタンを取得
		var gid = button.data('gid')							//data-gid の値を取得
		var modal = $(this)										//モーダルを取得
		modal.find('.modal-body input#gid').val(gid.toString())	//inputタグにも表示
	})

	$('#genre_form').validate({
		rules: {
			genreid :{
				required: true,
				maxlength: genreIdMax
			},
			genrename :{
				required: true,
				maxlength: genreNameMax
			}
		},
		messages: {
			genreid :{
				required: errMsgGenreIdRequired,
				maxlength: genreIdMax + errMsgGenreIdMaxlength
			},
			genrename :{
				required: errMsgGenreNameRequired,
				maxlength: genreNameMax + errMsgGenreNameMaxlength
			}
		}
	});

});
