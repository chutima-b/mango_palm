/*
 * 日記画面JavaScript
 *
 * @author Mitsuharu Shimizu
 * @version 1.0.1
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
var isSending = false;
var tmpScrollTop = 0;
$(function() {
	$(window).scrollTop(1); // scrollTopが0だと上スクロールイベントが発生しないので、1pxスクロールしておく

	$(window).on("scroll", function() {
		var nowScrollTop = $(window).scrollTop();
		var isDown = false;
		if (tmpScrollTop < nowScrollTop) {
			isDown = true;
		}
		tmpScrollTop = nowScrollTop;

		var scrollHeight = $(document).height(); // ページの高さ
		var scrollPosition = $(window).height() + $(window).scrollTop(); // 表示領域の高さ + スクロール位置
		if ((scrollHeight - scrollPosition) / scrollHeight <= 0.05) {
			// スクロール位置がページ下部の5%の範囲に入った場合
			if (isSending || !nextPageUrl) {
				// 次ページ取得処理中
				// あるいは、
				// 次ページが存在しない場合は何もしない
				return false;
			}
			isSending = true;
			$().showLoading(); // ローディング表示
			$.ajax({
				url: nextPageUrl, // 次のページのHTMLを取得
				method: 'get',
			}).done(function(data, status, xhr) {
				// 通信成功
//				console.log('SUCCESS', data);
				if (!$(data).find('#next_page').attr("href")) {
					// 取得したHTMLに、次のページが無ければnullを設定しておく
					nextPageUrl = null;
				} else {
					nextPageUrl = $(data).find('#next_page').attr("href"); // 取得したHTMLから、次のページのURLを取得して設定し直す
				}
				$(data).find('#diary_list').each(function() { // 取得したHTMLから日記の部分を抜き出す
					$('#diary_list').append('<hr>');
					$('#diary_list').append($(this).html()); // 取得したHTMLを日記の最後に追加
					return false;
				});
			}).fail(function(xhr, status, error) {
				// 通信失敗
				console.log('ERROR', xhr, status, error);
			}).always(function(arg1, status, arg2) {
				// 通信完了
				isSending = false;
				// ローディング非表示
				$().hideLoading();
			});
		} else if ($(window).scrollTop() <= 100 && !isDown) {
			// スクロール位置が100以下で上スクロールの場合
			if (isSending || !beforePageUrl) {
				// 前ページ取得処理中
				// あるいは、
				// 前ページが存在しない場合は何もしない
				return false;
			}
			isSending = true;
			$().showLoading(); // ローディング表示
			$.ajax({
				url: beforePageUrl, // 前のページのHTMLを取得
				method: 'get',
			}).done(function(data, status, xhr) {
				// 通信成功
//				console.log('SUCCESS', data);
				if (!$(data).find('#before_page').attr("href")) {
					// 取得したHTMLに、前のページが無ければnullを設定しておく
					beforePageUrl = null;
				} else {
					beforePageUrl = $(data).find('#before_page').attr("href"); // 取得したHTMLから、前のページのURLを取得して設定し直す
				}
				$('#diary_list').prepend('<hr>');
				$('#diary_list').prepend($(data).find('#diary_list').html()); // 取得したHTMLを日記の先頭に追加
				if (tmpScrollTop == 0) {
					// scrollTopが0だと次の上スクロールイベントが発生しないので、1pxスクロールしておく
					// （ただし、現在位置がスクロールしている場合は、現在位置以上に上に戻すと、
					// 　連続して上スクロールイベントが発生してしまうのでなにもしない
					$(window).scrollTop(1);
				}
			}).fail(function(xhr, status, error) {
				// 通信失敗
				console.log('ERROR', xhr, status, error);
			}).always(function(arg1, status, arg2) {
				// 通信完了
				isSending = false;
				// ローディング非表示
				$().hideLoading();
			});
		}
	});

	// 「投稿」「編集」クリック
	$('#new_post_modal').on('show.bs.modal', function (event) {
		$('.tooltip-validation').hide();
		var button = $(event.relatedTarget)						//モーダルを呼び出すときに使われたボタンを取得
		var did = button.data('did')							//data-did の値を取得
		var msg = button.data('postmsg')						//data-postmsg の値を取得
		var img = button.data('img')							//data-img の値を取得
		var modal = $(this)										//モーダルを取得
		modal.find('.modal-footer input#did').val(did)			//inputタグにも表示
		modal.find('.modal-body textarea#postmsg').val(msg)		//inputタグにも表示
		if (img) {
			$('#upload_image_btn_area').css({'margin-right':'1em'});
			$('#upload_imagefile_thumbnail').attr('src', img);
			$('#upload_imagefile_thumbnail').css({'display':'block', 'width':'60px'});
		} else {
			$('#upload_imagefile_thumbnail').attr('src', '');
			$('#upload_imagefile_thumbnail').css({'display':'none'});
		}
		if (!img) {
			img = '';
		}
	})

	// 「削除」クリック
	$('#delete_post_modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget)						//モーダルを呼び出すときに使われたボタンを取得
		var did = button.data('did')							//data-did の値を取得
		var modal = $(this)										//モーダルを取得
		modal.find('.modal-body input#did').val(did)			//inputタグにも表示
	})

	// 画像選択時にサムネイル表示
	$('#upload_imagefile').change(function() {
		if (this.files.length > 0) {
			// 選択されたファイル情報を取得
			var file = this.files[0];
			// readerのresultプロパティに、データURLとしてエンコードされたファイルデータを格納
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function() {
				$('#upload_image_btn_area').css({'margin-right':'1em'});
				$('#upload_imagefile_thumbnail').css({'display':'block', 'width':'60px'});
				$('#upload_imagefile_thumbnail').attr('src', reader.result);
			}
		}
	});

	// 投稿内容バリデーション
	$('#post_edit').validate({
		rules: {
			postmsg :{
				required: true,
				maxlength: postmsgMax
			}
		},
		messages: {
			postmsg :{
				required: errMsgPostMsgRequired,
				maxlength: postmsgMax + errMsgPostMsgMaxlength
			}
		}
	});

});
