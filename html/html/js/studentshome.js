/*
 * ホーム画面（受講者）JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.2
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
$(function() {
	var SWITCH_CAL_INTRO_CAL = "cal";
	var SWITCH_CAL_INTRO_INTRO = "intro";

	// カレンダー/イントロダクション切替チェックボックスにbootstrapSwitch適用
	$("#switch_cal_intro").bootstrapSwitch({
		onText: calIntroSwitchCal,
		offText: calIntroSwitchIntro,
		offColor: "info"
	});

	// カレンダー/イントロダクション切替で、
	// カレンダーとイントロダクションの表示を切り替える。
	$("#switch_cal_intro").on("switchChange.bootstrapSwitch", function() {
		$(".tgl_cal_intro").toggle();
		var switchCalIntro = SWITCH_CAL_INTRO_INTRO;
		if ($('#switch_cal_intro').prop('checked')) {
			// カレンダー表示
			$().locadCalendar();

			// 初期状態がイントロだった場合、
			// カレンダー表示に切り替えた際に
			// なぜかカレンダーが正常に表示されないので
			// カレンダーの描画処理を呼び出す。
			$('#calendar').data('fullCalendar').render();

			// 受講講座表示設定
			$().setupDisplayCourseListForCalendar();
			// カレンダーに切り替えた場合はセッションストレージに"cal"を設定
			switchCalIntro = SWITCH_CAL_INTRO_CAL;
		} else {
			// 受講講座表示設定
			$().setupDisplayCourseListForIntro();
		}
		if (window.sessionStorage) {
			// セッションストレージにスイッチの状態を格納
			sessionStorage.setItem('switch_cal_intro', switchCalIntro);
		}
	});

	// セッションストレージからカレンダー/イントロダクション切替スイッチの状態が取得出来た場合
	if (window.sessionStorage && sessionStorage.getItem('switch_cal_intro') != null) {
		// カレンダー/イントロダクション切替利用の場合
		if (calIntroDisplayMode == 9) {
			// 取得した値に応じてカレンダー/イントロダクション切替スイッチを設定する。
			if (sessionStorage.getItem('switch_cal_intro') == SWITCH_CAL_INTRO_CAL) {
				// セッションストレージから取得した値が"cal"の時はスイッチをON（カレンダー）に設定
				$('#switch_cal_intro').bootstrapSwitch('state', true);
				$('#calendar').show();
				$('#intro').hide();
				// 受講講座表示設定
				$().setupDisplayCourseListForCalendar();
			} else {
				// セッションストレージから取得した値が"intro"の時はスイッチをOFF（イントロ）に設定
				$('#switch_cal_intro').bootstrapSwitch('state', false);
				$('#calendar').hide();
				$('#intro').show();
				// 受講講座表示設定
				$().setupDisplayCourseListForIntro();
			}
		}
	}

	// カレンダー表示
	$().locadCalendar();

});

// カレンダー用受講講座表示設定
$.fn.setupDisplayCourseListForCalendar = function() {
	if (calIntroDisplayCourseList == 0
	 || calIntroDisplayCourseList == 2) {
		// 定義のカレンダー/イントロ表示時の「受講講座」の表示/非表示設定が
		// 「カレンダーの時表示する」
		// または
		// 「カレンダー/イントロの両方表示する」
		// の場合は、「受講講座」を表示
		$('#course_list').show();
	} else {
		// 上記条件以外は「受講講座」を非表示
		$('#course_list').hide();
	}
};

// イントロ用受講講座表示設定
$.fn.setupDisplayCourseListForIntro = function() {
	if (calIntroDisplayCourseList == 1
	 || calIntroDisplayCourseList == 2) {
		// 定義のカレンダー/イントロ表示時の「受講講座」の表示/非表示設定が
		// 「イントロの時表示する」
		// または
		// 「カレンダー/イントロの両方表示する」
		// の場合は、「受講講座」を表示
		$('#course_list').show();
	} else {
		// 上記条件以外は「受講講座」を非表示
		$('#course_list').hide();
	}
};

// カレンダー表示
$.fn.locadCalendar = function() {
	$('#calendar').fullCalendar({
		locale: fullCalendarLang,
//		theme: true,
		theme: false,
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay,listWeek'
		},
		navLinks: true, // can click day/week names to navigate views
		editable: false,
		eventLimit: true, // allow "more" link when too many events
		eventSources: [{
		    url: scheduleUrl,
		    dataType: 'json',
		    async: true,
		    type: 'POST',
		    data: {
		    	schedule: 1
		    },
		    success: function(response) {
		    	console.log('retrieve schedule success.');
//		    	console.log(response);
		    },
		    error: function (response) {
		    	console.log('retrieve schedule error.');
		    	console.log(response);
		    }
		}],
		noEventsMessage: noEventsMsg
	});
};