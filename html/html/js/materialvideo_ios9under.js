/*
 * 教材動画再生画面（iOS9以下用スクリプト）JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

$(function() {
	var btnPlay = document.getElementById('play');
	btnPlay.disabled = true;

	var audio = new Audio();
	var video = document.createElement('video');
	video.style.display = 'none';
	document.body.appendChild(video);

	var canvas = document.getElementById('canvas');
	var ctx = canvas.getContext('2d');
	var togglePlay;

	var videoHeight = 0;
	var videoWidth = 0;

	var prms1 = new Promise(function(resolve, reject) {
		video.addEventListener('canplay', function() {
			resolve();
		});
		video.addEventListener('error', function() {
			reject();
			console.log('failed loading video');
		});
	});

	var prms2 = new Promise(function(resolve, reject) {
		audio.addEventListener('canplay', function() {
			resolve();
		});
		audio.addEventListener('error', function() {
			reject();
			console.log('failed loading audio');
		});
	});

	Promise.all([prms1, prms2]).then(function() {
		btnPlay.disabled = false;
	});

	video.src = videoSrc;
	video.load();
	audio.src = videoSrc; // 動画ファイルを突っ込んでも再生される
	audio.load();

	togglePlay = function() {
		if (audio.paused) {
		  	$("#play").html('<span class="glyphicon glyphicon-pause" aria-hidden="true"></span>');
			audio.play();
			(function loop() {
				video.currentTime = audio.currentTime;
				ctx.drawImage(video, 0, 0, videoWidth, videoHeight, 0, 0, canvas.width, canvas.height);
				if (!audio.paused) {
					requestAnimationFrame(loop);
				}
			})();
		} else {
			$().sendMaterialPlayTime(true);
			$("#play").html('<span class="glyphicon glyphicon-play" aria-hidden="true"></span>');
			audio.pause();
		}
	};

	btnPlay.addEventListener('click',togglePlay);

	video.addEventListener("loadedmetadata", function () {
		videoHeight = video.videoHeight;
		videoWidth = video.videoWidth;

		canvas.width = screen.width;
		canvas.height = videoHeight * (screen.width / videoWidth);

		audio.currentTime = playTime;

		totalTime = video.duration;
		$("#totalTime").text($().formatDisplayTime(totalTime));
	}, false);

	audio.addEventListener("loadedmetadata", function () {
		audio.currentTime = playTime;
	}, false);

	audio.addEventListener("timeupdate", function () {
		if (audio.currentTime > playTime) {
			playTime = audio.currentTime;
		}
		$("#timeDisplay").text($().formatDisplayTime(audio.currentTime));
	}, false);

	audio.addEventListener("ended", function () {
		$().sendMaterialPlayTime(true);
		$("#play").html('<span class="glyphicon glyphicon-play" aria-hidden="true"></span>');
	}, false);

	$("#rew").on("click", function () {
		audio.currentTime += $(this).data("skip");
	});

	$("#fastFwd").on("click", function () {
		if (!isPreview
		 && audio.currentTime + $(this).data("skip") > playTime) {
			// プレビュー以外の場合で、
			// 早送り後の時間が教材再生時間を超えてしまう場合は
			// 早送り後の時間を教材再生時間にする
			audio.currentTime = playTime;
		} else {
			audio.currentTime += $(this).data("skip");
		}
	});

	$("#restart").on("click", function () {
		$().sendMaterialPlayTime(true);
		audio.currentTime = 0;
		video.currentTime = 0;
	});

});
