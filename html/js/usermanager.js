/*
 * システム管理（ユーザー管理）画面JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.5
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
var isSending = false;
var tmpScrollTop = 0;
$(function() {
	$(window).scrollTop(1); // scrollTopが0だと上スクロールイベントが発生しないので、1pxスクロールしておく

	$(window).on("scroll", function() {
		var nowScrollTop = $(window).scrollTop();
		var isDown = false;
		if (tmpScrollTop < nowScrollTop) {
			isDown = true;
		}
		tmpScrollTop = nowScrollTop;

		var scrollHeight = $(document).height(); // ページの高さ
		var scrollPosition = $(window).height() + $(window).scrollTop(); // 表示領域の高さ + スクロール位置
		if ((scrollHeight - scrollPosition) / scrollHeight <= 0.05) {
			// スクロール位置がページ下部の5%の範囲に入った場合
			if (isSending || !nextPageUrl) {
				// 次ページ取得処理中
				// あるいは、
				// 次ページが存在しない場合は何もしない
				return false;
			}
			isSending = true;
			$().showLoading(); // ローディング表示
			$.ajax({
				url: nextPageUrl, // 次のページのHTMLを取得
				method: 'get',
			}).done(function(data, status, xhr) {
				// 通信成功
//				console.log('SUCCESS', data);
				if (!$(data).find('#next_page').attr("href")) {
					// 取得したHTMLに、次のページが無ければnullを設定しておく
					nextPageUrl = null;
				} else {
					nextPageUrl = $(data).find('#next_page').attr("href"); // 取得したHTMLから、次のページのURLを取得して設定し直す
				}
				$(data).find('#user_list').each(function() { // 取得したHTMLからユーザー一覧の部分を抜き出す
					$(this).find('tr:first').remove(); // 取得したHTMLからカラム名の部分を削除
					$('#user_list').append($(this).html()); // 取得したHTMLをユーザー一覧の最後に追加
					return false;
				});
			}).fail(function(xhr, status, error) {
				// 通信失敗
				console.log('ERROR', xhr, status, error);
			}).always(function(arg1, status, arg2) {
				// 通信完了
				isSending = false;
				// ローディング非表示
				$().hideLoading();
			});
		} else if ($(window).scrollTop() <= 100 && !isDown) {
			// スクロール位置が100以下で上スクロールの場合
			if (isSending || !beforePageUrl) {
				// 前ページ取得処理中
				// あるいは、
				// 前ページが存在しない場合は何もしない
				return false;
			}
			isSending = true;
			$().showLoading(); // ローディング表示
			$.ajax({
				url: beforePageUrl, // 前のページのHTMLを取得
				method: 'get',
			}).done(function(data, status, xhr) {
				// 通信成功
//				console.log('SUCCESS', data);
				if (!$(data).find('#before_page').attr("href")) {
					// 取得したHTMLに、前のページが無ければnullを設定しておく
					beforePageUrl = null;
				} else {
					beforePageUrl = $(data).find('#before_page').attr("href"); // 取得したHTMLから、前のページのURLを取得して設定し直す
				}
				$('#user_list').find('tr:first').remove(); // 挿入元HTMLからカラム名の部分を削除
				$('#user_list').prepend($(data).find('#user_list').html()); // 取得したHTMLをユーザー一覧の先頭に追加
				if (tmpScrollTop == 0) {
					// scrollTopが0だと次の上スクロールイベントが発生しないので、1pxスクロールしておく
					// （ただし、現在位置がスクロールしている場合は、現在位置以上に上に戻すと、
					// 　連続して上スクロールイベントが発生してしまうのでなにもしない
					$(window).scrollTop(1);
				}
			}).fail(function(xhr, status, error) {
				// 通信失敗
				console.log('ERROR', xhr, status, error);
			}).always(function(arg1, status, arg2) {
				// 通信完了
				isSending = false;
				// ローディング非表示
				$().hideLoading();
			});
		}
	});

	$('[data-toggle="tooltip"]').tooltip();

	$('#usermanage_btn_guide_help').on('click', function () {
		$('#usermanage_btn_guide').toggle();
	});

	// アップロードCSVファイル選択時にファイル名を表示
	$('#usermanage_file_upload').change(function() {
		// 選択したファイル名を表示
		$('#usermanage_file_name').text($(this).val());
		// ローディング表示
		$().showLoading();
		// フォーム送信
		$('#usermanage_upload_form').submit();
	});

	// ユーザー登録ダイアログ画像選択ボタンクリック
	$('#usermanage_regist_btn_imgupload').click(function() {
		$('#usermanage_regist_imgupload').click();
		return false;
	});

	// ユーザー登録ダイアログの画像選択時にサムネイル表示
	$('#usermanage_regist_imgupload').change(function() {
		if (this.files.length > 0) {
			// 選択されたファイル情報を取得
			var file = this.files[0];
			// readerのresultプロパティに、データURLとしてエンコードされたファイルデータを格納
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function() {
				$('#usermanage_regist_thumbnail').attr('src', reader.result);
				$('#usermanage_regist_thumbnail').css({'width':pictureMaxLength});
			}
		}
	});

	// 編集ボタンクリック
	$(document).on('click', "[id^=usermanager_btn_edit_]", function() {
		var
			str = $(this).attr("id"),
			uid = str.match(/\d/g).join(""),
			userid = $('#usermanage_list_userid_'+uid).text(),
			surname = $('#usermanage_list_surname_'+uid).text(),
			firstname = $('#usermanage_list_firstname_'+uid).text(),
			nickname = $('#usermanage_list_nickname_'+uid).text(),
			pictureFile = $('#usermanage_list_image_'+uid).attr('src'),
			coursenames = $('#usermanager_coursenames_'+uid).val();

		// ダイアログ入力項目初期化
		$('#usermanage_regist_password').val(''); // パスワードはデフォルト未入力（未入力の場合は更新しない）
		$('#usermanage_regist_password').attr('placeholder', dialogPlaceholderPasswd);
		$('#usermanage_regist_password_confirm').val(''); // パスワードはデフォルト未入力（未入力の場合は更新しない）
		$('#usermanage_regist_userid').val(userid);
		$('#usermanage_regist_nickname').val(nickname);
		$('#usermanage_regist_thumbnail').attr('src', pictureFile);
		$('#usermanage_regist_surname').val(surname);
		$('#usermanage_regist_firstname').val(firstname);
		$('#usermanage_regist_uid').val(uid);

		// 受講中講座表示
		$('#usermanage_course_area').show();
		if (coursenames != '') {
			var reg = new RegExp(courseNameDelim, "g");
			coursenames = coursenames.replace(reg , "<br>") ;
		}
		$('#usermanage_course').html(coursenames);

		// 編集の場合はパスワードの入力は必須ではない
		$('#usermanage_regist_password').rules("remove", 'required');
		$('#usermanage_regist_password_confirm').rules("remove", 'required');

		// ダイアログ表示
		$('#user_edit_modal').modal('show');
		return false;
	});

	// 停止ボタンクリック
	$(document).on('click', "[id^=usermanager_btn_stop_]", function() {
		$().openConfirmDialog(this, stopUrl, confirmMsgTitleStop, confirmMsgBodyStop);
		return false;
	});

	// 再開ボタンクリック
	$(document).on('click', "[id^=usermanager_btn_restart_]", function() {
		$().openConfirmDialog(this, restartUrl, confirmMsgTitleRestart, confirmMsgBodyRestart);
		return false;
	});

	// 削除ボタンクリック
	$(document).on('click', "[id^=usermanager_btn_delete_]", function() {
		$().openConfirmDialog(this, deleteUrl, confirmMsgTitleDelete, confirmMsgBodyDelete);
		return false;
	});

	// 新しいユーザーボタンクリック
	$('#usermanager_btn_newuser').click(function() {
		// ダイアログ入力項目初期化
		$('#usermanage_regist_password').removeAttr('placeholder');
		$('#usermanage_regist_userid').val('');
		$('#usermanage_regist_password').val('');
		$('#usermanage_regist_password_confirm').val('');
		$('#usermanage_regist_nickname').val('');
		$('#usermanage_regist_thumbnail').attr('src', '');
		$('#usermanage_regist_surname').val('');
		$('#usermanage_regist_firstname').val('');
		$('#usermanage_regist_uid').val('');
		$('#usermanage_course').html('');
		$('#usermanage_course_area').hide();

		// 登録の場合はパスワードの入力は必須
		$('#usermanage_regist_password').rules("add", {
			required: true,
			messages: {
				required: errMsgPasswdRequired
			}
		});
		$('#usermanage_regist_password_confirm').rules("add", {
			required: true,
			messages: {
				required: errMsgPasswdConfirmRequired
			}
		});
	});

	// 新しいユーザー、編集ダイアログ非表示イベント
	$('#user_edit_modal').on('hide.bs.modal', function (event) {
		// 表示されているエラーのツールチップを非表示にする
		$('.tooltip-validation').hide();
	});

	// ダウンロードボタンクリック
	$('#usermanager_btn_download').click(function() {
		location.href = downloadUrl;
		return false;
	});

	// 検索フォームバリデーション
	$('#usermanage_form').validate({
		rules: {
			usermanage_userid :{
				maxlength: useridMax
			},
			usermanage_surname :{
				maxlength: surnameMax
			},
			usermanage_firstname :{
				maxlength: firstnameMax
			}
		},
		messages: {
			usermanage_userid :{
				maxlength: useridMax + errMsgMaxlength
			},
			usermanage_surname :{
				maxlength: surnameMax + errMsgMaxlength
			},
			usermanage_firstname :{
				maxlength: firstnameMax + errMsgMaxlength
			}
		}
	});

	// 独自バリデーション用メソッド
	var methods = {
		username: function(value, element) {
			return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
		}
	};

	// 独自バリデーション用メソッドの登録
	$.each(methods, function(key) {
		$.validator.addMethod(key, this);
	});

	// 登録フォームバリデーション
	$('#usermanage_regist_form').validate({
		rules: {
			usermanage_regist_userid :{
				required: true,
				username: 'username',
				maxlength: useridMax
			},
			usermanage_regist_password :{
				minlength: passwdMin,
				maxlength: passwdMax
			},
			usermanage_regist_password_confirm :{
				minlength: passwdMin,
				maxlength: passwdMax,
				equalTo: "#usermanage_regist_password",
			},
			usermanage_regist_nickname :{
				required: true,
				maxlength: nicknameMax
			},
			usermanage_regist_surname :{
				required: true,
				maxlength: surnameMax
			},
			usermanage_regist_firstname :{
				required: true,
				maxlength: firstnameMax
			}
		},
		messages: {
			usermanage_regist_userid :{
				required: errMsgUseridRequired,
				username: errMsgUseridAlphaNumeric,
				maxlength: useridMax + errMsgMaxlength
			},
			usermanage_regist_password :{
				minlength: passwdMin + errMsgMinlength,
				maxlength: passwdMax + errMsgMaxlength
			},
			usermanage_regist_password_confirm :{
				minlength: passwdMin + errMsgMinlength,
				maxlength: passwdMax + errMsgMaxlength,
				equalTo: errMsgPasswdConfirmEqualto
			},
			usermanage_regist_nickname :{
				required: errMsgNicknameRequired,
				maxlength: nicknameMax + errMsgMaxlength
			},
			usermanage_regist_surname :{
				required: errMsgSurnameRequired,
				maxlength: surnameMax + errMsgMaxlength
			},
			usermanage_regist_firstname :{
				required: errMsgFirstnameRequired,
				maxlength: firstnameMax + errMsgMaxlength
			}
		}
	});

});

// 確認ダイアログ表示
$.fn.openConfirmDialog = function(element, actionUrl, confirmMsgTitle, confirmMsgBody) {
	var
		str = $(element).attr("id"),
		uid = str.match(/\d/g).join("");

	// フォームアクションURL設定
	$("#usermanage_confirm_form").attr("action", actionUrl);

	// 確認ダイアログメッセージ設定
	$('#usermanage_confirm_modal_title').text(confirmMsgTitle);
	$('#usermanage_confirm_modal_body').text($('#usermanage_list_userid_'+uid).text() + confirmMsgBody);

	// 処理対象ユーザーテーブルID設定
	$('#usermanage_confirm_modal_uid').val(uid);

	// 確認ダイアログ表示
	$('#usermanage_confirm_modal_btn_open').click();
	return false;
};
