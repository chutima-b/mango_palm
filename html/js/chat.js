/*
 * チャット画面JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.2
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
var chatUrl = location.protocol+'//'+location.hostname+location.pathname;
var params = location.search;
var chatIntervalId = null;
var isSending = false;
var isGetting = false;
var isPastGetting = false;
var tmpScrollTop = 0;

$(function() {
	// 一定間隔でチャットデータ更新
	$().setChatReloadInterval();

	// 入力エリアまでスクロール
	$().scrollInputMessageArea();

	// エンターキーを無効化して画面遷移させない
	$('#chat_message').keypress(function (e) {
		if (e.which === 13) {
			$('#chat_submit').click();
			return false;
		}
	});

	// 画像選択時にサムネイル表示
	$('#chat_upload_imagefile').change(function() {
		if (this.files.length > 0) {
			// 選択されたファイル情報を取得
			var file = this.files[0];
			// readerのresultプロパティに、データURLとしてエンコードされたファイルデータを格納
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function() {
				$('#upload_imagefile_thumbnail').css({'display':'block', 'width':'60px'});
				$('#upload_imagefile_thumbnail').attr('src', reader.result);
			}
		}
	});

	// 送信ボタン押下
	$('#chat_submit').click(function() {
		if (isSending) {
			return false;
		}
		if ($('#chat_message').val() == '') {
			$().displayModalDialog(dialogErrorTitle, dialogErrorBodyEmptyMsg);
			return false;
		}
		if ($('#chat_message').val().length > 256) {
			$().displayModalDialog(dialogErrorTitle, dialogErrorBodyStrOverMsg);
			return false;
		}
		var form = $('#chat_form')[0];
		var formData = new FormData(form);
		isSending = true;
		$('#chat_message').prop("disabled", true); // 連続した「送信」ボタンの押下防止
		$.ajax({
			url: chatUrl+'/submit'+params,
			method: 'post',
			dataType: 'json',
			async: true,
			data: formData,
			processData: false,
			contentType: false
		}).done(function(data, status, xhr) {
			// 通信成功
//			console.log('SUCCESS', data);
			$('#chat_upload_imagefile').val('');
			$('#chat_message').val('');
			// チャット差分データ取得
			$().clearChatReloadInterval();
			$().getChatDifferData(true);
			$().setChatReloadInterval();
		}).fail(function(xhr, status, error) {
			// 通信失敗
			var errmsg = '';
			try{
				var parseData = JSON.parse(JSON.stringify(xhr));
				errmsg = parseData.responseJSON.error;
			} catch (e) {
			}
			console.log('ERROR', xhr, status, error, errmsg);
			$().displayModalDialog(dialogErrorTitle, errmsg);
		}).always(function(arg1, status, arg2) {
			// 通信完了
			$('#chat_message').prop("disabled", false);
			$('#upload_imagefile_thumbnail').attr('src', '');
			$('#upload_imagefile_thumbnail').css({'display':'none'});
			isSending = false;
		});
		return false;
	});

	$(window).on("scroll", function() {
		var nowScrollTop = $(window).scrollTop();
		var isDown = false;
		if (tmpScrollTop < nowScrollTop) {
			isDown = true;
		}
		tmpScrollTop = nowScrollTop;

		var scrollHeight = $(document).height(); // ページの高さ
		var scrollPosition = $(window).height() + $(window).scrollTop(); // 表示領域の高さ + スクロール位置
		if ($(window).scrollTop() <= 100 && !isDown) {
			// スクロール位置が100以下で上スクロールの場合
			if (isPastGetting) {
				// 前ページ取得処理中は何もしない
				return false;
			}
			isPastGetting = true;
			$().showLoading(); // ローディング表示
			$.ajax({
				url: chatUrl+'/getpastdata'+params,
				type: 'get',
				data: {
					'first_id': $('#chat_firstid').val()
				}
			}).done(function(data, status, xhr) {
				// 通信成功
//				console.log('SUCCESS', data);
				if (data == null) {
					return false;
				}
				var parseData = JSON.parse(JSON.stringify(data));

				//console.log('RETURN DATA:::', parseData);

				// hiddenのfirst_id設定
				$('#chat_firstid').val(parseData.first_id);

				// チャットHTML生成
				var html = $().createChatHtml(parseData.chat_data);

				// HTML追加
				$('#chatarea').prepend(html);

				// scrollTopが0だと上スクロールイベントが発生しないので、1pxスクロールしておく
				$(window).scrollTop(1);

				return false;
			}).fail(function(xhr, status, error) {
				// 通信失敗
				console.log('ERROR', xhr, status, error);
				$().displayModalDialog(dialogErrorTitle, error);
			}).always(function(arg1, status, arg2) {
				// 通信完了
				isPastGetting = false;
				// ローディング非表示
				$().hideLoading();
			});
		}
	});

	$('.img-responsive').load(function() {
		// テキストデータのみ読み込んだ時点でスクロールしてしまう場合があるので、
		// 画像を読み込んだ後に
		// 再度、追加したHTMLの分スクロールさせる。
		$().scrollInputMessageArea();
	});
});

// 一定間隔でチャットデータ更新
$.fn.setChatReloadInterval = function() {
	chatIntervalId = setInterval("$().getChatDifferData(true)" , chatReloadInterval);
};

// チャットリロードキャンセル
$.fn.clearChatReloadInterval = function() {
	if (chatIntervalId != null) {
		clearInterval(chatIntervalId);
	}
};

// モーダルダイアログ表示
$.fn.displayModalDialog = function(title, body) {
	$('#chat_modal_title').html(title);
	$('#chat_modal_body').html(body);
	$('#chat_btn_modal').click();
};

// チャット差分データ取得
$.fn.getChatDifferData = function(isAsync) {
	if (isGetting) {
		return false;
	}
	isGetting = true;
	$.ajax({
		url: chatUrl+'/getdata'+params,
		type: 'get',
		async: isAsync,
		data: {
			'last_id': $('#chat_lastid').val()
		}
	}).done(function(data, status, xhr) {
		// 通信成功
//		console.log('SUCCESS', data);
		$().displayChatData(data);
	}).fail(function(xhr, status, error) {
		// 通信失敗
		console.log('ERROR', xhr, status, error);
		$().displayModalDialog(dialogErrorTitle, error);
	}).always(function(arg1, status, arg2) {
		// 通信完了
		isGetting = false;
	});
};

// チャットデータ表示
$.fn.displayChatData = function(data) {
	if (data == null) {
		return false;
	}
	var parseData = JSON.parse(JSON.stringify(data));

	//console.log('RETURN DATA:::', parseData);

	// hiddenのlastid設定
	$('#chat_lastid').val(parseData.last_id);

	// チャットHTML生成
	var html = $().createChatHtml(parseData.chat_data);

	// HTML追加
	$('#chatarea').append(html);

	// 追加したHTMLの分スクロール
	$().scrollInputMessageArea();

	return false;
};

// チャットデータHTML生成
$.fn.createChatHtml = function(chatData) {
	var html = '';
	for(var cnt = 0; cnt < chatData.length; cnt++) {
		if (chatData[cnt].is_self == 1) {
			// 自身の投稿
			html += '<div class="chat_box_right">';
			html += '<div class="chat_area_right">';
			html += '<div class="chat_hukidashi_right">';
			html += $().htmlspecialchars(chatData[cnt].post);
			if (chatData[cnt].image_file != null) {
				html += '<img class="img-responsive center-block" src="' + chatData[cnt].image_file + '">';
			}
			html += '</div></div>';
			html += '<div class="chat_area_right">';
			html += '<div class="chat_info chat_info_right">';
			html += chatData[cnt].nickname + '&nbsp;' + chatData[cnt].created_at;
			html += '</div></div></div>';
		} else {
			// チャット相手の投稿
			html += '<div class="chat_box_left">';
			html += '<div class="chat_face_left">';
			html += '<img class="img-responsive img-circle" src="' + chatData[cnt].profile_img + '">';
			html += '</div>';
			html += '<div class="chat_area_left">';
			html += '<div class="chat_hukidashi_left">';
			html += $().htmlspecialchars(chatData[cnt].post);
			if (chatData[cnt].image_file != null) {
				html += '<img class="img-responsive center-block" src="' + chatData[cnt].image_file + '">';
			}
			html += '</div></div>';
			html += '<div class="chat_area_left">';
			html += '<div class="chat_info chat_info_left">';
			html += chatData[cnt].nickname + '&nbsp;' + chatData[cnt].created_at;
			html += '</div></div></div>';
		}
	}
	return html;
};

// 入力エリアまでスクロール
$.fn.scrollInputMessageArea = function() {
	if (isPastGetting) {
		// 過去データ取得中は入力エリアまでスクロールさせない
		return;
	}
	var target = $('#chat_message').offset().top;
	$('html,body').animate({scrollTop:target}, 500, 'swing');
	$('#chat_message').focus();
};

// 特殊文字をHTMLエンティティに変換する
$.fn.htmlspecialchars = function(ch) {
	ch = ch.replace(/&/g,"&amp;");
	ch = ch.replace(/"/g,"&quot;");
	ch = ch.replace(/'/g,"&#039;");
	ch = ch.replace(/</g,"&lt;");
	ch = ch.replace(/>/g,"&gt;");
	return ch ;
};
