/*
 * 日記一覧画面JavaScript
 *
 * @author Mitsuharu Shimizu
 * @version 1.0.1
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
var isSending = false;
var tmpScrollTop = 0;
$(function() {
	$(".diarylist_display_check").bootstrapSwitch();

	$(window).scrollTop(1); // scrollTopが0だと上スクロールイベントが発生しないので、1pxスクロールしておく

	$(window).on("scroll", function() {
		var nowScrollTop = $(window).scrollTop();
		var isDown = false;
		if (tmpScrollTop < nowScrollTop) {
			isDown = true;
		}
		tmpScrollTop = nowScrollTop;

		var scrollHeight = $(document).height(); // ページの高さ
		var scrollPosition = $(window).height() + $(window).scrollTop(); // 表示領域の高さ + スクロール位置
		if ((scrollHeight - scrollPosition) / scrollHeight <= 0.05) {
			// スクロール位置がページ下部の5%の範囲に入った場合
			if (isSending || !nextPageUrl) {
				// 次ページ取得処理中
				// あるいは、
				// 次ページが存在しない場合は何もしない
				return false;
			}
			isSending = true;
			$().showLoading(); // ローディング表示
			$.ajax({
				url: nextPageUrl, // 次のページのHTMLを取得
				method: 'get',
			}).done(function(data, status, xhr) {
				// 通信成功
//				console.log('SUCCESS', data);
				if (!$(data).find('#next_page').attr("href")) {
					// 取得したHTMLに、次のページが無ければnullを設定しておく
					nextPageUrl = null;
				} else {
					nextPageUrl = $(data).find('#next_page').attr("href"); // 取得したHTMLから、次のページのURLを取得して設定し直す
				}
				$(data).find('#diary_list').each(function() { // 取得したHTMLから日記一覧の部分を抜き出す
					$('#diary_list').append('<hr>');
					$('#diary_list').append($(this).html()); // 取得したHTMLを日記一覧の最後に追加
					$(".diarylist_display_check").bootstrapSwitch();
					return false;
				});
			}).fail(function(xhr, status, error) {
				// 通信失敗
				console.log('ERROR', xhr, status, error);
			}).always(function(arg1, status, arg2) {
				// 通信完了
				isSending = false;
				// ローディング非表示
				$().hideLoading();
			});
		} else if ($(window).scrollTop() <= 100 && !isDown) {
			// スクロール位置が100以下で上スクロールの場合
			if (isSending || !beforePageUrl) {
				// 前ページ取得処理中
				// あるいは、
				// 前ページが存在しない場合は何もしない
				return false;
			}
			isSending = true;
			$().showLoading(); // ローディング表示
			$.ajax({
				url: beforePageUrl, // 前のページのHTMLを取得
				method: 'get',
			}).done(function(data, status, xhr) {
				// 通信成功
//				console.log('SUCCESS', data);
				if (!$(data).find('#before_page').attr("href")) {
					// 取得したHTMLに、前のページが無ければnullを設定しておく
					beforePageUrl = null;
				} else {
					beforePageUrl = $(data).find('#before_page').attr("href"); // 取得したHTMLから、前のページのURLを取得して設定し直す
				}
				$('#diary_list').prepend('<hr>');
				$('#diary_list').prepend($(data).find('#diary_list').html()); // 取得したHTMLを日記一覧の先頭に追加
				if (tmpScrollTop == 0) {
					// scrollTopが0だと次の上スクロールイベントが発生しないので、1pxスクロールしておく
					// （ただし、現在位置がスクロールしている場合は、現在位置以上に上に戻すと、
					// 　連続して上スクロールイベントが発生してしまうのでなにもしない
					$(window).scrollTop(1);
				}
				$(".diarylist_display_check").bootstrapSwitch();
			}).fail(function(xhr, status, error) {
				// 通信失敗
				console.log('ERROR', xhr, status, error);
			}).always(function(arg1, status, arg2) {
				// 通信完了
				isSending = false;
				// ローディング非表示
				$().hideLoading();
			});
		}
	});

	// 日記表示/非表示切り替えスイッチ変更
	$(document).on('switchChange.bootstrapSwitch', '[id^=diarylist_display_check_]', function(event, state) {
		var
		str = $(this).attr("id"),
		uid = str.match(/\d/g).join("");
		// 該当ユーザーのフォーム送信
		$('#diarylist_hide_edit_'+uid).submit();
	});
});
