/*
 * フォーラム画面JavaScript
 *
 * @author Mitsuharu Shimizu
 * @version 1.0.1
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
$(function() {

	// 「新しいフォーラム＋」クリック
	$('#new_forum_modal').on('show.bs.modal', function (event) {
		// 表示されているエラーのツールチップを非表示にする
		$('.tooltip-validation').hide();
		var modal = $(this);									//モーダルを取得
		modal.find('.modal-body input#forum_name').val('');		//空にする
		modal.find('.modal-body textarea#postmsg').val('');		//空にする
		$('#upload_imagefile_thumbnail').attr('src', '');		//空にする
		$('#upload_imagefile_thumbnail').css({'display':'none'});
	});

	// 「削除」クリック
	$('#delete_forum_modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);						//モーダルを呼び出すときに使われたボタンを取得
		var fid = button.data('fid');								//data-fid の値を取得
		var modal = $(this);										//モーダルを取得
		modal.find('.modal-body input#fid').val(fid.toString());	//inputタグにも表示
	});

	// 画像選択時にサムネイル表示
	$('#upload_imagefile').change(function() {
		if (this.files.length > 0) {
			// 選択されたファイル情報を取得
			var file = this.files[0];
			// readerのresultプロパティに、データURLとしてエンコードされたファイルデータを格納
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function() {
				$('#upload_image_btn_area').css({'margin-right':'1em'});
				$('#upload_imagefile_thumbnail').css({'display':'block', 'width':'60px'});
				$('#upload_imagefile_thumbnail').attr('src', reader.result);
			}
		}
	});

	// 「新しいフォーラム＋」登録バリデーション
	$('#forum_registration').validate({
		rules: {
			forum_name :{
				required: true,
				maxlength: forumNameMax
			},
			postmsg :{
				required: true,
				maxlength: postmsgMax
			}
		},
		messages: {
			forum_name :{
				required: errMsgForumNameRequired,
				maxlength: forumNameMax + errMsgForumNameMaxlength
			},
			postmsg :{
				required: errMsgPostMsgRequired,
				maxlength: postmsgMax + errMsgPostMsgMaxlength
			}
		}
	});

});
