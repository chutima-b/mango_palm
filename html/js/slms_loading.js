/*
 * ローディング表示用JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

// ローディング表示
$.fn.showLoading = function() {
	$("body").append("<div id='slms_loading'></div>");
};

// ローディング削除
$.fn.hideLoading = function(){
	$("#slms_loading").remove();
}
