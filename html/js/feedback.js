/*
 * フィードバック画面JavaScript
 *
 * @author Mitsuharu Shimizu
 * @version 1.0.1
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
$(function() {

	$('#feedback_form').validate();

	// フィードバックボタン押下
	$('#feedback_submit').click(function() {
		// 下書きフラグを落としておく
		$('#draft_flg').val(0);
		// 評価点のルールを追加
		$("[id^=points_]").each(function() {
			var
				str = $(this).attr("id"),
				id = str.match(/\d/g).join("");
			$(this).rules('add', {
				required: true,
				maxlength: pointMax,
				max : parseInt($('#evaluation_points_' + id).val()),
				min : 0,
				messages: {
					required: errMsgFeedbackpointsRequired,
					maxlength: pointMax + errMsgFeedbackPointMaxLength,
					max : errMsgFeedbackPointMaxValue1 + $('#evaluation_points_' + id).val() + errMsgFeedbackPointMaxValue2,
					min : errMsgFeedbackPointMinValue
				}
			});
		});
		// フィードバック用バリデーションルール追加
		// ※何故か外側で追加するとエラーになるのでここで追加
		$().addValidationFeedbackRules();
		// フォーム送信
		$('#feedback_form').submit();
	});

	// 下書き保存ボタン押下
	$('#draft_submit').click(function() {
		// 表示されているエラーのツールチップを非表示にする
		$('.tooltip-validation').hide();
		// 下書きフラグを設定
		$('#draft_flg').val(1);
		// 評価点のルールを削除
		$("[id^=points_]").each(function() {
			$(this).rules('remove');
		});
		// フィードバック用バリデーションルール追加
		// ※何故か外側で追加するとエラーになるのでここで追加
		$().addValidationFeedbackRules();
		// フォーム送信
		$('#feedback_form').submit();
	});

});

// バリデーションルール追加：フィードバック
$.fn.addValidationFeedbackRules = function() {
	$("textarea[id^=feedback_]").each(function() {
		$(this).rules('add', {
			maxlength: feedbackMessageMax,
			messages: {
				maxlength: feedbackMessageMax + errMsgFeedbackMessageMaxlength
			}
		});
	});
};
