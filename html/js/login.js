/*
 * ログイン画面JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.1
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
$(function() {
	$('#login_userid').focus();

	$('#login_btn_guest').click(function() {
		$('#login_form').attr('action', gestLoginUrl);
		$('#login_form').submit();
	});
});
