/*
 * 課題編集画面JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.5
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
var pageurl = location.protocol+'//'+location.hostname+location.pathname;
var params = location.search;
$(function() {
	var descriptionElement = $('#taskedit_description');
	// テーマ説明テキストエリアのWYSIWYG適用
	descriptionElement.summernote({
		lang: summernoteLang,
		toolbar: [
			['style', ['style', 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
			['fonts', ['fontsize', 'fontname']],
			['color', ['color']],
//			['undo', ['undo', 'redo', 'help']],
			['undo', ['undo', 'redo']],
			['ckMedia', ['ckImageUploader', 'ckVideoEmbeeder']],
			['misc', ['link', 'picture', 'video', 'table', 'hr', 'codeview', 'fullscreen']],
			['para', ['ul', 'ol', 'paragraph', 'leftButton', 'centerButton', 'rightButton', 'justifyButton', 'outdentButton', 'indentButton']],
			['height', ['lineheight']],
		],
		height: 300,
		fontNames: ["ＭＳ Ｐゴシック", "ＭＳ Ｐ明朝","メイリオ", "YuGothic","Yu Gothic","Hiragino Kaku Gothic Pro","sans-serif", "Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana"],
		callbacks: {
			onImageUpload: function(files, editor, welEditable) {
				$().descriptImgUpload(files[0], editor, welEditable);
			},
			// jquery-validationとsummernote併用時の不具合回避
			onChange: function(contents, $editable) {
				descriptionElement.val(descriptionElement.summernote('isEmpty') ? "" : contents);
				validateResult.element(descriptionElement);
			}
		},
	});

	// 「課題のタイプ」のプルダウン変更
	$('#taskedit_type').change(function() {
		// テキストエリア、「正解」表示設定
		$().setupSelectionTextAndQuizAnswer();
	});

	// 「登録」ボタンクリック
	$('#taskedit_btn_regist').click(function() {
		var selectedTaskeditType = $('#taskedit_type').val();
		if (selectedTaskeditType == taskTypeSingleSelection
		 || selectedTaskeditType == taskTypeMultipleChoice
		 || selectedTaskeditType == taskTypeQuizSingleSelection
		 || selectedTaskeditType == taskTypeSurveySingleSelection
		 || selectedTaskeditType == taskTypeSurveyMultipleChoice
		 ) {
			// 単一選択または複数選択またはクイズ（単一選択）
			// または、アンケート（単一選択）、アンケート（複数選択）の場合は、
			// テキストエリアに選択肢を入力させる
			$('#taskedit_choices').rules("add", {
				required: true,
				messages: {
					required: errMsgChoicesTextareaRequired
				}
			});
		} else {
			$('#taskedit_choices').rules("remove");
			$('#taskedit_choices').val('');
		}

		// 解説の入力文字数をチェック
		$('#taskedit_comment').rules("add", {
			maxlength: taskCommentMax,
			messages: {
				maxlength: taskCommentMax + errMsgCommentMaxlength
			}
		});

		// クイズの場合
		if ($('#taskedit_quiz').val() == "1") {
			if (selectedTaskeditType == taskTypeQuizSingleSelection) {
				// クイズ（単一選択）の場合は、「正解」の選択肢のいずれかにチェックがあるか確認
				$("input[name='taskedit_quiz_answer']").rules("add", {
					required: true,
					messages: {
						required: errMsgQuizAnswerRequired
					}
				});

			} else {
				$("input[name='taskedit_quiz_answer']").rules("remove");
			}
		}

		$('#taskedit_form').submit();
	});

	// フォームバリデーション
	// jquery-validationとsummernote併用時の不具合回避のため、
	// 戻り値を保持しておく
	var validateResult = $('#taskedit_form').validate({
		rules: {
			taskedit_name :{
				required: true,
				maxlength: taskNameMax
			},
			taskedit_description :{
				required: true,
				maxlength: taskDescriptionMax
			},
			taskedit_type :{
				required: true
			},
			taskedit_evaluation_points :{
				required: true
			}
		},
		messages: {
			taskedit_name :{
				required: errMsgTasknameRequired,
				maxlength: taskNameMax + errMsgTasknameMaxlength
			},
			taskedit_description :{
				required: errMsgDescriptionRequired,
				maxlength: taskDescriptionMax + errMsgDescriptionMaxlength
			},
			taskedit_type :{
				required: errMsgTaskTypeRequired
			},
			taskedit_evaluation_points :{
				required: errMsgEvaluationPoints
			}
		},
		// jquery-validationとsummernote併用時の不具合回避
		ignore: ":hidden:not(#theme_description),.note-editable.panel-body"
	});

	// 画面ロード時に、テキストエリア、「正解」表示設定
	$().setupSelectionTextAndQuizAnswer();

	// クイズ（単一選択）の場合は、画面ロード時に、
	// 選択肢欄および、クイズ正解の値を元に、
	// 「正解」のhtmlを生成する。
	if ($('#taskedit_type').val() == taskTypeQuizSingleSelection) {
		var quzAnswerHtml = $().createQuizAnswerHtml($("#taskedit_choices").val(), quizAnswer);
		$("#taskedit_quiz_answer_area").html(quzAnswerHtml);
	}

	// クイズ（単一選択）の場合は、
	// 選択肢に入力した内容で「正解」を生成する。
	$("#taskedit_choices").keyup(function() {
		if ($('#taskedit_type').val() != taskTypeQuizSingleSelection) {
			return;
		}
		// クイズ（単一選択）の場合は、
		// 選択肢に入力した内容で「正解」のhtmlを生成する。
		quzAnswerHtml = $().createQuizAnswerHtml(this.value, null);
		$("#taskedit_quiz_answer_area").html(quzAnswerHtml);
	});

	$("#taskedit_quiz_answer_area").on('change', "input[name='taskedit_quiz_answer']", function() {
		// 「正解」の選択肢を操作した際は、入力チェックのエラーメッセージを削除する。
		// ※親要素を指定して子要素のセレクターを指定しているのは、
		// 　あとから生成した子要素のイベントを拾えるようにするため。
		$('#taskedit_quiz_answer_error').text('');
	});

});

// テキストエリア、「正解」表示設定
$.fn.setupSelectionTextAndQuizAnswer = function() {
	var selectedTaskeditType = $('#taskedit_type').val();
	if (selectedTaskeditType != ''
	 && (selectedTaskeditType == taskTypeSingleSelection
			 || selectedTaskeditType == taskTypeMultipleChoice
			 || selectedTaskeditType == taskTypeQuizSingleSelection
			 || selectedTaskeditType == taskTypeSurveySingleSelection
			 || selectedTaskeditType == taskTypeSurveyMultipleChoice
	 	)
	 ) {
		// 単一選択または複数選択またはクイズ（単一選択）
		// または、アンケート（単一選択）、アンケート（複数選択）の場合、
		// テキストエリアの入力を有効にする。
		$('#taskedit_choices').prop('disabled', false);

		if (selectedTaskeditType == taskTypeQuizSingleSelection) {
			// クイズ（単一選択）の場合、「正解」を表示する。
			$('#taskedit_quiz_answer_form_group').show();
		} else {
			// クイズ（単一選択）の以外場合、「正解」を非表示にする。
			$('#taskedit_quiz_answer_form_group').hide();
		}
	} else {
		// 単一選択または複数選択またはクイズ（単一選択）
		// または、アンケート（単一選択）、アンケート（複数選択）以外の場合、
		// テキストエリアの入力を無効にする。
		$('#taskedit_choices').prop('disabled', true);
	}
};

// 選択肢入力欄に入力した内容で
// 「正解」のhtmlを生成する。
// quizAnswer（クイズ正解）の指定がある場合には、
// quizAnswerに指定された選択肢をチェックする。
$.fn.createQuizAnswerHtml = function(str, quizAnswer) {
	if (str == null || str == '') {
		return '';
	}
	str = $().htmlspecialchars(str);
	str = str.replace(/\r\n/g, '\n');
	str = str.replace(/\r/g, '\n');
	var lines = str.split('\n');
	var rtn = '';
	var cnt = 0; // ループ内で処理しないでぬけるケースがあるのでindexは使わないで独自にカウントする
	$.each(lines, function(index, value) {
		if (value == '') {
			return true;
		}
		if (cnt > 0) {
			rtn += '<br>';
		}
		var checked = '';
		if (!isNaN(quizAnswer) && quizAnswer == (cnt+1)) {
			// quizAnswerが数値かつ、quizAnswerに指定された選択肢の場合、
			// 選択肢をチェック状態にする。
			checked = ' checked';
		}
		rtn += '<input type="radio" name="taskedit_quiz_answer" value="' + (cnt+1) +'"' + checked + '>' + value;
		cnt++;
	});

	return rtn;
};

//特殊文字をHTMLエンティティに変換する
$.fn.htmlspecialchars = function(ch) {
	ch = ch.replace(/&/g,"&amp;");
	ch = ch.replace(/"/g,"&quot;");
	ch = ch.replace(/'/g,"&#039;");
	ch = ch.replace(/</g,"&lt;");
	ch = ch.replace(/>/g,"&gt;");
	return ch ;
};

// 問題画像ファイルアップロード
$.fn.descriptImgUpload = function(file, editor, welEditable) {
	data = new FormData();
	data.append("descriptimg", file);
	$.ajax({
		data: data,
		type: "POST",
		url: pageurl + "/descriptimg" + params,
		cache: false,
		contentType: false,
		processData: false,
		success: function(url) {
//			editor.insertImage(welEditable, url);
			$('#taskedit_description').summernote('editor.insertImage', url);
		},
		error: function(response){
			$('#error').html(response.responseText);
		},
		complete: function() {
		},
	});
};
