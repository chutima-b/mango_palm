/*
 * 日記Like!機能JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */
var isSendingLike = false;
$(function() {
	// Bootstrap Tooltip
	$('[data-toggle="tooltip"]').tooltip();

	// 「Like!」クリック
	// ※クリックの位置で子要素のエレメントになってしまう事があるので
	// 　aタグを指定しておく
	$("a[id^=diary_like_]").click(function() {
		$().sendDiaryLike($(this));
	});

});

// 「Like!」または「Like!を取り消す」送信
$.fn.sendDiaryLike = function(element) {
	if (isSendingLike) {
		return false;
	}
	isSendingLike = true;
	try {
		var
			strId = element.attr("id"),
			did = strId.match(/\d/g).join(""),
			strLikeMode = element.attr("like-mode"),
			strCid = element.attr("cid"),
			isDelete = false,
			url = diaryAddUrl;

		if (strLikeMode == '0') {
			isDelete = true;
			url = diaryDelUrl;
		}
		url += strCid;
		$.ajax({
			url: url,
			type: 'post',
			async: true,
			data: {
				'did': did
			}
		}).done(function(response) {
			// 通信成功
			//console.log('SUCCESS', response);
			$().changeDiaryLikeHtml(element, did, response, isDelete);
		}).fail(function(response) {
			// 通信失敗
			console.log('ERROR', response.responseText, response);
		}).always(function(arg1, status, arg2) {
			// 通信完了
			isSendingLike = false;
		});
	} catch (e) {
		console.log(e);
		isSendingLike = false;
	}
};

// 「Like!」押下後の表示内容差替え処理
$.fn.changeDiaryLikeHtml = function(element, did, responseData, isDelete) {
	// 「Like!」の数更新
	var parseData = JSON.parse(JSON.stringify(responseData));
	$('#diary_like_cnt_'+did).text(parseData.count);

	// 各種属性、cssクラス差替え
	var likeMode = '0',
		tooltipTitle = diaryLikeTooltipDel,
		remove_diary_like_glyphicon_class = 'glyphicon glyphicon-heart-empty diary_heart_empty',
		add_diary_like_glyphicon_class = 'glyphicon glyphicon-heart diary_heart',
		remove_diary_like_cnt_class = 'diary_heart_empty',
		add_diary_like_cnt_class = 'diary_heart';

	// 削除だった場合は追加に変更
	if (isDelete) {
		likeMode = '1';
		tooltipTitle = diaryLikeTooltipAdd,
		remove_diary_like_glyphicon_class = 'glyphicon glyphicon-heart diary_heart';
		add_diary_like_glyphicon_class = 'glyphicon glyphicon-heart-empty diary_heart_empty';
		remove_diary_like_cnt_class = 'diary_heart';
		add_diary_like_cnt_class = 'diary_heart_empty';
	}

	// htmlに反映
	element.attr('like-mode', likeMode);
	element.attr('title', tooltipTitle).tooltip('fixTitle').tooltip('hide');
	$('#diary_like_glyphicon_'+did).removeClass(remove_diary_like_glyphicon_class);
	$('#diary_like_glyphicon_'+did).addClass(add_diary_like_glyphicon_class);
	$('#diary_like_cnt_'+did).removeClass(remove_diary_like_cnt_class);
	$('#diary_like_cnt_'+did).addClass(add_diary_like_cnt_class);
};
