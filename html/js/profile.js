/*
 * プロフィール画面JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.1
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
$(function() {
	$('#profile_chat_use').bootstrapSwitch();

	$('#profile_btn_imgupload').click(function() {
		$('#profile_imgupload').click();
		return false;
	});

	$('#profile_imgupload').change(function() {
		if (this.files.length > 0) {
			// 選択されたファイル情報を取得
			var file = this.files[0];
			// readerのresultプロパティに、データURLとしてエンコードされたファイルデータを格納
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function() {
				$('#thumbnail').attr('src', reader.result);
				$('#thumbnail').css({'width':pictureMaxLength});
			}
		}
	});

	$('#profile_btn_regist').click(function() {
		// 新しいパスワードが入力されている場合
		if ($('#profile_new_passwd').val() != '') {
			// 現在のパスワードの入力チェックをルールに追加
			$('#profile_passwd').rules("add", {
				required: true,
				messages: {
					required: errMsgPasswdRequired
				}
			});
			// 新しいパスワードの確認の入力チェックをルールに追加
			$('#profile_new_passwd_confirm').rules("add", {
				required: true,
				messages: {
					required: errMsgNewPasswdConfirmRequired
				}
			});
		} else {
			$('#profile_passwd').rules("remove");
			$('#profile_new_passwd_confirm').rules("remove");
		}
	});

	$('#profile_form').validate({
		rules: {
			profile_nickname :{
				required: true,
				maxlength: nicknameMax
			},
			profile_introduction :{
				maxlength: introductionMax
			},
			profile_passwd :{
				minlength: passwdMin,
				maxlength: passwdMax
			},
			profile_new_passwd :{
				minlength: passwdMin,
				maxlength: passwdMax
			},
			profile_new_passwd_confirm :{
				minlength: passwdMin,
				maxlength: passwdMax,
				equalTo: "#profile_new_passwd",
			},
		},
		messages: {
			profile_nickname :{
				required: errMsgNicknameRequired,
				maxlength: nicknameMax + errMsgNicknameMaxlength
			},
			profile_introduction :{
				maxlength: introductionMax + errMsgIntroductionMaxlength
			},
			profile_passwd :{
				minlength: passwdMin + errMsgMinlength,
				maxlength: passwdMax + errMsgMaxlength
			},
			profile_new_passwd :{
				minlength: passwdMin + errMsgMinlength,
				maxlength: passwdMax + errMsgMaxlength
			},
			profile_new_passwd_confirm :{
				minlength: passwdMin + errMsgMinlength,
				maxlength: passwdMax + errMsgMaxlength,
				equalTo: errMsgNewPasswdConfirmEqualto
			},
		}
	});
});
