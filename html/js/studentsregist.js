/*
 * 参加者登録画面JavaScript
 *
 * @author Mitsuharu Shimizu
 * @version 1.0.1
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
var isSending = false;
var tmpScrollTop = 0;
$(function() {
	$(window).scrollTop(1); // scrollTopが0だと上スクロールイベントが発生しないので、1pxスクロールしておく

	$(window).on("scroll", function() {
		var nowScrollTop = $(window).scrollTop();
		var isDown = false;
		if (tmpScrollTop < nowScrollTop) {
			isDown = true;
		}
		tmpScrollTop = nowScrollTop;

		var scrollHeight = $(document).height(); // ページの高さ
		var scrollPosition = $(window).height() + $(window).scrollTop(); // 表示領域の高さ + スクロール位置
		if ((scrollHeight - scrollPosition) / scrollHeight <= 0.05) {
			// スクロール位置がページ下部の5%の範囲に入った場合
			if (isSending || !nextPageUrl) {
				// 次ページ取得処理中
				// あるいは、
				// 次ページが存在しない場合は何もしない
				return false;
			}
			isSending = true;
			$().showLoading(); // ローディング表示
			$.ajax({
				url: nextPageUrl, // 次のページのHTMLを取得
				method: 'get',
			}).done(function(data, status, xhr) {
				// 通信成功
//				console.log('SUCCESS', data);
				if (!$(data).find('#next_page').attr("href")) {
					// 取得したHTMLに、次のページが無ければnullを設定しておく
					nextPageUrl = null;
				} else {
					nextPageUrl = $(data).find('#next_page').attr("href"); // 取得したHTMLから、次のページのURLを取得して設定し直す
				}
				$(data).find('#studentsregist_table_userlist').each(function() { // 取得したHTMLからユーザー一覧の部分を抜き出す
					$(this).children('thead').remove(); // 取得したHTMLからカラム名の部分を削除
					$('#studentsregist_table_userlist').append($(this).html()); // 取得したHTMLをユーザー一覧の最後に追加
					return false;
				});
			}).fail(function(xhr, status, error) {
				// 通信失敗
				console.log('ERROR', xhr, status, error);
			}).always(function(arg1, status, arg2) {
				// 通信完了
				isSending = false;
				// ローディング非表示
				$().hideLoading();
			});
		} else if ($(window).scrollTop() <= 100 && !isDown) {
			// スクロール位置が100以下で上スクロールの場合
			if (isSending || !beforePageUrl) {
				// 前ページ取得処理中
				// あるいは、
				// 前ページが存在しない場合は何もしない
				return false;
			}
			isSending = true;
			$().showLoading(); // ローディング表示
			$.ajax({
				url: beforePageUrl, // 前のページのHTMLを取得
				method: 'get',
			}).done(function(data, status, xhr) {
				// 通信成功
//				console.log('SUCCESS', data);
				if (!$(data).find('#before_page').attr("href")) {
					// 取得したHTMLに、前のページが無ければnullを設定しておく
					beforePageUrl = null;
				} else {
					beforePageUrl = $(data).find('#before_page').attr("href"); // 取得したHTMLから、前のページのURLを取得して設定し直す
				}
				$('#studentsregist_table_userlist').children('thead').remove(); // 挿入元HTMLからカラム名の部分を削除
				$('#studentsregist_table_userlist').prepend($(data).find('#studentsregist_table_userlist').html()); // 取得したHTMLをユーザー一覧の先頭に追加
				if (tmpScrollTop == 0) {
					// scrollTopが0だと次の上スクロールイベントが発生しないので、1pxスクロールしておく
					// （ただし、現在位置がスクロールしている場合は、現在位置以上に上に戻すと、
					// 　連続して上スクロールイベントが発生してしまうのでなにもしない
					$(window).scrollTop(1);
				}
			}).fail(function(xhr, status, error) {
				// 通信失敗
				console.log('ERROR', xhr, status, error);
			}).always(function(arg1, status, arg2) {
				// 通信完了
				isSending = false;
				// ローディング非表示
				$().hideLoading();
			});
		}
	});


	// 登録ボタンクリック
	$("[id^=studentsregist_btn_regist_]").click(function() {
		$('.studentsregist_btn_regist').prop("disabled", true);
		var
			str = $(this).attr("id"),
			uid = str.match(/\d/g).join("");
		$('#studentsregist_form_regist_'+uid).submit();
		return false;
	});

	// 検索バリデーション
	$('#students_search_form').validate({
		rules: {
			userid :{
				maxlength: useridMax
			},
			family :{
				maxlength: familyMax
			},
			first :{
				maxlength: firstMax
			}
		},
		messages: {
			userid :{
				maxlength: useridMax + errMsgUserIDMaxlength
			},
			family :{
				maxlength: familyMax + errMsgFamilyMaxlength
			},
			first :{
				maxlength: firstMax + errMsgFirstMaxlength
			}
		}
	});

});
