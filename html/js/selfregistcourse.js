/*
 * 講座申込画面（受講講座自己登録）JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */
$(function() {
	// 「講座を見る」ボタンクリック
	$("[id^=btn_course_info_]").click(function() {
		var
			str = $(this).attr("id"),
			cid = str.match(/\d/g).join(""),
			genreIcon = $('#genre_icon_' + $('#course_genre_' + cid).val()), // ジャンルアイコン
			courseName = $('#course_name_' + cid).text(), // 講座名
			teacherName = $('#course_teacher_name_' + cid).text(), // 講師名
			teacherPicture = $('#course_teacher_picture_' + cid).val(), // 講師写真
			courseDescription = $('#course_description_' + cid).text(); // 講座紹介

		// ダイアログ入力項目初期化
		if (genreIcon[0]) { // ジャンルアイコンの要素が取得できた場合
			$('#modal_genre_icon').attr('src', genreIcon.attr('src')); // ジャンルアイコン
			$('#modal_genre_icon').show();
		} else {
			$('#modal_genre_icon').attr('src', null);
			$('#modal_genre_icon').hide();
		}
		$('#modal_course_name').text(courseName); // 講座名
		$('#modal_teacher').text(teacherName); // 講師名
		$('#modal_teacher_picture').attr('src', teacherPicture); // 講師写真
		$('#modal_course_description').html(courseDescription); // 講座紹介
		$('#modal_cid').val(cid);

		// ダイアログ表示
		$('#modal_regist_course').modal('show');
		return false;
	});

});
