/*
 * 講座編集画面JavaScript
 *
 * @author Takahiro Kimura
 * @version 1.0.5
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
var pathname = location.pathname.replace(/\/regist/, '');
pathname = pathname.replace(/\/delete/, '');
var pageurl = location.protocol+'//'+location.hostname+pathname;
var params = location.search;

$(function() {
	var descriptionElement = $('#courseedit_description');
	// 講座紹介テキストエリアのWYSIWYG適用
	descriptionElement.summernote({
		lang: summernoteLang,
		toolbar: [
			['style', ['style', 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
			['fonts', ['fontsize', 'fontname']],
			['color', ['color']],
//			['undo', ['undo', 'redo', 'help']],
			['undo', ['undo', 'redo']],
			['ckMedia', ['ckImageUploader', 'ckVideoEmbeeder']],
			['misc', ['link', 'picture', 'video', 'table', 'hr', 'codeview', 'fullscreen']],
			['para', ['ul', 'ol', 'paragraph', 'leftButton', 'centerButton', 'rightButton', 'justifyButton', 'outdentButton', 'indentButton']],
			['height', ['lineheight']],
		],
		height: 300,
		fontNames: ["ＭＳ Ｐゴシック", "ＭＳ Ｐ明朝","メイリオ", "YuGothic","Yu Gothic","Hiragino Kaku Gothic Pro","sans-serif", "Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana"],
		callbacks: {
			onImageUpload: function(files, editor, welEditable) {
				$().descriptImgUpload(files[0], editor, welEditable);
			},
			// jquery-validationとsummernote併用時の不具合回避
			onChange: function(contents, $editable) {
				descriptionElement.val(descriptionElement.summernote('isEmpty') ? "" : contents);
				validateResult.element(descriptionElement);
			}
		},
	});

	// チェックボックスのスタイル適用
	$("[type='checkbox']").bootstrapSwitch();

	if (firstThemeDate != '' && lastThemeLimit != '') {
		// テーマの日付、期限が設定されている場合
		var now = new Date();
		// テーマの日付が今日の日付より前の場合は、
		// テーマの日付を開始日のstartDateに設定。
		// テーマの日付が今日の日付以降の場合は、
		// 今日の日付を開始日のstartDateに設定。
		var startStartDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		var themeDate = new Date(firstThemeDate);
		if (themeDate < startStartDate) {
			startStartDate = themeDate;
		}
		// テーマの期限が、今日の日付より後の場合は、
		// テーマの期限を終了日のstartDateに設定。
		// テーマの期限が、今日の日付以前の場合は、
		// 今日の日付を終了日のstartDateに設定。
		var startEndDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		var themeLimit = new Date(lastThemeLimit);
		if (themeLimit > startEndDate) {
			startEndDate = themeLimit;
		}

		// 開始日用datepicker
		$("#courseedit_startdate").datepicker( {
			format: 'yyyy/mm/dd',
			language: datepickerLang,
			startDate: startStartDate,
			autoclose: true
		});

		// 終了日用datepicker
		$("#courseedit_enddate").datepicker( {
			format: 'yyyy/mm/dd',
			language: datepickerLang,
			startDate: startEndDate,
			autoclose: true
		});
	} else {
		// テーマの日付、期限が設定されていない場合
		// 今日の日付をstartDateに設定
		// 開始日用datepicker
		$("#courseedit_startdate").datepicker( {
			format: 'yyyy/mm/dd',
			language: datepickerLang,
			startDate: Date(),
			autoclose: true
		});

		// 終了日用datepicker
		$("#courseedit_enddate").datepicker( {
			format: 'yyyy/mm/dd',
			language: datepickerLang,
			startDate: Date(),
			autoclose: true
		});
	}

	// カラーピッカー表示
	$('#courseedit_calendar_color').simplecolorpicker({
		picker: true, theme: 'glyphicons'
	});

	// 「講座の期間を設定する」設定
	$().setupUseSchedule();

	// 「講座の期間を設定する」のスイッチ変更
	$('#courseedit_use_schedule').on('switchChange.bootstrapSwitch', function(event, state) {
		$().setupUseSchedule();
	});

	$('[data-toggle="tooltip"]').tooltip();

	$('#courseedit_use_schedule_guide_help').on('click', function () {
		$('#courseedit_use_schedule_guide').toggle();
	});

	$('#courseedit_permit_guest_guide_help').on('click', function () {
		$('#courseedit_permit_guest_guide').toggle();
	});

	// 「新しいテーマ」ボタンクリック
	$('#courseedit_btn_new_theme').click(function() {
		location.href = themeUrl;
	});

	// 「登録」ボタンクリック
	$('#courseedit_btn_regist').click(function() {
		if ($('#courseedit_use_schedule').prop('checked')) {
			$('#courseedit_startdate').rules("add", {
				required: true,
				messages: {
					required: errMsgStartdateRequired
				}
			});
			$('#courseedit_enddate').rules("add", {
				required: true,
				messages: {
					required: errMsgEnddateRequired
				}
			});
		} else {
			$('#courseedit_startdate').rules("remove");
			$('#courseedit_enddate').rules("remove");
		}
		if (descriptionElement.summernote('codeview.isActivated')) {
			descriptionElement.summernote('codeview.deactivate');
		}
		$('#courseedit_form').submit();
	});

	// 「削除」ボタンクリック
	$('#courseedit_btn_delete').click(function() {
		// 削除確認モーダルダイアログ表示
		$('#courseedit_modal_delete').modal('show');
	});

	// 削除確認モーダルダイアログのOKボタンクリック
	$('#courseedit_modal_delete_btn_ok').click(function() {
		// 削除処理実行
		location.href = deleteActionUrl;
		// ダイアログを閉じる
		$('#courseedit_modal_delete').modal('hide');
		// ローディング表示
		$().showLoading();
		return false;
	});

	// 登録フォームバリデーション
	// jquery-validationとsummernote併用時の不具合回避のため、
	// 戻り値を保持しておく
	var validateResult = $('#courseedit_form').validate({
		rules: {
			courseedit_courseid :{
				required: true,
				maxlength: 50
			},
			courseedit_coursename :{
				required: true,
				maxlength: 50
			},
			courseedit_genre :{
				required: true
			},
			courseedit_description :{
				required: true
			}
		},
		messages: {
			courseedit_courseid :{
				required: errMsgCouceidRequired,
				maxlength: errMsgCouceidMaxlength
			},
			courseedit_coursename :{
				required: errMsgCoucenameRequired,
				maxlength: errMsgCoucenameMaxlength
			},
			courseedit_genre :{
				required: errMsgGenreRequired
			},
			courseedit_description :{
				required: errMsgDescriptionRequired
			}
		},
		// jquery-validationとsummernote併用時の不具合回避
		ignore: ":hidden:not(#courseedit_description),.note-editable.panel-body"
	});

	// コピーフォームバリデーション
	$('#courseedit_form_copy').validate({
		rules: {
			courseedit_copy_courceid :{
				required: true,
				maxlength: 50
			},
			courseedit_copy_courcename :{
				required: true,
				maxlength: 50
			}
		},
		messages: {
			courseedit_copy_courceid :{
				required: errMsgCopyCouceidRequired,
				maxlength: errMsgCopyCouceidMaxlength
			},
			courseedit_copy_courcename :{
				required: errMsgCopyCoucenameRequired,
				maxlength: errMsgCopyCoucenameMaxlength
			}
		},
		// フォーム送信時の処理
		submitHandler: function(form) {
			// ダイアログを閉じる
			$('#courseedit_modal_copy').modal('hide');
			// ローディング表示
			$().showLoading();
			// フォーム送信
			form.submit();
		}
	});
});

// 講座紹介画像ファイルアップロード
$.fn.descriptImgUpload = function(file, editor, welEditable) {
	data = new FormData();
	data.append("descriptimg", file);
	$.ajax({
		data: data,
		type: "POST",
		url: pageurl + "/descriptimg" + params,
		cache: false,
		contentType: false,
		processData: false,
		success: function(url) {
//			editor.insertImage(welEditable, url);
			$('#courseedit_description').summernote('editor.insertImage', url);
		},
		error: function(response){
			$('#error').html(response.responseText);
		},
		complete: function() {
		},
	});
}

//「講座の期間を設定する」設定
$.fn.setupUseSchedule = function() {
	if ($('#courseedit_use_schedule').prop('checked')) {
		// 「ON」の場合
		// 「開始日」、「終了日」を有効にする
		$('#courseedit_startdate').prop('disabled', false);
		$('#courseedit_enddate').prop('disabled', false);
		// 「カレンダー」を有効にする
		$('#courseedit_calendar_color').prop('disabled', false);
		$('.simplecolorpicker').css("pointer-events", "auto");
	} else {
		// 「OFF」の場合
		// 「開始日」、「終了日」を無効にする
		$('#courseedit_startdate').prop('disabled', true);
		$('#courseedit_enddate').prop('disabled', true);
		// 「カレンダー」を無効にする
		$('#courseedit_calendar_color').prop('disabled', true);
		$('.simplecolorpicker').css("pointer-events", "none");
	}
};
