/*
 * ホーム画面（管理者）JavaScript
 *
 * @author Mitsuharu Shimizu
 * @version 1.0.3
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
var pageurl = location.protocol+'//'+location.hostname+location.pathname;
var params = location.search;

$(function() {
	// お知らせ編集ダイアログ呼び出し時のカラーピッカーの色保持用
	var noticeDialogLoadColor;

	var contentElement = $('#notice_content');
	// お知らせ編集ダイアログ「内容」テキストエリアのWYSIWYG適用
	contentElement.summernote({
		lang: summernoteLang,
		toolbar: [
			['style', ['style', 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
			['fonts', ['fontsize', 'fontname']],
			['color', ['color']],
//			['undo', ['undo', 'redo', 'help']],
			['undo', ['undo', 'redo']],
			['ckMedia', ['ckImageUploader', 'ckVideoEmbeeder']],
			['misc', ['link', 'picture', 'video', 'table', 'hr', 'codeview', 'fullscreen']],
			['para', ['ul', 'ol', 'paragraph', 'leftButton', 'centerButton', 'rightButton', 'justifyButton', 'outdentButton', 'indentButton']],
			['height', ['lineheight']],
		],
		height: 200,
		fontNames: ["ＭＳ Ｐゴシック", "ＭＳ Ｐ明朝","メイリオ", "YuGothic","Yu Gothic","Hiragino Kaku Gothic Pro","sans-serif", "Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana"],
		callbacks: {
			onImageUpload: function(files, editor, welEditable) {
				$().contentImgUpload(files[0], editor, welEditable);
			},
			// jquery-validationとsummernote併用時の不具合回避
			onChange: function(contents, $editable) {
				contentElement.val(contentElement.summernote('isEmpty') ? "" : contents);
				validateResult.element(contentElement);
			}
		},
		dialogsInBody: true // summernoteとbootstrapのmodal(showイベント)の衝突回避
	});

	// 「カレンダーに表示する」のスタイル適用
	$("#calendar_use").bootstrapSwitch();

	// 「カレンダーに表示する」のスイッチ変更
	$('#calendar_use').on('switchChange.bootstrapSwitch', function(event, state) {
		$().setupCalendarUse();
	});

	// 「カレンダーに表示する」設定
	$().setupCalendarUse();

	// 日付選択
	$('#targetdt').datepicker({
		language: datepickerLang,
		format: "yyyy/mm/dd",
		startDate: Date(),
		autoclose: true
	});

	// datepickerが表示されると何故か
	// お知らせダイアログの'show.bs.modal'や'hide.bs.modal'イベントが発生し
	// 挙動がおかしくなってしまうので、それぞれのイベントを抑止する
	$('#targetdt').on("show", function(e){
		e.preventDefault();
		e.stopPropagation();
	}).on("hide", function(e){
		e.preventDefault();
		e.stopPropagation();
	});

	// カラーピッカー表示
	$('#color').simplecolorpicker({
		picker: true, theme: 'glyphicons'
	});

	// 「新しいお知らせ」「編集」ダイアログが
	$('#new_post_modal').on('shown.bs.modal', function (event) {
		// エラー表示後に一旦ダイアログをキャンセルし、
		// 再度ダイアログを開くと、show.bs.modalイベントのあとに、
		// なぜか「内容」のエラーメッセージだけ再表示されてしまうので、
		// エラーメッセージをもう一度消しておく
		$('.tooltip-validation').hide();
	});

	// 「新しいお知らせ」「編集」クリック
	$('#new_post_modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);								// モーダルを呼び出すときに使われたボタンを取得
		var nid = button.data('nid');										// data-nid の値を取得
		var targetdt = button.data('targetdt');								// data-targetdt の値を取得
		var title = button.data('title');									// data-title の値を取得
		var content = button.data('content');								// data-content の値を取得
		var courseid = button.data('courseid');								// data-courseid の値を取得
		var calendaruse = button.data('calendaruse');						// data-courseid の値を取得
		var color = button.data('color');									// data-color の値を取得

		// 色を選択したあと登録しないでダイアログを閉じた場合に色を元に戻せるように
		// 表示時の色を保持しておく
		noticeDialogLoadColor = color;

		// ボタンのdata-xxxから取得した値の反映
		var modal = $(this);												// モーダルを取得
		modal.find('.modal-body input#nid').val(nid);						// お知らせテーブルID
		modal.find('.modal-body input#title').val(title);					// タイトル
		content = $().decodeHtmlspecialchars(content);
		modal.find('.modal-body textarea#notice_content').val(content);			// 内容
		contentElement.summernote('code', content);
		modal.find('.modal-body select#course').val(courseid);				// 講座
		if (calendaruse == 1) {
			$('#calendar_use').prop('checked', true).change();
			$().setupCalendarUse();
			modal.find('.modal-body input#targetdt').val(targetdt);			// 日付
			// simplecolorpickerの色選択
			$('#color').simplecolorpicker('selectColor', color);
		} else {
			$('#calendar_use').prop('checked', false).change();
			$().setupCalendarUse();
			// simplecolorpickerをデフォルト状態に戻す。
			$('#color').simplecolorpicker('selectColor', colorpickerDefaultColor);
		}
	});

	// お知らせ編集ダイアログを閉じる（キャンセル）際の処理
	$('#new_post_modal').on('hidden.bs.modal', function () {
		// キャンセル時は色を最初の選択状態に戻す。
		$('#color').simplecolorpicker('selectColor', colorpickerDefaultColor);
		// キャンセル時編集内容を元に戻す。
		contentElement.summernote('reset');
		// エラーメッセージを消しておく
		$('.tooltip-validation').hide();
	});

	// 「削除」クリック
	$('#delete_post_modal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget)						//モーダルを呼び出すときに使われたボタンを取得
		var nid = button.data('nid')							//data-pid の値を取得
		var modal = $(this)										//モーダルを取得
		modal.find('.modal-body input#nid').val(nid.toString())	//inputタグにも表示
	});

	// お知らせ「登録」ボタンクリック
	$('#notice_entry').click(function() {
		if ($('#calendar_use').prop('checked')) {
			// 「日付」のバリデーションルール追加
			$('#targetdt').rules("add", {
				required: true,
				messages: {
					required: errMsgNoticeTargetdtRequired
				}
			});
			// 「カレンダー」のバリデーションルール追加
			$('#color').rules("add", {
				required: true,
				messages: {
					required: errMsgNoticeColorRequired
				}
			});
		} else {
			// 「日付」のバリデーションルール削除
			$('#targetdt').rules("remove");
			// 「カレンダー」のバリデーションルール削除
			$('#color').rules("remove");
		}
		if (contentElement.summernote('codeview.isActivated')) {
			contentElement.summernote('codeview.deactivate');
		}
		if (contentElement.summernote('isEmpty')) {
			contentElement.val('');
			contentElement.summernote('code', '');
		}
		// jquery-validationとsummernote併用時の不具合回避のため、
		// ここで「内容」のバリデーションルールを追加する
		// ※ここで入れないと、ポップアップの表示時に「内容」のバリデーションが実施されてしまい
		// 　「新しいお知らせ」の際に、最初から内容が入力されていない旨のメッセージが出てしまう。
		$('#notice_content').rules("add", {
			required: true,
			messages: {
				required: errMsgNoticeContentRequired
			}
		});
	});

	// 「新しいお知らせ」「編集」バリデーション
	// jquery-validationとsummernote併用時の不具合回避のため、
	// 戻り値を保持しておく
	var validateResult = $('#notice_edit_form').validate({
		rules: {
			title :{
				required: true,
				maxlength: noticeTitleMax
			},
			course :{
				required: true
			}
		},
		messages: {
			title :{
				required: errMsgNoticeTitleRequired,
				maxlength: noticeTitleMax + errMsgNoticeTitleMaxlength
			},
			course :{
				required: errMsgNoticeCourseRequired
			}
		},
		// jquery-validationとsummernote併用時の不具合回避
		ignore: ":hidden:not(#notice_content),.note-editable.panel-body"
	});

});

// 「カレンダーに表示する」設定
$.fn.setupCalendarUse = function() {
	if ($('#calendar_use').prop('checked')) {
		// 「YES」の場合
		// 「日付」、「カレンダー」を有効にする
		$('#calendar_use_items').show();
	} else {
		// 「NO」の場合
		// 「日付」、「カレンダー」を無効にする
		$('#calendar_use_items').hide();
	}
};

// お知らせ編集ダイアログ「内容」画像ファイルアップロード
$.fn.contentImgUpload = function(file, editor, welEditable) {
	data = new FormData();
	data.append("contentimg", file);
	$.ajax({
		data: data,
		type: "POST",
		url: pageurl + "/contentimg" + params,
		cache: false,
		contentType: false,
		processData: false,
		success: function(url) {
//			editor.insertImage(welEditable, url);
			$('#notice_content').summernote('editor.insertImage', url);
		},
		error: function(response){
			$('#error_notice_edit').html(response.responseText);
		},
		complete: function() {
		},
	});
};

// phpのhtmlspecialcharsでエンコードされた文字列をデコードする
$.fn.decodeHtmlspecialchars = function(str) {
	if (str == '' || str == undefined) {
		str = '';
		return str;
	}
	str = str.replace(/&amp;/g,'&');
	str = str.replace(/&quot;/g,'\"');
	str = str.replace(/&#039;/g,'\'');
	str = str.replace(/&lt;/g,'<');
	str = str.replace(/&gt;/g,'>');
	str = str.replace(/&#92;/g,'\\');
	return str;
};