<?php
/*
 * 今の設定を引き継いで新しいculture_common.phpを生成するスクリプト
 * 新しいculture_common.phpをconfig/developmentにコピーする
 * スクリプトを実行すると、culture_common_org.phpにリネームされ新しいculture_common.phpがstdoutに出力される。
 * 新しいculture_common.phpにリダイレクトして使う。
 
 * @author Kazuyuki Saito
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

copy('culture_common.php', 'culture_common_org.php');

$new = file_get_contents('development/culture_common.php');
$org = file_get_contents('culture_common_org.php');

// 対象: $config['...'] = ...;
preg_match_all('/\$config.*?;/s', $org, $result);

foreach ($result[0] as $line) {
	$item = explode(" = ", $line);
	// 正規表現の[をエスケープ
	$search = str_replace('[', '\\[', $item[0]);
	// $configの$をエスケープ
	$pattern = '/\\' . $search . '.*?;/s';
	$new = preg_replace($pattern, $line, $new);
}

file_put_contents('culture_common.php', $new);

