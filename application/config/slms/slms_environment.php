<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * sLMS環境依存設定ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

// 各種ファイル保存先ベースパス
$config['BASE_SAVE_PATH'] = '/var/www/mangop.net/html';

// ワークパス設定
// ※画像アップロード処理の際などにシステムが一時的に使用する
$config['WORK_PATH'] = '/var/tmp/mangop.net/work';

// 各種ファイルアップロード最大サイズ基準値（KB単位）
// php.iniの「upload_max_filesize」より大きな値を設定した場合、
// 最大値は「upload_max_filesize」のサイズになります。
// また、0 を指定しても最大値は「upload_max_filesize」のサイズが適用されます。
$config['BASE_UPLOAD_MAX_SIZE'] = 1000000000;

// URLサブディレクトリ
// ※システムの稼働する環境がURLのルートではなく、
// 　サブディレクトリの配下の場合、
// 　サブディレクトリを定義する。
// 　例：
// 　　http://example.com/hogehoge
// 　　の場合、
// 　　$config['URL_SUBDIR'] = 'hogehoge';
// 　　のように定義する。
// ※サブディレクトリ配下で運用する場合は、
// 　この定義の他にCodeIgniter側の各種設定も必要。
// ※ルートで運用する場合は空にする。
$config['URL_SUBDIR'] = '';

// ゲスト機能使用設定（使用しない：0／使用する：1）
$config['USE_GUEST'] = 0;

// 受講講座自己登録機能使用設定（使用しない：0／使用する：1）
$config['USE_SELF_REGIST_COURSE'] = 0;

// 自動ログイン用クッキーの有効期間（日数）
$config['AUTO_LOGIN_COKKIE_EXPIRE'] = 14;

// 教材再生時間判定値（％値）：SUCCESS
$config['MATERIAL_MOVIE_PLAYTIME_SUCCESS'] = 100;
// 教材再生時間判定値（％値）：WARNING
$config['MATERIAL_MOVIE_PLAYTIME_WARNING'] = 70;

// お知らせ一覧の新着アイコン表示対象期間（日数）
// ※0を設定した場合は新着アイコンは表示しない
$config['NOTICE_NEW_INDICATION_PERIOD'] = 7;

// ホーム画面（受講者）：カレンダー/イントロダクション表示設定
// （0:カレンダー固定／1:イントロダクション固定／9:カレンダー/イントロダクション切替）
$config['STUDENTSHOME_CAL_INTRO_DISPLAY_MODE'] = 1;
// ホーム画面（受講者）：カレンダー/イントロ切替時のデフォルト値
// （0:カレンダー／1:イントロダクション）
$config['STUDENTSHOME_CAL_INTRO_SWITCHMODE_DEFAULT'] = 0;
// ホーム画面（受講者）：カレンダー/イントロ表示時の「受講講座」の表示/非表示設定
// （0:「カレンダーの時表示する」／1:「イントロの時表示する」／2:「カレンダー/イントロの両方表示する」／3:「カレンダー/イントロの両方表示しない」）
$config['STUDENTSHOME_CAL_INTRO_DISPLAY_COURSE_LIST'] = 2;
// 【ゲスト用】ホーム画面（受講者）：カレンダー/イントロダクション表示設定
// （0:カレンダー固定／1:イントロダクション固定／9:カレンダー/イントロダクション切替）
$config['STUDENTSHOME_GUEST_CAL_INTRO_DISPLAY_MODE'] = 9;
// 【ゲスト用】ホーム画面（受講者）：カレンダー/イントロ切替時のデフォルト値
// （0:カレンダー／1:イントロダクション）
$config['STUDENTSHOME_GUEST_CAL_INTRO_SWITCHMODE_DEFAULT'] = 0;
// 【ゲスト用】ホーム画面（受講者）：カレンダー/イントロ表示時の「受講講座」の表示/非表示設定
// （0:「カレンダーの時表示する」／1:「イントロの時表示する」／2:「カレンダー/イントロの両方表示する」／3:「カレンダー/イントロの両方表示しない」）
$config['STUDENTSHOME_GUEST_CAL_INTRO_DISPLAY_COURSE_LIST'] = 0;

// 課題管理画面：ダウンロードCSVファイル接頭辞
$config['TASKMANAGER_CSV_DOWNLOAD_FILE_PREFIX'] = 'task_';

// フィードバック画面：ダウンロード画像ファイル接頭辞
$config['FEEDBACK_IMG_DOWNLOAD_FILE_PREFIX'] = 'feedback_';

// システム管理（ユーザー管理）画面：ユーザーデータダウンロードCSVファイル接頭辞
$config['USERMANAGE_USER_CSV_DOWNLOAD_FILE_PREFIX'] = 'user_';

// 各種消費量調査バッチ：集計対象Apacheログファイルパス
$config['BATCH_CHECK_CONSUMPTION_APACHE_LOG_PATH'] = '/var/log/httpd/mangop.net-access_log';

