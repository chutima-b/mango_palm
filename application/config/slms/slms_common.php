<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * sLMS共通設定ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

// カレンダー用カラーピッカー定義（jquery-simplecolorpicker）
// ※array('色'=>'色の表示名')
$config['SIMPLECOLORPICKER_OPTIONTAG_COLORS'] = array(
		'#7bd148' => 'Green',
		'#5484ed' => 'Bold blue',
		'#a4bdfc' => 'Blue',
		'#46d6db' => 'Turquoise',
		'#7ae7bf' => 'Light green',
		'#51b749' => 'Bold green',
		'#fbd75b' => 'Yellow',
		'#ffb878' => 'Orange',
		'#ff887c' => 'Red',
		'#dc2127' => 'Bold red',
		'#dbadff' => 'Purple',
		'#e1e1e1' => 'Gray',
);

// 背景用カラーピッカー定義（jquery-simplecolorpicker）
// ※array('色'=>'色の表示名')
$config['SIMPLECOLORPICKER_OPTIONTAG_COLORS_FOR_BACKGROUND'] = array(
		'#e1e1e1' => 'Gray',
		'#ffffff' => 'White',
		'#7bd148' => 'Green',
		'#5484ed' => 'Bold blue',
		'#a4bdfc' => 'Blue',
		'#46d6db' => 'Turquoise',
		'#7ae7bf' => 'Light green',
		'#51b749' => 'Bold green',
		'#fbd75b' => 'Yellow',
		'#ffb878' => 'Orange',
		'#ff887c' => 'Red',
		'#dc2127' => 'Bold red',
		'#dbadff' => 'Purple',
);

// 権限：ゲスト
$config['AUTH_GUEST'] = -9;
// 権限：どの講座にも未登録（テーブル上はどの講座にも登録は無いが、ログイン認証時に未登録だった場合に便宜上、返却される値）
$config['AUTH_UNREGISTERED'] = -1;
// 権限：受講者
$config['AUTH_STUDENT'] = 0;
// 権限：講師
$config['AUTH_TEACHER'] = 1;
// 権限：運用管理者
$config['AUTH_OP_MANAGER'] = 2;
// 権限：システム管理者
$config['AUTH_SYS_MANAGER'] = 9;

// 課題タイプ：0:単一選択
$config['TASK_TYPE_SINGLE_SELECTION'] = 0;
// 課題タイプ：1:複数選択
$config['TASK_TYPE_MULTIPLE_CHOICE'] = 1;
// 課題タイプ：2:テキスト
$config['TASK_TYPE_TEXT'] = 2;
// 課題タイプ：3:ファイル
$config['TASK_TYPE_FILE'] = 3;
// 課題タイプ：4:クイズ（単一選択）
$config['TASK_TYPE_QUIZ_SINGLE_SELECTION'] = 4;
// 課題タイプ：5:アンケート（単一選択）
$config['TASK_TYPE_SURVEY_SINGLE_SELECTION'] = 5;
// 課題タイプ：6:アンケート（複数選択）
$config['TASK_TYPE_SURVEY_MULTIPLE_CHOICE'] = 6;

// 教材タイプ：0:外部ファイル
$config['MATERIAL_TYPE_EXTERNAL'] = 0;
// 教材タイプ：1:動画ファイル
$config['MATERIAL_TYPE_MOVIE'] = 1;
// 教材タイプ：2:写真ファイル
$config['MATERIAL_TYPE_PICTURE'] = 2;
// 教材タイプ：3:PDFファイル
$config['MATERIAL_TYPE_PDF'] = 3;

// 課題を作成するフラグ：作成しない
$config['EVALUATION_USE_FLAG_OFF'] = 0;
// 課題を作成するフラグ：作成する
$config['EVALUATION_USE_FLAG_ON'] = 1;

// テーマ公開フラグ：非公開
$config['THEME_CLOSE'] = 0;
// テーマ公開フラグ：公開
$config['THEME_OPEN'] = 1;
// テーマ公開フラグ：公開終了
$config['THEME_PUBLIC_END'] = 9;

// フォーラム使用フラグ：未使用
$config['FORUM_USE_FLAG_OFF'] = 0;
// フォーラム使用フラグ：使用
$config['FORUM_USE_FLAG_ON'] = 1;

// 下書き保存フラグ：下書き
$config['DRAFT_FLAG_ON'] = 1;
// 下書き保存フラグ：下書き以外
$config['DRAFT_FLAG_OFF'] = 0;

// 姓名表記用定数定義：名
$config['NAME_STYLE_FIRSTNAME'] = 'firstname';
// 姓名表記用定数定義：姓
$config['NAME_STYLE_LASTNAME'] = 'lastname';

// テーブル判定キー：ユーザーテーブル
$config['TABLE_KEY_USER'] = 1;
// テーブル判定キー：お知らせテーブル
$config['TABLE_KEY_NOTICE'] = 2;
// テーブル判定キー：フォーラムテーブル
$config['TABLE_KEY_FORUM'] = 4;
// テーブル判定キー：チャットテーブル
$config['TABLE_KEY_CHAT'] = 5;
// テーブル判定キー：日記テーブル
$config['TABLE_KEY_DIARY'] = 6;
// テーブル判定キー：講座情報テーブル
$config['TABLE_KEY_COURSE'] = 8;
// テーブル判定キー：テーマテーブル
$config['TABLE_KEY_THEME'] = 9;
// テーブル判定キー：教材テーブル
$config['TABLE_KEY_MATERIALS'] = 10;
// テーブル判定キー：課題情報テーブル
$config['TABLE_KEY_TASK'] = 11;
// テーブル判定キー：課題解答情報テーブル
$config['TABLE_KEY_TASK_RESULT'] = 12;
// テーブル判定キー：ジャンルテーブル
$config['TABLE_KEY_GENRE'] = 15;
// テーブル判定キー：設定テーブル
$config['TABLE_KEY_SETTING'] = 16;

// コントローラ種別定義
// ※すべてのユーザーがアクセスできる画面→100番台
// ※講師以上のユーザーがアクセスできる画面→200番台
// ※システム管理者のみがアクセスできる画面→300番台
// ※ファイル表示用等の画面以外のコントローラ→800番台
// ※アクセスエラー画面等のシステムの機能とは直接関係ない画面→900番台
// コントローラ種別：ログイン画面
$config['CONTROLLER_KIND_LOGIN'] = 101;
// コントローラ種別：ホーム画面（受講者）
$config['CONTROLLER_KIND_STUDENTS_HOME'] = 102;
// コントローラ種別：講座画面
$config['CONTROLLER_KIND_COURSE'] = 103;
// コントローラ種別：テーマ画面
$config['CONTROLLER_KIND_THEME'] = 104;
// コントローラ種別：課題画面
$config['CONTROLLER_KIND_TASK'] = 105;
// コントローラ種別：フォーラム管理画面
$config['CONTROLLER_KIND_FORUM_MANAGE'] = 106;
// コントローラ種別：フォーラム画面
$config['CONTROLLER_KIND_FORUM'] = 107;
// コントローラ種別：チャット一覧画面
$config['CONTROLLER_KIND_CHAT_LIST'] = 108;
// コントローラ種別：チャット画面
$config['CONTROLLER_KIND_CHAT'] = 109;
// コントローラ種別：日記画面
$config['CONTROLLER_KIND_DIARY'] = 110;
// コントローラ種別：日記一覧画面
$config['CONTROLLER_KIND_DIARY_LIST'] = 111;
// コントローラ種別：プロフィール画面
$config['CONTROLLER_KIND_PROFILE'] = 112;
// コントローラ種別：お知らせ画面
$config['CONTROLLER_KIND_NOTICE'] = 113;
// コントローラ種別：動画再生画面
$config['CONTROLLER_KIND_MATERIALVIDEO'] = 114;
// コントローラ種別：講座申込画面（受講講座自己登録）
$config['CONTROLLER_KIND_SELF_REGIST_COURSE'] = 115;
// コントローラ種別：ホーム画面（管理者）
$config['CONTROLLER_KIND_STAFF_HOME'] = 201;
// コントローラ種別：講座編集画面
$config['CONTROLLER_KIND_COURSE_EDIT'] = 202;
// コントローラ種別：テーマ編集画面
$config['CONTROLLER_KIND_THEME_EDIT'] = 203;
// コントローラ種別：課題編集画面
$config['CONTROLLER_KIND_TASK_EDIT'] = 204;
// コントローラ種別：参加者登録画面
$config['CONTROLLER_KIND_STUDENTS_REGIST'] = 205;
// コントローラ種別：講師運用管理者登録画面
$config['CONTROLLER_KIND_STAFF_MANAGER'] = 206;
// コントローラ種別：受講者管理画面
$config['CONTROLLER_KIND_STUDENTSMANAGE'] = 207;
// コントローラ種別：課題管理画面
$config['CONTROLLER_KIND_TASKMANAGER'] = 208;
// コントローラ種別：フィードバック画面
$config['CONTROLLER_KIND_FEEDBACK'] = 209;
// コントローラ種別：チャット管理画面
$config['CONTROLLER_KIND_CHATMANAGER'] = 210;
// コントローラ種別：受講者課題画面
$config['CONTROLLER_KIND_TASKMANAGEUSERS'] = 211;
// コントローラ種別：システム管理（ユーザー管理）画面
$config['CONTROLLER_KIND_USERMANAGER'] = 301;
// コントローラ種別：システム管理（ジャンル）画面
$config['CONTROLLER_KIND_GENREMANAGER'] = 302;
// コントローラ種別：システム管理（設定）画面
$config['CONTROLLER_KIND_SETTINGS'] = 303;
// コントローラ種別：ファイル表示コントローラー
$config['CONTROLLER_KIND_DISPLAY_FILE'] = 801;
// コントローラ種別：アクセスエラー画面
$config['CONTROLLER_KIND_ACCESS_ERROR'] = 901;

// 画面アクセス判定定義
// 権限ごとに、アクセスを許可する画面のコントローラー名を記述する
$config['ACCESS_JUDGEMENT'] = array(
	// ゲスト
	$config['AUTH_GUEST'] => array(
		$config['CONTROLLER_KIND_COURSE'],
		$config['CONTROLLER_KIND_DIARY'],
		$config['CONTROLLER_KIND_FORUM'],
		$config['CONTROLLER_KIND_FORUM_MANAGE'],
		$config['CONTROLLER_KIND_NOTICE'],
		$config['CONTROLLER_KIND_STUDENTS_HOME'],
		$config['CONTROLLER_KIND_THEME'],
		$config['CONTROLLER_KIND_MATERIALVIDEO'],
		$config['CONTROLLER_KIND_DISPLAY_FILE'],
	),
	// どの講座にも未登録
	$config['AUTH_UNREGISTERED'] => array(
		$config['CONTROLLER_KIND_NOTICE'],
		$config['CONTROLLER_KIND_PROFILE'],
		$config['CONTROLLER_KIND_STUDENTS_HOME'],
		$config['CONTROLLER_KIND_MATERIALVIDEO'],
		$config['CONTROLLER_KIND_DISPLAY_FILE'],
		$config['CONTROLLER_KIND_SELF_REGIST_COURSE'],
	),
	// 受講者
	$config['AUTH_STUDENT'] => array(
		$config['CONTROLLER_KIND_CHAT'],
		$config['CONTROLLER_KIND_CHAT_LIST'],
		$config['CONTROLLER_KIND_COURSE'],
		$config['CONTROLLER_KIND_DIARY'],
		$config['CONTROLLER_KIND_DIARY_LIST'],
		$config['CONTROLLER_KIND_FORUM'],
		$config['CONTROLLER_KIND_FORUM_MANAGE'],
		$config['CONTROLLER_KIND_NOTICE'],
		$config['CONTROLLER_KIND_PROFILE'],
		$config['CONTROLLER_KIND_STUDENTS_HOME'],
		$config['CONTROLLER_KIND_TASK'],
		$config['CONTROLLER_KIND_THEME'],
		$config['CONTROLLER_KIND_MATERIALVIDEO'],
		$config['CONTROLLER_KIND_DISPLAY_FILE'],
		$config['CONTROLLER_KIND_SELF_REGIST_COURSE'],
	),
	// 講師
	$config['AUTH_TEACHER'] => array(
		$config['CONTROLLER_KIND_CHAT'],
		$config['CONTROLLER_KIND_CHAT_LIST'],
		$config['CONTROLLER_KIND_COURSE'],
		$config['CONTROLLER_KIND_COURSE_EDIT'],
		$config['CONTROLLER_KIND_DIARY'],
		$config['CONTROLLER_KIND_DIARY_LIST'],
		$config['CONTROLLER_KIND_FEEDBACK'],
		$config['CONTROLLER_KIND_FORUM'],
		$config['CONTROLLER_KIND_FORUM_MANAGE'],
		$config['CONTROLLER_KIND_NOTICE'],
		$config['CONTROLLER_KIND_PROFILE'],
		$config['CONTROLLER_KIND_STAFF_HOME'],
		$config['CONTROLLER_KIND_STAFF_MANAGER'],
		$config['CONTROLLER_KIND_STUDENTS_HOME'],
		$config['CONTROLLER_KIND_TASK'],
		$config['CONTROLLER_KIND_TASK_EDIT'],
		$config['CONTROLLER_KIND_TASKMANAGER'],
		$config['CONTROLLER_KIND_TASKMANAGEUSERS'],
		$config['CONTROLLER_KIND_THEME'],
		$config['CONTROLLER_KIND_THEME_EDIT'],
		$config['CONTROLLER_KIND_MATERIALVIDEO'],
		$config['CONTROLLER_KIND_DISPLAY_FILE'],
		$config['CONTROLLER_KIND_SELF_REGIST_COURSE'],
	),
	// 運用管理者
	$config['AUTH_OP_MANAGER'] => array(
		$config['CONTROLLER_KIND_CHAT'],
		$config['CONTROLLER_KIND_CHATMANAGER'],
		$config['CONTROLLER_KIND_CHAT_LIST'],
		$config['CONTROLLER_KIND_COURSE'],
		$config['CONTROLLER_KIND_COURSE_EDIT'],
		$config['CONTROLLER_KIND_DIARY'],
		$config['CONTROLLER_KIND_DIARY_LIST'],
		$config['CONTROLLER_KIND_FEEDBACK'],
		$config['CONTROLLER_KIND_FORUM'],
		$config['CONTROLLER_KIND_FORUM_MANAGE'],
		$config['CONTROLLER_KIND_NOTICE'],
		$config['CONTROLLER_KIND_PROFILE'],
		$config['CONTROLLER_KIND_STAFF_HOME'],
		$config['CONTROLLER_KIND_STAFF_MANAGER'],
		$config['CONTROLLER_KIND_STUDENTS_HOME'],
		$config['CONTROLLER_KIND_STUDENTSMANAGE'],
		$config['CONTROLLER_KIND_STUDENTS_REGIST'],
		$config['CONTROLLER_KIND_TASK'],
		$config['CONTROLLER_KIND_TASK_EDIT'],
		$config['CONTROLLER_KIND_TASKMANAGER'],
		$config['CONTROLLER_KIND_TASKMANAGEUSERS'],
		$config['CONTROLLER_KIND_THEME'],
		$config['CONTROLLER_KIND_THEME_EDIT'],
		$config['CONTROLLER_KIND_MATERIALVIDEO'],
		$config['CONTROLLER_KIND_DISPLAY_FILE'],
		$config['CONTROLLER_KIND_SELF_REGIST_COURSE'],
	)
);

// 講座画面アクセス判定定義
// 受講者情報テーブルに登録されているか判定を行う対象の画面コントローラーを定義
$config['COURSE_ACCESS_JUDGEMENT'] = array(
	$config['CONTROLLER_KIND_CHAT'],
	$config['CONTROLLER_KIND_CHATMANAGER'],
	$config['CONTROLLER_KIND_CHAT_LIST'],
	$config['CONTROLLER_KIND_COURSE'],
	$config['CONTROLLER_KIND_COURSE_EDIT'],
	$config['CONTROLLER_KIND_DIARY'],
	$config['CONTROLLER_KIND_DIARY_LIST'],
	$config['CONTROLLER_KIND_FEEDBACK'],
	$config['CONTROLLER_KIND_STAFF_MANAGER'],
	$config['CONTROLLER_KIND_STUDENTSMANAGE'],
	$config['CONTROLLER_KIND_STUDENTS_REGIST'],
	$config['CONTROLLER_KIND_FORUM'],
	$config['CONTROLLER_KIND_FORUM_MANAGE'],
	$config['CONTROLLER_KIND_TASK'],
	$config['CONTROLLER_KIND_TASK_EDIT'],
	$config['CONTROLLER_KIND_TASKMANAGER'],
	$config['CONTROLLER_KIND_TASKMANAGEUSERS'],
	$config['CONTROLLER_KIND_THEME'],
	$config['CONTROLLER_KIND_THEME_EDIT'],
	$config['CONTROLLER_KIND_MATERIALVIDEO'],
);

// 講座編集権限判定定義
// 該当講座の編集権限がある場合のみアクセスを許可する画面コントローラーを定義
$config['COURSE_EDIT_JUDGEMENT'] = array(
	$config['CONTROLLER_KIND_COURSE_EDIT'],
	$config['CONTROLLER_KIND_FEEDBACK'],
	$config['CONTROLLER_KIND_STAFF_MANAGER'],
	$config['CONTROLLER_KIND_STUDENTSMANAGE'],
	$config['CONTROLLER_KIND_STUDENTS_REGIST'],
	$config['CONTROLLER_KIND_TASK_EDIT'],
	$config['CONTROLLER_KIND_TASKMANAGER'],
	$config['CONTROLLER_KIND_TASKMANAGEUSERS'],
	$config['CONTROLLER_KIND_THEME_EDIT'],
);

// 画像処理ライブラリ：ワークディレクトリパス
$config['IMGLIB_WORK_PATH'] = $this->config['WORK_PATH'].'/imglib';

// 各種画像ファイル保存先パス
$config['IMAGES_SAVE_PATH'] = $this->config['BASE_SAVE_PATH'].'/images';

// ファイル表示コントローラー：最大使用メモリ（php.iniのmemory_limitの値を上書き）
$config['DISPLAYFILE_MEMORY_LIMIT'] = '2000M';

// プロフィール画面：ニックネーム最大文字数
$config['PROFILE_NICKNAME_MAXLENGTH'] = 128;
// プロフィール画面：フォーム項目パスワード最小文字数
$config['PROFILE_PASSWD_MINLENGTH'] = 6;
// プロフィール画面：フォーム項目パスワード最大文字数
$config['PROFILE_PASSWD_MAXLENGTH'] = 20;
// プロフィール画面：自己紹介最大文字数
$config['PROFILE_INTRODUCTION_MAXLENGTH'] = 500;
// プロフィール画面：デフォルト画像URL（base_urlからのパス）
$config['PROFILE_DEFAULT_IMAGE_URL'] = 'images/profile/no_image.gif';
// プロフィール画面：画像アップロード最大サイズ（KB単位）
$config['PROFILE_IMAGE_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// プロフィール画面：画像アップロード対象拡張子
$config['PROFILE_IMAGE_UPLOAD_EXTENSIONS'] = array('gif', 'png', 'jpg', 'jpeg');
// プロフィール画面：画像アップロードパス
$config['PROFILE_IMAGE_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/profile';
// プロフィール画面：画像保存先パス
$config['PROFILE_IMAGE_SAVE_PATH'] = $config['IMAGES_SAVE_PATH'].'/profile';
// プロフィール画面：画像長辺最大サイズ
$config['PROFILE_IMAGE_MAX_LENGTH'] = 64;

// 講座編集画面：講座紹介欄画像アップロード対象拡張子
$config['COURSEEDIT_DESCRIPT_IMAGE_UPLOAD_EXTENSIONS'] = array('gif', 'png', 'jpg', 'jpeg');
// 講座編集画面：講座紹介欄画像アップロード最大サイズ（KB単位）
$config['COURSEEDIT_DESCRIPT_IMAGE_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// 講座編集画面：講座紹介欄画像アップロードパス
$config['COURSEEDIT_DESCRIPT_IMAGE_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/courseedit';
// 講座編集画面：講座紹介欄画像保存先パス
$config['COURSEEDIT_DESCRIPT_IMAGE_SAVE_PATH'] = $config['IMAGES_SAVE_PATH'].'/courseedit/descript';
// 講座編集画面：講座紹介欄画像長辺最大サイズ
$config['COURSEEDIT_DESCRIPT_IMAGE_MAX_LENGTH'] = 1000;

// テーマ編集画面：テーマタイトル最大文字数
$config['THEMEEDIT_TITLE_MAXLENGTH'] = 256;
// テーマ編集画面：テーマ目的最大文字数
$config['THEMEEDIT_GOAL_MAXLENGTH'] = 500;
// テーマ編集画面：テーマ説明画像アップロード最大サイズ（KB単位）
$config['THEMEEDIT_DESCRIPTION_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// テーマ編集画面：テーマ説明画像アップロード対象拡張子
$config['THEMEEDIT_DESCRIPTION_IMAGE_UPLOAD_EXTENSIONS'] = array('gif', 'png', 'jpg', 'jpeg');
// テーマ編集画面：テーマ説明画像アップロードパス
$config['THEMEEDIT_DESCRIPTION_IMAGE_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/themeedit';
// テーマ編集画面：テーマ説明画像保存先パス
$config['THEMEEDIT_DESCRIPTION_IMAGE_SAVE_PATH'] = $config['IMAGES_SAVE_PATH'].'/themeedit/descript';
// テーマ編集画面：テーマ説明画像長辺最大サイズ
$config['THEMEEDIT_DESCRIPTION_IMAGE_MAX_LENGTH'] = 1000;
// テーマ編集画面：教材説明最大文字数
$config['THEMEEDIT_MATERIAL_DESCRIPTION_MAXLENGTH'] = 100;
// テーマ編集画面：教材リソース最大文字数
$config['THEMEEDIT_MATERIAL_RESOURCE_MAXLENGTH'] = 256;
// テーマ編集画面：教材アップロード最大サイズ（KB単位）
$config['THEMEEDIT_MATERIAL_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// テーマ編集画面：教材アップロード対象拡張子
$config['THEMEEDIT_MATERIAL_UPLOAD_EXTENSIONS'] = array('png', 'jpg', 'jpeg', 'mp4', 'pdf', 'mov');
// テーマ編集画面：JavaScript変数用教材アップロード対象＜＜動画ファイル＞＞拡張子※JavaScriptの文字列に展開するのでシングルクオートを含める
$config['THEMEEDIT_MATERIAL_UPLOAD_MOVIE_EXTENSIONS_FOR_JAVASCRIPT'] = array("'mp4'", "'mov'");
// テーマ編集画面：教材アップロードパス
$config['THEMEEDIT_MATERIAL_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/material';
// テーマ編集画面：教材保存先パス
$config['THEMEEDIT_MATERIAL_SAVE_PATH'] = $this->config['BASE_SAVE_PATH'].'/materials';
// テーマ編集画面：教材保存先URL（base_urlからのパス）
$config['THEMEEDIT_MATERIAL_SAVE_URL'] = 'materials';

// テーマ教材ファイル判別用拡張子リスト
$config['THEME_MATERIAL_EXTENSION_LIST'] = array('mp4', 'pdf', 'png', 'jpeg', 'jpg', 'mov');
// テーマ教材ファイルタイプリスト
$config['THEME_MATERIAL_TYPE_LIST'] = array(
		$config['MATERIAL_TYPE_MOVIE'],
		$config['MATERIAL_TYPE_PDF'],
		$config['MATERIAL_TYPE_PICTURE'],
		$config['MATERIAL_TYPE_PICTURE'],
		$config['MATERIAL_TYPE_PICTURE'],
		$config['MATERIAL_TYPE_MOVIE']
);

// 課題編集画面：課題名最大文字数
$config['TASKEDIT_NAME_MAXLENGTH'] = 256;
// 課題編集画面：課題説明最大文字数
$config['TASKEDIT_DESCRIPTION_MAXLENGTH'] = 500;
// 課題編集画面：解説最大文字数
$config['TASKEDIT_COMMENT_MAXLENGTH'] = 500;
// 課題編集画面：問題画像アップロード最大サイズ（KB単位）
$config['TASKEDIT_DESCRIPTION_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// 課題編集画面：問題画像アップロード対象拡張子
$config['TASKEDIT_DESCRIPTION_IMAGE_UPLOAD_EXTENSIONS'] = array('gif', 'png', 'jpg', 'jpeg');
// 課題編集画面：問題画像アップロードパス
$config['TASKEDIT_DESCRIPTION_IMAGE_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/taskedit';
// 課題編集画面：問題画像保存先パス
$config['TASKEDIT_DESCRIPTION_IMAGE_SAVE_PATH'] = $config['IMAGES_SAVE_PATH'].'/taskedit/descript';
// 課題編集画面：問題画像長辺最大サイズ
$config['TASKEDIT_DESCRIPTION_IMAGE_MAX_LENGTH'] = 1000;

// テーマ画面：教材保存先パス
$config['THEME_MATERIAL_SAVE_PATH'] = $this->config['BASE_SAVE_PATH'].'/materials';
// テーマ画面：教材保存先URL（base_urlからのパス）
$config['THEME_MATERIAL_SAVE_URL'] = 'materials';
// テーマ画面：解答画像最大幅（px）
$config['THEME_RESULT_IMG_MAX_WIDTH'] = '500';
// テーマ画面：日記最大文字数
// ※文字数のカウントは改行コードのCRとLFがそれぞれ1文字としてカウントされるので
// 　文中に、CR+LFの改行がある場合は、改行ごとに2文字カウントされる
$config['THEME_DIARY_LENGTH'] = 50;
// テーマ画面：日記最大表示件数
$config['THEME_DIARY_MAX'] = 10;
// テーマ画面：フォーラム最大文字数
// ※文字数のカウントは改行コードのCRとLFがそれぞれ1文字としてカウントされるので
// 　文中に、CR+LFの改行がある場合は、改行ごとに2文字カウントされる
$config['THEME_FORUM_LENGTH'] = 50;
// テーマ画面：フォーラム最大表示件数
$config['THEME_FORUM_MAX'] = 10;

// フォーラム管理画面：フォーラム名最大文字数
$config['FORUM_MANAGE_FORUM_NAME_MAXLENGTH'] = 256;

// フォーラム画面：デフォルト画像URL（base_urlからのパス）
$config['FORUM_DEFAULT_IMAGE_URL'] = $config['PROFILE_DEFAULT_IMAGE_URL'];
// フォーラム画面：投稿メッセージ最大文字数
$config['FORUM_POST_MESSAGE_MAXLENGTH'] = 500;
// フォーラム画面：画像アップロード最大サイズ（KB単位）
$config['FORUM_IMAGE_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// フォーラム画面：画像アップロード対象拡張子
$config['FORUM_IMAGE_UPLOAD_EXTENSIONS'] = array('gif', 'png', 'jpg', 'jpeg');
// フォーラム画面：画像アップロードパス
$config['FORUM_IMAGE_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/forum';
// フォーラム画面：画像保存先パス
$config['FORUM_IMAGE_SAVE_PATH'] = $config['IMAGES_SAVE_PATH'].'/forum';
// フォーラム画面：画像保存先URL（base_urlからのパス）
$config['FORUM_IMAGE_SAVE_URL'] = 'images/forum';
// フォーラム画面：画像長辺最大サイズ
$config['FORUM_IMAGE_MAX_LENGTH'] = 1000;
// フォーラム画面：表示件数
$config['FORUM_DISP_COUNT'] = 10;

// 課題画面：解答画像アップロード最大サイズ（KB単位）
$config['TASK_IMAGE_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// 課題画面：解答画像アップロード対象拡張子
$config['TASK_IMAGE_UPLOAD_EXTENSIONS'] = array('gif', 'png', 'jpg', 'jpeg');
// 課題画面：解答画像アップロードパス
$config['TASK_IMAGE_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/task';
// 課題画面：解答画像保存先パス
$config['TASK_IMAGE_SAVE_PATH'] = $config['IMAGES_SAVE_PATH'].'/task';
// 課題画面：解答画像長辺最大サイズ
$config['TASK_IMAGE_MAX_LENGTH'] = 1000;

// 日記画面：デフォルト画像URL（base_urlからのパス）
$config['DIARY_DEFAULT_IMAGE_URL'] = $config['PROFILE_DEFAULT_IMAGE_URL'];
// 日記画面：投稿メッセージ最大文字数
$config['DIARY_MESSAGE_MAXLENGTH'] = 500;
// 日記画面：画像アップロード最大サイズ（KB単位）
$config['DIARY_IMAGE_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// 日記画面：画像アップロード対象拡張子
$config['DIARY_IMAGE_UPLOAD_EXTENSIONS'] = array('gif', 'png', 'jpg', 'jpeg');
// 日記画面：画像アップロードパス
$config['DIARY_IMAGE_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/diary';
// 日記画面：画像保存先パス
$config['DIARY_IMAGE_SAVE_PATH'] = $config['IMAGES_SAVE_PATH'].'/diary';
// 日記画面：画像保存先URL（base_urlからのパス）
$config['DIARY_IMAGE_SAVE_URL'] = 'images/diary';
// 日記画面：画像長辺最大サイズ
$config['DIARY_IMAGE_MAX_LENGTH'] = 1000;
// 日記画面：表示件数
$config['DIARY_DISP_COUNT'] = 10;

// 日記一覧画面：デフォルト画像URL（base_urlからのパス）
$config['DIARYLIST_DEFAULT_IMAGE_URL'] = $config['PROFILE_DEFAULT_IMAGE_URL'];
// 日記一覧画面：権限（受講者）
$config['DIARYLIST_AUTH_STUDENT'] = 'メンバー';
// 日記一覧画面：権限（講師）
$config['DIARYLIST_AUTH_TEACHER'] = 'サポート';
// 日記一覧画面：権限（運用管理者）
$config['DIARYLIST_AUTH_OP_MANAGER'] = '運用管理者';
// 日記一覧画面：権限（システム管理者）
$config['DIARYLIST_AUTH_SYS_MANAGER'] = 'システム管理者';
// 日記一覧画面：表示件数
$config['DIARYLIST_DISP_COUNT'] = 10;

// チャット画面：リロードインターバル（ミリ秒）
$config['CHAT_RELOAD_INTERVAL'] = 2000;
// チャット画面：画像アップロード最大サイズ（KB単位）
$config['CHAT_IMAGE_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// チャット画面：画像アップロード対象拡張子
$config['CHAT_IMAGE_UPLOAD_EXTENSIONS'] = array('gif', 'png', 'jpg', 'jpeg');
// チャット画面：画像アップロードパス
$config['CHAT_IMAGE_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/chat';
// チャット画面：画像保存先パス
$config['CHAT_IMAGE_SAVE_PATH'] = $config['IMAGES_SAVE_PATH'].'/chat';
// チャット画面：画像長辺最大サイズ
$config['CHAT_IMAGE_MAX_LENGTH'] = 150;
// チャット画面：表示件数
$config['CHAT_DISP_COUNT'] = 20;

// チャット一覧画面：表示件数
$config['CHATLIST_DISP_COUNT'] = 20;

// お知らせ一覧の新着アイコン画像URL（base_urlからのパス）
$config['NOTICE_NEW_INDICATION_IMAGE_URL'] = 'images/notice/new.gif';

// ホーム画面（管理者）：お知らせタイトル最大文字数
$config['STAFFHOME_NOTICETITLE_MAXLENGTH'] = 100;
// ホーム画面（管理者）：お知らせ内容画像アップロード最大サイズ（KB単位）
$config['STAFFHOME_CONTENT_IMAGE_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// ホーム画面（管理者）：お知らせ内容画像アップロード対象拡張子
$config['STAFFHOME_CONTENT_IMAGE_UPLOAD_EXTENSIONS'] = array('gif', 'png', 'jpg', 'jpeg');
// ホーム画面（管理者）：お知らせ内容画像アップロードパス
$config['STAFFHOME_CONTENT_IMAGE_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/notice';
// ホーム画面（管理者）：お知らせ内容画像保存先パス
$config['STAFFHOME_CONTENT_IMAGE_SAVE_PATH'] = $config['IMAGES_SAVE_PATH'].'/notice/content';
// ホーム画面（管理者）：お知らせ内容画像長辺最大サイズ
$config['STAFFHOME_CONTENT_IMAGE_MAX_LENGTH'] = 1000;

// ホーム画面（受講者）：日記最大文字数
// ※文字数のカウントは改行コードのCRとLFがそれぞれ1文字としてカウントされるので
// 　文中に、CR+LFの改行がある場合は、改行ごとに2文字カウントされる
$config['STUDENTSHOME_DIARY_LENGTH'] = 50;
// ホーム画面（受講者）：日記最大表示件数
$config['STUDENTSHOME_DIARY_MAX'] = 10;
// ホーム画面（受講者）：フォーラム最大文字数
// ※文字数のカウントは改行コードのCRとLFがそれぞれ1文字としてカウントされるので
// 　文中に、CR+LFの改行がある場合は、改行ごとに2文字カウントされる
$config['STUDENTSHOME_FORUM_LENGTH'] = 50;
// ホーム画面（受講者）：フォーラム最大表示件数
$config['STUDENTSHOME_FORUM_MAX'] = 10;

// 受講者管理画面：デフォルト画像URL（base_urlからのパス）
$config['STUDENTS_MANAGER_DEFAULT_IMAGE_URL'] = $config['PROFILE_DEFAULT_IMAGE_URL'];
// 受講者管理画面：受講者リスト表示件数
$config['STUDENTS_MANAGER_DISP_COUNT'] = 10;

// 参加者登録画面：デフォルト画像URL（base_urlからのパス）
$config['STUDENTS_REGIST_DEFAULT_IMAGE_URL'] = $config['PROFILE_DEFAULT_IMAGE_URL'];
// 参加者登録画面：参加者リスト表示件数
$config['STUDENTS_REGIST_DISP_COUNT'] = 10;
// 参加者登録画面：検索入力ユーザーID最大文字数
$config['STUDENTS_REGIST_SEARCH_USERID_MAXLENGTH'] = 50;
// 参加者登録画面：検索入力姓最大文字数
$config['STUDENTS_REGIST_SEARCH_SURNAME_MAXLENGTH'] = 50;
// 参加者登録画面：検索入力名最大文字数
$config['STUDENTS_REGIST_SEARCH_FIRSTNAME_MAXLENGTH'] = 50;

// 受講者課題画面：ダウンロードCSVファイル保存先パス
$config['TASKMANAGEUSERS_CSV_DOWNLOAD_PATH'] = $this->config['WORK_PATH'].'/taskmanager';
// 受講者課題画面：ユーザー一覧表示件数
$config['TASKMANAGEUSERS_USERLIST_DISP_COUNT'] = 10;

// フィードバック画面：評価点最大文字数
$config['FEEDBACK_POINT_MAXLENGTH'] = 3;
// フィードバック画面：フィードバックメッセージ最大文字数
$config['FEEDBACK_MESSAGE_MAXLENGTH'] = $config['TASKEDIT_COMMENT_MAXLENGTH'];

// システム管理（ユーザー管理）画面：ユーザーリスト表示件数
$config['USERMANAGE_DISP_COUNT'] = 10;
// システム管理（ユーザー管理）画面：フォーム項目ユーザーID最大文字数
$config['USERMANAGE_USERID_MAXLENGTH'] = $config['STUDENTS_REGIST_SEARCH_USERID_MAXLENGTH'];
// システム管理（ユーザー管理）画面：フォーム項目パスワード最小文字数
$config['USERMANAGE_PASSWD_MINLENGTH'] = $config['PROFILE_PASSWD_MINLENGTH'];
// システム管理（ユーザー管理）画面：フォーム項目パスワード最大文字数
$config['USERMANAGE_PASSWD_MAXLENGTH'] = $config['PROFILE_PASSWD_MAXLENGTH'];
// システム管理（ユーザー管理）画面：フォーム項目姓最大文字数
$config['USERMANAGE_SURNAME_MAXLENGTH'] = $config['STUDENTS_REGIST_SEARCH_SURNAME_MAXLENGTH'];
// システム管理（ユーザー管理）画面：フォーム項目名最大文字数
$config['USERMANAGE_FIRSTNAME_MAXLENGTH'] = $config['STUDENTS_REGIST_SEARCH_FIRSTNAME_MAXLENGTH'];
// システム管理（ユーザー管理）画面：フォーム項目ニックネーム最大文字数
$config['USERMANAGE_NICKNAME_MAXLENGTH'] = $config['PROFILE_NICKNAME_MAXLENGTH'];
// システム管理（ユーザー管理）画面：ユーザー登録用CSVファイルアップロード最大サイズ（KB単位）
$config['USERMANAGE_USER_CSV_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// システム管理（ユーザー管理）画面：ユーザー登録用CSVファイルアップロードパス
$config['USERMANAGE_USER_CSV_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/usermanager';
// システム管理（ユーザー管理）画面：ユーザー登録用CSVファイルアップロード文字コード自動判別定義
$config['USERMANAGE_USER_CSV_UPLOAD_DETECT_ORDER'] = 'UTF-8,SJIS-win,EUC-JP,JIS,ASCII';
// システム管理（ユーザー管理）画面：ユーザーデータダウンロードCSVファイル保存先パス
$config['USERMANAGE_USER_CSV_DOWNLOAD_PATH'] = $this->config['WORK_PATH'].'/usermanager';
// システム管理（ユーザー管理）画面：講座名区切り文字
$config['USERMANAGE_COURSENAME_DELIMITER'] = "###DELIM###";

// システム管理（ジャンル）画面：ジャンルリスト表示件数
$config['GENREMANAGER_DISP_COUNT'] = 20;
// システム管理（ジャンル）画面：ジャンルID最大文字数
$config['GENREMANAGER_GENREID_MAXLENGTH'] = 10;
// システム管理（ジャンル）画面：ジャンル名最大文字数
$config['GENREMANAGER_GENRENAME_MAXLENGTH'] = 50;
// システム管理（ジャンル）画面：デフォルト画像
$config['GENREMANAGER_DEFAULT_IMAGE_FILE'] = 'noimage.png';
// システム管理（ジャンル）画面：デフォルト画像URL（base_urlからのパス）
$config['GENREMANAGER_DEFAULT_IMAGE_URL'] = 'images/genre/'.$config['GENREMANAGER_DEFAULT_IMAGE_FILE'];
// システム管理（ジャンル）画面：画像アップロード最大サイズ（KB単位）
$config['GENREMANAGER_IMAGE_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// システム管理（ジャンル）画面：画像アップロード対象拡張子
$config['GENREMANAGER_IMAGE_UPLOAD_EXTENSIONS'] = array('gif', 'png', 'jpg', 'jpeg');
// システム管理（ジャンル）画面：画像アップロードパス
$config['GENREMANAGER_IMAGE_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/genre';
// システム管理（ジャンル）画面：画像保存先パス
$config['GENREMANAGER_IMAGE_SAVE_PATH'] = $config['IMAGES_SAVE_PATH'].'/genre';
// システム管理（ジャンル）画面：画像長辺最大サイズ
$config['GENREMANAGER_IMAGE_MAX_LENGTH'] = 64;

// システム管理（設定）画面：タイトル最大文字数
$config['SETTING_TITLE_MAXLENGTH'] = 256;
// システム管理（設定）画面：ロゴ画像未指定時の画像ファイル（base_urlからのパス）
$config['SETTINGS_LOGO_NO_IMAGE_URL'] = 'images/settings/noimage.png';
// システム管理（設定）画面：ロゴ画像URL（base_urlからのパス）
$config['SETTINGS_LOGO_IMAGE_URL'] = 'images/settings/logo/';
// システム管理（設定）画面：ロゴ画像アップロード最大サイズ（KB単位）
$config['SETTINGS_LOGO_IMAGE_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// システム管理（設定）画面：ロゴ画像アップロード対象拡張子
$config['SETTINGS_LOGO_IMAGE_UPLOAD_EXTENSIONS'] = array('gif', 'png', 'jpg', 'jpeg');
// システム管理（設定）画面：ロゴ画像アップロードパス
$config['SETTINGS_LOGO_IMAGE_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/settings';
// システム管理（設定）画面：ロゴ画像保存先パス
$config['SETTINGS_LOGO_IMAGE_SAVE_PATH'] = $config['IMAGES_SAVE_PATH'].'/settings/logo';
// システム管理（設定）画面：ロゴ画像長辺最大サイズ
$config['SETTINGS_LOGO_IMAGE_MAX_LENGTH'] = 500;
// システム管理（設定）画面：イントロダクション画像アップロード最大サイズ（KB単位）
$config['SETTINGS_INTRODUCTION_IMAGE_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// システム管理（設定）画面：イントロダクション画像アップロード対象拡張子
$config['SETTINGS_INTRODUCTION_IMAGE_UPLOAD_EXTENSIONS'] = array('gif', 'png', 'jpg', 'jpeg');
// システム管理（設定）画面：イントロダクション画像アップロードパス
$config['SETTINGS_INTRODUCTION_IMAGE_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/settings';
// システム管理（設定）画面：イントロダクション画像保存先パス
$config['SETTINGS_INTRODUCTION_IMAGE_SAVE_PATH'] = $config['IMAGES_SAVE_PATH'].'/settings/introduction';
// システム管理（設定）画面：イントロダクション画像長辺最大サイズ
$config['SETTINGS_INTRODUCTION_IMAGE_MAX_LENGTH'] = 1000;
// システム管理（設定）画面：壁紙画像未指定時の画像ファイル（base_urlからのパス）
$config['SETTINGS_BACKGROUND_NO_IMAGE_URL'] = $config['SETTINGS_LOGO_NO_IMAGE_URL'];
// システム管理（設定）画面：壁紙画像URL（base_urlからのパス）
$config['SETTINGS_BACKGROUND_IMAGE_URL'] = 'images/settings/wallpaper/';
// システム管理（設定）画面：壁紙画像アップロード最大サイズ（KB単位）
$config['SETTINGS_BACKGROUND_IMAGE_UPLOAD_MAX_SIZE'] = $this->config['BASE_UPLOAD_MAX_SIZE'];
// システム管理（設定）画面：壁紙画像アップロード対象拡張子
$config['SETTINGS_BACKGROUND_IMAGE_UPLOAD_EXTENSIONS'] = array('gif', 'png', 'jpg', 'jpeg');
// システム管理（設定）画面：壁紙画像アップロードパス
$config['SETTINGS_BACKGROUND_IMAGE_UPLOAD_PATH'] = $this->config['WORK_PATH'].'/settings';
// システム管理（設定）画面：壁紙画像保存先パス
$config['SETTINGS_BACKGROUND_IMAGE_SAVE_PATH'] = $config['IMAGES_SAVE_PATH'].'/settings/wallpaper';
// システム管理（設定）画面：壁紙画像長辺最大サイズ
$config['SETTINGS_BACKGROUND_IMAGE_MAX_LENGTH'] = 5000;
