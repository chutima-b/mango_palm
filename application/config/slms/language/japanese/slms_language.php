<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * sLMS言語依存設定ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

// HTML 言語設定（<html lang="xx">）
$config['HTML_LANG'] = 'ja';

// カレンダー（FullCalendar） 言語設定
$config['FULLCALENDAR_LANG'] = 'ja';

// Datepicker（bootstrap-datepicker） 言語設定
$config['DATEPICKER_LANG'] = $config['FULLCALENDAR_LANG'];

// WYSIWYGエディッタ（summernote）言語指定
$config['SUMMERNOTE_LANG'] = 'ja-JP';
// WYSIWYGエディッタ（summernote）言語ファイル
// ※“html/libs/summernote/lang/”ディレクトリ内のファイル名を指定
$config['SUMMERNOTE_LANG_FILE'] = 'summernote-ja-JP.custom.js';

// 姓名表記定義
// lastname firstname または、firstname lastname
$config['NAME_STYLE'] = "lastname firstname";
