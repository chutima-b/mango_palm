<?php
/*
 * 教材動画再生画面コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Materialvideo extends EX_Controller {

	/** 講座テーブルID. */
	private $cid = 0;
	/** 教材テーブルID. */
	private $mid = 0;
	/** プレヴューフラグ. */
	private $is_preview = false;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_MATERIALVIDEO'));

		$this->mid = $this->input->get('mid', true);
		$this->cid = $this->input->get('cid', true);
		$_prev = $this->input->get('prev', true);
		if ($_prev == 1) {
			$this->is_preview = true;
		}

		$this->load->model("Materials_Model", 'materials_model', true);
		$this->load->model("Materials_Play_Model", 'materials_play_model', true);
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		// 教材情報取得
		$_material_info = $this->materials_model->getMaterialInfo($this->mid);
		if (is_null($_material_info)) {
			return;
		}

		// 課題有効フラグ取得
		$this->load->model("Theme_Model", 'theme_model', true);
		$_evaluation_use = $this->theme_model->getEvaluationUse($_material_info[0]['theme_id']);

		// 教材再生時間情報取得
		$_material_play_info = $this->materials_play_model->getInfoOfUser($this->mid);
		$_play_time = 0;
		$_is_watch_end = false;
		if (!is_null($_material_play_info)) {
			if (!is_null($_material_play_info->play_time)) {
				$_play_time = $_material_play_info->play_time;
			}
			if (!is_null($_material_play_info->percentage)
			 && $_material_play_info->percentage == 100) {
				$_is_watch_end = true;
			}
		}

		// 動画ソース
		$_src = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_MATERIALS')."&id=".$_material_info[0]['id']."&cid=".$this->cid;
		// mimeタイプ
		$this->load->helper('file');
		$_file_path = $this->config->item('THEME_MATERIAL_SAVE_PATH').$_material_info[0]['material_path'];
		$_mime = get_mime_by_extension($_file_path);
		// 教材説明
		$_title = $_material_info[0]['material_description'];

		$_is_students = false;
		if ($this->getCourseAuth() === $this->config->item('AUTH_STUDENT')) {
			$_is_students = true;
		}

		$_view_data['src'] = $_src;
		$_view_data['play_time'] = $_play_time;
		$_view_data['evaluation_use'] = $_evaluation_use;
		$_view_data['is_watch_end'] = $_is_watch_end;
		$_view_data['is_students'] = $_is_students;
		$_view_data['is_preview'] = $this->is_preview;
		$_view_data['mime'] = htmlentities($_mime, ENT_QUOTES, 'UTF-8');
		$_view_data['title'] = htmlentities($_title, ENT_QUOTES, 'UTF-8');
		$this->load->view('materialvideo', $_view_data); // ※動画再生画面は別ウィンドウなので共通ヘッダー、共通フッターは使用しない
	}

	/**
	 * 教材再生時間登録
	 */
	public function registtime()
	{
		$_play_time = $this->input->post('play_time', true);
		$_total_time = $this->input->post('total_time', true);
		if ($this->getCourseAuth() !== $this->config->item('AUTH_STUDENT')) {
			// 受講者以外は何もしない
			echo "Nothing is done.";
			exit;
		}

		if (is_null($this->mid)
		 || is_null($_play_time)
		 || is_null($_total_time)
		 || !is_numeric($_play_time)
		 || !is_numeric($_total_time)) {
			header("HTTP/1.0 400 Bad Request");
			echo "Bad Request";
			exit;
		}

		$_play_time = floor($_play_time);
		$_total_time = floor($_total_time);
		$_percentage = floor(($_play_time / $_total_time) * 100);

		// 教材情報取得
		$_material_info = $this->materials_model->getMaterialInfo($this->mid);
		if (is_null($_material_info)) {
			header("HTTP/1.0 500 Internal Server Error");
			echo "Material data is not found.";
			exit;
		}

		// 課題有効フラグ取得
		$this->load->model("Theme_Model", 'theme_model', true);
		$_evaluation_use = $this->theme_model->getEvaluationUse($_material_info[0]['theme_id']);
		if ($_evaluation_use == $this->config->item('EVALUATION_USE_FLAG_OFF')) {
			// 課題を作成しないテーマは何もしない
			echo "Nothing is done.";
			exit;
		}

		// 教材再生時間登録
		if (!$this->materials_play_model->regist(
				$_material_info[0]['course_id'],
				$_material_info[0]['theme_id'],
				$this->mid,
				$_play_time,
				$_total_time,
				$_percentage)) {
			header("HTTP/1.0 500 Internal Server Error");
			echo "Fail regist time.";
			exit;
		}

		header("HTTP/1.0 200 OK");
		header('Content-Type: application/json; charset=utf-8');
		$_json_data = array('play_time' => $_play_time, 'total_time' => $_total_time, 'percentage' => $_percentage);
		echo json_encode($_json_data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
		exit;
	}

}
