<?php
/*
 * ホーム画面(受講者用)コントローラー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class StudentsHome extends EX_Controller {
	/** 権限情報. */
	private $authority_info = null;
	/** カレンダー/イントロダクション表示設定. */
	private $cal_intro_display_mode = 0;
	/** カレンダー/イントロ切替時のデフォルト値. */
	private $cal_intro_switchmode_default = 0;
	/** カレンダー/イントロ表示時の「受講講座」の表示/非表示設定. */
	private $cal_intro_display_course_list = 0;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_STUDENTS_HOME')); // ホーム画面（受講者）
		$this->lang->load('students_home_lang');
		$this->setPageTitle($this->lang->line('students_home_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link href="{$_base_url}libs/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet">
<link href="{$_base_url}libs/fullcalendar/lib/cupertino/jquery-ui.min.css" rel="stylesheet">
<link href="{$_base_url}libs/fullcalendar/fullcalendar.min.css" rel="stylesheet">
<link href="{$_base_url}libs/fullcalendar/fullcalendar.print.min.css" rel="stylesheet" media="print">
<link href="{$_base_url}css/studentshome.css?v=1.0.5" rel="stylesheet">
<link href="{$_base_url}css/diary_like.css?v=1.0.0" rel="stylesheet">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}libs/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="{$_base_url}libs/fullcalendar/lib/moment.min.js"></script>
<script src="{$_base_url}libs/fullcalendar/fullcalendar.min.js"></script>
<script src="{$_base_url}libs/fullcalendar/locale-all.js"></script>
<script src="{$_base_url}js/studentshome.js?v=1.0.2"></script>
<script src="{$_base_url}js/diary_like.js?v=1.0.0"></script>
EOT;
		$this->addScriptFiles($_add_script);

		// ユーザー情報取得
		$this->authority_info = $this->session->userdata('authority');

		// カレンダー/イントロダクション表示設定
		$this->cal_intro_display_mode = $this->config->item('STUDENTSHOME_CAL_INTRO_DISPLAY_MODE');
		// カレンダー/イントロ切替時のデフォルト値
		$this->cal_intro_switchmode_default = $this->config->item('STUDENTSHOME_CAL_INTRO_SWITCHMODE_DEFAULT');
		// カレンダー/イントロ表示時の「受講講座」の表示/非表示設定
		$this->cal_intro_display_course_list = $this->config->item('STUDENTSHOME_CAL_INTRO_DISPLAY_COURSE_LIST');

		// ゲストの場合は
		// カレンダー/イントロダクション関連の定義を差替え
		if ($this->authority_info[0]->authority == $this->config->item('AUTH_GUEST')) {
			$this->cal_intro_display_mode = $this->config->item('STUDENTSHOME_GUEST_CAL_INTRO_DISPLAY_MODE');
			// カレンダー/イントロ切替時のデフォルト値
			$this->cal_intro_switchmode_default = $this->config->item('STUDENTSHOME_GUEST_CAL_INTRO_SWITCHMODE_DEFAULT');
			// カレンダー/イントロ表示時の「受講講座」の表示/非表示設定
			$this->cal_intro_display_course_list = $this->config->item('STUDENTSHOME_GUEST_CAL_INTRO_DISPLAY_COURSE_LIST');
		}
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		// お知らせ情報取得
		$this->load->model('Notice_Model', 'notice_model', true);
		$_notice = $this->notice_model->getNoticeList();

		// 講座情報取得
		$this->load->model('Course_Model', 'course_model', true);
		$_course = $this->course_model->getStudentsHomeCourseList();

		// イントロダクション情報取得
		$this->load->model('Setting_Model', 'setting_model', true);
		$_introduction = $this->setting_model->get_introduction();

		// 日記投稿情報取得
		$this->load->model('Diary_Model', 'diary_model', true);
		$_diary = $this->diary_model->getDiaryListForStudentshome();

		// フォーラム情報取得
		$this->load->model('Forum_Model', 'forum_model', true);
		$_forum = $this->forum_model->getForumListForStudentshome();

		// 出力情報設定
		$param['uid'] = $this->authority_info[0]->id;
		$param['notice_data'] = $_notice;
		$param['course_data'] = $_course;
		$param['introduction_data'] = $_introduction;
		$param['cal_intro_display_mode'] = $this->cal_intro_display_mode;
		$param['cal_intro_switchmode_default'] = $this->cal_intro_switchmode_default;
		$param['cal_intro_display_course_list'] = $this->cal_intro_display_course_list;
		$param['diary_data'] = $_diary;
		$param['forum_data'] = $_forum;
		$param['script'] = $this->createJavaScript();

		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('students_home', $param); // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * スケジュール
	 *
	 * カレンダー表示用スケジュールデータをJSON形式で出力
	 */
	public function schedule()
	{
		$_schedule = $this->input->post('schedule', true);
		if (!$_schedule || $_schedule != 1) {
			die("error");
		}

		$_authority = $this->session->userdata['authority'];
		$this->load->model('Schedule_Model', 'schedule_model', true);
		$_json = $this->schedule_model->getScheduleByJSON($_authority[0]->id);

		header("Content-Type: application/json; charset=utf-8");
		echo $_json;
		exit;
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		$this->lang->load('diary_lang');

		$_script = <<<EOT
<script>
	var fullCalendarLang = "{$this->config->item('FULLCALENDAR_LANG')}";
	var scheduleUrl = "{$this->commonlib->baseUrl()}studentshome/schedule";
	var noEventsMsg = "{$this->lang->line('students_home_noeventmsg')}";
	var calIntroSwitchCal = "{$this->lang->line('students_home_cal_intro_switch_cal')}";
	var calIntroSwitchIntro = "{$this->lang->line('students_home_cal_intro_switch_intro')}";
	var calIntroDisplayMode = {$this->cal_intro_display_mode};
	var calIntroDisplayCourseList = {$this->cal_intro_display_course_list};
	var diaryAddUrl = "{$this->commonlib->baseUrl()}diary/like_add?cid=";
	var diaryDelUrl = "{$this->commonlib->baseUrl()}diary/like_del?cid=";
	var diaryLikeTooltipAdd = "{$this->lang->line('diary_link_like_add')}";
	var diaryLikeTooltipDel = "{$this->lang->line('diary_link_like_del')}";
</script>
EOT;
		return $_script;
	}

}
