<?php
/*
 * チャット一覧画面コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Chatlist extends EX_Controller {
	/** 講座ID. */
	private $cid = 0;
	/** 現在のページ. */
	private $page = 0;
	/** 講座権限. */
	private $course_auth = 0;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_CHAT_LIST')); // チャット一覧画面
		$this->lang->load('chat_list_lang');
		$this->setPageTitle($this->lang->line('chat_list_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css =<<<EOT
<link rel="stylesheet" href="{$_base_url}css/slms_loading.css?v=1.0.0">
<link rel="stylesheet" href="{$_base_url}css/chatlist.css?v=1.0.1">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}js/slms_loading.js?v=1.0.0"></script>
<script src="{$_base_url}js/chatlist.js?v=1.0.1"></script>
EOT;
		$this->addScriptFiles($_add_script);

		// 講座ID取得
		$this->cid = $this->input->get('cid', true);
		// この講座での権限取得
		$this->load->model("Students_Model", 'students_model', true);
		$_authority_info = $this->session->userdata('authority');
		$this->course_auth = $this->students_model->getCourseAuth($this->cid, $_authority_info[0]->id);

		// 現在のページパラメータ取得
		$this->page = $this->input->get_post('page', true);
		if (is_null($this->page) || !is_numeric($this->page) || $this->page < 1) {
			$this->page = 1;
		}
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		$_view_data = array();

		// 講座名取得
		$this->load->model("Course_Model", 'course_model', true);
		$_course_name = $this->course_model->getCourseName($this->cid);
		$_view_data['course_name'] = $_course_name;

		// チャットユーザー一覧取得
		$this->load->model("Chatlist_Model", 'chatlist_model', true);
		$_authority_info = $this->session->userdata('authority');
		$_chatlist_disp_count = $this->config->item('CHATLIST_DISP_COUNT');
		$_chat_users = $this->chatlist_model->getChatUserList($this->page, $_chatlist_disp_count, $this->cid, $_authority_info[0]->id);
		if ($this->page > 1 && is_null($_chat_users)) {
			$this->page--;
			$_chat_users = $this->chatlist_model->getChatUserList($this->page, $_chatlist_disp_count, $this->cid, $_authority_info[0]->id);
		}
		$_before_page = null;
		$_next_page = null;
		if (!is_null($_chat_users)) {
			$_profile_default_image = $this->commonlib->baseUrl().$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
			$this->lang->load('chat_list_lang');
			foreach ($_chat_users as $user) {
				// 写真URL設定
				if (is_null($user->picture_file)) {
					$user->picture_file = $_profile_default_image;
				} else {
					$user->picture_file = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=".$user->uid;
				}
				// 権限文字列設定
				switch ($user->authority) {
				case $this->config->item('AUTH_STUDENT'):
					$user->authority = $this->lang->line('chat_list_auth_student');
					break;
				case $this->config->item('AUTH_TEACHER'):
					$user->authority = $this->lang->line('chat_list_auth_teacher');
					break;
				case $this->config->item('AUTH_OP_MANAGER'):
					$user->authority = $this->lang->line('chat_list_auth_op_manager');
					break;
				case $this->config->item('AUTH_SYS_MANAGER'):
					$user->authority = $this->lang->line('chat_list_auth_sys_manager');
					break;
				}
			}
			// ユーザー一覧表示開始数（件目）
			$_start_cnt = ($this->page - 1) * $_chatlist_disp_count + 1;
			if (count($_chat_users) == 0) {
				$_start_cnt = 0;
			}

			$_base_page_url = $this->commonlib->baseUrl().'chatlist?cid='.$this->cid;

			// 前ページURL
			if ($_start_cnt > 1) {
				$_before_page = $_base_page_url.'&page='.($this->page - 1);

			}

			// チャット一覧総件数取得
			$_chatlist_count = $this->chatlist_model->getTotalCount($this->cid, $_authority_info[0]->id);

			// 全ページ数算出
			$_total_pages = ceil($_chatlist_count / $_chatlist_disp_count);

			// 次ページURL
			if ($this->page < $_total_pages) {
				$_next_page = $_base_page_url.'&page='.($this->page + 1);
			}
		}
		$_view_data['chat_users'] = $_chat_users;

		// 戻る画面設定
		if (isset($_SERVER['HTTP_REFERER'])) {
			$this->setBackUrl($_SERVER['HTTP_REFERER']);
		}
		$_view_data['cid'] = $this->cid;
		$_view_data['before_page'] = $_before_page;
		$_view_data['next_page'] = $_next_page;
		$_view_data['script'] = $this->createJavaScript($_before_page, $_next_page);
		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('chat_list', $_view_data); // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript($before_page, $next_page)
	{
		// 前ページURL
		$_befor_page_url = '';
		if (!is_null($before_page)) {
			$_befor_page_url = $before_page;
		}
		// 次ページURL
		$_next_page_url = '';
		if (!is_null($next_page)) {
			$_next_page_url = $next_page;
		}
		// 管理者フラグ
		$_is_manager = 'false';
		if ($this->course_auth >= $this->config->item('AUTH_OP_MANAGER')) {
			// 運用管理者以上は管理者用の画面を表示させるため
			// 管理者フラグを立てる
			$_is_manager = 'true';
		}

		$_script = <<<EOT
<script>
	var beforePageUrl = "{$_befor_page_url}";
	var nextPageUrl = "{$_next_page_url}";
	var isManager = {$_is_manager};
</script>
EOT;
		return $_script;
	}

}
