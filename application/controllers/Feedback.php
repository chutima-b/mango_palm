<?php
/*
 * フィードバック画面コントローラー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends EX_Controller {

	/** 講座ID. */
	private $cid = 0;
	/** テーマID. */
	private $thid = 0;
	/** ユーザーテーブルID. */
	private $uid = 0;
	/** 受講者管理画面の戻り先ページ. */
	private $tmpage = 0;
	/** 評価点最大文字数. */
	private $point_max = null;
	/** フィードバックメッセージ最大文字数. */
	private $feedback_message_max = null;
	/** テーマ情報. */
	private $theme_data = null;
	/** 課題リスト. */
	private $task_list = null;
	/** 処理完了メッセージ. */
	private $complete_message = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_FEEDBACK')); // フィードバック画面
		$this->lang->load('feedback_lang');
		$this->setPageTitle($this->lang->line('feedback_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link href="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.css?v=1.0.0" rel="stylesheet">
<link rel="stylesheet" href="{$_base_url}css/feedback.css?v=1.0.0">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}libs/jqueryvalidation/jquery.validate.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.js?v=1.0.0"></script>
<script src="{$_base_url}js/feedback.js?v=1.0.1"></script>
EOT;
		$this->addScriptFiles($_add_script);

		// テーブルモデル読み込み
		$this->load->model('Theme_Model', 'theme', TRUE);
		$this->load->model('Task_Model', 'task', TRUE);
		$this->load->model('Task_Result_Model', 'task_result', TRUE);
		$this->load->model('Task_Feedback_Model', 'task_feedback', TRUE);

		// パラメータ取得
		$this->cid = $this->input->get_post('cid', true);
		$this->thid = $this->input->get_post('thid', true);
		$this->uid = $this->input->get_post('uid', true);
		$this->tmpage = $this->input->get_post('tmpage', true);
		if (is_null($this->tmpage) || !is_numeric($this->tmpage) || $this->tmpage < 1) {
			$this->tmpage = 1;
		}

		// 評価点最大文字数
		$this->point_max = $this->config->item('FEEDBACK_POINT_MAXLENGTH');
		// フィードバックメッセージ最大文字数
		$this->feedback_message_max = $this->config->item('FEEDBACK_MESSAGE_MAXLENGTH');

		// テーマ情報取得
		$this->theme_data = $this->theme->getThemeInfo($this->thid);

		// 課題リスト取得
		$this->task_list = $this->task->getTaskList($this->cid, $this->thid);
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * 解答データ生成
	 *
	 * @param int $cid
	 * @param object $task_data
	 * @param array $result_data
	 * @return array 解答データ
	 */
	private function makeResultDispData($cid, $task_data, $result_data)
	{

		if ($task_data->task_type == $this->config->item('TASK_TYPE_SINGLE_SELECTION')
		 || $task_data->task_type == $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')
		 || $task_data->task_type == $this->config->item('TASK_TYPE_SURVEY_SINGLE_SELECTION')
		) {

			$choiceArray = array();
			$choices = $this->commonlib->nl2Array($task_data->choices);
			$cnt = 1;
			$is_correct_quiz = false;
			foreach ($choices as $ch) :
				$choice['item'] = htmlspecialchars($ch);
				if (!is_null($result_data['result_radio'])
				 && htmlspecialchars($result_data['result_radio']) == htmlspecialchars($ch)) {
					// 解答した課題の値と、課題の項目名が一致した場合
					// その項目を選択状態にする。
					$choice['select'] = 1;
				} else {
					$choice['select'] = 0;
				}
				if ($task_data->task_type == $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')) {
					// クイズの単一選択の場合
					if ($task_data->quiz_answer == $cnt) {
						// クイズの正解値とカウンター値が同じ場合は正答項目としてフラグを立てる。
						$choice['correct_item'] = true;
						if ($choice['select'] == 1) {
							// さらに、受講者の解答項目と一致した場合は正解フラグを立てる。
							$is_correct_quiz = true;
						}
					} else {
						// クイズの正解値とカウンター値が違う場合は正当項目ではない。
						$choice['correct_item'] = false;
					}
				}
				array_push($choiceArray, $choice);
				$cnt++;
			endforeach ;
			if ($task_data->task_type == $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')) {
				$result_data['is_correct_quiz'] = $is_correct_quiz;
			}
			$result_data += array('radio_array' => $choiceArray);

		} elseif ($task_data->task_type == $this->config->item('TASK_TYPE_MULTIPLE_CHOICE')
				|| $task_data->task_type == $this->config->item('TASK_TYPE_SURVEY_MULTIPLE_CHOICE')) {

			$choiceArray = array();
			$choices = $this->commonlib->nl2Array($task_data->choices);
			foreach ($choices as $ch) :
				$choice['item'] = htmlspecialchars($ch);
				$choice['select'] = 0;
				if (!is_null($result_data['result_check'])) {
					$items = explode("\n", $result_data['result_check']);
					foreach ($items as $item) {
						if (htmlspecialchars($ch) == htmlspecialchars($item)) {
							$choice['select'] = 1;
						}
					}
				}
				array_push($choiceArray, $choice);
			endforeach ;
			$result_data += array('check_array' => $choiceArray);

		} elseif ($task_data->task_type == $this->config->item('TASK_TYPE_TEXT')) {

		} elseif ($task_data->task_type == $this->config->item('TASK_TYPE_FILE')) {
			// 画像ソース
			if (!is_null($result_data['result_file'])) {
				$result_data['result_file'] = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_TASK_RESULT')."&id=".$result_data['id']."&cid=".$cid;

				// ダウンロードファイル名生成
				$this->load->model("Students_Model", 'students_model', true);
				$_authority_info = $this->session->userdata('authority');
				$_course_authority = $this->students_model->getCourseAuth($cid, $_authority_info[0]->id);
				$_result_file = $this->task_result->getFilePath($result_data['id'], $cid, $_course_authority);
				$_ext = pathinfo($_result_file, PATHINFO_EXTENSION);
				$result_data['result_file_name'] = $this->config->item('FEEDBACK_IMG_DOWNLOAD_FILE_PREFIX').$this->commonlib->getTimeWithMillSecond().'.'.$_ext;
			}
		}

		return $result_data;
	}

	/**
	 * 下書き・フィードバック更新処理
	 */
	public function update()
	{
		$draft_flg = $this->input->post('draft_flg', true);
		foreach ($this->task_list as $_task) {
			$points = $this->input->post('points_'.$_task->id, true);
			$feedback = $this->input->post('feedback_'.$_task->id, true);

			if ($draft_flg == $this->config->item('DRAFT_FLAG_OFF') || $points != '') {
				if ($this->theme_data[0]['evaluation_use'] == $this->config->item('EVALUATION_USE_FLAG_ON')) {
					$this->form_validation->set_rules('points_'.$_task->id, '['.$_task->task_name.'] - '.$this->lang->line('feedback_input_points')
							, 'trim|required|integer|max_length['.$this->feedback_message_max.']|less_than['.($_task->evaluation_points+1).']|greater_than[-1]');
				}
			}
			$this->form_validation->set_rules('feedback_'.$_task->id, '['.$_task->task_name.'] - '.$this->lang->line('feedback_input_feedback'), 'trim|max_length['.$this->feedback_message_max.']');

			if ($this->form_validation->run()) {

				if ($points === "") {
					$points = null;
				}

				if ($draft_flg == $this->config->item('DRAFT_FLAG_ON')) {
					$this->task_result->draftUpdate($_task->id, $this->uid, $points, $feedback, $this->config->item('DRAFT_FLAG_ON'));
				} else {
					$this->task_result->feedbackUpdate($_task->id, $this->uid, $points, $feedback, $this->config->item('DRAFT_FLAG_OFF'));
				}
			} else {
				$this->displayView();
				return;
			}
		}
		$this->task_feedback->update($this->cid, $this->thid, $this->uid, $draft_flg);
		$_cmp_msg = '';
		if ($draft_flg == $this->config->item('DRAFT_FLAG_OFF')) {
			$_cmp_msg = '&cmp=1';
		}
		redirect($this->commonlib->baseUrl().'feedback?cid='.$this->cid.'&thid='.$this->thid.'&uid='.$this->uid.'&tmpage='.$this->tmpage.$_cmp_msg, 'location');
	}

	/**
	 * 教材情報成型
	 *
	 * @param 教材情報リスト $material_list
	 * @return NULL|unknown
	 */
	private function makeMaterialDispData($material_list)
	{
		if (is_null($material_list)) {
			return null;
		}
		foreach ($material_list as $_material_data) {
			if ($_material_data->material_type == $this->config->item('MATERIAL_TYPE_MOVIE')) {
				$_material_data->link_path = $this->commonlib->baseUrl()."materialvideo?mid=".$_material_data->id."&cid=".$this->cid."&prev=1";
			} else if ($_material_data->material_type != $this->config->item('MATERIAL_TYPE_EXTERNAL')) {
				$_material_data->link_path = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_MATERIALS')."&id=".$_material_data->id."&cid=".$this->cid;
			} else {
				$_material_data->link_path = $_material_data->external_material_path;
			}
		}
		return $material_list;
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		$this->load->model('User_Model', 'user', TRUE);

		// ユーザー情報取得
		$_user = $this->user->getData($this->uid);

		// 教材情報取得
		$this->load->model('Materials_Model', 'materials_model', true);
		$_materials_list = $this->materials_model->getMaterialsListWithMaterialsPlay($this->cid, $this->thid, $this->uid);
		// 教材情報成型
		$_materials_list = $this->makeMaterialDispData($_materials_list);

		// 課題情報成型
		if (!is_null($this->task_list)) {
			foreach ($this->task_list as $_task) {
				// 課題回答情報取得
				$task_result_data = $this->task_result->getTaskResult($_task->id, $this->uid)[0];
				if (!is_null($task_result_data)) {
					// 解答データ生成
					$task_result_data = $this->makeResultDispData($this->cid, $_task, $task_result_data);
				}
				$_task->result_data = $task_result_data;
			}
		}

		// 課題フィードバック情報取得
		$_task_feedback = $this->task_feedback->getData($this->cid, $this->thid, $this->uid);

		// 表示データ設定
		$param['cid'] = $this->cid;
		$param['thid'] = $this->thid;
		$param['uid'] = $this->uid;
		$param['tmpage'] = $this->tmpage;
		$param['theme'] = $this->theme_data[0];
		$param['user'] = $_user;
		$param['material_list'] = $_materials_list;
		$param['task_list'] = $this->task_list;
		$param['task_feedback'] = $_task_feedback;
		$param['script'] = $this->createJavaScript();
		if ($this->input->get('cmp', true)) {
			$this->complete_message = $this->lang->line('feedback_complete_msg');
		}
		$param['complete_msg'] = $this->complete_message;

		// 戻る画面設定
		$this->setBackUrl($this->commonlib->baseUrl()."taskmanageusers?cid=".$this->cid."&thid=".$this->thid."&page=".$this->tmpage);

		$this->load->view('common_header');		// 共通ヘッダー部
		$this->load->view('feedback', $param);	// コンテンツ部
		$this->load->view('common_footer');// 共通フッター部
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		$_script = <<<EOT
<script>
	var pointMax = {$this->point_max};
	var feedbackMessageMax = {$this->feedback_message_max};
	var errMsgFeedbackPointMaxLength = "{$this->lang->line('feedback_script_err_msg_point_maxlength')}";
	var errMsgFeedbackMessageMaxlength = "{$this->lang->line('feedback_script_err_msg_feedbackmessage_maxlength')}";
	var errMsgFeedbackpointsRequired = "{$this->lang->line('feedback_script_err_msg_feedbackpoints_required')}";
	var errMsgFeedbackPointMaxValue1 = "{$this->lang->line('feedback_script_err_msg_feedbackpoints_maxvalue_1')}";
	var errMsgFeedbackPointMaxValue2 = "{$this->lang->line('feedback_script_err_msg_feedbackpoints_maxvalue_2')}";
	var errMsgFeedbackPointMinValue = "{$this->lang->line('feedback_script_err_msg_feedbackpoints_minvalue')}";
</script>
EOT;
		return $_script;
	}

}
