<?php
/*
 * 各種消費量を調査するバッチクラス
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Batch_check_consumption extends CI_Controller {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('log'); // バッチ用ログ出力を行うため、ヘルパークラスをロード

		if (!$this->input->is_cli_request()) {
			// コマンドラインからのリクエスト以外は受け付けない
			$this->output->set_status_header(403, 'Forbidden');
			echo "403 Forbidden".PHP_EOL;
			exit;
		}
	}

	/**
	 * 全てのコマンドを実行
	 */
	public function execute_all()
	{
		batch_info('start execute_all', true);

		// ディスク使用量
		$this->disk_amount();
		// ネットワーク転送量
		$this->network_amount();

		batch_info('end execute_all', true);
	}

	/**
	 * ディスク使用量
	 */
	public function disk_amount()
	{
		batch_info('start disk_amount', true);

		// 画像ファイルディスク使用量
		$_images_disk_amount = $this->execute_command_disk_amount($this->config->item('IMAGES_SAVE_PATH'));
		if ($_images_disk_amount === false) {
			echo "Failed in a check of the disk amount of consumption of image files.\n";
		} else {
			echo "disk amount of image files : ".$_images_disk_amount."\n";
		}

		// 教材ファイルディスク使用量
		$_materials_disk_amount = $this->execute_command_disk_amount($this->config->item('THEMEEDIT_MATERIAL_SAVE_PATH'));
		if ($_materials_disk_amount === false) {
			echo "Failed in a check of the disk amount of consumption of material files.\n";
		} else {
			echo "disk amount of material files : ".$_materials_disk_amount."\n";
		}

		// データベース使用量
		$this->load->model("Batch_Check_Consumption_Model", 'batch_check_consumption_model', true);
		$_db_amount = $this->batch_check_consumption_model->getConsumptionOfDatabase();
		if (is_null($_db_amount)) {
			echo "Failed in a check of the disk amount of consumption of database.\n";
		} else {
			echo "disk amount of database : ".$_db_amount[0]->MB."M\n";
		}

		batch_info('end disk_amount', true);
	}

	/**
	 * ファイルシステムディスク使用量取得
	 *
	 * @param string $path 対象ディレクトリパス
	 * @return boolean|string ファイルシステムディスク使用量
	 */
	private function execute_command_disk_amount($path)
	{
		// “2>&1”を付加して標準エラー（STDERR）を標準出力（STDOUT）にリダイレクトしておき、
		// execコマンドのパラメータでエラーメッセージを取得できるようにしておく。
		// 「253M      /var/www/dev_slms/html/images」のように出力されるので、
		// 使用量の部分だけを抜き出す。
		$_command = 'du -h -s '.$path.' 2>&1 | awk \'{print $1;}\'';
		return $this->execute_command($_command);
	}

	/**
	 * ネットワーク転送量
	 */
	public function network_amount()
	{
		batch_info('start network_amount', true);

		// “2>&1”を付加して標準エラー（STDERR）を標準出力（STDOUT）にリダイレクトしておき、
		// execコマンドのパラメータでエラーメッセージを取得できるようにしておく。
		// 指定したログファイルのネットワーク転送量を集計するコマンドを発行
		// HTTPステータスコードが200番台のものを集計し、MB単位に変換する
		$_command = 'cat '.$this->config->item('BATCH_CHECK_CONSUMPTION_APACHE_LOG_PATH').' | awk \'{if($9 ~ /2[0-9][0-9]/){sum+=$10}}END{print sum/1024/1024}\'';
		$_network_amount = $this->execute_command($_command);
		if ($_network_amount === false) {
			echo "Failed in a check of the disk amount of consumption of material files.\n";
		} else {
			echo "network amount : ".$_network_amount."M\n";
		}

		batch_info('end network_amount', true);
	}

	/**
	 * コマンド実行
	 *
	 * ※パラメータに指定する、実行コマンドには、
	 * 　“2>&1”を含めて、標準エラー（STDERR）を標準出力（STDOUT）にリダイレクトしておき、
	 * 　execコマンドのパラメータでエラーメッセージを取得できるようにしておく。
	 *
	 * @param string $command 実行コマンド
	 * @return boolean|string コマンド実行結果
	 */
	private function execute_command($command)
	{
		$_output = array();
		$_exec_ret = null;
		exec($command, $_output, $_exec_ret);
		if ($_exec_ret == 0) {
			if (!is_null($_output)) {
				return $_output[0];
			}
		} else {
			batch_error('Fail command:'.$command);
			if (!is_null($_output)) {
				foreach ($_output as $_line) {
					batch_error($_line);
				}
			}
			return false;
		}
		return '';
	}

}
