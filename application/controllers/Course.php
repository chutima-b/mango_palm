<?php
/*
 * 講座画面コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends EX_Controller {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_COURSE')); // 講座画面
		$this->lang->load('course_lang');
		$this->setPageTitle($this->lang->line('course_header_pagetitle'));

		$this->load->model("Course_Model", 'course_model', true);
		$this->load->model("Theme_Model", 'theme_model', true);
		$this->load->model("Profile_Model", 'profile_model', true);
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		// ビュー表示
		$this->displayView();
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		// 講座情報取得
		$_course_data = $this->course_model->getCourse($this->input->get('cid', TRUE));
		$_view_data['course'] = $_course_data[0];

		// テーマ情報取得
		$_authority_info = $this->session->userdata('authority');
		$_theme_data = $this->theme_model->getThemeListOfCourse($_course_data[0]->id, $_authority_info[0]->id);
		// テーマ情報が取得できた場合
		if (!is_null($_theme_data) && count($_theme_data) > 0) {
			foreach ($_theme_data as $_theme_tmp) {
				// 提出済みフラグ設定
				if ($_theme_tmp->is_submit == 1) {
					$_theme_tmp->is_submit = true;
				} else {
					$_theme_tmp->is_submit = false;
				}
				// フィードバック済みフラグ設定
				if ($_theme_tmp->is_feedback == 1) {
					$_theme_tmp->is_feedback = true;
				} else {
					$_theme_tmp->is_feedback = false;
				}
				// 受講者確認済みフラグ設定
				if ($_theme_tmp->is_confirmed == 1) {
					$_theme_tmp->is_confirmed = true;
				} else {
					$_theme_tmp->is_confirmed = false;
				}
				// 期限切れフラグ設定
				$_today = date("Y/m/d");
				$_theme_tmp->is_over_timelimit = false;
				if (!is_null($_theme_tmp->time_limit) && strtotime($_theme_tmp->time_limit) < strtotime($_today)) {
					$_theme_tmp->is_over_timelimit = true;
				}
			}
			// ビュー表示用データに設定
			$_view_data['theme'] = $_theme_data;
		}

		// チャット利用フラグ取得
		$_profile = $this->profile_model->get_profile($_authority_info[0]->id);
		$_view_data['chat_use'] = $_profile[0]->chat_use;

		// 講座ID
		$_view_data['cid'] = $this->input->get('cid', TRUE);

		// 講座権限
		$_view_data['course_auth'] = $this->getCourseAuth();

		// 戻る画面設定
		$this->setBackUrl($this->commonlib->baseUrl()."studentshome");

		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('course', $_view_data); // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

}
