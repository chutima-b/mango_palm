<?php
/*
 * 講座申込画面（受講講座自己登録）コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Selfregistcourse extends EX_Controller {
	/** エラーメッセージ. */
	private $error_message = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_SELF_REGIST_COURSE')); // 講座申込画面（受講講座自己登録）
		$this->lang->load('selfregistcourse_lang');
		$this->setPageTitle($this->lang->line('selfregistcourse_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_script =<<<EOT
<script src="{$_base_url}js/selfregistcourse.js?v=1.0.0"></script>
EOT;
		$this->addScriptFiles($_add_script);
		$this->setBackUrl($this->homeUrl());
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * 講座登録
	 */
	public function regist()
	{
		$_cid = $this->input->post('cid', true);

		$this->load->model('Students_Model', 'students_model', true);

		$_authority = $this->session->userdata('authority');
		if ($this->students_model->insert($_cid, $_authority[0]->id, $this->config->item('AUTH_STUDENT'))) {
			// 登録成功した場合
			// セッション情報更新
			$this->load->model("Authority_Model", 'authority_model', true);
			$_authority = $this->authority_model->get_authority();
			$this->session->set_userdata("authority", $_authority);
			// ホーム画面に遷移
			redirect($this->commonlib->baseUrl().'studentshome');
			exit;
		}
		// エラーの場合は画面遷移しない
		$this->error_message = $this->lang->line('selfregistcourse_err_msg_regist');
		$this->displayView();
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		$_authority = $this->session->userdata('authority');
		$_auth = $_authority[0]->authority;

		$this->load->model('Course_Model', 'course_model', true);

		// 講座件数取得
		$_course_count = $this->course_model->getCourseCountOfSelfRegistCourse();

		// ジャンル別講座件数取得
		$_genre_course_count = $this->course_model->getGenreCourseCountOfSelfRegistCourse();

		// 講座情報取得
		$_course_data = $this->course_model->getCourseListOfSelfRegistCourse();

		// 情報設定
		$_view_data['auth'] = $_auth;
		$_view_data['course_count'] = $_course_count;
		$_view_data['genre_course_count'] = $_genre_course_count;
		$_view_data['course_data'] = $_course_data;
		$_view_data['error_msg'] = $this->error_message;

		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('selfregistcourse', $_view_data); // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

}
