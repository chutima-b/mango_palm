<?php
/*
 * フォーラム画面コントローラー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends EX_Controller {

	/** 講座情報テーブルID. */
	private $cid = null;
	/** テーマテーブルID. */
	private $thid = null;
	/** フォーラム管理テーブルID. */
	private $fid = null;
	/** 戻り先パラメータ. */
	private $pre = null;
	/** 現在のページ. */
	private $page = 0;
	/** 投稿メッセージ最大文字数. */
	private $postmsg_max = null;
	/** 権限情報. */
	private $authority_info = null;
	/** エラーメッセージ. */
	private $error_message = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_FORUM')); // フォーラム画面
		$this->lang->load('forum_lang');
		$this->setPageTitle($this->lang->line('forum_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link href="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.css?v=1.0.0" rel="stylesheet">
<link rel="stylesheet" href="{$_base_url}css/slms_loading.css?v=1.0.0">
<link rel="stylesheet" href="{$_base_url}css/forum.css?v=1.0.0">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}libs/jqueryvalidation/jquery.validate.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.js?v=1.0.0"></script>
<script src="{$_base_url}js/slms_loading.js?v=1.0.0"></script>
<script src="{$_base_url}js/forum.js?v=1.0.2"></script>
EOT;
		$this->addScriptFiles($_add_script);

		// 投稿メッセージ最大文字数
		$this->postmsg_max = $this->config->item('FORUM_POST_MESSAGE_MAXLENGTH');
		// ユーザー情報取得
		$this->authority_info = $this->session->userdata('authority');

		// パラメータ取得
		$this->cid = $this->input->get_post('cid', true);
		$this->thid = $this->input->get_post('thid', true);
		$this->fid = $this->input->get_post('fid', true);
		$this->pre = $this->input->get_post('pre', true);
		$this->page = $this->input->get_post('page', true);
		if (is_null($this->page) || !is_numeric($this->page) || $this->page < 1) {
			$this->page = 1;
		}
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		$authority = $this->session->userdata['authority'];
		$id = $authority[0]->id;

		// 戻る画面の設定
		if ($this->pre == "home") {
			$this->setBackUrl($this->commonlib->baseUrl()."studentshome");
		} else if ($this->pre == "themetop") {
			$this->setBackUrl($this->commonlib->baseUrl()."theme?cid=".$this->cid."&thid=".$this->thid);
		} else {
			$this->setBackUrl($this->commonlib->baseUrl()."forummanage?cid=".$this->cid."?pre=".$this->pre."&thid=".$this->thid);
		}

		// 受講者モデル
		$this->load->model('Students_Model', 'students', TRUE);

		// 権限取得
		$auth = $this->students->getCourseAuth($this->cid, $id);

		// 講座モデル
		$this->load->model('Course_Model', 'course', TRUE);

		// 講座名取得
		$course_data = $this->course->getCourse($this->cid);

		// フォーラム管理モデル
		$this->load->model('Forum_Manage_Model', 'forummanage', TRUE);

		// フォーラム名取得
		$forummanage_data = $this->forummanage->getForumInfo($this->fid);

		// フォーラムモデル
		$this->load->model('Forum_Model', 'forum', TRUE);

		$_forum_disp_count = $this->config->item('FORUM_DISP_COUNT');
		$forum_data = $this->forum->getForumList($this->page, $_forum_disp_count, $this->fid);
		//		var_dump($forum_data);
		if ($this->page > 1 && is_null($forum_data)) {
			$this->page--;
			$forum_data = $this->forum->getForumList($this->page, $_forum_disp_count, $this->fid);
		}

		// ユーザー一覧表示開始数（件目）
		$_start_cnt = ($this->page - 1) * $_forum_disp_count + 1;
		if (count($forum_data) == 0) {
			$_start_cnt = 0;
		}

		$_base_page_url = $this->commonlib->baseUrl().'forum?cid='.$this->cid.'&fid='.$this->fid.'&pre='.$this->pre;
		if (!is_null($this->thid)) {
			$_base_page_url .= '&thid='.$this->thid;
		}

		// 前ページURL
		$_before_page = null;
		if ($_start_cnt > 1) {
			$_before_page = $_base_page_url.'&page='.($this->page - 1);

		}

		// フォーラム総投稿数取得
		$_forum_count = $this->forum->getTotalCount($this->fid);

		// 全ページ数算出
		$_total_pages = ceil($_forum_count / $_forum_disp_count);

		// 次ページURL
		$_next_page = null;
		if ($this->page < $_total_pages) {
			$_next_page = $_base_page_url.'&page='.($this->page + 1);
		}

		$param['auth'] = $auth;
		$param['course_data'] = $course_data;
		$param['forummanage_data'] = $forummanage_data;
		$param['forum_data'] = $this->formatForumData($forum_data);
		$param['before_page'] = $_before_page;
		$param['next_page'] = $_next_page;
		$param['pre'] = $this->pre;
		$param['thid'] = $this->thid;
		$param['fid'] = $this->fid;
		$param['page'] = $this->page;
		$param['script'] = $this->createJavaScript($_before_page, $_next_page);
		$param['error_msg'] = $this->error_message;

		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('forum', $param);  // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * フォーラムデータ加工
	 *
	 * @param array $forum_data フォーラムデータ
	 * @return array フォーラムデータ:
	 */
	private function formatForumData($forum_data) {

		$userid = $this->authority_info[0]->userid;

		$retdatas = array();

		foreach ($forum_data as $f) {

			if ($f['picture_file'] == null) {
				$f['picture_file'] = $this->commonlib->baseUrl().$this->config->item('FORUM_DEFAULT_IMAGE_URL');
			} else {
				$f['picture_file'] = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=".$f['uid'];
			}

			if ($f['image_file'] != null and $f['image_file'] != "") {
				$f['image_file'] = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_FORUM')."&id=".$f['id'];
			}

			if ($f['userid'] == $userid) {
				$f += array('my_post' => 1);
			} else {
				$f += array('my_post' => 0);
			}

			array_push($retdatas, $f);
		}

		return $retdatas;
	}

	/**
	 * 投稿削除
	 */
	public function delete()
	{

		$pid = $this->input->post('pid', true);

		// フォーラムモデル
		$this->load->model('Forum_Model', 'forum', TRUE);

		$this->forum->deletePost($pid);

		$path = $this->commonlib->baseUrl().'forum?cid='.$this->cid."&pre=".$this->pre."&page=".$this->page;
		if ($this->input->get_post('thid', true)) {
			$path .= '&thid='.$this->thid;
		}
		$path .= '&fid='.$this->fid;
//		redirect($path, 'refresh');
		redirect($path, 'location');
	}

	/**
	 * フォーラム変更
	 */
	public function edit()
	{

		$pid = $this->input->post('pid', true);
		$postmsg = $this->input->post('postmsg', true);

		$this->form_validation->set_rules('postmsg', $this->lang->line('forum_button_title_post'), 'trim|required|max_length['.$this->postmsg_max.']');

		if ($this->form_validation->run()) {

			// 画像がアップロードされたか判定
			$_save_file = null;
			if (is_uploaded_file($_FILES["upload_imagefile"]["tmp_name"])) {
				// 画像アップロード処理
				$_save_file = $this->image_upload();
				if (!is_null($this->error_message)) {
					$this->displayView();
					return;
				}
			}

			// フォーラムモデル
			$this->load->model('Forum_Model', 'forum', TRUE);

			// 新規投稿
			if ($pid == null or $pid == "") {
				$this->forum->registPost($this->fid, $postmsg, $_save_file);
			} else {
				// 画像ファイルがnullの場合
				if (is_null($_save_file)) {
					// 更新前の情報取得
					$_db_post = $this->forum->getPostInfo($pid);
					if (!empty($_db_post[0]->image_file)) {
						// 更新前に画像が既に登録されている場合はそのまま登録し直す
						$_save_file = $_db_post[0]->image_file;
					}
				}

				$this->forum->editPost($pid, $postmsg, $_save_file);
			}

			if (!is_null($this->error_message)) {
				$this->displayView();
				return;
			} else {
				$path = $this->commonlib->baseUrl().'forum?cid='.$this->cid."&pre=".$this->pre."&page=".$this->page;
				if ($this->input->get_post('thid', true)) {
					$path .= '&thid='.$this->thid;
				}
				$path .= '&fid='.$this->fid;

//				redirect($path, 'refresh');
				redirect($path, 'location');
			}
		} else {
			$this->displayView();
		}

	}

	/**
	 * 画像ファイルアップロード
	 *
	 * @return string|NULL アップロード画像ファイルパス
	 */
	private function image_upload() {
		// 画像アップロードパス取得
		$_upload_path = $this->config->item('FORUM_IMAGE_UPLOAD_PATH');
		if (!file_exists($_upload_path)) {
			mkdir($_upload_path, 0777, true);
		}
		// 画像ライブラリロード
		$_upload_config = array(
				'max_size' => $this->config->item('FORUM_IMAGE_UPLOAD_MAX_SIZE'),
				'encrypt_name' => true,
				'upload_path' => $_upload_path,
				'allowed_types' => implode("|", $this->config->item('FORUM_IMAGE_UPLOAD_EXTENSIONS'))
		);
		$this->load->library('upload', $_upload_config);

		// 画像アップロード
		if ($this->upload->do_upload('upload_imagefile')) {
			$file_info = $this->upload->data();
			// 保存先パス＋講座ID＋フォーラムID
			$_save_path = $this->config->item('FORUM_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid.DIRECTORY_SEPARATOR.$this->fid;
			if (!file_exists($_save_path)) {
				mkdir($_save_path, 0777, true);
			}
			// 保存先パス＋[ユーザーテーブルID]_yyyyMMddHHmmssSSS＋拡張子
			$_save_file_path = $_save_path.DIRECTORY_SEPARATOR.$this->authority_info[0]->id.'_'.$this->commonlib->getTimeWithMillSecond().$file_info['file_ext'];

			// リサイズ後の画像サイズ算出
			// 画像処理ライブラリロード
			$this->load->library('imagelib');
			$_resize_size = $this->imagelib->calResizeSize($file_info['full_path'], $this->config->item('FORUM_IMAGE_MAX_LENGTH'));

			// 画像リサイズ処理
			if (!is_null($_resize_size)) {
				$_image_config = array(
						'source_image' => $file_info['full_path'],
						'new_image' => $_save_file_path,
						'width' => $_resize_size[0],
						'height' => $_resize_size[1]
				);
				// CodeIgniterの画像処理ライブラリロード
				$this->load->library('image_lib', $_image_config);
				// リサイズ実行
				$this->image_lib->resize();
				// リサイズ元のアップロード画像削除
				unlink($file_info['full_path']);
			} else {
				// リサイズしなかった場合はファイルを保存先に移動
				rename($file_info['full_path'], $_save_file_path);
			}

			return $this->cid.'/'.$this->fid.'/'.basename($_save_file_path);
		} else {
			// 画像アップロードエラー
			// エラーメッセージ生成
			$this->error_message = $this->upload->display_errors();
		}
		return null;
	}

	/**
	 * JavaScriptデータ生成
	 *
	 * @param string $before_page 前ページURL
	 * @param string $next_page 次ページURL
	 */
	private function createJavaScript($before_page, $next_page)
	{
		$_befor_page_url = '';
		if (!is_null($before_page)) {
			$_befor_page_url = $before_page;
		}
		$_next_page_url = '';
		if (!is_null($next_page)) {
			$_next_page_url = $next_page;
		}

		$_script = <<<EOT
<script>
	var beforePageUrl = "{$_befor_page_url}";
	var nextPageUrl = "{$_next_page_url}";
	var postmsgMax = {$this->postmsg_max};
	var errMsgPostMsgRequired = "{$this->lang->line('forum_script_err_msg_postmsg_required')}";
	var errMsgPostMsgMaxlength = "{$this->lang->line('forum_script_err_msg_postmsg_maxlength')}";
</script>
EOT;
		return $_script;
	}

}
