<?php
/*
 * チャット管理画面コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Chatmanager extends EX_Controller {

	/** 講座テーブルID. */
	private $cid = 0;
	/** ユーザーテーブルID. */
	private $uid = 0;
	/** チャット相手のユーザーテーブルID. */
	private $tkid = 0;
	/** エラーメッセージ. */
	private $error_message = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_CHATMANAGER')); // チャット管理画面

		$this->lang->load('chatmanager_lang');
		$this->load->model("Profile_Model", 'profile_model', true);
		$this->load->model("Chat_Model", 'chat_model', true);
		// パラメータ取得
		$this->cid = $this->input->get_post('cid', true);
		$this->tkid = $this->input->get_post('tkid', true);
		$this->uid = $this->input->get_post('uid', true);
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		// チャット相手プロフィール取得
		$_tk_profile = $this->profile_model->get_profile($this->tkid);
		$_view_data['tk_profile'] = $_tk_profile[0];

		// チャットユーザーリスト
		$_user_list = $this->chat_model->getUserList($this->cid, $this->tkid);
		$_view_data['user_list'] = $_user_list;

		// チャット初期データ取得
		$_chat_data = $this->chat_model->getChatDataList($this->cid, $this->uid, $this->tkid);
		$_view_data['chat_data'] = $_chat_data;

		$_view_data['cid'] = $this->cid;
		$_view_data['tkid'] = $this->tkid;
		$_view_data['script'] = $this->createJavaScript();

		$this->load->view('chatmanager', $_view_data); // ※チャット画面はポップアップなので共通ヘッダー、共通フッターは使用しない
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		$_script = <<<EOT
<script>
	var dialogErrorTitle = "{$this->lang->line('chatmanager_dialog_error_title')}";
	var chatReloadInterval = "{$this->config->item('CHAT_RELOAD_INTERVAL')}";
</script>
EOT;
		return $_script;
	}

	/**
	 * チャットデータ取得
	 */
	public function getdata()
	{
		$_last_id = $this->input->get('last_id', true);
		$_chat_data = $this->chat_model->getChatDataList($this->cid, $this->uid, $this->tkid, false, $_last_id, true);
		$_json_data = null;
		if (!is_null($_chat_data)) {
			$_json_data = array();
			$_json_data['chat_data'] = $_chat_data;
			$_json_data['last_id'] = $_chat_data[count($_chat_data)-1]['id'];
		}
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($_json_data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
		exit;
	}

	/**
	 * チャット過去データ取得
	 */
	public function getpastdata()
	{
		$_first_id = $this->input->get('first_id', true);
		$_chat_data = $this->chat_model->getPastChatDataList($this->cid, $this->uid, $this->tkid, $_first_id);
		$_json_data = null;
		if (!is_null($_chat_data)) {
			$_json_data = array();
			$_json_data['chat_data'] = $_chat_data;
			$_json_data['first_id'] = $_chat_data[0]['id'];
		}
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($_json_data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
		exit;
	}

}
