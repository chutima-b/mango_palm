<?php
/*
 * 講座編集画面コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class CourseEdit extends EX_Controller {
	/** ビュー画面へ渡すデータ. */
	private $view_data = null;
	/** cidパラメータ. */
	private $cid = 0;
	/** エラーメッセージ. */
	private $error_message = null;
	/** 処理完了メッセージ. */
	private $complete_message = null;
	/** 最初のテーマ日付け. */
	private $first_theme_date = null;
	/** 最後のテーマ期限. */
	private $last_theme_limit = null;
	/** 「講座の期間を設定する」変更不可フラグ. */
	private $disable_use_schedule = false;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_COURSE_EDIT')); // 講座編集画面
		$this->lang->load('courseedit_lang');
		$this->setPageTitle($this->lang->line('courseedit_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link href="{$_base_url}libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="{$_base_url}libs/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet">
<link href="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.css?v=1.0.0" rel="stylesheet">
<link href="{$_base_url}libs/summernote/summernote.css" rel="stylesheet">
<link href="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker.css" rel="stylesheet">
<link href="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker-regularfont.css" rel="stylesheet">
<link href="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker-glyphicons.css" rel="stylesheet">
<link href="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker-fontawesome.css" rel="stylesheet">
<link rel="stylesheet" href="{$_base_url}css/slms_loading.css?v=1.0.0">
<link rel="stylesheet" href="{$_base_url}css/courseedit.css?v=1.0.3">
EOT;
		$this->addCssFiles($_add_css);
		$_summernote_lang_file = $this->config->item('SUMMERNOTE_LANG_FILE');
		$_summernote_lang_script = "";
		if (!empty($_summernote_lang_file)) {
			$_summernote_lang_script = "<script src=\"{$_base_url}libs/summernote/lang/{$this->config->item('SUMMERNOTE_LANG_FILE')}\"></script>";
		}
		$_add_script =<<<EOT
<script src="{$_base_url}libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="{$_base_url}libs/bootstrap-datepicker/locales/bootstrap-datepicker.ja.min.js"></script>
<script src="{$_base_url}libs/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="{$_base_url}libs/summernote/summernote.custom.js"></script>
{$_summernote_lang_script}
<script src="{$_base_url}libs/jqueryvalidation/jquery.validate.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.js?v=1.0.0"></script>
<script src="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker.js"></script>
<script src="{$_base_url}js/slms_loading.js?v=1.0.0"></script>
<script src="{$_base_url}js/courseedit.js?v=1.0.5"></script>
EOT;
		$this->addScriptFiles($_add_script);

		$this->view_data = array();
		$this->cid = $this->input->get('cid', true);

		$this->load->model("Course_Model", 'course_model', true);
		$this->load->model("Genre_Model", 'genre_model', true);
		$this->load->model("Theme_Model", 'theme_model', true);
		$this->load->model("Students_Model", 'students_model', true);

		// 最初のテーマ日付け取得
		$this->first_theme_date = $this->theme_model->getFirstDate($this->cid);
		// 最後のテーマ期限取得
		$this->last_theme_limit = $this->theme_model->getLastLimit($this->cid);
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * 講座コピー
	 */
	public function copy()
	{
		$this->form_validation->set_rules('courseedit_copy_courceid', $this->lang->line('courseedit_dialog_copy_courseid'), 'trim|required|max_length[50]|callback_check_has_course_code');
		$this->form_validation->set_rules('courseedit_copy_courcename', $this->lang->line('courseedit_dialog_copy_coursename'), 'trim|required|max_length[50]');
		if ($this->form_validation->run()) {
			$this->db->trans_begin();
			$_copy_cid = 0;
			// 講座情報テーブルをコピー
			if (!$_copy_cid = $this->course_model->copy($this->cid)) {
				$this->db->trans_rollback();
				$this->error_message = $this->lang->line('courseedit_err_msg_copy_table_course');
				$this->displayView();
				return;
			}
			// コピー元講座に紐付く講座説明のファイルをコピー
			$_src_img_path = $this->config->item('COURSEEDIT_DESCRIPT_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid;
			$_dest_img_path = $this->config->item('COURSEEDIT_DESCRIPT_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_copy_cid;
			if (!$this->commonlib->copyFiles($_src_img_path, $_dest_img_path)) {
				$this->db->trans_rollback();
				$this->error_message = $this->lang->line('courseedit_err_msg_copy_image_course_description');
				$this->displayView();
				return;
			}
			// 講座説明内の画像のリンク文字列を新しいリンク文字列に更新
			if (!$this->course_model->update_description_image($this->cid, $_copy_cid)) {
				$this->db->trans_rollback();
				$this->error_message = $this->lang->line('courseedit_err_msg_copy_update_course_description');
				$this->displayView();
				return;
			}
			// コピー元講座に紐付くテーマテーブルをコピー
			// ※すでにコピー先講座情報IDが分かるので、
			// 　同時にテーマ説明内の画像のリンク文字列を新しいリンク文字列に更新
			if (!$this->theme_model->copyOfCourse($this->cid, $_copy_cid)) {
				$this->db->trans_rollback();
				$this->error_message = $this->lang->line('courseedit_err_msg_copy_table_theme');
				$this->displayView();
				return;
			}
			// コピーしたテーマに紐付くテーマ説明のファイルをコピー
			$_src_img_path = $this->config->item('THEMEEDIT_DESCRIPTION_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid;
			$_dest_img_path = $this->config->item('THEMEEDIT_DESCRIPTION_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_copy_cid;
			if (!$this->commonlib->copyFiles($_src_img_path, $_dest_img_path)) {
				$this->db->trans_rollback();
				$this->error_message = $this->lang->line('courseedit_err_msg_copy_image_theme_description');
				$this->displayView();
				return;
			}
			// コピーしたテーマに紐付く教材、課題をコピー
			if (!$this->copyOfRelationThemes($_copy_cid)) {
				$this->db->trans_rollback();
				// エラーメッセージは設定済み
				$this->displayView();
				return;
			}
			// コピーした講座に紐付く講師、運用管理者、ゲストをコピー
			if (!$this->students_model->copyOfCourse($this->cid, $_copy_cid)) {
				$this->db->trans_rollback();
				$this->error_message = $this->lang->line('courseedit_err_msg_copy_table_students');
				$this->displayView();
				return;
			}

			// 正常登録出来たらコピー先の講座に遷移
			$this->db->trans_commit();
			redirect($this->commonlib->baseUrl().'courseedit?cid='.$_copy_cid);
			exit;
		} else {
			// エラー時は画面にとどまる
			$this->displayView();
		}
	}

	/**
	 * コピーしたテーマに紐付く教材、課題をコピー
	 *
	 * @param コピー先講座情報テーブルID $copy_cid
	 * @return boolean 更新結果（true: 成功 / false: 失敗）
	 */
	private function copyOfRelationThemes($copy_cid)
	{
		// コピー元講座のテーマID取得
		$_src_theme_ids = $this->theme_model->getThemeIdOfCourse($this->cid);
		if (is_null($_src_theme_ids)) {
			// 紐付くテーマが無い場合は何もしない
			return true;
		}

		// コピー先講座のテーマID取得
		$_dest_theme_ids = $this->theme_model->getThemeIdOfCourse($copy_cid);

		// テーマごとに教材、課題をコピー
		// 新しいテーマIDを用いてコピーしなければいけないため、
		// テーマごとにコピーする。
		$this->load->model("Materials_Model", 'materials_model', true);
		$this->load->model("Task_Model", 'task_model', true);
		for ($_idx = 0; $_idx < count($_dest_theme_ids); $_idx++) {
			$_src_thid = $_src_theme_ids[$_idx]->id;
			$_dest_thid = $_dest_theme_ids[$_idx]->id;

			// テーマに紐付く教材テーブルをコピー
			if (!$this->materials_model->copyOfTheme($this->cid, $copy_cid, $_src_thid, $_dest_thid)) {
				$this->error_message = $this->lang->line('courseedit_err_msg_copy_table_materials');
				return false;
			}

			// テーマに紐付く教材ファイルをコピー
			$_src_materials_path = $this->config->item('THEMEEDIT_MATERIAL_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid.DIRECTORY_SEPARATOR.$_src_thid;
			$_dest_materials_path = $this->config->item('THEMEEDIT_MATERIAL_SAVE_PATH').DIRECTORY_SEPARATOR.$copy_cid.DIRECTORY_SEPARATOR.$_dest_thid;
			if (!$this->commonlib->copyFiles($_src_materials_path, $_dest_materials_path)) {
				$this->error_message = $this->lang->line('courseedit_err_msg_copy_materials');
				return false;
			}

			// テーマに紐付く課題情報テーブルをコピー
			if (!$this->task_model->copyOfTheme($this->cid, $copy_cid, $_src_thid, $_dest_thid)) {
				$this->error_message = $this->lang->line('courseedit_err_msg_copy_table_task');
				return false;
			}

			// テーマに紐付く課題ファイルをコピー
			$_src_task_img_path = $this->config->item('TASKEDIT_DESCRIPTION_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid.DIRECTORY_SEPARATOR.$_src_thid;
			$_dest_task_img_path = $this->config->item('TASKEDIT_DESCRIPTION_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$copy_cid.DIRECTORY_SEPARATOR.$_dest_thid;
			if (!$this->commonlib->copyFiles($_src_task_img_path, $_dest_task_img_path)) {
				$this->error_message = $this->lang->line('courseedit_err_msg_copy_image_task_description');
				return false;
			}

		}
		return true;
	}

	/**
	 * 登録
	 */
	public function regist()
	{
		$this->form_validation->set_rules('courseedit_courseid', $this->lang->line('courseedit_courseid'), 'trim|required|max_length[50]|callback_check_has_course_code');
		$this->form_validation->set_rules('courseedit_coursename', $this->lang->line('courseedit_coursename'), 'trim|required|max_length[50]');
		$this->form_validation->set_rules('courseedit_genre', $this->lang->line('courseedit_genre'), 'trim|required');
		$this->form_validation->set_rules('courseedit_description', $this->lang->line('courseedit_description'), 'trim|required');
		if (!is_null($this->input->post("courseedit_use_schedule", true))) {
			// 「講座の期間を設定する」がYESの場合は、開始日、終了日、カレンダー（色）もバリデーションする。
			$this->form_validation->set_rules('courseedit_startdate', $this->lang->line('courseedit_startdate'), 'trim|required|callback_check_startdate');
			$this->form_validation->set_rules('courseedit_enddate', $this->lang->line('courseedit_enddate'), 'trim|required|callback_check_enddate');
			$this->form_validation->set_rules('courseedit_calendar_color', $this->lang->line('courseedit_calendar_color'), 'trim|required');
		}
		if ($this->form_validation->run()) {
			if (is_null($this->cid) || empty($this->cid)) {
				// 新規登録
				if (!$this->cid = $this->course_model->regist()) {
					$this->error_message = $this->lang->line('courseedit_err_msg_regist');
				}
			} else {
				// 更新登録
				if (!$this->course_model->update()) {
					$this->error_message = $this->lang->line('courseedit_err_msg_update');
				}
			}
			$_has_guest = $this->students_model->hasGuest($this->cid);
			if (!is_null($this->input->post("courseedit_permit_guest", true))) {
				// 「ゲストの受講を許可する」がYESの場合、
				// 講座にゲストユーザーを登録する
				if (!$_has_guest) {
					if (!$this->students_model->insertGuest($this->cid)) {
						$this->error_message = $this->lang->line('courseedit_err_msg_guest_regist');
					}
				}
			} else {
				// 「ゲストの受講を許可する」がNOの場合、
				// 講座からゲストユーザーを削除する
				if ($_has_guest) {
					if (!$this->students_model->deleteGuest($this->cid)) {
						$this->error_message = $this->lang->line('courseedit_err_msg_guest_delete');
					}
				}
			}
			if (is_null($this->error_message)) {
				// 正常登録出来たらホーム画面に戻る
				$this->complete_message = $this->lang->line('courseedit_complete_msg_regist');
			}
		}
		// 登録後も画面にとどまる。
		$this->displayView();
	}

	/**
	 * 講座コード重複チェック
	 *
	 * @return boolean true：重複なし / false：重複あり
	 */
	public function check_has_course_code($coursecode)
	{
		$_check_cid = $this->cid;
		if (is_null($_check_cid) || empty($_check_cid)) {
			$_check_cid = 0;
		}
		if ($this->course_model->hasCourseCode($_check_cid, $coursecode)) {
			$this->form_validation->set_message("check_has_course_code", $this->lang->line('courseedit_err_msg_already_used_coursecode'));
			return false;
		}
		return true;
	}

	/**
	 * 開始日付チェック
	 *
	 * @return boolean true：開始日より後 / false：開始日より前
	 */
	public function check_startdate()
	{
		if (is_null($this->first_theme_date)) {
			return true;
		}
		$_startdate = new DateTime($this->input->post('courseedit_startdate', true));
		$_first_date = new DateTime($this->first_theme_date);
		if ($_startdate > $_first_date) {
			$this->form_validation->set_message("check_startdate", $this->lang->line('courseedit_err_msg_startdate_theme_date'));
			return false;
		}
		return true;
	}

	/**
	 * 終了日付チェック
	 *
	 * @return boolean true：エラー無 / false：エラーあり
	 */
	public function check_enddate()
	{
		$_startdate = new DateTime($this->input->post('courseedit_startdate', true));
		$_enddate = new DateTime($this->input->post('courseedit_enddate', true));
		if ($_startdate > $_enddate) {
			$this->form_validation->set_message("check_enddate", $this->lang->line('courseedit_err_msg_past_enddate'));
			return false;
		}

		if (is_null($this->last_theme_limit)) {
			return true;
		}
		$_last_limit = new DateTime($this->last_theme_limit);
		if ($_enddate < $_last_limit) {
			$this->form_validation->set_message("check_enddate", $this->lang->line('courseedit_err_msg_enddate_theme_limit'));
			return false;
		}
		return true;
	}

	/**
	 * 講座紹介画像アップロード
	 */
	public function descriptimg()
	{
		if (isset($_FILES["descriptimg"])
		&& is_uploaded_file($_FILES["descriptimg"]["tmp_name"])) {
				// 画像アップロードパス取得
			$_upload_path = $this->config->item('COURSEEDIT_DESCRIPT_IMAGE_UPLOAD_PATH');
			if (!file_exists($_upload_path)) {
				mkdir($_upload_path, 0777, true);
			}
			// 画像ライブラリロード
			$_upload_config = array(
					'max_size' => $this->config->item('COURSEEDIT_DESCRIPT_IMAGE_UPLOAD_MAX_SIZE'),
					'encrypt_name' => true,
					'upload_path' => $_upload_path,
					'allowed_types' => implode("|", $this->config->item('COURSEEDIT_DESCRIPT_IMAGE_UPLOAD_EXTENSIONS'))
			);
			$this->load->library('upload', $_upload_config);

			// 画像アップロード
			if ($this->upload->do_upload('descriptimg')) {
				$file_info = $this->upload->data();
				// 保存先ディレクトリ生成
				// 保存先パス＋講座情報テーブルID
				$_save_path = $this->config->item('COURSEEDIT_DESCRIPT_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid;
				if (!file_exists($_save_path)) {
					mkdir($_save_path, 0777, true);
				}
				// 保存先パス＋_yyyyMMddHHmmssSSS＋拡張子
				$_filename = $this->commonlib->getTimeWithMillSecond().$file_info['file_ext'];
				$_save_file_path = $_save_path.DIRECTORY_SEPARATOR.$_filename;

				// リサイズ後の画像サイズ算出
				// 画像処理ライブラリロード
				$this->load->library('imagelib');
				$_resize_size = $this->imagelib->calResizeSize($file_info['full_path'], $this->config->item('COURSEEDIT_DESCRIPT_IMAGE_MAX_LENGTH'));

				// 画像リサイズ処理
				if (!is_null($_resize_size)) {
					$_image_config = array(
						'source_image' => $file_info['full_path'],
						'new_image' => $_save_file_path,
						'width' => $_resize_size[0],
						'height' => $_resize_size[1]
					);
					// CodeIgniterの画像処理ライブラリロード
					$this->load->library('image_lib', $_image_config);
					// リサイズ実行
					$this->image_lib->resize();
					// リサイズ元のアップロード画像削除
					unlink($file_info['full_path']);
				} else {
					// リサイズしなかった場合はファイルを保存先に移動
					rename($file_info['full_path'], $_save_file_path);
				}
				echo $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_COURSE')."&id=".$this->cid."&fl=".$_filename;
				exit;
			} else {
				// 画像アップロードエラー
				// エラーメッセージ生成
				header("HTTP/1.0 404 Not Found");
				echo $this->upload->display_errors('<div class="alert alert-danger" role="alert">', '</div>');
				exit;
			}
		}
	}

	/**
	 * 削除
	 */
	public function delete()
	{
		if (!$this->error_message && !$this->theme_model->deleteByCourseId($this->cid)) { // テーマ情報削除
			$this->error_message = $this->lang->line('courseedit_err_msg_delete_table_theme');
		}
		if (!$this->error_message) {
			$this->deleteThemeFiles(); // 関連するテーマファイル削除
		}
		$this->load->model('Materials_Play_Model', 'materials_play_model', true);
		if (!$this->error_message && !$this->materials_play_model->deleteByCourseId($this->cid)) { // 教材再生時間保持情報削除
			$this->error_message = $this->lang->line('courseedit_err_msg_delete_table_material_play');
		}
		$this->load->model("Materials_Model", 'materials_model', true);
		if (!$this->error_message) {
			$this->deleteMaterialFiles(); // 関連する教材削除
		}
		if (!$this->error_message && !$this->materials_model->deleteByCourseId($this->cid)) { // 教材情報削除
			$this->error_message = $this->lang->line('courseedit_err_msg_delete_table_material');
		}
		$this->load->model("Task_Model", 'task_model', true);
		if (!$this->error_message && !$this->task_model->deleteByCourseId($this->cid)) { // 課題情報削除
			$this->error_message = $this->lang->line('courseedit_err_msg_delete_table_task');
		}
		if (!$this->error_message) {
			$this->deleteTaskFiles(); // 関連する課題ファイル削除
		}
		$this->load->model("Task_Result_Model", 'task_result_model', true);
		if (!$this->error_message) {
			$this->deleteResultFiles(); // 関連する課題解答ファイル削除
		}
		if (!$this->error_message && !$this->task_result_model->deleteByCourseId($this->cid)) { // 課題解答情報削除
			$this->error_message = $this->lang->line('courseedit_err_msg_delete_table_task_result');
		}
		if (!$this->error_message && !$this->students_model->deleteByCourseId($this->cid)) { // 講座登録者情報削除
			$this->error_message = $this->lang->line('courseedit_err_msg_delete_table_students');
		}
		$this->load->model("Diary_Model", 'diary_model', true);
		if (!$this->error_message) { // 日記の画像ファイル削除
			$this->deleteDiaryImageFiles();
		}
		if (!$this->error_message && !$this->diary_model->deleteHideInfoByCid($this->cid)) { // 日記非表示情報削除
			$this->error_message = $this->lang->line('courseedit_err_msg_delete_table_diaryhide');
		}
		$this->load->model("Diary_Like_Model", 'diary_like_model', true);
		if (!$this->error_message && !$this->diary_like_model->deleteOfCourse($this->cid)) { // 日記「Like!」削除
			$this->error_message = $this->lang->line('courseedit_err_msg_delete_table_diarylike');
		}
		if (!$this->error_message && !$this->diary_model->deleteByCid($this->cid)) { // 日記情報削除 ※日記テーブルに紐付いている他の削除処理に影響しないようにここで削除
			$this->error_message = $this->lang->line('courseedit_err_msg_delete_table_diary');
		}
		$this->load->model("Forum_Model", 'forum_model', true);
		if (!$this->error_message) { // フォーラムの画像ファイル削除
			$this->deleteForumImageFiles();
		}
		if (!$this->error_message && !$this->forum_model->deleteByCid($this->cid)) { // フォーラム情報削除
			$this->error_message = $this->lang->line('courseedit_err_msg_delete_table_forum');
		}
		$this->load->model("Forum_Manage_Model", 'forum_manage_model', true);
		if (!$this->error_message && !$this->forum_manage_model->deleteByCid($this->cid)) { // フォーラム管理情報削除
			$this->error_message = $this->lang->line('courseedit_err_msg_delete_table_forummanage');
		}
		if (!$this->course_model->delete()) { // 講座情報削除 ※講座テーブルに紐付いている他の削除処理に影響しないように最後に削除
			$this->error_message = $this->lang->line('courseedit_err_msg_delete_table_course');
		}
		if (!$this->error_message) {
			$this->deleteCourseFiles(); // 関連する講座ファイル削除
		}

		if (!$this->error_message) {
			// 正常削除出来たらホーム画面に戻る
			redirect($this->homeUrl());
			exit;
		}
		$this->displayView();
	}

	/**
	 * 関連する教材ファイル削除
	 */
	private function deleteMaterialFiles()
	{
		$_materials = $this->materials_model->getMaterialPathByCourseId($this->cid);
		if ($_materials) {
			// 教材ファイルは講座単位でファイルがまとまっているのでディレクトリごとごっそり削除する。
			$this->commonlib->removeDirs($this->config->item('THEME_MATERIAL_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid);
		}
	}

	/**
	 * 関連する課題解答ファイル削除
	 */
	private function deleteResultFiles()
	{
		// 課題解答ファイルは講座単位でファイルがまとまっているのでディレクトリごとごっそり削除する。
		$this->commonlib->removeDirs($this->config->item('TASK_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid);
	}

	/**
	 * 関連する課題「問題」ファイル削除
	 */
	private function deleteTaskFiles()
	{
		// 課題の「問題」のファイルは講座単位でファイルがまとまっているのでディレクトリごとごっそり削除する。
		$this->commonlib->removeDirs($this->config->item('TASKEDIT_DESCRIPTION_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid);
	}

	/**
	 * 関連する日記の画像ファイル削除
	 */
	private function deleteDiaryImageFiles()
	{
		// 日記の画像ファイルは講座単位でファイルがまとまっているのでディレクトリごとごっそり削除する。
		$this->commonlib->removeDirs($this->config->item('DIARY_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid);
	}

	/**
	 * 関連するフォーラムの画像ファイル削除
	 */
	private function deleteForumImageFiles()
	{
		// フォーラムの画像ファイルは講座単位でファイルがまとまっているのでディレクトリごとごっそり削除する。
		$this->commonlib->removeDirs($this->config->item('FORUM_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid);
	}

	/**
	 * 関連するテーマファイル削除
	 */
	private function deleteThemeFiles()
	{
		// テーマ説明ファイルは講座単位でファイルがまとまっているのでディレクトリごとごっそり削除する。
		$this->commonlib->removeDirs($this->config->item('THEMEEDIT_DESCRIPTION_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid);
	}

	/**
	 * 関連する講座ファイル削除
	 */
	private function deleteCourseFiles()
	{
		// 講座紹介ファイルは講座単位でファイルがまとまっているのでディレクトリごとごっそり削除する。
		$this->commonlib->removeDirs($this->config->item('COURSEEDIT_DESCRIPT_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid);
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		// 講座データ取得
		$_course = $this->course_model->getCourse($this->cid);
		if (is_null($_course)) {
			$_course = new stdClass();
			$_course->id = '';
			$_course->course_code = '';
			$_course->course_name = '';
			$_course->genre = '';
			$_course->teacher_name = $this->lang->line('courseedit_unregistered');
			$_course->picture_file = $this->commonlib->baseUrl().$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
			$_course->op_mng_name = $this->lang->line('courseedit_unregistered');
			$_course->op_mng_picture_file = $this->commonlib->baseUrl().$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
			$_course->course_description = '';
			$_course->forum_use = true;
			$_course->schedule_use = true;
			$_course->permitted_guest = false;
			$_course->start_date = '';
			$_course->end_date = '';
		} else {
			$_course = $_course[0];
			$_course->forum_use = $_course->forum_use == 1 ? true : false;
			$_course->schedule_use = $_course->schedule_use == 1 ? true : false;
			$_course->permitted_guest = $_course->permitted_guest == 1 ? true : false;
		}
		// ジャンルデータ取得
		$_genre_list = $this->genre_model->get_genre_list();
		// テーマ一覧取得
		$_theme_list = $this->theme_model->getThemeListForCourseedit($this->cid);
		// この講座での権限取得
		$_authority_info = $this->session->userdata('authority');
		$_authority = $this->students_model->getCourseAuth($this->cid, $_authority_info[0]->id);
		// ビューのデータに設定
		$this->view_data['course'] = $_course;
		$this->view_data['genre_list'] = $_genre_list;
		$this->view_data['theme_list'] = $this->workThemeList($_theme_list);
		$this->view_data['authority'] = $_authority;
		$this->view_data['cid'] = $this->cid;
		$this->view_data['script'] = $this->createJavaScript();
		$this->view_data['error_msg'] = $this->error_message;
		$this->view_data['complete_msg'] = $this->complete_message;
		$this->view_data['summernote_lang_file'] = $this->config->item('SUMMERNOTE_LANG_FILE');
		$this->view_data['colorpicker_colors'] = $this->config->item('SIMPLECOLORPICKER_OPTIONTAG_COLORS');
		$this->view_data['disable_use_schedule'] = $this->disable_use_schedule;
		// 戻る画面設定
		$this->setBackUrl($this->homeUrl());
		// ビュー表示
		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('courseedit', $this->view_data);    // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * テーマ一覧加工
	 *
	 * @param object $theme_list テーマ一覧
	 * @return NULL|object テーマ一覧
	 */
	private function workThemeList($theme_list)
	{
		if (is_null($theme_list)) {
			return null;
		}

		$_students_cnt = $this->students_model->getCountOfCourse($this->cid);
		foreach ($theme_list as $_theme) {
			if ($_theme->open == $this->config->item('THEME_OPEN')
			 || $_theme->open == $this->config->item('THEME_PUBLIC_END')
			 || !is_null($_theme->theme_date)
			 || !is_null($_theme->time_limit)) {
				// 一つでも公開または公開終了のテーマがある場合
				// または、一つでもテーマの日付け、期限が設定されている場合は、
				// 「講座の期間を設定する」を変更させない
				// （すべてのテーマが非公開かつ、テーマの日付、期限がまだひとつも設定されていない場合のみ、変更が可能）
				// ※もともとテーマが存在する場合は「講座の期間を設定する」を変更させない仕様であったが、
				// 　講座のコピー時に期間をクリアして作成する仕様となったため、
				// 　講座配下のテーマが全て非公開かつ、日付、期限が未設定の場合は後から変更できるようにする為に
				// 　このような仕様に変更となった。
				$this->disable_use_schedule = true;
			}
			$_theme->attended = 0; // 全員受講済みのフラグ
			if ($_students_cnt != 0
			 && $_students_cnt == $_theme->attended_cnt) {
				// 全ての受講者が受講済みの場合
				// 全員受講済みフラグを立てる
				// （「受講済」ラベルを表示する）
				$_theme->attended = 1;

				if ($_theme->over_limit == 1) {
					// 全ての受講者が受講済みの場合で、
					// テーマの期限が切れている場合は、
					// 期限切れフラグを落とす
					// （「期限切れ」のラベルを表示しない）
					$_theme->over_limit = 0;
				}
			}
		}

		return $theme_list;
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		$_first_theme_date = $this->first_theme_date;
		if (is_null($_first_theme_date)) {
			$_first_theme_date = '';
		}
		$_last_theme_limit = $this->last_theme_limit;
		if (is_null($_last_theme_limit)) {
			$_last_theme_limit = '';
		}

		$_script = <<<EOT
<script>
	var summernoteLang = "{$this->config->item('SUMMERNOTE_LANG')}";
	var datepickerLang = "{$this->config->item('DATEPICKER_LANG')}";
	var errMsgCouceidRequired = "{$this->lang->line('courseedit_err_msg_courseid_required')}";
	var errMsgCouceidMaxlength = "{$this->lang->line('courseedit_err_msg_courseid_maxlength')}";
	var errMsgCoucenameRequired = "{$this->lang->line('courseedit_err_msg_coursename_required')}";
	var errMsgCoucenameMaxlength = "{$this->lang->line('courseedit_err_msg_coursename_maxlength')}";
	var errMsgGenreRequired = "{$this->lang->line('courseedit_err_msg_genre_required')}";
	var errMsgDescriptionRequired = "{$this->lang->line('courseedit_err_msg_description_required')}";
	var errMsgStartdateRequired = "{$this->lang->line('courseedit_err_msg_startdate_required')}";
	var errMsgEnddateRequired = "{$this->lang->line('courseedit_err_msg_enddate_required')}";
	var errMsgCopyCouceidRequired = "{$this->lang->line('courseedit_err_msg_copy_courseid_required')}";
	var errMsgCopyCouceidMaxlength = "{$this->lang->line('courseedit_err_msg_copy_courseid_maxlength')}";
	var errMsgCopyCoucenameRequired = "{$this->lang->line('courseedit_err_msg_copy_coursename_required')}";
	var errMsgCopyCoucenameMaxlength = "{$this->lang->line('courseedit_err_msg_copy_coursename_maxlength')}";
	var themeUrl = "{$this->commonlib->baseUrl()}themeedit?cid={$this->cid}";
	var deleteActionUrl = "{$this->commonlib->baseUrl()}courseedit/delete?cid={$this->cid}";
	var firstThemeDate = "{$_first_theme_date}";
	var lastThemeLimit = "{$_last_theme_limit}";
</script>
EOT;
		return $_script;
	}

}
