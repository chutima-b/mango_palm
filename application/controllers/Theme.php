<?php
/*
 * テーマ画面コントローラー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Theme extends EX_Controller {
	/** 完了メッセージ種別：課題の提出. */
	const COMPLETE_MSG_KIND_SUBMIT = 1;
	/** 完了メッセージ種別：受講終了. */
	const COMPLETE_MSG_KIND_ATTENDED = 2;

	/** 講座ID. */
	private $cid = 0;
	/** テーマID. */
	private $thid = 0;
	/** 戻り先画面パラメータ. */
	private $pre = null;
	/** 完了メッセージ種別パラメータ. */
	private $cmp = null;
	/** ユーザーテーブルID. */
	private $uid = 0;
	/** 提出済みフラグ. */
	private $is_submit = false;
	/** フィードバック済みフラグ. */
	private $is_feedback = false;
	/** クイズフラグ. */
	private $is_quiz = false;
	/** クイズ正解数. */
	private $quiz_correct_cnt = 0;
	/** 全ての課題回答済みフラグ. */
	private $is_all_answer = true;
	/** 完了メッセージ. */
	private $complete_msg = null;
	/** エラーメッセージ. */
	private $error_msg = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_THEME')); // テーマ画面
		$this->lang->load('theme_lang');
		$this->setPageTitle($this->lang->line('theme_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link href="{$_base_url}css/theme.css?v=1.0.4" rel="stylesheet">
<link href="{$_base_url}css/diary_like.css?v=1.0.0" rel="stylesheet">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}js/diary_like.js?v=1.0.0"></script>
EOT;
		$this->addScriptFiles($_add_script);

		$this->cid = $this->input->get_post('cid', true);
		$this->thid = $this->input->get_post('thid', true);
		$this->pre = $this->input->get_post('pre', true);
		$this->cmp = $this->input->get_post('cmp', true);
		$authority = $this->session->userdata['authority'];
		$this->uid = $authority[0]->id;

		// テーマモデル
		$this->load->model('Theme_Model', 'theme', true);
		// 課題モデル
		$this->load->model('Task_Model', 'task', true);
		// 課題フィードバックモデル
		$this->load->model('Task_Feedback_Model', 'task_feedback_model', true);
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView(true);
	}

	/**
	 * 課題の提出
	 */
	public function submit()
	{
		if (!$this->input->get('attend', true)) {
			// 「課題を作成する」がYESの場合
			// まず、教材再生時間判定を行う
			if (!$this->checkPlayTimeOfMaterialVideos()) {
				$this->displayView();
				return;
			}
		}

		if (!$this->task_feedback_model->regist($this->cid, $this->thid, $this->uid)) {
			$this->error_msg = $this->lang->line('theme_error_msg_submit');
			$this->displayView();
			return;
		}

		$_comp_kind = '';
		if (!$this->input->get('attend', true)) {
			// 「課題を作成する」がYESの場合
			// 課題編集画面で保存した“解説”をデフォルトのフィードバックメッセージとする。
			$this->load->model('Task_Model', 'task_model', true);
			$this->load->model('Task_Result_Model', 'task_result_model', true);
			$this->task_list = $this->task_model->getTaskList($this->cid, $this->thid);
			foreach ($this->task_list as $_task) {
				$this->task_result_model->feedbackUpdate($_task->id, $this->uid, null, $_task->comment);
			}

			// クイズの場合、自動でフィードバックを行う
			// また、受講者確認日時も設定する
			if ($this->input->post('quiz', true) == "1") {
				$this->task_feedback_model->updateForQuizSubmit($this->cid, $this->thid, $this->uid);
			}

			$_comp_kind = self::COMPLETE_MSG_KIND_SUBMIT;
		} else {
			// 「課題を作成する」がNOの場合
			// 受講者確認日時を設定する
			$this->task_feedback_model->registConfirmdAt($this->cid, $this->thid, $this->uid);

			$_comp_kind = self::COMPLETE_MSG_KIND_ATTENDED;
		}
		// 処理が成功した場合は自画面へリダイレクト
		// ※DBエラーが発生した場合は、CodeIgniter内部でエラー画面を出力して終了するためここは呼び出されない
		redirect($this->commonlib->baseUrl()."theme?cid=".$this->cid."&thid=".$this->thid."&pre=".$this->pre."&cmp=".$_comp_kind);
	}

	/**
	 * 教材再生時間判定
	 *
	 * @return boolean 判定結果（true：「最後まで再生する」の教材動画がすべて規定再生時間以上再生済み / false：規定再生時間に満たない教材動画あり）
	 */
	private function checkPlayTimeOfMaterialVideos()
	{
		// 教材再生時間判定値がSUCCESS値に満たない教材動画情報取得
		$this->load->model("Materials_Play_Model", 'materials_play_model', true);
		$_not_success_videos = $this->materials_play_model->getNotSuccessMaterials($this->cid, $this->thid);

		if (is_null($_not_success_videos)) {
			// 教材再生時間判定値がSUCCESS値に満たない教材動画情報が無い場合は、
			// 「最後まで再生する」の教材動画がすべて規定再生時間以上再生済み
			return true;
		}

		// 規定再生時間に満たない教材動画がある場合はエラーメッセージを生成
		foreach ($_not_success_videos as $_video_info) {
			if (is_null($this->error_msg)) {
				$this->error_msg = '"';
			} else {
				$this->error_msg .= ',&nbsp;"';
			}
			$this->error_msg .= htmlspecialchars($_video_info->material_description, ENT_QUOTES, 'UTF-8');
			$this->error_msg .= '"';
		}
		$this->error_msg = $this->lang->line('theme_error_msg_not_success_material_videos'). "<br>\n" . $this->error_msg;
		return false;
	}

	/**
	 * ビュー表示
	 */
	private function displayView($is_index=false)
	{
		$theme_datas = null;
		$materials_datas = null;
		$taskcountdata = null;
		$taskdatas = null;
		$taskfeedback_data = null;
		$task_point = null;
		$students_data = null;
		$diary_data = null;
		$forum_data = null;

		if (isset($this->thid) and $this->thid !== null) {

			// テーマ情報取得
			$theme_datas = $this->theme->getThemeInfo($this->thid, true);
			if (!is_null($theme_datas) && count($theme_datas) > 0) {
				$theme_datas = $theme_datas[0];
				if ($theme_datas['quiz'] == 1) {
					$this->is_quiz = true;
				}
			} else {
				$theme_datas = null;
			}

			// 教材情報取得
			$this->load->model('Materials_Model', 'materials_model', true);
			$materials_datas = $this->materials_model->getMaterialsList($this->thid);

			// 課題フィードバック情報取得
			$taskfeedback_data = $this->task_feedback_model->getData($this->cid, $this->thid, $this->uid);
			if (is_null($taskfeedback_data)) {
				$taskfeedback_data = new stdClass();
				$taskfeedback_data->submit_at = '';
				$taskfeedback_data->draft_flag = 1;
				$taskfeedback_data->feedback_at = '';
			} else {
				$this->is_submit = true;
				if (!is_null($taskfeedback_data->feedback_at)) {
					$this->is_feedback = true;
					if ($is_index) {
						// インデックスからの呼出の場合
						if (is_null($taskfeedback_data->confirmed_at)) {
							// フィードバック済みで、一度もテーマ画面を表示していない（受講者確認日時が未設定）場合
							// 受講者確認日時を設定する
							$this->task_feedback_model->registConfirmdAt($this->cid, $this->thid, $this->uid);
						}
					}
				}
			}

			// 課題情報取得
			$taskdatas = $this->task->getThemeTaskList($this->thid);
			// 課題データ成型＆クイズ正解数設定
			// メソッド内で$this->is_feedbackを使っているので
			// この位置で成型する
			$taskdatas = $this->setupTaskData($taskdatas);

			if ($this->is_quiz) {
				// クイズの場合
				// 出題数を表示
				$task_point = $this->task->getCountOfExamination($this->cid, $this->thid);

				if ($this->is_feedback) {
					// フィードバック済みの場合は“クイズの正解数/出題数”を表示する。
					$task_point = $this->quiz_correct_cnt.'/'.$task_point;
				}
			} else {
				// クイズ以外の場合
				// このテーマの課題の評価点合計の評価点を表示
				// 課題評価点合計取得
				$task_point = $this->task->getTotalEvaluationPoints($this->cid, $this->thid);

				if ($this->is_feedback) {
					// フィードバック済みの場合は実際の“評価点/評価点”を表示する。
					// 課題解答評価点合計取得
					$this->load->model('Task_Result_Model', 'task_result', true);
					$feedback_point = $this->task_result->getTotalFeedbackPoints($this->cid, $this->thid, $this->uid);
					$task_point = $feedback_point.'/'.$task_point;
				}
			}

			// 受講者モデル
			$this->load->model('Students_Model', 'students', true);

			// 受講者情報取得
			$students_data = $this->students->getStudentsInfo($this->cid, $this->uid);

			// 日記モデル
			$this->load->model('Diary_Model', 'diary', true);

			// 日記投稿情報取得
			$diary_data = $this->diary->getDiaryListForTheme($this->cid);

			// フォーラム管理モデル
			$this->load->model('Forum_Model', 'forum', true);

			// フォーラム情報取得
			$forum_data = $this->forum->getForumListForTheme($this->cid);

		}

		if (!is_null($this->cmp)) {
			if ($this->cmp == self::COMPLETE_MSG_KIND_SUBMIT) {
				$this->complete_msg = $this->lang->line('theme_complete_msg_submit');
			} else if ($this->cmp == self::COMPLETE_MSG_KIND_ATTENDED) {
				$this->complete_msg = $this->lang->line('theme_complete_msg_attend');
			}
		}

		// 出力情報設定
		$param['uid'] = $this->uid;
		$param['theme_data'] = $theme_datas;
		$param['materials_data'] = $this->makeLinkData($materials_datas);
		$param['task_data'] = $taskdatas;
		$param['taskfeedback_data'] = $taskfeedback_data;
		$param['task_point'] = $task_point;
		$param['students_data'] = $students_data;
		$param['diary_data'] = $diary_data;
		$param['forum_data'] = $forum_data;
		$param['is_submit'] = $this->is_submit;
		$param['is_all_answer'] = $this->is_all_answer;
		$param['is_feedback'] = $this->is_feedback;
		$param['is_quiz'] = $this->is_quiz;
		$param['pre'] = $this->pre;
		$param['complete_msg'] = $this->complete_msg;
		$param['error_msg'] = $this->error_msg;
		$param['script'] = $this->createJavaScript();

		// 戻る画面設定
		if (!is_null($this->pre) && $this->pre == 'home') {
			$this->setBackUrl($this->commonlib->baseUrl()."studentshome");
		} else {
			$this->setBackUrl($this->commonlib->baseUrl()."course?cid=".$this->cid);
		}

		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('theme', $param); // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * 課題データ成型
	 *
	 * @param array $list_datas 課題情報
	 * @return array 課題情報
	 */
	private function setupTaskData($list_datas) {
		if (is_null($list_datas)) {
			return null;
		}
		$retdatas = array();
		foreach ($list_datas as $task) {
			if (is_null($task['result_at'])) {
				// 1件でも未回答がある場合は、
				// 全ての課題回答済みフラグを落とす
				$this->is_all_answer = false;
			}

			// 評価点生成
			$task['points'] = $task['evaluation_points'];
			if ($this->is_feedback) {
				$task['points'] = $task['feedback_points'].'/'.$task['evaluation_points'];
			}

			// 解答データ生成
			switch ($task['task_type']) {
			case $this->config->item('TASK_TYPE_SINGLE_SELECTION'):
			case $this->config->item('TASK_TYPE_MULTIPLE_CHOICE'):
			case $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION'):
			case $this->config->item('TASK_TYPE_SURVEY_SINGLE_SELECTION'):
			case $this->config->item('TASK_TYPE_SURVEY_MULTIPLE_CHOICE'):
				if (!is_null($task['result_at'])) {
					// 解答済みの場合
					$_input_type = 'radio';
					if ($task['task_type'] == $this->config->item('TASK_TYPE_MULTIPLE_CHOICE')
					 || $task['task_type'] == $this->config->item('TASK_TYPE_SURVEY_MULTIPLE_CHOICE')
					) {
						$_input_type = 'checkbox';
					}
					$_choices = $this->commonlib->nl2Array($task['choices']);
					$_choices_tmp = "";
					for ($_cnt = 1; $_cnt <= count($_choices); $_cnt++) {
						$_checked = '';
						$_choices[$_cnt-1] = htmlspecialchars($_choices[$_cnt-1]);
						if ($task['task_type'] == $this->config->item('TASK_TYPE_SINGLE_SELECTION')
						 || $task['task_type'] == $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')
						 || $task['task_type'] == $this->config->item('TASK_TYPE_SURVEY_SINGLE_SELECTION')
						) {
							if (!is_null($task['result_radio'])
							 && htmlspecialchars($task['result_radio']) == $_choices[$_cnt-1]) {
								// 解答した課題の値と、課題の項目名が一致した場合
								// その項目を選択状態にする。
								$_checked = ' checked';

								// さらに、フィードバック済みで、クイズの単一選択の場合
								if ($this->is_feedback && $this->is_quiz && !is_null($task['quiz_answer'])) {
									if ($task['quiz_answer'] == $_cnt) {
										// クイズの正解と解答のカウンター値が同じ場合は正解
										$task['is_correct_quiz'] = true;
										// クイズの正解数をカウントしておく
										$this->quiz_correct_cnt++;
									} else {
										// クイズの正解と解答のカウンター値が違う場合は不正解
										$task['is_correct_quiz'] = false;
									}
								}
							}
						} else {
							if (!is_null($task['result_check'])) {
								$_result_datas = explode("\n", $task['result_check']);
								foreach ($_result_datas as $_result) {
									if (htmlspecialchars($_result) == $_choices[$_cnt-1]) {
										$_checked = ' checked';
									}
								}
							}
						}
						$_choices_item = $_choices[$_cnt-1];
						if ($this->is_feedback
						 && $this->is_quiz
						 && !is_null($task['quiz_answer'])
						 && $task['quiz_answer'] == $_cnt) {
							// フィードバック済みの場合、クイズの正解の項目に色を付ける
							$_choices_item = '<span class="correct">'.$_choices_item.'</span>';
						}
						$_choices_tmp .= "<input type=\"$_input_type\" value=\"$_cnt\"$_checked disabled>{$_choices_item}<br>\n";
					}
					$task['answer'] = $_choices_tmp;
				} else {
					// 未回答の場合
					$_choices = $this->commonlib->nl2Array($task['choices']);
					$_choices_tmp = "";
					for ($_cnt = 1; $_cnt <= count($_choices); $_cnt++) {
						$_checked = '';
						$_choices[$_cnt-1] = htmlspecialchars($_choices[$_cnt-1]);
						$_choices_tmp .= "{$_choices[$_cnt-1]}<br>\n";
					}
					$task['answer'] = $_choices_tmp;
				}
				break;
			case $this->config->item('TASK_TYPE_TEXT'):
				if (!is_null($task['result_at']) && !is_null($task['result_text'])) {
					// 解答済みの場合
					$task['answer'] = '<textarea class="form-control" rows="3" disabled>'.htmlspecialchars($task['result_text']).'</textarea>';
				} else {
					// 未回答の場合
					$task['answer'] = '<textarea class="form-control" rows="3" disabled></textarea>';
				}
				break;
			case $this->config->item('TASK_TYPE_FILE'):
				if (!is_null($task['result_at']) && !is_null($task['result_file'])) {
					// 解答済みの場合
					$_src = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_TASK_RESULT')."&id=".$task['tarid']."&cid=".$this->cid;
					$task['answer'] = "<img src=\"$_src\" class=\"img-responsive\">";
				} else {
					// 未回答の場合
					$task['answer'] = '';
				}
				break;
			}
			array_push($retdatas, $task);
		}
		return $retdatas;
	}

	/**
	 * リンク用データの生成
	 *
	 * @package array 教材情報
	 * @return array リンクデータ設定済み教材情報
	 */
	private function makeLinkData($datas) {

		// 言語ファイル読み込み
		$this->lang->load('theme_lang');

		$retdatas = array();
		if ($datas != null) {
			foreach ($datas as $r) {

				if ($r['material_type'] == $this->config->item('MATERIAL_TYPE_MOVIE')) {
					$r += array('link_path' => $this->commonlib->baseUrl()."materialvideo?mid=".$r['id']."&cid=".$this->cid);
				} else if ($r['material_type'] != $this->config->item('MATERIAL_TYPE_EXTERNAL')) {
					$r += array('link_path' => $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_MATERIALS')."&id=".$r['id']."&cid=".$this->cid);
				} else {
					$r += array('link_path' => $r['external_material_path']);
				}
				array_push($retdatas, $r);
			}
		}

		return $retdatas;
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		$this->lang->load('diary_lang');

		$_script = <<<EOT
<script>
	var diaryAddUrl = "{$this->commonlib->baseUrl()}diary/like_add?cid=";
	var diaryDelUrl = "{$this->commonlib->baseUrl()}diary/like_del?cid=";
	var diaryLikeTooltipAdd = "{$this->lang->line('diary_link_like_add')}";
	var diaryLikeTooltipDel = "{$this->lang->line('diary_link_like_del')}";
</script>
EOT;
		return $_script;
	}

}