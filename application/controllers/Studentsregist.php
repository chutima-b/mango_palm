<?php
/*
 * 参加者登録画面コントローラー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Studentsregist extends EX_Controller {

	/** 講座情報テーブルID. */
	private $cid = null;
	/** 登録対象権限フラグ. */
	private $target = 0;
	/** ユーザーID. */
	private $userid = null;
	/** 姓. */
	private $family = null;
	/** 名. */
	private $first = null;
	/** 現在のページ. */
	private $page = 0;
	/* 前ページURL. */
	private $before_page = null;
	/* 次ページURL. */
	private $next_page = null;
	/** 受講者リスト表示件数. */
	private $students_list_count = null;
	/** 検索入力ユーザーID最大文字数. */
	private $search_userid_max = null;
	/** 検索入力姓最大文字数. */
	private $search_surname_max = null;
	/** 検索入力名最大文字数. */
	private $search_firstname_max = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_STUDENTS_REGIST')); // 参加者登録画面
		$this->lang->load('students_regist_lang');
		$this->load->model('Course_Model', 'course_model', true);
		// パラメータ取得
		$this->cid = $this->input->get_post('cid', true);
		$this->target = $this->input->get_post('target', true);
		$this->userid = urldecode($this->input->get_post('userid', true));
		$this->family = urldecode($this->input->get_post('family', true));
		$this->first = urldecode($this->input->get_post('first', true));
		$this->page = $this->input->get_post('page', true);
		if (is_null($this->page) || !is_numeric($this->page) || $this->page < 1) {
			$this->page = 1;
		}
		$course_name = $this->course_model->getCourseName($this->cid);
		$page_title = $this->lang->line('students_regist_header_pagetitle_students');
		if ($this->target == $this->config->item('AUTH_TEACHER')) {
			$page_title = $this->lang->line('students_regist_header_pagetitle_teacher');
		} else if ($this->target == $this->config->item('AUTH_OP_MANAGER')) {
			$page_title = $this->lang->line('students_regist_header_pagetitle_op_manager');
		}
		$page_title = $course_name.'&nbsp;'.$page_title;
		$this->setPageTitle($page_title);
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link href="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.css?v=1.0.0" rel="stylesheet">
<link href="{$_base_url}css/slms_loading.css?v=1.0.0" rel="stylesheet">
<link rel="stylesheet" href="{$_base_url}css/studentsregist.css?v=1.0.1">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}libs/jqueryvalidation/jquery.validate.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.js?v=1.0.0"></script>
<script src="{$_base_url}js/slms_loading.js?v=1.0.0"></script>
<script src="{$_base_url}js/studentsregist.js?v=1.0.1"></script>
EOT;
		$this->addScriptFiles($_add_script);

		// 受講者リスト表示件数
		$this->students_list_count = $this->config->item('STUDENTS_REGIST_DISP_COUNT');
		// 検索入力ユーザーID最大文字数
		$this->search_userid_max = $this->config->item('STUDENTS_REGIST_SEARCH_USERID_MAXLENGTH');
		// 検索入力姓最大文字数
		$this->search_surname_max = $this->config->item('STUDENTS_REGIST_SEARCH_SURNAME_MAXLENGTH');
		// 検索入力名最大文字数
		$this->search_firstname_max = $this->config->item('STUDENTS_REGIST_SEARCH_FIRSTNAME_MAXLENGTH');

	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		$candidate_count = 0;
		$candidate_data = null;

		$this->load->model('Course_Model', 'course_model', true);
		$course_name = $this->course_model->getCourseName($this->cid);

		$page_title = $this->lang->line('students_regist_pagetitle_students');
		if ($this->target == $this->config->item('AUTH_TEACHER')) {
			$page_title = $this->lang->line('students_regist_pagetitle_teacher');
		} else if ($this->target == $this->config->item('AUTH_OP_MANAGER')) {
			$page_title = $this->lang->line('students_regist_pagetitle_op_manager');
		}
		$page_title = $course_name.'&nbsp;'.$page_title;

		// 受講者情報モデル
		$this->load->model('Students_Model', 'students', TRUE);

		// 総件数取得
		$candidate_count = $this->students->getCandidateCount($this->cid, $this->userid, $this->family, $this->first);

		// ページデータ作成
		$page_data = $this->makePageData($candidate_count);

		$candidate_data = null;

		if (0 < $candidate_count) {
			// 一覧データ生成
			$candidate_data = $this->students->getCandidateList($this->cid, $this->userid, $this->family, $this->first, $page_data['page'], $this->students_list_count);
		}

		// データ設定
		$param['cid'] = $this->cid;
		$param['target'] = $this->target;
		$param['userid'] = $this->userid;
		$param['family'] = $this->family;
		$param['first'] = $this->first;
		$param['page_title'] = $page_title;
		$param['page_data'] = $page_data;
		$param['candidate_count'] = $candidate_count;
		$param['candidate_data'] = $this->makeImagePath($candidate_data);
		$param['script'] = $this->createJavaScript();

		// 戻る画面設定
		if ($this->target == $this->config->item('AUTH_STUDENT')) {
			$this->setBackUrl($this->commonlib->baseUrl().'studentsmanage?cid='.$this->cid);
		} else {
			$this->setBackUrl($this->commonlib->baseUrl().'staffmanager?cid='.$this->cid);
		}

		$this->load->view('common_header');				// 共通ヘッダー部
		$this->load->view('students_regist', $param);	// コンテンツ部
		$this->load->view('common_footer');				// 共通フッター部
	}

	/**
	 * 検索時チェック用
	 */
	public function search() {

		$this->form_validation->set_rules('userid', $this->lang->line('students_regist_saerch_userid'), 'trim|max_length['.$this->search_userid_max.']');
		$this->form_validation->set_rules('family', $this->lang->line('students_regist_search_familyname'), 'trim|max_length['.$this->search_surname_max.']');
		$this->form_validation->set_rules('first', $this->lang->line('students_regist_search_firstname'), 'trim|max_length['.$this->search_firstname_max.']');

		if ($this->form_validation->run()) {
			redirect($this->commonlib->baseUrl().'studentsregist?cid='.$this->cid.'&target='.$this->target.'&page='.$this->page.'&userid='.urlencode($this->userid).'&family='.urlencode($this->family).'&first='.urlencode($this->first), 'location');
		} else {
			$this->displayView();
		}

	}

	/**
	 * 画像リンク情報設定処理
	 *
	 * @param array $data 参加者登録画面候補者一覧情報
	 * @return array 画像リンク情報設定済み参加者登録画面候補者一覧情報
	 */
	private function makeImagePath($data) {
		$retdatas = array();
		if ($data) {
			foreach ((array)$data AS $d) {
				$image_path = $d['picture_file'];
				if ($image_path != "") {
					$d['picture_file'] = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=".$d['uid'];
				} else {
					$d['picture_file'] = $this->commonlib->baseUrl().$this->config->item('STUDENTS_REGIST_DEFAULT_IMAGE_URL');
				}
				array_push($retdatas, $d);
			}
		}

		return $retdatas;
	}

	/**
	 * ページデータ生成処理
	 *
	 * @param int $tcnt 総件数
	 * @return array ページデータ
	 */
	private function makePageData($tcnt) {

		// 総ページ数
		$max_page = ceil($tcnt / $this->students_list_count);

		// 登録後対応
		if ($max_page < $this->page) {
			$this->page = $max_page;
		}

		// 前ページ（0は前ページなし）
		$prev = 0;
		if (1 < $this->page) {
			$prev = $this->page - 1;
		}
		// 次ページ（0は次ページなし）
		$next = 0;
		if ($this->page < $max_page) {
			$next = $this->page + 1;
		}

		$_base_page_url = $this->commonlib->baseUrl().'studentsregist?cid='.$this->cid.'&target='.$this->target.'&page='.$this->page.'&userid='.urlencode($this->userid).'&family='.urlencode($this->family).'&first='.urlencode($this->first);

		// 前ページURL
		$this->before_page = null;
		if (1 < $this->page) {
			$this->before_page = $_base_page_url.'&page='.($this->page - 1);

		}

		// 次ページURL
		$this->next_page = null;
		if ($this->page < $max_page) {
			$this->next_page = $_base_page_url.'&page='.($this->page + 1);
		}

		// データ設定
		$page_data['before_page'] = $this->before_page;
		$page_data['next_page'] = $this->next_page;
		$page_data['page'] = $this->page;
		$page_data['max_page'] = $max_page;
		$page_data['prev'] = $prev;
		$page_data['next'] = $next;
		$page_data['total'] = $tcnt;

		return $page_data;
	}

	/**
	 * 参加者情報登録
	 */
	public function regist() {

		$uid = $this->input->post('uid', true);

		// 受講者情報モデル
		$this->load->model('Students_Model', 'students', TRUE);

		if ($this->target == $this->config->item('AUTH_TEACHER') or $this->target == $this->config->item('AUTH_OP_MANAGER')) {
			$this->students->deleteByAuth($this->cid, $this->target);
		}

		$this->students->insert($this->cid, $uid, $this->target);

		if ($this->target == $this->config->item('AUTH_STUDENT')) {
			redirect($this->commonlib->baseUrl().'studentsregist?cid='.$this->cid.'&target='.$this->target.'&page='.$this->page.'&userid='.urlencode($this->userid).'&family='.urlencode($this->family).'&first='.urlencode($this->first), 'location');
		} else {
			redirect($this->commonlib->baseUrl().'staffmanager?cid='.$this->cid, 'location');
		}
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		$_befor_page_url = '';
		if (!is_null($this->before_page)) {
			$_befor_page_url = $this->before_page;
		}
		$_next_page_url = '';
		if (!is_null($this->next_page)) {
			$_next_page_url = $this->next_page;
		}
		$_script = <<<EOT
<script>
	var beforePageUrl = "{$_befor_page_url}";
	var nextPageUrl = "{$_next_page_url}";
	var useridMax = {$this->search_userid_max};
	var familyMax = {$this->search_surname_max};
	var firstMax = {$this->search_firstname_max};
	var errMsgUserIDMaxlength = "{$this->lang->line('students_regist_script_err_msg_userid_maxlength')}";
	var errMsgFamilyMaxlength = "{$this->lang->line('students_regist_script_err_msg_family_maxlength')}";
	var errMsgFirstMaxlength = "{$this->lang->line('students_regist_script_err_msg_first_maxlength')}";
</script>
EOT;
		return $_script;
	}

}
