<?php
/*
 * 課題画面コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Task extends EX_Controller {
	/** 権限情報. */
	private $authority_info = null;
	/** 講座テーブルID. */
	private $cid = 0;
	/** テーマテーブルID. */
	private $thid = 0;
	/** 課題テーブルID. */
	private $taid = 0;
	/** 課題データ. */
	private $task_data = null;
	/** 解答画像保存先パス. */
	private $task_image_save_dir = null;
	/** 戻り画面URL. */
	private $back_url = null;
	/** エラーメッセージ. */
	private $error_message = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_TASK')); // 課題画面
		$this->lang->load('task_lang');
		$this->setPageTitle($this->lang->line('task_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link href="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.css?v=1.0.0" rel="stylesheet">
<link rel="stylesheet" href="{$_base_url}css/task.css?v=1.0.0">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}libs/jqueryvalidation/jquery.validate.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.js?v=1.0.1"></script>
<script src="{$_base_url}js/task.js?v=1.0.3"></script>
EOT;
		$this->addScriptFiles($_add_script);

		// ユーザー情報取得
		$this->authority_info = $this->session->userdata('authority');

		$this->view_data = array();
		$this->load->model("Task_Model", 'task_model', true);

		// パラメータ取得
		$this->cid = $this->input->get('cid', true);
		$this->thid = $this->input->get('thid', true);
		$this->taid = $this->input->get('taid', true);
		// 解答画像保存先
		$this->task_image_save_dir = $this->config->item('TASK_IMAGE_SAVE_PATH')
				.DIRECTORY_SEPARATOR.$this->cid
				.DIRECTORY_SEPARATOR.$this->thid
				.DIRECTORY_SEPARATOR.$this->taid;
		// 課題データ取得
		$this->task_data = $this->task_model->getTaskWithResult($this->taid);
		// 戻り画面URL設定
		$this->back_url = $this->commonlib->baseUrl()."theme?cid=".$this->cid."&thid=".$this->thid."#task_".$this->taid;
		$this->setBackUrl($this->back_url);
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * 解答
	 */
	public function submission()
	{
		$this->load->model("Task_Result_Model", 'task_result_model', true);

		$this->task_result_model->course_id = $this->input->get('cid', true);
		$this->task_result_model->theme_id = $this->input->get('thid', true);
		$this->task_result_model->task_id = $this->input->get('taid', true);
		$this->task_result_model->uid = $this->authority_info[0]->id;

		$_is_no_answer = false;
		if ($this->input->post('task_is_noanswer', true) == '1') {
			$_is_no_answer = true;
			if (isset($_POST['task_file_tmp'])) {
				// 画像の場合は、保存されているファイルを削除
				$_task_img_save_path = $this->task_image_save_dir;
				$this->commonlib->removeDirs($_task_img_save_path);
			}
		}

		$_is_regist = false;
		if ($_is_no_answer) {
			// 無解答ボタン押下の場合は、
			// 解答データはNULLで登録
			$_is_regist = true;
		} else {
			if (!is_null($this->input->post('task_radio', true))) {
				// 単一選択の場合
				$this->task_result_model->result_radio = $this->createResultData(array($this->input->post('task_radio', true)));
				$_is_regist = true;
			} else if (!is_null($this->input->post('task_checkbox[]', true))) {
				// 複数選択の場合
				$this->task_result_model->result_check = $this->createResultData($this->input->post('task_checkbox[]', true));
				$_is_regist = true;
			} else if (!is_null($this->input->post('task_text', true))) {
				// テキストの場合
				$this->task_result_model->result_text = $this->input->post('task_text', true);
				$_is_regist = true;
			} else if (isset($_POST['task_file_tmp'])) {
				// ファイルの場合
				// 画像アップロード処理
				$this->task_result_model->result_file = $this->imageUpload();
				$_is_regist = true;
			}
		}

		// 課題情報登録
		if ($_is_regist
		 && is_null($this->error_message)
		 && $this->task_result_model->regist()) {
			redirect($this->back_url);
		} else {
			$this->displayView();
		}
	}

	/**
	 * 画像アップロード
	 *
	 * @return string 保存した画像ファイルパス
	 */
	private function imageUpload()
	{
		// 画像アップロードパス取得
		$_upload_path = $this->config->item('TASK_IMAGE_UPLOAD_PATH');
		if (!file_exists($_upload_path)) {
			mkdir($_upload_path, 0777, true);
		}
		// 画像ライブラリロード
		$_upload_config = array(
				'max_size' => $this->config->item('TASK_IMAGE_UPLOAD_MAX_SIZE'),
				'encrypt_name' => true,
				'upload_path' => $_upload_path,
				'allowed_types' => implode("|", $this->config->item('TASK_IMAGE_UPLOAD_EXTENSIONS'))
		);
		$this->load->library('upload', $_upload_config);

		// 画像アップロード
		if ($this->upload->do_upload('task_file')) {
			$file_info = $this->upload->data();
			// 保存先ディレクトリ生成
			// 保存先パス＋講座情報テーブルID＋テーマテーブルID＋課題情報テーブルID
			if (!file_exists($this->task_image_save_dir)) {
				mkdir($this->task_image_save_dir, 0777, true);
			} else {
				// 古い保存先ファイルを削除しておく
				$this->deleteSaveFile($this->task_image_save_dir.DIRECTORY_SEPARATOR.$this->authority_info[0]->id);
			}
			// 保存先パス＋ユーザーテーブルID＋拡張子
			$_save_file_path = $this->task_image_save_dir.DIRECTORY_SEPARATOR.$this->authority_info[0]->id.$file_info['file_ext'];

			// リサイズ後の画像サイズ算出
			// 画像処理ライブラリロード
			$this->load->library('imagelib');
			$_resize_size = $this->imagelib->calResizeSize($file_info['full_path'], $this->config->item('TASK_IMAGE_MAX_LENGTH'));

			// 画像リサイズ処理
			if (!is_null($_resize_size)) {
				$_image_config = array(
					'source_image' => $file_info['full_path'],
					'new_image' => $_save_file_path,
					'width' => $_resize_size[0],
					'height' => $_resize_size[1]
				);
				// CodeIgniterの画像処理ライブラリロード
				$this->load->library('image_lib', $_image_config);
				// リサイズ実行
				$this->image_lib->resize();
				// リサイズ元のアップロード画像削除
				unlink($file_info['full_path']);
			} else {
				// リサイズしなかった場合はファイルを保存先に移動
				rename($file_info['full_path'], $_save_file_path);
			}

			// データベースに保存する形式は
			// 定義ファイルに定義した「解答画像保存先パス」を除いた部分
			// （/講座ID/テーマID/課題ID/ファイル名）
			return DIRECTORY_SEPARATOR.$this->cid
				.DIRECTORY_SEPARATOR.$this->thid
				.DIRECTORY_SEPARATOR.$this->taid
				.DIRECTORY_SEPARATOR.basename($_save_file_path);
		} else {
			// 画像アップロードエラー
			// エラーメッセージ生成
			$this->error_message = $this->upload->display_errors('<div class="alert alert-danger" role="alert">', '</div>');
		}
		return null;

	}

	/**
	 * 保存先ファイル削除.
	 *
	 * @param string 保存先ファイル名（拡張子無し）
	 */
	private function deleteSaveFile($save_file_name_ex_ext)
	{
		$_array_ext = $this->config->item('TASK_IMAGE_UPLOAD_EXTENSIONS');
		foreach ($_array_ext as $_ext) {
			$_delete_file = $save_file_name_ex_ext.'.'.$_ext;
			if (file_exists($_delete_file)) {
				unlink($_delete_file);
			}
		}
	}

	/**
	 * 送信パラメータから解答データ生成
	 *
	 * @param array $array_param 送信パラメータ
	 * @return string 解答データ
	 */
	private function createResultData($array_param)
	{
		$_result = "";
		$_choices = $this->commonlib->nl2Array($this->task_data[0]->choices);
		foreach ($array_param as $_param) {
			if ($_result != "") {
				$_result .= "\n";
			}
			$_result .= $_choices[$_param-1];
		}
		return $_result;
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		// 課題データ設定
		if (!is_null($this->task_data)) {
			$_enctype = '';
			switch ($this->task_data[0]->task_type) {
			case $this->config->item('TASK_TYPE_SINGLE_SELECTION'):
			case $this->config->item('TASK_TYPE_MULTIPLE_CHOICE'):
			case $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION'):
			case $this->config->item('TASK_TYPE_SURVEY_SINGLE_SELECTION'):
			case $this->config->item('TASK_TYPE_SURVEY_MULTIPLE_CHOICE'):
				$_input_type = 'radio';
				$_input_name = 'task_radio';
				$_input_required = ' required';
				if ($this->task_data[0]->task_type == $this->config->item('TASK_TYPE_MULTIPLE_CHOICE')
				 || $this->task_data[0]->task_type == $this->config->item('TASK_TYPE_SURVEY_MULTIPLE_CHOICE')) {
					$_input_type = 'checkbox';
					$_input_name = 'task_checkbox[]';
					$_input_required = '';
				}
				$_choices = $this->commonlib->nl2Array($this->task_data[0]->choices);
				$_choices_tmp = "";
				for ($_cnt = 1; $_cnt <= count($_choices); $_cnt++) {
					$_choices[$_cnt-1] = htmlspecialchars($_choices[$_cnt-1]);
					$_checked = '';
					if ($this->task_data[0]->task_type == $this->config->item('TASK_TYPE_SINGLE_SELECTION')
					 || $this->task_data[0]->task_type == $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')
					 || $this->task_data[0]->task_type == $this->config->item('TASK_TYPE_SURVEY_SINGLE_SELECTION')
					) {
						if (!is_null($this->task_data[0]->result_radio)
						 && htmlspecialchars($this->task_data[0]->result_radio) == $_choices[$_cnt-1]) {
							$_checked = ' checked';
						}
					} else {
						if (!is_null($this->task_data[0]->result_check)) {
							$_result_datas = explode("\n", $this->task_data[0]->result_check);
							foreach ($_result_datas as $_result) {
								if (htmlspecialchars($_result) == $_choices[$_cnt-1]) {
									$_checked = ' checked';
								}
							}
						}
					}
					$_choices_tmp .= "<label><input type=\"$_input_type\" name=\"$_input_name\" value=\"$_cnt\"$_checked $_input_required>{$_choices[$_cnt-1]}</label><br>\n";
				}
				$this->task_data[0]->answer = $_choices_tmp;
				break;
			case $this->config->item('TASK_TYPE_TEXT'):
				if (!is_null($this->task_data[0]->result_at) && !is_null($this->task_data[0]->result_text)) {
					// 解答済みの場合
					$this->task_data[0]->answer = '<textarea class="form-control" rows="4" cols="70" name="task_text" required>'.htmlspecialchars($this->task_data[0]->result_text).'</textarea><br>';
				} else {
					// 未回答の場合
					$this->task_data[0]->answer = '<textarea class="form-control" rows="4" cols="70" name="task_text" required></textarea><br>';
				}
				break;
			case $this->config->item('TASK_TYPE_FILE'):
				$_enctype = ' enctype="multipart/form-data"';
				$_src = '';
				if (!is_null($this->task_data[0]->result_at) && !is_null($this->task_data[0]->result_file)) {
					// 解答済みの場合
					$_src = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_TASK_RESULT')."&id=".$this->task_data[0]->tarid."&cid=".$this->cid;
				}
				$this->task_data[0]->answer =<<<EOT
				<label class="btn btn-primary" for="task_file">
					<input id="task_file" name="task_file" type="file" accept="image/gif,image/png,image/jpeg" style="display:none;">
					{$this->lang->line('task_btn_file')}
				</label>
				<img id="thumbnail" class="img-responsive" src="{$_src}">
				<input type="hidden" name="task_file_tmp">
EOT;
				break;
			}
			$this->view_data['enctype'] = $_enctype;
			$this->view_data['task'] = $this->task_data[0];
		}

		$this->view_data['urlparam'] = '?cid='.$this->cid.'&thid='.$this->thid.'&taid='.$this->taid;

		if (!is_null($this->error_message)) {
			// エラーメッセージ設定
			$this->view_data['error_msg'] = $this->error_message;
		}

		$this->view_data['script'] = $this->createJavaScript();

		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('task', $this->view_data); // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		$_script = <<<EOT
<script>
	var pictureMaxLength = "{$this->config->item('TASK_IMAGE_MAX_LENGTH')}px";
	var validMsgRequireChoose = "{$this->lang->line('task_valid_msg_require_choose')}";
	var validMsgRequireText = "{$this->lang->line('task_valid_msg_require_text')}";
</script>
EOT;
		return $_script;
	}

}
