<?php
/*
 * ログイン画面コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/** 権限情報. */
	public $authorityInfo = null; // form_validationでアクセスするのでpublicにする。

	/** 基準URL. */
	private $base_url = null;

	/** ユーザーID **/
	private $mangopid = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();

		$this->lang->load('login_lang');
		$_assets_url = $this->commonlib->assetsUrl();
		$_add_css = <<<EOT
<link href="{$_assets_url}libs/awesome-bootstrap-checkbox-master/bower_components/Font-Awesome/css/font-awesome.css" rel="stylesheet">
<link href="{$_assets_url}libs/awesome-bootstrap-checkbox-master/awesome-bootstrap-checkbox.css" rel="stylesheet">
<link href="{$_assets_url}css/login.css?v=1.0.0" rel="stylesheet">
EOT;
		$_view_data = array('controller_kind' => $this->config->item('CONTROLLER_KIND_LOGIN'),
				'page_title' => $this->lang->line('login_header_pagetitle'),
				'add_css' => $_add_css,
				'add_scripts' => '<script src="'.$_assets_url.'js/login.js?v=1.0.1"></script>',
				'_assets_url' => $_assets_url
		);
		$this->load->vars($_view_data);

		$this->base_url = $this->commonlib->baseUrl();
		$this->load->model("Authority_Model", 'authority_model', true);
		$this->load->model("Setting_Model", 'setting_model', true);

		$this->mangopid = $this->input->get('mangopid', true);
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		// セッションが存在するか確認
		$this->check_living_session();

		// 自動ログイン可能か判定
		if (!is_null(get_cookie('token'))) {
			$token = get_cookie('token');
			$this->authorityInfo = $this->authority_model->auto_log_in($token);
			print_r($this->authorityInfo);
			exit();
			// 自動ログイン情報がある場合
			if (!is_null($this->authorityInfo)) {
				// 一旦auto_loginを削除
				$this->delete_auto_login($token);
				// 新たにauto_loginをセット（tokenを再生成）
				$this->setup_auto_login();
				// 最終ログイン日時更新
				$this->authority_model->update_login($this->authorityInfo[0]->id, $this->authorityInfo[0]->authority);
				// 認証成功時の処理
				$this->proc_succeeding();
			}
		}

		// 認証されなかった場合はログイン画面を表示
		$this->displayView();
	}

	/**
	 * セッションが存在するか確認
	 */
	private function check_living_session() {
		if($this->session->userdata('is_logged_in')) { // セッションがある場合
			$this->authorityInfo = $this->session->userdata('authority');
			$this->authority_model->update_login($this->authorityInfo[0]->id, $this->authorityInfo[0]->authority); // 最終ログイン日時更新
			if ($this->authorityInfo[0]->authority == $this->config->item('AUTH_TEACHER')
			 || $this->authorityInfo[0]->authority == $this->config->item('AUTH_OP_MANAGER')
			 || $this->authorityInfo[0]->authority == $this->config->item('AUTH_SYS_MANAGER')) {
				// 管理系ユーザーの場合はホーム画面（管理者）に遷移
				redirect($this->base_url.'staffhome');
			} else {
				// 受講者系ユーザーの場合はホーム画面（受講者）に遷移
				redirect($this->base_url.'studentshome');
			}
		}
	}

	/**
	 * 認証成功時の処理
	 */
	private function proc_succeeding() {
		// 壁紙情報も合わせて設定
		$this->load->model("Setting_Model", 'setting_model', true);

		// セッションにログイン情報設定
		$_data = array(
			"authority" => $this->authorityInfo,
			"is_logged_in" => true
		);
		$this->session->set_userdata($_data);

		// 一旦セッションから設定情報を削除しておく
		$this->session->unset_userdata('system_title');
		$this->session->unset_userdata('logo');
		$this->session->unset_userdata('wallpaper');
		$this->session->unset_userdata('backcolor');

		// 設定情報が取得出来たらセッションに設定
		$_settings = $this->setting_model->get_display_data();
		$_is_set_back_color = false;
		if (!is_null($_settings)) {
			// タイトル
			if (!is_null($_settings->title) && strlen($_settings->title) > 0) {
				$this->session->set_userdata('system_title', $_settings->title);
			}
			// ロゴ
			if (!is_null($_settings->logo_img) && strlen($_settings->logo_img) > 0) {
				$this->session->set_userdata('logo', $_settings->logo_img);
			}
			// 壁紙
			if (!is_null($_settings->background_img)
			 && strlen($_settings->background_img) > 0
			 && $_settings->back_img_use == 1) {
				// 壁紙は、壁紙使用フラグが立っている場合に設定
				$this->session->set_userdata('wallpaper', $_settings->background_img);
			}
			// 背景色
			if (!is_null($_settings->background_color) && strlen($_settings->background_color) > 0) {
				$this->session->set_userdata('backcolor', $_settings->background_color);
				$_is_set_back_color = true;
			}
		}

		if (!$_is_set_back_color) {
			// 背景色が設定されなかった場合は
			// 定義の先頭に指定された色を背景色に設定する
			$_simplecolorpicker_optiontag_colors = $this->config->item('SIMPLECOLORPICKER_OPTIONTAG_COLORS_FOR_BACKGROUND');
			reset($_simplecolorpicker_optiontag_colors);
			$_default_color = key($_simplecolorpicker_optiontag_colors);
			$this->session->set_userdata('backcolor', $_default_color);
		}

		// 権限に応じた画面に遷移
		if ($this->authorityInfo[0]->authority == $this->config->item('AUTH_TEACHER')
		 || $this->authorityInfo[0]->authority == $this->config->item('AUTH_OP_MANAGER')
		 || $this->authorityInfo[0]->authority == $this->config->item('AUTH_SYS_MANAGER')) {
			redirect($this->base_url.'staffhome');
		} else {
			redirect($this->base_url.'studentshome');
		}
	}

	/**
	 * オートログイン情報削除
	 */
	public function delete_auto_login($token)
	{
		$this->authority_model->delete_token($token);

		$_cookie_name = 'token';
		$_cookie_expire = time() - 1800;
		$_cookie_path = '/';
		$_cookie_domain = $_SERVER['SERVER_NAME'];
		setcookie($_cookie_name, $token, $_cookie_expire, $_cookie_path, $_cookie_domain);
	}

	/**
	 * 「ログインしたままにする」削除
	 */
	public function delete_login_continue()
	{
		$_cookie_name = 'login_continue';
		$_cookie_expire = time() - 1800;
		$_cookie_path = '/';
		$_cookie_domain = $_SERVER['SERVER_NAME'];
		setcookie($_cookie_name, $token, $_cookie_expire, $_cookie_path, $_cookie_domain);
	}

	/**
	 * オートログイン情報設定
	 */
	public function setup_auto_login()
	{
		$_token = hash("sha256", uniqid().mt_rand(1, 999999999).'_auto_login');

		$this->authority_model->update_token($this->authorityInfo[0]->id, $_token);

		$_cookie_name = 'token';
		$_cookie_expire = time() + 3600 * 24 * $this->config->item('AUTO_LOGIN_COKKIE_EXPIRE');
		$_cookie_path = '/';
		$_cookie_domain = $_SERVER['SERVER_NAME'];
		setcookie($_cookie_name, $_token, $_cookie_expire, $_cookie_path, $_cookie_domain);

		// 「ログインしたままにする」状態を保存
		$this->setup_login_continue();
	}

	/**
	 * 「ログインしたままにする」設定
	 */
	public function setup_login_continue()
	{
		$_cookie_name = 'login_continue';
		$_cookie_expire = time() + 3600 * 24 * $this->config->item('AUTO_LOGIN_COKKIE_EXPIRE'); // オートログインと同じ期間にする。
		$_cookie_path = '/';
		$_cookie_domain = $_SERVER['SERVER_NAME'];
		setcookie($_cookie_name, '1', $_cookie_expire, $_cookie_path, $_cookie_domain);
	}

	/**
	 * ログイン
	 */
	public function login()
	{
		$this->check_living_session();

		$this->form_validation->set_rules('login_userid', $this->lang->line('login_userid'), 'trim|required');
		$this->form_validation->set_rules('login_password', $this->lang->line('login_passwd'), 'trim|required|callback_validate_credentials'); // ここのコールバック関数でログイン認証する

		// ログイン認証できた場合
		if ($this->form_validation->run()) {
			// 一旦auto_loginを削除
			if (!is_null(get_cookie('token'))) {
				$this->delete_auto_login(get_cookie('token'));
			}

			if(!is_null($this->input->post('login_continue', true))) {
				// 新たにauto_loginをセット（tokenを生成）
				$this->setup_auto_login();
			} else {
				// 「ログインしたままにする」削除
				$this->delete_login_continue();
			}

			// 最終ログイン日時更新
			$this->authority_model->update_login($this->authorityInfo[0]->id, $this->authorityInfo[0]->authority);
			// 認証成功時の処理
			$this->proc_succeeding();
		}

		// 認証されなかった場合はログイン画面を表示
		$this->displayView();
	}

	/**
	 * ゲストログイン
	 */
	public function guest()
	{
		$this->authorityInfo = $this->authority_model->guest_log_in();

		// ゲストログイン認証できた場合
		if (!is_null($this->authorityInfo)) {
			// 一旦auto_loginを削除
			if (!is_null(get_cookie('token'))) {
				$this->delete_auto_login(get_cookie('token'));
			}

			if(!is_null($this->input->post('login_continue', true))) {
				// 新たにauto_loginをセット（tokenを生成）
				$this->setup_auto_login();
			} else {
				// 「ログインしたままにする」削除
				$this->delete_login_continue();
			}

			// 認証成功時の処理
			$this->proc_succeeding();
		}

		// 認証されなかった場合はログイン画面を表示
		$this->displayView();
	}

	/**
	 * ログイン認証
	 */
	public function validate_credentials()
	{
		$this->authorityInfo = $this->authority_model->can_log_in();
		if (!is_null($this->authorityInfo)) {
			return true;
		} else {
			$this->form_validation->set_message("validate_credentials", $this->lang->line('login_err_msg_login'));
			return false;
		}
	}

	/**
	 * ログアウト
	 */
	public function logout()
	{
		if (!is_null(get_cookie('token'))) {
			$this->delete_auto_login(get_cookie('token'));
		}
		$this->session->sess_destroy();
		redirect($this->base_url.'login');
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		// セッションが無いのでDBから設定情報取得
		$_settings = $this->setting_model->get_display_data();
		$_view_data = array();
		$_is_set_back_color = false;
		if (!is_null($_settings)) {
			// タイトル設定
			$_view_data['tmp_system_title'] = $_settings->title;
			// ロゴ設定
			$_view_data['tmp_logo'] = $_settings->logo_img;
			// 壁紙設定
			if ($_settings->back_img_use == 1) {
				// 壁紙は、壁紙使用フラグが立っている場合に設定
				$_view_data['tmp_wallpaper'] = $_settings->background_img;
			}
			// 背景色設定
			if (!is_null($_settings->background_color) && strlen($_settings->background_color) > 0) {
				$_view_data['tmp_backcolor'] = $_settings->background_color;
				$_is_set_back_color = true;
			}
		}

		if (!$_is_set_back_color) {
			// 背景色が設定されなかった場合は
			// 定義の先頭に指定された色を背景色に設定する
			$_simplecolorpicker_optiontag_colors = $this->config->item('SIMPLECOLORPICKER_OPTIONTAG_COLORS_FOR_BACKGROUND');
			reset($_simplecolorpicker_optiontag_colors);
			$_default_color = key($_simplecolorpicker_optiontag_colors);
			$_view_data['tmp_backcolor'] = $_default_color;
		}

		$_view_data['login_continue'] = false;
		if (!is_null(get_cookie('login_continue'))) {
			$_view_data['login_continue'] = true;
		}

		$_view_data['script'] = $this->createJavaScript();

		$param['mangopid'] = $this->mangopid;

		$this->load->view('common_header', $_view_data); // 共通ヘッダー部
		$this->load->view('login', $param);         // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		$_script = <<<EOT
<script>
	var gestLoginUrl = "{$this->commonlib->baseUrl()}login/guest";
</script>
EOT;
		return $_script;
	}

}
