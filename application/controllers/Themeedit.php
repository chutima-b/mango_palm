<?php
/*
 * テーマ編集画面コントローラー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class ThemeEdit extends EX_Controller {
	/** エラー種別：公開時、課題未作成エラー. */
	const ERR_KIND_OPEN_HASNT_TASK = '1';

	/** テーマタイトル最大文字数. */
	private $themetitle_max = null;
	/** テーマ目的最大文字数. */
	private $themegoal_max = null;
	/** 教材説明最大文字数. */
	private $materialdescription_max = null;
	/** 教材リソース最大文字数. */
	private $materialresource_max = null;
	/** 講座情報テーブルデータ. */
	private $course_data = null;
	/** 講座開始日. */
	private $course_start_date = '';
	/** 講座終了日. */
	private $course_end_date = '';
	/** 課題を作成するフラグ. */
	private $evaluation_use = false;
	/** スケジュール利用フラグ. */
	private $is_schedule_use = false;
	/** クイズフラグ. */
	private $is_quiz = false;
	/** エラーメッセージ. */
	private $error_message = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_THEME_EDIT')); // テーマ編集画面
		$this->lang->load('themeedit_lang');
		$this->setPageTitle($this->lang->line('themeedit_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link rel="stylesheet" type="text/css" href="{$_base_url}libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css">
<link href="{$_base_url}libs/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet">
<link href="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.css?v=1.0.0" rel="stylesheet">
<link href="{$_base_url}libs/summernote/summernote.css" rel="stylesheet">
<link rel="stylesheet" href="{$_base_url}css/slms_loading.css?v=1.0.0">
<link rel="stylesheet" href="{$_base_url}css/themeedit.css?v=1.0.4">
EOT;
		$this->addCssFiles($_add_css);
		$_summernote_lang_file = $this->config->item('SUMMERNOTE_LANG_FILE');
		$_summernote_lang_script = "";
		if (!empty($_summernote_lang_file)) {
			$_summernote_lang_script = "<script src=\"{$_base_url}libs/summernote/lang/{$this->config->item('SUMMERNOTE_LANG_FILE')}\"></script>";
		}
		$_add_script =<<<EOT
<script src="{$_base_url}libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="{$_base_url}libs/bootstrap-datepicker/locales/bootstrap-datepicker.ja.min.js"></script>
<script src="{$_base_url}libs/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="{$_base_url}libs/summernote/summernote.custom.js"></script>
{$_summernote_lang_script}
<script src="{$_base_url}libs/jqueryvalidation/jquery.validate.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.js?v=1.0.0"></script>
<script src="{$_base_url}js/slms_loading.js?v=1.0.0"></script>
<script src="{$_base_url}js/themeedit.js?v=1.0.12"></script>
EOT;
		$this->addScriptFiles($_add_script);

		// テーマタイトル最大文字数
		$this->themetitle_max = $this->config->item('THEMEEDIT_TITLE_MAXLENGTH');
		// テーマ目的最大文字数
		$this->themegoal_max = $this->config->item('THEMEEDIT_GOAL_MAXLENGTH');
		// 教材説明最大文字数
		$this->materialdescription_max = $this->config->item('THEMEEDIT_MATERIAL_DESCRIPTION_MAXLENGTH');
		// 教材リソース最大文字数
		$this->materialresource_max = $this->config->item('THEMEEDIT_MATERIAL_RESOURCE_MAXLENGTH');

		$this->load->model('Theme_Model', 'theme', true);
		$this->load->model("Course_Model", 'course_model', true);
		$this->load->model('Task_Model', 'task', true);
		$this->load->model('Task_Result_Model', 'task_result_model', true);
		$this->load->model('Materials_Model', 'materials_model', true);

		$_cid = $this->input->get_post('cid', true);
		$this->course_data = $this->course_model->getCourse($_cid);
		$this->course_start_date = $this->course_data[0]->start_date;
		$this->course_end_date = $this->course_data[0]->end_date;
		if ($this->course_data[0]->schedule_use == 1) {
			$this->is_schedule_use = true;
		}
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		$cid = $this->input->get_post('cid', true);
		$theme_datas = null;
		$materials_datas = null;
		$taskdatas = null;
		$is_attended = false; // １件でも受講済み（課題フィードバックテーブルにデータがある）フラグ

		$thid = $this->input->get_post('thid', true);

		// テーマ情報取得
		$theme_tmp = $this->theme->getThemeInfoForEdit($thid);
		if (isset($thid) && $thid !== null
		 && !is_null($theme_tmp) && count($theme_tmp) > 0) {

			$theme_datas = $theme_tmp[0];

			if ($theme_datas['evaluation_use'] == 1) {
				$theme_datas['evaluation_use'] = true;
				$this->evaluation_use = true;
			} else {
				$theme_datas['evaluation_use'] = false;
			}

			if ($this->course_data[0]->permitted_guest == 1) {
				// 講座情報テーブルの“ゲスト許可”の値が“許可する”の場合、
				// “課題を作成する”のスイッチは“NO”にする
				$theme_datas['evaluation_use'] = false;
			}

			if ($theme_datas['quiz'] == 1) {
				$theme_datas['quiz'] = true;
				$this->is_quiz = true;
			} else {
				$theme_datas['quiz'] = false;
			}

			// 教材情報取得
			$materials_datas = $this->materials_model->getMaterialsList($thid);

			// 課題情報取得
			$taskdatas = $this->task->getThemeTaskList($thid);

			// 該当テーマの受講情報が存在するか取得
			$this->load->model('Task_Feedback_Model', 'task_feedback_model', TRUE);
			$is_attended = $this->task_feedback_model->hasDataOfTheme($cid, $thid);
		}

		$param = array();

		$cmp = $this->input->get_post('cmp', true);
		if (isset($cmp) && $cmp !== null) {
			$cmp_msg = $this->lang->line('themeedit_complete_msg_update');
			if ($cmp == 1) {
				$cmp_msg = $this->lang->line('themeedit_complete_msg_regist');
			}
			$param['complete_msg'] = '<div class="alert alert-success" role="alert">'.$cmp_msg.'</div>';
		}

		$err = $this->input->get_post('err', true);
		if (isset($err) && $err !== null) {
			if ($err == self::ERR_KIND_OPEN_HASNT_TASK) {
				$this->error_message = '<div class="alert alert-danger" role="alert">'.$this->lang->line('themeedit_err_msg_hasnt_task').'</div>';
			}
		}

		// 戻る画面設定
		$this->setBackUrl($this->commonlib->baseUrl()."courseedit?cid=".$cid);
		// 表示データ設定
		$param['cid'] = $cid;
		$param['thid'] = $thid;
		$param['course_data'] = $this->course_data;
		$param['theme_data'] = $theme_datas;
		$param['is_attended'] = $is_attended;
		$param['materials_data'] = $this->makeLinkData($materials_datas);
		$param['task_data'] = $this->makeResultData($taskdatas);
		$param['summernote_lang_file'] = $this->config->item('SUMMERNOTE_LANG_FILE');
		$param['course_auth'] = $this->getCourseAuth();
		$param['script'] = $this->createJavaScript();
		if (!is_null($this->error_message)) {
			$param['error_msg'] = $this->error_message;
		}

		$this->load->view('common_header');			// 共通ヘッダー部
		$this->load->view('themeedit', $param);		// コンテンツ部
		$this->load->view('common_footer');			// 共通フッター部
	}

	/**
	 * 解答データ生成
	 *
	 * @param array $list_datas 課題情報
	 * @return array 課題情報
	 */
	private function makeResultData($list_datas) {
		if (is_null($list_datas)) {
			return null;
		}

		$retdatas = array();
		foreach ($list_datas as $task) {
			$task['has_result'] = $this->task_result_model->hasResultOfTask($task['id']);
			switch ($task['task_type']) {
				case $this->config->item('TASK_TYPE_SINGLE_SELECTION'):
				case $this->config->item('TASK_TYPE_MULTIPLE_CHOICE'):
				case $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION'):
				case $this->config->item('TASK_TYPE_SURVEY_SINGLE_SELECTION'):
				case $this->config->item('TASK_TYPE_SURVEY_MULTIPLE_CHOICE'):
					$_input_type = 'radio';
					if ($task['task_type'] == $this->config->item('TASK_TYPE_MULTIPLE_CHOICE')
					 || $task['task_type'] == $this->config->item('TASK_TYPE_SURVEY_MULTIPLE_CHOICE')) {
						$_input_type = 'checkbox';
					}
					$_choices = $this->commonlib->nl2Array($task['choices']);
					$_choices_tmp = "";
					for ($_cnt = 1; $_cnt <= count($_choices); $_cnt++) {
						$_choices[$_cnt-1] = htmlspecialchars($_choices[$_cnt-1]);
						if ($this->is_quiz
						 && $task['task_type'] == $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')
						 && !is_null($task['quiz_answer'])
						 && $_cnt == $task['quiz_answer']) {
							$_choices[$_cnt-1] = '<span class="correct">'.$_choices[$_cnt-1].'</span>';
						}
						$_choices_tmp .= "<input type=\"$_input_type\" value=\"$_cnt\" disabled>{$_choices[$_cnt-1]}<br>\n";
					}
					$task['answer'] = $_choices_tmp;
					break;
				case $this->config->item('TASK_TYPE_TEXT'):
					$task['answer'] = '<textarea class="form-control" rows="3" disabled></textarea>';
					break;
				case $this->config->item('TASK_TYPE_FILE'):
					$task['answer'] = '<button type="button" class="btn btn-primary" disabled>'.$this->lang->line('themeedit_task_item_btn_file').'</button>';
					break;
			}
			array_push($retdatas, $task);
		}
		return $retdatas;
	}

	/**
	 * リンク用データの生成
	 *
	 * @param array $datas 教材ファイル格納先PATH
	 * @return array リンク用データ
	 */
	private function makeLinkData($datas) {

		// 言語ファイル読み込み
		$this->lang->load('theme_lang');

		$retdatas = array();
		if ($datas != null) {
			$cid = $this->input->get_post('cid', true);
			foreach ($datas as $r) {

				if ($r['material_type'] == $this->config->item('MATERIAL_TYPE_MOVIE')) {
					$r += array('link_path' => $this->commonlib->baseUrl()."materialvideo?mid=".$r['id']."&cid=".$cid."&prev=1");
				} else if ($r['material_type'] != $this->config->item('MATERIAL_TYPE_EXTERNAL')) {
					$r += array('link_path' => $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_MATERIALS')."&id=".$r['id']."&cid=".$cid);
				} else {
					$r += array('link_path' => $r['external_material_path']);
				}
				array_push($retdatas, $r);
			}
		}

		return $retdatas;
	}

	/**
	 * テーマ公開・休止
	 */
	public function open() {

		$cid = $this->input->post('cid', true);
		$thid = $this->input->post('thid', true);
		$open = $this->input->post('open', true);
		if ($open == $this->config->item('THEME_OPEN')) {
			$open = $this->config->item('THEME_CLOSE');
		} else {
			$open = $this->config->item('THEME_OPEN');
		}

		// 公開の場合
		if ($open == $this->config->item('THEME_OPEN')) {
			// 課題を作成する取得
			$_evause = $this->theme->getEvaluationUse($thid);
			if ($_evause == 1) {
				// 「課題を作成する」がYESの場合
				// 課題件数取得
				$_taskcnt = $this->task->getTaskCount($thid);
				// 課題が作成されて無い場合は公開させない
				if ($_taskcnt == 0) {
					redirect($this->commonlib->baseUrl().'themeedit?cid='.$cid.'&thid='.$thid.'&err='.self::ERR_KIND_OPEN_HASNT_TASK, 'location');
					exit;
				}
			}
		}

		$this->theme->updateOpen($thid, $open);
		redirect($this->commonlib->baseUrl().'themeedit?cid='.$cid.'&thid='.$thid, 'location');
	}

	/**
	 * テーマ公開終了
	 */
	public function end() {

		$cid = $this->input->post('cid', true);
		$thid = $this->input->post('thid', true);

		$this->theme->updateOpen($thid, $this->config->item('THEME_PUBLIC_END'));
		redirect($this->commonlib->baseUrl().'themeedit?cid='.$cid.'&thid='.$thid, 'location');
	}

	/**
	 * テーマ登録・更新
	 */
	public function themeedit() {

		$cid = $this->input->post('cid', true);
		$thid = $this->input->post('thid', true);
		$title = $this->input->post('title', true);
		$goal = $this->input->post('goal', true);
		$themedate = $this->input->post('themedate', true);
		// WYSIWYG対応のためCodeigniterではXSS対策は実施せずに、専用関数で除去する。
		$description = $this->commonlib->removeXssForWYSIWYG($this->input->post('description'));
		$time_limit = $this->input->post('time_limit', true);
		$evaluation_use = 0;
		if (!is_null($this->input->post("evaluation_use", true))) {
			$evaluation_use = 1;
		}
		$quiz = 0;
		if (!is_null($this->input->post("quiz", true))) {
			$quiz = 1;
		}

		$this->form_validation->set_rules('title', $this->lang->line('themeedit_input_theme'), 'trim|required|max_length['.$this->themetitle_max.']');
		$this->form_validation->set_rules('goal', $this->lang->line('themeedit_input_goal'), 'trim|required|max_length['.$this->themegoal_max.']');
		$this->form_validation->set_rules('description', $this->lang->line('themeedit_input_description'), 'trim|required');
		if ($this->is_schedule_use) {
			// 講座の期間が設定されている場合は、日付および、期限の未入力チェック及び、日付が講座期間内かチェックする
			$this->form_validation->set_rules('themedate', $this->lang->line('themeedit_input_date'), 'trim|required|callback_check_themedate');
			$this->form_validation->set_rules('time_limit', $this->lang->line('themeedit_input_time_limit'), 'trim|required|callback_check_time_limit');
		}

		if ($this->form_validation->run()) {

			// 新規投稿
			$_cmp_flg = '1';
			if ($thid == null or $thid == "") {
				$thid = $this->theme->registTheme($cid, $title, $goal, $themedate, $description, $time_limit, $evaluation_use, $quiz);
			} else {
				$this->theme->updateTheme($thid, $title, $goal, $themedate, $description, $time_limit, $evaluation_use, $quiz);
				$_cmp_flg = '2';
			}

			redirect($this->commonlib->baseUrl().'themeedit?cid='.$cid.'&thid='.$thid.'&cmp='.$_cmp_flg, 'location');
		} else {
			$this->displayView();
		}
	}

	/**
	 * 日付チェック
	 *
	 * @return boolean true：エラーなし / false：エラーあり
	 */
	public function check_themedate()
	{
		$_startdate = new DateTime($this->course_start_date);
		$_enddate = new DateTime($this->course_end_date);
		$_theme_date = new DateTime($this->input->post('themedate', true));
		if ($_theme_date < $_startdate) {
			$this->form_validation->set_message("check_themedate", $this->lang->line('themeedit_input_err_msg_date_startdate'));
			return false;
		}
		if ($_theme_date > $_enddate) {
			$this->form_validation->set_message("check_themedate", $this->lang->line('themeedit_input_err_msg_date_enddate'));
			return false;
		}
		return true;
	}

	/**
	 * 期限チェック
	 *
	 * @return boolean true：エラーなし / false：エラーあり
	 */
	public function check_time_limit()
	{
		$_theme_date = new DateTime($this->input->post('themedate', true));
		$_time_limit = new DateTime($this->input->post('time_limit', true));
		if ($_theme_date > $_time_limit) {
			$this->form_validation->set_message("check_time_limit", $this->lang->line('themeedit_input_err_msg_timelimit_past_enddate'));
			return false;
		}

		$_startdate = new DateTime($this->course_start_date);
		$_enddate = new DateTime($this->course_end_date);
		if ($_time_limit < $_startdate) {
			$this->form_validation->set_message("check_time_limit", $this->lang->line('themeedit_input_err_msg_timelimit_startdate'));
			return false;
		}
		if ($_time_limit > $_enddate) {
			$this->form_validation->set_message("check_time_limit", $this->lang->line('themeedit_input_err_msg_timelimit_enddate'));
			return false;
		}
		return true;
	}

	/**
	 * テーマ説明画像アップロード
	 */
	public function descriptimg()
	{
		$_cid = $this->input->get_post('cid', true);
		if (isset($_FILES["descriptimg"])
		&& is_uploaded_file($_FILES["descriptimg"]["tmp_name"])) {
				// 画像アップロードパス取得
			$_upload_path = $this->config->item('THEMEEDIT_DESCRIPTION_IMAGE_UPLOAD_PATH');
			if (!file_exists($_upload_path)) {
				mkdir($_upload_path, 0777, true);
			}
			// 画像ライブラリロード
			$_upload_config = array(
					'max_size' => $this->config->item('THEMEEDIT_DESCRIPTION_UPLOAD_MAX_SIZE'),
					'encrypt_name' => true,
					'upload_path' => $_upload_path,
					'allowed_types' => implode("|", $this->config->item('THEMEEDIT_DESCRIPTION_IMAGE_UPLOAD_EXTENSIONS'))
			);
			$this->load->library('upload', $_upload_config);

			// 画像アップロード
			if ($this->upload->do_upload('descriptimg')) {
				$file_info = $this->upload->data();
				// 保存先ディレクトリ生成
				// 保存先パス＋講座情報テーブルID（新規の時はテーマIDが無いので講座情報テーブルIDのみを利用する）
				$_save_path = $this->config->item('THEMEEDIT_DESCRIPTION_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_cid;
				if (!file_exists($_save_path)) {
					mkdir($_save_path, 0777, true);
				}
				// 保存先パス＋_yyyyMMddHHmmssSSS＋拡張子
				$_filename = $this->commonlib->getTimeWithMillSecond().$file_info['file_ext'];
				$_save_file_path = $_save_path.DIRECTORY_SEPARATOR.$_filename;

				// リサイズ後の画像サイズ算出
				// 画像処理ライブラリロード
				$this->load->library('imagelib');
				$_resize_size = $this->imagelib->calResizeSize($file_info['full_path'], $this->config->item('THEMEEDIT_DESCRIPTION_IMAGE_MAX_LENGTH'));

				// 画像リサイズ処理
				if (!is_null($_resize_size)) {
					$_image_config = array(
						'source_image' => $file_info['full_path'],
						'new_image' => $_save_file_path,
						'width' => $_resize_size[0],
						'height' => $_resize_size[1]
					);
					// CodeIgniterの画像処理ライブラリロード
					$this->load->library('image_lib', $_image_config);
					// リサイズ実行
					$this->image_lib->resize();
					// リサイズ元のアップロード画像削除
					unlink($file_info['full_path']);
				} else {
					// リサイズしなかった場合はファイルを保存先に移動
					rename($file_info['full_path'], $_save_file_path);
				}
				echo $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_THEME')."&cid=".$_cid."&fl=".$_filename;
				exit;
			} else {
				// 画像アップロードエラー
				// エラーメッセージ生成
				header("HTTP/1.0 404 Not Found");
				echo $this->upload->display_errors('<div class="alert alert-danger" role="alert">', '</div>');
				exit;
			}
		}
	}

	/**
	 * テーマ削除処理
	 */
	public function themedelete() {

		$cid = $this->input->post('cid', true);
		$thid = $this->input->post('thid', true);

		// テーマ単位で教材再生時間保持情報削除
		$this->load->model('Materials_Play_Model', 'materials_play_model', true);
		$this->materials_play_model->deleteByThemeId($thid);

		// 教材情報削除
		// テーマ単位で教材ファイル削除
		$this->commonlib->removeDirs($this->config->item('THEME_MATERIAL_SAVE_PATH').DIRECTORY_SEPARATOR.$cid.DIRECTORY_SEPARATOR.$thid);
		// テーマ単位で教材データ削除
		$this->materials_model->deleteByThemeId($thid);

		// 課題解答情報モデル
		$this->load->model('Task_Result_Model', 'taskresult', TRUE);
		// テーマ単位で課題解答ファイル削除
		$this->commonlib->removeDirs($this->config->item('TASK_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$cid.DIRECTORY_SEPARATOR.$thid);
		// テーマ単位で課題解答データ削除
		$this->taskresult->deleteByThemeId($thid);

		// 課題フィードバックモデル
		$this->load->model('Task_Feedback_Model', 'task_feedback', TRUE);
		$this->task_feedback->deleteByThemeId($thid);

		// 課題削除
		// テーマ単位で課題ファイル削除
		$this->commonlib->removeDirs($this->config->item('TASKEDIT_DESCRIPTION_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$cid.DIRECTORY_SEPARATOR.$thid);
		// テーマ単位で課題データ削除
		$this->task->deleteByThemeId($thid);

		// テーマ削除
		$this->theme->deleteByThemeId($thid);

		redirect($this->commonlib->baseUrl().'courseedit?cid='.$cid, 'location');
	}

	/**
	 * 課題削除処理
	 */
	public function taskdelete() {

		$cid = $this->input->post('cid', true);
		$thid = $this->input->post('thid', true);
		$taid = $this->input->post('taid', true);

		// 課題削除
		$this->task->deleteByTaskId($taid);

		redirect($this->commonlib->baseUrl().'themeedit?cid='.$cid.'&thid='.$thid, 'location');
	}

	/**
	 * 教材削除処理
	 */
	public function materialdelete() {

		$cid = $this->input->post('cid', true);
		$thid = $this->input->post('thid', true);
		$mid = $this->input->post('mid', true);

		// 教材に紐付く教材再生時間保持情報削除
		$this->load->model('Materials_Play_Model', 'materials_play_model', true);
		$this->materials_play_model->deleteByMaterialId($mid);

		// 教材情報取得
		$material_data = $this->materials_model->getMaterialInfo($mid);
		if (is_null($material_data)) {
			return;
		}
		$material_data = $material_data[0];
		if ($material_data['material_type'] != $this->config->item('MATERIAL_TYPE_EXTERNAL')) {
			@unlink($this->config->item('THEME_MATERIAL_SAVE_PATH').DIRECTORY_SEPARATOR.$material_data['material_path']);
		}

		$this->materials_model->delete($mid);

		redirect($this->commonlib->baseUrl().'themeedit?cid='.$cid.'&thid='.$thid, 'location');
	}

	/**
	 * 教材登録処理
	 */
	public function materialregist() {

		$cid = $this->input->post('cid', true);
		$thid = $this->input->post('thid', true);
		$description = $this->input->post('description', true);
		$resource = $this->input->post('resource', true);

		$this->form_validation->set_rules('description', $this->lang->line('themeedit_material_description'), 'trim|required|max_length['.$this->materialdescription_max.']');
		$this->form_validation->set_rules('resource', $this->lang->line('themeedit_material_resource'), 'trim|required|max_length['.$this->materialresource_max.']');

		if ($this->form_validation->run()) {

			$materialType = 0;
			$playEnd = 0;
			$external = null;
			$_save_file = null;
			if ($this->checkExternalType($resource) == false) {

				// 教材アップロード処理
				$_save_file = $this->material_upload($cid, $thid);

				if ($_save_file == null) {
					$this->displayView();
					return;
				}

				// ファイルの情報取得
				$file_info = pathinfo($_save_file);
				// ファイル拡張子
				$ext = strtolower($file_info['extension']);

				$extlist = $this->config->item('THEME_MATERIAL_EXTENSION_LIST');
				$typelist = $this->config->item('THEME_MATERIAL_TYPE_LIST');
				$idx = 0;
				$rflg = false;
				for ($idx = 0; $idx < count($extlist); $idx++) {
					if ($ext == $extlist[$idx]) {
						// 教材タイプ設定
						$materialType = $typelist[$idx];
						$rflg = true;
					}
				}

				if ($materialType == $this->config->item('MATERIAL_TYPE_MOVIE')
				 && $this->input->post('play_end', true)) {
					// 動画の場合で、「最後まで再生」がYESの場合は、
					// 「最後まで再生フラグ」を立てる
					$playEnd = 1;
				}

			} else {
				$external = $resource;
			}

			// 教材情報登録
			$this->materials_model->insaertMaterial($cid, $thid, $description, $materialType, $playEnd, $external, $_save_file);

			redirect($this->commonlib->baseUrl().'themeedit?cid='.$cid.'&thid='.$thid, 'location');
		} else {
			$this->displayView();
		}
	}

	/**
	 * 外部ファイル判定処理
	 *
	 * @param string $resource リソース文字列
	 * @return boolean 判定結果 (true：外部リンク / false：外部リンクではない)
	 */
	private function checkExternalType($resource) {

		if (mb_substr($resource, 0, 4, "UTF-8") == "http") {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 教材アップロード処理
	 *
	 * @param int $cid 講座ID
	 * @param int $thid テーマID
	 * @return string|NULL 保存先パス
	 */
	private function material_upload($cid, $thid) {
		// 教材アップロードパス取得
		$_upload_path = $this->config->item('THEMEEDIT_MATERIAL_UPLOAD_PATH');
		if (!file_exists($_upload_path)) {
			mkdir($_upload_path, 0777, true);
		}
		// 教材ライブラリロード
		$_upload_config = array(
				'max_size' => $this->config->item('THEMEEDIT_MATERIAL_UPLOAD_MAX_SIZE'),
				'encrypt_name' => true,
				'upload_path' => $_upload_path,
				'allowed_types' => implode("|", $this->config->item('THEMEEDIT_MATERIAL_UPLOAD_EXTENSIONS'))
		);
		$this->load->library('upload', $_upload_config);

		// 教材アップロード
		if ($this->upload->do_upload('upload_file')) {
			$file_info = $this->upload->data();
			// 保存先ファイル名生成（拡張子無し）
			// 保存先パス＋講座ID＋テーマID
			$_save_path = $this->config->item('THEMEEDIT_MATERIAL_SAVE_PATH');
			if (!file_exists($_save_path)) {
				mkdir($_save_path, 0777, true);
			}
			// 講座IDフォルダ
			$_save_file_path_tmp = $_save_path.DIRECTORY_SEPARATOR.$cid;
			if (!file_exists($_save_file_path_tmp)) {
				mkdir($_save_file_path_tmp, 0777, true);
			}
			// テーマIDフォルダ
			$_save_file_path_tmp = $_save_file_path_tmp.DIRECTORY_SEPARATOR.$thid;
			if (!file_exists($_save_file_path_tmp)) {
				mkdir($_save_file_path_tmp, 0777, true);
			}
			// 保存先パス＋講座ID＋テーマID
			$_move_file_path = $_save_file_path_tmp.DIRECTORY_SEPARATOR.$file_info['file_name'];
			$_save_file_path = DIRECTORY_SEPARATOR.$cid.DIRECTORY_SEPARATOR.$thid.DIRECTORY_SEPARATOR.$file_info['file_name'];

			// ファイルを保存先に移動
			rename($file_info['full_path'], $_move_file_path);

			return $_save_file_path;
		} else {
			// 教材アップロードエラー
			// エラーメッセージ生成
			$this->error_message = $this->upload->display_errors('<div class="alert alert-danger" role="alert">', '</div>');
		}
		return null;
	}

	/**
	 * 課題のリセット処理
	 *
	 * 全ての課題解答データ、課題解答ファイル、課題の提出データ、フィードバックデータを削除する
	 */
	public function resettaskresult()
	{
		$cid = $this->input->post('cid', true);
		$thid = $this->input->post('thid', true);

		// 課題解答情報モデル
		$this->load->model('Task_Result_Model', 'task_result_model', true);
		// テーマ単位で課題解答ファイル削除
		$this->commonlib->removeDirs($this->config->item('TASK_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$cid.DIRECTORY_SEPARATOR.$thid);
		// テーマ単位で課題解答データ削除
		$this->task_result_model->deleteByThemeId($thid);

		// テーマに紐付く教材再生時間保持情報削除
		$this->load->model('Materials_Play_Model', 'materials_play_model', true);
		$this->materials_play_model->deleteByThemeId($thid);

		// 課題フィードバックモデル
		$this->load->model('Task_Feedback_Model', 'task_feedback_model', true);
		$this->task_feedback_model->deleteByThemeId($thid);

		redirect($this->commonlib->baseUrl().'themeedit?cid='.$cid.'&thid='.$thid, 'location');
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		$_evaluation_use = $this->evaluation_use ? 'true' : 'false';
		$_arrayTmpExt = $this->config->item('THEMEEDIT_MATERIAL_UPLOAD_MOVIE_EXTENSIONS_FOR_JAVASCRIPT');
		$_material_upload_extensions = implode(",", $_arrayTmpExt);

		$_script = <<<EOT
<script>
	var THEME_OPEN = {$this->config->item('THEME_OPEN')};
	var THEME_PUBLIC_END = {$this->config->item('THEME_PUBLIC_END')};
	var summernoteLang = "{$this->config->item('SUMMERNOTE_LANG')}";
	var datepickerLang = "{$this->config->item('DATEPICKER_LANG')}";
	var titleMax = {$this->themetitle_max};
	var goalMax = {$this->themegoal_max};
	var materialDescriptionMax = {$this->materialdescription_max};
	var materialResourceMax = {$this->materialresource_max};
	var titleRequired = "{$this->lang->line('themeedit_input_err_msg_theme')}";
	var goalRequired = "{$this->lang->line('themeedit_input_err_msg_goal')}";
	var dateRequired = "{$this->lang->line('themeedit_input_err_msg_date')}";
	var descriptionRequired = "{$this->lang->line('themeedit_input_err_msg_description')}";
	var timelimitRequired = "{$this->lang->line('themeedit_input_err_msg_timelimit')}";
	var evaluationUse = {$_evaluation_use};
	var materialUploadExtensions = [{$_material_upload_extensions}];
	var errMsgTitleMaxlength = "{$this->lang->line('themeedit_script_err_msg_theme_maxlength')}";
	var errMsgGoalMaxlength = "{$this->lang->line('themeedit_script_err_msg_goal_maxlength')}";
	var errMsgMaterialDescriptionMaxlength = "{$this->lang->line('themeedit_material_script_err_msg_description_maxlength')}";
	var errMsgMaterialDescriptionRequired = "{$this->lang->line('themeedit_material_script_err_msg_description_required')}"
	var errMsgMaterialResourceMaxlength = "{$this->lang->line('themeedit_material_script_err_msg_resource_maxlength')}";
	var errMsgMaterialResourceRequired = "{$this->lang->line('themeedit_material_script_err_msg_resource_required')}"
</script>
EOT;
		return $_script;
	}

}
