<?php
/*
 * ファイル表示コントローラー
 *
 * tblパラメータにて指定された、テーブル判定キーを元に、データ取得対象のテーブルを判定し、
 * idパラメータにて指定された、データ取得対象のテーブルのIDから、ファイルパスを取得して、
 * 画面に出力を行う。
 *
 * URL生成例：
 *   $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_TASK_RESULT')."&id=".$_taid;
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Displayfile extends EX_Controller {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		// ベースコントローラ初期化
		$this->initialize($this->config->item('CONTROLLER_KIND_DISPLAY_FILE'));
		// 教材動画再生処理において、
		// 動画出力中は当コントローラに継続アクセスされるために、
		// セッションファイルがロックされてしまい、
		// ajaxからのアクセスがタイムアウトしてしまうため、
		// ここでセッションを終了しておく
		session_write_close();
		// 実行時間を制限しない
		set_time_limit(0);
		// 最大使用メモリを上書き
		ini_set('memory_limit', $this->config->item('DISPLAYFILE_MEMORY_LIMIT'));
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		// ファイルパス取得
		$_tbl = $this->input->get('tbl', true); // テーブル判定キー取得
		$_id = $this->input->get('id', true); // データ取得対象のテーブルのID取得
		$_cid = $this->input->get('cid', true); // 講座情報テーブルID取得
		$_file_path = '';
		switch ($_tbl) {
		case $this->config->item('TABLE_KEY_USER'): // ユーザーテーブル
			$this->load->model("Profile_Model", 'profile_model', true);
			$_picture_file = $this->profile_model->getPictureFile($_id);
			if (!is_null($_picture_file)) {
				$_file_path = $this->config->item('PROFILE_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_picture_file;
			} else {
				$_img_url_prefix = '';
				if (strlen($this->config->item('URL_SUBDIR')) > 0) {
					$_img_url_prefix = $this->config->item('URL_SUBDIR').'/';
				}
				$_file_path = $_img_url_prefix.$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
			}
			break;
		case $this->config->item('TABLE_KEY_NOTICE'); // お知らせテーブル
			$_filename = $this->input->get('fl', true); // ファイル名
			$_file_path = $this->config->item('STAFFHOME_CONTENT_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_filename;
			break;
		case $this->config->item('TABLE_KEY_FORUM'); // フォーラムテーブル
			$this->load->model("Forum_Model", 'forum_model', true);
			$_result_file = $this->forum_model->getImageFile($_id);
			if (!is_null($_result_file)) {
				$_file_path = $this->config->item('FORUM_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR .$_result_file;
			}
			break;
		case $this->config->item('TABLE_KEY_CHAT'); // チャットテーブル
			$_course_authority = $this->getCourseAuthority($_cid);
			if (is_null($_course_authority)) {
				// 講座の権限が無い場合は表示しない
				return;
			}
			$this->load->model("Chat_Model", 'chat_model', true);
			$_result_file = $this->chat_model->getImageFile($_id);
			if (!is_null($_result_file)) {
				$_file_path = $this->config->item('CHAT_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR .$_result_file;
			}
			break;
		case $this->config->item('TABLE_KEY_DIARY'); // 日記テーブル
			$this->load->model("Diary_Model", 'diary_model', true);
			$_result_file = $this->diary_model->getImageFile($_id);
			if (!is_null($_result_file)) {
				$_file_path = $this->config->item('DIARY_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR .$_result_file;
			}
			break;
		case $this->config->item('TABLE_KEY_COURSE'); // 講座情報テーブル
			$_filename = $this->input->get('fl', true); // ファイル名
			$_file_path = $this->config->item('COURSEEDIT_DESCRIPT_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_id.DIRECTORY_SEPARATOR.$_filename;
			break;
		case $this->config->item('TABLE_KEY_THEME'); // テーマテーブル
			$_filename = $this->input->get('fl', true); // ファイル名
			$_file_path = $this->config->item('THEMEEDIT_DESCRIPTION_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_cid.DIRECTORY_SEPARATOR.$_filename;
			break;
		case $this->config->item('TABLE_KEY_MATERIALS'): // 教材テーブル
			$_course_authority = $this->getCourseAuthority($_cid);
			if (is_null($_course_authority)) {
				// 講座の権限が無い場合は表示しない
				return;
			}
			$this->load->model("Materials_Model", 'materials_model', true);
			$_result_file = $this->materials_model->getMaterialPathWithCid($_id, $_cid);
			if (!is_null($_result_file)) {
				$_file_path = $this->config->item('THEME_MATERIAL_SAVE_PATH').$_result_file;
			}
			break;
		case $this->config->item('TABLE_KEY_TASK'); // 課題情報テーブル
			$_thid = $this->input->get('thid', true); // テーマID
			$_filename = $this->input->get('fl', true); // ファイル名
			$_file_path = $this->config->item('TASKEDIT_DESCRIPTION_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_cid.DIRECTORY_SEPARATOR.$_thid.DIRECTORY_SEPARATOR.$_filename;
			break;
		case $this->config->item('TABLE_KEY_TASK_RESULT'): // 課題解答情報テーブル
			$_course_authority = $this->getCourseAuthority($_cid);
			// 画像ファイルパス取得
			$this->load->model("Task_Result_Model", 'task_result_model', true);
			$_result_file = $this->task_result_model->getFilePath($_id, $_cid, $_course_authority);
			if (!is_null($_result_file)) {
				$_file_path = $this->config->item('TASK_IMAGE_SAVE_PATH').$_result_file;
			}
			break;
		case $this->config->item('TABLE_KEY_GENRE'): // ジャンルテーブル
			$this->load->model("Genre_Model", 'genre_model', true);
			$_icon = $this->genre_model->getIconFile($_id);
			if (is_null($_icon)) {
				$_icon = $this->config->item('GENREMANAGER_DEFAULT_IMAGE_FILE');
			}
			$_file_path = $this->config->item('GENREMANAGER_IMAGE_SAVE_PATH').'/'.$_icon;
			break;
		case $this->config->item('TABLE_KEY_SETTING'); // 設定テーブル
			if (strcmp($this->input->get('kind', true), 'intro') !== 0) {
				exit;
			}
			$_filename = $this->input->get('fl', true); // ファイル名
			$_file_path = $this->config->item('SETTINGS_INTRODUCTION_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_filename;
			break;
		}

		// 画面出力
		if (file_exists($_file_path)) {
			// mimeタイプ取得
			$this->load->helper('file');
			$_mime_type = get_mime_by_extension($_file_path);

			if (stripos($_mime_type , 'video') === 0) {
				// mimeタイプが“video”から始まる場合は動画出力
				$this->responseVideo($_file_path, $_mime_type);
			} else {
				// 動画以外はそのまま出力
				header('Content-Type: '.$_mime_type);
				readfile($_file_path);
				exit;
			}
		}
	}

	/**
	 * 講座での権限取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return NULL|int 講座での権限
	 */
	private function getCourseAuthority($cid)
	{
		// この講座での権限取得
		$this->load->model("Students_Model", 'students_model', true);
		if (!$cid) {
			return null;
		}
		$_authority_info = $this->session->userdata('authority');
		return $this->students_model->getCourseAuth($cid, $_authority_info[0]->id);
	}

	/**
	 * 動画出力
	 *
	 * @param string $file_path ファイルパス
	 * @param string $mime_type MIMEタイプ
	 */
	private function responseVideo($file_path, $mime_type)
	{
		if (isset($_SERVER['HTTP_RANGE'])) {
			// ブラウザから“HTTP_RANGE”(部分リクエスト)が要求された場合
			$_fp = @fopen($file_path, 'rb');
			$_size   = filesize($file_path);
			$_length = $_size;
			$_start = 0;
			$_end = $_size - 1;

			header('Content-type:'.$mime_type);
			header("Accept-Ranges: 0-$_length");

			// 要求に基づき、開始位置、終了位置を判定する
			$_tmp_start = $_start;
			$_tmp_end = $_end;
			// “HTTP_RANGE”を$_rangeに展開
			// （“bytes=xxxxxx”の形式で入っているのでxxxxxxを抽出）
			list(, $_range) = explode('=', $_SERVER['HTTP_RANGE'], 2);
			if (strpos($_range, ',') !== false) {
				// “,”が入っている場合
				// 要求されたRangeを満たせない旨返答
				header('HTTP/1.1 416 Requested Range Not Satisfiable');
				header("Content-Range: bytes $_start-$_end/$_size");
				exit;
			}
			if ($_range[0] == '-') {
				// １文字目が“-”の場合
				// “-”以降の値を開始位置に設定
				$_tmp_start = $_size - substr($_range, 1);
			} else {
				// “bytes=開始バイト-終了バイト”の形式で入っている場合
				$_range  = explode('-', $_range);
				$_tmp_start = $_range[0];
				// 終了バイトが入っていれば終了位置に設定し、入ってなければファイルサイズを終了位置に設定
				$_tmp_end   = (isset($_range[1]) && is_numeric($_range[1])) ? $_range[1] : $_size;
			}
			// 判定した終了位置がファイルの終了位置より大きい場合はファイルの終了位置を設定
			$_tmp_end = ($_tmp_end > $_end) ? $_end : $_tmp_end;
			if ($_tmp_start > $_tmp_end || $_tmp_start > $_size - 1 || $_tmp_end >= $_size) {
				// 判定した開始位置が終了位置より大きい場合、
				// または
				// 判定した開始位置がファイルの終了位置より大きい場合
				// または
				// 判定した終了位置がファイルサイズの値以上の場合
				// 要求されたRangeを満たせない旨返答
				header('HTTP/1.1 416 Requested Range Not Satisfiable');
				header("Content-Range: bytes $_start-$_end/$_size");
				exit;
			}

			// 指定されたRangeで動画を出力
			$_start = $_tmp_start;
			$_end = $_tmp_end;
			$_length = $_end - $_start + 1;
			fseek($_fp, $_start);
			header('HTTP/1.1 206 Partial Content');
			header("Content-Range: bytes $_start-$_end/$_size");
			header("Content-Length: $_length");
			$_buffer = 1024 * 8;
			while(!feof($_fp) && ($_p = ftell($_fp)) <= $_end) {
				if ($_p + $_buffer > $_end) {
					$_buffer = $_end - $_p + 1;
				}
				echo fread($_fp, $_buffer);
				flush();
			}
			fclose($_fp);
			exit;
		} else {
			// ブラウザから“HTTP_RANGE”(部分リクエスト)の要求が無い場合（初回のリクエスト）
			// “HTTP_RANGE”に対応している旨および、
			// 動画、動画サイズを返答
			header("Accept-Ranges: bytes");
			header("Content-type: ".$mime_type);
			header("Content-length: " . filesize($file_path));
			header("Etag: \"" . md5($_SERVER["REQUEST_URI"]) . filesize($file_path) . "\"");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s", filemtime($file_path)) . " GMT");
			readfile($file_path);
			exit;
		}
	}

}
