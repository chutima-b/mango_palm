<?php
/*
 * 課題管理画面コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Taskmanager extends EX_Controller {
	/** 講座ID. */
	private $cid = 0;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_TASKMANAGER')); // 課題管理画面

		$this->load->model("Taskmanage_Model", 'taskmanage_model', true);
		$this->lang->load('taskmanager_lang');
		$this->setPageTitle($this->lang->line('taskmanager_header_pagetitle'));

		// パラメータ取得
		$this->cid = $this->input->get('cid', true);

		// 戻り画面URL設定
		$this->setBackUrl($this->homeUrl());
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		// パラメータ取得
		$this->cid = $this->input->get('cid', true);

		// 講座名取得
		$this->load->model("Course_Model", 'course_model', true);
		$_course_name = $this->course_model->getCourseName($this->cid);

		// 受講者数取得
		$this->load->model("Students_Model", 'students_model', true);
		$_participants = $this->students_model->getCountOfCourse($this->cid);

		// 課題管理情報付テーマ一覧取得
		$_theme_list = $this->getThemeListWithTaskmanageData();

		// ビューのデータに設定
		$_view_data['cid'] = $this->cid;
		$_view_data['coursename'] = $_course_name;
		$_view_data['participants'] = $_participants;
		$_view_data['theme_list'] = $_theme_list;

		// ビュー表示
		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('taskmanager', $_view_data); // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * 課題管理情報付テーマ一覧取得
	 *
	 * @return object 課題管理情報付テーマ一覧
	 */
	private function getThemeListWithTaskmanageData()
	{
		$this->load->model("Theme_Model", 'theme_model', true);
		$this->load->model("Task_Model", 'task_model', true);
		$_theme_list = $this->theme_model->getThemeListForTaskmanager($this->cid);
		if (!is_null($_theme_list)) {
			$_today = date("Y/m/d");
			foreach ($_theme_list as $_theme) {
				$_theme->label_style = ''; // デフォルトのパネルは無し（フィードバックしなければいけない受講者がいない）
				$_theme->label_message = '';
				// 課題管理情報リスト取得
				$_theme->taskmanage_list = $this->taskmanage_model->getList($this->cid, $_theme->id);
				if (!is_null($_theme->taskmanage_list)) {
					if ($_theme->evaluation_use == $this->config->item('EVALUATION_USE_FLAG_ON')) {
						// 「課題を作成する」がYESの場合
						$_is_all_submit = true; // 全件提出済みフラグ
						$_is_all_feedback = true; // 全件フィードバック済みフラグ
						$_has_un_feedback = false; // 提出済みの課題に対しての未フィードバックありフラグ
						$_is_time_limit = false; // 期限切れフラグ
						foreach ($_theme->taskmanage_list as $_students) {
							// パネル表示設定
							if (strcmp($_students->submit_at, '-') != 0
							 && strcmp($_students->feedback_at, '-') != 0) {
								// 提出された課題に対してフィードバックされていれば何もしない
								continue;
							}
							if (strcmp($_students->submit_at, '-') === 0) {
								// 一件でも提出されてなければフラグを落とす
								$_is_all_submit = false;
								// 提出されていなければ当然フィードバックもされていないので
								// フラグを落とす
								$_is_all_feedback = false;
								// 未提出のユーザーがいる場合で、提出期限の場合は期限切れフラグを立てる
								if (!is_null($_theme->time_limit) && strtotime($_theme->time_limit) < strtotime($_today)) {
									$_is_time_limit = true;
									// 未提出で期限切れの場合は、期限切れパネルにするため
									// 以降のパネル判定処理は行わない
									break;
								}
							} else if (strcmp($_students->feedback_at, '-') === 0) {
								// 課題は提出済みだが、フィードバックされていない場合はフラグを立てる
								$_has_un_feedback = true;
								// 一件でもフィードバックされてなければフラグを落とす
								$_is_all_feedback = false;
							}
						}
						if ($_is_all_submit && $_is_all_feedback) {
							// 全件提出済みかつ、全件フィードバックの場合
							// label-primary（受講済）
							$_theme->label_style = 'label-primary';
							$_theme->label_message = $this->lang->line('taskmanager_label_msg_attended');
						}
						if ($_is_time_limit) {
							// 未提出のユーザーがいる場合で、提出期限が切れた場合は label-danger（期限切れ）
							$_theme->label_style = 'label-danger';
							$_theme->label_message = $this->lang->line('taskmanager_label_msg_danger');
						}
						if ($_has_un_feedback) {
							// 提出済みの課題に対する、
							// フィードバック未送信のユーザーがいる場合は label-warning （フィードバック）
							$_theme->label_style = 'label-warning';
							$_theme->label_message = $this->lang->line('taskmanager_label_msg_warning');
						}
					} else {
						// 「課題を作成する」がNOの場合
						$_is_all_submit = true; // 全件提出済みフラグ
						$_is_time_limit = false; // 期限切れフラグ
						foreach ($_theme->taskmanage_list as $_students) {
							// パネル表示設定
							if (strcmp($_students->submit_at, '-') === 0) {
								// 一件でも提出されてなければフラグを落とす
								$_is_all_submit = false;
								// 未提出のユーザーがいる場合で、提出期限の場合は期限切れフラグを立てる
								if (!is_null($_theme->time_limit) && strtotime($_theme->time_limit) < strtotime($_today)) {
									$_is_time_limit = true;
									// 未提出で期限切れの場合は、期限切れパネルにするため
									// 以降のパネル判定処理は行わない
									break;
								}
							}
						}
						if ($_is_all_submit) {
							// 全件提出済みの場合
							// label-primary（受講済）
							$_theme->label_style = 'label-primary';
							$_theme->label_message = $this->lang->line('taskmanager_label_msg_attended');
						}
						if ($_is_time_limit) {
							// 未提出のユーザーがいる場合で、提出期限が切れた場合は label-danger（期限切れ）
							$_theme->label_style = 'label-danger';
							$_theme->label_message = $this->lang->line('taskmanager_label_msg_danger');
						}
					}
				}
			}
		}
		return $_theme_list;
	}

}
