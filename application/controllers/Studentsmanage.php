<?php
/*
 * 受講者管理画面コントローラー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Studentsmanage extends EX_Controller {

	/** 講座情報テーブルID. */
	private $cid = null;
	/** 現在のページ. */
	private $page = 0;
	/** 受講者リスト表示件数. */
	private $students_list_count = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_STUDENTSMANAGE')); // 受講者管理画面
		$this->lang->load('students_manage_lang');
		$this->setPageTitle($this->lang->line('students_manage_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css =<<<EOT
<link href="{$_base_url}css/slms_loading.css?v=1.0.0" rel="stylesheet">
<link href="{$_base_url}css/studentsmanage.css?v=1.0.1" rel="stylesheet">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}js/slms_loading.js?v=1.0.0"></script>
<script src="{$_base_url}js/studentsmanage.js?v=1.0.1"></script>
EOT;
		$this->addScriptFiles($_add_script);

		// 受講者リスト表示件数
		$this->students_list_count = $this->config->item('STUDENTS_MANAGER_DISP_COUNT');

		// パラメータ取得
		$this->cid = $this->input->get_post('cid', true);
		$this->page = $this->input->get_post('page', true);
		if (is_null($this->page) || !is_numeric($this->page) || $this->page < 1) {
			$this->page = 1;
		}
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		// 講座情報モデル
		$this->load->model('Course_Model', 'course', TRUE);

		$course_data = $this->course->getCourseInfo($this->cid);

		// 受講者情報モデル
		$this->load->model('Students_Model', 'students', TRUE);

		// 総件数取得
		$students_count = $this->students->getStudentsListCount($this->cid);

		// ページデータ作成
		$page_data = $this->makePageData($students_count);

		$students_data = null;
		if (0 < $students_count) {
			// 一覧データ生成
			$students_data = $this->students->getStudentsList($this->cid, $page_data['page'], $this->students_list_count);
		}

		// データ設定
		$param['cid'] = $this->cid;
		$param['auth_students'] = $this->config->item('AUTH_STUDENT');
		$param['course_data'] = $this->makeImagePath($course_data);
		$param['page_data'] = $page_data;
		$param['students_count'] = $students_count;
		$param['students_data'] = $this->makeImagePath($students_data);

		// 戻る画面設定
		$this->setBackUrl($this->commonlib->baseUrl().'staffhome');

		$this->load->view('common_header');           // 共通ヘッダー部
		$this->load->view('students_manage', $param); // コンテンツ部
		$this->load->view('common_footer');           // 共通フッター部
	}

	/**
	 * 画像情報設定処理
	 *
	 * @param array $data 講座情報
	 * @return array 画像情報設定後の講座情報
	 */
	private function makeImagePath($data) {
		$retdatas = array();
		foreach ((array)$data AS $d) {
			$image_path = $d['picture_file'];
			if ($image_path != "") {
				$d['picture_file'] = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=".$d['id'];
			} else {
				$d['picture_file'] = $this->commonlib->baseUrl().$this->config->item('STUDENTS_MANAGER_DEFAULT_IMAGE_URL');
			}
			array_push($retdatas, $d);
		}

		return $retdatas;
	}

	/**
	 * ページデータ生成処理
	 *
	 * @param int $tcnt 総件数
	 * @return array ページデータ
	 */
	private function makePageData($tcnt) {

		// 総ページ数
		$max_page = ceil($tcnt / $this->students_list_count);

		// 解除後対応
		if ($max_page < $this->page) {
			$this->page = $max_page;
		}

		// 前ページ（0は前ページなし）
		$prev = 0;
		if (1 < $this->page) {
			$prev = $this->page - 1;
		}
		// 次ページ（0は次ページなし）
		$next = 0;
		if ($this->page < $max_page) {
			$next = $this->page + 1;
		}

		$_base_page_url = $this->commonlib->baseUrl().'studentsmanage?cid='.$this->cid;

		// 前ページURL
		$_before_page = null;
		if (1 < $this->page) {
			$_before_page = $_base_page_url.'&page='.($this->page - 1);

		}

		// 次ページURL
		$_next_page = null;
		if ($this->page < $max_page) {
			$_next_page = $_base_page_url.'&page='.($this->page + 1);
		}

		// データ設定
		$page_data['before_page'] = $_before_page;
		$page_data['next_page'] = $_next_page;
		$page_data['page'] = $this->page;
		$page_data['max_page'] = $max_page;
		$page_data['prev'] = $prev;
		$page_data['next'] = $next;
		$page_data['script'] = $this->createJavaScript($_before_page, $_next_page);

		return $page_data;
	}

	/**
	 * 受講者解除処理
	 */
	public function release() {

		$stid = $this->input->post('stid', true);

		// 受講者情報モデル
		$this->load->model('Students_Model', 'students', TRUE);

		$this->students->delete($stid);

		redirect($this->commonlib->baseUrl().'/studentsmanage?cid='.$this->cid.'&page='.$this->page, 'location');
	}

	/**
	 * JavaScriptデータ生成
	 *
	 * @param string $before_page 前ページURL
	 * @param string $next_page 次ページURL
	 */
	private function createJavaScript($before_page, $next_page)
	{
		$_befor_page_url = '';
		if (!is_null($before_page)) {
			$_befor_page_url = $before_page;
		}
		$_next_page_url = '';
		if (!is_null($next_page)) {
			$_next_page_url = $next_page;
		}
		$_script = <<<EOT
<script>
	var beforePageUrl = "{$_befor_page_url}";
	var nextPageUrl = "{$_next_page_url}";
</script>
EOT;
		return $_script;
	}
}
