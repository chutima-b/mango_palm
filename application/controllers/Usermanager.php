<?php
/*
 * システム管理（ユーザー管理）コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Usermanager extends EX_Controller {
	/** ユーザー一覧１ページ当たり表示件数. */
	private $user_list_limit = null;
	/** 現在のページ. */
	private $page = 0;
	/** 検索パラメータ：ユーザーID. */
	private $search_userid = null;
	/** 検索パラメータ：姓. */
	private $search_surname = null;
	/** 検索パラメータ：名. */
	private $search_firstname = null;
	/** ユーザーID最大文字数. */
	private $userid_max = null;
	/** パスワード最小文字数. */
	private $passwd_min = null;
	/** パスワード最大文字数. */
	private $passwd_max = null;
	/** 姓最大文字数. */
	private $surname_max = null;
	/** 名最大文字数. */
	private $firstname_max = null;
	/** ニックネーム最大文字数. */
	private $nickname_max = null;
	/** エラーメッセージ. */
	private $error_message = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_USERMANAGER')); // システム管理（ユーザー管理）画面
		$this->lang->load('usermanager_lang');
		$this->setPageTitle($this->lang->line('usermanager_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link href="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.css?v=1.0.0" rel="stylesheet">
<link rel="stylesheet" href="{$_base_url}css/slms_loading.css?v=1.0.0">
<link rel="stylesheet" href="{$_base_url}css/usermanager.css?v=1.0.2">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}libs/jqueryvalidation/jquery.validate.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.js?v=1.0.0"></script>
<script src="{$_base_url}js/slms_loading.js?v=1.0.0"></script>
<script src="{$_base_url}js/usermanager.js?v=1.0.5"></script>
EOT;
		$this->addScriptFiles($_add_script);

		$this->load->model("User_Model", 'user_model', true);
		$this->user_list_limit = $this->config->item('USERMANAGE_DISP_COUNT');

		// パラメータ取得
		$this->page = $this->input->post('usermanage_page', true);
		if (is_null($this->page) || !is_numeric($this->page)) {
			$this->page = $this->input->get('page', true);
		}
		if (is_null($this->page) || !is_numeric($this->page) || $this->page < 1) {
			$this->page = 1;
		}
		$this->search_userid = $this->input->post('usermanage_userid', true);
		if (is_null($this->search_userid) || $this->search_userid === '') {
			$this->search_userid = urldecode($this->input->get('userid', true));
		}
		$this->search_surname = $this->input->post('usermanage_surname', true);
		if (is_null($this->search_surname) || $this->search_surname === '') {
			$this->search_surname = urldecode($this->input->get('surname', true));
		}
		$this->search_firstname = $this->input->post('usermanage_firstname', true);
		if (is_null($this->search_firstname) || $this->search_firstname === '') {
			$this->search_firstname = urldecode($this->input->get('firstname', true));
		}

		// ユーザーID最大文字数
		$this->userid_max = $this->config->item('USERMANAGE_USERID_MAXLENGTH');
		// パスワード最小文字数
		$this->passwd_min = $this->config->item('USERMANAGE_PASSWD_MINLENGTH');
		// パスワード最大文字数
		$this->passwd_max = $this->config->item('USERMANAGE_PASSWD_MAXLENGTH');
		// 姓最大文字数
		$this->surname_max = $this->config->item('USERMANAGE_SURNAME_MAXLENGTH');
		// 名最大文字数
		$this->firstname_max = $this->config->item('USERMANAGE_FIRSTNAME_MAXLENGTH');
		// ニックネーム最大文字数
		$this->nickname_max = $this->config->item('USERMANAGE_NICKNAME_MAXLENGTH');

		// 戻る画面設定
		if (isset($_SERVER['HTTP_REFERER'])) {
			if (stripos($_SERVER["HTTP_REFERER"], 'usermanager') !== false) {
				$this->setBackUrl($this->homeUrl());
			} else {
				$this->setBackUrl($_SERVER['HTTP_REFERER']);
			}
		} else {
			$this->setBackUrl($this->homeUrl());
		}
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * 登録
	 */
	public function regist()
	{
		$this->form_validation->set_rules('usermanage_regist_userid', $this->lang->line('usermanager_dialog_userid'), 'trim|required|alpha_numeric|max_length['.$this->userid_max.']|callback_has_userid');
		if (!$this->input->post("usermanage_regist_uid", true)) {
			// 新規登録の場合はパスワード入力必須
			$this->form_validation->set_rules('usermanage_regist_password', $this->lang->line('usermanager_dialog_passwd'), 'trim|required|min_length['.$this->passwd_min.']|max_length['.$this->passwd_max.']|callback_check_password');
			$this->form_validation->set_rules('usermanage_regist_password_confirm', $this->lang->line('usermanager_dialog_passwd_confirm'), 'trim|required|min_length['.$this->passwd_min.']|max_length['.$this->passwd_max.']|matches[usermanage_regist_password]');
		} else {
			// 更新登録の場合で、パスワード入力がある場合は、最小、最大長チェック
			if ($this->input->post("usermanage_regist_password", true)) {
				$this->form_validation->set_rules('usermanage_regist_password', $this->lang->line('usermanager_dialog_passwd'), 'trim|min_length['.$this->passwd_min.']|max_length['.$this->passwd_max.']|callback_check_password');
				$this->form_validation->set_rules('usermanage_regist_password_confirm', $this->lang->line('usermanager_dialog_passwd_confirm'), 'trim|min_length['.$this->passwd_min.']|max_length['.$this->passwd_max.']|matches[usermanage_regist_password]');
			}
		}
		$this->form_validation->set_rules('usermanage_regist_nickname', $this->lang->line('usermanager_dialog_nickname'), 'trim|required|max_length['.$this->nickname_max.']');
		$this->form_validation->set_rules('usermanage_regist_surname', $this->lang->line('usermanager_dialog_surname'), 'trim|required|max_length['.$this->surname_max.']');
		$this->form_validation->set_rules('usermanage_regist_firstname', $this->lang->line('usermanager_dialog_firstname'), 'trim|required|max_length['.$this->firstname_max.']');

		if ($this->form_validation->run()) {
			// 画像がアップロードされたか判定
			$_save_file = null;
			if (is_uploaded_file($_FILES["usermanage_regist_imgupload"]["tmp_name"])) {
				// 画像アップロード処理
				$_save_file = $this->user_image_upload();
				if (!is_null($this->error_message)) {
					$this->displayView();
					return;
				}
			}

			if (!$this->input->post("usermanage_regist_uid", true)) {
				// ユーザーテーブルのIDの指定が無い場合は新規登録
				if (!$_uid = $this->user_model->regist(
						trim($this->input->post("usermanage_regist_userid", true)),
						trim($this->input->post("usermanage_regist_password", true)),
						trim($this->input->post("usermanage_regist_surname", true)),
						trim($this->input->post("usermanage_regist_firstname", true)),
						trim($this->input->post("usermanage_regist_nickname", true)),
						$_save_file
					)
				) {
					$this->error_message = $this->lang->line('usermanager_err_msg_regist');
				}
				// 画像ファイルリネーム
				if (!is_null($_save_file)) {
					$_img_dir = $this->config->item('PROFILE_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR;
					$_src_img = $_img_dir.$_save_file;
					$_src_info = new SplFileInfo($_src_img);
					$_destPath = $_img_dir.$_uid.'.'.$_src_info->getExtension();
					rename($_src_img, $_destPath);
					// ユーザーテーブルの画像パス更新
					if (!$this->user_model->updatePictureFile(basename($_destPath), $_uid)) {
						$this->error_message = $this->lang->line('usermanager_err_msg_update_picture_file');
					}
				}
			} else {
				// ユーザーテーブルのIDの指定が有る場合は更新登録
				if (!$this->user_model->update(
						$this->input->post("usermanage_regist_uid", true),
						trim($this->input->post("usermanage_regist_userid", true)),
						trim($this->input->post("usermanage_regist_password", true)),
						trim($this->input->post("usermanage_regist_surname", true)),
						trim($this->input->post("usermanage_regist_firstname", true)),
						trim($this->input->post("usermanage_regist_nickname", true)),
						$_save_file
				)
				) {
					$this->error_message = $this->lang->line('usermanager_err_msg_update');
				}
			}
			if (!is_null($this->error_message)) {
				$this->displayView();
				return;
			}
			redirect($_SERVER['HTTP_REFERER']);
			exit;
		}
		$this->displayView();
	}

	/**
	 * ユーザー画像アップロード
	 *
	 * return string 保存した画像ファイル名
	 */
	private function user_image_upload() {
		// 画像アップロードパス取得
		$_upload_path = $this->config->item('PROFILE_IMAGE_UPLOAD_PATH');
		if (!file_exists($_upload_path)) {
			mkdir($_upload_path, 0777, true);
		}
		// 画像ライブラリロード
		$_upload_config = array(
			'max_size' => $this->config->item('PROFILE_IMAGE_UPLOAD_MAX_SIZE'),
			'encrypt_name' => true,
			'upload_path' => $_upload_path,
			'allowed_types' => implode("|", $this->config->item('PROFILE_IMAGE_UPLOAD_EXTENSIONS'))
		);
		$this->load->library('upload', $_upload_config);

		// 画像アップロード
		if ($this->upload->do_upload('usermanage_regist_imgupload')) {
			$file_info = $this->upload->data();
			// 保存先ディレクトリ生成
			$_save_path = $this->config->item('PROFILE_IMAGE_SAVE_PATH');
			if (!file_exists($_save_path)) {
				mkdir($_save_path, 0777, true);
			}
			// 保存先ファイル名生成（拡張子無し）
			// 保存先パス＋ユーザーテーブルID
			$_uid = $this->commonlib->getTimeWithMillSecond(); // 新規の場合はユーザーIDが無いため、一時的に現在日時を設定（登録後にファイルリネームする）
			if ($this->input->post("usermanage_regist_uid", true)) {
				// 更新の場合は正式なユーザーIDを取得
				$_uid = $this->input->post("usermanage_regist_uid", true);
			}
			$_save_file_path_tmp = $_save_path.DIRECTORY_SEPARATOR.$_uid;
			// 古い保存先ファイルを削除しておく
			$this->deleteSaveUserPictureFile($_save_file_path_tmp);
			// 保存先パス＋ユーザーテーブルID＋拡張子
			$_save_file_path = $_save_file_path_tmp.$file_info['file_ext'];

			// 画像処理ライブラリロード
			$this->load->library('imagelib');

			// 正方形にトリミング＆リサイズ
			$this->imagelib->resizeSquare($this, $file_info['full_path'], $_save_file_path, $this->config->item('PROFILE_IMAGE_MAX_LENGTH'));

			return basename($_save_file_path);
		} else {
			// 画像アップロードエラー
			// エラーメッセージ生成
			$this->error_message = $this->upload->display_errors();
		}
		return null;
	}

	/**
	 * ユーザー画像保存先ファイル削除.
	 *
	 * @param string ユーザー画像保存先ファイル名（拡張子無し）
	 */
	private function deleteSaveUserPictureFile($save_file_name_ex_ext)
	{
		$_array_ext = $this->config->item('PROFILE_IMAGE_UPLOAD_EXTENSIONS');
		foreach ($_array_ext as $_ext) {
			$_delete_file = $save_file_name_ex_ext.'.'.$_ext;
			if (file_exists($_delete_file)) {
				unlink($_delete_file);
			}
		}
	}

	/**
	 * ユーザーID重複チェック
	 */
	public function has_userid()
	{
		if ($this->user_model->hasUserid(
				$this->input->post("usermanage_regist_uid", true), $this->input->post("usermanage_regist_userid", true))) {
			$this->form_validation->set_message("has_userid", $this->lang->line('usermanager_already_used_userid'));
			return false;
		}
		return true;
	}

	/**
	 * パスワード文字列チェック
	 */
	public function check_password()
	{
		if (!$this->commonlib->checkPasswordString($this->input->post("usermanage_regist_password", true))) {
			$this->form_validation->set_message("check_password", $this->lang->line('usermanager_alpha_numeric_passwd'));
			return false;
		}
		return true;
	}

	/**
	 * 停止
	 */
	public function stop()
	{
		if (!$this->user_model->stop($this->input->post("usermanage_confirm_modal_uid", true))) {
			$this->error_message = $this->lang->line('usermanager_err_msg_stop');
		}
		if (!is_null($this->error_message)) {
			$this->displayView();
			return;
		}
		redirect($_SERVER['HTTP_REFERER']);
		exit;
	}

	/**
	 * 再開
	 */
	public function restart()
	{
		if (!$this->user_model->restart($this->input->post("usermanage_confirm_modal_uid", true))) {
			$this->error_message = $this->lang->line('usermanager_err_msg_restart');
		}
		if (!is_null($this->error_message)) {
			$this->displayView();
			return;
		}
		redirect($_SERVER['HTTP_REFERER']);
		exit;
	}

	/**
	 * 削除
	 */
	public function delete()
	{
		$this->load->model("Students_Model", 'students_model', true);
		$this->load->model("Chat_Model", 'chat_model', true);
		$this->load->model("Diary_Model", 'diary_model', true);
		$this->load->model("Forum_Model", 'forum_model', true);
		$this->load->model("Task_Result_Model", 'task_result_model', true);
		$this->load->model("Task_Feedback_Model", 'task_feedback_model', true);
		$this->load->model('Materials_Play_Model', 'materials_play_model', true);
		$_uid = $this->input->post("usermanage_confirm_modal_uid", true);

		// 関連ファイル削除
		$_user_image = $this->user_model->getImageFile($_uid);
		if (!is_null($_user_image)) {
			unlink($this->config->item('PROFILE_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_user_image);
		}
		$_chat_images = $this->chat_model->getImageFiles($_uid);
		$this->deleteFiles($_chat_images);
		$_diary_images = $this->diary_model->getImageFiles($_uid);
		$this->deleteFiles($_diary_images);
		$_forum_images = $this->forum_model->getImageFiles($_uid);
		$this->deleteFiles($_forum_images);
		$_task_result_files = $this->task_result_model->getRealPathOfResultFiles($_uid);
		$this->deleteFiles($_task_result_files);

		// 関連テーブルデータ削除
		if (!$this->user_model->delete($_uid)
		 || !$this->students_model->deleteByUid($_uid)
		 || !$this->chat_model->deleteByUid($_uid)
		 || !$this->diary_model->deleteByUid($_uid)
		 || !$this->diary_model->deleteHideInfoByUid($_uid)
		 || !$this->forum_model->deleteByUid($_uid)
		 || !$this->task_result_model->deleteByUid($_uid)
		 || !$this->task_feedback_model->deleteByUid($_uid)
		 || !$this->materials_play_model->deleteByUid($_uid)
		) {
			$this->error_message = $this->lang->line('usermanager_err_msg_delete');
		}
		if (!is_null($this->error_message)) {
			$this->displayView();
			return;
		}
		redirect($_SERVER['HTTP_REFERER']);
		exit;
	}

	/**
	 * ファイル削除
	 *
	 * @param object $files
	 */
	private function deleteFiles($files)
	{
		if (is_null($files)) {
			return;
		}
		foreach ($files as $fileobj) {
			foreach ($fileobj as $file) {
				unlink($file);
			}
		}
	}

	/**
	 * ユーザー登録用CSVアップロード
	 */
	public function upload()
	{
		// アップロードパス取得
		$_upload_path = $this->config->item('USERMANAGE_USER_CSV_UPLOAD_PATH');
		if (!file_exists($_upload_path)) {
			mkdir($_upload_path, 0777, true);
		}
		// アップロード定義生成
		$_upload_config = array(
				'max_size' => $this->config->item('USERMANAGE_USER_CSV_UPLOAD_MAX_SIZE'),
				'encrypt_name' => true,
				'upload_path' => $_upload_path,
				'allowed_types' => 'csv'
		);
		$this->load->library('upload', $_upload_config);

		// アップロード処理実行
		if ($this->upload->do_upload('usermanage_file_upload')) {
			$_file_info = $this->upload->data(); // アップロードファイル情報取得

			// Mac対応のため、改行コードや文字コードを統一して一旦一時ファイルに展開
			setlocale(LC_ALL, 'ja_JP.UTF-8');
			$_data = file_get_contents($_file_info['full_path']);
			$_data = preg_replace("/\r\n|\r|\n/", "\n", $_data); // 改行コードをLFに統一しておく
			$_fileObj = null;
			$_tmp = tmpfile();
			$_metadatas = stream_get_meta_data($_tmp);
			$_tmp_filename = $_metadatas['uri'];
			if (!mb_detect_encoding($_data, $this->config->item('USERMANAGE_USER_CSV_UPLOAD_DETECT_ORDER'))) {
				// 文字コード判定できないファイルはUTF-16LE(BOM)付ファイルのタブ区切りCSVファイルとして取り扱う。
				// 一時ファイルにそのままデータ書き込み
				fwrite($_tmp, $_data);

				// 一時ファイルから文字コードを変換しながらcsvデータ読み込み
				$_spec = "php://filter/read=convert.iconv.utf-16%2Futf-8/resource=$_tmp_filename";
				$_fileObj = new SplFileObject($_spec, 'rb');
				$_fileObj->setCsvControl("\t"); // デフォルトの区切り文字をタブに変更する
			} else {
				// 文字コード判定できた場合、
				// 一時ファイルに文字コードを変換してデータ書き込み
				$_data = mb_convert_encoding($_data, 'UTF-8', $this->config->item('USERMANAGE_USER_CSV_UPLOAD_DETECT_ORDER'));
				fwrite($_tmp, $_data);

				// 一時ファイルからcsvデータ読み込み
				$_fileObj = new SplFileObject($_tmp_filename);
			}
			$_fileObj->setFlags(SplFileObject::READ_CSV);

			// ファイルインポート
			$this->db->trans_begin();
			foreach ($_fileObj as $_line) {
				if (is_null($_line[0])) {
					// 空行の場合は何もしない
					continue;
				}
				if (count($_line) != 4) {
					$_fileObj = null;
					fclose($_tmp);
					unlink($_file_info['full_path']);
					$this->error_message = $this->lang->line('usermanager_err_msg_upload_format');
					$this->displayView();
					return;
				}
				if (strlen($_line[0]) == 0 || strlen($_line[0]) > $this->userid_max) {
					// ユーザーIDが1文字以上、最大文字数以内ではない場合
					$_fileObj = null;
					fclose($_tmp);
					unlink($_file_info['full_path']);
					$this->error_message = $this->lang->line('usermanager_err_msg_upload_userid_length');
					$this->displayView();
					return;
				}
				if (!ctype_alnum($_line[0])) {
					// ユーザーIDが半角英数ではない場合
					$_fileObj = null;
					fclose($_tmp);
					unlink($_file_info['full_path']);
					$this->error_message = $this->lang->line('usermanager_err_msg_upload_userid_alpha_numeric');
					$this->displayView();
					return;
				}
				if (strlen($_line[1]) < $this->passwd_min || strlen($_line[1]) > $this->passwd_max) {
					// パスワードが最低文字数以上、最大文字数以内ではない場合
					$_fileObj = null;
					fclose($_tmp);
					unlink($_file_info['full_path']);
					$this->error_message = $this->lang->line('usermanager_err_msg_upload_passwd_length');
					$this->displayView();
					return;
				}
				if (!$this->commonlib->checkPasswordString($_line[1])) {
					// パスワードが半角英数記号ではない場合
					$_fileObj = null;
					fclose($_tmp);
					unlink($_file_info['full_path']);
					$this->error_message = $this->lang->line('usermanager_err_msg_upload_passwd_alpha_numeric');
					$this->displayView();
					return;
				}
				if (strlen($_line[2]) == 0 || strlen($_line[2]) > $this->surname_max) {
					// 姓が1文字以上、最大文字数以内ではない場合
					$_fileObj = null;
					fclose($_tmp);
					unlink($_file_info['full_path']);
					$this->error_message = $this->lang->line('usermanager_err_msg_upload_surname_length');
					$this->displayView();
					return;
				}
				if (strlen($_line[3]) == 0 || strlen($_line[3]) > $this->firstname_max) {
					// 名が1文字以上、最大文字数以内ではない場合
					$_fileObj = null;
					fclose($_tmp);
					unlink($_file_info['full_path']);
					$this->error_message = $this->lang->line('usermanager_err_msg_upload_firstname_length');
					$this->displayView();
					return;
				}
				// ニックネームにはフルネームを設定
				$_nickname = $this->commonlib->createFullName($_line[3], $_line[2]);
				// ユーザーテーブル登録
				$this->user_model->regist(
						$_line[0], // ユーザーID
						$_line[1], // パスワード
						$_line[2], // 姓
						$_line[3], // 名
						$_nickname, // ニックネーム
						null,
						true
				);
			}
			$_fileObj = null;

			fclose($_tmp);

			unlink($_file_info['full_path']);

			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				$this->error_message = $this->lang->line('usermanager_err_msg_upload');
			} else {
				$this->db->trans_commit();
				redirect($this->commonlib->baseUrl().'usermanager');
			}
		} else {
			$this->error_message = $this->upload->display_errors();
		}
		$this->displayView();
	}

	/**
	 * ダウンロード
	 */
	public function download()
	{
		$_userlist = $this->user_model->get_list_for_download(
				$this->search_userid, $this->search_surname, $this->search_firstname);
		if (!is_null($_userlist)) {
			// ファイルパス生成
			$_file_path = $this->config->item('USERMANAGE_USER_CSV_DOWNLOAD_PATH');
			if (!file_exists($_file_path)) {
				mkdir($_file_path, 0777, true);
			}
			$_file_path .= DIRECTORY_SEPARATOR.$this->config->item('USERMANAGE_USER_CSV_DOWNLOAD_FILE_PREFIX')
						.$this->commonlib->getTimeWithMillSecond().'.csv';

			// ファイル生成
			// Windows Macともに開ける形式である、UTF-16LE(BOM付)タブ区切りCSVとして生成する。
			$_spec = "php://filter/write=convert.iconv.utf-8%2Futf-16le/resource=$_file_path";
			$_fileObj = new SplFileObject($_spec, "wb");
			$_fileObj->setCsvControl("\t"); // デフォルトの区切り文字をタブに変更する
			$_fileObj->fwrite("\xEF\xBB\xBF"); // BOMを書き込んだ後，1行ずつファイルに書き込む
			foreach ($_userlist as $_user) {
				$_column_array = array();
				foreach ($_user as $_key=>$_value) {
					$_column_data;
					if ($_key == 'validityflg') {
						if ($_value == 1) {
							$_column_data = $this->lang->line('usermanager_csvdata_validityflg_true');
						} else {
							$_column_data = $this->lang->line('usermanager_csvdata_validityflg_false');
						}
					} else {
						$_column_data = $_value;
					}
					$_column_array[] = $_column_data;
				}
				$_fileObj->fputcsv($_column_array);
			}
			$_fileObj = null;

			// ファイル出力
			mb_http_output("pass"); // mb_output_handler()が自動的に呼ばれるのを防ぐおまじない
			header("Cache-Control: public"); // SSL環境、IE8以下でダウンロードが出来ない対応
			header("Pragma: public"); // SSL環境、IE8以下でダウンロードが出来ない対応
			header('Content-Type: application/force-download');
			header('Content-Length: '.filesize($_file_path));
			header('Content-disposition: attachment; filename="'.basename($_file_path).'"');

			$_handle = fopen($_file_path, 'rb');
			if (!$_handle) {
				exit;
			}
			while(!feof($_handle)) {
				echo fread($_handle, '4096');
				ob_flush();
				flush();
			}
			fclose($_handle);

			// ファイル出力したのでファイルは削除
			unlink($_file_path);
		}

	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		// ユーザー一覧取得（１画面分）
		$_userlist = $this->user_model->get_list(
				$this->page, $this->user_list_limit, $this->search_userid
				, $this->search_surname, $this->search_firstname);
		if ($this->page > 1 && is_null($_userlist)) {
			$this->page--;
			$_userlist = $this->user_model->get_list(
					$this->page, $this->user_list_limit, $this->search_userid
					, $this->search_surname, $this->search_firstname);
		}
		if (!is_null($_userlist)) {
			foreach ($_userlist as $_user) {
				$this->load->model("Course_Model", 'course_model', true);
				$_course_info = $this->course_model->getCourseOfUser($_user->id);
				if (!is_null($_course_info)) {
					$_user->course_info = $_course_info;
				}
				$_user->picture_file = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=".$_user->id;
				if ($_user->validityflg == 1) {
					// ユーザーが有効の場合は、停止ボタンを表示
					$_user->id_validity = 'usermanager_btn_stop_'.$_user->id;
					$_user->btn_validity = $this->lang->line('usermanager_btn_stop');
					$_user->btn_validity_class = "btn-default";
				} else {
					// ユーザーが無効の場合は、再開ボタンを表示
					$_user->id_validity = 'usermanager_btn_restart_'.$_user->id;
					$_user->btn_validity = $this->lang->line('usermanager_btn_restart');
					$_user->btn_validity_class = "btn-danger";
				}
			}
		}
		// ユーザー一覧トータル件数取得
		$_usercnt = $this->user_model->get_count(
				$this->search_userid, $this->search_surname, $this->search_firstname);

		// ユーザー一覧表示開始数（件目）
		$_start_cnt = ($this->page - 1) * $this->user_list_limit + 1;
		// ユーザー一覧表示終了数（件目）
		$_end_cnt = $_start_cnt + count($_userlist) - 1;
		if (count($_userlist) == 0) {
			$_start_cnt = 0;
			$_end_cnt = 0;
		}

		// 前ページありフラグ
		$_before_page = null;
		if ($_start_cnt > 1) {
			$_before_page = $this->commonlib->baseUrl().'usermanager?page='.($this->page - 1).$this->createUrlParamsOfSearch();

		}

		// 全ページ数算出
		$_total_pages = ceil($_usercnt / $this->user_list_limit);

		// 次ページ有フラグ
		$_next_page = null;
		if ($this->page < $_total_pages) {
			$_next_page = $this->commonlib->baseUrl().'usermanager?page='.($this->page + 1).$this->createUrlParamsOfSearch();
		}

		// 登録フォームアクションURL
		$_regist_action_url = $this->commonlib->baseUrl().'usermanager/regist?page='.$this->page.$this->createUrlParamsOfSearch();

		// アップロードフォームアクションURL
		$_upload_action_url = $this->commonlib->baseUrl().'usermanager/upload?page='.$this->page.$this->createUrlParamsOfSearch();

		// ビューのデータに設定
		$_view_data['page'] = $this->page;
		$_view_data['user_list'] = $_userlist;
		$_view_data['user_cnt'] = $_usercnt;
		$_view_data['start_cnt'] = $_start_cnt;
		$_view_data['end_cnt'] = $_end_cnt;
		$_view_data['before_page'] = $_before_page;
		$_view_data['next_page'] = $_next_page;
		$_view_data['regist_action_url'] = $_regist_action_url;
		$_view_data['upload_action_url'] = $_upload_action_url;
		$_view_data['search_userid'] = $this->search_userid;
		$_view_data['search_surname'] = $this->search_surname;
		$_view_data['search_firstname'] = $this->search_firstname;
		$_view_data['script'] = $this->createJavaScript($_before_page, $_next_page);
		if (!is_null($this->error_message)) {
			$_view_data['error_msg'] = $this->error_message;
		}

		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('usermanager', $_view_data); // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * URLパラメータ用検索パラメータ生成
	 *
	 * @return URLパラメータ用検索パラメータ文字列
	 */
	private function createUrlParamsOfSearch()
	{
		$_rtn = '';
		if (!is_null($this->search_userid) && $this->search_userid !== '') {
			$_rtn .= '&userid='.urlencode($this->search_userid);
		}
		if (!is_null($this->search_surname) && $this->search_surname !== '') {
			$_rtn .= '&surname='.urlencode($this->search_surname);
		}
		if (!is_null($this->search_firstname) && $this->search_firstname !== '') {
			$_rtn .= '&firstname='.urlencode($this->search_firstname);
		}
		return $_rtn;
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript($before_page, $next_page)
	{
		$_befor_page_url = '';
		if (!is_null($before_page)) {
			$_befor_page_url = $before_page;
		}
		$_next_page_url = '';
		if (!is_null($next_page)) {
			$_next_page_url = $next_page;
		}
		$_script = <<<EOT
<script>
	var beforePageUrl = "{$_befor_page_url}";
	var nextPageUrl = "{$_next_page_url}";
	var useridMax = "{$this->userid_max}";
	var passwdMin = "{$this->passwd_min}";
	var passwdMax = "{$this->passwd_max}";
	var surnameMax = "{$this->surname_max}";
	var firstnameMax = "{$this->firstname_max}";
	var nicknameMax = "{$this->nickname_max}";
	var errMsgUseridRequired = "{$this->lang->line('usermanager_script_err_msg_userid_required')}";
	var errMsgUseridAlphaNumeric = "{$this->lang->line('usermanager_script_err_msg_userid_alpha_numeric')}";
	var errMsgSurnameRequired = "{$this->lang->line('usermanager_script_err_msg_surname_required')}";
	var errMsgFirstnameRequired = "{$this->lang->line('usermanager_script_err_msg_firstname_required')}";
	var errMsgNicknameRequired = "{$this->lang->line('usermanager_script_err_msg_nickname_required')}";
	var errMsgPasswdRequired = "{$this->lang->line('usermanager_script_err_msg_passwd_required')}";
	var errMsgPasswdConfirmRequired = "{$this->lang->line('usermanager_script_err_msg_passwd_confirm_required')}";
	var errMsgPasswdConfirmEqualto = "{$this->lang->line('usermanager_script_err_msg_passwd_confirm_equalto')}";
	var errMsgMaxlength = "{$this->lang->line('usermanager_script_err_msg_maxlength')}";
	var errMsgMinlength = "{$this->lang->line('usermanager_script_err_msg_minlength')}";
	var dialogPlaceholderPasswd = "{$this->lang->line('usermanager_dialog_placeholder_passwd')}";
	var stopUrl = "{$this->commonlib->baseUrl()}usermanager/stop?page={$this->page}{$this->createUrlParamsOfSearch()}";
	var confirmMsgTitleStop = "{$this->lang->line('usermanager_confirm_msg_title_stop')}";
	var confirmMsgBodyStop = "{$this->lang->line('usermanager_confirm_msg_body_stop')}";
	var restartUrl = "{$this->commonlib->baseUrl()}usermanager/restart?page={$this->page}{$this->createUrlParamsOfSearch()}";
	var confirmMsgTitleRestart = "{$this->lang->line('usermanager_confirm_msg_title_restart')}";
	var confirmMsgBodyRestart = "{$this->lang->line('usermanager_confirm_msg_body_restart')}";
	var deleteUrl = "{$this->commonlib->baseUrl()}usermanager/delete?page={$this->page}{$this->createUrlParamsOfSearch()}";
	var confirmMsgTitleDelete = "{$this->lang->line('usermanager_confirm_msg_title_delete')}";
	var confirmMsgBodyDelete = "{$this->lang->line('usermanager_confirm_msg_body_delete')}";
	var downloadUrl = "{$this->commonlib->baseUrl()}usermanager/download?page={$this->page}{$this->createUrlParamsOfSearch()}";
	var courseNameDelim = "{$this->config->item('USERMANAGE_COURSENAME_DELIMITER')}";
	var pictureMaxLength = "{$this->config->item('PROFILE_IMAGE_MAX_LENGTH')}px";
</script>
EOT;
		return $_script;
	}

}
