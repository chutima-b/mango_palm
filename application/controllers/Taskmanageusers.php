<?php
/*
 * 受講者課題画面コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Taskmanageusers extends EX_Controller {
	/** 講座ID. */
	private $cid = 0;
	/** テーマID. */
	private $thid = 0;
	/** 現在のページ. */
	private $page = 0;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_TASKMANAGEUSERS')); // 受講者課題画面

		$this->load->model("Taskmanage_Model", 'taskmanage_model', true);
		$this->lang->load('taskmanageusers_lang');
		$this->setPageTitle($this->lang->line('taskmanageusers_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css =<<<EOT
<link rel="stylesheet" href="{$_base_url}css/slms_loading.css?v=1.0.0">
<link rel="stylesheet" href="{$_base_url}css/taskmanageusers.css?v=1.0.1">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}js/slms_loading.js?v=1.0.0"></script>
<script src="{$_base_url}js/taskmanageusers.js?v=1.0.2"></script>
EOT;
		$this->addScriptFiles($_add_script);

		// パラメータ取得
		$this->cid = $this->input->get('cid', true);
		$this->thid = $this->input->get('thid', true);
		$this->page = $this->input->get_post('page', true);
		if (is_null($this->page) || !is_numeric($this->page) || $this->page < 1) {
			$this->page = 1;
		}

		// 戻り画面URL設定
		$this->setBackUrl($this->commonlib->baseUrl().'taskmanager?cid='.$this->cid);
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * ダウンロード
	 */
	public function download()
	{
		// ダウンロード用課題管理情報リスト取得
		$_data_list = $this->taskmanage_model->getListForDownload($this->cid, $this->thid);

		if (!is_null($_data_list)) {
			// CSV用データに加工
			foreach ($_data_list as $_data) {
				$_is_quiz = false;
				if ($_data->quiz == 1) {
					$_is_quiz = true;
				}
				foreach ($_data as $_data_key=>$_data_value) {
					if ($_data_key == 'tasklist') {
						// 課題データの加工
						$_quiz_correct_cnt = 0; // クイズ正解数
						foreach ($_data_value as $_task) {
							$_choices = $this->commonlib->nl2Array($_task->choices);
							foreach ($_task as $_task_key=>$_task_value) {
								if ($_task_key == 'result_data') {
									// 課題の解答情報の場合
									if ($_task->task_type == $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')) {
										// クイズ（単一選択）の場合は正解数をカウントしておく
										for ($_cnt = 1; $_cnt <= count($_choices); $_cnt++) {
											// 解答した課題の値と、課題の項目名が同じ場合
											if (!is_null($_task_value)
											 && $_task_value == $_choices[$_cnt-1]) {
												// フィードバック済みのクイズの場合
												if (!is_null($_data->feedback_at)
												&& $_is_quiz
												&& !is_null($_task->quiz_answer)) {
													if ($_task->quiz_answer == $_cnt) {
														// クイズの正解と解答のカウンター値が同じ場合は正解を評価点に設定
														$_task->feedback_points = $this->lang->line('taskmanageusers_csv_item_correct');
														// クイズの正解数をカウント
														$_quiz_correct_cnt++;
													} else {
														// クイズの正解と解答のカウンター値が違う場合は不正解を評価点に設定
														$_task->feedback_points = $this->lang->line('taskmanageusers_csv_item_incorrect');
													}
												}
											}
										}
									}

									if ($_task->task_type == $this->config->item('TASK_TYPE_FILE')) {
										if (!is_null($_task->result_data)) {
											$_task->result_data = basename($_task->result_data); // ファイル（画像）の場合はファイル名を設定する
										}
									}
								}
							}
						}
						if (!is_null($_data->feedback_at)
						 && $_is_quiz) {
							// クイズの場合は正解数を“実際の評価点合計”に設定
							$_data->feedback_points = $_quiz_correct_cnt;
						}
					}
				}
			}

			// CSV出力
			// ファイルパス生成
			$_file_path = $this->config->item('TASKMANAGEUSERS_CSV_DOWNLOAD_PATH');
			if (!file_exists($_file_path)) {
				mkdir($_file_path, 0777, true);
			}
			$_file_path .= DIRECTORY_SEPARATOR.$this->config->item('TASKMANAGER_CSV_DOWNLOAD_FILE_PREFIX')
			.$this->commonlib->getTimeWithMillSecond().'.csv';

			// ファイル生成
			// Windows Macともに開ける形式である、UTF-16LE(BOM付)タブ区切りCSVとして生成する。
			$_spec = "php://filter/write=convert.iconv.utf-8%2Futf-16le/resource=$_file_path";
			$_fileObj = new SplFileObject($_spec, "wb");
			$_fileObj->setCsvControl("\t"); // デフォルトの区切り文字をタブに変更する
			$_fileObj->fwrite("\xEF\xBB\xBF"); // BOMを書き込んだ後，1行ずつファイルに書き込む
			$_today = date('Y/m/d H:i:s');
					$_is_first = true;
			foreach ($_data_list as $_data) {
				// １ユーザーごとに1レコードにする。
				$_column_array = array();
				// 受講者、ジャンル、講座、テーマ情報
				$_column_array[] = $_data->userid; // ユーザーID
				$_column_array[] = $_data->nickname; // ニックネーム
				$_column_array[] = $_data->surname; // 姓
				$_column_array[] = $_data->firstname; // 名
				$_column_array[] = $_data->genreid; // ジャンルID
				$_column_array[] = $_data->genrename; // ジャンル名
				$_column_array[] = $_data->course_code; // 講座ID
				$_column_array[] = $_data->course_name; // 講座名
				$_column_array[] = $_data->theme_name; // テーマ名
				$_column_array[] = $_data->theme_date; // テーマ日付け
				$_column_array[] = $_data->submit_at; // 提出日
				$_column_array[] = $_data->time_limit; // 提出期限
				$_column_array[] = $_data->feedback_at; // フィードバック日
				$_column_array[] = count($_data->materialslist); // 教材動画数
				foreach ($_data->materialslist as $_material) {
					// 教材動画再生情報（教材動画数分カラムが増える）
					$_column_array[] = $_material->material_description; // 教材動画リンク文字列
					$_column_array[] = $_material->total_time; // 教材動画トータル時間
					$_column_array[] = $_material->play_time; // 教材動画再生時間
				}
				$_column_array[] = $_data->feedback_points; // 実際の評価点
				$_column_array[] = $_data->evaluation_points; // 出題した評価点合計
				$_column_array[] = count($_data->tasklist); // 課題数

				$_header_column = array();
				if ($_is_first) {
					// 初回生成時の場合、１行目に挿入するカラム名情報生成
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_userid');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_nickname');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_lastname');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_firstname');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_genreid');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_genrename');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_coursecode');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_coursename');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_themename');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_themedate');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_submitdate');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_themelimit');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_feedbackdate');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_moviecnt');
					for ($_cnt = 1; $_cnt <= count($_data->materialslist); $_cnt++) {
						$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_moviename').$_cnt;
						$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_movietotal').$_cnt;
						$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_movieplaytime').$_cnt;
					}
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_total_feedbackpoint');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_total_evaluationpoint');
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_taskcnt');
				}
				$_cnt = 1;
				foreach ($_data->tasklist as $_task) {
					// 課題情報（課題数分カラムが増える）
					$_column_array[] = $_task->task_name; // 課題名
					$_column_array[] = $_task->task_description; // 課題の説明
					$_column_array[] = $_task->feedback_points; // クイズの場合、正解/不正解、クイズ以外は評価点１
					$_column_array[] = $_task->result_data; // 解答
					$_column_array[] = $_task->feedback_message; // 解説

					if ($_is_first) {
						// 初回生成時の場合、１行目に挿入するカラム名情報生成
						// 課題情報はクイズ（単一選択）の場合、カラム名が変わるのでここで生成する
						$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_taskname').$_cnt;
						$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_taskdescription').$_cnt;
						if ($_task->task_type == $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')) {
							$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_quizresult').$_cnt;
						} else {
							$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_feedbackpoints').$_cnt;
						}
						$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_answer').$_cnt;
						$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_feedbackmessage').$_cnt;
						$_cnt++;
					}
				}
				// ダウンロード日
				$_column_array[] = $_today; // ダウンロード日時
				if ($_is_first) {
					// 初回生成時の場合、１行目に挿入するカラム名情報生成
					$_header_column[] = $this->lang->line('taskmanageusers_csv_column_name_downloaddate');
					// 生成したカラムを１行目に挿入
					$_fileObj->fputcsv($_header_column);
				}
				$_fileObj->fputcsv($_column_array);
				$_is_first = false;
			}
			$_fileObj = null;

			// ファイル出力
			mb_http_output("pass"); // mb_output_handler()が自動的に呼ばれるのを防ぐおまじない
			header("Cache-Control: public"); // SSL環境、IE8以下でダウンロードが出来ない対応
			header("Pragma: public"); // SSL環境、IE8以下でダウンロードが出来ない対応
			header('Content-Type: application/force-download');
			header('Content-Length: '.filesize($_file_path));
			header('Content-disposition: attachment; filename="'.basename($_file_path).'"');

			$_handle = fopen($_file_path, 'rb');
			if (!$_handle) {
				exit;
			}
			while(!feof($_handle)) {
				echo fread($_handle, '4096');
				ob_flush();
				flush();
			}
			fclose($_handle);

			// ファイル出力したのでファイルは削除
			unlink($_file_path);
		}
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		// パラメータ取得
		$this->cid = $this->input->get('cid', true);

		// 講座名取得
		$this->load->model("Course_Model", 'course_model', true);
		$_course_name = $this->course_model->getCourseName($this->cid);

		// 受講者数取得
		$this->load->model("Students_Model", 'students_model', true);
		$_participants = $this->students_model->getCountOfCourse($this->cid);

		// ユーザー一覧表示件数
		$_userlist_disp_count = $this->config->item('TASKMANAGEUSERS_USERLIST_DISP_COUNT');

		// 該当テーマの受講者課題管理情報
		$_theme_data = $this->createThemeOfTaskmanageUserData($_userlist_disp_count);

		// ユーザー一覧表示開始数（件目）
		$_start_cnt = ($this->page - 1) * $_userlist_disp_count + 1;
		if (is_null($_theme_data->taskmanage_list)
		 || count($_theme_data->taskmanage_list) == 0) {
			$_start_cnt = 0;
		}

		$_base_page_url = $this->commonlib->baseUrl().'taskmanageusers?cid='.$this->cid.'&thid='.$this->thid;

		// 前ページURL
		$_before_page = null;
		if ($_start_cnt > 1) {
			$_before_page = $_base_page_url.'&page='.($this->page - 1);

		}

		// 全ページ数算出
		$_total_pages = ceil($_participants / $_userlist_disp_count);

		// 次ページURL
		$_next_page = null;
		if ($this->page < $_total_pages) {
			$_next_page = $_base_page_url.'&page='.($this->page + 1);
		}

		// ビューのデータに設定
		$_view_data['cid'] = $this->cid;
		$_view_data['thid'] = $this->thid;
		$_view_data['before_page'] = $_before_page;
		$_view_data['next_page'] = $_next_page;
		$_view_data['page'] = $this->page;
		$_view_data['coursename'] = $_course_name;
		$_view_data['participants'] = $_participants;
		$_view_data['theme_data'] = $_theme_data;
		$_view_data['script'] = $this->createJavaScript($_before_page, $_next_page);

		// ビュー表示
		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('taskmanageusers', $_view_data); // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * 該当テーマの受講者課題管理情報生成
	 *
	 * @param int ユーザー一覧表示件数
	 * @return object 該当テーマの受講者課題管理情報
	 */
	private function createThemeOfTaskmanageUserData($userlist_disp_count)
	{
		$this->load->model("Theme_Model", 'theme_model', true);
		$this->load->model("Task_Model", 'task_model', true);
		$_theme_data = $this->theme_model->getThemeInfoForTaskmanageusers($this->cid, $this->thid);
 		if (!is_null($_theme_data)) {
			$_today = date("Y/m/d");
			// 受講者課題画面用課題管理情報リスト取得
			$_theme_data->taskmanage_list = $this->taskmanage_model->getListOfUsers($this->page, $userlist_disp_count, $this->cid, $this->thid);
			if ($this->page > 1 && is_null($_theme_data->taskmanage_list)) {
				$this->page--;
				$_theme_data->taskmanage_list = $this->taskmanage_model->getListOfUsers($this->page, $userlist_disp_count, $this->cid, $this->thid);
			}
			if (!is_null($_theme_data->taskmanage_list)) {
				if ($_theme_data->evaluation_use == $this->config->item('EVALUATION_USE_FLAG_ON')) {
					// 「課題を作成する」がYESの場合
					foreach ($_theme_data->taskmanage_list as $_students) {
						// クイズの場合
						if ($_theme_data->quiz == 1) {
							// フィードバック済みの場合
							if (strcmp($_students->feedback_at, '-') != 0) {
								// 評価点の表示内容を“正解数/出題数”に差し替える。
								$_tasklist = $this->task_model->getTaskListWhithResult($this->cid, $this->thid, $_students->uid);
								$_quiz_correct_cnt = 0; // クイズ正解数
								foreach ($_tasklist as $_task) {
									$_choices = $this->commonlib->nl2Array($_task->choices);
									if ($_task->task_type == $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')) {
										// クイズ（単一選択）の場合は正解数をカウントしておく
										for ($_cnt = 1; $_cnt <= count($_choices); $_cnt++) {
											// 解答した課題の値と、課題の項目名が同じ場合で、
											if (!is_null($_task->result_radio)
											 && $_task->result_radio == $_choices[$_cnt-1]) {
												// クイズの正答とカウンター値が一致した場合、
												if ($_task->quiz_answer == $_cnt) {
													// クイズの正解数をカウント
													$_quiz_correct_cnt++;
												}
											}
										}
									}
								}
								$_students->points = $_quiz_correct_cnt.'/'.$_students->quiz_cnt;
							} else {
								$_students->points = '-';
							}
						}
					}
				}
			}
 		}
		return $_theme_data;
	}

	/**
	 * JavaScriptデータ生成
	 *
	 * @param string $before_page 前ページURL
	 * @param string $next_page 次ページURL
	 * @return JavaScript文字列
	 */
	private function createJavaScript($before_page, $next_page)
	{
		$_befor_page_url = '';
		if (!is_null($before_page)) {
			$_befor_page_url = $before_page;
		}
		$_next_page_url = '';
		if (!is_null($next_page)) {
			$_next_page_url = $next_page;
		}

		$_script = <<<EOT
<script>
	var beforePageUrl = '{$_befor_page_url}';
	var nextPageUrl = '{$_next_page_url}';
	var feedbackUrl = '{$this->commonlib->baseUrl()}feedback';
	var downloadUrl = '{$this->commonlib->baseUrl()}taskmanageusers/download';
</script>
EOT;
		return $_script;
	}

}
