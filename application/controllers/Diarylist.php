<?php
/*
 * 日記一覧画面コントローラー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Diarylist extends EX_Controller {

	/** 講座情報テーブルID. */
	private $cid = null;
	/** テーマテーブルID. */
	private $thid = null;
	/** 戻り先パラメータ. */
	private $pre = null;
	/** 現在のページ. */
	private $page = 0;
	/** 日記の戻り先ページ. */
	private $dpage = 0;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_DIARY_LIST')); // 日記一覧画面
		$this->lang->load('diary_list_lang');
		$this->setPageTitle($this->lang->line('diary_list_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link rel="stylesheet" href="{$_base_url}libs/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css">
<link rel="stylesheet" href="{$_base_url}css/slms_loading.css?v=1.0.0">
<link rel="stylesheet" href="{$_base_url}css/diarylist.css?v=1.0.1">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}libs/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="{$_base_url}js/slms_loading.js?v=1.0.0"></script>
<script src="{$_base_url}js/diarylist.js?v=1.0.1"></script>
EOT;
		$this->addScriptFiles($_add_script);


		// パラメータ取得
		$this->cid = $this->input->get_post('cid', true);
		$this->thid = $this->input->get_post('thid', true);
		$this->pre = $this->input->get_post('pre', true);
		$this->page = $this->input->get_post('page', true);
		if (is_null($this->page) || !is_numeric($this->page) || $this->page < 1) {
			$this->page = 1;
		}
		$this->dpage = $this->input->get_post('dpage', true);
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		// 戻る画面設定
		$path = $this->commonlib->baseUrl()."diary?cid=".$this->cid."&pre=".$this->pre."&page=".$this->dpage;
		if ($this->pre == "theme") {
			$path .= "&thid=".$this->thid;
		}

		// 戻る画面の設定
		$this->setBackUrl($path);

		// 講座モデル
		$this->load->model('Course_Model', 'course', TRUE);

		// 講座名取得
		$course_data = $this->course->getCourse($this->cid);

		// 日記一覧モデル
		$this->load->model('Diary_List_Model', 'diarylist', TRUE);

		// 日記一覧リスト取得
		$_diarylist_disp_count = $this->config->item('DIARYLIST_DISP_COUNT');
		$diarylist_data = $this->diarylist->getDiaryList($this->page, $_diarylist_disp_count, $this->cid);
		if ($this->page > 1 && is_null($diarylist_data)) {
			$this->page--;
			$diarylist_data = $this->diarylist->getDiaryList($this->page, $_diarylist_disp_count, $this->cid);
		}

		// ユーザー一覧表示開始数（件目）
		$_start_cnt = ($this->page - 1) * $_diarylist_disp_count + 1;
		if (count($diarylist_data) == 0) {
			$_start_cnt = 0;
		}

		$_base_page_url = $this->commonlib->baseUrl()."diarylist?cid=".$this->cid."&pre=".$this->pre."&dpage=".$this->dpage;
		if ($this->pre == "theme") {
			$_base_page_url .= '&thid='.$this->thid;
		}

		// 前ページURL
		$_before_page = null;
		if ($_start_cnt > 1) {
			$_before_page = $_base_page_url.'&page='.($this->page - 1);

		}

		// 日記参加者数取得
		$_diarylist_count = $this->diarylist->getTotalCount($this->cid);

		// 全ページ数算出
		$_total_pages = ceil($_diarylist_count / $_diarylist_disp_count);

		// 次ページURL
		$_next_page = null;
		if ($this->page < $_total_pages) {
			$_next_page = $_base_page_url.'&page='.($this->page + 1);
		}

		$param['course_data'] = $course_data;
		$param['diarylist_data'] = $this->formatDiaryData($diarylist_data);
		$param['before_page'] = $_before_page;
		$param['next_page'] = $_next_page;
		$param['pre'] = $this->pre;
		$param['thid'] = $this->thid;
		$param['page'] = $this->page;
		$param['dpage'] = $this->dpage;
		$param['script'] = $this->createJavaScript($_before_page, $_next_page);

		$this->load->view('common_header');      // 共通ヘッダー部
		$this->load->view('diary_list', $param); // コンテンツ部
		$this->load->view('common_footer');      // 共通フッター部
	}

	/**
	 * JavaScriptデータ生成
	 *
	 * @param string $before_page 前ページURL
	 * @param string $next_page 次ページURL
	 */
	private function createJavaScript($before_page, $next_page)
	{
		$_befor_page_url = '';
		if (!is_null($before_page)) {
			$_befor_page_url = $before_page;
		}
		$_next_page_url = '';
		if (!is_null($next_page)) {
			$_next_page_url = $next_page;
		}
		$_script = <<<EOT
<script>
	var beforePageUrl = "{$_befor_page_url}";
	var nextPageUrl = "{$_next_page_url}";
</script>
EOT;
		return $_script;
	}

	/**
	 * 日記データ成型
	 *
	 * @param array $diarylist_data 日記データ
	 * @return array 成型後の日記データ
	 */
	private function formatDiaryData($diarylist_data) {

		$retdatas = array();

		foreach ($diarylist_data as $d) {

			if ($d['picture_file'] == null) {
				$d['picture_file'] = $this->commonlib->baseUrl().$this->config->item('DIARYLIST_DEFAULT_IMAGE_URL');
			} else {
				$d['picture_file'] = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=".$d['uid'];
			}

			if ($d['authority'] == $this->config->item('AUTH_STUDENT')) {
				$d += array('auth' => $this->config->item('DIARYLIST_AUTH_STUDENT'));
			} elseif ($d['authority'] == $this->config->item('AUTH_TEACHER')) {
				$d += array('auth' => $this->config->item('DIARYLIST_AUTH_TEACHER'));
			} elseif ($d['authority'] == $this->config->item('AUTH_OP_MANAGER')) {
				$d += array('auth' => $this->config->item('DIARYLIST_AUTH_OP_MANAGER'));
			} elseif ($d['authority'] == $this->config->item('AUTH_SYS_MANAGER')) {
				$d += array('auth' => $this->config->item('DIARYLIST_AUTH_SYS_MANAGER'));
			}

			array_push($retdatas, $d);
		}

		return $retdatas;
	}

	/**
	 * 非表示変更処理
	 */
	public function edit() {
		// リダイレクト画面設定
		$path = $this->commonlib->baseUrl()."diarylist?cid=".$this->cid."&pre=".$this->pre."&page=".$this->page."&dpage=".$this->dpage;
		if ($this->pre == "theme") {
			$path .= "&thid=".$this->thid;
		}

		$display = $this->input->post('display_check', true);
		$hideid = $this->input->post('uid', true);
		$hid = $this->input->post('hid', true);

		// 日記一覧モデル
		$this->load->model('Diary_List_Model', 'diarylist', TRUE);

		// 非表示判定
		if ($display == null or $display == "") {
			$this->diarylist->registHide($this->cid, $hideid);
		} else {
			$this->diarylist->deleteHide($hid);
		}

//		redirect($path, 'refresh');
		redirect($path, 'location');
	}

}
