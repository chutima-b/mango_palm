<?php
/*
 * 講師運用管理者登録画面コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Staffmanager extends EX_Controller {
	/** ビュー画面へ渡すデータ. */
	private $view_data = null;
	/** cidパラメータ. */
	private $cid = 0;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_STAFF_MANAGER')); // 講師運用管理者登録画面
		$this->lang->load('staffmanager_lang');
		$this->setPageTitle($this->lang->line('staffmanager_header_pagetitle'));
		$this->addScriptFiles('<script src="'.$this->commonlib->baseUrl().'js/staffmanager.js?v=1.0.0"></script>');

		$this->view_data = array();
		$this->cid = $this->input->get('cid', true);
		$this->lang->load('courseedit_lang');
		$this->load->model("Course_Model", 'course_model', true);
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		// 講座データ取得
		$_course = $this->course_model->getCourse($this->cid);
		// このコースでの権限取得
		$this->load->model("Students_Model", 'students_model', true);
		$_authority_info = $this->session->userdata('authority');
		$_authority = $this->students_model->getCourseAuth($this->cid, $_authority_info[0]->id);
		// ビューのデータに設定
		$this->view_data['course'] = $_course[0];
		$this->view_data['authority'] = $_authority;
		$this->view_data['cid'] = $this->cid;
		$this->view_data['script'] = $this->createJavaScript();
		// 戻る画面設定
		$this->setBackUrl($this->homeUrl());
		// ビュー表示
		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('staffmanager', $this->view_data);    // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		$_script = <<<EOT
<script>
	var registTeacherUrl = "{$this->commonlib->baseUrl()}studentsregist?cid={$this->cid}&target={$this->config->item('AUTH_TEACHER')}";
	var registOpManagerUrl = "{$this->commonlib->baseUrl()}studentsregist?cid={$this->cid}&target={$this->config->item('AUTH_OP_MANAGER')}";
</script>
EOT;
		return $_script;
	}

}
