<?php
/*
 * フォーラム管理画面コントローラー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Forummanage extends EX_Controller {

	/** 権限情報. */
	private $authority_info = null;
	/** フォーラム名最大文字数. */
	private $forum_name_max = null;
	/** 投稿メッセージ最大文字数. */
	private $postmsg_max = null;
	/** エラーメッセージ. */
	private $error_message = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_FORUM_MANAGE')); // フォーラム管理画面
		$this->lang->load('forum_manage_lang');
		$this->setPageTitle($this->lang->line('forum_manage_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link href="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.css?v=1.0.0" rel="stylesheet">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}libs/jqueryvalidation/jquery.validate.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.js?v=1.0.0"></script>
<script src="{$_base_url}js/forummanage.js?v=1.0.1"></script>
EOT;
		$this->addScriptFiles($_add_script);

		// ユーザー情報取得
		$this->authority_info = $this->session->userdata('authority');
		// フォーラム名最大文字数
		$this->forum_name_max = $this->config->item('FORUM_MANAGE_FORUM_NAME_MAXLENGTH');
		// 投稿メッセージ最大文字数
		$this->postmsg_max = $this->config->item('FORUM_POST_MESSAGE_MAXLENGTH');
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		$cid = $this->input->get_post('cid', true);
		$pre = $this->input->get_post('pre', true);
		$thid = null;

		// 戻る画面設定
		if ($pre == "theme") {
			$thid = $this->input->get_post('thid', true);
			$this->setBackUrl($this->commonlib->baseUrl()."theme?cid=".$cid."&thid=".$thid);
		} else {
			$this->setBackUrl($this->commonlib->baseUrl()."course?cid=".$cid);
		}

		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('forum_manage', $this->makeDispData($cid, $pre, $thid));  // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * フォーラム新規追加
	 */
	public function registration()
	{

		$cid = $this->input->post('cid', true);
		$pre = $this->input->post('pre', true);
		$postmsg = $this->input->post('postmsg', true);

		$this->form_validation->set_rules('forum_name', $this->lang->line('forum_manage_entry_title'), 'trim|required|max_length['.$this->forum_name_max.']');
		$this->form_validation->set_rules('postmsg', $this->lang->line('forum_manage_textarea_default_message'), 'trim|required|max_length['.$this->postmsg_max.']');

		if ($this->form_validation->run()) {

			$forum_name = $this->input->post('forum_name', true);

			// フォーラム管理登録
			$this->load->model('Forum_Manage_Model', 'forummanage', TRUE);
			$fid = $this->forummanage->registrationForum($cid, $forum_name);

			if (is_null($fid)) {
				$this->error_message = $this->lang->line('forum_manage_err_msg_forumname_regist');
				$this->displayView();
				return;
			} else {
				// フォーラム登録
				// 画像がアップロードされたか判定
				$_save_file = null;
				if (is_uploaded_file($_FILES["upload_imagefile"]["tmp_name"])) {
					// 画像アップロード処理
					$_save_file = $this->image_upload($cid, $fid);
					if (!is_null($this->error_message)) {
						$this->forummanage->deleteForum($fid);
						$this->displayView();
						return;
					}
				}

				$this->load->model('Forum_Model', 'forum', TRUE);
				$this->forum->registPost($fid, $postmsg, $_save_file);

				if (!is_null($this->error_message)) {
					$this->displayView();
					return;
				} else {
					$path = $this->commonlib->baseUrl().'forummanage?cid='.$cid."&pre=".$pre;
					if ($pre == "theme") {
						$thid = $this->input->get('thid', true);
						$path .= '&thid='.$thid;
					}
					redirect($path, 'refresh');
				}
			}
		} else {
			$this->displayView();
		}
	}

	/**
	 * 画像ファイルアップロード
	 *
	 * @param int $cid 講座ID
	 * @param int $fid フォーラムID
	 * @return string|NULL アップロード画像ファイルパス
	 */
	private function image_upload($cid, $fid) {
		// 画像アップロードパス取得
		$_upload_path = $this->config->item('FORUM_IMAGE_UPLOAD_PATH');
		if (!file_exists($_upload_path)) {
			mkdir($_upload_path, 0777, true);
		}
		// 画像ライブラリロード
		$_upload_config = array(
				'max_size' => $this->config->item('FORUM_IMAGE_UPLOAD_MAX_SIZE'),
				'encrypt_name' => true,
				'upload_path' => $_upload_path,
				'allowed_types' => implode("|", $this->config->item('FORUM_IMAGE_UPLOAD_EXTENSIONS'))
		);
		$this->load->library('upload', $_upload_config);

		// 画像アップロード
		if ($this->upload->do_upload('upload_imagefile')) {
			$file_info = $this->upload->data();
			// 保存先パス＋講座ID＋フォーラムID
			$_save_path = $this->config->item('FORUM_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$cid.DIRECTORY_SEPARATOR.$fid;
			if (!file_exists($_save_path)) {
				mkdir($_save_path, 0777, true);
			}
			// 保存先パス＋[ユーザーテーブルID]_yyyyMMddHHmmssSSS＋拡張子
			$_save_file_path = $_save_path.DIRECTORY_SEPARATOR.$this->authority_info[0]->id.'_'.$this->commonlib->getTimeWithMillSecond().$file_info['file_ext'];

			// リサイズ後の画像サイズ算出
			// 画像処理ライブラリロード
			$this->load->library('imagelib');
			$_resize_size = $this->imagelib->calResizeSize($file_info['full_path'], $this->config->item('FORUM_IMAGE_MAX_LENGTH'));

			// 画像リサイズ処理
			if (!is_null($_resize_size)) {
				$_image_config = array(
						'source_image' => $file_info['full_path'],
						'new_image' => $_save_file_path,
						'width' => $_resize_size[0],
						'height' => $_resize_size[1]
				);
				// CodeIgniterの画像処理ライブラリロード
				$this->load->library('image_lib', $_image_config);
				// リサイズ実行
				$this->image_lib->resize();
				// リサイズ元のアップロード画像削除
				unlink($file_info['full_path']);
			} else {
				// リサイズしなかった場合はファイルを保存先に移動
				rename($file_info['full_path'], $_save_file_path);
			}

			return $cid.'/'.$fid.'/'.basename($_save_file_path);
		} else {
			// 画像アップロードエラー
			// エラーメッセージ生成
			$this->error_message = $this->upload->display_errors();
		}
		return null;
	}

	/**
	 * フォーラム削除
	 */
	public function delete()
	{

		$cid = $this->input->post('cid', true);
		$pre = $this->input->post('pre', true);
		$fid = $this->input->post('fid', true);

		// フォーラム管理モデル
		$this->load->model('Forum_Manage_Model', 'forummanage', TRUE);

		$this->forummanage->deleteForum($fid);

		$path = $this->commonlib->baseUrl().'forummanage?cid='.$cid."&pre=".$pre;
		if ($pre == "theme") {
			$thid = $this->input->get('thid', true);
			$path .= '&thid='.$thid;
		}
		redirect($path, 'refresh');
	}

	/**
	 * フォーラム変更
	 */
	public function edit()
	{

		$cid = $this->input->post('cid', true);
		$pre = $this->input->post('pre', true);
		$fid = $this->input->post('fid', true);
		$action = $this->input->post('action', true);

		// フォーラム管理モデル
		$this->load->model('Forum_Manage_Model', 'forummanage', TRUE);

		if ($action == "stop")
		{
			$this->forummanage->stopForum($fid);
		}
			elseif ($action == "resumption")
		{
			$this->forummanage->resumptionForum($fid);
		}
		elseif ($action == "delete")
		{
			$this->forummanage->deleteForum($fid);
		}
		else
		{
			;
		}

		$path = $this->commonlib->baseUrl().'forummanage?cid='.$cid."&pre=".$pre;
		if ($pre == "theme") {
			$thid = $this->input->get('thid', true);
			$path .= '&thid='.$thid;
		}
//		redirect($path, 'refresh');
		redirect($path, 'location');
	}

	/**
	 * フォーラム管理画面表示データ生成
	 *
	 * @param int $cid 講座ID
	 * @param string $pre 遷移元画面判定文字列
	 * @param int $thid テーマID
	 * @return array フォーラム管理画面表示データ
	 */
	private function makeDispData($cid, $pre, $thid)
	{
		$id = $this->authority_info[0]->id;

		// 受講者モデル
		$this->load->model('Students_Model', 'students', TRUE);

		// 権限取得
		$auth = $this->students->getCourseAuth($cid, $id);

		// 講座モデル
		$this->load->model('Course_Model', 'course', TRUE);

		// 講座名取得
		$course_data = $this->course->getCourse($cid);

		// フォーラム管理モデル
		$this->load->model('Forum_Manage_Model', 'forummanage', TRUE);

		// フォーラム管理情報取得
		$forummanage_datas = $this->forummanage->getForumManageInfo($cid);

		// 出力情報設定
		$param['auth'] = $auth;
		$param['course_data'] = $course_data;
		$param['forummanage_data'] = $forummanage_datas;
		$param['script'] = $this->createJavaScript();
		$param['error_msg'] = $this->error_message;

		// 遷移用
		$param['pre'] = $pre;
		$param['thid'] = $thid;

		return $param;
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		$_script = <<<EOT
<script>
	var forumNameMax = {$this->forum_name_max};
	var errMsgForumNameRequired = "{$this->lang->line('forum_manage_script_err_msg_forumname_required')}";
	var errMsgForumNameMaxlength = "{$this->lang->line('forum_manage_script_err_msg_forumname_maxlength')}";
	var postmsgMax = {$this->postmsg_max};
	var errMsgPostMsgRequired = "{$this->lang->line('forum_manage_script_err_msg_postmsg_required')}";
	var errMsgPostMsgMaxlength = "{$this->lang->line('forum_manage_script_err_msg_postmsg_maxlength')}";
</script>
EOT;
		return $_script;
	}

}
