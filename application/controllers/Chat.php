<?php
/*
 * チャット画面コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends EX_Controller {

	/** 講座テーブルID. */
	private $cid = 0;
	/** ユーザーテーブルID. */
	private $uid = 0;
	/** チャット相手のユーザーテーブルID. */
	private $tkid = 0;
	/** 権限情報. */
	private $authority_info = null;
	/** エラーメッセージ. */
	private $error_message = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_CHAT')); // チャット画面

		$this->lang->load('chat_lang');
		$this->load->model("Profile_Model", 'profile_model', true);
		$this->load->model("Chat_Model", 'chat_model', true);
		// パラメータ取得
		$this->cid = $this->input->get('cid', true);
		$this->tkid = $this->input->get('tkid', true);
		// ユーザー情報取得
		$this->authority_info = $this->session->userdata('authority');
		$this->uid = $this->authority_info[0]->id;
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		// チャット相手プロフィール取得
		$_tk_profile = $this->profile_model->get_profile($this->input->get('tkid', true));
		$_view_data['tk_profile'] = $_tk_profile[0];

		// チャット初期データ取得
		$_chat_data = $this->chat_model->getChatDataList($this->cid, $this->uid, $this->tkid);
		$_view_data['chat_data'] = $_chat_data;

		$_view_data['script'] = $this->createJavaScript();

		$this->load->view('chat', $_view_data); // ※チャット画面はポップアップなので共通ヘッダー、共通フッターは使用しない
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		$_script = <<<EOT
<script>
	var dialogErrorTitle = "{$this->lang->line('chat_dialog_error_title')}";
	var dialogErrorBodyEmptyMsg = "{$this->lang->line('chat_dialog_error_body_empty_message')}";
	var dialogErrorBodyStrOverMsg = "{$this->lang->line('chat_dialog_error_body_str_over_message')}";
	var chatReloadInterval = "{$this->config->item('CHAT_RELOAD_INTERVAL')}";
</script>
EOT;
		return $_script;
	}

	/**
	 * チャット送信
	 */
	public function submit()
	{
		$_post = $this->input->post('chat_message', true);
		$_image_file = null;
		if (isset($_FILES["chat_upload_imagefile"])
		 && is_uploaded_file($_FILES["chat_upload_imagefile"]["tmp_name"])) {
			$_image_file = $this->imageUpload($this->cid, $this->uid, $this->tkid);
		}
		if (is_null($this->error_message)) {
			if ($this->chat_model->regist($this->cid, $this->uid, $this->tkid, $_post, $_image_file)) {
				$result["status"] = "success";
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode($result);
				exit;
			} else {
				$this->error_message = "Chat Submit Error.";
				$this->responseErrorResult("HTTP/1.0 500 Internal Server Error");
			}
		} else {
			$this->responseErrorResult("HTTP/1.0 414 URI Too Long");
		}
	}

	/**
	 * エラーレスポンス応答
	 *
	 * @param string $http_status_message HTTPステータスメッセージ
	 */
	private function responseErrorResult($http_status_message)
	{
		$result["status"] = "error";
		$result["error"] = $this->error_message;
		header($http_status_message);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
		exit;
	}

	/**
	 * 画像アップロード
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param int $uid ユーザーテーブルID
	 * @param int $tkid チャット相手のユーザーテーブルID
	 * @return string 保存した画像ファイルパス
	 */
	private function imageUpload($cid, $uid, $tkid)
	{
		// 画像アップロードパス取得
		$_upload_path = $this->config->item('CHAT_IMAGE_UPLOAD_PATH');
		if (!file_exists($_upload_path)) {
			mkdir($_upload_path, 0777, true);
		}
		// 画像ライブラリロード
		$_upload_config = array(
				'max_size' => $this->config->item('CHAT_IMAGE_UPLOAD_MAX_SIZE'),
				'encrypt_name' => true,
				'upload_path' => $_upload_path,
				'allowed_types' => implode("|", $this->config->item('CHAT_IMAGE_UPLOAD_EXTENSIONS'))
		);
		$this->load->library('upload', $_upload_config);

		// 画像アップロード
		if ($this->upload->do_upload('chat_upload_imagefile')) {
			$file_info = $this->upload->data();
			// 保存先ディレクトリ生成
			// 保存先パス＋講座情報テーブルID
			$_save_path = $this->config->item('CHAT_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$cid;
			if (!file_exists($_save_path)) {
				mkdir($_save_path, 0777, true);
			}
			// ファイル名：[ユーザーテーブルID]_[チャット相手のユーザーテーブルID]_yyyyMMddHHmmssSSS＋拡張子
			$_file_name = $uid.'_'.$tkid.'_'.$this->commonlib->getTimeWithMillSecond().$file_info['file_ext'];
			// 保存先パス＋ファイル名
			$_save_file_path = $_save_path.DIRECTORY_SEPARATOR.$_file_name;

			// リサイズ後の画像サイズ算出
			// 画像処理ライブラリロード
			$this->load->library('imagelib');
			$_resize_size = $this->imagelib->calResizeSize($file_info['full_path'], $this->config->item('CHAT_IMAGE_MAX_LENGTH'));

			// 画像リサイズ処理
			if (!is_null($_resize_size)) {
				$_image_config = array(
						'source_image' => $file_info['full_path'],
						'new_image' => $_save_file_path,
						'width' => $_resize_size[0],
						'height' => $_resize_size[1]
				);
				// CodeIgniterの画像処理ライブラリロード
				$this->load->library('image_lib', $_image_config);
				// リサイズ実行
				$this->image_lib->resize();
				// リサイズ元のアップロード画像削除
				unlink($file_info['full_path']);
			} else {
				// リサイズしなかった場合はファイルを保存先に移動
				rename($file_info['full_path'], $_save_file_path);
			}

			return $cid.'/'.basename($_save_file_path);
		} else {
			// 画像アップロードエラー
			// エラーメッセージ生成
			$this->error_message = $this->upload->display_errors();
		}
		return null;

	}

	/**
	 * チャットデータ取得
	 */
	public function getdata()
	{
		$_last_id = $this->input->get('last_id', true);
		$_chat_data = $this->chat_model->getChatDataList($this->cid, $this->uid, $this->tkid, false, $_last_id, true);
		$_json_data = null;
		if (!is_null($_chat_data)) {
			$_json_data = array();
			$_json_data['chat_data'] = $_chat_data;
			$_json_data['last_id'] = $_chat_data[count($_chat_data)-1]['id'];
		}
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($_json_data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
		exit;
	}

	/**
	 * チャット過去データ取得
	 */
	public function getpastdata()
	{
		$_first_id = $this->input->get('first_id', true);
		$_chat_data = $this->chat_model->getPastChatDataList($this->cid, $this->uid, $this->tkid, $_first_id);
		$_json_data = null;
		if (!is_null($_chat_data)) {
			$_json_data = array();
			$_json_data['chat_data'] = $_chat_data;
			$_json_data['first_id'] = $_chat_data[0]['id'];
		}
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($_json_data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
		exit;
	}

}
