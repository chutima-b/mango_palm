<?php
/*
 * ホーム画面(管理者用)コントローラー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class StaffHome extends EX_Controller {

	/** お知らせタイトル最大文字数. */
	private $noticetitle_max = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_STAFF_HOME')); // ホーム画面（管理者）
		$this->lang->load('staff_home_lang');
		$this->setPageTitle($this->lang->line('staff_home_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link rel="stylesheet" type="text/css" href="{$_base_url}libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css">
<link href="{$_base_url}libs/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet">
<link href="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.css?v=1.0.0" rel="stylesheet">
<link href="{$_base_url}libs/summernote/summernote.css" rel="stylesheet">
<link href="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker.css" rel="stylesheet">
<link href="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker-regularfont.css" rel="stylesheet">
<link href="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker-glyphicons.css" rel="stylesheet">
<link href="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker-fontawesome.css" rel="stylesheet">
<link href="{$_base_url}css/staffhome.css?v=1.0.2" rel="stylesheet">
EOT;
		$this->addCssFiles($_add_css);
		$_summernote_lang_file = $this->config->item('SUMMERNOTE_LANG_FILE');
		$_summernote_lang_script = "";
		if (!empty($_summernote_lang_file)) {
			$_summernote_lang_script = "<script src=\"{$_base_url}libs/summernote/lang/{$this->config->item('SUMMERNOTE_LANG_FILE')}\"></script>";
		}
		$_add_script =<<<EOT
<script src="{$_base_url}libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="{$_base_url}libs/bootstrap-datepicker/locales/bootstrap-datepicker.ja.min.js"></script>
<script src="{$_base_url}libs/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="{$_base_url}libs/summernote/summernote.custom.js"></script>
{$_summernote_lang_script}
<script src="{$_base_url}libs/jqueryvalidation/jquery.validate.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.js?v=1.0.0"></script>
<script src="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker.js"></script>
<script src="{$_base_url}js/staffhome.js?v=1.0.3"></script>
EOT;
		$this->addScriptFiles($_add_script);

		// お知らせタイトル最大文字数
		$this->noticetitle_max = $this->config->item('STAFFHOME_NOTICETITLE_MAXLENGTH');
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * お知らせ編集
	 */
	public function noticeedit()
	{
		// パラメータ取得
		$nid = $this->input->post('nid', true);
		$targetdt = $this->input->post('targetdt', true);
		$title = $this->input->post('title', true);
		// WYSIWYG対応のためCodeigniterではXSS対策は実施せずに、専用関数で除去する。
		$content = $this->commonlib->removeXssForWYSIWYG($this->input->post('content'));
		$course = $this->input->post('course', true);
		$calendar_use = $this->input->post('calendar_use', true);
		$color = $this->input->post('color', true);

		$this->form_validation->set_rules('title', $this->lang->line('staff_home_notice_col_title'), 'trim|required|max_length['.$this->noticetitle_max.']');
		$this->form_validation->set_rules('content', $this->lang->line('staff_home_notice_col_content'), 'trim|required');
		$this->form_validation->set_rules('course', $this->lang->line('staff_home_notice_col_course'), 'trim|required');
		if (!is_null($this->input->post("calendar_use", true))) {
			// 「カレンダーに表示する」がONの場合は、日付、カレンダー（色）もバリデーションする。
			$this->form_validation->set_rules('targetdt', $this->lang->line('staff_home_notice_col_date'), 'trim|required');
			$this->form_validation->set_rules('color', $this->lang->line('staff_home_notice_col_color'), 'trim|required');
		} else {
			// 「カレンダーに表示する」がOFFの場合
			// カレンダー使用フラグは'0'に設定
			$calendar_use = '0';
			// カレンダー（色）は未設定にしておく
			$color = null;
		}

		if ($this->form_validation->run()) {

			// お知らせモデル
			$this->load->model('Notice_Model', 'notice', true);

			if ($nid == null or $nid == "") {
				$this->notice->insertNotice($targetdt, $title, $content, $course, $calendar_use, $color);
			} else {
				$this->notice->updateNotice($nid, $targetdt, $title, $content, $course, $calendar_use, $color);
			}

			redirect($this->homeUrl());
		} else {
			$this->displayView();
		}
	}

	/**
	 * お知らせ「内容」画像アップロード
	 */
	public function contentimg()
	{
		if (isset($_FILES["contentimg"])
		&& is_uploaded_file($_FILES["contentimg"]["tmp_name"])) {
				// 画像アップロードパス取得
			$_upload_path = $this->config->item('STAFFHOME_CONTENT_IMAGE_UPLOAD_PATH');
			if (!file_exists($_upload_path)) {
				mkdir($_upload_path, 0777, true);
			}
			// 画像ライブラリロード
			$_upload_config = array(
					'max_size' => $this->config->item('STAFFHOME_CONTENT_IMAGE_UPLOAD_MAX_SIZE'),
					'encrypt_name' => true,
					'upload_path' => $_upload_path,
					'allowed_types' => implode("|", $this->config->item('STAFFHOME_CONTENT_IMAGE_UPLOAD_EXTENSIONS'))
			);
			$this->load->library('upload', $_upload_config);

			// 画像アップロード
			if ($this->upload->do_upload('contentimg')) {
				$file_info = $this->upload->data();
				// 保存先ディレクトリ生成
				// 保存先パス
				$_save_path = $this->config->item('STAFFHOME_CONTENT_IMAGE_SAVE_PATH');
				if (!file_exists($_save_path)) {
					mkdir($_save_path, 0777, true);
				}
				// 保存先パス＋_yyyyMMddHHmmssSSS＋拡張子
				$_filename = $this->commonlib->getTimeWithMillSecond().$file_info['file_ext'];
				$_save_file_path = $_save_path.DIRECTORY_SEPARATOR.$_filename;

				// リサイズ後の画像サイズ算出
				// 画像処理ライブラリロード
				$this->load->library('imagelib');
				$_resize_size = $this->imagelib->calResizeSize($file_info['full_path'], $this->config->item('STAFFHOME_CONTENT_IMAGE_MAX_LENGTH'));

				// 画像リサイズ処理
				if (!is_null($_resize_size)) {
					$_image_config = array(
						'source_image' => $file_info['full_path'],
						'new_image' => $_save_file_path,
						'width' => $_resize_size[0],
						'height' => $_resize_size[1]
					);
					// CodeIgniterの画像処理ライブラリロード
					$this->load->library('image_lib', $_image_config);
					// リサイズ実行
					$this->image_lib->resize();
					// リサイズ元のアップロード画像削除
					unlink($file_info['full_path']);
				} else {
					// リサイズしなかった場合はファイルを保存先に移動
					rename($file_info['full_path'], $_save_file_path);
				}
				echo $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_NOTICE')."&fl=".$_filename;
				exit;
			} else {
				// 画像アップロードエラー
				// エラーメッセージ生成
				header("HTTP/1.0 404 Not Found");
				echo $this->upload->display_errors('<div class="alert alert-danger" role="alert">', '</div>');
				exit;
			}
		}
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		$authority = $this->session->userdata['authority'];
		$auth = $authority[0]->authority;

		// お知らせモデル
		$this->load->model('Notice_Model', 'notice', true);

		// お知らせ情報取得
		$notice_data = $this->notice->getNoticeList();

		// 講座情報モデル
		$this->load->model('Course_Model', 'course', true);

		// 講座件数取得
		$course_count = $this->course->getStaffHomeCourseCount();

		// ジャンル別講座件数取得
		$genre_course_count = $this->course->getStaffHomeGenreCourseCount();

		// 講座情報取得
		$course_data = $this->course->getStaffHomeCourseList();

		// お知らせ編集ダイアログ用講座情報取得
		$notice_course_list = $this->course->getCourseListForNoticeCourse();

		// 情報設定
		$params['auth'] = $auth;
		$params['notice_data'] = $notice_data;
		$params['course_count'] = $course_count;
		$params['genre_course_count'] = $genre_course_count;
		$params['course_data'] = $course_data;
		$params['notice_course_list'] = $notice_course_list;
		$params['colorpicker_colors'] = $this->config->item('SIMPLECOLORPICKER_OPTIONTAG_COLORS');
		$params['script'] = $this->createJavaScript();

		$this->load->view('common_header');       // 共通ヘッダー部
		$this->load->view('staff_home', $params); // コンテンツ部
		$this->load->view('common_footer');       // 共通フッター部
	}

	/**
	 * お知らせ削除
	 */
	public function noticedelete()
	{
		// パラメータ取得
		$nid = $this->input->post('nid', true);

		// お知らせモデル
		$this->load->model('Notice_Model', 'notice', true);

		// お知らせ削除
		$this->notice->deleteNotice($nid);

		redirect($this->homeUrl());
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		// カラーピッカーのデフォルトカラーは先頭に定義されている色を設定する
		$simplecolorpicker_optiontag_colors = $this->config->item('SIMPLECOLORPICKER_OPTIONTAG_COLORS');
		reset($simplecolorpicker_optiontag_colors);
		$colorpicker_default_color = key($simplecolorpicker_optiontag_colors);

		$_script = <<<EOT
<script>
	var summernoteLang = "{$this->config->item('SUMMERNOTE_LANG')}";
	var datepickerLang = "{$this->config->item('DATEPICKER_LANG')}";
	var noticeTitleMax = {$this->noticetitle_max};
	var errMsgNoticeTitleRequired = "{$this->lang->line('staff_home_script_err_msg_noticetitle_required')}";
	var errMsgNoticeTitleMaxlength = "{$this->lang->line('staff_home_script_err_msg_noticetitle_maxlength')}";
	var errMsgNoticeContentRequired = "{$this->lang->line('staff_home_script_err_msg_noticecontent_required')}";
	var errMsgNoticeTargetdtRequired = "{$this->lang->line('staff_home_script_err_msg_noticetargetdt_required')}";
	var errMsgNoticeCourseRequired = "{$this->lang->line('staff_home_script_err_msg_noticecourse_required')}";
	var errMsgNoticeColorRequired = "{$this->lang->line('staff_home_script_err_msg_noticecolor_required')}";
	var colorpickerDefaultColor = "{$colorpicker_default_color}";
</script>
EOT;
		return $_script;
	}

}
