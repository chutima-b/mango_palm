<?php
/*
 * プロフィール画面コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends EX_Controller {
	/** ビュー画面へ渡すデータ. */
	private $view_data = null;
	/** エラーメッセージ. */
	private $error_message = null;
	/** 権限情報. */
	private $authority_info = null;
	/** ニックネーム最大文字数. */
	private $nickname_max = null;
	/** 自己紹介最大文字数. */
	private $introduction_max = null;
	/** パスワード最小文字数. */
	private $passwd_min = null;
	/** パスワード最大文字数. */
	private $passwd_max = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_PROFILE')); // プロフィール画面
		$this->lang->load('profile_lang');
		$this->setPageTitle($this->lang->line('profile_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link href="{$_base_url}libs/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet">
<link href="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.css?v=1.0.0" rel="stylesheet">
<link rel="stylesheet" href="{$_base_url}css/profile.css?v=1.0.2">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}libs/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validate.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.js?v=1.0.0"></script>
<script src="{$_base_url}js/profile.js?v=1.0.1"></script>
EOT;
		$this->addScriptFiles($_add_script);

		$this->load->model("Profile_Model", 'profile_model', true);
		$this->view_data = array('home_url' => $this->homeUrl());
		// ユーザー情報取得
		$this->authority_info = $this->session->userdata('authority');
		// ニックネーム最大文字数
		$this->nickname_max = $this->config->item('PROFILE_NICKNAME_MAXLENGTH');
		// 自己紹介最大文字数
		$this->introduction_max = $this->config->item('PROFILE_INTRODUCTION_MAXLENGTH');
		// パスワード最小文字数
		$this->passwd_min = $this->config->item('PROFILE_PASSWD_MINLENGTH');
		// パスワード最大文字数
		$this->passwd_max = $this->config->item('PROFILE_PASSWD_MAXLENGTH');
	}

	/**
	 * 初期表示画面
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * プロフィール登録
	 */
	public function regist()
	{
		$this->form_validation->set_rules('profile_nickname', $this->lang->line('profile_nickname'), 'trim|required|max_length['.$this->nickname_max.']');
		$this->form_validation->set_rules('profile_introduction', $this->lang->line('profile_introduction'), 'max_length['.$this->introduction_max.']');
		$_new_password = null;
		if ($this->input->post("profile_new_passwd", true)) {
			// 新しいパスワードの入力がある場合は入力チェックを行う
			// 現在のパスワード（未入力、登録されているパスワードと一致しているか）
			$this->form_validation->set_rules('profile_passwd', $this->lang->line('profile_passwd'), 'trim|required|min_length['.$this->passwd_min.']|max_length['.$this->passwd_max.']|callback_check_present_password');
			// 新しいパスワード（入力文字列、最小文字数、最大文字数）
			$this->form_validation->set_rules('profile_new_passwd', $this->lang->line('profile_new_passwd'), 'trim|min_length['.$this->passwd_min.']|max_length['.$this->passwd_max.']|callback_check_new_password_string');
			// 新しいパスワードの確認（未入力、新しいパスワードと一致しているか）
			$this->form_validation->set_rules('profile_new_passwd_confirm', $this->lang->line('profile_new_passwd_confirm'), 'trim|required|min_length['.$this->passwd_min.']|max_length['.$this->passwd_max.']|matches[profile_new_passwd]');
			// パスワード更新用変数に設定
			$_new_password = $this->input->post("profile_new_passwd", true);
		}

		$_view_data = array();
		if ($this->form_validation->run()) {
			// 画像がアップロードされたか判定
			$_save_file = null;
			if (is_uploaded_file($_FILES["profile_imgupload"]["tmp_name"])) {
				// 画像アップロード処理
				$_save_file = $this->image_upload();
				if (!is_null($this->error_message)) {
					$this->view_data['error_msg'] = $this->error_message;
					$this->displayView();
					return;
				}
			}

			// 画像ファイルがnullの場合
			if (is_null($_save_file)) {
				// 更新前の情報取得
				$_db_profile = $this->profile_model->get_profile($this->authority_info[0]->id);
				if (!empty($_db_profile[0]->picture_file)) {
					// 更新前に画像が既に登録されている場合はそのまま登録し直す
					$_save_file = $_db_profile[0]->picture_file;
				}
			}

			// プロフィール更新
			$this->profile_model->update_profile(
				$this->authority_info[0]->id,
				$this->input->post("profile_nickname", true),
				$_save_file,
				$this->input->post("profile_introduction", true),
				($this->input->post("profile_chat_use", true) == '1' ? 1 : 0),
				$_new_password
			);

			// セッション情報更新
			$this->authority_info[0]->nickname = $this->input->post("profile_nickname", true);
			$this->session->set_userdata("authority", $this->authority_info);

			if (!is_null($this->error_message)) {
				$this->view_data['error_msg'] = $this->error_message;
			} else {
				redirect($this->homeUrl());
			}
		}

		$this->displayView();
	}

	/**
	 * 現在のパスワード判定
	 */
	public function check_present_password()
	{
		if (!$this->profile_model->checkPassword(
				$this->authority_info[0]->id, $this->input->post("profile_passwd", true))) {
			$this->form_validation->set_message("check_present_password", $this->lang->line('profile_wrong_passwd'));
			return false;
		}
		return true;
	}

	/**
	 * 新しいパスワード文字列チェック
	 */
	public function check_new_password_string()
	{
		if (!$this->commonlib->checkPasswordString($this->input->post("profile_new_passwd", true))) {
			$this->form_validation->set_message("check_new_password_string", $this->lang->line('profile_alpha_numeric_new_passwd'));
			return false;
		}
		return true;
	}

	/**
	 * 画像アップロード
	 *
	 * return string 保存した画像ファイル名
	 */
	private function image_upload() {
		// 画像アップロードパス取得
		$_upload_path = $this->config->item('PROFILE_IMAGE_UPLOAD_PATH');
		if (!file_exists($_upload_path)) {
			mkdir($_upload_path, 0777, true);
		}
		// 画像ライブラリロード
		$_upload_config = array(
			'max_size' => $this->config->item('PROFILE_IMAGE_UPLOAD_MAX_SIZE'),
			'encrypt_name' => true,
			'upload_path' => $_upload_path,
			'allowed_types' => implode("|", $this->config->item('PROFILE_IMAGE_UPLOAD_EXTENSIONS'))
		);
		$this->load->library('upload', $_upload_config);

		// 画像アップロード
		if ($this->upload->do_upload('profile_imgupload')) {
			$file_info = $this->upload->data();
			// 保存先ディレクトリ生成
			$_save_path = $this->config->item('PROFILE_IMAGE_SAVE_PATH');
			if (!file_exists($_save_path)) {
				mkdir($_save_path, 0777, true);
			}
			// 保存先ファイル名生成（拡張子無し）
			// 保存先パス＋ユーザーテーブルID
			$_save_file_path_tmp = $_save_path.DIRECTORY_SEPARATOR.$this->authority_info[0]->id;
			// 古い保存先ファイルを削除しておく
			$this->deleteSaveFile($_save_file_path_tmp);
			// 保存先パス＋ユーザーテーブルID＋拡張子
			$_save_file_path = $_save_file_path_tmp.$file_info['file_ext'];

			// 画像処理ライブラリロード
			$this->load->library('imagelib');

			// 正方形にトリミング＆リサイズ
			$this->imagelib->resizeSquare($this, $file_info['full_path'], $_save_file_path, $this->config->item('PROFILE_IMAGE_MAX_LENGTH'));

			return basename($_save_file_path);
		} else {
			// 画像アップロードエラー
			// エラーメッセージ生成
			$this->error_message = $this->upload->display_errors('<div class="alert alert-danger" role="alert">', '</div>');
		}
		return null;
	}

	/**
	 * 保存先ファイル削除.
	 *
	 * @param string 保存先ファイル名（拡張子無し）
	 */
	private function deleteSaveFile($save_file_name_ex_ext)
	{
		$_array_ext = $this->config->item('PROFILE_IMAGE_UPLOAD_EXTENSIONS');
		foreach ($_array_ext as $_ext) {
			$_delete_file = $save_file_name_ex_ext.'.'.$_ext;
			if (file_exists($_delete_file)) {
				unlink($_delete_file);
			}
		}
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		// プロフィール設定
		// デフォルト表示値設定
		$_profile = new stdClass();
		$_profile->nickname = '';
		$_profile->introduction = '';
		$_profile->chat_use = false;
		$_profile->picture_file = $this->commonlib->baseUrl().$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
		// DBからプロフィール取得
		$_db_profile = $this->profile_model->get_profile($this->authority_info[0]->id);
		if (!is_null($_db_profile)) {
			// DBから取得できた場合は値を設定し直す
			$_profile->nickname = $_db_profile[0]->nickname;
			$_profile->introduction = $_db_profile[0]->introduction;
			$_profile->chat_use = $_db_profile[0]->chat_use == 1 ? true : false;
			if (!empty($_db_profile[0]->picture_file)) {
				$_profile->picture_file = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=".$this->authority_info[0]->id;
			}
		}
		// ビューのデータに設定
		$this->view_data['profile'] = $_profile;
		$this->view_data['script'] = $this->createJavaScript();
		$this->view_data['introduction_max'] = $this->introduction_max;
		// 戻る画面設定
		$this->setBackUrl($this->homeUrl());
		// ビュー表示
		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('profile', $this->view_data); // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		$_script = <<<EOT
<script>
	var nicknameMax = {$this->nickname_max};
	var introductionMax = {$this->introduction_max};
	var passwdMin = "{$this->passwd_min}";
	var passwdMax = "{$this->passwd_max}";
	var errMsgNicknameRequired = "{$this->lang->line('profile_script_err_msg_nickname_required')}";
	var errMsgNicknameMaxlength = "{$this->lang->line('profile_script_err_msg_nickname_maxlength')}";
	var errMsgIntroductionMaxlength = "{$this->lang->line('profile_script_err_msg_introduction_maxlength')}";
	var errMsgPasswdRequired = "{$this->lang->line('profile_script_err_msg_passwd_required')}";
	var errMsgNewPasswdConfirmRequired = "{$this->lang->line('profile_script_err_msg_new_passwd_confirm_required')}";
	var errMsgNewPasswdConfirmEqualto = "{$this->lang->line('profile_script_err_msg_new_passwd_confirm_equalto')}";
	var errMsgMaxlength = "{$this->lang->line('profile_script_err_msg_maxlength')}";
	var errMsgMinlength = "{$this->lang->line('profile_script_err_msg_minlength')}";
	var pictureMaxLength = "{$this->config->item('PROFILE_IMAGE_MAX_LENGTH')}px";
</script>
EOT;
		return $_script;
	}

}
