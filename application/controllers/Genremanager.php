<?php
/*
 * システム管理（ジャンル）コントローラー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Genremanager extends EX_Controller {

	/** 現在のページ. */
	private $page = 0;
	/** ジャンルID最大文字数. */
	private $genreid_max = null;
	/** ジャンル名最大文字数. */
	private $genrename_max = null;
	/** エラーメッセージ. */
	private $error_message = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_GENREMANAGER')); // システム管理（ジャンル）画面
		$this->lang->load('genremanager_lang');
		$this->setPageTitle($this->lang->line('genremanager_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link href="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.css?v=1.0.0" rel="stylesheet">
<link href="{$_base_url}css/slms_loading.css?v=1.0.0" rel="stylesheet">
<link rel="stylesheet" href="{$_base_url}css/genremanager.css?v=1.0.1">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}libs/jqueryvalidation/jquery.validate.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.js?v=1.0.0"></script>
<script src="{$_base_url}js/slms_loading.js?v=1.0.0"></script>
<script src="{$_base_url}js/genremanager.js?v=1.0.2"></script>
EOT;
		$this->addScriptFiles($_add_script);

		// ジャンルID最大文字数
		$this->genreid_max = $this->config->item('GENREMANAGER_GENREID_MAXLENGTH');
		// ジャンル名最大文字数
		$this->genrename_max = $this->config->item('GENREMANAGER_GENRENAME_MAXLENGTH');

		// パラメータ取得
		$this->page = $this->input->get_post('page', true);
		if (is_null($this->page) || !is_numeric($this->page) || $this->page < 1) {
			$this->page = 1;
		}
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * ジャンル登録処理
	 */
	public function regist() {
		$this->form_validation->set_rules('genreid', $this->lang->line('genremanager_regist_item_genreid'), 'trim|required|alpha_dash|max_length['.$this->genreid_max.']');
		$this->form_validation->set_rules('genrename', $this->lang->line('genremanager_regist_item_genrename'), 'trim|required|max_length['.$this->genrename_max.']');

		if ($this->form_validation->run()) {
			$gid = $this->input->post('gid', true);
			$genreid = $this->input->post('genreid', true);
			$genrename = $this->input->post('genrename', true);
			$delete_icon = false;
			if (!is_null($this->input->post("delete_icon"))) {
				$delete_icon = true;
			}

			$_save_file = null;
			if (!$delete_icon) {
				// アイコン画像がアップロードされたか判定
				if (is_uploaded_file($_FILES["icon"]["tmp_name"])) {
					// アイコン画像アップロード処理
					$_save_file = $this->genre_icon_upload();
				}
				if (!is_null($this->error_message)) {
					$this->displayView();
					return;
				}
			}

			// ジャンルモデル
			$this->load->model('Genre_Model', 'genre', TRUE);

			$ret = 0;
			if ($gid == null or $gid == "") {
				$ret = $this->genre->regist($genreid, $genrename, $_save_file);
				$gid = $this->genre->id;
				// アイコン画像ファイルリネーム
				if (!is_null($_save_file)) {
					$_extension = substr($_save_file, strrpos($_save_file, '.') + 1);
					$_icon_save_path = $this->config->item('GENREMANAGER_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR;
					$_destFile = $gid.'.'.$_extension;
					$_destPath = $_icon_save_path.$_destFile;
					rename($_icon_save_path.$_save_file, $_destPath);
					// ジャンルテーブルの画像パス更新
					if (!$this->genre->updateIconFile($_destFile, $gid)) {
						$this->error_message = $this->lang->line('genremanager_error_msg_update_icon_file');
					}
					if (!is_null($this->error_message)) {
						$this->displayView();
						return;
					}
				}
			} else {
				$ret = $this->genre->update($gid, $genreid, $genrename, $_save_file, $delete_icon);
			}

			if ($ret == 0) {
				redirect($this->commonlib->baseUrl().'genremanager', 'location');
			} else {
				redirect($this->commonlib->baseUrl().'genremanager?err='.$ret, 'location');
			}
		} else {
			$this->displayView();
		}
	}

	/**
	 * アイコン画像アップロード
	 *
	 * return string 保存した画像ファイル名
	 */
	private function genre_icon_upload() {
		// 画像アップロードパス取得
		$_upload_path = $this->config->item('GENREMANAGER_IMAGE_UPLOAD_PATH');
		if (!file_exists($_upload_path)) {
			mkdir($_upload_path, 0777, true);
		}
		// 画像ライブラリロード
		$_upload_config = array(
				'max_size' => $this->config->item('GENREMANAGER_IMAGE_UPLOAD_MAX_SIZE'),
				'encrypt_name' => true,
				'upload_path' => $_upload_path,
				'allowed_types' => implode("|", $this->config->item('GENREMANAGER_IMAGE_UPLOAD_EXTENSIONS'))
		);
		$this->load->library('upload', $_upload_config);

		// 画像アップロード
		if ($this->upload->do_upload('icon')) {
			$file_info = $this->upload->data();
			// 保存先ディレクトリ生成
			$_save_path = $this->config->item('GENREMANAGER_IMAGE_SAVE_PATH');
			if (!file_exists($_save_path)) {
				mkdir($_save_path, 0777, true);
			}
			// 保存先ファイル名生成（拡張子無し）
			// 保存先パス＋ジャンルテーブルID
			$_gid = $this->commonlib->getTimeWithMillSecond(); // 新規の場合はジャンルIDが無いため、一時的に現在日時を設定（登録後にファイルリネームする）
			if ($this->input->post("gid", true)) {
				// 更新の場合は正式なジャンルDを取得
				$_gid = $this->input->post("gid", true);
			}
			$_save_file_path_tmp = $_save_path.DIRECTORY_SEPARATOR.$_gid;
			// 古い保存先ファイルを削除しておく
			$this->deleteSaveUserPictureFile($_gid);
			// 保存先パス＋アイコンテーブルID＋拡張子
			$_save_file_path = $_save_file_path_tmp.$file_info['file_ext'];

			// 画像処理ライブラリロード
			$this->load->library('imagelib');

			// 正方形にトリミング＆リサイズ
			$this->imagelib->resizeSquare($this, $file_info['full_path'], $_save_file_path, $this->config->item('GENREMANAGER_IMAGE_MAX_LENGTH'));

			return basename($_save_file_path);
		} else {
			// 画像アップロードエラー
			// エラーメッセージ生成
			$this->error_message = $this->upload->display_errors();
		}
		return null;
	}

	/**
	 * アイコン画像保存先ファイル削除.
	 *
	 * @param string $gid ジャンルID
	 */
	private function deleteSaveUserPictureFile($gid)
	{
		$save_file_name_ex_ext = $this->config->item('GENREMANAGER_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$gid;
		$_array_ext = $this->config->item('GENREMANAGER_IMAGE_UPLOAD_EXTENSIONS');
		foreach ($_array_ext as $_ext) {
			$_delete_file = $save_file_name_ex_ext.'.'.$_ext;
			if (file_exists($_delete_file)) {
				unlink($_delete_file);
			}
		}
	}

	/**
	 * ビュー表示
	 */
	private function displayView() {
		$err = $this->input->get('err', true);

		// ジャンルモデル
		$this->load->model('Genre_Model', 'genre', TRUE);

		// 全ジャンル数取得
		$genre_cnt = $this->genre->getGenreCount();

		// 講座情報モデル
		$this->load->model('Course_Model', 'course', TRUE);

		// 全コース数取得
		$course_cnt = $this->course->getAllCourseCount();

		// ジャンルリスト取得
		$_genre_disp_count = $this->config->item('GENREMANAGER_DISP_COUNT');
		$genre_data = $this->genre->getAllGenreInfoList($this->page, $_genre_disp_count);
		if ($this->page > 1 && is_null($genre_data)) {
			$this->page--;
			$genre_data = $this->genre->getAllGenreInfoList($this->page, $_genre_disp_count);
		}

		// ジャンルリスト表示開始数（件目）
		$_start_cnt = ($this->page - 1) * $_genre_disp_count + 1;
		if (count($genre_data) == 0) {
			$_start_cnt = 0;
		}

		$_base_page_url = $this->commonlib->baseUrl().'genremanager?err='.$err;

		// 前ページURL
		$_before_page = null;
		if ($_start_cnt > 1) {
			$_before_page = $_base_page_url.'&page='.($this->page - 1);

		}

		// 全ページ数算出
		$_total_pages = ceil($genre_cnt / $_genre_disp_count);

		// 次ページURL
		$_next_page = null;
		if ($this->page < $_total_pages) {
			$_next_page = $_base_page_url.'&page='.($this->page + 1);
		}

		$error_msg = null;
		if ($err == 1) {
			$error_msg = $this->lang->line('genremanager_error_msg_duplication_1');
		} elseif ($err == 2) {
			$error_msg = $this->lang->line('genremanager_error_msg_duplication_2');
		} elseif ($err == 3) {
			$error_msg = $this->lang->line('genremanager_error_msg_duplication_3');
		}

		// 戻る画面設定
		$this->setBackUrl($this->commonlib->baseUrl()."usermanager");

		// 表示情報設定
		$param['before_page'] = $_before_page;
		$param['next_page'] = $_next_page;
		$param['page'] = $this->page;
		$param['error_msg'] = $error_msg;
		$param['genre_cnt'] = $genre_cnt;
		$param['course_cnt'] = $course_cnt;
		$param['genre_data'] = $genre_data;
		$param['icon_default_image'] = $this->commonlib->baseUrl().$this->config->item('GENREMANAGER_DEFAULT_IMAGE_URL');
		$param['script'] = $this->createJavaScript($_before_page, $_next_page);
		if (!is_null($this->error_message)) {
			$param['error_msg'] = $this->error_message;
		}

		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('genremanager', $param);        // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * ジャンル削除処理
	 */
	public function delete() {

		$gid = $this->input->post('gid', true);

		// ジャンル削除
		$this->load->model('Genre_Model', 'genre', TRUE);
		$this->genre->delete($gid);
		// アイコンファイル削除
		$this->deleteSaveUserPictureFile($gid);

		redirect($this->commonlib->baseUrl().'genremanager', 'location');
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript($before_page, $next_page)
	{
		$_befor_page_url = '';
		if (!is_null($before_page)) {
			$_befor_page_url = $before_page;
		}
		$_next_page_url = '';
		if (!is_null($next_page)) {
			$_next_page_url = $next_page;
		}
		$_default_icon = $this->commonlib->baseUrl().'/'.$this->config->item('GENREMANAGER_DEFAULT_IMAGE_URL');
		$_script = <<<EOT
<script>
	var beforePageUrl = "{$_befor_page_url}";
	var nextPageUrl = "{$_next_page_url}";
	var genreIdMax = {$this->genreid_max};
	var genreNameMax = {$this->genrename_max};
	var errMsgGenreIdRequired = "{$this->lang->line('genremanager_script_err_msg_genreid_required')}";
	var errMsgGenreIdMaxlength = "{$this->lang->line('genremanager_script_err_msg_genreid_maxlength')}";
	var errMsgGenreNameRequired = "{$this->lang->line('genremanager_script_err_msg_genrename_required')}";
	var errMsgGenreNameMaxlength = "{$this->lang->line('genremanager_script_err_msg_genrename_maxlength')}";
	var defaultIcon = "{$_default_icon}";
	var defaultIconMaxLength = "{$this->config->item('GENREMANAGER_IMAGE_MAX_LENGTH')}px";
</script>
EOT;
		return $_script;
	}

}
