<?php
/*
 * 日記画面コントローラー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Diary extends EX_Controller {

	/** 講座情報テーブルID. */
	private $cid = null;
	/** テーマテーブルID. */
	private $thid = null;
	/** 日記テーブルID. */
	private $did = null;
	/** 戻り先パラメータ. */
	private $pre = null;
	/** 現在のページ. */
	private $page = 0;
	/** 投稿メッセージ最大文字数. */
	private $diarymsg_max = null;
	/** 権限情報. */
	private $authority_info = null;
	/** エラーメッセージ. */
	private $error_message = null;
	/** ビュー画面へ渡すデータ. */
	private $view_data = array();

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_DIARY')); // 日記画面
		$this->lang->load('diary_lang');
		$this->setPageTitle($this->lang->line('diary_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css =<<<EOT
<link href="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.css?v=1.0.0" rel="stylesheet">
<link href="{$_base_url}css/slms_loading.css?v=1.0.0" rel="stylesheet">
<link href="{$_base_url}css/diary.css?v=1.0.3" rel="stylesheet">
<link href="{$_base_url}css/diary_like.css?v=1.0.0" rel="stylesheet">
EOT;
		$this->addCssFiles($_add_css);
		$_add_script =<<<EOT
<script src="{$_base_url}libs/jqueryvalidation/jquery.validate.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.js?v=1.0.0"></script>
<script src="{$_base_url}js/slms_loading.js?v=1.0.0"></script>
<script src="{$_base_url}js/diary.js?v=1.0.1"></script>
<script src="{$_base_url}js/diary_like.js?v=1.0.1"></script>
EOT;
		$this->addScriptFiles($_add_script);

		// 投稿メッセージ最大文字数
		$this->diarymsg_max = $this->config->item('DIARY_MESSAGE_MAXLENGTH');
		// ユーザー情報取得
		$this->authority_info = $this->session->userdata('authority');

		// パラメータ取得
		$this->cid = $this->input->get_post('cid', true);
		$this->thid = $this->input->get_post('thid', true);
		$this->did = $this->input->get_post('did', true);
		$this->pre = $this->input->get_post('pre', true);
		$this->page = $this->input->get_post('page', true);
		if (is_null($this->page) || !is_numeric($this->page) || $this->page < 1) {
			$this->page = 1;
		}
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		$this->load->model("Students_Model", 'students_model', true);
		$_course_auth = $this->students_model->getCourseAuth($this->cid, $this->authority_info[0]->id);

		// 戻る画面設定
		if ($this->pre == "home") {
			$this->setBackUrl($this->commonlib->baseUrl()."studentshome");
		} else if ($this->pre == "theme") {
			$this->setBackUrl($this->commonlib->baseUrl()."theme?cid=".$this->cid."&thid=".$this->thid);
		} else {
			$this->setBackUrl($this->commonlib->baseUrl()."course?cid=".$this->cid);
		}

		// 講座名取得
		$this->load->model('Course_Model', 'course', TRUE);
		$course_data = $this->course->getCourse($this->cid);

		// 日記リスト取得
		$this->load->model('Diary_Model', 'diary', TRUE);
		$_diary_disp_count = $this->config->item('DIARY_DISP_COUNT');
		$diary_data = $this->diary->getDiaryList($this->page, $_diary_disp_count, $this->cid);
		if ($this->page > 1 && is_null($diary_data)) {
			$this->page--;
			$diary_data = $this->diary->getDiaryList($this->page, $_diary_disp_count, $this->cid);
		}

		// ユーザー一覧表示開始数（件目）
		$_start_cnt = ($this->page - 1) * $_diary_disp_count + 1;
		if (count($diary_data) == 0) {
			$_start_cnt = 0;
		}

		$_base_page_url = $this->commonlib->baseUrl().'diary?cid='.$this->cid."&pre=".$this->pre;
		if ($this->pre == "theme") {
			$_base_page_url .= '&thid='.$this->thid;
		}

		// 前ページURL
		$_before_page = null;
		if ($_start_cnt > 1) {
			$_before_page = $_base_page_url.'&page='.($this->page - 1);

		}

		// 日記投稿総件数取得
		$_diary_count = $this->diary->getTotalCount($this->cid);

		// 全ページ数算出
		$_total_pages = ceil($_diary_count / $_diary_disp_count);

		// 次ページURL
		$_next_page = null;
		if ($this->page < $_total_pages) {
			$_next_page = $_base_page_url.'&page='.($this->page + 1);
		}

		$this->view_data['auth'] = $_course_auth;
		$this->view_data['course_data'] = $course_data;
		$this->view_data['diary_data'] = $this->formatDiaryData($diary_data);
		$this->view_data['before_page'] = $_before_page;
		$this->view_data['next_page'] = $_next_page;
		$this->view_data['pre'] = $this->pre;
		$this->view_data['thid'] = $this->thid;
		$this->view_data['page'] = $this->page;
		$this->view_data['uid'] = $this->authority_info[0]->id;
		$this->view_data['script'] = $this->createJavaScript($_before_page, $_next_page);

		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('diary', $this->view_data); // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * 日記データ加工
	 *
	 * @param array $diary_data 日記データ
	 * @return array 日記データ:
	 */
	private function formatDiaryData($diary_data) {

		$userid = $this->authority_info[0]->userid;

		$retdatas = array();

		foreach ($diary_data as $d) {

			if ($d['picture_file'] == null) {
				$d['picture_file'] = $this->commonlib->baseUrl().$this->config->item('DIARY_DEFAULT_IMAGE_URL');
			} else {
				$d['picture_file'] = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=".$d['uid'];
			}

			if ($d['image_file'] != null and $d['image_file'] != "") {
				$d['image_file'] = $this->config->item('DIARY_IMAGE_SAVE_URL')."/".$d['image_file'];
				$d['image_file'] = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_DIARY')."&id=".$d['id'];
			}

			if ($d['userid'] == $userid) {
				$d += array('my_post' => 1);
			} else {
				$d += array('my_post' => 0);
			}

			if ($d['hide_flg'] == 0) {
				array_push($retdatas, $d);
			}
		}

		return $retdatas;
	}

	/**
	 * 「Like!」追加
	 */
	public function like_add()
	{
		$_diary_id = $this->input->post('did', true);
		if (!$_diary_id) {
			// 「400 Bad Request」
			header("HTTP/1.0 400 Bad Request");
			echo "request parameter : ".$_diary_id;
			exit;
		}
		$this->load->model("Diary_Like_Model", 'diary_like_model', true);
		if (!$this->diary_like_model->add($_diary_id, $this->authority_info[0]->id)) {
			header("HTTP/1.0 500 Internal Server Error");
			echo "Fail add 'Like!'";
			exit;
		}
		$_count = $this->diary_like_model->getCount($_diary_id);
		if (is_null($_count)) {
			header("HTTP/1.0 500 Internal Server Error");
			echo "Fail get 'Like!' count";
			exit;
		}
		$_json_data = array('count'=>$_count);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($_json_data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
		exit;
	}

	/**
	 * 「Like!」削除
	 */
	public function like_del()
	{
		$_diary_id = $this->input->post('did', true);
		if (!$_diary_id) {
			// 「400 Bad Request」
			header("HTTP/1.0 400 Bad Request");
			echo "request parameter : ".$_diary_id;
			exit;
		}
		$this->load->model("Diary_Like_Model", 'diary_like_model', true);
		if (!$this->diary_like_model->delete($_diary_id, $this->authority_info[0]->id)) {
			header("HTTP/1.0 500 Internal Server Error");
			echo "Fail delete 'Like!'";
			exit;
		}
		$_count = $this->diary_like_model->getCount($_diary_id);
		if (is_null($_count)) {
			header("HTTP/1.0 500 Internal Server Error");
			echo "Fail get 'Like!' count";
			exit;
		}
		$_json_data = array('count'=>$_count);
		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($_json_data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
		exit;
	}

	/**
	 * 投稿削除
	 */
	public function delete()
	{
		// 日記投稿削除
		$this->load->model('Diary_Model', 'diary', TRUE);
		$this->diary->deletePost($this->did);

		// 該当日記の「Like!」削除
		$this->load->model("Diary_Like_Model", 'diary_like_model', true);
		$this->diary_like_model->deleteOfDiary($this->did);

		$path = $this->commonlib->baseUrl().'diary?cid='.$this->cid."&pre=".$this->pre."&page=".$this->page;
		if ($this->pre == "theme") {
			$path .= '&thid='.$this->thid;
		}
//		redirect($path, 'refresh');
		redirect($path, 'location');
	}

	/**
	 * 日記投稿・編集
	 */
	public function edit()
	{
		$postmsg = $this->input->post('postmsg', true);

		$this->form_validation->set_rules('postmsg', $this->lang->line('diary_button_title_send'), 'trim|required|max_length['.$this->diarymsg_max.']');

		if ($this->form_validation->run()) {

			// 画像がアップロードされたか判定
			$_save_file = null;
			if (is_uploaded_file($_FILES["upload_imagefile"]["tmp_name"])) {
				// 画像アップロード処理
				$_save_file = $this->image_upload();
				if (!is_null($this->error_message)) {
					$this->view_data['error_msg'] = $this->error_message;
					$this->displayView();
					return;
				}
			}

			// 日記モデル
			$this->load->model('Diary_Model', 'diary', TRUE);

			// 新規投稿
			$_is_new = false;
			if ($this->did == null or $this->did == "") {
				$this->diary->registPost($this->cid, $postmsg, $_save_file);
				$_is_new = true;
			} else {
				// 画像ファイルがnullの場合
				if (is_null($_save_file)) {
					// 更新前の情報取得
					$_db_post = $this->diary->getPostInfo($this->did);
					if (!empty($_db_post[0]->image_file)) {
						// 更新前に画像が既に登録されている場合はそのまま登録し直す
						$_save_file = $_db_post[0]->image_file;
					}
				}
				$this->diary->editPost($this->did, $postmsg, $_save_file);
			}

			if (!is_null($this->error_message)) {
				$this->view_data['error_msg'] = $this->error_message;
				$this->displayView();
				return;
			} else {
				$path = $this->commonlib->baseUrl().'diary?cid='.$this->cid."&pre=".$this->pre;
				if (!$_is_new) {
					// 更新の場合は元のページに戻す
					$path .= "&page=".$this->page;
				}
				if ($this->pre == "theme") {
					$path .= '&thid='.$this->thid;
				}

//				redirect($path, 'refresh');
				redirect($path, 'location');
			}
		} else {
			$this->displayView();
		}
	}

	/**
	 * 画像ファイルアップロード
	 *
	 * @return string|NULL アップロード画像ファイルパス
	 */
	private function image_upload() {
		// 画像アップロードパス取得
		$_upload_path = $this->config->item('DIARY_IMAGE_UPLOAD_PATH');
		if (!file_exists($_upload_path)) {
			mkdir($_upload_path, 0777, true);
		}
		// 画像ライブラリロード
		$_upload_config = array(
				'max_size' => $this->config->item('DIARY_IMAGE_UPLOAD_MAX_SIZE'),
				'encrypt_name' => true,
				'upload_path' => $_upload_path,
				'allowed_types' => implode("|", $this->config->item('DIARY_IMAGE_UPLOAD_EXTENSIONS'))
		);
		$this->load->library('upload', $_upload_config);

		// 画像アップロード
		if ($this->upload->do_upload('upload_imagefile')) {
			$file_info = $this->upload->data();
			// 保存先パス＋＋講座ID
			$_save_path = $this->config->item('DIARY_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid;
			if (!file_exists($_save_path)) {
				mkdir($_save_path, 0777, true);
			}
			// 保存先パス＋[ユーザーテーブルID]_yyyyMMddHHmmssSSS＋拡張子
			$_save_file_path = $_save_path.DIRECTORY_SEPARATOR.$this->authority_info[0]->id.'_'.$this->commonlib->getTimeWithMillSecond().$file_info['file_ext'];

			// リサイズ後の画像サイズ算出
			// 画像処理ライブラリロード
			$this->load->library('imagelib');
			$_resize_size = $this->imagelib->calResizeSize($file_info['full_path'], $this->config->item('DIARY_IMAGE_MAX_LENGTH'));

			// 画像リサイズ処理
			if (!is_null($_resize_size)) {
				$_image_config = array(
						'source_image' => $file_info['full_path'],
						'new_image' => $_save_file_path,
						'width' => $_resize_size[0],
						'height' => $_resize_size[1]
				);
				// CodeIgniterの画像処理ライブラリロード
				$this->load->library('image_lib', $_image_config);
				// リサイズ実行
				$this->image_lib->resize();
				// リサイズ元のアップロード画像削除
				unlink($file_info['full_path']);
			} else {
				// リサイズしなかった場合はファイルを保存先に移動
				rename($file_info['full_path'], $_save_file_path);
			}

			return $this->cid.'/'.basename($_save_file_path);
		} else {
			// 画像アップロードエラー
			// エラーメッセージ生成
			$this->error_message = $this->upload->display_errors('<div class="alert alert-danger" role="alert">', '</div>');
		}
		return null;
	}

	/**
	 * JavaScriptデータ生成
	 *
	 * @param string $before_page 前ページURL
	 * @param string $next_page 次ページURL
	 */
	private function createJavaScript($before_page, $next_page)
	{
		$_befor_page_url = '';
		if (!is_null($before_page)) {
			$_befor_page_url = $before_page;
		}
		$_next_page_url = '';
		if (!is_null($next_page)) {
			$_next_page_url = $next_page;
		}
		$_script = <<<EOT
<script>
	var beforePageUrl = "{$_befor_page_url}";
	var nextPageUrl = "{$_next_page_url}";
	var postmsgMax = {$this->diarymsg_max};
	var errMsgPostMsgRequired = "{$this->lang->line('diary_script_err_msg_postmsg_required')}";
	var errMsgPostMsgMaxlength = "{$this->lang->line('diary_script_err_msg_postmsg_maxlength')}";
	var diaryAddUrl = "{$this->commonlib->baseUrl()}diary/like_add?cid=";
	var diaryDelUrl = "{$this->commonlib->baseUrl()}diary/like_del?cid=";
	var diaryLikeTooltipAdd = "{$this->lang->line('diary_link_like_add')}";
	var diaryLikeTooltipDel = "{$this->lang->line('diary_link_like_del')}";
</script>
EOT;
		return $_script;
	}

}
