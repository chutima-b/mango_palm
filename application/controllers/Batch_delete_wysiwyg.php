<?php
/*
 * WYSIWYGエディターでアップロードした画像ファイルの削除するバッチクラス
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Batch_delete_wysiwyg extends CI_Controller {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('log'); // バッチ用ログ出力を行うため、ヘルパークラスをロード

		if (!$this->input->is_cli_request()) {
			// コマンドラインからのリクエスト以外は受け付けない
			$this->output->set_status_header(403, 'Forbidden');
			echo "403 Forbidden".PHP_EOL;
			exit;
		}

		// 実行時間を制限しない
		set_time_limit(0);
	}

	/**
	 * WYSIWYG不要画像削除
	 */
	public function execute()
	{
		batch_info('start execute', true);
		// 講座説明の不要画像削除
		$this->proc_course();
		// お知らせの不要画像削除
		$this->proc_notice();
		// テーマ説明の不要画像削除
		$this->proc_theme();
		// 課題「問題」の不要画像削除
		$this->proc_task();
		// システム管理（設定）のイントロダクションの不要画像削除
		$this->proc_settings();
		batch_info('end execute', true);
	}

	/**
	 * 講座説明の不要画像削除処理
	 */
	private function proc_course()
	{
		batch_info('start proc_course', true);

		// 講座情報テーブルから、“ID”、“講座説明”の一覧を取得する。
		$this->load->model("Course_Model", 'course_model', true);
		$_course_list = $this->course_model->getCourseListForBatchDeleteWYSIWYG();
		if (is_null($_course_list) || count($_course_list) == 0) {
			// 講座が存在しない場合は何もしない
			batch_info('end proc_course', true);
			return;
		}

		// アップロード対象拡張子取得
		$_target_ext_list = $this->config->item('COURSEEDIT_DESCRIPT_IMAGE_UPLOAD_EXTENSIONS');

		// 取得した講座一覧から、各講座ごとに処理
		foreach ($_course_list as $_course) {
			// 該当講座の保存先パス取得
			$_save_path = $this->config->item('COURSEEDIT_DESCRIPT_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_course->id;
			if (!file_exists($_save_path)) {
				// 保存先ディレクトリが無い場合は何もしない
				continue;
			}

			// 該当の講座情報テーブルIDのディレクトリから、画像ファイルの一覧を取得する。
			$_file_list = scandir($_save_path);

			// 取得したファイル一覧から、各ファイルごとに処理
			foreach ($_file_list as $_file) {
				if (!$this->is_target_file($_file, $_target_ext_list)) {
					// 処理対象外のファイルは何もしない
					continue;
				}
				// 該当講座IDの“講座説明”の文字列を解析し、取得した画像ファイル名が文字列に含まれるか確認する。
				if (strpos($_course->course_description, $_file) === false) {
					// 文字列に含まれていない画像ファイルを削除する。
					@unlink($_save_path.DIRECTORY_SEPARATOR.$_file);
				}
			}
		}

		batch_info('end proc_course', true);
	}

	/**
	 * お知らせの不要画像削除
	 */
	private function proc_notice()
	{
		batch_info('start proc_notice', true);

		// お知らせ情報テーブルから、“内容”の一覧を取得する。
		$this->load->model("Notice_Model", 'notice_model', true);
		$_notice_list = $this->notice_model->getNoticeListForBatchDeleteWYSIWYG();
		if (is_null($_notice_list) || count($_notice_list) == 0) {
			// お知らせが無い場合は何もしない
			batch_info('end proc_notice', true);
			return;
		}

		// “お知らせ内容画像保存先パス”取得
		$_save_path = $this->config->item('STAFFHOME_CONTENT_IMAGE_SAVE_PATH');
		if (!file_exists($_save_path)) {
			// 保存先ディレクトリが無い場合は何もしない
			batch_info('end proc_notice', true);
			return;
		}

		// “お知らせ内容画像保存先パス”内から、画像ファイルの一覧を取得する。
		$_file_list = scandir($_save_path);

		// アップロード対象拡張子取得
		$_target_ext_list = $this->config->item('STAFFHOME_CONTENT_IMAGE_UPLOAD_EXTENSIONS');

		// お知らせ情報テーブルから取得した“内容”の文字列を順次解析
		foreach ($_file_list as $_file) {
			if (!$this->is_target_file($_file, $_target_ext_list)) {
				// 処理対象外のファイルは何もしない
				continue;
			}
			// 取得した画像ファイル一覧のファイル名が“内容”に含まれるか確認
			$_is_use_file = false; // 使用中フラグ
			foreach ($_notice_list as $_notice) {
				if (strpos($_notice->content, $_file) !== false) {
					// “内容”に含まれる場合は、使用中フラグを立てる
					$_is_use_file = true;
					break;
				}
			}
			if (!$_is_use_file) {
				// どのお知らせの“内容”にも使用されていない画像は削除する
				@unlink($_save_path.DIRECTORY_SEPARATOR.$_file);
			}
		}

		batch_info('end proc_notice', true);
	}

	/**
	 * テーマ説明の不要画像削除
	 */
	private function proc_theme()
	{
		batch_info('start proc_theme', true);

		// テーマテーブルから“講座ID”および“テーマ説明”の一覧を取得する。
		$this->load->model("Course_Model", 'course_model', true);
		$_course_list = $this->course_model->getCourseIdListForBatchDeleteWYSIWYGOfThemeAndTask();
		if (is_null($_course_list) || count($_course_list) == 0) {
			// 講座が存在しない場合は何もしない
			batch_info('end proc_theme', true);
			return;
		}

		$this->load->model("Theme_Model", 'theme_model', true);

		// アップロード対象拡張子取得
		$_target_ext_list = $this->config->item('THEMEEDIT_DESCRIPTION_IMAGE_UPLOAD_EXTENSIONS');

		// 取得した一覧を“講座ID”ごとに以降の処理を実行する。
		foreach ($_course_list as $_course) {
			// 該当の“講座ID”のテーマ情報取得
			$_theme_list = $this->theme_model->getThemeListForBatchDeleteWYSIWYG($_course->id);
			if (is_null($_theme_list) || count($_theme_list) == 0) {
				// テーマが無い場合は何もしない
				continue;
			}

			// 該当講座の保存先パス取得
			$_save_path = $this->config->item('THEMEEDIT_DESCRIPTION_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_course->id;
			if (!file_exists($_save_path)) {
				// 保存先ディレクトリが無い場合は何もしない
				continue;
			}

			// “テーマ説明画像保存先パス”内の、該当の“講座ID”のディレクトリから、画像ファイルの一覧を取得する。
			$_file_list = scandir($_save_path);

			// 取得したファイル一覧から、各ファイルごとに処理
			foreach ($_file_list as $_file) {
				if (!$this->is_target_file($_file, $_target_ext_list)) {
					// 処理対象外のファイルは何もしない
					continue;
				}

				// 該当の“講座ID”の“テーマ説明”の文字列を順次解析し、取得した画像ファイル名が文字列に含まれるか確認する。
				$_is_use_file = false; // 使用中フラグ
				foreach ($_theme_list as $_theme) {
					if (strpos($_theme->description, $_file) !== false) {
						// “内容”に含まれる場合は、使用中フラグを立てる
						$_is_use_file = true;
						break;
					}
				}
				if (!$_is_use_file) {
					// いずれの“テーマ説明”にも使用されていない画像は削除する
					@unlink($_save_path.DIRECTORY_SEPARATOR.$_file);
				}
			}
		}

		batch_info('end proc_theme', true);
	}

	/**
	 * 課題「問題」の不要画像削除
	 */
	private function proc_task()
	{
		batch_info('start proc_task', true);

		// テーマテーブルから“講座ID”および“テーマ説明”の一覧を取得する。
		$this->load->model("Course_Model", 'course_model', true);
		$_course_list = $this->course_model->getCourseIdListForBatchDeleteWYSIWYGOfThemeAndTask();
		if (is_null($_course_list) || count($_course_list) == 0) {
			// 講座が存在しない場合は何もしない
			batch_info('end proc_task', true);
			return;
		}

		$this->load->model("Theme_Model", 'theme_model', true);
		$this->load->model("Task_Model", 'task_model', true);

		// アップロード対象拡張子取得
		$_target_ext_list = $this->config->item('TASKEDIT_DESCRIPTION_IMAGE_UPLOAD_EXTENSIONS');

		// 取得した一覧を“講座ID”ごとに以降の処理を実行する。
		foreach ($_course_list as $_course) {
			// 該当の“講座ID”のテーマ情報取得
			$_theme_list = $this->theme_model->getThemeListForBatchDeleteWYSIWYGOfTask($_course->id);
			if (is_null($_theme_list) || count($_theme_list) == 0) {
				// テーマが無い場合は何もしない
				continue;
			}

			foreach ($_theme_list as $_theme) {
				// 該当の“講座ID”、“テーマID”の課題情報取得
				$_task_list = $this->task_model->getTaskListForBatchDeleteWYSIWYG($_course->id, $_theme->id);
				if (is_null($_task_list) || count($_task_list) == 0) {
					// 課題が無い場合は何もしない
					continue;
				}

				// 該当講座の保存先パス取得
				$_save_path = $this->config->item('TASKEDIT_DESCRIPTION_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_course->id.DIRECTORY_SEPARATOR.$_theme->id;
				if (!file_exists($_save_path)) {
					// 保存先ディレクトリが無い場合は何もしない
					continue;
				}

				// “テーマ説明画像保存先パス”内の、該当の“講座ID”、“テーマID”のディレクトリから、画像ファイルの一覧を取得する。
				$_file_list = scandir($_save_path);

				// 取得したファイル一覧から、各ファイルごとに処理
				foreach ($_file_list as $_file) {
					if (!$this->is_target_file($_file, $_target_ext_list)) {
						// 処理対象外のファイルは何もしない
						continue;
					}

					// 該当の“講座ID”、“テーマID”の“問題”の文字列を順次解析し、取得した画像ファイル名が文字列に含まれるか確認する。
					$_is_use_file = false; // 使用中フラグ
					foreach ($_task_list as $_task) {
						if (strpos($_task->task_description, $_file) !== false) {
							// “問題”に含まれる場合は、使用中フラグを立てる
							$_is_use_file = true;
							break;
						}
					}
					if (!$_is_use_file) {
						// いずれの“問題”にも使用されていない画像は削除する
						@unlink($_save_path.DIRECTORY_SEPARATOR.$_file);
					}
				}
			}
		}

		batch_info('end proc_task', true);
	}

	/**
	 * システム管理（設定）のイントロダクションの不要画像削除
	 */
	private function  proc_settings()
	{
		batch_info('start proc_settings', true);

		// 設定テーブルから、“イントロダクション”を取得する。
		$this->load->model("Setting_Model", 'setting_model', true);
		$_introduction = $this->setting_model->get_introduction();

		// イントロダクション画像保存先パス取得
		$_save_path = $this->config->item('SETTINGS_INTRODUCTION_IMAGE_SAVE_PATH');
		if (!file_exists($_save_path)) {
			// 保存先ディレクトリが無い場合は何もしない
			batch_info('end proc_settings', true);
			return;
		}

		// “イントロダクション画像保存先パス”内から、画像ファイルの一覧を取得する。
		$_file_list = scandir($_save_path);

		// アップロード対象拡張子取得
		$_target_ext_list = $this->config->item('SETTINGS_INTRODUCTION_IMAGE_UPLOAD_EXTENSIONS');

		// 取得したファイル一覧から、各ファイルごとに処理
		foreach ($_file_list as $_file) {
			if (!$this->is_target_file($_file, $_target_ext_list)) {
				// 処理対象外のファイルは何もしない
				continue;
			}
			// “イントロダクション”の文字列を解析し、取得した画像ファイル名が文字列に含まれるか確認する。
			if (strpos($_introduction, $_file) === false) {
				// 文字列に含まれていない画像ファイルを削除する。
				@unlink($_save_path.DIRECTORY_SEPARATOR.$_file);
			}
		}

		batch_info('end proc_settings', true);
	}

	/**
	 * 処理対象ファイルか判定
	 *
	 * @param string $file 対象判定ファイル
	 * @param array $target_extensions 処理対象拡張子
	 * @return boolean 判定結果（true:処理対象 / false:処理対象外）
	 */
	private function is_target_file($file, $target_extensions)
	{
		if($file == '.' || $file == '..') {
			return false;
		}
		if (!strrpos($file, '.')) {
			// 拡張子が無い場合は処理対象ではない
			return false;
		}
		$_ext = substr($file, strrpos($file, '.') + 1); // 拡張子取得
		if (!in_array($_ext, $target_extensions)) {
			// アップロード対象の拡張子以外は処理対象ではない
			return false;
		}
		return true;
	}

}
