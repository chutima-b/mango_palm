<?php
/*
 * 課題編集画面コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Taskedit extends EX_Controller {
	/** 講座テーブルID. */
	private $cid = 0;
	/** テーマテーブルID. */
	private $thid = 0;
	/** 課題テーブルID. */
	private $taid = 0;
	/** 新規作成フラグ. */
	private $is_new = false;
	/** 課題名最大文字数. */
	private  $task_name_maxlength = 0;
	/** 課題説明最大文字数. */
	private  $task_description_maxlength = 0;
	/** 解説最大文字数. */
	private  $task_comment_maxlength = 0;
	/** 戻り画面URL. */
	private $back_url = null;
	/** エラーメッセージ. */
	private $error_message = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_TASK_EDIT')); // 課題編集画面
		$this->lang->load('task_edit_lang');
		$this->setPageTitle($this->lang->line('task_edit_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link href="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.css?v=1.0.0" rel="stylesheet">
<link href="{$_base_url}libs/summernote/summernote.css" rel="stylesheet">
<link rel="stylesheet" href="{$_base_url}css/taskedit.css?v=1.0.0">
EOT;
		$this->addCssFiles($_add_css);
		$_summernote_lang_file = $this->config->item('SUMMERNOTE_LANG_FILE');
		$_summernote_lang_script = "";
		if (!empty($_summernote_lang_file)) {
			$_summernote_lang_script = "<script src=\"{$_base_url}libs/summernote/lang/{$this->config->item('SUMMERNOTE_LANG_FILE')}\"></script>";
		}
		$_add_script =<<<EOT
<script src="{$_base_url}libs/jqueryvalidation/jquery.validate.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.js?v=1.0.0"></script>
<script src="{$_base_url}libs/summernote/summernote.custom.js"></script>
{$_summernote_lang_script}
<script src="{$_base_url}js/taskedit.js?v=1.0.5"></script>
EOT;
		$this->addScriptFiles($_add_script);

		// パラメータ取得
		$this->cid = $this->input->get('cid', true);
		$this->thid = $this->input->get('thid', true);
		$this->taid = $this->input->get('taid', true);
		if (is_null($this->taid) || empty($this->taid)) {
			$this->is_new = true;
		}
		$this->load->model("Task_Model", 'task_model', true);

		// 最大文字数設定
		$this->task_name_maxlength = $this->config->item('TASKEDIT_NAME_MAXLENGTH');
		$this->task_description_maxlength = $this->config->item('TASKEDIT_DESCRIPTION_MAXLENGTH');
		$this->task_comment_maxlength = $this->config->item('TASKEDIT_COMMENT_MAXLENGTH');

		// 戻り画面URL設定
		$this->back_url = $this->commonlib->baseUrl()."themeedit?cid=".$this->cid."&thid=".$this->thid;
		$this->setBackUrl($this->back_url);
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * 登録
	 */
	public function regist()
	{
		$this->form_validation->set_rules('taskedit_name', $this->lang->line('task_edit_task_name'), 'trim|required|max_length['.$this->task_name_maxlength.']');
		$this->form_validation->set_rules('taskedit_description', $this->lang->line('task_edit_description'), 'trim|required|max_length['.$this->task_description_maxlength.']');
		$this->form_validation->set_rules('taskedit_type', $this->lang->line('task_edit_task_type'), 'trim|required');
		if ($this->input->post("taskedit_type", true) == $this->config->item('TASK_TYPE_SINGLE_SELECTION')
		 || $this->input->post("taskedit_type", true) == $this->config->item('TASK_TYPE_MULTIPLE_CHOICE')
		 || $this->input->post("taskedit_type", true) == $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')
		 || $this->input->post("taskedit_type", true) == $this->config->item('TASK_TYPE_SURVEY_SINGLE_SELECTION')
		 || $this->input->post("taskedit_type", true) == $this->config->item('TASK_TYPE_SURVEY_MULTIPLE_CHOICE')
		) {
			// 「課題のタイプ」が単一選択または、複数選択または、クイズ（単一選択）
			// または、アンケート（単一選択）、アンケート（複数選択）の場合は、選択肢入力のテキストエリアをバリデーションする
			$this->form_validation->set_rules('taskedit_choices', $this->lang->line('task_edit_choices_textarea'), 'trim|required');
		}

		// 「評価を有効にする」が“YES”の場合
		if ($this->input->post("taskedit_evaluation_use", true) === '1') {
			// 解説の文字数をバリデーションする
			$this->form_validation->set_rules('taskedit_comment', $this->lang->line('task_edit_comment'), 'trim|max_length['.$this->task_comment_maxlength.']');
			if ($this->input->post("taskedit_quiz", true) === '1') {
				if ($this->input->post("taskedit_type", true) == $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')) {
					// クイズの場合で、「課題のタイプ」がクイズ（単一選択）の場合は「正解」をバリデーションする
					$this->form_validation->set_rules('taskedit_quiz_answer', $this->lang->line('task_edit_quiz_answer'), 'trim|required');
				}
			} else {
				// クイズ以外の場合は「評価点」をバリデーションする
				$this->form_validation->set_rules('taskedit_evaluation_points', $this->lang->line('task_edit_evaluation_points'), 'trim|required|callback_check_evaluation_points');
			}
		}

		if ($this->form_validation->run()) {
			if (is_null($this->taid) || empty($this->taid)) {
				// 新規登録
				if (!$this->task_model->regist()) {
					$this->error_message = $this->lang->line('task_edit_err_msg_regist');
				}
			} else {
				// 更新登録
				if (!$this->task_model->update()) {
					$this->error_message = $this->lang->line('task_edit_err_msg_update');
				}
			}
			if (is_null($this->error_message)) {
				// 正常登録出来たら呼出元画面に戻る
				redirect($this->back_url);
				exit;
			}
		}
		$this->displayView();
	}

	/**
	 * 評価点チェック
	 *
	 * @return boolean true：エラーなし / false：エラーあり
	 */
	public function check_evaluation_points()
	{
		if ($this->input->post('taskedit_evaluation_points', true) > 100) {
			$this->form_validation->set_message("check_evaluation_points", $this->lang->line('task_edit_err_msg_evaluation_points_over_100'));
			return false;
		}
		return true;
	}

	/**
	 * 問題画像アップロード
	 */
	public function descriptimg()
	{
		$this->cid = $this->input->get_post('cid', true);
		if (isset($_FILES["descriptimg"])
		&& is_uploaded_file($_FILES["descriptimg"]["tmp_name"])) {
				// 画像アップロードパス取得
			$_upload_path = $this->config->item('TASKEDIT_DESCRIPTION_IMAGE_UPLOAD_PATH');
			if (!file_exists($_upload_path)) {
				mkdir($_upload_path, 0777, true);
			}
			// 画像ライブラリロード
			$_upload_config = array(
					'max_size' => $this->config->item('TASKEDIT_DESCRIPTION_UPLOAD_MAX_SIZE'),
					'encrypt_name' => true,
					'upload_path' => $_upload_path,
					'allowed_types' => implode("|", $this->config->item('TASKEDIT_DESCRIPTION_IMAGE_UPLOAD_EXTENSIONS'))
			);
			$this->load->library('upload', $_upload_config);

			// 画像アップロード
			if ($this->upload->do_upload('descriptimg')) {
				$file_info = $this->upload->data();
				// 保存先ディレクトリ生成
				// 保存先パス＋講座情報テーブルID＋テーマID（新規の時は課題IDが無いので講座情報テーブルID、テーマIDのみを利用する）
				$_save_path = $this->config->item('TASKEDIT_DESCRIPTION_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$this->cid.DIRECTORY_SEPARATOR.$this->thid;
				if (!file_exists($_save_path)) {
					mkdir($_save_path, 0777, true);
				}
				// 保存先パス＋_yyyyMMddHHmmssSSS＋拡張子
				$_filename = $this->commonlib->getTimeWithMillSecond().$file_info['file_ext'];
				$_save_file_path = $_save_path.DIRECTORY_SEPARATOR.$_filename;

				// リサイズ後の画像サイズ算出
				// 画像処理ライブラリロード
				$this->load->library('imagelib');
				$_resize_size = $this->imagelib->calResizeSize($file_info['full_path'], $this->config->item('TASKEDIT_DESCRIPTION_IMAGE_MAX_LENGTH'));

				// 画像リサイズ処理
				if (!is_null($_resize_size)) {
					$_image_config = array(
						'source_image' => $file_info['full_path'],
						'new_image' => $_save_file_path,
						'width' => $_resize_size[0],
						'height' => $_resize_size[1]
					);
					// CodeIgniterの画像処理ライブラリロード
					$this->load->library('image_lib', $_image_config);
					// リサイズ実行
					$this->image_lib->resize();
					// リサイズ元のアップロード画像削除
					unlink($file_info['full_path']);
				} else {
					// リサイズしなかった場合はファイルを保存先に移動
					rename($file_info['full_path'], $_save_file_path);
				}
				echo $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_TASK')."&cid=".$this->cid."&thid=".$this->thid."&fl=".$_filename;
				exit;
			} else {
				// 画像アップロードエラー
				// エラーメッセージ生成
				header("HTTP/1.0 404 Not Found");
				echo $this->upload->display_errors('<div class="alert alert-danger" role="alert">', '</div>');
				exit;
			}
		}
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		// テーマ名称取得
		$this->load->model("Theme_Model", 'theme_model', true);
		$_theme_data = $this->theme_model->getThemeInfo($this->thid);
		$_theme_name = $_theme_data[0]['name'];
		$_evaluation_use = $_theme_data[0]['evaluation_use'];
		$_quiz = $_theme_data[0]['quiz'];
		// 課題情報取得
		$_task_data = null;
		if (!$this->is_new) {
			// 課題データ取得
			$_task_data = $this->task_model->getTask($this->taid);
			if (!is_null($_task_data)) {
				$_task_data = $_task_data[0];
			}
		}
		if (is_null($_task_data)) {
			$_task_data = new stdClass();
			$_task_data->id = '';
			$_task_data->course_id = '';
			$_task_data->theme_id = '';
			$_task_data->task_name = '';
			$_task_data->task_description = '';
			$_task_data->time_limit = '';
			$_task_data->task_type = '';
			$_task_data->choices = '';
			$_task_data->evaluation_points = '';
			$_task_data->quiz_answer = '';
			$_task_data->comment = '';
		}
		// ビューのデータに設定
		$this->view_data['theme_name'] = $_theme_name;
		$this->view_data['evaluation_use'] = $_evaluation_use;
		$this->view_data['quiz'] = $_quiz;
		$this->view_data['task'] = $_task_data;
		$this->view_data['task_types'] = array(
				$this->config->item('TASK_TYPE_SINGLE_SELECTION') => $this->lang->line('task_edit_select_radio'),
				$this->config->item('TASK_TYPE_MULTIPLE_CHOICE') => $this->lang->line('task_edit_select_checkbox'),
				$this->config->item('TASK_TYPE_TEXT') => $this->lang->line('task_edit_select_text'),
				$this->config->item('TASK_TYPE_FILE') => $this->lang->line('task_edit_select_file')
			);
		if ($_quiz) {
			$this->view_data['task_types'] = array(
					$this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION') => $this->lang->line('task_edit_select_quiz_radio'),
					$this->config->item('TASK_TYPE_SURVEY_SINGLE_SELECTION') => $this->lang->line('task_edit_select_survey_radio'),
					$this->config->item('TASK_TYPE_SURVEY_MULTIPLE_CHOICE') => $this->lang->line('task_edit_select_survey_checkbox'),
					$this->config->item('TASK_TYPE_TEXT') => $this->lang->line('task_edit_select_survey_text')
			);
		}
		$this->view_data['form_regist_action_url'] = $this->commonlib->baseUrl().'taskedit/regist?cid='.$this->cid.'&thid='.$this->thid.'&taid='.$this->taid;
		$this->view_data['script'] = $this->createJavaScript($_task_data->quiz_answer);
		$this->view_data['error_msg'] = $this->error_message;
		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('task_edit', $this->view_data);     // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript($quiz_answer)
	{
		$_script = <<<EOT
<script>
	var summernoteLang = "{$this->config->item('SUMMERNOTE_LANG')}";
	var quizAnswer = '{$quiz_answer}';
	var taskTypeSingleSelection = {$this->config->item('TASK_TYPE_SINGLE_SELECTION')};
	var taskTypeMultipleChoice = {$this->config->item('TASK_TYPE_MULTIPLE_CHOICE')};
	var taskTypeText = {$this->config->item('TASK_TYPE_TEXT')};
	var taskTypeFile = {$this->config->item('TASK_TYPE_FILE')};
	var taskTypeQuizSingleSelection = {$this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')};
	var taskTypeSurveySingleSelection = {$this->config->item('TASK_TYPE_SURVEY_SINGLE_SELECTION')};
	var taskTypeSurveyMultipleChoice = {$this->config->item('TASK_TYPE_SURVEY_MULTIPLE_CHOICE')};
	var taskNameMax = {$this->task_name_maxlength};
	var taskDescriptionMax = {$this->task_description_maxlength};
	var taskCommentMax = {$this->task_comment_maxlength};
	var errMsgTasknameRequired = "{$this->lang->line('task_edit_err_msg_taskname_required')}";
	var errMsgTasknameMaxlength = "{$this->lang->line('task_edit_err_msg_taskname_maxlength')}";
	var errMsgDescriptionRequired = "{$this->lang->line('task_edit_err_msg_description_required')}";
	var errMsgDescriptionMaxlength = "{$this->lang->line('task_edit_err_msg_description_maxlength')}";
	var errMsgTaskTypeRequired = "{$this->lang->line('task_edit_err_msg_task_type_required')}";
	var errMsgEvaluationPoints = "{$this->lang->line('task_edit_err_msg_evaluation_points')}";
	var errMsgChoicesTextareaRequired = "{$this->lang->line('task_edit_err_msg_choices_textarea_required')}";
	var errMsgCommentMaxlength = "{$this->lang->line('task_edit_err_msg_comment_maxlength')}";
	var errMsgQuizAnswerRequired = "{$this->lang->line('task_edit_err_msg_quiz_answer_required')}";
</script>
EOT;
		return $_script;
	}

}
