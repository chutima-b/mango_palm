<?php
/*
 * アクセスエラー画面コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Accesserror extends EX_Controller {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_ACCESS_ERROR')); // アクセスエラー画面
		$this->lang->load('access_error_lang');
		$this->setPageTitle($this->lang->line('access_error_header_pagetitle'));

		// 戻る画面設定
		if (isset($_SERVER['HTTP_REFERER'])) {
			$this->setBackUrl($_SERVER['HTTP_REFERER']);
		} else {
			$this->setBackUrl($this->homeUrl());
		}
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('access_error');  // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}
}
