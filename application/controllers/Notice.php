<?php
/*
 * お知らせ画面コントローラー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Notice extends EX_Controller {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_NOTICE')); // お知らせ画面
		$this->lang->load('notice_lang');
		$this->setPageTitle($this->lang->line('notice_header_pagetitle'));
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$datas = null;
		$id = $this->input->get('nid', true);
		$pre = $this->input->get('pre', true);
		if (isset($id) and $id !== null) {

			// お知らせモデル
			$this->load->model('Notice_Model', 'notice', TRUE);

			// お知らせ情報取得
			$datas = $this->notice->getNotice($id);
		}

		// 出力情報設定
		$param['notice_data'] = $datas;

		// 戻る画面設定
		if ($pre == 'students') {
			$this->setBackUrl($this->commonlib->baseUrl().'studentshome');
		} else {
			$this->setBackUrl($this->commonlib->baseUrl().'staffhome');
		}

		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('notice', $param);        // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}
}
