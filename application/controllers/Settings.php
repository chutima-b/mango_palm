<?php
/*
 * システム管理（設定）コントローラー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends EX_Controller {
	/** 戻るページ種別：システム管理（ユーザー管理）. */
	const BACK_PAGE_KIND_USERMANAGER = 0;
	/** 戻るページ種別：システム管理（ジャンル）. */
	const BACK_PAGE_KIND_GENREMANAGER = 1;

	/** タイトル最大文字数. */
	private $title_maxlength = 0;
	/** 戻るページ種別. */
	private $backkind = self::BACK_PAGE_KIND_USERMANAGER;
	/** エラーメッセージ. */
	private $error_message = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->initialize($this->config->item('CONTROLLER_KIND_SETTINGS')); // システム管理（設定）画面
		$this->lang->load('settings_lang');
		$this->setPageTitle($this->lang->line('settings_header_pagetitle'));
		$_base_url = $this->commonlib->baseUrl();
		$_add_css = <<<EOT
<link href="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.css?v=1.0.0" rel="stylesheet">
<link href="{$_base_url}libs/summernote/summernote.css" rel="stylesheet">
<link href="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker.css" rel="stylesheet">
<link href="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker-regularfont.css" rel="stylesheet">
<link href="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker-glyphicons.css" rel="stylesheet">
<link href="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker-fontawesome.css" rel="stylesheet">
<link href="{$_base_url}libs/awesome-bootstrap-checkbox-master/bower_components/Font-Awesome/css/font-awesome.css" rel="stylesheet">
<link href="{$_base_url}libs/awesome-bootstrap-checkbox-master/awesome-bootstrap-checkbox.css" rel="stylesheet">
<link href="{$_base_url}css/settings.css?v=1.0.1" rel="stylesheet">
EOT;
		$this->addCssFiles($_add_css);
		$_summernote_lang_file = $this->config->item('SUMMERNOTE_LANG_FILE');
		$_summernote_lang_script = "";
		if (!empty($_summernote_lang_file)) {
			$_summernote_lang_script = "<script src=\"{$_base_url}libs/summernote/lang/{$this->config->item('SUMMERNOTE_LANG_FILE')}\"></script>";
		}
		$_add_script =<<<EOT
<script src="{$_base_url}libs/summernote/summernote.custom.js"></script>
{$_summernote_lang_script}
<script src="{$_base_url}libs/jqueryvalidation/jquery.validate.min.js"></script>
<script src="{$_base_url}libs/jqueryvalidation/jquery.validation.custom.js?v=1.0.0"></script>
<script src="{$_base_url}libs/jquery-simplecolorpicker/jquery.simplecolorpicker.js"></script>
<script src="{$_base_url}js/settings.js?v=1.0.2"></script>
EOT;
		$this->addScriptFiles($_add_script);

		$this->load->model("Setting_Model", 'setting_model', true);

		$this->title_maxlength = $this->config->item('SETTING_TITLE_MAXLENGTH');

		// 戻るページ種別設定
		if (isset($_SERVER['HTTP_REFERER'])) {
			if (stripos($_SERVER['HTTP_REFERER'], 'genremanager') !== false) {
				$this->backkind = self::BACK_PAGE_KIND_GENREMANAGER;
			} else {
				$this->backkind = self::BACK_PAGE_KIND_USERMANAGER;
			}
		}
		if (!is_null($this->input->get_post('backkind', true))) {
			$this->backkind = $this->input->get_post('backkind', true);
		}
	}

	/**
	 * インデックス
	 */
	public function index()
	{
		$this->displayView();
	}

	/**
	 * 登録
	 */
	public function regist()
	{
		$this->form_validation->set_rules('title', $this->lang->line('settings_item_title'), 'trim|max_length['.$this->title_maxlength.']');

		if ($this->form_validation->run()) {
			// タイトル
			$_title = null;
			if (strlen($this->input->post('title', true)) > 0) {
				$_title = $this->input->post('title', true);
			}

			// イントロダクション
			$_introduction = null;
			if (strlen($this->input->post('introduction', true)) > 0) {
				// WYSIWYG対応のためCodeigniterではXSS対策は実施せずに、専用関数で除去する。
				$_introduction = $this->commonlib->removeXssForWYSIWYG($this->input->post('introduction'));
			}

			// ロゴを削除する
			$_is_logo_delete = false;
			if (!is_null($this->input->post("delete_logo", true))) {
				$_is_logo_delete = true;
				// 設定テーブルの更新前に現在のロゴファイル情報を取得して削除
				$this->deleteLogoFile();
			}

			// ロゴ
			$_logo_img = null;
			// ロゴ画像を削除するにチェックが無く、
			// ロゴ画像がアップロードされた場合
			if (!$_is_logo_delete
			 && is_uploaded_file($_FILES["logo_img"]["tmp_name"])) {
				// ロゴ画像アップロード処理
				$_logo_img = $this->logo_image_upload();
				if (!is_null($this->error_message)) {
					$this->displayView();
					return;
				}
				// ロゴが更新された場合
				if (!is_null($_logo_img)) {
					// 設定テーブルの更新前に現在のロゴファイル情報を取得して削除
					$this->deleteLogoFile();
				}
			}

			// 壁紙を使う
			$_back_img_use = 0;
			if (!is_null($this->input->post("use_background_img", true))) {
				$_back_img_use = 1;
			}

			// 壁紙画像がアップロードされた場合
			$_background_img = null;
			if (is_uploaded_file($_FILES["background_img"]["tmp_name"])) {
				// 壁紙画像アップロード処理
				$_background_img = $this->back_image_upload();
				if (!is_null($this->error_message)) {
					$this->displayView();
					return;
				}
				// 壁紙が更新された場合
				if (!is_null($_background_img)) {
					// 設定テーブルの更新前に壁紙情報を取得
					$_old_wallpaper = $this->setting_model->get_wallpaper();
					if (!is_null($_old_wallpaper)) {
						// 現在の壁紙画像ファイル削除
						unlink($this->config->item('SETTINGS_BACKGROUND_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_old_wallpaper);
					}
				}
			}

			// 設定更新
			if (!$this->setting_model->update(
					$_title,
					$_is_logo_delete,
					$_logo_img,
					$_introduction,
					$this->input->post('background_color', true),
					$_back_img_use,
					$_background_img
				)) {
				$this->error_message = $this->lang->line('settings_err_msg_regist');
			}

			// セッションに再設定
			$_settings = $this->setting_model->get_display_data();
			// タイトル
			$this->session->set_userdata('system_title', $_settings->title);
			// ロゴ
			$this->session->set_userdata('logo', $_settings->logo_img);
			// 背景
			$this->session->set_userdata('backcolor', $_settings->background_color);

			// 壁紙を使用する場合
			$_is_set_back_img = false;
			if ($_back_img_use) {
				// 壁紙を再設定しておく
				if (!is_null($_settings->background_img) && strlen($_settings->background_img) > 0) {
					$this->session->set_userdata('wallpaper', $_settings->background_img);
					$_is_set_back_img = true;
				}
			}

			if (!$_is_set_back_img) {
				// 壁紙を使わない場合、あるいは壁紙がセッションに再設定されなかった場合は、
				// セッション変数から壁紙情報を削除しておく
				$this->session->unset_userdata('wallpaper');
			}
		}

		if (is_null($this->error_message)) {
			header("Location: {$this->commonlib->baseUrl()}settings?backkind={$this->backkind}&cmp=1");
		}

		$this->displayView();
	}

	/**
	 * ロゴファイル削除
	 */
	private function deleteLogoFile()
	{
		$_logo = $this->setting_model->get_logo();
		if (!is_null($_logo)) {
			unlink($this->config->item('SETTINGS_LOGO_IMAGE_SAVE_PATH').DIRECTORY_SEPARATOR.$_logo);
		}
	}

	/**
	 * ロゴ画像アップロード
	 *
	 * return string 保存した画像ファイル名
	 */
	private function logo_image_upload() {
		// 画像アップロードパス取得
		$_upload_path = $this->config->item('SETTINGS_LOGO_IMAGE_UPLOAD_PATH');
		if (!file_exists($_upload_path)) {
			mkdir($_upload_path, 0777, true);
		}
		// 画像ライブラリロード
		$_upload_config = array(
				'max_size' => $this->config->item('SETTINGS_LOGO_IMAGE_UPLOAD_MAX_SIZE'),
				'encrypt_name' => true,
				'upload_path' => $_upload_path,
				'allowed_types' => implode("|", $this->config->item('SETTINGS_LOGO_IMAGE_UPLOAD_EXTENSIONS'))
		);
		$this->load->library('upload', $_upload_config);

		// 画像アップロード
		if ($this->upload->do_upload('logo_img')) {
			$file_info = $this->upload->data();
			// 保存先ディレクトリ生成
			$_save_path = $this->config->item('SETTINGS_LOGO_IMAGE_SAVE_PATH');
			if (!file_exists($_save_path)) {
				mkdir($_save_path, 0777, true);
			}
			// 保存先パス＋現在日時＋拡張子
			$_save_file_path = $_save_path.DIRECTORY_SEPARATOR.$this->commonlib->getTimeWithMillSecond().$file_info['file_ext'];

			// リサイズ後の画像サイズ算出
			// 画像処理ライブラリロード
			$this->load->library('imagelib');
			$_resize_size = $this->imagelib->calResizeSize($file_info['full_path'], $this->config->item('SETTINGS_LOGO_IMAGE_MAX_LENGTH'));

			// 画像リサイズ処理
			if (!is_null($_resize_size)) {
				$_image_config = array(
						'source_image' => $file_info['full_path'],
						'new_image' => $_save_file_path,
						'width' => $_resize_size[0],
						'height' => $_resize_size[1]
				);
				// CodeIgniterの画像処理ライブラリロード
				$this->load->library('image_lib', $_image_config);
				// リサイズ実行
				$this->image_lib->resize();
				// リサイズ元のアップロード画像削除
				unlink($file_info['full_path']);
			} else {
				// リサイズしなかった場合はファイルを保存先に移動
				rename($file_info['full_path'], $_save_file_path);
			}

			return basename($_save_file_path);
		} else {
			// 画像アップロードエラー
			// エラーメッセージ生成
			$this->error_message = $this->upload->display_errors();
		}
		return null;
	}

	/**
	 * イントロダクション画像アップロード
	 */
	public function introimg()
	{
		if (isset($_FILES["introimg"])
		&& is_uploaded_file($_FILES["introimg"]["tmp_name"])) {
			// 画像アップロードパス取得
			$_upload_path = $this->config->item('SETTINGS_INTRODUCTION_IMAGE_UPLOAD_PATH');
			if (!file_exists($_upload_path)) {
				mkdir($_upload_path, 0777, true);
			}
			// 画像ライブラリロード
			$_upload_config = array(
					'max_size' => $this->config->item('SETTINGS_INTRODUCTION_IMAGE_UPLOAD_MAX_SIZE'),
					'encrypt_name' => true,
					'upload_path' => $_upload_path,
					'allowed_types' => implode("|", $this->config->item('SETTINGS_INTRODUCTION_IMAGE_UPLOAD_EXTENSIONS'))
			);
			$this->load->library('upload', $_upload_config);

			// 画像アップロード
			if ($this->upload->do_upload('introimg')) {
				$file_info = $this->upload->data();
				// 保存先ディレクトリ生成
				// 保存先パス
				$_save_path = $this->config->item('SETTINGS_INTRODUCTION_IMAGE_SAVE_PATH');
				if (!file_exists($_save_path)) {
					mkdir($_save_path, 0777, true);
				}
				// 保存先パス＋_yyyyMMddHHmmssSSS＋拡張子
				$_filename = $this->commonlib->getTimeWithMillSecond().$file_info['file_ext'];
				$_save_file_path = $_save_path.DIRECTORY_SEPARATOR.$_filename;

				// リサイズ後の画像サイズ算出
				// 画像処理ライブラリロード
				$this->load->library('imagelib');
				$_resize_size = $this->imagelib->calResizeSize($file_info['full_path'], $this->config->item('SETTINGS_INTRODUCTION_IMAGE_MAX_LENGTH'));

				// 画像リサイズ処理
				if (!is_null($_resize_size)) {
					$_image_config = array(
							'source_image' => $file_info['full_path'],
							'new_image' => $_save_file_path,
							'width' => $_resize_size[0],
							'height' => $_resize_size[1]
					);
					// CodeIgniterの画像処理ライブラリロード
					$this->load->library('image_lib', $_image_config);
					// リサイズ実行
					$this->image_lib->resize();
					// リサイズ元のアップロード画像削除
					unlink($file_info['full_path']);
				} else {
					// リサイズしなかった場合はファイルを保存先に移動
					rename($file_info['full_path'], $_save_file_path);
				}
				echo $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_SETTING')."&kind=intro&fl=".$_filename;
				exit;
			} else {
				// 画像アップロードエラー
				// エラーメッセージ生成
				header("HTTP/1.0 404 Not Found");
				echo $this->upload->display_errors('<div class="alert alert-danger" role="alert">', '</div>');
				exit;
			}
		}
	}

	/**
	 * 壁紙画像アップロード
	 *
	 * return string 保存した画像ファイル名
	 */
	private function back_image_upload() {
		// 画像アップロードパス取得
		$_upload_path = $this->config->item('SETTINGS_BACKGROUND_IMAGE_UPLOAD_PATH');
		if (!file_exists($_upload_path)) {
			mkdir($_upload_path, 0777, true);
		}
		// 画像ライブラリロード
		$_upload_config = array(
				'max_size' => $this->config->item('SETTINGS_BACKGROUND_IMAGE_UPLOAD_MAX_SIZE'),
				'encrypt_name' => true,
				'upload_path' => $_upload_path,
				'allowed_types' => implode("|", $this->config->item('SETTINGS_BACKGROUND_IMAGE_UPLOAD_EXTENSIONS'))
		);
		$this->load->library('upload', $_upload_config);

		// 画像アップロード
		if ($this->upload->do_upload('background_img')) {
			$file_info = $this->upload->data();
			// 保存先ディレクトリ生成
			$_save_path = $this->config->item('SETTINGS_BACKGROUND_IMAGE_SAVE_PATH');
			if (!file_exists($_save_path)) {
				mkdir($_save_path, 0777, true);
			}
			// 保存先パス＋現在日時＋拡張子
			$_save_file_path = $_save_path.DIRECTORY_SEPARATOR.$this->commonlib->getTimeWithMillSecond().$file_info['file_ext'];

			// リサイズ後の画像サイズ算出
			// 画像処理ライブラリロード
			$this->load->library('imagelib');
			$_resize_size = $this->imagelib->calResizeSize($file_info['full_path'], $this->config->item('SETTINGS_BACKGROUND_IMAGE_MAX_LENGTH'));

			// 画像リサイズ処理
			if (!is_null($_resize_size)) {
				$_image_config = array(
					'source_image' => $file_info['full_path'],
					'new_image' => $_save_file_path,
					'width' => $_resize_size[0],
					'height' => $_resize_size[1]
				);
				// CodeIgniterの画像処理ライブラリロード
				$this->load->library('image_lib', $_image_config);
				// リサイズ実行
				$this->image_lib->resize();
				// リサイズ元のアップロード画像削除
				unlink($file_info['full_path']);
			} else {
				// リサイズしなかった場合はファイルを保存先に移動
				rename($file_info['full_path'], $_save_file_path);
			}

			return basename($_save_file_path);
		} else {
			// 画像アップロードエラー
			// エラーメッセージ生成
			$this->error_message = $this->upload->display_errors();
		}
		return null;
	}

	/**
	 * ビュー表示
	 */
	private function displayView()
	{
		// ビューのデータに設定
		$_view_data['settings'] = $this->setting_model->get_display_data_for_settings();
		$_view_data['colorpicker_colors'] = $this->config->item('SIMPLECOLORPICKER_OPTIONTAG_COLORS_FOR_BACKGROUND');
		$_wallpaper = $this->session->userdata('wallpaper');
		if (!$_wallpaper) {
			$_wallpaper = $this->commonlib->baseUrl().$this->config->item('SETTINGS_BACKGROUND_NO_IMAGE_URL');
		}
		$_view_data['wallpaper'] = $_wallpaper;
		$_view_data['backkind'] = $this->backkind;
		$_view_data['script'] = $this->createJavaScript();
		if (!is_null($this->error_message)) {
			$_view_data['error_msg'] = $this->error_message;
		}

		$_cmp = $this->input->get('cmp', true);
		if (!is_null($_cmp) && $_cmp == 1) {
			$_view_data['complete_msg'] = $this->lang->line('settings_complete_msg_regist');
		}

		// 戻る画面設定
		if ($this->backkind == self::BACK_PAGE_KIND_USERMANAGER) {
			$this->setBackUrl($this->commonlib->baseUrl()."usermanager");
		} else {
			$this->setBackUrl($this->commonlib->baseUrl()."genremanager");
		}

		$this->load->view('common_header'); // 共通ヘッダー部
		$this->load->view('settings', $_view_data); // コンテンツ部
		$this->load->view('common_footer'); // 共通フッター部
	}

	/**
	 * URLパラメータ用検索パラメータ生成
	 *
	 * @return URLパラメータ用検索パラメータ文字列
	 */
	private function createUrlParamsOfSearch()
	{
		$_rtn = '';
		if (!is_null($this->search_userid) && $this->search_userid !== '') {
			$_rtn .= '&userid='.urlencode($this->search_userid);
		}
		if (!is_null($this->search_surname) && $this->search_surname !== '') {
			$_rtn .= '&surname='.urlencode($this->search_surname);
		}
		if (!is_null($this->search_firstname) && $this->search_firstname !== '') {
			$_rtn .= '&firstname='.urlencode($this->search_firstname);
		}
		return $_rtn;
	}

	/**
	 * JavaScriptデータ生成
	 */
	private function createJavaScript()
	{
		$_script = <<<EOT
<script>
	var summernoteLang = "{$this->config->item('SUMMERNOTE_LANG')}";
	var titleMaxLength = {$this->title_maxlength};
	var errMsgBackgroundimgRequired = "{$this->lang->line('settings_script_err_msg_backgroundimg_required')}";
	var errMsgTitleMaxlength = "{$this->lang->line('settings_script_err_msg_title_maxlength')}";
</script>
EOT;
		return $_script;
	}

}
