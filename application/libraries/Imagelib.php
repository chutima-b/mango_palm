<?php
/*
 * 画像処理ライブラリ
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Imagelib {

	/**
	 * リサイズ後の画像サイズ算出.
	 *
	 * @param string $srcPath   元画像ファイルパス
	 * @param int $maxLength 長辺最大長
	 * @return array リサイズ後の画像サイズ (width, height)
	 */
	public function calResizeSize($srcPath, $maxLength)
	{
		$_size = getimagesize($srcPath);
		$_width_orig = $_size[0];
		$_height_orig = $_size[1];

		$_width = $_width_orig;
		$_height = $_height_orig;
		$_ratio = 0;
		if ($_width_orig > $_height_orig) {
			if ($maxLength >= $_width_orig) {
				return null;
			}
			$_ratio = $maxLength / $_width_orig;
		} else {
			if ($maxLength >= $_height_orig) {
				return null;
			}
			$_ratio = $maxLength / $_height_orig;
		}
		$_width = (int)($_width_orig * $_ratio);
		$_height = (int)($_height_orig * $_ratio);
		return array($_width, $_height);
	}

	/**
	 * 画像を正方形にリサイズし、指定された長さに拡大・縮小する。
	 * ※ソース画像は削除する。
	 *
	 * @param CI_Controller $loader コントローラーオブジェクト
	 * @param string $srcPath ソース画像パス
	 * @param string $destPath 画像保存先パス
	 * @param int $length １辺の長さ(px)
	 */
	public function resizeSquare(&$controller, $srcPath, $destPath, $length)
	{
		$_size = getimagesize($srcPath);
		$_width_orig = $_size[0];
		$_height_orig = $_size[1];

		$_width = $_width_orig;
		$_height = $_height_orig;

		// 保存先保存済みフラグ
		$_is_saved = false;

		// 正方形ではない場合はトリミングする
		if ($_width_orig != $_height_orig) {
			$_x_axis = 0; // 切り取る画像のX座標(左から)の始点ピクセル
			$_y_axis = 0; // 切り取る画像のY座標(上から)の始点ピクセル
			if ($_width_orig > $_height_orig) {
				// 横幅が大きい場合は横幅を高さに合わせる。
				$_trim_size = $_width_orig - $_height_orig;
				$_width = $_width_orig - $_trim_size;
				$_x_axis = floor($_trim_size / 2);
			} else {
				// 高さが大きい場合は高さを横幅に合わせる。
				$_trim_size = $_height_orig - $_width_orig;
				$_height = $_height_orig - $_trim_size;
				$_y_axis = floor($_trim_size / 2);
			}
			// ワークディレクトリ生成
			$_work_path = $controller->config->item('IMGLIB_WORK_PATH');
			if (!file_exists($_work_path)) {
				mkdir($_work_path, 0777, true);
			}
			$_work_file_path = $_work_path.DIRECTORY_SEPARATOR.$controller->commonlib->getTimeWithMillSecond().'_'.basename($srcPath);
			// トリミング実行
			$_config = array(
				'source_image' => $srcPath,
				'new_image' => $_work_file_path,
				'width' => $_width,
				'height' => $_height,
				'x_axis' => $_x_axis,
				'y_axis' => $_y_axis,
				'maintain_ratio' => false,
			);
			$controller->load->library('image_lib', $_config, 'image_lib_for_square');
			$controller->image_lib_for_square->crop();
			// ソースファイルを削除
			unlink($srcPath);
			// ソースファイルパスをワークワイルのパスに差替え
			$srcPath = $_work_file_path;
		}

		// 画像サイズが指定された長さではない場合は拡大・縮小処理を実施する。
		if ($_width != $length) {
			$_config = array(
					'source_image' => $srcPath,
					'new_image' => $destPath,
					'width' => $length,
					'height' => $length
			);
			// CodeIgniterの画像処理ライブラリロード
			$controller->load->library('image_lib', $_config, 'image_lib_for_resize');
			// リサイズ実行
			$controller->image_lib_for_resize->resize();
			// ソースファイルを削除
			unlink($srcPath);

			// リサイズした場合は既に画像保存先に保存済みなので
			// フラグを立てておく
			$_is_saved = true;
		}

		if (!$_is_saved) {
			// 画像保存先に保存していない場合は
			// ソースファイルを画像保存先に保存する。
			rename($srcPath, $destPath);
		}
	}

}
