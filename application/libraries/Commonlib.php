<?php
/*
 * 共通ライブラリ
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Commonlib {

	/**
	 * 基準URL取得.
	 */
	public function baseUrl() {
		//return base_url();
		$CI =& get_instance();
		$_url_subdir = $CI->config->item('URL_SUBDIR');
		if (strlen($_url_subdir) > 0) {
			$_url_subdir .= '/';
		}
		if (!isset($_SERVER['HTTPS']) && !isset($_SERVER['HTTP_HOST'])) {
			return null;
		}
		return (empty($_SERVER['HTTPS']) ? 'http://' : 'https://').$_SERVER['HTTP_HOST'].'/'.$_url_subdir;
	}

	/**
	 * Assets Url.
	 */
	public function assetsUrl() {
		return base_url() . "html/";
	}
	
	/**
	 * ミリ秒付き日時取得
	 *
	 * @param string $format フォーマット文字列
	 * @param string $millisec_delim ミリ秒連結区切り文字
	 * @return string ミリ秒付き日時
	 */
	public function getTimeWithMillSecond($format='YmdHis', $millisec_delim='') {
		$arry_time = explode('.', microtime(true));
		return date($format, $arry_time[0]).$millisec_delim.substr($arry_time[1], 0, 3);
	}

	/**
	 * ファイルディレクトリコピー
	 *
	 * 指定されたコピー元を、指定されたコピー先に再帰的にコピーする
	 *
	 * @param string $src_path コピー元パス
	 * @param string $dest_path コピー先パス
	 * @return boolean コピー結果（true: 成功 / false: 失敗）
	 */
	public function copyFiles($src_path, $dest_path) {
		if (!file_exists($src_path)) {
			// コピー元パスが存在しない場合は何もしない
			return true;
		}
		if (is_dir($src_path)) {
			// コピー元がディレクトリだった場合は
			// コピー先にディレクトリを作成
			if (!is_dir($dest_path)) {
				if (!mkdir($dest_path, 0777, true)) {
					return false;
				}
			}
			// コピー元ディレクトリを開く
			if(!$_dir_handle = @opendir($src_path)) {
				return false;
			}
			// コピー元ディレクトリ内を展開
			while (false !== ($_entry = readdir($_dir_handle))) {
				if($_entry == '.' || $_entry == '..') {
					continue;
				}
				// コピー元、コピー先のパスを生成し、
				// 当関数を再帰呼び出し
				$_child_src_path = $src_path.DIRECTORY_SEPARATOR.$_entry;
				$_child_dest_path = $dest_path.DIRECTORY_SEPARATOR.$_entry;
				if (!$this->copyFiles($_child_src_path, $_child_dest_path)) {
					return false;
				}
			}
		} else {
			return copy($src_path, $dest_path);
		}
		return true;
	}

	/**
	 * 指定ディレクトリ以下を再帰的に削除
	 *
	 * @param string $dir ディレクトリ名
	 */
	public function removeDirs($dir) {
		if(!$_dir_handle = @opendir($dir)) {
			return;
		}
		while (false !== ($_entry = readdir($_dir_handle))) {
			if($_entry == '.' || $_entry == '..') {
				continue;
			}
			$_target_path = $dir.DIRECTORY_SEPARATOR.$_entry;
			if (is_dir($_target_path)) {
				$this->removeDirs($_target_path);
			} else {
				@unlink($_target_path);
			}
		}
		closedir($_dir_handle);
		@rmdir($dir);
	}

	/**
	 * 文字列を改行ごとに配列に変換
	 *
	 * @param string $string 変換対象文字列
	 * @return array 改行ごとに分割された配列データ
	 */
	public function nl2Array($string) {
		$_array = explode("\n", $string); // とりあえず\nごとに配列化
		$_array = array_map('trim', $_array); // trimで余計なホワイトスペース除去
		$_array = array_filter($_array, 'strlen'); // 空文字の行を削除
		$_array = array_values($_array); // 配列のキーを振り直す
		return $_array;
	}

	/**
	 * WYSIWYG用XSS文字列除去
	 *
	 * @param string $string 対象文字列
	 * @return string 除去後文字列
	 */
	public function removeXssForWYSIWYG($string) {
		$_rtn = $string;
		$_rtn = str_ireplace('<script', '', $_rtn);
		$_rtn = str_ireplace('</script>', '', $_rtn);
		$_rtn = str_ireplace('<form', '', $_rtn);
		$_rtn = str_ireplace('</form>', '', $_rtn);
// 動画を貼り付けられなくなるのでiframeは除去しない
//		$_rtn = str_ireplace('<iframe', '', $_rtn);
//		$_rtn = str_ireplace('</iframe>', '', $_rtn);
		$_rtn = str_ireplace('javascript:', '', $_rtn);
		$_rtn = str_ireplace('onmouse', '', $_rtn);
		$_rtn = str_ireplace('onerror', '', $_rtn);
		$_rtn = str_ireplace('onload', '', $_rtn);
		$_rtn = str_ireplace(':expression', '', $_rtn);
		$_rtn = str_ireplace('data:text/html', '', $_rtn);
		$_rtn = str_ireplace('behavior:url', '', $_rtn);
		return $_rtn;
	}

	/**
	 * パスワード文字列チェック
	 *
	 * @param string $string
	 * @return boolean true：OK / false：NG
	 */
	public function checkPasswordString($string) {
		// 半角英数記号チェック
		return preg_match("/^[!-~]+$/", $string);
	}

	/**
	 * フルネーム生成
	 * 言語依存設定ファイル(slms_language.php)の$config['NAME_STYLE']に定義された形式で
	 * フルネーム文字列を生成する
	 *
	 * @param string $firstname 名
	 * @param string $lastname 姓
	 * @return string フルネーム
	 */
	public function createFullName($firstname, $lastname) {
		$CI =& get_instance();
		$_rep_firstname = $CI->config->item('NAME_STYLE_FIRSTNAME');
		$_rep_lastname = $CI->config->item('NAME_STYLE_LASTNAME');
		$_name_style = $CI->config->item('NAME_STYLE');
		return str_replace(array($_rep_firstname, $_rep_lastname), array($firstname, $lastname), $_name_style);
	}

}
