<?php
/*
 * 教材情報モデル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

class Materials_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 教材パス取得
	 *
	 * @param int $id 教材テーブルID
	 * @return string 教材パス
	 */
	public function getMaterialPath($id) {

		$_sql = "SELECT material_path FROM materials WHERE id = ?";
		$_values = array($id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result[0]->material_path;
		}
		return null;
	}

	/**
	 * 講座情報テーブルID指定教材パス取得
	 *
	 * @param int $id 教材テーブルID
	 * @param int $cid 講座情報テーブルID
	 * @return string 教材パス
	 */
	public function getMaterialPathWithCid($id, $cid) {

		$_sql = "SELECT material_path FROM materials WHERE id = ? AND course_id = ?";
		$_values = array($id, $cid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result[0]->material_path;
		}
		return null;
	}

	/**
	 * 教材情報取得
	 *
	 * @param int $id 教材情報テーブルID
	 * @return array 教材情報
	 */
	public function getMaterialInfo($id) {

		$sql =<<< EOF
			SELECT
				`id`,
				`course_id`,
				`theme_id`,
				`material_description`,
				`material_type`,
				`play_end`,
				`external_material_path`,
				`material_path`
			FROM
				`materials`
			WHERE
				`id` = ?
EOF;

		$_values = array($id);
		if ($query = $this->db->query($sql, $_values)) {
			// 成功処理
			return $query->result('array');
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * 教材情報削除
	 *
	 * @param int $mid 教材情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function delete($mid) {
		$_sql ="DELETE FROM materials WHERE id = ?";
		$_values = array($mid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * テーマに紐付く教材情報取得
	 *
	 * @param int $id テーマID
	 * @return array テーマに紐付く教材情報
	 */
	public function getMaterialsList($id=NULL) {

		$sql =<<< EOF
			SELECT
				m.`id` AS id,
				m.`material_description` AS material_description,
				m.`material_type` AS material_type,
				m.`play_end` AS play_end,
				m.`external_material_path` AS external_material_path,
				m.`material_path` AS material_path
			FROM
				`materials` AS m
			INNER JOIN
				`theme` AS t
			ON (
				m.`theme_id` = t.`id`
			AND
				m.`course_id` = t.`course_id`
			)
			WHERE
				t.`id` = ?
			ORDER BY
				m.`material_type`, m.`material_description`
EOF;

		$_values = array($id);
		if ($query = $this->db->query($sql, $_values)) {
			// 成功処理
			return $query->result('array');
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * 指定ユーザーの教材再生時間情報付の教材情報取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param int $thid テーマテーブルID
	 * @param int $uid ユーザーテーブルID
	 * @return object|NULL 指定ユーザーの教材再生時間情報付の教材情報
	 */
	public function getMaterialsListWithMaterialsPlay($cid, $thid, $uid)
	{
		$_sql =<<<EOT
SELECT
	m.id,
	m.material_description,
	m.material_type,
	m.external_material_path,
	m.material_path,
	mp.percentage
FROM
	materials AS m
LEFT JOIN
	materials_play AS mp
ON
	mp.material_id = m.id
AND
	mp.course_id = m.course_id
AND
	mp.theme_id = m.theme_id
AND
	mp.uid = ?
WHERE
	m.course_id = ?
AND
	m.theme_id = ?
ORDER BY
	m.material_type, m.material_description
EOT;

		$_values = array($uid, $cid, $thid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		}
		return null;
	}

	/**
	 * 講座に紐付く教材ファイル格納先PATH取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return object 講座に紐付く教材ファイル格納先PATH
	 */
	public function getMaterialPathByCourseId($cid) {
		$_sql = "SELECT material_path FROM materials WHERE course_id = ?";
		$_values = array($cid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			// 成功処理
			return $_query->result('object');
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * 講座に紐付く教材情報削除
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByCourseId($cid) {
		$_sql ="DELETE FROM materials WHERE course_id = ?";
		$_values = array($cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * テーマに紐付く教材ファイル格納先PATH取得
	 *
	 * @param int $thid テーマ情報テーブルID
	 * @return object テーマに紐付く教材ファイル格納先PATH
	 */
	public function getMaterialPathByThemeId($thid) {
		$_sql = "SELECT material_path FROM materials WHERE theme_id = ?";
		$_values = array($thid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			// 成功処理
			return $_query->result('object');
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * テーマに紐付く教材情報削除
	 *
	 * @param int $thid テーマ情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByThemeId($thid) {
		$_sql ="DELETE FROM materials WHERE theme_id = ?";
		$_values = array($thid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 教材情報登録
	 *
	 * @param string $save_path 教材保存パス
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function insaertMaterial($cid, $thid, $description, $type, $play_end, $external, $save_path) {

		$sql =<<< EOF
			INSERT INTO
				`materials`
			(
			    `course_id`,
			    `theme_id`,
			    `material_description`,
				`material_type`,
				`play_end`,
				`external_material_path`,
				`material_path`,
			    `created_at`
			)
			VALUES
			(
			    ?,
			    ?,
				?,
				?,
				?,
			    ?,
			    ?,
			    CURRENT_TIMESTAMP
			)
EOF;
		$_values = array($cid, $thid, $description, $type, $play_end, $external, $save_path);
		if ($this->db->query($sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定テーマの教材コピー
	 *
	 * 指定されたテーマに紐付く教材をコピーする。
	 *
	 * @param int $src_cid コピー元講座情報テーブルID
	 * @param int $dest_cid コピー先講座情報テーブルID
	 * @param int $src_thid コピー元テーマテーブルID
	 * @param int $dest_thid コピー先テーマテーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function copyOfTheme($src_cid, $dest_cid, $src_thid, $dest_thid) {
		$_search_str = DIRECTORY_SEPARATOR.$src_cid.DIRECTORY_SEPARATOR.$src_thid.DIRECTORY_SEPARATOR;
		$_replace_str = DIRECTORY_SEPARATOR.$dest_cid.DIRECTORY_SEPARATOR.$dest_thid.DIRECTORY_SEPARATOR;
		$_sql =<<<EOT
INSERT INTO materials
(
	course_id,
	theme_id,
	material_description,
	material_type,
	play_end,
	external_material_path,
	material_path
)
SELECT
	? AS course_id,
	? AS theme_id,
	material_description,
	material_type,
	play_end,
	external_material_path,
	CASE WHEN material_path IS NOT NULL THEN REPLACE(material_path, ?, ?) ELSE NULL END AS material_path
FROM
	materials
WHERE
	course_id = ?
AND
	theme_id = ?
ORDER BY id
EOT;
		$_values = array($dest_cid, $dest_thid, $_search_str, $_replace_str, $src_cid, $src_thid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

}