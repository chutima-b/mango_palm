<?php
/*
 * フォーラム情報モデル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

class Forum_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();

		// 独自処理
	}

	/**
	 * フォーラム一覧取得
	 *
	 * @param int $page 現在表示しているページ
	 * @param int $limit 取得件数
	 * @param int $fid フォーラム管理ID
	 * @return array フォーラム一覧
	 */
	public function getForumList($page, $limit, $fid) {
		$_offset = $limit * ($page - 1);

		$sql =<<< EOF
			SELECT
				f.`id`,
				f.`forum_id`,
				f.`uid`,
				f.`message`,
				f.`image_file`,
				DATE_FORMAT(f.`created_at`, '%Y/%m/%d %H:%i') as post_time,
				u.`userid`,
				u.`nickname`,
				u.`picture_file`
			FROM
				`forum` AS f
			INNER JOIN
				`user` AS u
			ON (
				f.`uid` = u.`id`
			)
			WHERE
				f.`forum_id` = ?
			ORDER BY
				f.`id`
			LIMIT {$limit} OFFSET {$_offset}
EOF;

		$_values = array($fid);
		if ($query = $this->db->query($sql, $_values)) {
			return $query->result('array');
		}
		return null;
	}

	/**
	 * フォーラム総投稿数取得
	 *
	 * @param int $fid フォーラム管理ID
	 * @return int フォーラム総投稿数
	 */
	public function getTotalCount($fid) {
		$_sql =<<< EOF
			SELECT
				COUNT(f.`id`) AS cnt
			FROM
				`forum` AS f
			INNER JOIN
				`user` AS u
			ON (
				f.`uid` = u.`id`
			)
			WHERE
				f.`forum_id` = ?
EOF;

		$_values = array($fid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('object')[0]->cnt;
		}
		return 0;
	}

	/**
	 * ホーム画面（受講者）用フォーラム投稿一覧取得
	 * 自分が受講している講座のフォーラムを取得する。
	 * ※システム管理者の場合はすべてのフォーラムを取得する。
	 *
	 * @return object ホーム画面（受講者）用日記投稿一覧
	 */
	public function getForumListForStudentshome()
	{
		$_profile_default_image = $this->commonlib->baseUrl().$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
		$_profile_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=";
		$_authority = $this->session->userdata['authority'];
		$_uid = $_authority[0]->id;
		$_image_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_FORUM')."&id=";

		$_values = array($_uid);
		$_sql =<<<EOT
SELECT
	f.forum_id,
	m.forum_name,
	m.course_id,
	(CASE WHEN u.picture_file IS NULL THEN '{$_profile_default_image}' ELSE concat('{$_profile_save_url}', u.id) END) AS picture_file,
	u.nickname,
	f.message,
	(CASE WHEN f.image_file IS NOT NULL AND f.image_file <> '' THEN concat('{$_image_save_url}', f.id) ELSE NULL END) AS image_file,
	DATE_FORMAT(f.created_at, '%Y/%m/%d %H:%i') AS created_at
FROM
	forum AS f
INNER JOIN
	manage_forum AS m
ON
	m.id = f.forum_id
INNER JOIN
	user AS u
ON
	u.id = f.uid
AND
	u.validityflg = 1
WHERE
	m.course_id IN (
		SELECT s.course_id FROM students AS s WHERE s.uid = ?
	)
ORDER BY
	f.id DESC
LIMIT {$this->config->item('STUDENTSHOME_FORUM_MAX')}
EOT;
		if ($this->config->item('AUTH_SYS_MANAGER') == $_authority[0]->authority) {
			// システム管理者の場合は全フォーラム取得
			$_values = array();
			$_sql =<<<EOT
SELECT
	f.forum_id,
	m.forum_name,
	m.course_id,
	(CASE WHEN u.picture_file IS NULL THEN '{$_profile_default_image}' ELSE concat('{$_profile_save_url}', u.id) END) AS picture_file,
	u.nickname,
	f.message,
	(CASE WHEN f.image_file IS NOT NULL AND f.image_file <> '' THEN concat('{$_image_save_url}', f.id) ELSE NULL END) AS image_file,
	DATE_FORMAT(f.created_at, '%Y/%m/%d %H:%i') AS created_at
FROM
	forum AS f
INNER JOIN
	manage_forum AS m
ON
	m.id = f.forum_id
INNER JOIN
	user AS u
ON
	u.id = f.uid
AND
	u.validityflg = 1
ORDER BY
	f.id DESC
LIMIT {$this->config->item('STUDENTSHOME_FORUM_MAX')}
EOT;
		}

		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('object');
		} else {
			return null;
		}
	}

	/**
	 * テーマ画面用フォーラム投稿一覧取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return object テーマ画面用フォーラム投稿一覧
	 */
	public function getForumListForTheme($cid)
	{
		$_profile_default_image = $this->commonlib->baseUrl().$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
		$_profile_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=";
		$_image_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_FORUM')."&id=";

		$_sql =<<<EOT
SELECT
	f.forum_id,
	m.forum_name,
	m.course_id,
	(CASE WHEN u.picture_file IS NULL THEN '{$_profile_default_image}' ELSE concat('{$_profile_save_url}', u.id) END) AS picture_file,
	u.nickname,
	f.message,
	(CASE WHEN f.image_file IS NOT NULL AND f.image_file <> '' THEN concat('{$_image_save_url}', f.id) ELSE NULL END) AS image_file,
	DATE_FORMAT(f.created_at, '%Y/%m/%d %H:%i') AS created_at
FROM
	forum AS f
INNER JOIN
	manage_forum AS m
ON
	m.id = f.forum_id
INNER JOIN
	user AS u
ON
	u.id = f.uid
AND
	u.validityflg = 1
WHERE
	m.course_id = ?
ORDER BY
	f.id DESC
LIMIT {$this->config->item('THEME_FORUM_MAX')}
EOT;

		$_values = array($cid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('object');
		} else {
			return null;
		}
	}

	/*
	 * フォーラム画面投稿削除
	 */
	public function deletePost($pid)
	{
		$_values = array($pid);

		$sql =<<<EOF
			DELETE
			FROM
			  `forum`
			WHERE
			  `id` = ?
EOF;
		$this->db->query($sql, $_values);
	}

	/*
	 * フォーラム画面新規投稿
	 */
	public function registPost($fid, $postmsg, $imagefile)
	{
		$authority = $this->session->userdata['authority'];
		$uid = $authority[0]->id;

		$sql =<<<EOF
			INSERT INTO
			    `forum`
			(
			    `forum_id`,
			    `uid`,
			    `message`,
			    `image_file`
			)
			VALUES
			(
				?,
				?,
				?,
				?
			)
EOF;
		$_values = array($fid, $uid, $postmsg, $imagefile);
		$this->db->query($sql, $_values);
	}

	/*
	 * フォーラム画面投稿編集
	 */
	public function editPost($pid, $postmsg, $imagefile)
	{
		// イメージファイルが未設定の場合更新対象外とする
		if ($imagefile == null or $imagefile == "") {
			$sql =<<<EOF
				UPDATE
					`forum`
				SET
					`message` = ?,
					`updated_at` = CURRENT_TIMESTAMP
				WHERE
					`id` = ?
EOF;
			$_values = array($postmsg, $pid);
		} else {
			$sql =<<<EOF
				UPDATE
					`forum`
				SET
					`message` = ?,
					`image_file` = ?,
					`updated_at` = CURRENT_TIMESTAMP
				WHERE
					`id` = ?
EOF;
			$_values = array($postmsg, $imagefile, $pid);
		}
		$this->db->query($sql, $_values);
	}

	/*
	 * フォーラム画面投稿取得
	 */
	public function getPostInfo($pid)
	{
		$sql =<<<EOF
			SELECT
				`id`,
				`forum_id`,
				`uid`,
				`message`,
				`image_file`,
				`created_at`,
				`updated_at`
			FROM
				`forum`
			WHERE
				`id` = ?
EOF;
		$_values = array($pid);
		if ($query = $this->db->query($sql, $_values)) {
			// 成功処理
			$retData = $query->result('array');
		}
	}

	/**
	 * 画像ファイル取得
	 *
	 * @param int $fid フォーラムID
	 * @return string 画像ファイル
	 */
	public function getImageFile($fid) {
		$sql =<<<EOF
			SELECT
				`image_file`
			FROM
				`forum`
			WHERE
				`id` = ?
EOF;
		$_values = array($fid);
		if ($query = $this->db->query($sql, $_values)) {
			// 成功処理
			if (0 < $query->num_rows()) {
				$tmp = $query->result('array')[0];
				return $tmp['image_file'];
			}
		}
	}

	/**
	 * 指定ユーザー画像ファイル取得
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return object 指定ユーザー画像ファイル
	 */
	public function getImageFiles($uid)
	{
		$_file_path = $this->config->item('FORUM_IMAGE_SAVE_PATH').'/';

		$_sql =<<<EOT
SELECT
	concat('{$_file_path}', image_file)
FROM
	forum
WHERE
	uid = ?
AND
	image_file IS NOT NULL
EOT;
		$_values = array($uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * 指定ユーザーデータ削除
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByUid($uid)
	{
		$_sql = "DELETE FROM forum WHERE uid = ?";
		$_values = array($uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定講座データ削除
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByCid($cid)
	{
		$_sql =<<<EOT
DELETE
	f
FROM
	forum AS f
INNER JOIN
	manage_forum AS mf
ON
	mf.id = f.forum_id
WHERE mf.course_id = ?
EOT;
		$_values = array($cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

}