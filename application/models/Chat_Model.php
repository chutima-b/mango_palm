<?php
/*
 * チャット画面モデル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Chat_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * チャットデータリスト取得
	 *
	 * @param int $id 講座情報テーブルID
	 * @param int $uid ユーザーテーブルID
	 * @param int $tkuid 会話相手のユーザーテーブルID
	 * @param boolean $is_limit 取得件数指定フラグ（true：取得件数指定あり / false：取得件数指定なし）
	 * @param int $offset_id オフセットするID（このIDより“後”の会話を取得）（このIDを含めない）
	 * @param boolean $is_array 返却データの配列指定（true：配列で返却 / false：objectで返却）
	 * @return array|object チャットデータリスト
	 */
	public function getChatDataList($cid, $uid, $tkuid, $is_limit = true, $offset_id=null, $is_array=false)
	{
		$_limit = '';
		if ($is_limit) {
			$_limit = "LIMIT {$this->config->item('CHAT_DISP_COUNT')}";
		}
		$_offset = '';
		if (!is_null($offset_id)) {
			// 指定IDより後の会話を取得
			$_offset = "AND c.id > ?";
		}

		$_profile_default_image = $this->commonlib->baseUrl().$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
		$_profile_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=";
		$_chat_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_CHAT')."&cid=".$cid."&id=";

		$_sql =<<<EOT
SELECT
    c.id,
    (SELECT u1.nickname FROM user AS u1 WHERE u1.id = c.uid) AS nickname,
    c.post,
    (SELECT
        (CASE WHEN u2.picture_file IS NULL THEN '{$_profile_default_image}' ELSE concat('{$_profile_save_url}', u2.id) END)
    FROM user AS u2 WHERE u2.id = c.uid) AS profile_img,
    (CASE WHEN c.uid = ? THEN 1 ELSE 0 END) AS is_self,
    (CASE WHEN c.image_file IS NULL THEN NULL ELSE concat('{$_chat_save_url}', c.id) END) AS image_file,
    DATE_FORMAT(c.created_at, '%Y/%m/%d %H:%i') AS created_at
FROM
    chat AS c
WHERE
    c.course_id = ?
{$_offset}
AND
   (
        (c.uid = ? AND c.talk_uid = ?)
    OR
        (c.uid = ? AND c.talk_uid = ?)
    )
ORDER BY
    c.id DESC
{$_limit}
EOT;
		$_values = array($uid, $cid, $uid, $tkuid, $tkuid, $uid);
		if (!is_null($offset_id)) {
			$_values = array($uid, $cid, $offset_id, $uid, $tkuid, $tkuid, $uid);
		}
		$_query = $this->db->query($_sql, $_values);
		if ($_query
		 && $_query->num_rows() > 0) {
			if ($is_array) {
				return array_reverse($_query->result('array'));
			} else {
				return array_reverse($_query->result('object'));
			}
		} else {
			return null;
		}
	}

	/**
	 * チャット過去データリスト取得
	 *
	 * @param int $id 講座情報テーブルID
	 * @param int $uid ユーザーテーブルID
	 * @param int $tkuid 会話相手のユーザーテーブルID
	 * @param int $offset_id オフセットするID（このIDより“前”の会話を取得）（このIDを含めない）
	 * @param boolean $is_array 返却データの配列指定（true：配列で返却 / false：objectで返却）
	 * @return array チャット過去データリスト
	 */
	public function getPastChatDataList($cid, $uid, $tkuid, $offset_id)
	{
		$_profile_default_image = $this->commonlib->baseUrl().$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
		$_profile_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=";
		$_chat_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_CHAT')."&cid=".$cid."&id=";

		$_sql =<<<EOT
SELECT
    c.id,
    (SELECT u1.nickname FROM user AS u1 WHERE u1.id = c.uid) AS nickname,
    c.post,
    (SELECT
        (CASE WHEN u2.picture_file IS NULL THEN '{$_profile_default_image}' ELSE concat('{$_profile_save_url}', u2.id) END)
    FROM user AS u2 WHERE u2.id = c.uid) AS profile_img,
    (CASE WHEN c.uid = ? THEN 1 ELSE 0 END) AS is_self,
    (CASE WHEN c.image_file IS NULL THEN NULL ELSE concat('{$_chat_save_url}', c.id) END) AS image_file,
    DATE_FORMAT(c.created_at, '%Y/%m/%d %H:%i') AS created_at
FROM
    chat AS c
WHERE
    c.course_id = ?
AND
	/* 指定IDより前の会話を取得 */
	c.id < ?
AND
   (
        (c.uid = ? AND c.talk_uid = ?)
    OR
        (c.uid = ? AND c.talk_uid = ?)
    )
ORDER BY
    c.id DESC
LIMIT {$this->config->item('CHAT_DISP_COUNT')}
EOT;
		$_values = array($uid, $cid, $offset_id, $uid, $tkuid, $tkuid, $uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query
		 && $_query->num_rows() > 0) {
			return array_reverse($_query->result('array'));
		} else {
			return null;
		}
	}

	/**
	 * 指定チャット相手のチャットユーザーリスト取得
	 * ※チャット管理画面のユーザー一覧プルダウンにて使用
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param int $tkuid 会話相手のユーザーテーブルID
	 * @return object チャットユーザーリスト
	 */
	public function getUserList($cid, $tkuid)
	{
		$_sql =<<<EOT
SELECT
	DISTINCT(c.uid),
	u.nickname
FROM
	chat AS c
INNER JOIN
	user AS u
ON
	u.id = c.uid
WHERE
	c.course_id = ?
AND
	c.talk_uid = ?
ORDER BY
	u.id
EOT;
		$_values = array($cid, $tkuid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query
		&& $_query->num_rows() > 0) {
			return $_query->result('object');
		} else {
			return null;
		}
	}

	/**
	 * チャット登録
	 *
	 * @param int $id 講座情報テーブルID
	 * @param int $uid ユーザーテーブルID
	 * @param int $tkuid 会話相手のユーザーテーブルID
	 * @param int $post 投稿メッセージ
	 * @param int $img_file 投稿画像ファイル
	 * @return boolean 成功：true / 失敗：false
	 */
	public function regist($cid, $uid, $tkuid, $post, $img_file=null)
	{
		$_sql =<<<EOT
INSERT INTO chat (course_id, uid, talk_uid ,post, image_file)
VALUES (?, ?, ?, ?, ?)
EOT;
		$_values = array($cid, $uid, $tkuid, $post, $img_file);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定チャットID画像ファイル取得
	 *
	 * @param int $id チャットテーブルID
	 * @return object 指定チャットID画像ファイル
	 */
	public function getImageFile($id)
	{
		$_sql =<<<EOT
SELECT
	image_file
FROM
	chat
WHERE
	id = ?
EOT;
		$_values = array($id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object')[0];
			return $_result->image_file;
		} else {
			return null;
		}
	}

	/**
	 * 指定ユーザー画像ファイル取得
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return object 指定ユーザー画像ファイル
	 */
	public function getImageFiles($uid)
	{
		$_chat_save_path = $this->config->item('CHAT_IMAGE_SAVE_PATH').'/';

		$_sql =<<<EOT
SELECT
	concat('{$_chat_save_path}', image_file)
FROM
	chat
WHERE
	(uid = ? OR talk_uid = ?)
AND
	image_file IS NOT NULL
EOT;
		$_values = array($uid, $uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * 指定ユーザーデータ削除
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByUid($uid)
	{
		$_sql = "DELETE FROM chat WHERE uid = ? OR talk_uid = ?";
		$_values = array($uid, $uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

}
