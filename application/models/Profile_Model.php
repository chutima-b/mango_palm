<?php
/*
 * プロフィールモデル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

class Profile_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 指定IDの写真パス取得
	 *
	 * @param int $id ユーザーテーブルID
	 * @return string 写真パス
	 */
	public function getPictureFile($id)
	{
		$_sql = "SELECT picture_file FROM user WHERE id = ?";
		$_values = array($id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result[0]->picture_file;
		}
		return null;
	}

	/**
	 * プロフィール取得
	 *
	 * @param int $id ユーザーテーブルID
	 * @return object プロフィール情報
	 */
	public function get_profile($id)
	{
		$_sql =<<<EOT
SELECT
    nickname, picture_file, introduction, chat_use
FROM user
WHERE id=?
EOT;
		$_values = array($id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * ニックネームが登録されているか判定
	 *
	 * @param int $id ユーザーテーブルID
	 * @param string $nickname ニックネーム
	 * @return boolean true:登録済み / false:未登録
	 */
	public function hasNickname($id, $nickname)
	{
		$_sql =<<<EOT
SELECT id FROM user WHERE id <> ? AND nickname=?
EOT;
		$_values = array($id, $nickname);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * パスワード判定
	 * パスワードがあっているか判定する
	 *
	 * @param int $id
	 * @param string $password
	 * @return boolean true:OK / false:NG
	 */
	public function checkPassword($id, $password)
	{
		$_sql =<<<EOT
SELECT
	id
FROM
	user AS u
WHERE
	u.id=?
AND
	u.passwd=SHA2(?, 0)
AND
	u.validityflg = 1
LIMIT 1
EOT;
		$_values = array($id, $password);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * プロフィール登録
	 *
	 * @param string $nickname
	 * @param string $picture_file
	 * @param string $introduction
	 * @param int    $chat_use
	 * @param string $password
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function update_profile($id, $nickname, $picture_file, $introduction, $chat_use, $password)
	{
		$_values = array($nickname, $picture_file, $introduction, $chat_use, $id);
		$_update_passwd = "";
		if (!is_null($password)) {
			$_update_passwd = 'passwd = SHA2(?, 0),';
			$_values = array($nickname, $picture_file, $introduction, $chat_use, $password, $id);
		}
		$_sql =<<<EOT
UPDATE user
SET
	nickname = ?,
	picture_file = ?,
	introduction = ?,
	chat_use = ?,
	{$_update_passwd}
	updated_at = NOW()
WHERE id = ?
EOT;
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

}