<?php
/*
 * 各種消費量を調査モデル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

class Batch_Check_Consumption_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * データベース使用量取得
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return object データベース使用量取得
	 */
	public function getConsumptionOfDatabase()
	{
		$_sql =<<<EOT
SELECT
	table_schema,
	sum(data_length+index_length) /1024 /1024 AS MB
FROM
	information_schema.tables
WHERE
	table_schema = '{$this->db->database}'
EOT;
		$_query = $this->db->query($_sql);
		if ($_query->num_rows() > 0) {
			return $_query->result('object');
		}
		return null;
	}

}