<?php
/*
 * フォーラム管理情報モデル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
class Forum_Manage_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * フォーラム情報取得
	 *
	 * @param int $fid フォーラム管理ID
	 * @return array フォーラム情報
	 */
	public function getForumInfo($fid) {

		$sql =<<< EOF
			SELECT
			  `id`,
			  `course_id`,
			  `forum_name`,
			  `owner_uid`,
			  `use`
			FROM
			  `manage_forum`
			WHERE
			  `id` = ?
EOF;

		$_values = array($fid);
		if ($query = $this->db->query($sql, $_values)) {
			// 成功処理
			return $query->result('array');
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * フォーラム管理画面情報取得
	 *
	 * @param int 講座情報テーブルID
	 * @return array フォーラム管理画面情報
	 */
	public function getForumManageInfo($id) {

		$authority = $this->session->userdata['authority'];

		if ($this->config->item('AUTH_SYS_MANAGER') == $authority[0]->authority) {

			$sql =<<< EOF
				SELECT
					m.`id`,
					m.`forum_name`,
					m.`use`,
					1 AS owner_flg,
					u.`nickname`,
					(CASE WHEN cf.`cnt` is null THEN 0 ELSE cf.`cnt` END) AS cnt,
					(CASE WHEN nf.`lastest_post` is null THEN '' ELSE nf.`lastest_post` END) AS lastest_post
				FROM
					`manage_forum` AS m
				LEFT JOIN
					(SELECT
						`forum_id`,
						COUNT(*) AS cnt
					FROM
						`forum`
					GROUP BY
						`forum_id`) AS cf
				ON (
					m.id = cf.forum_id
				)
				LEFT JOIN
					(SELECT
						`forum_id`,
						DATE_FORMAT(max(`created_at`), '%Y/%m/%d %H:%i') AS lastest_post
					FROM
						`forum`
					GROUP BY
						`forum_id`) AS nf
				ON (
					m.id = nf.forum_id
				)
				INNER JOIN
					`user` AS u
				ON (
					m.owner_uid = u.id
				)
				WHERE
					`course_id` = ?
				ORDER BY
					m.id DESC
EOF;

			$_values = array($id);
			if ($query = $this->db->query($sql, $_values)) {
				// 成功処理
				return $query->result('array');
			} else {
				// 失敗処理
				return null;
			}
		} else {

			$uid = $authority[0]->id;

			$sql =<<< EOF
				SELECT
					m.`id`,
					m.`forum_name`,
					m.`use`,
					(CASE WHEN u.`id` = ? THEN 1 ELSE 0 END) AS owner_flg,
					u.`nickname`,
					(CASE WHEN cf.`cnt` is null THEN 0 ELSE cf.`cnt` END) AS cnt,
					(CASE WHEN nf.`lastest_post` is null THEN '' ELSE nf.`lastest_post` END) AS lastest_post
				FROM
					`manage_forum` AS m
				LEFT JOIN
					(SELECT
						`forum_id`,
						COUNT(*) AS cnt
					FROM
						`forum`
					GROUP BY
						`forum_id`) AS cf
				ON (
					m.id = cf.forum_id
				)
				LEFT JOIN
					(SELECT
						`forum_id`,
						DATE_FORMAT(max(`created_at`), '%Y/%m/%d %H:%i') AS lastest_post
					FROM
						`forum`
					GROUP BY
						`forum_id`) AS nf
				ON (
					m.id = nf.forum_id
				)
				INNER JOIN
					`user` AS u
				ON (
					m.owner_uid = u.id
				)
				WHERE
					`course_id` = ?
				ORDER BY
					m.id DESC
EOF;

			$_values = array($uid, $id);
			if ($query = $this->db->query($sql, $_values)) {
				// 成功処理
				return $query->result('array');
			} else {
				// 失敗処理
				return null;
			}
		}
	}

	/**
	 * フォーラム管理画面フォーラム停止処理
	 *
	 * @param int フォーラム管理ID
	 */
	public function stopForum($fid)
	{
		$use = 0; // 無効
		$this->editUse($fid, $use);
	}

	/**
	 * フォーラム管理画面フォーラム再開処理
	 *
	 * @param int フォーラム管理ID
	 */
	public function resumptionForum($fid)
	{
		$use = 1; // 有効
		$this->editUse($fid, $use);
	}

	/**
	 * フォーラム停止再開変更処理
	 *
	 * @param int フォーラム管理ID
	 * @param int 利用フラグ
	 */
	private function editUse($fid, $use)
	{
		$sql =<<<EOF
			UPDATE
			  `manage_forum`
			SET
			  `use` = ?,
			  `updated_at` = CURRENT_TIMESTAMP
			WHERE
			  `id` = ?
EOF;
		$_values = array($use, $fid);
		$this->db->query($sql, $_values);
	}

	/**
	 * フォーラム管理画面フォーラム削除
	 *
	 * @param int フォーラム管理ID
	 */
	public function deleteForum($fid)
	{
		$_values = array($fid);

		$sql =<<<EOF
			DELETE
			FROM
			  `forum`
			WHERE
			  `forum_id` = ?
EOF;
		$this->db->query($sql, $_values);

		$sql =<<<EOF
			DELETE
			FROM
			  `manage_forum`
			WHERE
			  `id` = ?
EOF;
		$this->db->query($sql, $_values);
	}

	/**
	 * 指定講座フォーラム管理情報削除
	 *
	 * @param int $cid 講座情報テーブルID
	 */
	public function deleteByCid($cid)
	{
		$_sql = "DELETE FROM manage_forum WHERE course_id = ?";
		$_values = array($cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * フォーラム登録
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param string $forum_name フォーラム名
	 * @return int フォーラム管理ID
	 */
	public function registrationForum($cid, $forum_name)
	{
		$authority = $this->session->userdata['authority'];
		$uid = $authority[0]->id;

		$sql =<<<EOF
			INSERT INTO
			    `manage_forum`
			(
			    `course_id`,
			    `forum_name`,
			    `owner_uid`,
			    `use`,
			    `created_at`
			)
			VALUES
			(
				?,
				?,
				?,
				1,
				CURRENT_TIMESTAMP
			)
EOF;
		$_values = array($cid, $forum_name, $uid);
		if ($this->db->query($sql, $_values)) {
			return $this->db->insert_id();
		} else {
			return null;
		}
	}

}