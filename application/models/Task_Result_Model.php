<?php
/*
 * 課題解答情報モデル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

class Task_Result_Model extends CI_Model {

	/** 講座テーブルID. */
	public $course_id = 0;
	/** テーマテーブルID. */
	public $theme_id = 0;
	/** 課題テーブルID. */
	public $task_id = 0;
	/** ユーザーテーブルID. */
	public $uid = 0;
	/** 解答単一選択. */
	public $result_radio = null;
	/** 解答複数選択. */
	public $result_check = null;
	/** 解答テキスト. */
	public $result_text = null;
	/** 解答ファイルパス. */
	public $result_file = null;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 該当テーマの課題解答情報が存在するか判定
	 *
	 * @param int $cid 講座ID
	 * @param int $thid テーマID
	 * @return boolean true:課題回答情報あり / false:課題回答情報なし
	 */
	public function hasResultOfTheme($cid, $thid)
	{
		$_sql =<<<EOT
SELECT COUNT(id) AS cnt FROM task_result WHERE course_id = ? AND theme_id = ?
EOT;
		$_values = array($cid, $thid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object')[0];
			if ($_result->cnt > 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 該当課題の課題解答情報が存在するか判定
	 *
	 * @param int $taid 課題ID
	 * @return boolean true:課題回答情報あり / false:課題回答情報なし
	 */
	public function hasResultOfTask($taid)
	{
		$_sql =<<<EOT
SELECT COUNT(id) AS cnt FROM task_result WHERE task_id = ?
EOT;
		$_values = array($taid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object')[0];
			if ($_result->cnt > 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 指定IDの解答ファイル取得
	 *
	 * @param int $id 課題解答情報テーブルID
	 * @param int $cid 講座ID
	 * @param int $authority 対象講座での権限
	 * @return string 解答ファイル
	 */
	public function getFilePath($id, $cid, $course_authority)
	{
		if ($course_authority == $this->config->item('AUTH_UNREGISTERED')) {
			return null;
		}

		$_sql = "SELECT result_file FROM task_result WHERE id = ? AND course_id = ?";
		$_values = array($id, $cid);

		if ($course_authority == $this->config->item('AUTH_STUDENT')) {
			// 受講者の場合は自分の解答ファイルのみ表示可能
			$_sql = "SELECT result_file FROM task_result WHERE id = ? AND course_id = ? AND uid = ?";
			$_authority_info = $this->session->userdata('authority');
			$_values = array($id, $cid, $_authority_info[0]->id);
		}
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result[0]->result_file;
		}
		return null;
	}

	/**
	 * 登録
	 */
	public function regist()
	{
		$_sql = <<<EOT
INSERT INTO task_result (course_id, theme_id, task_id, uid, result_radio, result_check, result_text, result_file, result_at)
VALUES
(?, ?, ?, ?, ?, ?, ?, ?, NOW())
ON DUPLICATE KEY UPDATE
result_radio = ?,
result_check = ?,
result_text = ?,
result_file = ?,
result_at = NOW(),
updated_at = NOW()
;
EOT;
		$_values = array(
				$this->course_id,
				$this->theme_id,
				$this->task_id,
				$this->uid,
				$this->result_radio,
				$this->result_check,
				$this->result_text,
				$this->result_file,
				$this->result_radio,
				$this->result_check,
				$this->result_text,
				$this->result_file
			);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 講座に紐付く課題解答情報削除
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByCourseId($cid)
	{
		$_sql = "DELETE FROM task_result WHERE course_id = ?";
		$_values = array($cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * テーマに紐付く課題解答情報削除
	 *
	 * @param int $thid テーマ情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByThemeId($thid)
	{
		$_sql = "DELETE FROM task_result WHERE theme_id = ?";
		$_values = array($thid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 課題解答情報削除
	 *
	 * @param int $taid 課題情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByTaskId($taid)
	{
		$_sql = "DELETE FROM task_result WHERE id = ?";
		$_values = array($taid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定ユーザーデータ削除
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByUid($uid)
	{
		$_sql = "DELETE FROM task_result WHERE uid = ?";
		$_values = array($uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定ユーザー画像ファイル取得
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return object 指定ユーザー画像ファイル
	 */
	public function getRealPathOfResultFiles($uid)
	{
		$_sql =<<<EOT
SELECT
	concat('{$this->config->item('TASK_IMAGE_SAVE_PATH')}', result_file) AS result_file
FROM
	task_result
WHERE
	uid = ?
AND
	result_file IS NOT NULL
EOT;
		$_values = array($uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * 課題回収情報取得
	 *
	 * @param int $taid 課題ID
	 * @param int $uid ユーザーID
	 * @return array 課題回収情報
	 */
	public function getTaskResult($taid, $uid) {
		$_sql =<<<EOT
			SELECT
				`id`,
				`course_id`,
				`theme_id`,
				`task_id`,
				`uid`,
				`result_radio`,
				`result_check`,
				`result_text`,
				`result_file`,
				`feedback_points`,
				`feedback_message`,
				`result_at`,
				`created_at`,
				`updated_at`
			FROM
				`task_result`
			WHERE
				`task_id` = ?
			AND
				`uid` = ?
EOT;
		$_values = array($taid, $uid);
		if ($query = $this->db->query($_sql, $_values)) {
			// 成功処理
			return $query->result('array');
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * 評価点合計取得
	 *
	 * @param int $cid 講座ID
	 * @param int $thid テーマID
	 * @param int $uid ユーザーテーブルID
	 * @return int 評価点合計
	 */
	public function getTotalFeedbackPoints($cid, $thid, $uid)
	{
		$_sql =<<<EOT
SELECT
	SUM(feedback_points) AS total_point
FROM
	task_result
WHERE
	course_id = ?
AND
	theme_id = ?
AND
	uid = ?
EOT;
		$_values = array($cid, $thid, $uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object')[0]->total_point;
			if (is_null($_result)) {
				$_result = 0;
			}
			return $_result;
		} else {
			return 0;
		}
	}

	/**
	 * 下書き保存更新
	 *
	 * @param int $taid 課題情報テーブルID
	 * @param int $uid ユーザーテーブルID
	 * @param int $point 評価点
	 * @param string $message フィードバックメッセージ
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function draftUpdate($taid, $uid, $point, $message) {
		$_sql =<<<EOT
			UPDATE
				`task_result`
			SET
				`feedback_points` = ?,
				`feedback_message` = ?,
				`updated_at` = CURRENT_TIMESTAMP
			WHERE
				`task_id` = ?
			AND
				`uid` = ?
EOT;
		$_values = array($point, $message, $taid, $uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * フィードバック更新
	 *
	 * @param int $taid 課題情報テーブルID
	 * @param int $uid ユーザーテーブルID
	 * @param int $point 評価点
	 * @param string $message フィードバックメッセージ
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function feedbackUpdate($taid, $uid, $point, $message) {
		$_sql =<<<EOT
			UPDATE
				`task_result`
			SET
				`feedback_points` = ?,
				`feedback_message` = ?,
				`updated_at` = CURRENT_TIMESTAMP
			WHERE
				`task_id` = ?
			AND
				`uid` = ?
EOT;
		$_values = array($point, $message, $taid, $uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

}