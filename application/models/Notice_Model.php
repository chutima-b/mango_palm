<?php
/*
 * お知らせ情報モデル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

class Notice_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();

		// 独自処理
	}

	/**
	 * ホーム画面のお知らせリスト取得用
	 * 全ユーザー向けのお知らせ
	 * および
	 * ユーザーが登録されている講座のお知らせを取得する
	 *
	 * @return array お知らせリスト
	 */
	public function  getNoticeList() {
		$authority = $this->session->userdata['authority'];

		$_sql =<<< EOF
SELECT
	n.id,
	n.title,
	DATE_FORMAT(n.target_dt,'%Y/%m/%d') AS target_dt,
	DATE_FORMAT(n.created_at,'%Y/%m/%d') AS created_at,
	n.content,
	n.course_id,
	n.calendar_use,
	n.calendar_color,
	(CASE WHEN {$this->config->item('NOTICE_NEW_INDICATION_PERIOD')} = 0 THEN NULL WHEN datediff(n.created_at, CURDATE()) > -{$this->config->item('NOTICE_NEW_INDICATION_PERIOD')} THEN 1 ELSE NULL END) AS is_new
FROM
	notice AS n
LEFT JOIN
	students AS s
ON
	s.course_id = n.course_id
AND
	s.course_id <> 0
WHERE
	(
		s.uid = ?
	OR
		n.course_id = 0
	OR
		n.course_id IS NULL
	)
ORDER BY
	n.id DESC
EOF;
		// システム管理者の場合は講座に関係なくお知らせを取得する
		if ($this->config->item('AUTH_SYS_MANAGER') == $authority[0]->authority) {
			$_sql =<<<EOT
SELECT
	n.id,
	n.title,
	DATE_FORMAT(n.target_dt,'%Y/%m/%d') AS target_dt,
	DATE_FORMAT(n.created_at,'%Y/%m/%d') AS created_at,
	n.content,
	n.course_id,
	n.calendar_use,
	n.calendar_color,
	(CASE WHEN {$this->config->item('NOTICE_NEW_INDICATION_PERIOD')} = 0 THEN NULL WHEN datediff(n.created_at, CURDATE()) > -{$this->config->item('NOTICE_NEW_INDICATION_PERIOD')} THEN 1 ELSE NULL END) AS is_new
FROM
	notice AS n
ORDER BY
	n.id DESC
EOT;
		}

		$_values = array($authority[0]->id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			// 成功処理
			return $_query->result('array');
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * お知らせ情報取得（お知らせ画面用）
	 *
	 * @param string $id ID
	 * @return string お知らせ情報
	 */
	public function  getNotice($id=NULL) {
		$_icon_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_GENRE')."&id=";

		$sql =<<< EOF
			SELECT
				n.`title`,
				DATE_FORMAT(n.`created_at`,'%Y/%m/%d') AS created_at,
				n.`content`,
				c.calendar_color,
				(CASE WHEN g.icon IS NOT NULL THEN concat('{$_icon_save_url}', g.id) ELSE NULL END) AS icon,
				c.`course_name`
			FROM
				`notice` AS n
			LEFT JOIN
				`course` AS c
			ON
				c.`id` = n.`course_id`
			LEFT JOIN
				`genre` AS g
			ON
				g.`id` = c.`genre`
			WHERE
				n.`id` = ?
EOF;
		$_values = array($id);
		if ($query = $this->db->query($sql, $_values)) {
			// 成功処理
			return $query->result('array');
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * 不要なWYSIWYG画像削除バッチ用お知らせ情報取得
	 *
	 * @return object お知らせ情報
	 */
	public function getNoticeListForBatchDeleteWYSIWYG() {
		$_sql =<<<EOT
SELECT id, content FROM notice ORDER BY id
EOT;
		$_query = $this->db->query($_sql);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		}
		return null;
	}

	/**
	 * お知らせ情報登録
	 *
	 * @param string $targetdt 日付
	 * @param string $title タイトル
	 * @param string $content 内容
	 * @param int $course_id 講座情報テーブルID
	 * @param int $calendar_use カレンダー使用フラグ
	 * @param string $color カレンダー色
	 */
	public function insertNotice($targetdt, $title, $content, $course_id, $calendar_use, $color) {
		$sql =<<< EOF
			INSERT INTO
				`notice`
			(
				`title`,
				`target_dt`,
				`content`,
				`course_id`,
				`calendar_use`,
				`calendar_color`,
				`created_at`
			)
			VALUES
			(
				?,
				?,
				?,
				?,
				?,
				?,
				CURRENT_TIMESTAMP
			)
EOF;
		$_values = array($title, $targetdt, $content, $course_id, $calendar_use, $color);
		$this->db->query($sql, $_values);
	}

	/**
	 * お知らせ情報更新
	 *
	 * @param int $nid ID
	 * @param string $targetdt 日付
	 * @param string $title タイトル
	 * @param string $content 内容
	 * @param int $course_id 講座情報テーブルID
	 * @param int $calendar_use カレンダー使用フラグ
	 * @param string $color カレンダー色
	 */
	public function updateNotice($nid, $targetdt, $title, $content, $course_id, $calendar_use, $color) {
		$sql =<<< EOF
			UPDATE
				`notice`
			SET
				`title` = ?,
				`target_dt` = ?,
				`content` = ?,
				`course_id` = ?,
				`calendar_use` = ?,
				`calendar_color` = ?,
				`updated_at` = CURRENT_TIMESTAMP
			WHERE
				id = ?
EOF;
		$_values = array($title, $targetdt, $content, $course_id, $calendar_use, $color, $nid);
		$this->db->query($sql, $_values);
	}

	/**
	 * お知らせ情報削除
	 *
	 * @param int $nid ID
	 */
	public function deleteNotice($nid) {
		$sql =<<< EOF
			DELETE
			FROM
				`notice`
			WHERE
				id = ?
EOF;
		$_values = array($nid);
		$this->db->query($sql, $_values);
	}
}