<?php
/*
 * 課題情報モデル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

class Task_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * テーマ画面の課題数を取得して返す
	 *
	 * @param int $thid テーマテーブルID
	 * @return int テーマ画面の課題数
	 */
	public function getTaskCount($thid) {

		$_sql =<<< EOF
SELECT
	COUNT(ta.id) AS cnt
FROM
	theme AS th
INNER JOIN
	task AS ta
ON
	ta.theme_id = th.id
WHERE
	th.id = ?
EOF;

		$_values = array($thid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result[0]->cnt;
		} else {
			return 0;
		}
	}

	/**
	 * テーマ画面の課題情報取得
	 *
	 * @param int $id テーマテーブルID
	 * @return array テーマ画面の課題情報
	 */
	public function getThemeTaskList($id=NULL) {

		$authority = $this->session->userdata['authority'];
		$uid = $authority[0]->id;

		$sql =<<< EOF
			SELECT
				t.id AS id,
				t.task_name AS task_name,
				t.task_description AS description,
				t.task_type,
				t.choices,
				t.evaluation_points AS evaluation_points,
				t.quiz_answer AS quiz_answer,
				t.comment AS comment,
				tret.id AS tarid,
				tret.result_radio,
				tret.result_check,
				tret.result_text,
				tret.result_file,
				(CASE WHEN tf.draft_flag IS NULL OR tf.draft_flag = 1 OR tf.feedback_at IS NULL OR tf.feedback_at = '0000-00-00 00:00:00' THEN NULL ELSE tret.feedback_points END) AS feedback_points,
				(CASE WHEN tf.draft_flag IS NULL OR tf.draft_flag = 1 OR tf.feedback_at IS NULL OR tf.feedback_at = '0000-00-00 00:00:00' THEN NULL ELSE tret.feedback_message END) AS feedback_message,
				(CASE WHEN tret.result_at is null OR tret.result_at = '0000-00-00 00:00:00' THEN null ELSE DATE_FORMAT(tret.result_at, '%Y/%m/%d') END) AS result_at,
				DATE_FORMAT(tret.created_at, '%Y/%m/%d') AS created_at
			FROM
				`task` AS t
			LEFT JOIN
				(
				SELECT
					tr.*
				FROM
					`task_result` AS tr
				INNER JOIN
					`user` AS u
				ON (
					tr.uid = u.id
				AND
					u.id = ?
				)) AS tret

			ON (
				t.id = tret.task_id
			AND
				t.course_id = tret.course_id
			AND
				t.theme_id = tret.theme_id
			)
			LEFT JOIN
				task_feedback AS tf
			ON
				tf.course_id = t.course_id
			AND
				tf.theme_id = t.theme_id
			AND
				tf.uid = ?
			WHERE
				t.theme_id = ?
			ORDER BY t.task_name, t.id
EOF;

		$_values = array($uid, $uid, $id);
		if ($query = $this->db->query($sql, $_values)) {
			// 成功処理
			return $query->result('array');
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * 指定講座、テーマの課題情報リスト取得
	 *
	 * @param int $cid 講座ID
	 * @param int $thid テーマID
	 * @return object 課題情報リスト取得
	 */
	public function getTaskList($cid, $thid)
	{
		$_sql =<<<EOT
SELECT
    id,
    course_id,
    theme_id,
    task_name,
    task_description,
    task_type,
    choices,
    evaluation_points,
    quiz_answer,
    comment
FROM
    task
WHERE
    course_id = ?
AND
    theme_id = ?
ORDER BY
    task_name, id
EOT;
		$_values = array($cid, $thid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * 指定講座、テーマの解答データ付課題情報リスト取得
	 *
	 * @param int $cid 講座ID
	 * @param int $thid テーマID
	 * @return object 課題情報リスト取得
	 */
	public function getTaskListWhithResult($cid, $thid, $uid)
	{
		$_sql =<<<EOT
SELECT
    t.id,
    t.course_id,
    t.theme_id,
    t.task_name,
    t.task_description,
    t.task_type,
    t.choices,
    t.evaluation_points,
    t.quiz_answer,
    t.comment,
    tr.id AS tarid,
    tr.result_radio,
    tr.result_check,
    tr.result_text,
    tr.result_file,
    (CASE WHEN tr.result_at is null OR tr.result_at = '0000-00-00 00:00:00' THEN null ELSE DATE_FORMAT(tr.result_at, '%Y/%m/%d') END) AS result_at
FROM
    task AS t
LEFT JOIN
    task_result AS tr
ON
    tr.task_id = t.id
AND
    tr.uid = ?
WHERE
    t.course_id = ?
AND
    t.theme_id = ?
ORDER BY
    task_name, id
EOT;
		$_values = array($uid, $cid, $thid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * 指定課題IDの課題情報取得
	 *
	 * @param int $id 課題テーブルID
	 * @return object 課題情報
	 */
	public function getTask($id)
	{
		$_sql =<<<EOT
SELECT
    id,
    course_id,
    theme_id,
    task_name,
    task_description,
    task_type,
    choices,
    evaluation_points,
    quiz_answer,
    comment
FROM
    task
WHERE
    id = ?
EOT;
		$_values = array($id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * 指定課題IDの解答データ付課題情報取得
	 *
	 * @param int $id 課題テーブルID
	 * @return object 課題情報
	 */
	public function getTaskWithResult($id)
	{
		$_authority = $this->session->userdata['authority'];
		$_uid = $_authority[0]->id;

		$_sql =<<<EOT
SELECT
    t.id,
    t.course_id,
    t.theme_id,
    t.task_name,
    t.task_description,
    t.task_type,
    t.choices,
    t.evaluation_points,
    t.quiz_answer,
    t.comment,
    tr.id AS tarid,
    tr.result_radio,
    tr.result_check,
    tr.result_text,
    tr.result_file,
    (CASE WHEN tr.result_at is null OR tr.result_at = '0000-00-00 00:00:00' THEN null ELSE DATE_FORMAT(tr.result_at, '%Y/%m/%d') END) AS result_at
FROM
    task AS t
LEFT JOIN
    task_result AS tr
ON
    tr.task_id = t.id
AND
    tr.uid = ?
WHERE
    t.id = ?
EOT;
		$_values = array($_uid, $id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * 評価点合計取得
	 *
	 * @param int $cid 講座ID
	 * @param int $thid テーマID
	 * @return int 評価点合計
	 */
	public function getTotalEvaluationPoints($cid, $thid)
	{
		$_sql =<<<EOT
SELECT
	SUM(evaluation_points) AS total_point
FROM
	task
WHERE
	course_id = ?
AND
	theme_id = ?
EOT;
		$_values = array($cid, $thid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object')[0]->total_point;
			if (is_null($_result)) {
				$_result = 0;
			}
			return $_result;
		} else {
			return 0;
		}
	}

	/**
	 * クイズ出題数取得（単一選択のみ）
	 *
	 * @param int $cid 講座ID
	 * @param int $thid テーマID
	 * @return int クイズ出題数
	 */
	public function getCountOfExamination($cid, $thid)
	{
		$_sql =<<<EOT
SELECT
	COUNT(ta.id) AS cnt
FROM
	task AS ta
WHERE
	ta.course_id = ?
AND
	ta.theme_id = ?
AND
	ta.task_type = {$this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')}
EOT;
		$_values = array($cid, $thid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object')[0]->cnt;
			if (is_null($_result)) {
				$_result = 0;
			}
			return $_result;
		} else {
			return 0;
		}
	}

	/**
	 * 指定講座の不要なWYSIWYG画像削除バッチ用課題情報取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param int $thid テーマテーブルID
	 * @return object テーマ情報
	 */
	public function getTaskListForBatchDeleteWYSIWYG($cid, $thid) {
		$_sql =<<<EOT
SELECT
	id,
	task_description
FROM
	task
WHERE
	course_id = ?
AND
	theme_id = ?
ORDER BY
	id
EOT;
		$_values = array($cid, $thid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		}
		return null;
	}

	/**
	 * 新規登録
	 *
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function regist() {
		$_sql =<<<EOT
INSERT INTO task
    (course_id, theme_id, task_name, task_description, task_type, choices, evaluation_points, quiz_answer, comment)
VALUES
    (?, ?, ?, ?, ?, ?, ?, ?, ?)
EOT;
		$_quiz_answer = null;
		if ($this->input->post("taskedit_type", true) == $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')) {
			$_quiz_answer = $this->input->post('taskedit_quiz_answer', true);
		}
		$_values = array(
				$this->input->get('cid', true),
				$this->input->get('thid', true),
				$this->input->post('taskedit_name', true),
				// WYSIWYG対応のためCodeigniterではXSS対策は実施せずに、専用関数で除去する。
				$this->commonlib->removeXssForWYSIWYG($this->input->post('taskedit_description')),
				$this->input->post('taskedit_type', true),
				$this->input->post('taskedit_choices', true),
				$this->input->post('taskedit_evaluation_points', true),
				$_quiz_answer,
				$this->input->post('taskedit_comment', true)
		);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 更新登録
	 *
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function update() {
		$_sql =<<<EOT
UPDATE task
    set task_name = ?,
    task_description = ?,
    task_type = ?,
    choices = ?,
    evaluation_points = ?,
    quiz_answer = ?,
    comment = ?,
    updated_at = NOW()
WHERE id = ?
EOT;
		$_quiz_answer = null;
		if ($this->input->post("taskedit_type", true) == $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')) {
			$_quiz_answer = $this->input->post('taskedit_quiz_answer', true);
		}
		$_values = array(
				$this->input->post('taskedit_name', true),
				// WYSIWYG対応のためCodeigniterではXSS対策は実施せずに、専用関数で除去する。
				$this->commonlib->removeXssForWYSIWYG($this->input->post('taskedit_description')),
				$this->input->post('taskedit_type', true),
				$this->input->post('taskedit_choices', true),
				$this->input->post('taskedit_evaluation_points', true),
				$_quiz_answer,
				$this->input->post('taskedit_comment', true),
				$this->input->get('taid', true)
		);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定テーマの課題コピー
	 *
	 * 指定されたテーマに紐付く課題をコピーする。
	 *
	 * @param int $src_cid コピー元講座情報テーブルID
	 * @param int $dest_cid コピー先講座情報テーブルID
	 * @param int $src_thid コピー元テーマテーブルID
	 * @param int $dest_thid コピー先テーマテーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function copyOfTheme($src_cid, $dest_cid, $src_thid, $dest_thid) {
		$_search_str = 'displayfile?tbl='.$this->config->item('TABLE_KEY_TASK').'&amp;cid='.$src_cid.'&amp;thid='.$src_thid.'&amp;fl=';
		$_replace_str = 'displayfile?tbl='.$this->config->item('TABLE_KEY_TASK').'&amp;cid='.$dest_cid.'&amp;thid='.$dest_thid.'&amp;fl=';
		$_sql =<<<EOT
INSERT INTO task
(
	course_id,
	theme_id,
	task_name,
	task_description,
	task_type,
	choices,
	evaluation_points,
	quiz_answer,
	comment
)
SELECT
	? AS course_id,
	? AS theme_id,
	task_name,
	REPLACE(task_description, ?, ?) AS task_description,
	task_type,
	choices,
	evaluation_points,
	quiz_answer,
	comment
FROM
	task
WHERE
	course_id = ?
AND
	theme_id = ?
ORDER BY id
EOT;
		$_values = array($dest_cid, $dest_thid, $_search_str, $_replace_str, $src_cid, $src_thid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 講座に紐付く課題情報削除
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByCourseId($cid)
	{
		$_sql = "DELETE FROM task WHERE course_id = ?";
		$_values = array($cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * テーマに紐付く課題情報削除
	 *
	 * @param int $thid テーマ情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByThemeId($thid)
	{
		$_sql = "DELETE FROM task WHERE theme_id = ?";
		$_values = array($thid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 課題情報削除
	 *
	 * @param int $taid 課題情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByTaskId($taid)
	{
		$_sql = "DELETE FROM task WHERE id = ?";
		$_values = array($taid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

}