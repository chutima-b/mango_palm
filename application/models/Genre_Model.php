<?php
/*
 * ジャンルモデル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

class Genre_Model extends CI_Model {
	/** ジャンルテーブルID. */
	public $id = 0;
	/** アイコンデフォルト画像URL. */
	private $icon_default_image;
	/** アイコン画像URL. */
	private $icon_save_url;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->icon_default_image = $this->commonlib->baseUrl().$this->config->item('GENREMANAGER_DEFAULT_IMAGE_URL');
		$this->icon_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_GENRE')."&id=";
	}

	/**
	 * システム管理（ジャンル）用全ジャンル数取得
	 *
	 * @return int 全ジャンル数
	 */
	public function getGenreCount() {
		$_sql = "SELECT * FROM `genre`";
		if ($query = $this->db->query($_sql, null)) {
			// 成功処理
			return $query->num_rows();
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * システム管理（ジャンル）用全ジャンル情報リスト取得
	 *
	 * @param int $page 現在表示しているページ
	 * @param int $limit 取得件数
	 * @return array 全ジャンル情報リスト
	 */
	public function getAllGenreInfoList($page, $limit) {
		$_offset = $limit * ($page - 1);

		$sql =<<< EOF
			SELECT
				(CASE WHEN g.icon IS NULL THEN '{$this->icon_default_image}' ELSE concat('{$this->icon_save_url}', g.id) END) AS icon,
				g.`id`,
				g.`id`,
				g.`genreid`,
				g.`genrename`,
				(CASE WHEN c.gid IS NULL THEN 0 ELSE c.cnt END) AS cnt
			FROM
				`genre` AS g
			LEFT JOIN (
				SELECT
					`genre` AS gid,
					COUNT(*) AS cnt
				FROM
					`course`
				GROUP BY
					`genre`
				) AS c
			ON (
				g.`id` = c.gid
			)
			ORDER BY
				g.`id`
			LIMIT {$limit} OFFSET {$_offset}
EOF;
		if ($query = $this->db->query($sql, null)) {
			return $query->result('array');
		}
		return null;
	}

	/**
	 * ジャンル一覧取得
	 *
	 * @return object ジャンル一覧
	 */
	public function get_genre_list()
	{
		$_sql =<<<EOT
SELECT
	id,
	(CASE WHEN icon IS NULL THEN '{$this->icon_default_image}' ELSE concat('{$this->icon_save_url}', id) END) AS icon,
	genreid,
	genrename
FROM
	genre
EOT;
		$_query = $this->db->query($_sql);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * ジャンル登録処理
	 * @param string $genreid ジャンルID
	 * @param string $genrename ジャンル名
	 * @param string $icon アイコン
	 * @return int 処理結果 0:OK, 1:ID重複, 2:名称重複, 3:両方重複, 9:NG
	 */
	public function regist($genreid, $genrename, $icon) {

		$check = $this->checkGenre(null, $genreid, $genrename);
		if (0 < $check) {
			// 重複あり
			return $check;
		}

		$sql =<<< EOF
			INSERT INTO
				`genre`
			(
				`genreid`,
				`genrename`,
				`icon`,
				`created_at`
			)
			VALUES
			(
				?,
				?,
				?,
				CURRENT_TIMESTAMP
			)
EOF;
		$_values = array($genreid, $genrename, $icon);
		if ($this->db->query($sql, $_values)) {
			// 成功処理
			$this->id = $this->db->insert_id();
			return 0;
		} else {
			// 失敗処理
			return 9;
		}
	}

	/**
	 * アイコンファイル更新
	 *
	 * @param string $icon_file アイコン画像ファイルパス
	 * @param int $gid ユーザーテーブルID
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function updateIconFile($icon_file, $gid)
	{
		$_sql =<<<EOT
UPDATE genre SET icon = ?
WHERE id = ?
EOT;
		$_values = array($icon_file, $gid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ジャンル更新処理
	 * @param int $gid ジャンルテーブルID
	 * @param string $genreid ジャンルID
	 * @param string $genrename ジャンル名
	 * @param string $icon アイコン
	 * @param boolean $delete_icon アイコン削除フラグ
	 * @return int 処理結果 0:OK, 1:ID重複, 2:名称重複, 3:両方重複, 9:NG
	 */
	public function update($gid, $genreid, $genrename, $icon, $delete_icon=false) {

		$check = $this->checkGenre($gid, $genreid, $genrename);
		if (0 < $check) {
			// 重複あり
			return $check;
		}

		$sql =<<< EOF
			UPDATE
				`genre`
			SET
				`genreid` = ?,
				`genrename` = ?,
				`updated_at` = CURRENT_TIMESTAMP
EOF;
		$_values = array($genreid, $genrename);
		if ($icon || $delete_icon) {
			if ($delete_icon) {
				$icon = null;
			}
			// アイコンファイルの指定がある場合
			// またはアイコンを削除する場合は更新
			$sql .= ", `icon` = ?";
			$_values[] = $icon;
		}
		$sql .= " WHERE `id` = ?";
		$_values[] = $gid;

		if ($this->db->query($sql, $_values)) {
			// 成功処理
			return 0;
		} else {
			// 失敗処理
			return 9;
		}
	}

	// 重複チェック処理
	private function checkGenre($gid, $genreid, $genrename) {

		$ret = 0;

		$sql =<<< EOF
			SELECT
				*
			FROM
				`genre`
			WHERE
				`genreid` = ?
EOF;
		$_values = array($genreid);
		if ($gid != null) {
			$sql .= " AND `id` <> ?";
			array_push($_values, $gid);
		}
		$query = $this->db->query($sql, $_values);
		if (0 < $query->num_rows()) {
			// 重複あり
			$ret += 1;
		}

		$sql =<<< EOF
			SELECT
				*
			FROM
				`genre`
			WHERE
				`genrename` = ?
EOF;
		$_values = array($genrename);
		if ($gid != null) {
			$sql .= " AND `id` <> ?";
			array_push($_values, $gid);
		}
		$query = $this->db->query($sql, $_values);
		if (0 < $query->num_rows()) {
			// 重複あり
			$ret += 2;
		}
		return $ret;
	}

	/**
	 * ジャンル削除処理
	 *
	 * @param int $gid ジャンルテーブルID
	 * @return boolean 処理結果
	 */
	public function delete($gid) {

		$sql =<<< EOF
			DELETE FROM
				`genre`
			WHERE
				`id` = ?
EOF;
		$_values = array($gid);
		if ($this->db->query($sql, $_values)) {
			// 成功処理
			return true;
		} else {
			// 失敗処理
			return false;
		}
	}

	/**
	 * 指定IDのアイコン取得
	 *
	 * @param int $id ジャンルテーブルID
	 * @return string アイコン
	 */
	public function getIconFile($id)
	{
		$_sql =<<<EOT
SELECT
	icon
FROM
	genre
WHERE
	id = ?
EOT;
		$_values = array($id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result[0]->icon;
		}
		return null;
	}

}