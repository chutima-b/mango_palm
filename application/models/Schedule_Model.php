<?php
/*
 * スケジュールモデル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

class Schedule_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 全ユーザー向けのお知らせ
	 * および
	 * 指定ユーザーが登録されている講座のお知らせ（受講者、講師、運用管理者権限）
	 * および
	 * 指定ユーザーが登録されている講座のテーマ情報（受講者、講師、運用管理者権限）
	 * のスケジュールをJSON形式で取得
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return string JSON形式のスケジュール
	 */
	public function getScheduleByJSON($uid)
	{
		$output_arrays = array();

		// 色指定が取得できなかった場合のデフォルトカラー
		// 定義ファイルの先頭の値をデフォルトカラーにしておく
		$simplecolorpicker_optiontag_colors = $this->config->item('SIMPLECOLORPICKER_OPTIONTAG_COLORS');
		reset($simplecolorpicker_optiontag_colors);
		$colorpicker_default_color = key($simplecolorpicker_optiontag_colors);

		// お知らせ取得
		$_sql =<<<EOT
SELECT
	n.id,
	n.title,
	DATE_FORMAT(n.target_dt, '%Y-%m-%d') AS target_dt,
	n.calendar_color
FROM
	notice AS n
LEFT JOIN
	students AS s
ON
	s.course_id = n.course_id
WHERE
	n.calendar_use = 1
AND
	n.target_dt IS NOT NULL
AND
	n.target_dt <> '0000-00-00 00:00:00'
AND
	(
		s.uid = ?
	OR
		n.course_id = 0
	OR
		n.course_id IS NULL
	)
ORDER BY
	n.id
EOT;

		// システム管理者の場合は講座に関係なくお知らせを取得する
		$authority = $this->session->userdata['authority'];
		if ($this->config->item('AUTH_SYS_MANAGER') == $authority[0]->authority) {
			$_sql =<<<EOT
SELECT
	n.id,
	n.title,
	DATE_FORMAT(n.target_dt, '%Y-%m-%d') AS target_dt,
	n.calendar_color
FROM
	notice AS n
WHERE
	n.calendar_use = 1
AND
	n.target_dt IS NOT NULL
AND
	n.target_dt <> '0000-00-00 00:00:00'
ORDER BY
	n.id
EOT;
		}

		$_values = array($uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_notice_list = $_query->result('object');
			$_baseUrl = $this->commonlib->baseUrl().'notice?nid=';
			foreach ($_notice_list as $_notice) {
				$_url = $_baseUrl.$_notice->id.'&pre=students';
				$_color = $_notice->calendar_color;
				if (!$_color) {
					$_color = $colorpicker_default_color;
				}
				$output_arrays[] = array(
					'title' => $_notice->title,
					'start' => $_notice->target_dt,
					'url' => $_url,
					'color' => $_color
				);
			}
		}

		// テーマ取得
		if ($this->config->item('AUTH_GUEST') == $authority[0]->authority
		 || $this->config->item('AUTH_STUDENT') == $authority[0]->authority) {
			// ゲストまたは受講者の場合は自分が所属しているテーマを表示する
			$_sql =<<<EOT
SELECT
	th.course_id,
	c.course_name,
	th.id,
	th.name,
	DATE_FORMAT(th.theme_date, '%Y-%m-%d') AS theme_date,
	/* FullCalendarのendは1日短く表示されるので加算しておく */
	DATE_FORMAT(DATE_ADD(th.time_limit, INTERVAL 1 DAY), '%Y-%m-%d') AS time_limit,
	c.calendar_color
FROM
	theme AS th
INNER JOIN
	course AS c
ON
	c.id = th.course_id
AND
	c.schedule_use = 1
INNER JOIN
	students AS s
ON
	s.course_id = th.course_id
AND
	s.authority IN ({$this->config->item('AUTH_GUEST')}, {$this->config->item('AUTH_STUDENT')}, {$this->config->item('AUTH_TEACHER')}, {$this->config->item('AUTH_OP_MANAGER')})
INNER JOIN
	user AS u
ON
	u.id = s.uid
AND
	u.id = ?
WHERE
	th.open = {$this->config->item('THEME_OPEN')}
AND
	th.theme_date IS NOT NULL
AND
	th.time_limit IS NOT NULL
ORDER BY
	th.course_id, th.id
EOT;
			$_values = array($uid);
			$_query = $this->db->query($_sql, $_values);
			if ($_query->num_rows() > 0) {
				$_theme_list = $_query->result('object');
				$_baseUrl = $this->commonlib->baseUrl().'theme?cid=';
				foreach ($_theme_list as $_theme) {
					$_url = $_baseUrl.$_theme->course_id.'&thid='.$_theme->id.'&pre=home';
					$_color = $_theme->calendar_color;
					if (!$_color) {
						$_color = $colorpicker_default_color;
					}
					$output_arrays[] = array(
						'title' => $_theme->course_name,
						'start' => $_theme->theme_date,
						'end' => $_theme->time_limit,
						'url' => $_url,
						'color' => $_color
					);
				}
			}
		} else if ($this->config->item('AUTH_TEACHER') == $authority[0]->authority
		 || $this->config->item('AUTH_OP_MANAGER') == $authority[0]->authority) {
			// 講師、運用管理者の場合は、自分が所属している公開終了以外のテーマを表示する
			$_sql =<<<EOT
SELECT
	th.course_id,
	c.course_name,
	th.id,
	th.name,
	DATE_FORMAT(th.theme_date, '%Y-%m-%d') AS theme_date,
	/* FullCalendarのendは1日短く表示されるので加算しておく */
	DATE_FORMAT(DATE_ADD(th.time_limit, INTERVAL 1 DAY), '%Y-%m-%d') AS time_limit,
	c.calendar_color
FROM
	theme AS th
INNER JOIN
	course AS c
ON
	c.id = th.course_id
AND
	c.schedule_use = 1
INNER JOIN
	students AS s
ON
	s.course_id = th.course_id
AND
	s.authority IN ({$this->config->item('AUTH_GUEST')}, {$this->config->item('AUTH_STUDENT')}, {$this->config->item('AUTH_TEACHER')}, {$this->config->item('AUTH_OP_MANAGER')})
INNER JOIN
	user AS u
ON
	u.id = s.uid
AND
	u.id = ?
WHERE
	th.open <> {$this->config->item('THEME_PUBLIC_END')}
AND
	th.theme_date IS NOT NULL
AND
	th.time_limit IS NOT NULL
ORDER BY
	th.course_id, th.id
EOT;
			$_values = array($uid);
			$_query = $this->db->query($_sql, $_values);
			if ($_query->num_rows() > 0) {
				$_theme_list = $_query->result('object');
				$_baseUrl = $this->commonlib->baseUrl().'theme?cid=';
				foreach ($_theme_list as $_theme) {
					$_url = $_baseUrl.$_theme->course_id.'&thid='.$_theme->id.'&pre=home';
					$_color = $_theme->calendar_color;
					if (!$_color) {
						$_color = $colorpicker_default_color;
					}
					$output_arrays[] = array(
						'title' => $_theme->course_name,
						'start' => $_theme->theme_date,
						'end' => $_theme->time_limit,
						'url' => $_url,
						'color' => $_color
					);
				}
			}
		} else if ($this->config->item('AUTH_SYS_MANAGER') == $authority[0]->authority) {
			// システム管理者の場合は公開終了以外のテーマを表示する
			$_sql =<<<EOT
SELECT
	DISTINCT
	th.course_id,
	c.course_name,
	th.id,
	th.name,
	DATE_FORMAT(th.theme_date, '%Y-%m-%d') AS theme_date,
	/* FullCalendarのendは1日短く表示されるので加算しておく */
	DATE_FORMAT(DATE_ADD(th.time_limit, INTERVAL 1 DAY), '%Y-%m-%d') AS time_limit,
	c.calendar_color
FROM
	theme AS th
INNER JOIN
	course AS c
ON
	c.id = th.course_id
AND
	c.schedule_use = 1
INNER JOIN
	students AS s
ON
	s.course_id = th.course_id
AND
	s.authority IN ({$this->config->item('AUTH_GUEST')}, {$this->config->item('AUTH_STUDENT')}, {$this->config->item('AUTH_TEACHER')}, {$this->config->item('AUTH_OP_MANAGER')})
WHERE
	th.open <> {$this->config->item('THEME_PUBLIC_END')}
AND
	th.theme_date IS NOT NULL
AND
	th.time_limit IS NOT NULL
ORDER BY
	th.course_id, th.id
EOT;
			$_query = $this->db->query($_sql);
			if ($_query->num_rows() > 0) {
				$_theme_list = $_query->result('object');
				$_baseUrl = $this->commonlib->baseUrl().'theme?cid=';
				foreach ($_theme_list as $_theme) {
					$_url = $_baseUrl.$_theme->course_id.'&thid='.$_theme->id.'&pre=home';
					$_color = $_theme->calendar_color;
					if (!$_color) {
						$_color = $colorpicker_default_color;
					}
					$output_arrays[] = array(
						'title' => $_theme->course_name,
						'start' => $_theme->theme_date,
						'end' => $_theme->time_limit,
						'url' => $_url,
						'color' => $_color
					);
				}
			}
		}

		if (count($output_arrays) > 0) {
			return json_encode($output_arrays);
		} else {
			return '';
		}
	}

}