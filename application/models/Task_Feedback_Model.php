<?php
/*
 * 課題フィードバックモデル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

class Task_Feedback_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * テーマに課題フィードバックレコードが存在するか判定
	 *
	 * @param int $cid 講座ID
	 * @param int $thid テーマID
	 * @return boolean 判定結果（true:存在する / false:存在しない）
	 */
	public function hasDataOfTheme($cid, $thid)
	{
		$_sql =<<<EOF
SELECT
	COUNT(id) AS cnt
FROM
	task_feedback
WHERE
	course_id = ?
AND
	theme_id = ?
EOF;
		$_values = array($cid, $thid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			if ($_result[0]->cnt > 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 課題フィードバックデータ取得
	 *
	 * @param int $cid 講座ID
	 * @param int $thid テーマID
	 * @param int $uid ユーザーテーブルID
	 * @return object 課題フィードバックデータ
	 */
	public function getData($cid, $thid, $uid)
	{
		$_sql =<<<EOF
SELECT
	(CASE WHEN submit_at IS NULL OR submit_at = '0000-00-00 00:00:00' THEN NULL ELSE DATE_FORMAT(submit_at, '%Y/%m/%d') END) AS submit_at,
	draft_flag,
	(CASE WHEN feedback_at IS NULL OR feedback_at = '0000-00-00 00:00:00' THEN NULL ELSE DATE_FORMAT(feedback_at, '%Y/%m/%d') END) AS feedback_at,
	(CASE WHEN confirmed_at IS NULL OR confirmed_at = '0000-00-00 00:00:00' THEN NULL ELSE DATE_FORMAT(confirmed_at, '%Y/%m/%d') END) AS confirmed_at
FROM
	task_feedback
WHERE
	course_id = ?
AND
	theme_id = ?
AND
	uid = ?
EOF;
		$_values = array($cid, $thid, $uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result[0];
		}
		return null;
	}

	/**
	 * 新規登録
	 *
	 * @param int $cid 講座ID
	 * @param int $thid テーマID
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function regist($cid, $thid, $uid) {
		$_sql =<<<EOT
INSERT INTO task_feedback
    (course_id, theme_id, uid, submit_at)
VALUES
    (?, ?, ?, NOW())
EOT;
		$_values = array($cid, $thid, $uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 更新登録
	 *
	 * @param int $cid 講座ID
	 * @param int $thid テーマID
	 * @param int $uid ユーザーテーブルID
	 * @param int $draft_flag 下書きフラグ
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function update($cid, $thid, $uid, $draft_flag) {
		$_feedback = '';
		if ($draft_flag == $this->config->item('DRAFT_FLAG_OFF')) {
			$_feedback = 'feedback_at = NOW(),';
		}
		$_sql =<<<EOT
UPDATE task_feedback
    set draft_flag = ?,
    {$_feedback}
    updated_at = NOW()
WHERE
	course_id = ?
AND
	theme_id = ?
AND
	uid = ?
EOT;
		$_values = array($draft_flag, $cid, $thid, $uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * クイズ形式の課題提出時用更新登録
	 *
	 * @param int $cid 講座ID
	 * @param int $thid テーマID
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function updateForQuizSubmit($cid, $thid, $uid) {
		$_sql =<<<EOT
UPDATE task_feedback
    set draft_flag = {$this->config->item('DRAFT_FLAG_OFF')},
    feedback_at = NOW(),
    confirmed_at = NOW(),
    updated_at = NOW()
WHERE
	course_id = ?
AND
	theme_id = ?
AND
	uid = ?
EOT;
		$_values = array($cid, $thid, $uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 受講者確認日時登録
	 *
	 * @param int $cid 講座ID
	 * @param int $thid テーマID
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function registConfirmdAt($cid, $thid, $uid) {
		$_sql =<<<EOT
UPDATE task_feedback
    set confirmed_at = NOW(),
    updated_at = NOW()
WHERE
	course_id = ?
AND
	theme_id = ?
AND
	uid = ?
EOT;
		$_values = array($cid, $thid, $uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * テーマに紐付く課題フィードバック情報削除
	 *
	 * @param int $thid テーマ情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByThemeId($thid)
	{
		$_sql = "DELETE FROM task_feedback WHERE theme_id = ?";
		$_values = array($thid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定ユーザーデータ削除
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByUid($uid)
	{
		$_sql = "DELETE FROM task_feedback WHERE uid = ?";
		$_values = array($uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

}