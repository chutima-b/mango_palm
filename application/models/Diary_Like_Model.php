<?php
/*
 * 日記Likeモデル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Diary_Like_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 指定日記の「Like!」数取得
	 *
	 * @param int $did 日記テーブルID
	 * @return NULL|int 指定日記の「Like!」数
	 */
	public function getCount($did)
	{
		$_sql = "SELECT COUNT(id) AS cnt FROM diary_like WHERE diary_id = ?";
		$_values = array($did);
		if ($query = $this->db->query($_sql, $_values)) {
			// 成功処理
			return $query->result('object')[0]->cnt;
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * 「Like!」追加
	 *
	 * @param int $did 日記テーブルID
	 * @param int $uid ユーザーテーブルID
	 * @return boolean 成功：true / 失敗：false
	 */
	public function add($did, $uid)
	{
		$_sql =<<<EOT
INSERT INTO
	diary_like
	(
		diary_id,
		uid
	)
VALUES
	(
		?,
		?
	)
EOT;
		$_values = array($did, $uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 「Like!」削除
	 *
	 * @param int $did 日記テーブルID
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function delete($did, $uid)
	{
		$_sql =<<<EOT
DELETE FROM
	diary_like
WHERE
	diary_id = ?
AND
	uid = ?
EOT;
		$_values = array($did, $uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定日記の「Like!」を全て削除
	 *
	 * @param int $did 日記テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteOfDiary($did)
	{
		$_sql =<<<EOT
DELETE FROM
	diary_like
WHERE
	diary_id = ?
EOT;
		$_values = array($did);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定講座の「Like!」を全て削除
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteOfCourse($cid)
	{
		$_sql =<<<EOT
DELETE
	dl
FROM
	diary_like AS dl
INNER JOIN
	diary AS d
ON
	d.id = dl.diary_id
WHERE
	d.course_id = ?
EOT;
		$_values = array($cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

}
