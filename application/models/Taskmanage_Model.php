<?php
/*
 * 課題管理モデル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Taskmanage_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 課題管理情報リスト取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param int $thid テーマID
	 * @return object 課題管理情報リスト
	 */
	public function getList($cid, $thid)
	{
		$_sql =<<<EOT
SELECT
	(CASE WHEN tf.submit_at IS NULL OR tf.submit_at = '0000-00-00 00:00:00' THEN '-' ELSE DATE_FORMAT(tf.submit_at, '%Y/%m/%d') END) AS submit_at,
	(CASE WHEN tf.feedback_at IS NULL OR tf.feedback_at = '0000-00-00 00:00:00' THEN '-' ELSE DATE_FORMAT(tf.feedback_at, '%Y/%m/%d') END) AS feedback_at
FROM
	students AS s
INNER JOIN
	user AS u
ON
	u.id = s.uid
LEFT JOIN
	task_feedback AS tf
ON
	tf.course_id = s.course_id
AND
	tf.theme_id = ?
AND
	tf.uid = u.id
WHERE
	s.course_id = ?
AND
	u.validityflg = 1
AND
	s.authority = {$this->config->item('AUTH_STUDENT')}
ORDER BY
	s.uid
EOT;
		$_values = array($thid, $cid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * 受講者課題画面用課題管理情報リスト取得
	 *
	 * @param int $page 現在表示しているページ
	 * @param int $limit 取得件数
	 * @param int $cid 講座情報テーブルID
	 * @param int $thid テーマID
	 * @return object 課題管理情報リスト
	 */
	public function getListOfUsers($page, $limit, $cid, $thid)
	{
		$_profile_default_image = $this->commonlib->baseUrl().$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
		$_profile_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=";
		$_offset = $limit * ($page - 1);

		$_sql =<<<EOT
SELECT
	s.uid,
	(CASE WHEN u.picture_file IS NULL THEN '{$_profile_default_image}' ELSE concat('{$_profile_save_url}', u.id) END) AS picture_file,
	u.userid,
	u.surname,
	u.firstname,
	u.nickname,
	(CASE WHEN tf.submit_at IS NULL OR tf.submit_at = '0000-00-00 00:00:00' THEN '-' ELSE DATE_FORMAT(tf.submit_at, '%Y/%m/%d') END) AS submit_at,
	(CASE WHEN tf.feedback_at IS NULL OR tf.feedback_at = '0000-00-00 00:00:00' THEN '-' ELSE DATE_FORMAT(tf.feedback_at, '%Y/%m/%d') END) AS feedback_at,
	@feedback_points := (SELECT SUM(tr.feedback_points) FROM task_result AS tr WHERE tr.course_id = s.course_id AND tr.theme_id = tf.theme_id AND tr.uid = u.id)  AS feedback_points,
	@task_points := (SELECT SUM(ta.evaluation_points) FROM task AS ta WHERE ta.course_id = s.course_id AND ta.theme_id = tf.theme_id) AS evaluation_points,
	(CASE WHEN tf.feedback_at IS NULL THEN '-'
			WHEN @feedback_points IS NULL THEN '-'
			WHEN @task_points IS NULL THEN '-' ELSE
		CONCAT(@feedback_points, '/', @task_points)
	END) AS points,
	(SELECT COUNT(ta.id) FROM task AS ta WHERE ta.course_id = s.course_id AND ta.theme_id = tf.theme_id AND ta.task_type = {$this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')}) AS quiz_cnt
FROM
	students AS s
INNER JOIN
	user AS u
ON
	u.id = s.uid
LEFT JOIN
	task_feedback AS tf
ON
	tf.course_id = s.course_id
AND
	tf.theme_id = ?
AND
	tf.uid = u.id
WHERE
	s.course_id = ?
AND
	u.validityflg = 1
AND
	s.authority = {$this->config->item('AUTH_STUDENT')}
ORDER BY
	s.uid
LIMIT {$limit} OFFSET {$_offset}
EOT;
		$_values = array($thid, $cid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * ダウンロード用課題管理情報リスト取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param int $thid テーマID
	 * @return object ダウンロード用課題管理情報リスト
	 */
	public function getListForDownload($cid, $thid)
	{
		// 受講者、ジャンル、講座、テーマ情報取得
		$_sql =<<<EOT
SELECT
	c.id AS cid,
	th.id AS thid,
	u.id AS uid,
	th.quiz,
	u.userid,
	u.nickname,
	u.surname,
	u.firstname,
	g.genreid,
	g.genrename,
	c.course_code,
	c.course_name,
	th.name AS theme_name,
	DATE_FORMAT(th.theme_date, '%Y/%m/%d') AS theme_date,
	(CASE WHEN tf.submit_at IS NULL OR tf.submit_at = '0000-00-00 00:00:00' THEN NULL ELSE DATE_FORMAT(tf.submit_at, '%Y/%m/%d') END) AS submit_at,
	DATE_FORMAT(th.time_limit, '%Y/%m/%d') AS time_limit,
	DATE_FORMAT(tf.feedback_at, '%Y/%m/%d') AS feedback_at,
	/* 実際の評価点合計（クイズの場合の正解数はphp側でカウントする） */
	(SELECT
		SUM(tr.feedback_points)
	FROM
		task_result AS tr
	WHERE
		tr.course_id = s.course_id
	AND
		tr.theme_id = tf.theme_id
	AND
		tr.uid = u.id
	) AS feedback_points,
	/* 出題した評価点合計（クイズの場合はクイズ（単一選択）の出題数） */
	(CASE WHEN
		th.quiz = 1
	THEN
		(SELECT
			COUNT(ta.id)
		FROM
			task AS ta
		WHERE
			ta.course_id = c.id
		AND
			ta.theme_id = th.id
		AND
			ta.task_type = {$this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')})
	ELSE
		(SELECT
			SUM(ta.evaluation_points)
		FROM
			task AS ta
		WHERE
			ta.course_id = s.course_id
		AND
			ta.theme_id = tf.theme_id)
	END) AS evaluation_points
FROM
	students AS s
INNER JOIN
	user AS u
ON
	u.id = s.uid
INNER JOIN
	course AS c
ON
	c.id = s.course_id
INNER JOIN
	genre AS g
ON
	g.id = c.genre
INNER JOIN
	theme AS th
ON
	th.course_id = c.id
AND
	th.id = ?
LEFT JOIN
	task_feedback AS tf
ON
	tf.course_id = c.id
AND
	tf.theme_id = th.id
AND
	tf.uid = u.id
WHERE
	s.course_id = ?
AND
	u.validityflg = 1
AND
	s.authority = {$this->config->item('AUTH_STUDENT')}
ORDER BY
	s.uid
EOT;
		$_values = array($thid, $cid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 0) {
			return null;
		}
		$_user_list = $_query->result('object');

		foreach ($_user_list as $_user) {
			// 教材動画情報取得
			$_sql =<<<EOT
SELECT
	m.material_description,
	SEC_TO_TIME((CASE WHEN mp.play_time IS NULL THEN 0 ELSE mp.play_time END)) AS play_time,
	SEC_TO_TIME((CASE WHEN mp.total_time IS NULL THEN 0 ELSE mp.total_time END)) AS total_time
FROM
	materials AS m
LEFT JOIN
	materials_play AS mp
ON
	mp.material_id = m.id
AND
	mp.course_id = m.course_id
AND
	mp.theme_id = m.theme_id
AND
	mp.uid = ?
WHERE
	m.course_id = ?
AND
	m.theme_id = ?
AND
	m.material_type = {$this->config->item('MATERIAL_TYPE_MOVIE')}
ORDER BY
	m.id
EOT;
			$_values = array($_user->uid, $_user->cid, $_user->thid);
			$_query = $this->db->query($_sql, $_values);
			$_user->materialslist = array();
			if ($_query->num_rows() > 0) {
				$_user->materialslist = $_query->result('object');
			}

			// 課題情報取得
			$_sql =<<<EOT
SELECT
	t.task_type,
	t.choices,
	t.quiz_answer,
	t.task_name,
	t.task_description,
	(CASE WHEN tf.draft_flag = 1 THEN NULL ELSE tr.feedback_points END) AS feedback_points,
	(CASE WHEN t.task_type = {$this->config->item('TASK_TYPE_SINGLE_SELECTION')}
		OR t.task_type = {$this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')}
		OR t.task_type = {$this->config->item('TASK_TYPE_SURVEY_SINGLE_SELECTION')}
	THEN
		tr.result_radio
	WHEN t.task_type = {$this->config->item('TASK_TYPE_MULTIPLE_CHOICE')}
		OR t.task_type = {$this->config->item('TASK_TYPE_SURVEY_MULTIPLE_CHOICE')}
	THEN
		tr.result_check
	WHEN t.task_type = {$this->config->item('TASK_TYPE_TEXT')} THEN
		tr.result_text
	WHEN t.task_type = {$this->config->item('TASK_TYPE_FILE')} THEN
		tr.result_file
	ELSE
		NULL
	END
	) AS result_data,
	(CASE WHEN tf.draft_flag = 1 THEN NULL ELSE tr.feedback_message END) AS feedback_message
FROM
	task AS t
LEFT JOIN
	task_result AS tr
ON
	tr.task_id = t.id
AND
	tr.course_id = t.course_id
AND
	tr.theme_id = t.theme_id
AND
	tr.uid = ?
LEFT JOIN
	task_feedback AS tf
ON
	tf.course_id = t.course_id
AND
	tf.theme_id = t.theme_id
AND
	tf.uid = ?
WHERE
	t.course_id = ?
AND
	t.theme_id = ?
ORDER BY
	t.task_name, t.id
EOT;

			$_values = array($_user->uid, $_user->uid, $_user->cid, $_user->thid);

			if ($_user->submit_at == null) {
				// 未提出の場合、課題情報のみ取得
				$_sql =<<<EOT
SELECT
	t.task_type,
	t.choices,
	t.quiz_answer,
	t.task_name,
	t.task_description,
	NULL AS feedback_points,
	NULL AS result_data,
	NULL AS feedback_message
FROM
	task AS t
WHERE
	t.course_id = ?
AND
	t.theme_id = ?
ORDER BY
	t.task_name, t.id
EOT;
				$_values = array($_user->cid, $_user->thid);
			}

			$_query = $this->db->query($_sql, $_values);
			$_user->tasklist = array();
			if ($_query->num_rows() > 0) {
				$_user->tasklist = $_query->result('object');
			}
		}

		return $_user_list;
	}

}
