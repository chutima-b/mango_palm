<?php
/*
 * 教材再生時間保持モデル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

class Materials_Play_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 教材再生時間情報取得
	 *
	 * @param int $mid 教材テーブルID
	 * @return object 教材再生時間
	 */
	public function getInfoOfUser($mid)
	{
		$_authorityInfo = $this->session->userdata('authority');
		$_uid = $_authorityInfo[0]->id;

		$_sql =<<<EOT
SELECT
	id,
	course_id,
	theme_id,
	play_time,
	total_time,
	percentage
FROM
	materials_play
WHERE
	material_id = ?
AND
	uid = ?
EOT;
		$_values = array($mid, $_uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result[0];
		}
		return null;
	}

	/**
	 * 教材再生時間判定値がSUCCESS値に満たない教材動画情報取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param int $thid テーマテーブルID
	 * @return object|NULL 教材再生時間判定値がSUCCESS値に満たない教材動画情報
	 */
	public function getNotSuccessMaterials($cid, $thid)
	{
		$_authorityInfo = $this->session->userdata('authority');
		$_uid = $_authorityInfo[0]->id;

		$_sql =<<<EOT
SELECT
	m.id,
	m.material_description,
	mp.percentage
FROM
	materials AS m
LEFT JOIN
	materials_play AS mp
ON
	mp.material_id = m.id
AND
	mp.course_id = m.course_id
AND
	mp.theme_id = m.theme_id
AND
	mp.uid = ?
WHERE
	m.course_id = ?
AND
	m.theme_id = ?
AND
	m.material_type = {$this->config->item('MATERIAL_TYPE_MOVIE')}
AND
	m.play_end = 1
AND
	(
		mp.percentage IS NULL
	OR
		mp.percentage < {$this->config->item('MATERIAL_MOVIE_PLAYTIME_SUCCESS')}
	)
ORDER BY
	m.id
EOT;

		$_values = array($_uid, $cid, $thid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		}
		return null;
	}

	/**
	 * 教材再生時間登録
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param int $thid テーマテーブルID
	 * @param int $mid 教材情報テーブルID
	 * @param int $play_time 教材再生時間
	 * @param int $total_time 教材全編長時間
	 * @param int $percentage 教材再生時間割合
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function regist($cid, $thid, $mid, $play_time, $total_time, $percentage) {
		$_authorityInfo = $this->session->userdata('authority');
		$_uid = $_authorityInfo[0]->id;

		$sql =<<<EOF
INSERT INTO
	materials_play
(
	course_id,
	theme_id,
	material_id,
	uid,
	play_time,
	total_time,
	percentage
)
VALUES
(
	?,
	?,
	?,
	?,
	?,
	?,
	?
)
ON DUPLICATE KEY UPDATE
	play_time = ?,
	percentage = ?,
	updated_at = NOW()
EOF;
		$_values = array($cid, $thid, $mid, $_uid, $play_time, $total_time, $percentage, $play_time, $percentage);
		if ($this->db->query($sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 教材に紐付く教材再生時間保持情報削除
	 *
	 * @param int $mid 教材情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByMaterialId($mid) {
		$_sql ="DELETE FROM materials_play WHERE material_id = ?";
		$_values = array($mid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 講座に紐付く教材再生時間保持情報削除
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByCourseId($cid) {
		$_sql ="DELETE FROM materials_play WHERE course_id = ?";
		$_values = array($cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * テーマに紐付く教材再生時間保持情報削除
	 *
	 * @param int $thid テーマ情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByThemeId($thid) {
		$_sql ="DELETE FROM materials_play WHERE theme_id = ?";
		$_values = array($thid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定ユーザーデータ削除
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByUid($uid)
	{
		$_sql = "DELETE FROM materials_play WHERE uid = ?";
		$_values = array($uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

}