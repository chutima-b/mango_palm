<?php
/*
 * ログイン認証モデル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Authority_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * ログイン認証
	 *
	 * ※ゲストの場合はこのメソッドでのログインを許可しない
	 *
	 * @return NULL|object ユーザー情報オブジェクト（ユーザーテーブルID、ユーザーID、ニックネーム、最大権限）
	 */
	public function can_log_in()
	{
		// 講座に権限登録済みのユーザー認証（ゲスト以外）
		$_sql =<<<EOT
SELECT u.id, u.userid, u.nickname, (SELECT MAX(s2.authority) FROM students AS s2 WHERE s2.uid = u.id) AS authority
FROM user AS u
INNER JOIN students s ON
u.id = s.uid
WHERE
	u.userid=?
AND
	u.passwd=SHA2(?, 0)
AND
	u.validityflg = 1
AND
	NOT EXISTS (SELECT s3.authority FROM students AS s3 WHERE s3.uid = u.id AND s3.authority = {$this->config->item('AUTH_GUEST')})
LIMIT 1
EOT;
		$_values = array($this->input->post("login_userid", true), $this->input->post("login_password", true));
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result;
		}

		// 講座に登録が無い場合のユーザー認証（ゲスト以外）
		$_sql =<<<EOT
SELECT u.id, u.userid, u.nickname, {$this->config->item('AUTH_UNREGISTERED')} AS authority
FROM user AS u
WHERE u.userid=? AND u.passwd=SHA2(?, 0) AND u.validityflg = 1
AND
	NOT EXISTS (SELECT s.authority FROM students AS s WHERE s.uid = u.id AND s.authority = {$this->config->item('AUTH_GUEST')})
LIMIT 1
EOT;
		$_values = array($this->input->post("login_userid", true), $this->input->post("login_password", true));
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * ゲストログイン
	 *
	 * @return NULL|object ユーザー情報オブジェクト（ユーザーテーブルID、ユーザーID、ニックネーム、権限）
	 */
	public function guest_log_in()
	{
		$_sql =<<<EOT
SELECT u.id, u.userid, u.nickname, {$this->config->item('AUTH_GUEST')} AS authority
FROM user AS u
INNER JOIN students s ON
u.id = s.uid
WHERE s.authority = {$this->config->item('AUTH_GUEST')} AND u.validityflg = 1
ORDER BY u.id
LIMIT 1
EOT;
		$_query = $this->db->query($_sql);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * ユーザー情報取得
	 *
	 * @return NULL|object ユーザー情報オブジェクト（ユーザーテーブルID、ユーザーID、ニックネーム、最大権限）
	 */
	public function get_authority()
	{
		$_authority = $this->session->userdata('authority');

		// 講座に権限登録済みのユーザー認証（ゲスト以外）
		$_sql =<<<EOT
SELECT u.id, u.userid, u.nickname, (SELECT MAX(s2.authority) FROM students AS s2 WHERE s2.uid = u.id) AS authority
FROM user AS u
INNER JOIN students s ON
u.id = s.uid
WHERE
	u.id=?
AND
	u.validityflg = 1
LIMIT 1
EOT;
		$_values = array($_authority[0]->id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result;
		}
		return null;
	}

	/**
	 * オートログイン認証
	 *
	 * @param string $token
	 *
	 * @return NULL|object ユーザー情報オブジェクト（ユーザーテーブルID、ユーザーID、ニックネーム、最大権限）
	 */
	public function auto_log_in($token)
	{
		$_sql =<<< EOT
SELECT u.id, u.userid, u.nickname, (SELECT MAX(s2.authority) FROM students AS s2 WHERE s2.uid = u.id) AS authority
FROM user AS u
INNER JOIN students s ON
u.id = s.uid
WHERE u.token=? AND u.validityflg = 1
LIMIT 1
EOT;
		$_values = array($token);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * オートログイントークン削除
	 *
	 * @param string $token オートログイントークン
	 */
	public function delete_token($token)
	{
		$_sql = "UPDATE user set token=NULL WHERE token=?";
		$_values = array($token);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * オートログイントークン設定
	 *
	 * @param int $id ユーザーテーブルID
	 * @param string $token オートログイントークン
	 */
	public function update_token($id, $token)
	{
		$_sql = "UPDATE user set token=? WHERE id=?";
		$_values = array($token, $id);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 最終ログイン日時更新
	 *
	 * @param int $id ユーザーテーブルID
	 * @param int $authority 権限
	 */
	public function update_login($id, $authority)
	{
		if ($authority == $this->config->item('AUTH_GUEST')) {
			// ゲストの場合は最終ログイン日時を記録しない
			return true;
		}

		$_sql = "UPDATE user set login_at=NOW() WHERE id=?";
		$_values = array($id);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

}
