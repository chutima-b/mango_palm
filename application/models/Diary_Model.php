<?php
/*
 * 日記モデル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
class Diary_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 日記投稿一覧取得
	 *
	 * @param int $page 現在表示しているページ
	 * @param int $limit 取得件数
	 * @param int $course_id 講座テーブルID
	 * @return array 日記投稿一覧
	 */
	public function getDiaryList($page, $limit, $course_id) {

		$authority = $this->session->userdata['authority'];
		$uid = $authority[0]->id;
		$_offset = $limit * ($page - 1);

		$sql =<<< EOF
			SELECT
				d.`id`,
				d.`course_id`,
				d.`uid`,
				d.`post`,
				d.`image_file`,
				DATE_FORMAT(d.`created_at`, '%Y/%m/%d %H:%i') AS post_time,
				u.`userid`,
				u.`nickname`,
				u.`picture_file`,
				(CASE WHEN hd.`hide` is null THEN 0 ELSE 1 END) AS hide_flg,
				(CASE WHEN dl.`id` IS NULL THEN 0 ELSE 1 END) AS diary_like,
				(SELECT COUNT(dl2.`id`) FROM `diary_like` AS dl2 WHERE dl2.`diary_id` = d.`id`) AS diary_like_cnt
			FROM
				`diary` AS d
			INNER JOIN
				`user` AS u
			ON (
				d.`uid` = u.`id`
			AND
				u.`validityflg` = 1
			)
			LEFT JOIN
				(SELECT
					h.`hide_uid` AS hide
				FROM
					`user` AS u
				INNER JOIN
					`diary_hide_info` AS h
				ON (
					u.`id` = h.`uid`
				AND
					u.`validityflg` = 1
				)
				WHERE
					u.`id` = ?) AS hd
			ON (
				d.`uid` = hd.`hide`
			)
			LEFT JOIN
				`diary_like` AS dl
			ON
				dl.`diary_id` = d.`id`
			AND
				dl.`uid` = ?
			WHERE
				d.`course_id` = ?
			ORDER BY
				d.`id` DESC
			LIMIT {$limit} OFFSET {$_offset}
EOF;

		$_values = array($uid, $uid, $course_id);
		if ($query = $this->db->query($sql, $_values)) {
			return $query->result('array');
		}
		return null;
	}

	/**
	 * 日記投稿総件数取得
	 *
	 * 指定講座の日記投稿総件数を取得する
	 *
	 * @param int $course_id 講座テーブルID
	 * @return int 日記投稿総件数
	 */
	public function getTotalCount($course_id) {

		$_authority = $this->session->userdata('authority');
		$_uid = $_authority[0]->id;

		$_sql =<<< EOF
			SELECT
				COUNT(d.`id`) AS cnt
			FROM
				`diary` AS d
			INNER JOIN
				`user` AS u
			ON (
				d.`uid` = u.`id`
			AND
				u.`validityflg` = 1
			)
			LEFT JOIN
				(SELECT
					h.`hide_uid` AS hide
				FROM
					`user` AS u
				INNER JOIN
					`diary_hide_info` AS h
				ON (
					u.`id` = h.`uid`
				AND
					u.`validityflg` = 1
				)
				WHERE
					u.`id` = ?) AS hd
			ON (
				d.`uid` = hd.`hide`
			)
			LEFT JOIN
				`diary_like` AS dl
			ON
				dl.`diary_id` = d.`id`
			AND
				dl.`uid` = ?
			WHERE
				d.`course_id` = ?
EOF;

		$_values = array($_uid, $_uid, $course_id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('object')[0]->cnt;
		}
		return 0;
	}

	/**
	 * ホーム画面（受講者）用日記投稿一覧取得
	 * 自分が受講している講座の日記を取得する。
	 * ※システム管理者の場合はすべての日記を取得する。
	 *
	 * @return object ホーム画面（受講者）用日記投稿一覧
	 */
	public function getDiaryListForStudentshome()
	{
		$_profile_default_image = $this->commonlib->baseUrl().$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
		$_profile_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=";
		$_authority = $this->session->userdata['authority'];
		$_uid = $_authority[0]->id;
		$_image_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_DIARY')."&id=";

		$_values = array($_uid, $_uid, $_uid);
		$_sql =<<<EOT
SELECT
	d.id,
	d.uid,
	(CASE WHEN u.picture_file IS NULL THEN '{$_profile_default_image}' ELSE concat('{$_profile_save_url}', u.id) END) AS picture_file,
	u.nickname,
	c.id AS cid,
	c.course_name,
	c.calendar_color,
	d.post,
	(CASE WHEN d.image_file IS NOT NULL AND d.image_file <> '' THEN concat('{$_image_save_url}', d.id) ELSE NULL END) AS image_file,
	(CASE WHEN dl.id IS NULL THEN 0 ELSE 1 END) AS diary_like,
	(SELECT COUNT(dl2.id) FROM diary_like AS dl2 WHERE dl2.diary_id = d.id) AS diary_like_cnt,
	DATE_FORMAT(d.created_at, '%Y/%m/%d %H:%i') AS created_at
FROM
	diary AS d
INNER JOIN
	course AS c
ON
	c.id = d.course_id
INNER JOIN
	user AS u
ON
	u.id = d.uid
AND
	u.validityflg = 1
LEFT JOIN
	diary_hide_info AS h
ON
	h.uid = ?
AND
	h.hide_uid = d.uid
LEFT JOIN
	diary_like AS dl
ON
	dl.diary_id = d.id
AND
	dl.uid = ?
WHERE
	d.course_id IN (
		SELECT s.course_id FROM students AS s WHERE s.uid = ?
	)
AND
	h.id IS NULL
ORDER BY
	d.id DESC
LIMIT {$this->config->item('STUDENTSHOME_DIARY_MAX')}
EOT;
		if ($this->config->item('AUTH_SYS_MANAGER') == $_authority[0]->authority) {
			// システム管理者の場合は全日記取得
			$_values = array($_uid, $_uid);
			$_sql =<<<EOT
SELECT
	d.id,
	d.uid,
	(CASE WHEN u.picture_file IS NULL THEN '{$_profile_default_image}' ELSE concat('{$_profile_save_url}', u.id) END) AS picture_file,
	u.nickname,
	c.id AS cid,
	c.course_name,
	c.calendar_color,
	d.post,
	(CASE WHEN d.image_file IS NOT NULL AND d.image_file <> '' THEN concat('{$_image_save_url}', d.id) ELSE NULL END) AS image_file,
	(CASE WHEN dl.id IS NULL THEN 0 ELSE 1 END) AS diary_like,
	(SELECT COUNT(dl2.id) FROM diary_like AS dl2 WHERE dl2.diary_id = d.id) AS diary_like_cnt,
	DATE_FORMAT(d.created_at, '%Y/%m/%d %H:%i') AS created_at
FROM
	diary AS d
INNER JOIN
	course AS c
ON
	c.id = d.course_id
INNER JOIN
	user AS u
ON
	u.id = d.uid
AND
	u.validityflg = 1
LEFT JOIN
	diary_hide_info AS h
ON
	h.uid = ?
AND
	h.hide_uid = d.uid
LEFT JOIN
	diary_like AS dl
ON
	dl.diary_id = d.id
AND
	dl.uid = ?
WHERE
	h.id IS NULL
ORDER BY
	d.id DESC
LIMIT {$this->config->item('STUDENTSHOME_DIARY_MAX')}
EOT;
		}

		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('object');
		} else {
			return null;
		}
	}

	/**
	 * テーマ画面用日記投稿一覧取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return object テーマ画面用日記投稿一覧
	 */
	public function getDiaryListForTheme($cid)
	{
		$_profile_default_image = $this->commonlib->baseUrl().$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
		$_profile_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=";
		$_authority = $this->session->userdata['authority'];
		$_uid = $_authority[0]->id;
		$_image_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_DIARY')."&id=";

		$_values = array($_uid, $_uid, $cid);
		$_sql =<<<EOT
SELECT
	d.id,
	d.uid,
	(CASE WHEN u.picture_file IS NULL THEN '{$_profile_default_image}' ELSE concat('{$_profile_save_url}', u.id) END) AS picture_file,
	u.nickname,
	c.id AS cid,
	c.course_name,
	c.calendar_color,
	d.post,
	(CASE WHEN d.image_file IS NOT NULL AND d.image_file <> '' THEN concat('{$_image_save_url}', d.id) ELSE NULL END) AS image_file,
	(CASE WHEN dl.id IS NULL THEN 0 ELSE 1 END) AS diary_like,
	(SELECT COUNT(dl2.id) FROM diary_like AS dl2 WHERE dl2.diary_id = d.id) AS diary_like_cnt,
	DATE_FORMAT(d.created_at, '%Y/%m/%d %H:%i') AS created_at
FROM
	diary AS d
INNER JOIN
	course AS c
ON
	c.id = d.course_id
INNER JOIN
	user AS u
ON
	u.id = d.uid
AND
	u.validityflg = 1
LEFT JOIN
	diary_hide_info AS h
ON
	h.uid = ?
AND
	h.hide_uid = d.uid
LEFT JOIN
	diary_like AS dl
ON
	dl.diary_id = d.id
AND
	dl.uid = ?
WHERE
	d.course_id = ?
AND
	h.id IS NULL
ORDER BY
	d.id DESC
LIMIT {$this->config->item('THEME_DIARY_MAX')}
EOT;
		if ($this->config->item('AUTH_SYS_MANAGER') == $_authority[0]->authority) {
			// システム管理者の場合は全日記取得
			$_values = array($_uid, $_uid, $cid);
			$_sql =<<<EOT
SELECT
	d.id,
	d.uid,
	(CASE WHEN u.picture_file IS NULL THEN '{$_profile_default_image}' ELSE concat('{$_profile_save_url}', u.id) END) AS picture_file,
	u.nickname,
	c.id AS cid,
	c.course_name,
	c.calendar_color,
	d.post,
	(CASE WHEN d.image_file IS NOT NULL AND d.image_file <> '' THEN concat('{$_image_save_url}', d.id) ELSE NULL END) AS image_file,
	(CASE WHEN dl.id IS NULL THEN 0 ELSE 1 END) AS diary_like,
	(SELECT COUNT(dl2.id) FROM diary_like AS dl2 WHERE dl2.diary_id = d.id) AS diary_like_cnt,
	DATE_FORMAT(d.created_at, '%Y/%m/%d %H:%i') AS created_at
FROM
	diary AS d
INNER JOIN
	course AS c
ON
	c.id = d.course_id
INNER JOIN
	user AS u
ON
	u.id = d.uid
AND
	u.validityflg = 1
LEFT JOIN
	diary_hide_info AS h
ON
	h.uid = ?
AND
	h.hide_uid = d.uid
LEFT JOIN
	diary_like AS dl
ON
	dl.diary_id = d.id
AND
	dl.uid = ?
WHERE
	d.course_id = ?
AND
	h.id IS NULL
ORDER BY
	d.id DESC
LIMIT {$this->config->item('THEME_DIARY_MAX')}
EOT;
		}

		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('object');
		} else {
			return null;
		}
	}

	/**
	 * 日記画面投稿削除
	 *
	 * @param int $pid 日記ID
	 */
	public function deletePost($pid)
	{
		$_values = array($pid);

		$sql =<<<EOF
			DELETE
			FROM
			  `diary`
			WHERE
			  `id` = ?
EOF;
		$this->db->query($sql, $_values);
	}

	/**
	 * 日記画面新規投稿
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param string $postmsg 投稿メッセージ
	 * @param string $imagefile 画像ファイル
	 */
	public function registPost($cid, $postmsg, $imagefile)
	{
		$authority = $this->session->userdata['authority'];
		$uid = $authority[0]->id;

		$sql =<<<EOF
			INSERT INTO
				`diary`(
					`course_id`,
					`uid`,
					`post`,
					`image_file`,
					`created_at`
			) VALUES (
				?,
				?,
				?,
				?,
				CURRENT_TIMESTAMP
			)
EOF;
		$_values = array($cid, $uid, $postmsg, $imagefile);
		$this->db->query($sql, $_values);
	}

	/**
	 * 日記画面投稿編集
	 *
	 * @param int $did 日記ID
	 * @param string $postmsg 投稿メッセージ
	 * @param string $imagefile 画像ファイル
	 */
	public function editPost($did, $postmsg, $imagefile)
	{
		// イメージファイルが未設定の場合更新対象外とする
		if ($imagefile == null or $imagefile == "") {
			$sql =<<<EOF
				UPDATE
					`diary`
				SET
					`post` = ?,
					`updated_at` = CURRENT_TIMESTAMP
				WHERE
					`id` = ?
EOF;
			$_values = array($postmsg, $did);
		} else {
			$sql =<<<EOF
				UPDATE
					`diary`
				SET
					`post` = ?,
					`image_file` = ?,
					`updated_at` = CURRENT_TIMESTAMP
				WHERE
					`id` = ?
EOF;
			$_values = array($postmsg, $imagefile, $did);
		}
		$this->db->query($sql, $_values);
	}

	/**
	 * 日記画面投稿取得
	 *
	 * @param int $did 日記ID
	 */
	public function getPostInfo($did)
	{
		$sql =<<<EOF
			SELECT
				`id`,
				`course_id`,
				`uid`,
				`post`,
				`image_file`,
				`created_at`,
				`updated_at`
			FROM
				`diary`
			WHERE
				`id` = ?
EOF;
		$_values = array($pid);
		if ($query = $this->db->query($sql, $_values)) {
			// 成功処理
			$retData = $query->result('array');
		}
	}

	/**
	 * 画像ファイル取得
	 *
	 * @param int $did 日記ID
	 * @return string 画像ファイル
	 */
	public function getImageFile($did) {
		$sql =<<<EOF
			SELECT
				`image_file`
			FROM
				`diary`
			WHERE
				`id` = ?
EOF;
		$_values = array($did);
		if ($query = $this->db->query($sql, $_values)) {
			// 成功処理
			if (0 < $query->num_rows()) {
				$tmp = $query->result('array')[0];
				return $tmp['image_file'];
			}
		}
	}

	/**
	 * 指定ユーザー画像ファイル取得
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return object 指定ユーザー画像ファイル
	 */
	public function getImageFiles($uid)
	{
		$_diary_save_path = $this->config->item('DIARY_IMAGE_SAVE_PATH').'/';

		$_sql =<<<EOT
SELECT
	concat('{$_diary_save_path}', image_file)
FROM
	diary
WHERE
	uid = ?
AND
	image_file IS NOT NULL
EOT;
		$_values = array($uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * 日記テーブルから指定ユーザーデータ削除
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByUid($uid)
	{
		$_sql = "DELETE FROM diary WHERE uid = ?";
		$_values = array($uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 日記テーブルから指定講座データ削除
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByCid($cid)
	{
		$_sql = "DELETE FROM diary WHERE course_id = ?";
		$_values = array($cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 日記非表示情報テーブルから指定ユーザーデータ削除
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteHideInfoByUid($uid)
	{
		$_sql = "DELETE FROM diary_hide_info WHERE uid = ? OR hide_uid = ?";
		$_values = array($uid, $uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 日記非表示情報テーブルから指定講座データ削除
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteHideInfoByCid($cid)
	{
		$_sql = "DELETE FROM diary_hide_info WHERE course_id = ?";
		$_values = array($cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

}