<?php
/*
 * 設定テーブルモデル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_Model extends CI_Model {
	/** ロゴ画像保存先URL. */
	private $logo_image_base_url;
	/** 壁紙画像保存先URL. */
	private $background_image_base_url;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();

		$this->logo_image_base_url = $this->commonlib->assetsUrl().$this->config->item('SETTINGS_LOGO_IMAGE_URL');
		$this->background_image_base_url = $this->commonlib->assetsUrl().$this->config->item('SETTINGS_BACKGROUND_IMAGE_URL');
	}

	/**
	 * 通常画面表示用設定情報取得
	 *
	 * @return object 設定情報
	 */
	public function get_display_data()
	{
		$_sql =<<<EOT
SELECT
	title,
	(CASE WHEN logo_img IS NULL OR logo_img = '' THEN '' ELSE concat('{$this->logo_image_base_url}', logo_img) END) AS logo_img,
	introduction,
	background_color,
	back_img_use,
	(CASE WHEN background_img IS NULL OR background_img = '' THEN '' ELSE concat('{$this->background_image_base_url}', background_img) END) AS background_img
FROM
	setting
LIMIT 1
EOT;
		$_query = $this->db->query($_sql);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object')[0];
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * システム管理（設定）画面表示用設定情報取得
	 *
	 * @return object 設定情報
	 */
	public function get_display_data_for_settings()
	{
		$_logo_no_img_url = $this->commonlib->assetsUrl().$this->config->item('SETTINGS_LOGO_NO_IMAGE_URL');
		$_background_no_img_url = $this->commonlib->assetsUrl().$this->config->item('SETTINGS_BACKGROUND_NO_IMAGE_URL');

		$_sql =<<<EOT
SELECT
	title,
	(CASE WHEN logo_img IS NULL OR logo_img = '' THEN '{$_logo_no_img_url}' ELSE concat('{$this->logo_image_base_url}', logo_img) END) AS logo_img,
	introduction,
	background_color,
	back_img_use,
	(CASE WHEN background_img IS NULL OR background_img = '' THEN '{$_background_no_img_url}' ELSE concat('{$this->background_image_base_url}', background_img) END) AS background_img,
	(CASE WHEN background_img IS NULL OR background_img = '' THEN '' ELSE concat('{$this->background_image_base_url}', background_img) END) AS validate_background_img
FROM
	setting
LIMIT 1
EOT;
		$_query = $this->db->query($_sql);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object')[0];
			return $_result;
		} else {
			$_result = new stdClass();
			$_result->title = '';
			$_result->logo_img = $_logo_no_img_url;
			$_result->introduction = '';
			$_result->background_color = '';
			$_result->back_img_use = '';
			$_result->background_img = $_background_no_img_url;
			$_result->validate_background_img = '';
			return $_result;
		}
	}

	/**
	 * ロゴ取得
	 * ※テーブルの情報をそのまま返したいので
	 * 　NULLやレコードが無い場合はnullを返す。
	 *
	 * @return string ロゴ
	 */
	public function get_logo()
	{
		$_sql =<<<EOT
SELECT
	logo_img
FROM
	setting
LIMIT 1
EOT;
		$_query = $this->db->query($_sql);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object')[0]->logo_img;
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * 壁紙取得
	 * ※テーブルの情報をそのまま返したいので
	 * 　NULLやレコードが無い場合はnullを返す。
	 *
	 * @return string 壁紙
	 */
	public function get_wallpaper()
	{
		$_sql =<<<EOT
SELECT
	background_img
FROM
	setting
LIMIT 1
EOT;
		$_query = $this->db->query($_sql);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object')[0]->background_img;
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * イントロダクション取得
	 *
	 * @return string イントロダクション
	 */
	public function get_introduction()
	{
		$_sql =<<<EOT
SELECT
	introduction
FROM
	setting
LIMIT 1
EOT;
		$_query = $this->db->query($_sql);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object')[0]->introduction;
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * 更新
	 *
	 * @param string $title タイトル
	 * @param boolean $is_logo_delete ロゴを削除するフラグ
	 * @param string $logo_img ロゴ画像
	 * @param string $introduction イントロダクション
	 * @param string $background_color 背景色
	 * @param int $back_img_use 壁紙使用フラグ
	 * @param string $background_img 壁紙画像
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function update($title, $is_logo_delete, $logo_img, $introduction, $background_color, $back_img_use, $background_img)
	{
		if ($is_logo_delete) {
			$logo_img = null;
		}

		// レコードがあるか確認
		$_sql =<<<EOT
SELECT
	*
FROM
	setting
EOT;
		$_query = $this->db->query($_sql);

		$_values = array();
		if ($_query->num_rows() > 0) { // レコードがある場合はUPDATE
			$_logo_update_sql = 'logo_img = ?,';
			$_values = array($title, $logo_img, $introduction, $background_color, $back_img_use);
			if (!$is_logo_delete && !$logo_img) {
				// ロゴを削除しないが画像ファイルの指定が無い場合は
				// 現在のロゴファイルを使う
				$_logo_update_sql = '';
				$_values = array($title, $introduction, $background_color, $back_img_use);
			}
			$_background_img_sql = '';
			if (!is_null($background_img)) {
				// 壁紙画像が指定されている場合は更新する。
				$_background_img_sql = 'background_img = ?,';
				$_values[] = $background_img;
			}
			$_sql =<<<EOT
UPDATE
	setting
SET
	title = ?,
	{$_logo_update_sql}
	introduction = ?,
	background_color = ?,
	back_img_use = ?,
	{$_background_img_sql}
	updated_at = NOW()
EOT;
		} else { // レコードが無い場合はINSERT
			$_logo_insert_sql = 'logo_img,';
			$_logo_insert_placeholder = '?,';
			$_values = array($title, $logo_img, $introduction, $background_color, $back_img_use, $background_img);
			if (!$is_logo_delete && !$logo_img) {
				// ロゴを削除しないが画像ファイルの指定が無い場合は
				// INSERT文に含めない
				$_logo_insert_sql = '';
				$_logo_insert_placeholder = '';
				$_values = array($title, $introduction, $background_color, $back_img_use, $background_img);
			}
			$_sql =<<<EOT
INSERT INTO setting (
	title,
	{$_logo_insert_sql}
	introduction,
	background_color,
	back_img_use,
	background_img,
	updated_at
) VALUES (
	?,
	{$_logo_insert_placeholder}
	?,
	?,
	?,
	?,
	NOW()
)
EOT;
		}

		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

}
