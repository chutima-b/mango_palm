<?php
/*
 * テーマ情報モデル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

class Theme_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 課題を作成する取得
	 *
	 * @param int $id テーマテーブルID
	 * @return int 課題を作成する（0:作成しない / 1:作成する）
	 */
	public function getEvaluationUse($id) {
		$_sql =<<<EOT
SELECT
	evaluation_use
FROM
	theme
WHERE
	id = ?
EOT;
		$_values = array($id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result[0]->evaluation_use;
		} else {
			return 0;
		}
	}

	/**
	 * テーマ情報取得
	 *
	 * @param int $id テーマテーブルID
	 * @param boolean $is_open 公開データのみ取得フラグ
	 * @return array テーマ情報
	 */
	public function getThemeInfo($id=null, $is_open=false) {
		$_where_open = '';
		if ($is_open) {
			// 公開データのみ取得が指定されている場合は、公開データのみに絞り込む
			$_where_open = ' AND t.`open` = '.$this->config->item('THEME_OPEN');

			// ただし、講師以上の権限の場合は、公開終了以外を対象とする
			$_authority = $this->session->userdata('authority');
			if ($this->config->item('AUTH_TEACHER') == $_authority[0]->authority
			 || $this->config->item('AUTH_OP_MANAGER') == $_authority[0]->authority
			 || $this->config->item('AUTH_SYS_MANAGER') == $_authority[0]->authority) {
				$_where_open = ' AND t.`open` <> '.$this->config->item('THEME_PUBLIC_END');
			}
		}

		$_icon_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_GENRE')."&id=";

		$sql =<<< EOF
			SELECT
				t.`id` AS id,
				t.`course_id` AS course_id,
				c.`course_name` AS course_name,
				(CASE WHEN g.icon IS NULL THEN NULL ELSE concat('{$_icon_save_url}', g.id) END) AS icon,
				c.`forum_use` AS forum_use,
				t.`name` AS name,
				t.`goal` AS goal,
				t.`description` AS description,
				(CASE WHEN t.`theme_date` IS NULL OR t.`theme_date` = '0000-00-00 00:00:00' THEN '' ELSE DATE_FORMAT(t.`theme_date`, '%c/%e') END) AS theme_date,
				(CASE WHEN t.`time_limit` IS NULL OR t.`time_limit` = '0000-00-00 00:00:00' THEN '' ELSE DATE_FORMAT(t.`time_limit`, '%Y/%m/%d') END) AS time_limit,
				/* 期限が未設定の場合、またはテーマの日付に達している且つ、期限が過ぎてない場合は、課題の提出が可能 */
				(CASE WHEN t.`time_limit` IS NULL OR t.`time_limit` = '0000-00-00 00:00:00'
					OR (t.`theme_date` IS NOT NULL AND DATE_FORMAT(t.`theme_date`, '%Y%m%d') <= (CURDATE() + 0)
						AND t.`time_limit` IS NOT NULL AND DATE_FORMAT(t.`time_limit`, '%Y%m%d') >= (CURDATE() + 0))
					THEN 1 ELSE 0 END) AS submission_use,
				t.`evaluation_use`,
				t.`quiz`,
				t.`open`
			FROM
				`theme` AS t
			INNER JOIN
				`course` AS c
			ON (
				t.`course_id` = c.`id`
			)
			INNER JOIN
				`genre` AS g
			ON (
				g.`id` = c.`genre`
			)
			WHERE
				t.`id` = ?
			{$_where_open}
EOF;

		$_values = array($id);
		if ($query = $this->db->query($sql, $_values)) {
			// 成功処理
			return $query->result('array');
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * テーマ情報取得(編集画面用)
	 *
	 * @param int $id テーマテーブルID
	 * @return array テーマ情報
	 */
	public function getThemeInfoForEdit($id=NULL) {

		$sql =<<< EOF
			SELECT
				t.`id` AS id,
				t.`name` AS title,
				t.`goal` AS goal,
				t.`description` AS description,
				(CASE WHEN t.`theme_date` IS NULL OR t.`theme_date` = '0000-00-00 00:00:00' THEN '' ELSE DATE_FORMAT(t.`theme_date`, '%Y/%m/%d') END) AS theme_date,
				(CASE WHEN t.`time_limit` IS NULL OR t.`time_limit` = '0000-00-00 00:00:00' THEN '' ELSE DATE_FORMAT(t.`time_limit`, '%Y/%m/%d') END) AS time_limit,
				t.`evaluation_use`,
				t.`open`,
				t.`quiz`,
				(CASE WHEN t.`quiz` = 0 THEN
					/* クイズ以外の場合は、全課題の評価点の合計 */
					(SELECT SUM(ta.`evaluation_points`) FROM task AS ta WHERE ta.`course_id` = t.`course_id` AND ta.`theme_id` = t.`id`)
				ELSE
					/* クイズの場合は、単一選択の課題数 */
					(SELECT COUNT(ta.`id`) FROM task AS ta WHERE ta.`course_id` = t.`course_id` AND ta.`theme_id` = t.`id` AND ta.`task_type` = {$this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')})
				END) AS evaluation_points
			FROM
				`theme` AS t
			WHERE
				t.`id` = ?
EOF;
		$_values = array($id);
		if ($query = $this->db->query($sql, $_values)) {
			// 成功処理
			return $query->result('array');
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * 講座画面用テーマ一覧取得
	 *
	 * @param int $cid 講座テーブルID
	 * @param int $uid ユーザーテーブルID
	 * @return object テーマ情報
	 */
	public function getThemeListOfCourse($cid, $uid) {

		$_open = "th.`open` = {$this->config->item('THEME_OPEN')}";
		$_authority = $this->session->userdata('authority');
		if ($this->config->item('AUTH_TEACHER') == $_authority[0]->authority
		 || $this->config->item('AUTH_OP_MANAGER') == $_authority[0]->authority
		 || $this->config->item('AUTH_SYS_MANAGER') == $_authority[0]->authority) {
			// 講師以上の権限の場合は、公開終了以外を対象とする
			$_open = "th.`open` <> {$this->config->item('THEME_PUBLIC_END')}";
		}
		if ($this->config->item('AUTH_SYS_MANAGER') != $_authority[0]->authority) {
			// システム管理者以外の場合、課題フィードバック情報を取得する
			$sql =<<< EOF
				SELECT
					th.`id` AS id,
					th.`name` AS name,
					th.`goal` AS goal,
					th.`description` AS description,
					th.`open`,
					DATE_FORMAT(th.`theme_date`, '%c/%e') AS theme_date,
					DATE_FORMAT(th.`time_limit`, '%Y/%m/%d') AS time_limit,
					(CASE WHEN tf.`submit_at` IS NULL OR tf.`submit_at` = '0000-00-00 00:00:00' THEN 0 ELSE 1 END) AS is_submit,
					(CASE WHEN tf.`feedback_at` IS NULL OR tf.`feedback_at` = '0000-00-00 00:00:00' THEN 0 ELSE 1 END) AS is_feedback,
					(CASE WHEN tf.`confirmed_at` IS NULL OR tf.`confirmed_at` = '0000-00-00 00:00:00' THEN 0 ELSE 1 END) AS is_confirmed
				FROM
					`theme` AS th
				LEFT JOIN
					`task_feedback` AS tf
				ON
					tf.`course_id` = th.`course_id`
				AND
					tf.`theme_id` = th.`id`
				AND
					tf.`uid` = ?
				WHERE
					th.`course_id` = ?
				AND
					{$_open}
				ORDER BY
					th.`theme_date`, th.`id`
EOF;

			$_values = array($uid, $cid);
		} else {
			// システム管理者の場合、テーマ情報のみを取得する
			$sql =<<< EOF
				SELECT
					th.`id` AS id,
					th.`name` AS name,
					th.`goal` AS goal,
					th.`description` AS description,
					th.`open`,
					DATE_FORMAT(th.`theme_date`, '%c/%e') AS theme_date,
					DATE_FORMAT(th.`time_limit`, '%Y/%m/%d') AS time_limit,
					0 AS is_submit,
					0 AS is_feedback,
					0 AS is_confirmed
				FROM
					`theme` AS th
				WHERE
					th.`course_id` = ?
				AND
					{$_open}
				ORDER BY
					th.`theme_date`, th.`id`
EOF;

			$_values = array($cid);
		}
		if ($query = $this->db->query($sql, $_values)) {
			return $query->result('object');
		}
		return null;
	}

	/**
	 * 講座編集画面用テーマ一覧取得
	 *
	 * @param int $cid 講座テーブルID
	 * @return object テーマ情報
	 */
	public function getThemeListForCourseedit($cid) {
		$sql =<<< EOF
			SELECT
				th.id AS id,
				th.name AS name,
				th.goal AS goal,
				th.description AS description,
				DATE_FORMAT(th.theme_date, '%c/%e') AS theme_date,
				DATE_FORMAT(th.time_limit, '%Y/%m/%d') AS time_limit,
				th.evaluation_use,
				th.open,
				/* 期限切れの場合は1、それ以外は0 */
				(CASE WHEN
					th.time_limit IS NOT NULL
				AND
					th.time_limit <> '0000-00-00 00:00:00'
				AND
					DATE_FORMAT(th.time_limit, '%Y%m%d') < (CURDATE() + 0)
				THEN
					1
				ELSE
					0
				END) AS over_limit,
				/* 課題提出済み（受講済）の受講者数 */
				(SELECT
						COUNT(id)
					FROM
						task_feedback AS tf
					WHERE
						tf.course_id = th.course_id
					AND
						tf.theme_id = th.id) AS attended_cnt
			FROM
				theme AS th
			WHERE
				th.course_id = ?
			ORDER BY
				th.theme_date, th.id
EOF;

		$_values = array($cid);
		if ($query = $this->db->query($sql, $_values)) {
			// 成功処理
			return $query->result('object');
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * 課題管理画面用テーマ一覧取得
	 *
	 * @param int $cid 講座テーブルID
	 * @return object テーマ情報
	 */
	public function getThemeListForTaskmanager($cid) {
		$sql =<<< EOF
			SELECT
				th.`id` AS id,
				th.`name` AS name,
				th.`goal` AS goal,
				th.`description` AS description,
				DATE_FORMAT(th.`theme_date`, '%c/%e') AS theme_date,
				DATE_FORMAT(th.`time_limit`, '%Y/%m/%d') AS time_limit,
				th.`evaluation_use`,
				th.`open`,
				th.`quiz`
			FROM
				`theme` AS th
			WHERE
				th.`course_id` = ?
			ORDER BY
				th.`theme_date`, th.`id`
EOF;

		$_values = array($cid);
		if ($query = $this->db->query($sql, $_values)) {
			// 成功処理
			return $query->result('object');
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * 受講者課題画面用テーマ取得
	 *
	 * @param int $cid 講座テーブルID
	 * @param int $thid テーマテーブルID
	 * @return object テーマ情報
	 */
	public function getThemeInfoForTaskmanageusers($cid, $thid) {
		$sql =<<< EOF
SELECT
	th.name AS name,
	th.goal AS goal,
	th.description AS description,
	DATE_FORMAT(th.theme_date, '%c/%e') AS theme_date,
	DATE_FORMAT(th.time_limit, '%Y/%m/%d') AS time_limit,
	th.evaluation_use,
	th.open,
	th.quiz,
	/* 「課題を作成する」がYESかつ、 */
	/* 公開中または、公開終了、または、非公開であっても１件以上課題が提出されている場合 */
	/* ダウンロードボタンを有効にする */
	(
		CASE WHEN
		(
			SELECT
				COUNT(th2.id) AS cnt
			FROM
				theme AS th2
			WHERE
				th2.course_id = th.course_id
			AND
				th2.id = th.id
			AND
				th2.evaluation_use = {$this->config->item('EVALUATION_USE_FLAG_ON')}
			AND
			(
				th2.open = {$this->config->item('THEME_OPEN')}
			OR
				th2.open = {$this->config->item('THEME_PUBLIC_END')}
			OR
				(
					th2.open = {$this->config->item('THEME_CLOSE')}
				AND
					(
						SELECT
							COUNT(tf.id)
						FROM
							task_feedback AS tf
						WHERE
							tf.course_id = th2.course_id
						AND
							tf.theme_id = th2.id
						AND
							tf.submit_at IS NOT NULL
						AND
							tf.submit_at <> '0000-00-00 00:00:00'
					) > 0
				)
			)
		) > 0
		THEN
			1
		ELSE
			0
		END
	) AS enable_download
FROM
	theme AS th
WHERE
	th.course_id = ?
AND
	th.id = ?
ORDER BY
	th.theme_date
EOF;

		$_values = array($cid, $thid);
		$_query = $this->db->query($sql, $_values);
		if ($_query->num_rows() == 1) {
			return $_query->result('object')[0];
		}
		return null;
	}

	/**
	 * 指定テーマIDのテーマ名取得
	 *
	 * @param int $id テーマテーブルID
	 * @return string テーマ名称
	 */
	public function getThemeName($id) {
		$_sql = "SELECT name FROM theme WHERE id = ?";
		$_values = array($id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result[0]->name;
		} else {
			return null;
		}
	}

	/**
	 * 指定講座の最も早いテーマ日付けを取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return string 最も早いテーマ日付け
	 */
	public function getFirstDate($cid) {
		$_sql =<<<EOT
SELECT
	DATE_FORMAT(MIN(t.theme_date), '%Y/%m/%d') AS first_date
FROM
	theme AS t
WHERE
 t.course_id = ?
EOT;
		$_values = array($cid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result[0]->first_date;
		} else {
			return null;
		}
	}

	/**
	 * 指定講座の最も遅い期限を取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return string 最も遅い期限
	 */
	public function getLastLimit($cid) {
		$_sql =<<<EOT
SELECT
	DATE_FORMAT(MAX(t.time_limit), '%Y/%m/%d') AS last_limit
FROM
	theme AS t
WHERE
 t.course_id = ?
EOT;
		$_values = array($cid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result[0]->last_limit;
		} else {
			return null;
		}
	}

	/**
	 * 指定した講座情報テーブルIDのレコード削除
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByCourseId($cid) {
		$_sql = "DELETE FROM theme WHERE course_id = ?";
		$_values = array($cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定したテーマIDのレコード削除
	 *
	 * @param int $thid テーマ情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByThemeId($thid) {
		$_sql = "DELETE FROM theme WHERE id = ?";
		$_values = array($thid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * テーマ情報登録
	 *
	 * @param int $cid
	 * @param string $title
	 * @param string $goal
	 * @param string $themedate
	 * @param string $description
	 * @param string $time_limit
	 * @param int $evaluation_use
	 * @param int $quiz
	 * @return int テーマID
	 */
	public function registTheme($cid, $title, $goal, $themedate, $description, $time_limit, $evaluation_use, $quiz) {

		$sql =<<< EOF
			INSERT INTO
				`theme`
			(
				`course_id`,
				`name`,
				`goal`,
				`description`,
				`theme_date`,
				`time_limit`,
				`evaluation_use`,
				`open`,
				`quiz`,
				`created_at`
			)
			VALUES
			(
				?,
				?,
				?,
				?,
				?,
				?,
				?,
				0,
				?,
				CURRENT_TIMESTAMP
			)
EOF;

		$_values = array($cid, $title, $goal, $description, $themedate, $time_limit, $evaluation_use, $quiz);
		if ($this->db->query($sql, $_values)) {
			return $this->db->insert_id();
		} else {
			return null;
		}
	}

	/**
	 * テーマ情報更新
	 *
	 * @param int $thid
	 * @param string $title
	 * @param string $goal
	 * @param string $themedate
	 * @param string $description
	 * @param string $time_limit
	 * @param int $evaluation_use
	 * @param int $quiz
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function updateTheme($thid, $title, $goal, $themedate, $description, $time_limit, $evaluation_use, $quiz) {

		$sql =<<< EOF
			UPDATE
				`theme`
			SET
				`name` = ?,
				`goal` = ?,
				`description` = ?,
				`theme_date` = ?,
				`time_limit` = ?,
				`evaluation_use` = ?,
				`quiz` = ?,
				`updated_at` = CURRENT_TIMESTAMP
			WHERE
				`id` = ?
EOF;

		$_values = array($title, $goal, $description, $themedate, $time_limit, $evaluation_use, $quiz, $thid);
		if ($this->db->query($sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * テーマ公開状態更新
	 *
	 * @param int $thid テーマID
	 * @param int $open 0:非公開 / 1:公開 / 9:公開終了
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function updateOpen($thid, $open) {

		$sql =<<< EOF
			UPDATE
				`theme`
			SET
				`open` = ?,
				`updated_at` = CURRENT_TIMESTAMP
			WHERE
				`id` = ?
EOF;

		$_values = array($open, $thid);
		if ($this->db->query($sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定講座のテーマコピー
	 *
	 * 指定された講座に紐付くテーマをコピーする。
	 * この際、日付、期限はクリアする。
	 *
	 * @param int $src_cid コピー元講座情報テーブルID
	 * @param int $dest_cid コピー先講座情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function copyOfCourse($src_cid, $dest_cid) {
		$_search_str = 'displayfile?tbl='.$this->config->item('TABLE_KEY_THEME').'&amp;cid='.$src_cid.'&amp;fl=';
		$_replace_str = 'displayfile?tbl='.$this->config->item('TABLE_KEY_THEME').'&amp;cid='.$dest_cid.'&amp;fl=';
		$_sql =<<<EOT
INSERT INTO theme
(
	course_id,
	name,
	goal,
	description,
	evaluation_use,
	open,
	quiz
)
SELECT
	? AS course_id,
	name,
	goal,
	REPLACE(description, ?, ?) AS description,
	evaluation_use,
	{$this->config->item('THEME_CLOSE')} AS open,
	quiz
FROM
	theme
WHERE
	course_id = ?
ORDER BY id
EOT;
		$_values = array($dest_cid, $_search_str, $_replace_str, $src_cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定講座のテーマID取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return NULL|object 指定講座のテーマID
	 */
	public function getThemeIdOfCourse($cid)
	{
		$_sql =<<<EOT
SELECT
	id
FROM
	theme
WHERE
	course_id = ?
ORDER BY id
EOT;
		$_values = array($cid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('object');
		} else {
			return null;
		}
	}

	/**
	 * 指定講座の不要なWYSIWYG画像削除バッチ用テーマ情報取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return object テーマ情報
	 */
	public function getThemeListForBatchDeleteWYSIWYG($cid) {
		$_sql =<<<EOT
SELECT
	id,
	description
FROM
	theme
WHERE
	course_id = ?
ORDER BY
	id
EOT;
		$_values = array($cid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		}
		return null;
	}

	/**
	 * 指定講座の不要なWYSIWYG画像削除バッチ用テーマIDリスト取得（課題の問題削除用）
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return object テーマ情報
	 */
	public function getThemeListForBatchDeleteWYSIWYGOfTask($cid) {
		$_sql =<<<EOT
SELECT
	id
FROM
	theme
WHERE
	course_id = ?
ORDER BY
	id
EOT;
		$_values = array($cid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		}
		return null;
	}

}