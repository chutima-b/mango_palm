<?php
/*
 * 受講者情報テーブル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Students_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 受講者管理画面受講者一覧情報取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return array 受講者一覧情報
	 */
	public function getStudentsList($cid, $page, $limit) {

		$_sql =<<<EOT
			SELECT
				s.`id` AS stid,
				u.`id`,
				u.`userid`,
				u.`nickname`,
				u.`picture_file`
			FROM
				`students` AS s
			INNER JOIN
				`user` AS u
			ON (
				s.`uid` = u.`id`
			AND
				u.`validityflg` = 1
			)
			WHERE
				s.`course_id` = ?
			AND
				s.`authority` = {$this->config->item('AUTH_STUDENT')}
			ORDER BY u.id
			LIMIT
				?
			OFFSET
				?
EOT;

		$offset = $limit * ($page - 1);
		$_values = array($cid, $limit, $offset);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('array');
		} else {
			return null;
		}
	}

	/**
	 * 受講者管理画面受講者一覧件数取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return int 受講者一覧件数
	 */
	public function getStudentsListCount($cid) {

		$_sql =<<<EOT
			SELECT
				COUNT(s.`id`) AS cnt
			FROM
				`students` AS s
			INNER JOIN
				`user` AS u
			ON (
				s.`uid` = u.`id`
			AND
				u.`validityflg` = 1
			)
			WHERE
				s.`course_id` = ?
			AND
				s.`authority` = {$this->config->item('AUTH_STUDENT')}
EOT;
		$_values = array($cid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$tmp = $_query->result('array')[0];
			return $tmp['cnt'];
		} else {
			return null;
		}
	}

	/**
	 * 参加者登録画面候補者一覧情報取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param string $userid ユーザーID
	 * @param string $family 姓
	 * @param string $first 名
	 * @param int $page 該当ページ数
	 * @param int $limit 取得件数
	 * @return array 候補者一覧情報
	 */
	public function getCandidateList($cid, $userid, $family, $first, $page, $limit) {

		$_sql =<<<EOT
			SELECT
				u.`id` AS uid,
				u.`userid`,
				u.`surname`,
				u.`firstname`,
				u.`nickname`,
				u.`picture_file`
			FROM
				`user` AS u
			LEFT JOIN
				`students` AS auth
			ON (
				u.`id` = auth.`uid`
			AND
				(
					auth.`authority` = {$this->config->item('AUTH_GUEST')}
				OR
					auth.`authority` = {$this->config->item('AUTH_SYS_MANAGER')}
				)
			)
			LEFT JOIN
				`students` AS s
			ON (
				u.`id` = s.`uid`
			AND
				s.`course_id` = ?
			)
			WHERE
				auth.`id` IS null
			AND
				s.`id` IS null
			AND
				u.`validityflg` = 1
EOT;
		$_values = array($cid);

		if ($userid != null and $userid != "") {
			$_sql .= " AND u.userid LIKE ?";
			array_push($_values, $this->db->escape_like_str($userid)."%");
		}

		if ($family != null and $family != "") {
			$_sql .= " AND u.surname LIKE ?";
			array_push($_values, "%".$this->db->escape_like_str($family)."%");
		}

		if ($first != null and $first != "") {
			$_sql .= " AND u.firstname LIKE ?";
			array_push($_values, "%".$this->db->escape_like_str($first)."%");
		}

		$_sql .= " ORDER BY u.id LIMIT ? OFFSET ?";

		$offset = $limit * ($page - 1);
		array_push($_values, $limit);
		array_push($_values, $offset);

		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('array');
		} else {
			return null;
		}
	}

	/**
	 * 参加者登録画面候補者一覧件数取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param string $userid ユーザーID
	 * @param string $family 姓
	 * @param string $first 名
	 * @return int 候補者一覧件数
	 */
	public function getCandidateCount($cid, $userid, $family, $first) {

		$_sql =<<<EOT
			SELECT
				COUNT(u.`id`) AS cnt
			FROM
				`user` AS u
			LEFT JOIN
				`students` AS s
			ON (
				s.`uid` = u.`id`
			AND
				s.`course_id` = ?
			)
			LEFT JOIN
				(SELECT
					*
				FROM
					`students`
				WHERE
					`authority` = {$this->config->item('AUTH_GUEST')}
				OR
					`authority` = {$this->config->item('AUTH_SYS_MANAGER')}
				) AS auth
			ON (
				auth.`uid` = u.`id`
			)
			WHERE
				s.`id` IS null
			AND
				u.`validityflg` = 1
			AND
				auth.`id` IS null
EOT;
		$_values = array($cid);

			if ($userid != null and $userid != "") {
			$_sql .= " AND u.userid LIKE ?";
			array_push($_values, $this->db->escape_like_str($userid)."%");
		}

		if ($family != null and $family != "") {
			$_sql .= " AND u.surname LIKE ?";
			array_push($_values, "%".$this->db->escape_like_str($family)."%");
		}

		if ($first != null and $first != "") {
			$_sql .= " AND u.firstname LIKE ?";
			array_push($_values, "%".$this->db->escape_like_str($first)."%");
		}

		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$tmp = $_query->result('array')[0];
			return $tmp['cnt'];
		} else {
			return null;
		}
	}

	/**
	 * 講座に紐付く登録者情報削除
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByCourseId($cid)
	{
		$_sql = "DELETE FROM students WHERE course_id = ?";
		$_values = array($cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 受講者情報削除
	 *
	 * @param int $stid 受講者情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function delete($stid)
	{
		$_sql = "DELETE FROM students WHERE id = ?";
		$_values = array($stid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 管理者情報削除
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param int $auth 権限
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByAuth($cid, $auth)
	{
		$_sql = "DELETE FROM students WHERE course_id = ? AND authority = ?";
		$_values = array($cid, $auth);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定ユーザー受講者情報削除
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteByUid($uid)
	{
		$_sql = "DELETE FROM students WHERE uid = ?";
		$_values = array($uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 参加者情報登録
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param int $uid ユーザーID
	 * @param int $auth 権限
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function insert($cid, $uid, $auth) {
		if ($cid == null or $cid == "") {
			return false;
		}
		$_sql = <<<EOT
			INSERT INTO
				`students`
			(
			`course_id`,
			`uid`,
			`authority`,
			`created_at`
			)
			VALUES(
			?,
			?,
			?,
			CURRENT_TIMESTAMP
			)
EOT;
		$_values = array($cid, $uid, $auth);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ゲスト登録
	 * 指定口座にゲストユーザーを登録する
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function insertGuest($cid)
	{
		$_sql = <<<EOT
INSERT INTO students (
	course_id,
	uid,
	authority,
	created_at
) VALUES (
	?,
	(SELECT
		s.uid
	FROM
		students AS s
	WHERE
		s.authority = {$this->config->item('AUTH_GUEST')}
	AND
		/* ゲストはデフォルトで講座情報テーブルIDが0で登録されている */
		s.course_id = 0
	ORDER BY
		s.uid
	LIMIT 1),
	{$this->config->item('AUTH_GUEST')},
	NOW()
)
EOT;
		$_values = array($cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ゲストユーザー削除
	 * 指定講座のゲストユーザーを削除する
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function deleteGuest($cid)
	{
		$_sql =<<<EOT
DELETE FROM
	students
WHERE
	course_id = ?
AND
	authority = {$this->config->item('AUTH_GUEST')}
EOT;
		$_values = array($cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ゲスト登録済み判定
	 * 指定講座にゲストユーザー登録済みか判定
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return boolean true：ゲスト登録済み / false：ゲスト未登録
	 */
	public function hasGuest($cid)
	{
		$_sql =<<<EOT
SELECT
	id
FROM
	students AS s
WHERE
	s.course_id = ?
AND
	s.authority = {$this->config->item('AUTH_GUEST')}
EOT;
		$_values = array($cid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 権限取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param string $uid uid
	 * @return int 権限
	 */
	public function getCourseAuth($cid, $uid) {
		// 管理者以外
		$_sql =<<<EOT
			SELECT
				`authority`
			FROM
				`students`
			WHERE
				`course_id` = ?
			AND
				`uid` = ?
EOT;
		$_values = array($cid, $uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$tmp = $_query->result('array')[0];
			return $tmp['authority'];
		}

		// 管理者の場合
		$_sql =<<<EOT
			SELECT
				`id`
			FROM
				`students`
			WHERE
				`uid` = ?
			AND
				`authority` = ?
EOT;
		$_values = array($uid, $this->config->item('AUTH_SYS_MANAGER'));
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $this->config->item('AUTH_SYS_MANAGER');
		} else {
			return null;
		}
	}

	/**
	 * 講座登録されているか判定
	 *
	 * @param int $courseid 講座情報テーブルID
	 * @param int $uid ユーザーテーブルID
	 */
	public function getAuthority($courseid, $uid)
	{
		$_sql =<<<EOT
SELECT authority FROM students WHERE course_id=? AND uid=?
EOT;
		$_values = array($courseid, $uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('object')[0]->authority;
		} else {
			return false;
		}
	}

	/**
	 * 受講者情報取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param string $uid uid
	 * @return array 受講者情報
	 */
	public function getStudentsInfo($cid, $uid) {
		// 管理者以外
		$_sql =<<<EOT
			SELECT
				s.`authority` AS authority,
				u.`chat_use` AS chat_use
			FROM
				`students` AS s
			INNER JOIN
				`user` AS u
			ON (
				s.`uid` = u.id
			)
			WHERE
				s.`course_id` = ?
			AND
				s.`uid` = ?
EOT;
		$_values = array($cid, $uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('array')[0];
		}

		// 管理者の場合
		$_sql =<<<EOT
			SELECT
				s.`authority` AS authority,
				u.`chat_use` AS chat_use
			FROM
				`students` AS s
			INNER JOIN
				`user` AS u
			ON (
				s.`uid` = u.id
			)
			WHERE
				s.`uid` = ?
			AND
				s.`authority` = {$this->config->item('AUTH_SYS_MANAGER')}
			ORDER BY
				u.`id`
EOT;
		$_values = array($uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('array')[0];
		} else {
			return null;
		}
	}

	/**
	 * 指定講座IDの受講者数
	 *
	 * @param int $cid テーマテーブルID
	 * @return int 受講者数
	 */
	public function getCountOfCourse($cid) {
		// 受講者数にゲストは含めない。
		// 含めてしまうと講座画面の受講済ラベルの表示判定処理に影響が出てしまう。
		$_sql =<<<EOT
SELECT
	COUNT(s.id) AS cnt
FROM
	students AS s
INNER JOIN
	user AS u
ON
	u.id = s.uid
WHERE
	s.course_id = ?
AND
	u.validityflg = 1
AND
	s.authority = {$this->config->item('AUTH_STUDENT')}
EOT;
		$_values = array($cid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			return $_result[0]->cnt;
		} else {
			return 0;
		}
	}

	/**
	 * 指定講座のゲスト、講師、運用管理者コピー
	 *
	 * @param int $src_cid コピー元講座情報テーブルID
	 * @param int $dest_cid コピー先講座情報テーブルID
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function copyOfCourse($src_cid, $dest_cid) {
		$_sql =<<<EOT
INSERT INTO students
(
	course_id,
	uid,
	authority
)
SELECT
	? AS course_id,
	uid,
	authority
FROM
	students
WHERE
	course_id = ?
AND
	authority IN ({$this->config->item('AUTH_GUEST')}, {$this->config->item('AUTH_TEACHER')}, {$this->config->item('AUTH_OP_MANAGER')})
ORDER BY id
EOT;
		$_values = array($dest_cid, $src_cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

}
