<?php
/*
 * ユーザーモデル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

class User_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * ユーザー一覧取得
	 *
	 * @param int $page 現在表示しているページ
	 * @param int $limit 取得件数
	 * @param string $userid ユーザーID
	 * @param string $surname 姓
	 * @param string $firstname 名
	 * @return object ユーザー一覧
	 */
	public function get_list($page, $limit, $userid=null, $surname=null, $firstname=null)
	{
		$_sql =<<<EOT
SELECT
	u.id,
	u.userid,
	u.surname,
	u.firstname,
	u.nickname,
	u.picture_file,
	u.validityflg,
	DATE_FORMAT(u.login_at, '%Y/%m/%d %H:%i') AS login_at
FROM user AS u
LEFT JOIN students AS s
ON
	s.uid = u.id
AND
	(
		s.authority = {$this->config->item('AUTH_GUEST')}
	OR
		s.authority = {$this->config->item('AUTH_SYS_MANAGER')}
	)
WHERE
	s.id IS NULL
EOT;
		$_values = array();
		$_where_list = array();
		if (!is_null($userid) && $userid !== "") {
			$_where_list[] = "u.userid LIKE ?";
			$_values[] = $this->db->escape_like_str($userid)."%";
		}
		if (!is_null($surname) && $surname !== "") {
			$_where_list[] = "u.surname LIKE ?";
			$_values[] = "%".$this->db->escape_like_str($surname)."%";
		}
		if (!is_null($firstname) && $firstname !== "") {
			$_where_list[] = "u.firstname LIKE ?";
			$_values[] = "%".$this->db->escape_like_str($firstname)."%";
		}

		foreach ($_where_list as $_where) {
			$_sql .= ' AND '.$_where;
		}

		$_sql .= " ORDER BY u.id";

		$_sql .= " LIMIT ? OFFSET ?";
		$offset = $limit * ($page - 1);
		array_push($_values, $limit, $offset);

		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * ユーザー件数取得
	 *
	 * @param int $page 現在表示しているページ
	 * @param int $limit 取得件数
	 * @param string $userid ユーザーID
	 * @param string $surname 姓
	 * @param string $firstname 名
	 * @return object ユーザー件数
	 */
	public function get_count($userid=null, $surname=null, $firstname=null)
	{
		$_sql =<<<EOT
SELECT
	COUNT(u.id) AS cnt
FROM user AS u
LEFT JOIN students AS s
ON
	s.uid = u.id
AND
	(
		s.authority = {$this->config->item('AUTH_GUEST')}
	OR
		s.authority = {$this->config->item('AUTH_SYS_MANAGER')}
	)
WHERE
	s.id IS NULL
EOT;
		$_values = array();
		$_where_list = array();
		if (!is_null($userid) && $userid !== "") {
			$_where_list[] = "u.userid LIKE ?";
			$_values[] = $this->db->escape_like_str($userid)."%";
		}
		if (!is_null($surname) && $surname !== "") {
			$_where_list[] = "u.surname LIKE ?";
			$_values[] = "%".$this->db->escape_like_str($surname)."%";
		}
		if (!is_null($firstname) && $firstname !== "") {
			$_where_list[] = "u.firstname LIKE ?";
			$_values[] = "%".$this->db->escape_like_str($firstname)."%";
		}

		foreach ($_where_list as $_where) {
			$_sql .= ' AND '.$_where;
		}

		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result[0]->cnt;
		} else {
			return null;
		}
	}

	/**
	 * ユーザー情報取得
	 *
	 * @param int $id ユーザーテーブルID
	 * @return object ユーザー情報
	 */
	public function getData($id)
	{
		$_profile_default_image = $this->commonlib->baseUrl().$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
		$_profile_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=";

		$_sql =<<<EOT
SELECT
	u.userid,
	u.surname,
	u.firstname,
	u.nickname,
	(CASE WHEN u.picture_file IS NULL THEN '{$_profile_default_image}' ELSE concat('{$_profile_save_url}', u.id) END) AS picture_file,
	u.introduction,
	u.chat_use
FROM
	user AS u
WHERE
	u.id = ?
EOT;
		$_values = array($id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result[0];
		} else {
			return null;
		}
	}

	/**
	 * ユーザーIDが登録されているか判定
	 *
	 * @param int $uid ユーザーテーブルID
	 * @param string $userid ユーザーID
	 * @return boolean true:登録済み / false:未登録
	 */
	public function hasUserid($uid, $userid)
	{
		$_sql =<<<EOT
SELECT id FROM user WHERE userid=?
EOT;
		$_values = array($userid);
		if ($uid) {
			// idの指定がある場合は指定のid以外を検索（自分以外を検索）
			$_sql .= " AND id <> ?";
			$_values[] = $uid;
		}
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ニックネームが登録されているか判定
	 *
	 * @param int $uid ユーザーテーブルID
	 * @param string $nickname ニックネーム
	 * @return boolean true:登録済み / false:未登録
	 */
	public function hasNickname($uid, $nickname)
	{
		$_sql =<<<EOT
SELECT id FROM user WHERE nickname=?
EOT;
		$_values = array($nickname);
		if ($uid) {
			// idの指定がある場合は指定のid以外を検索（自分以外を検索）
			$_sql .= " AND id <> ?";
			$_values[] = $uid;
		}
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 登録
	 *
	 * @param string $userid ユーザーID
	 * @param string $passwd パスワード
	 * @param string $surname 姓
	 * @param string $firstname 名
	 * @param string $nickname ニックネーム
	 * @param string $picture_file 画像ファイルパス
	 * @param boolean $is_update 更新フラグ（true:重複データを更新する）
	 * @return mixed true:INSERTしたレコードのID / false:処理失敗
	 */
	public function regist($userid, $passwd, $surname, $firstname, $nickname, $picture_file, $is_update=false)
	{
		$_sql =<<<EOT
INSERT INTO user
	(userid, passwd, validityflg, surname, firstname, nickname, picture_file, chat_use)
VALUES
	(?, SHA2(?, 0), 1, ?, ?, ?, ?, 0)
EOT;
		$_values = array($userid, $passwd, $surname, $firstname, $nickname, $picture_file);

		if ($is_update) {
			$_sql .=<<<EOT
ON DUPLICATE KEY UPDATE
	userid = ?,
	passwd = SHA2(?, 0),
	surname = ?,
	firstname = ?,
	nickname = ?,
	updated_at = NOW()
EOT;
			array_push($_values, $userid, $passwd, $surname, $firstname, $nickname);
		}
		if ($this->db->query($_sql, $_values)) {
			if ($is_update) {
				return true;
			}
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	/**
	 * 更新
	 *
	 * @param int $uid ユーザーテーブルID
	 * @param string $userid ユーザーID
	 * @param string $passwd パスワード
	 * @param string $surname 姓
	 * @param string $firstname 名
	 * @param string $nickname ニックネーム
	 * @param string $picture_file 画像ファイルパス
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function update($uid, $userid, $passwd, $surname, $firstname, $nickname, $picture_file)
	{
		$_sql =<<<EOT
UPDATE user SET userid = ?,
				surname = ?,
				firstname = ?,
				nickname = ?,
				updated_at = NOW()
EOT;
		$_values = array($userid, $surname, $firstname, $nickname);
		if ($passwd) {
			// パスワードの入力がある場合は更新
			$_sql .= ", passwd = SHA2(?, 0)";
			$_values[] = $passwd;
		}
		if ($picture_file) {
			// 画像ファイルの指定がある場合は更新
			$_sql .= ", picture_file = ?";
			$_values[] = $picture_file;
		}
		$_sql .= " WHERE id = ?";
		$_values[] = $uid;

		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 画像ファイル更新
	 *
	 * @param string $picture_file 画像ファイルパス
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function updatePictureFile($picture_file, $uid)
	{
		$_sql =<<<EOT
UPDATE user SET picture_file = ?
WHERE id = ?
EOT;
		$_values = array($picture_file, $uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 停止
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function stop($uid)
	{
		$_sql =<<<EOT
UPDATE user SET validityflg = 0, updated_at = NOW()
WHERE id = ?
EOT;
		$_values = array($uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 再開
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function restart($uid)
	{
		$_sql =<<<EOT
UPDATE user SET validityflg = 1, updated_at = NOW()
WHERE id = ?
EOT;
		$_values = array($uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 指定ユーザー画像ファイル取得
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return string 指定ユーザー画像ファイル
	 */
	public function getImageFile($uid)
	{
		$_sql =<<<EOT
SELECT
	picture_file
FROM
	user
WHERE
	id = ?
AND
	picture_file IS NOT NULL
EOT;
		$_values = array($uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object')[0]->picture_file;
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * 削除
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function delete($uid)
	{
		$_sql =<<<EOT
DELETE FROM user WHERE id = ?
EOT;
		$_values = array($uid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ダウンロード用ユーザー一覧取得
	 *
	 * @param string $userid ユーザーID
	 * @param string $surname 姓
	 * @param string $firstname 名
	 * @return array ユーザー一覧
	 */
	public function get_list_for_download($userid=null, $surname=null, $firstname=null)
	{
		$_sql =<<<EOT
SELECT
	u.userid,
	u.surname,
	u.firstname,
    u.nickname,
	u.validityflg,
	DATE_FORMAT(u.login_at, '%Y/%m/%d %H:%i') AS login_at
FROM user AS u
LEFT JOIN students AS s
ON
	s.uid = u.id
AND
	(
		s.authority = {$this->config->item('AUTH_GUEST')}
	OR
		s.authority = {$this->config->item('AUTH_SYS_MANAGER')}
	)
WHERE
	s.id IS NULL
EOT;
		$_values = array();
		$_where_list = array();
		if (!is_null($userid) && $userid !== "") {
			$_where_list[] = "u.userid LIKE ?";
			$_values[] = $this->db->escape_like_str($userid)."%";
		}
		if (!is_null($surname) && $surname !== "") {
			$_where_list[] = "u.surname LIKE ?";
			$_values[] = "%".$this->db->escape_like_str($surname)."%";
		}
		if (!is_null($firstname) && $firstname !== "") {
			$_where_list[] = "u.firstname LIKE ?";
			$_values[] = "%".$this->db->escape_like_str($firstname)."%";
		}

		foreach ($_where_list as $_where) {
			$_sql .= ' AND '.$_where;
		}

		$_sql .= " ORDER BY u.id";

		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('array');
			return $_result;
		} else {
			return null;
		}
	}

}