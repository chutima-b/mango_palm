<?php
/*
 * 日記一覧モデル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

class Diary_List_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 日記参加者一覧取得
	 *
	 * @param int $page 現在表示しているページ
	 * @param int $limit 取得件数
	 * @param int $course_id 講座テーブルID
	 * @return array 日記参加者一覧
	 */
		public function getDiaryList($page, $limit, $course_id) {

		$authority = $this->session->userdata['authority'];
		$uid = $authority[0]->id;
		$_offset = $limit * ($page - 1);

		$sql =<<< EOF
			SELECT
				st.`uid`,
				st.`course_id`,
				st.`authority`,
				ur.`nickname`,
				ur.`picture_file`,
				ur.`introduction`,
				dh.`id` AS hid,
				dh.`hide_uid`,
				(CASE WHEN dh.`hide_uid` IS null THEN 1 ELSE 0 END) AS on_flg
			FROM
				`students` AS st
			INNER JOIN
				`user` AS ur
			ON (
			    st.`uid` = ur.`id`
			AND
				ur.`validityflg` = 1
			)
			LEFT JOIN
				`diary_hide_info` AS dh
			ON (
			    st.`course_id` = dh.`course_id`
			AND
			    st.`uid` = dh.`hide_uid`
			AND
			    dh.`uid` = ?
			)
			WHERE
				st.`course_id` = ?
			AND
				st.`uid` != ?
			AND
				st.`authority` IN ({$this->config->item('AUTH_STUDENT')}, {$this->config->item('AUTH_TEACHER')})
			ORDER BY
				ur.`id`
			LIMIT {$limit} OFFSET {$_offset}
EOF;

		$_values = array($uid, $course_id, $uid);
		if ($query = $this->db->query($sql, $_values)) {
			return $query->result('array');
		}
		return null;
	}

	/**
	 * 日記参加者数取得
	 *
	 * @param int $course_id 講座テーブルID
	 * @return int 日記参加者数
	 */
		public function getTotalCount($course_id) {

		$_authority = $this->session->userdata('authority');
		$_uid = $_authority[0]->id;

		$_sql =<<< EOF
			SELECT
				COUNT(st.`id`) AS cnt
			FROM
				`students` AS st
			INNER JOIN
				`user` AS ur
			ON (
			    st.`uid` = ur.`id`
			AND
				ur.`validityflg` = 1
			)
			LEFT JOIN
				`diary_hide_info` AS dh
			ON (
			    st.`course_id` = dh.`course_id`
			AND
			    st.`uid` = dh.`hide_uid`
			AND
			    dh.`uid` = ?
			)
			WHERE
				st.`course_id` = ?
			AND
				st.`uid` != ?
			AND
				st.`authority` IN ({$this->config->item('AUTH_STUDENT')}, {$this->config->item('AUTH_TEACHER')})
EOF;

		$_values = array($_uid, $course_id, $_uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('object')[0]->cnt;
		}
		return 0;
	}

	/*
	 * 日記一覧非表示データ削除
	 */
	public function deleteHide($hid)
	{
		$_values = array($hid);

		$sql =<<<EOF
			DELETE
			FROM
			  `diary_hide_info`
			WHERE
			  `id` = ?
EOF;
		$this->db->query($sql, $_values);
	}

	/*
	 * 日記一覧非表示データ登録
	 */
	public function registHide($cid, $hideid)
	{
		$authority = $this->session->userdata['authority'];
		$uid = $authority[0]->id;

		$sql =<<<EOF
			INSERT INTO
				`diary_hide_info`
			(
				`course_id`,
				`uid`,
				`hide_uid`,
				`created_at`
			)
			VALUES
			(
				?,
				?,
				?,
				CURRENT_TIMESTAMP
			)
EOF;
		$_values = array($cid, $uid, $hideid);
		$this->db->query($sql, $_values);
	}

}