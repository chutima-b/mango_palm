<?php
/*
 * チャット一覧画面モデル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Chatlist_Model extends CI_Model {

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 指定講座IDのユーザー情報取得
	 *
	 * @param int $page 現在表示しているページ
	 * @param int $limit 取得件数
	 * @param int $cid 講座情報テーブルID
	 * @param int $uid ユーザーテーブルID
	 * @return object ユーザー情報
	 */
	public function getChatUserList($page, $limit, $cid, $uid)
	{
		$_offset = $limit * ($page - 1);
		$_sql =<<<EOT
SELECT
    s.uid,
    s.authority,
    u.picture_file,
    u.nickname,
    u.introduction
FROM
    students AS s
INNER JOIN
    user AS u
ON
    u.id = s.uid
AND
    u.chat_use = 1
AND
   u.validityflg = 1
WHERE
    s.course_id = ?
AND
    s.uid <> ?
AND
    s.authority IN ({$this->config->item('AUTH_STUDENT')}, {$this->config->item('AUTH_TEACHER')})
ORDER BY
    u.id
LIMIT {$limit} OFFSET {$_offset}
EOT;
		$_values = array($cid, $uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query
		 && $_query->num_rows() > 0) {
			return $_query->result('object');
		} else {
			return null;
		}
	}

	/**
	 * チャット一覧総件数取得
	 *
	 * @param int $cid 講座情報テーブルID
	 * @param int $uid ユーザーテーブルID
	 * @return int チャット一覧総件数
	 */
	public function getTotalCount($cid, $uid) {
		$_sql =<<< EOF
SELECT
    COUNT(s.id) AS cnt
FROM
    students AS s
INNER JOIN
    user AS u
ON
    u.id = s.uid
AND
    u.chat_use = 1
AND
   u.validityflg = 1
WHERE
    s.course_id = ?
AND
    s.uid <> ?
AND
    s.authority IN ({$this->config->item('AUTH_STUDENT')}, {$this->config->item('AUTH_TEACHER')})
EOF;

		$_values = array($cid, $uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('object')[0]->cnt;
		}
		return 0;
	}

}
