<?php
/*
 * 講座情報モデル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

class Course_Model extends CI_Model {
	/** アイコン画像URL. */
	private $icon_save_url;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();
		$this->icon_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_GENRE')."&id=";
	}

	/**
	 * システム管理（ジャンル）用全コース数取得
	 *
	 * @return int 全コース件数
	 */
	public function getAllCourseCount() {
		$_sql = "SELECT id FROM `course`";
		if ($query = $this->db->query($_sql, null)) {
			// 成功処理
			return $query->num_rows();
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * 受講者管理画面用講座情報取得
	 *
	 * @param int $cid 講座ID
	 * @return array 受講者管理画面用講座情報
	 */
	public function getCourseInfo($cid) {

		$sql =<<< EOF
			SELECT
				c.`course_code`,
				c.`course_name`,
                g.`genrename`,
                (CASE WHEN u.`surname` IS null THEN "" ELSE u.`surname` END) AS surname,
                (CASE WHEN u.`firstname` IS null THEN "" ELSE u.`firstname` END) AS firstname,
                (CASE WHEN u.`nickname` IS null THEN "" ELSE u.`nickname` END) AS nickname,
				u.`id`,
                (CASE WHEN u.`picture_file` IS null THEN "" ELSE u.`picture_file` END) AS picture_file,
				c.`schedule_use`,
				(DATE_FORMAT(c.`start_date`, '%Y/%m/%d')) AS start_date,
				(DATE_FORMAT(c.`end_date`, '%Y/%m/%d')) AS end_date
			FROM
				`course` AS c
            INNER JOIN
            	`genre` AS g
            ON (
                c.`genre` = g.`id`
            )
			LEFT JOIN
				`students` AS s
            ON (
                c.`id` = s.`course_id`
            AND
                s.`authority` = {$this->config->item('AUTH_TEACHER')}
            )
            LEFT JOIN
            	`user` AS u
            ON (
                s.uid = u.id
            )
			WHERE
				c.`id` = ?
EOF;
		$_values = array($cid);
		$query = $this->db->query($sql, $_values);
		if ($query->num_rows() > 0) {
			// 成功処理
			return $query->result('array');
		} else {
			// 失敗処理
			return null;
		}
	}

	/**
	 * ホーム画面（受講者）用コース一覧取得
	 *
	 * @return array 講座一覧情報
	 */
	public function  getStudentsHomeCourseList() {

		$authority = $this->session->userdata['authority'];

		if ($this->config->item('AUTH_SYS_MANAGER') == $authority[0]->authority) {

			$sql =<<< EOF
				SELECT
					c.`id`,
					c.`course_name`,
					(CASE WHEN g.icon IS NULL THEN NULL ELSE concat('{$this->icon_save_url}', g.id) END) AS icon
				FROM
					`course` AS c
				INNER JOIN `genre` AS g
				ON
					g.`id` = c.`genre`
				ORDER BY
					g.`genreid`,
					c.`course_code`
EOF;

			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				// 成功処理
				return $query->result('array');
			} else {
				// 失敗処理
				return null;
			}

		} else {
			$uid = $authority[0]->id;

			$sql =<<< EOF
				SELECT
					C.`id` AS id,
					C.`course_name` AS course_name,
					(CASE WHEN G.icon IS NULL THEN NULL ELSE concat('{$this->icon_save_url}', G.id) END) AS icon
				FROM
					`user` AS U
				INNER JOIN
					`students` AS S
				ON
					(U.`id` = S.`uid`)
				INNER JOIN
					`course` AS C
				ON
					(S.course_id = C.id)
				INNER JOIN `genre` AS G
				ON
					G.`id` = C.`genre`
				WHERE
					U.`id` = ?
				ORDER BY
					G.`genreid`,
					C.`course_code`
EOF;

			$_values = array($uid);
			$query = $this->db->query($sql, $_values);
			if ($query->num_rows() > 0) {
				// 成功処理
				return $query->result('array');
			} else {
				// 失敗処理
				return null;
			}
		}
	}

	/**
	 * 指定講座IDの講座情報および講師の情報取得
	 *
	 * @param int $id 講座ID
	 * @return object 講座情報
	 */
	public function getCourse($id) {
		$this->lang->load('courseedit_lang');
		$_profile_default_name = $this->lang->line('courseedit_unregistered');
		$_profile_default_image = $this->commonlib->baseUrl().$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
		$_profile_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=";

		$_sql =<<<EOT
SELECT
    c.id,
    c.course_code,
    c.course_name,
    c.genre,
    (CASE WHEN g.icon IS NULL THEN NULL ELSE concat('{$this->icon_save_url}', g.id) END) AS icon,
    /* 講師姓 */
    u.surname AS teacher_surname,
    /* 講師名 */
    u.firstname AS teacher_firstname,
    /* 講師写真 */
    (CASE WHEN u.picture_file IS NULL THEN '{$_profile_default_image}' ELSE concat('{$_profile_save_url}', u.id) END) AS picture_file,
    /* 運用管理者姓 */
    u2.surname AS op_mng_surname,
    /* 運用管理者名 */
    u2.firstname AS op_mng_firstname,
    /* 運用管理者写真 */
    (CASE WHEN u2.picture_file IS NULL THEN '{$_profile_default_image}' ELSE concat('{$_profile_save_url}', u2.id) END) AS op_mng_picture_file,
    c.course_description,
    c.forum_use,
    c.schedule_use,
    c.permitted_guest,
    (CASE WHEN c.start_date IS NULL THEN NULL ELSE DATE_FORMAT(c.start_date, '%Y/%m/%d') END) AS start_date,
    (CASE WHEN c.end_date IS NULL THEN NULL ELSE DATE_FORMAT(c.end_date, '%Y/%m/%d') END) AS end_date,
    c.calendar_color,
    c.created_at,
    c.updated_at
FROM course As c
INNER JOIN genre AS g
ON
    g.id = c.genre
LEFT JOIN students AS s
ON
    s.course_id = c.id
AND
    s.authority = {$this->config->item('AUTH_TEACHER')}
LEFT JOIN user AS u
ON
    u.id = s.uid
LEFT JOIN students AS s2
ON
    s2.course_id = c.id
AND
    s2.authority = {$this->config->item('AUTH_OP_MANAGER')}
LEFT JOIN user AS u2
ON
    u2.id = s2.uid
WHERE c.id=?
EOT;
		$_values = array($id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			// 結果取得
			$_result = $_query->result('object');
			// 講師フルネーム生成
			$_result[0]->teacher_name = $_profile_default_name;
			if (!is_null($_result[0]->teacher_surname)
			 && !is_null($_result[0]->teacher_firstname)) {
				$_result[0]->teacher_name = $this->commonlib->createFullName(
						$_result[0]->teacher_firstname, $_result[0]->teacher_surname);
			}
			// 運用管理者フルネーム生成
			$_result[0]->op_mng_name = $_profile_default_name;
			if (!is_null($_result[0]->op_mng_surname)
			 && !is_null($_result[0]->op_mng_firstname)) {
				$_result[0]->op_mng_name = $this->commonlib->createFullName(
						$_result[0]->op_mng_firstname, $_result[0]->op_mng_surname);
			}
			// 講座情報返却
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * 指定講座IDの講座情報取得
	 *
	 * @param int $id 講座ID
	 * @return string 講座情報
	 */
	public function getCourseName($id) {
		$_sql =<<<EOT
SELECT course_name FROM course WHERE id=?
EOT;
		$_values = array($id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object')[0];
			return $_result->course_name;
		} else {
			return null;
		}
	}

	/**
	 * 講座コードが登録されているか判定
	 *
	 * @param int $id 講座情報テーブルID
	 * @param string $course_code 講座コード
	 * @return boolean true:登録済み / false:未登録
	 */
	public function hasCourseCode($cid, $course_code)
	{
		$_sql =<<<EOT
SELECT id FROM course WHERE id <> ? AND course_code = ?
EOT;
		$_values = array($cid, $course_code);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ホーム画面（管理者）用講座件数取得
	 *
	 * @return
	 */
	public function getStaffHomeCourseCount() {

		$authority = $this->session->userdata['authority'];

		if ($this->config->item('AUTH_SYS_MANAGER') == $authority[0]->authority) {

			$sql =<<< EOF
				SELECT
					COUNT(`id`) AS cnt
				FROM
					`course`
EOF;

			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				$ret = $query->result('array')[0];
				// 成功処理
				return $ret;
			} else {
				// 失敗処理
				return null;
			}

		} else {
			$sql =<<< EOF
				SELECT
					COUNT(c.`id`) AS cnt
				FROM
					`students` AS s
				INNER JOIN
					`course` AS c
				ON (
					s.`course_id` = c.`id`
				)
				WHERE
					s.`uid` = ?
EOF;

			$_values = array($authority[0]->id);
			$query = $this->db->query($sql, $_values);
			if ($query->num_rows() > 0) {
				$ret = $query->result('array')[0];
				// 成功処理
				return $ret;
			} else {
				// 失敗処理
				return null;
			}
		}
	}

	/**
	 * ホーム画面（管理者）用ジャンル別講座件数取得
	 *
	 * @return
	 */
	public function getStaffHomeGenreCourseCount() {

		$authority = $this->session->userdata['authority'];

		if ($this->config->item('AUTH_SYS_MANAGER') == $authority[0]->authority) {

			$sql =<<< EOF
				SELECT
	            	g.`id`,
				    g.`genrename`,
					(CASE WHEN g.icon IS NULL THEN NULL ELSE concat('{$this->icon_save_url}', g.id) END) AS icon,
					COUNT(c.`id`) AS cnt
				FROM
					`genre` AS g
				INNER JOIN
					`course` AS c
				ON (
				    g.`id` = c.`genre`
				)
	            GROUP BY
	            	g.`id`,
	            	g.`genrename`
				ORDER BY
					g.`genreid`
EOF;

			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				// 成功処理
				return $query->result('array');
			} else {
				// 失敗処理
				return null;
			}
		} else {

			$sql =<<< EOF
				SELECT
	            	g.`id`,
				    g.`genrename`,
				    (CASE WHEN g.icon IS NULL THEN NULL ELSE concat('{$this->icon_save_url}', g.id) END) AS icon,
					COUNT(c.`id`) AS cnt
				FROM
					`students` AS s
				INNER JOIN
					`course` AS c
				ON (
					s.`course_id` = c.`id`
				)
				INNER JOIN
					`genre` AS g
				ON (
				    c.`genre` = g.`id`
				)
				WHERE
					s.`uid` = ?
				AND
					s.`authority` IN ({$this->config->item('AUTH_TEACHER')}, {$this->config->item('AUTH_OP_MANAGER')})
	            GROUP BY
	            	g.`id`,
	            	g.`genrename`
				ORDER BY
					g.`genreid`
EOF;

			$_values = array($authority[0]->id);
			$query = $this->db->query($sql, $_values);
			if ($query->num_rows() > 0) {
				// 成功処理
				return $query->result('array');
			} else {
				// 失敗処理
				return null;
			}
		}
	}

	/**
	 * ホーム画面（管理者）用コース一覧取得
	 *
	 * @return array 講座一覧情報
	 */
	public function getStaffHomeCourseList() {

		$authority = $this->session->userdata['authority'];
		$id = $authority[0]->id;

		if ($this->config->item('AUTH_SYS_MANAGER') == $authority[0]->authority) {

			$sql =<<< EOF
				SELECT
					c.`id`,
					c.`course_code`,
					c.`course_name`,
					c.`genre`,
					l.`nickname`,
					l.`surname`,
					l.`firstname`,
					(CASE WHEN ccnt.`ccnt` is null THEN 0 ELSE ccnt.`ccnt` END) AS ccnt,
					(CASE WHEN ucnt.`ucnt` is null THEN 0 ELSE ucnt.`ucnt` END) AS ucnt,
					c.`schedule_use`,
					(CASE WHEN c.`start_date` IS NULL THEN NULL ELSE DATE_FORMAT(c.`start_date`, '%Y/%m/%d') END) AS start_date,
					(CASE WHEN c.`end_date` IS NULL THEN NULL ELSE DATE_FORMAT(c.`end_date`, '%Y/%m/%d') END) AS end_date,
					{$this->config->item('AUTH_SYS_MANAGER')} AS authority
				FROM
					`course` AS c
				INNER JOIN
					`genre` AS g
				ON (
				    c.`genre` = g.`id`
				)
				LEFT JOIN
					(
						SELECT
							s.`course_id`,
						    u.`nickname`,
						    u.`surname`,
						    u.`firstname`
						FROM
							`students` AS s
						INNER JOIN
							`user` AS u
						ON (
						    s.`uid` = u.`id`
						)
						WHERE
							s.`authority` = {$this->config->item('AUTH_TEACHER')}
					) AS l
				ON (
				    c.`id` = l.`course_id`
				)
				LEFT JOIN
					(
						SELECT
							`course_id`,
							COUNT(`name`) AS ccnt
						FROM
							`theme`
						GROUP BY
							`course_id`
					) AS ccnt
				ON (
					c.`id` = ccnt.`course_id`
				)
				LEFT JOIN
					(
				        SELECT
				        	`course_id`,
				        	COUNT(*) AS ucnt
				        FROM
				        	`students`
				        WHERE
				        	`authority` = {$this->config->item('AUTH_STUDENT')}
						GROUP BY
							`course_id`
					) AS ucnt
				ON (
				    c.`id` = ucnt.`course_id`
				)
				ORDER BY
					g.`genreid`,
					c.`course_code`
EOF;
			$_values = array($id);
			$query = $this->db->query($sql, $_values);
			if ($query->num_rows() > 0) {
				// 成功処理
				return $query->result('array');
			} else {
				// 失敗処理
				return null;
			}
		} else {

			$sql =<<< EOF
				SELECT
					c.`id`,
					c.`course_code`,
					c.`course_name`,
					c.`genre`,
					l.`nickname`,
					l.`surname`,
					l.`firstname`,
					(CASE WHEN ccnt.`ccnt` is null THEN 0 ELSE ccnt.`ccnt` END) AS ccnt,
					(CASE WHEN ucnt.`ucnt` is null THEN 0 ELSE ucnt.`ucnt` END) AS ucnt,
					(CASE WHEN fcnt.`fcnt` is null THEN 0 ELSE fcnt.`fcnt` END) AS fcnt,
					c.`schedule_use`,
					(CASE WHEN c.`start_date` IS NULL THEN NULL ELSE DATE_FORMAT(c.`start_date`, '%Y/%m/%d') END) AS start_date,
					(CASE WHEN c.`end_date` IS NULL THEN NULL ELSE DATE_FORMAT(c.`end_date`, '%Y/%m/%d') END) AS end_date,
					s.`authority`
				FROM
					`students` AS s
				INNER JOIN
					`course` AS c
				ON (
					s.`course_id` = c.`id`
				)
				INNER JOIN
					`genre` AS g
				ON (
				    c.`genre` = g.`id`
				)
				LEFT JOIN
					(
						SELECT
							s.`course_id`,
						    u.`nickname`,
						    u.`surname`,
						    u.`firstname`
						FROM
							`students` AS s
						INNER JOIN
							`user` AS u
						ON (
						    s.`uid` = u.`id`
						)
						WHERE
							s.`authority` = {$this->config->item('AUTH_TEACHER')}
					) AS l
				ON (
				    c.`id` = l.`course_id`
				)
				LEFT JOIN
					(
						SELECT
							`course_id`,
							COUNT(`name`) AS ccnt
						FROM
							`theme`
						GROUP BY
							`course_id`
					) AS ccnt
				ON (
					c.`id` = ccnt.`course_id`
				)
				LEFT JOIN
					(
				        SELECT
				        	`course_id`,
				        	COUNT(*) AS ucnt
				        FROM
				        	`students`
				        WHERE
				        	`authority` = {$this->config->item('AUTH_STUDENT')}
						GROUP BY
							`course_id`
					) AS ucnt
				ON (
				    c.`id` = ucnt.course_id
				)
				LEFT JOIN
					(
						SELECT
							`course_id`,
						    COUNT(*) AS fcnt
						FROM
						    `task_result`
						WHERE
						    `feedback_points` IS NULL
						GROUP BY
							`course_id`
					) AS fcnt
				ON (
				    c.`id` = fcnt.course_id
				)
				WHERE
					s.`uid` = ?
				AND
					s.`authority` IN ({$this->config->item('AUTH_TEACHER')}, {$this->config->item('AUTH_OP_MANAGER')})
				ORDER BY
					g.`genreid`,
					c.`course_code`
EOF;

			$_values = array($id);
			$query = $this->db->query($sql, $_values);
			if ($query->num_rows() > 0) {
				// 成功処理
				return $query->result('array');
			} else {
				// 失敗処理
				return null;
			}
		}
	}

	/**
	 * 講座申込画面（受講講座自己登録）用講座件数取得
	 *
	 * @return object 講座件数
	 */
	public function getCourseCountOfSelfRegistCourse() {
		$_sql =<<< EOF
SELECT
	COUNT(id) AS cnt
FROM
	course
EOF;

		$_query = $this->db->query($_sql);
		if ($_query->num_rows() > 0) {
			$ret = $_query->result('object')[0]->cnt;
			return $ret;
		}
		return null;
	}

	/**
	 * 講座申込画面（受講講座自己登録）用ジャンル別講座件数取得
	 *
	 * @return object ジャンル別講座件数
	 */
	public function getGenreCourseCountOfSelfRegistCourse() {
			$_sql =<<< EOF
SELECT
	g.id,
	g.genrename,
	(CASE WHEN g.icon IS NULL THEN NULL ELSE concat('{$this->icon_save_url}', g.id) END) AS icon,
	COUNT(c.id) AS cnt
FROM
	genre AS g
INNER JOIN
	course AS c
ON
	g.id = c.genre
AND
	/* 講座の期間を設定しない、または、講座の期間内の講座を取得 */
	(
		c.schedule_use = 0
	OR
		(
			c.schedule_use = 1
		AND
			(DATE_FORMAT(c.start_date, '%Y%m%d') <= (CURDATE() + 0))
		AND
			(DATE_FORMAT(c.end_date, '%Y%m%d') >= (CURDATE() + 0))
		)
	)
/* 講師が登録されている講座のみを取得 */
INNER JOIN
	students AS s
ON
	s.course_id = c.id
AND
	s.authority = {$this->config->item('AUTH_TEACHER')}
GROUP BY
	g.id,
	g.genrename
ORDER BY
	g.genreid
EOF;

		$_query = $this->db->query($_sql);
		if ($_query->num_rows() > 0) {
			return $_query->result('object');
		}
		return null;
	}

	/**
	 * 講座申込画面（受講講座自己登録）用講座一覧取得
	 *
	 * @return object 講座一覧情報
	 */
	public function getCourseListOfSelfRegistCourse() {
		$_profile_default_image = $this->commonlib->baseUrl().$this->config->item('PROFILE_DEFAULT_IMAGE_URL');
		$_profile_save_url = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=";

		$_authority = $this->session->userdata['authority'];
		$_uid = $_authority[0]->id;

		$_sql =<<< EOF
SELECT
	(CASE WHEN s.id IS NULL THEN 0 ELSE 1 END) AS is_registered,
	c.id,
	c.course_code,
	c.course_name,
	c.course_description,
	c.genre,
	l.surname AS t_surname,
	l.firstname AS t_firstname,
	(CASE WHEN l.picture_file IS NULL THEN '{$_profile_default_image}' ELSE concat('{$_profile_save_url}', l.uid) END) AS t_picture,
	(CASE WHEN th_cnt.cnt is null THEN 0 ELSE th_cnt.cnt END) AS th_cnt,
	(CASE WHEN u_cnt.cnt is null THEN 0 ELSE u_cnt.cnt END) AS u_cnt,
	c.schedule_use,
	(CASE WHEN c.start_date IS NULL THEN NULL ELSE DATE_FORMAT(c.start_date, '%Y/%m/%d') END) AS start_date,
	(CASE WHEN c.end_date IS NULL THEN NULL ELSE DATE_FORMAT(c.end_date, '%Y/%m/%d') END) AS end_date
FROM
	course AS c
INNER JOIN
	genre AS g
ON (
	c.genre = g.id
)
/* 講座登録済み情報 */
LEFT JOIN
	students AS s
ON
	s.course_id = c.id
AND
	s.uid = ?
/* 講師情報（講師が未登録の講座は一覧表示させない） */
INNER JOIN
	(
		SELECT
			u.id AS uid,
			s2.course_id,
			u.nickname,
			u.surname,
			u.firstname,
			u.picture_file
		FROM
			students AS s2
		INNER JOIN
			user AS u
		ON (
			s2.uid = u.id
		)
		WHERE
			s2.authority = {$this->config->item('AUTH_TEACHER')}
	) AS l
ON (
	c.id = l.course_id
)
/* 公開中かつ、テーマの期間が無いまたは、テーマの期間内のテーマ数 */
LEFT JOIN
	(
		SELECT
			course_id,
			COUNT(id) AS cnt
		FROM
			theme
		WHERE
			open = 1
		AND
			(
				(time_limit IS NULL OR time_limit = '0000-00-00 00:00:00')
			OR
				(
					theme_date IS NOT NULL AND DATE_FORMAT(theme_date, '%Y%m%d') <= (CURDATE() + 0)
				AND
					time_limit IS NOT NULL AND DATE_FORMAT(time_limit, '%Y%m%d') >= (CURDATE() + 0)
				)
			)
		GROUP BY
			course_id
	) AS th_cnt
ON (
	c.id = th_cnt.course_id
)
/* 受講者数 */
LEFT JOIN
	(
		SELECT
			course_id,
			COUNT(*) AS cnt
		FROM
			students
		WHERE
			authority = {$this->config->item('AUTH_STUDENT')}
		GROUP BY
			course_id
	) AS u_cnt
ON (
	c.id = u_cnt.course_id
)
WHERE
	/* 講座の期間を設定しない、または、講座の期間内の講座を取得 */
	(
		c.schedule_use = 0
	OR
		(
			c.schedule_use = 1
		AND
			(DATE_FORMAT(c.start_date, '%Y%m%d') <= (CURDATE() + 0))
		AND
			(DATE_FORMAT(c.end_date, '%Y%m%d') >= (CURDATE() + 0))
		)
	)
ORDER BY
	g.genreid,
	c.course_code
EOF;
		$_values = array($_uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			return $_query->result('object');
		}
		return null;
	}

	/**
	 * 指定ユーザーの受講している講座情報取得
	 *
	 * @param int $uid ユーザーテーブルID
	 * @return object 講座情報
	 */
	public function getCourseOfUser($uid) {
		$_sql =<<<EOT
SELECT course_name FROM course AS c
INNER JOIN students AS s
ON s.course_id = c.id
AND s.authority = {$this->config->item('AUTH_STUDENT')}
WHERE s.uid = ?
ORDER BY c.id
EOT;
		$_values = array($uid);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		} else {
			return null;
		}
	}

	/**
	 * お知らせ編集ダイアログ用講座情報取得
	 *
	 * @return object 講座情報
	 */
	public function getCourseListForNoticeCourse() {
		$authority = $this->session->userdata['authority'];
		$id = $authority[0]->id;

		if ($this->config->item('AUTH_SYS_MANAGER') == $authority[0]->authority) {
			$_sql =<<<EOT
SELECT
	c.id,
	c.course_name,
	c.genre
FROM
	course AS c
ORDER BY
	c.id
EOT;
			$_query = $this->db->query($_sql);
			if ($_query->num_rows() > 0) {
				$_result = $_query->result('object');
				return $_result;
			} else {
				return null;
			}
		} else {
			$_sql =<<<EOT
SELECT
	c.id,
	c.course_name,
	c.genre
FROM
	course AS c
INNER JOIN
	students AS s
ON
	s.course_id = c.id
AND
	s.authority = {$this->config->item('AUTH_OP_MANAGER')}
AND
	s.uid = ?
ORDER BY
	c.id
EOT;
			$_values = array($id);
			$_query = $this->db->query($_sql, $_values);
			if ($_query->num_rows() > 0) {
				$_result = $_query->result('object');
				return $_result;
			} else {
				return null;
			}
		}
	}

	/**
	 * 不要なWYSIWYG画像削除バッチ用講座情報取得
	 *
	 * @return object 講座情報
	 */
	public function getCourseListForBatchDeleteWYSIWYG() {
		$_sql =<<<EOT
SELECT id, course_description FROM course ORDER BY id
EOT;
		$_query = $this->db->query($_sql);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		}
		return null;
	}

	/**
	 * 不要なWYSIWYG画像削除バッチ用講座IDリスト取得（テーマ説明、課題の問題削除用）
	 *
	 * @return object 講座IDリスト
	 */
	public function getCourseIdListForBatchDeleteWYSIWYGOfThemeAndTask() {
		$_sql =<<<EOT
SELECT id FROM course ORDER BY id
EOT;
		$_query = $this->db->query($_sql);
		if ($_query->num_rows() > 0) {
			$_result = $_query->result('object');
			return $_result;
		}
		return null;
	}

	/**
	 * 指定講座IDのスケジュール利用フラグ取得
	 *
	 * @param int $id 講座ID
	 * @return boolean true：スケジュール利用する / false：スケジュール利用しない
	 */
	public function is_schedule_use($id) {
		$_sql = "SELECT schedule_use FROM course WHERE id = ?";
		$_values = array($id);
		$_query = $this->db->query($_sql, $_values);
		if ($_query->num_rows() == 1) {
			$_result = $_query->result('object');
			if ($_result[0]->schedule_use == 1) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 新規登録
	 *
	 * @return mixed true:INSERTしたレコードのID / false:処理失敗
	 */
	public function regist() {
		$_sql =<<<EOT
INSERT INTO course
(
	course_code,
	course_name,
	genre,
	course_description,
	forum_use,
	schedule_use,
	permitted_guest,
	start_date,
	end_date,
	calendar_color
)
VALUES
    (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
EOT;
		$_forum_use = 0;
		if (!is_null($this->input->post("courseedit_use_forum", true))) {
			$_forum_use = 1;
		}
		$_schedule_use = 0;
		$_calendar_color = null;
		if (!is_null($this->input->post("courseedit_use_schedule", true))) {
			$_schedule_use = 1;
			$_calendar_color = $this->input->post('courseedit_calendar_color', true);
		}
		$_permitted_guest = 0;
		if (!is_null($this->input->post("courseedit_permit_guest", true))) {
			$_permitted_guest = 1;
		}
		$_values = array(
				$this->input->post('courseedit_courseid', true),
				$this->input->post('courseedit_coursename', true),
				$this->input->post('courseedit_genre', true),
				// WYSIWYG対応のためCodeigniterではXSS対策は実施せずに、専用関数で除去する。
				$this->commonlib->removeXssForWYSIWYG($this->input->post('courseedit_description')),
				$_forum_use,
				$_schedule_use,
				$_permitted_guest,
				$this->input->post('courseedit_startdate', true),
				$this->input->post('courseedit_enddate', true),
				$_calendar_color
			);
		if ($this->db->query($_sql, $_values)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	/**
	 * 更新登録
	 *
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function update() {
		$_sql =<<<EOT
UPDATE course
    set course_code = ?,
	course_name = ?,
	genre = ?,
	course_description = ?,
	forum_use = ?,
	schedule_use = ?,
	permitted_guest = ?,
	start_date = ?,
	end_date = ?,
	calendar_color = ?,
	updated_at = NOW()
WHERE id = ?
EOT;
		$_forum_use = 0;
		if (!is_null($this->input->post("courseedit_use_forum"))) {
			$_forum_use = 1;
		}
		$_schedule_use = 0;
		$_calendar_color = null;
		if (!is_null($this->input->post("courseedit_use_schedule"))) {
			$_schedule_use = 1;
			$_calendar_color = $this->input->post('courseedit_calendar_color', true);
		}
		$_permitted_guest = 0;
		if (!is_null($this->input->post("courseedit_permit_guest", true))) {
			$_permitted_guest = 1;
		}
		$_values = array(
				$this->input->post('courseedit_courseid', true),
				$this->input->post('courseedit_coursename', true),
				$this->input->post('courseedit_genre', true),
				// WYSIWYG対応のためCodeigniterではXSS対策は実施せずに、専用関数で除去する。
				$this->commonlib->removeXssForWYSIWYG($this->input->post('courseedit_description')),
				$_forum_use,
				$_schedule_use,
				$_permitted_guest,
				$this->input->post('courseedit_startdate', true),
				$this->input->post('courseedit_enddate', true),
				$_calendar_color,
				$this->input->get('cid', true)
			);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * コピー登録
	 *
	 * 指定された講座をコピーする。
	 * この際、講座コード、講座名を差替え、開始日、終了日はクリアする。
	 *
	 * @param int $cid 講座情報テーブルID
	 * @return mixed true:INSERTしたレコードのID / false:処理失敗
	 */
	public function copy($cid) {
		$_sql =<<<EOT
INSERT INTO course
(
	course_code,
	course_name,
	genre,
	course_description,
	forum_use,
	schedule_use,
	permitted_guest
)
SELECT
	? AS course_code,
	? AS course_name,
	genre,
	course_description,
	forum_use,
	0 AS schedule_use,
	permitted_guest
FROM
	course
WHERE
	id = ?
EOT;
		$_values = array(
				$this->input->post("courseedit_copy_courceid", true),
				$this->input->post("courseedit_copy_courcename", true),
				$cid);
		if ($this->db->query($_sql, $_values)) {
			return $this->db->insert_id();
		} else {
			return false;
		}
	}

	/**
	 * 講座紹介画像差替え更新登録
	 *
	 * 講座情報テーブルコピー直後は、
	 * 講座紹介画像のURLが、コピー元講座用のURLになっているため、
	 * コピー先講座用のURLに変換して更新登録する
	 *
	 * @param int $src_cid コピー元講座情報テーブルID
	 * @param int $dest_cid コピー先講座情報テーブルID
	 * @return boolean true:処理成功 / false:処理失敗
	 */
	public function update_description_image($src_cid, $dest_cid) {
		$_search_str = 'displayfile?tbl='.$this->config->item('TABLE_KEY_COURSE').'&amp;id='.$src_cid.'&amp;fl=';
		$_replace_str = 'displayfile?tbl='.$this->config->item('TABLE_KEY_COURSE').'&amp;id='.$dest_cid.'&amp;fl=';
		$_sql =<<<EOT
UPDATE course
	set course_description = REPLACE(course_description, ?, ?),
	updated_at = NOW()
WHERE id = ?
EOT;
		$_values = array($_search_str, $_replace_str, $dest_cid);
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 削除
	 *
	 * @return boolean true：処理成功 / false：処理失敗
	 */
	public function delete() {
		$_sql = "DELETE FROM course WHERE id = ?";
		$_values = array($this->input->get('cid', true));
		if ($this->db->query($_sql, $_values)) {
			return true;
		} else {
			return false;
		}
	}

}