<?php
/*
 * 講座編集画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['courseedit_header_pagetitle'] = "Course Edit";
$lang['courseedit_pagetitle'] = "Course edit";
$lang['courseedit_courseid'] = "Course ID";
$lang['courseedit_coursename'] = "Course name";
$lang['courseedit_genre'] = "Curriculum";
$lang['courseedit_genre_default'] = "Please choose.";
$lang['courseedit_teachername'] = "Teacher";
$lang['courseedit_op_managername'] = "Operations manager";
$lang['courseedit_unregistered'] = "(Unregistered)";
$lang['courseedit_btn_regist_teacher'] = "Teacher regist";
$lang['courseedit_btn_regist_op_manager'] = "OP Manager regist";
$lang['courseedit_description'] = "Course Intro";
$lang['courseedit_use_forum'] = "Use forum";
$lang['courseedit_use_schedule'] = "Period of a course is set";
$lang['courseedit_permit_guest'] = "Guest's attendance is permitted.";
$lang['courseedit_startdate'] = "Start date";
$lang['courseedit_enddate'] = "End date";
$lang['courseedit_calendar_color'] = "Calendar";
$lang['courseedit_btn_copy'] = "Copy";
$lang['courseedit_btn_regist'] = "Submit";
$lang['courseedit_btn_delete'] = "Delete";
$lang['courseedit_btn_new_theme'] = "Add Theme";
$lang['courseedit_info_title_theme'] = "Theme";
$lang['courseedit_theme_label_close'] = "Close";
$lang['courseedit_theme_label_end'] = "Public end";
$lang['courseedit_theme_label_over_limit'] = "Expired";
$lang['courseedit_theme_label_attended'] = "Attended";
$lang['courseedit_err_msg_courseid_required'] = "Please input the Course ID.";
$lang['courseedit_err_msg_courseid_maxlength'] = "Please input within 50 character.";
$lang['courseedit_err_msg_coursename_required'] = "Please input a Course name.";
$lang['courseedit_err_msg_coursename_maxlength'] = "Please input within 50 character.";
$lang['courseedit_err_msg_genre_required'] = "Please choose the curriculum.";
$lang['courseedit_err_msg_description_required'] = "Please input course introduction.";
$lang['courseedit_err_msg_startdate_required'] = "Please input starting date.";
$lang['courseedit_err_msg_enddate_required'] = "Please input the end date.";
$lang['courseedit_err_msg_copy_courseid_required'] = $lang['courseedit_err_msg_courseid_required'];
$lang['courseedit_err_msg_copy_courseid_maxlength'] = $lang['courseedit_err_msg_courseid_maxlength'];
$lang['courseedit_err_msg_copy_coursename_required'] = $lang['courseedit_err_msg_coursename_required'];
$lang['courseedit_err_msg_copy_coursename_maxlength'] = $lang['courseedit_err_msg_coursename_maxlength'];
$lang['courseedit_dialog_copy_courseid'] = $lang['courseedit_courseid'];
$lang['courseedit_dialog_copy_coursename'] = $lang['courseedit_coursename'];
$lang['courseedit_dialog_copy_btn_cancel'] = "Cancel";
$lang['courseedit_dialog_copy_btn_ok'] = "Copy";
$lang['courseedit_dialog_delete_title'] = "Delete confirmation";
$lang['courseedit_dialog_delete_body'] = "course is eliminated. Would that be OK?";
$lang['courseedit_dialog_delete_btn_cancel'] = "Cancel";
$lang['courseedit_dialog_delete_btn_ok'] = "Delete";
$lang['courseedit_err_msg_already_used_coursecode'] = "The Course ID is used already.";
$lang['courseedit_err_msg_copy_table_course'] = "An error occurred at the time of copy of a course table.";
$lang['courseedit_err_msg_copy_image_course_description'] = "An error occurred at the time of copy of Lecture introduction images.";
$lang['courseedit_err_msg_copy_update_course_description'] = "An error occurred at the time of update of a course table course_description.";
$lang['courseedit_err_msg_copy_table_theme'] = "An error occurred at the time of copy of a theme table.";
$lang['courseedit_err_msg_copy_image_theme_description'] = "An error occurred at the time of copy of Theme Description images.";
$lang['courseedit_err_msg_copy_table_materials'] = "An error occurred at the time of copy of a materials table.";
$lang['courseedit_err_msg_copy_materials'] = "An error occurred at the time of copy of material files.";
$lang['courseedit_err_msg_copy_table_task'] = "An error occurred at the time of copy of a task table.";
$lang['courseedit_err_msg_copy_image_task_description'] = "An error occurred at the time of copy of Task Question images.";
$lang['courseedit_err_msg_copy_table_students'] = "An error occurred at the time of copy of a students table.";
$lang['courseedit_err_msg_regist'] = "An error occurred at the time of registration of a course.";
$lang['courseedit_err_msg_update'] = "An error occurred at the time of a update of a course.";
$lang['courseedit_err_msg_guest_regist'] = "An error occurred at the time of registration of a guest.";
$lang['courseedit_err_msg_guest_delete'] = "An error occurred at the time of delete of a guest.";
$lang['courseedit_err_msg_startdate_theme_date'] = "Please designate a start date before the theme date.";
$lang['courseedit_err_msg_past_enddate'] = "Please designate a end date after the start date.";
$lang['courseedit_err_msg_enddate_theme_limit'] = "Please designate a end date after the theme limit.";
$lang['courseedit_err_msg_delete_table_course'] = "Failed in table delete of 'course'.";
$lang['courseedit_err_msg_delete_table_theme'] = "Failed in table delete of 'theme'.";
$lang['courseedit_err_msg_delete_table_material_play'] = "Failed in table delete of 'materials_play'.";
$lang['courseedit_err_msg_delete_table_material'] = "Failed in table delete of 'materials'.";
$lang['courseedit_err_msg_delete_table_task'] = "Failed in table delete of 'task'.";
$lang['courseedit_err_msg_delete_table_task_result'] = "Failed in table delete of 'task_result'.";
$lang['courseedit_err_msg_delete_table_students'] = "Failed in table delete of 'students'.";
$lang['courseedit_err_msg_delete_table_diary'] = "Failed in table delete of 'diary'.";
$lang['courseedit_err_msg_delete_table_diaryhide'] = "Failed in table delete of 'students'.diary_hide_info";
$lang['courseedit_err_msg_delete_table_diarylike'] = "Failed in table delete of 'diary_like'.";
$lang['courseedit_err_msg_delete_table_forum'] = "Failed in table delete of 'forum'.";
$lang['courseedit_err_msg_delete_table_forummanage'] = "Failed in table delete of 'manage_forum'.";
$lang['courseedit_complete_msg_regist'] = "It was registered.";
$lang['courseedit_tooltip_guide'] = "Guide";
$lang['courseedit_use_schedule_guide1'] = "When a period of a course is set and a date and a time limit are set as each theme, a students can attend a theme in a set period.";
$lang['courseedit_use_schedule_guide2'] = "All themes are closed and when one isn't also established, a date of a theme and a time limit can change it.";
$lang['courseedit_use_schedule_guide3'] = "Before setting of a course makes a theme, please register after it's considered sufficiently.";
$lang['courseedit_permit_guest_guide'] = "When a theme is already made, it can't be changed.";
