<?php
/*
 * チャット管理画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['chatmanager_header_pagetitle'] = "Chat manager";
$lang['chatmanager_pagetitle'] = "Chat manager";
$lang['chatmanager_label_students'] = "Student";
$lang['chatmanager_select_msg'] = "Please select";
$lang['chatmanager_dialog_error_title'] = "ERROR";
