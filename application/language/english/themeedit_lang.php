<?php
/*
 * テーマ編集画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['themeedit_header_pagetitle'] = "Theme Edit";
$lang['themeedit_pagetitle'] = "ThemeEdit";
$lang['themeedit_input_theme'] = "Theme";
$lang['themeedit_input_goal'] = "Purpose";
$lang['themeedit_input_date'] = "Date";
$lang['themeedit_input_description'] = "Theme Description";
$lang['themeedit_input_time_limit'] = "Time limit";
$lang['themeedit_input_evaluation_use'] = "Create Assignment";
$lang['themeedit_theme_item_evaluation_points'] = "Examinations";
$lang['themeedit_input_quiz'] = "Quiz Form";
$lang['themeedit_comment_form_header'] = "About each input item of \"Theme\", \"Date\", \"Time limit\", \"Purpose\" and \"Theme Description\".";
$lang['themeedit_comment_form1'] = "A theme of during opening to the public and an opening to the public end can't change the setting.";
$lang['themeedit_comment_form2'] = "A theme with the task which is already attended can't change the setting.";
$lang['themeedit_comment_evaluation_use1'] = "A theme of during opening to the public and an opening to the public end can't change the setting.";
$lang['themeedit_comment_evaluation_use2'] = "A theme with the task which is already attended can't change the setting.";
$lang['themeedit_comment_evaluation_use3'] = "The course which permits guest's attendance can't make a problem.";
$lang['themeedit_comment_quiz1'] = "When I do the assignment in the quiz form, I'll know the result immediately without a lecturer's feeding the assignment a students has submitted back.";
$lang['themeedit_comment_quiz2'] = "When there is a participant who has attended, it isn't possible to change the setting.";
$lang['themeedit_comment_btn_reg_del'] = "When there is a participant who has attended, registration and elimination can't be done.";
$lang['themeedit_comment_btn_open1'] = "If you choose \"Create an assignment\", you can not publish without a task.";
$lang['themeedit_comment_btn_open2'] = "When a period of a Course is set, when \"date\" and \"time limit\" aren't input, it can't be opened.";
$lang['themeedit_comment_btn_open3'] = "When \"Unpublished\", it can't be made \"Public end\".";
$lang['themeedit_comment_btn_open4'] = "\"Public end\" when doing, it can't be made \"Open\" again.";
$lang['themeedit_comment_btn_new_material1'] = "After registering a new theme once, it becomes possible to register teaching materials.";
$lang['themeedit_comment_btn_new_material2'] = "You can not register and eliminate teaching materials when already have attended students.";
$lang['themeedit_comment_btn_new_material3'] = "You can not register and eliminate teaching materials when it is \"Open\" or \"Public end\".";
$lang['themeedit_comment_btn_new_task1'] = "After registering a new theme once, it becomes possible to register task.";
$lang['themeedit_comment_btn_new_task2'] = "You can not register, edit and eliminate task when already have attended students.";
$lang['themeedit_comment_btn_new_task3'] = "You can not register, edit and eliminate task when it is \"Open\" or \"Public end\".";
$lang['themeedit_theme_open'] = "Open";
$lang['themeedit_theme_close'] = "Unpublished";
$lang['themeedit_theme_end'] = "Public end";
$lang['themeedit_input_err_msg_theme'] = "Please enter a theme";
$lang['themeedit_input_err_msg_goal'] = "Please enter a purpose";
$lang['themeedit_input_err_msg_date'] = "Enter the date";
$lang['themeedit_input_err_msg_description'] = "Please enter the theme description";
$lang['themeedit_input_err_msg_timelimit'] = "Enter the time limit";
$lang['themeedit_input_err_msg_date_startdate'] = "A date establish a date after a course start date, please.";
$lang['themeedit_input_err_msg_date_enddate'] = "A date establish a date before the course end date, please.";
$lang['themeedit_input_err_msg_timelimit_startdate'] = "A time limit establish a date after a course start date, please.";
$lang['themeedit_input_err_msg_timelimit_past_enddate'] = "Please designate a time limit after the theme date.";
$lang['themeedit_input_err_msg_timelimit_enddate'] = "A time limit establish a date before the course end date, please.";
$lang['themeedit_title_material'] = "Material";
$lang['themeedit_button_entry'] = "Submit";
$lang['themeedit_button_new_material'] = "Add Material";
$lang['themeedit_button_new_task'] = "Add Assignment";
$lang['themeedit_button_reset_task_result'] = "Elimination of submitted assignments";
$lang['themeedit_button_edit'] = "Edit";
$lang['themeedit_button_delete'] = "Delete";
$lang['themeedit_button_cancel'] = "Cancel";
$lang['themeedit_button_yes'] = "Yes";
$lang['themeedit_button_no'] = "No";
$lang['themeedit_delete_confim_title'] = "Delete confirmation";
$lang['themeedit_themedelete_confim'] = "When it's eliminated, all data related to this theme such as material and data of a assignment is eliminated.<br>May I eliminate a theme?";
$lang['themeedit_themedelete_confim_for_manager'] = "This theme is being exhibited or ended opening to the public or have students<br>When it's eliminated, all data related to this theme such as material and assignment and data of a assignment is eliminated.<br>May I eliminate a theme?";
$lang['themeedit_task_item_evaluation_points'] = "Evaluation points";
$lang['themeedit_task_item_btn_file'] = "File";
$lang['themeedit_taskdelete_confim'] = "Are you sure you want to delete the specified assignments?";
$lang['themeedit_materialdelete_confim'] = "Are you sure you want to delete the specified teaching materials?";
$lang['themeedit_material_resource'] = "Resource";
$lang['themeedit_material_description'] = "Link character string";
$lang['themeedit_material_file_select'] = "File Select";
$lang['themeedit_material_overlap_error'] = "Same teaching materials described in the same theme of the same account can not be set.";
$lang['themeedit_material_guide1'] = "The file which can be uploaded";
$lang['themeedit_material_guide2'] = ".mp4 .mov";
$lang['themeedit_material_guide3'] = ".jpg .jpeg .png .pdf";
$lang['themeedit_material_guide4'] = "When designating the outside link, please input from http:// or https://.";
$lang['themeedit_material_play_end'] = "It's played until the end.";
$lang['themeedit_material_button_regist'] = "Save";
$lang['themeedit_script_err_msg_theme_maxlength'] = " characters max length.";
$lang['themeedit_script_err_msg_goal_maxlength'] = " characters max length.";
$lang['themeedit_material_script_err_msg_description_maxlength'] = " characters max length.";
$lang['themeedit_material_script_err_msg_description_required'] = "Please enter a description";
$lang['themeedit_material_script_err_msg_resource_maxlength'] = " characters max length.";
$lang['themeedit_material_script_err_msg_resource_required'] = "Please enter the resource";
$lang['themeedit_err_msg_hasnt_task'] = "It can't be opened because a assignment isn't made.";
$lang['themeedit_complete_msg_regist'] = "It was registered.";
$lang['themeedit_complete_msg_update'] = "It was updated.";
$lang['themeedit_modal_msg_change_evaluation_yes'] = '"'.$lang['themeedit_button_entry'].'" please press a button and preserve change in the theme.';
$lang['themeedit_modal_msg_change_quiz'] = '"'.$lang['themeedit_input_quiz'].'" was changed.<br>"'.$lang['themeedit_button_entry'].'" a problem can\'t be made until a button is pushed down and the state is preserved.';
$lang['themeedit_modal_msg_reset_task_result'] = "All submitted assignment are eliminated.<br>Would that be OK?";
$lang['themeedit_tooltip_guide'] = "Guide";
