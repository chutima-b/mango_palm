<?php
/*
 * 課題編集画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['task_edit_header_pagetitle'] = "Task Edit";
$lang['task_edit_pagetitle'] = "Task Edit";
$lang['task_edit_task_name'] = "Title";
$lang['task_edit_description'] = "Question";
$lang['task_edit_task_type'] = "Ansewr type";
$lang['task_edit_evaluation_points'] = "Evaluation points";
$lang['task_edit_select_un_selected'] = "Please choose.";
$lang['task_edit_select_radio'] = "Radio button";
$lang['task_edit_select_checkbox'] = "Check box";
$lang['task_edit_select_text'] = "Text";
$lang['task_edit_select_file'] = "File";
$lang['task_edit_select_quiz_radio'] = "Quiz (Radio button)";
$lang['task_edit_select_survey_radio'] = "Survey (Radio button)";
$lang['task_edit_select_survey_checkbox'] = "Survey (Check box)";
$lang['task_edit_select_survey_text'] = "Survey (Text)";
$lang['task_edit_choices_textarea'] = "Choices textarea";
$lang['task_edit_quiz_answer'] = "Answer";
$lang['task_edit_comment'] = "Comment";
$lang['task_edit_btn_regist'] = "Save";
$lang['task_edit_err_msg_taskname_required'] = "Please input the ".$lang['task_edit_task_name'];
$lang['task_edit_err_msg_taskname_maxlength'] = " characters max length.";
$lang['task_edit_err_msg_description_required'] = "Please input the ".$lang['task_edit_description'];
$lang['task_edit_err_msg_description_maxlength'] = " characters max length.";
$lang['task_edit_err_msg_task_type_required'] = "Please choose the ".$lang['task_edit_task_type'];
$lang['task_edit_err_msg_evaluation_points'] = "Please input the ".$lang['task_edit_evaluation_points'];
$lang['task_edit_err_msg_choices_textarea_required'] = "Please input the choices by a new paragraph end.";
$lang['task_edit_err_msg_evaluation_points_over_100'] = "Please input the numerical value of less than 100.";
$lang['task_edit_err_msg_comment_maxlength'] = " characters max length.";
$lang['task_edit_err_msg_quiz_answer_required'] = "Please choose a correct answer of a quiz.";
$lang['task_edit_err_msg_regist'] = "An error occurred at the time of registration of a task.";
$lang['task_edit_err_msg_update'] = "An error occurred at the time of a update of a task.";
