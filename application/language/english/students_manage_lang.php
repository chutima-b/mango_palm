<?php
/*
 * 受講者管理画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['students_manage_header_pagetitle'] = "Manage Students";
$lang['students_manage_pagetitle'] = "Manage Students";
$lang['students_manage_item_courseid'] = "Course ID";
$lang['students_manage_item_coursename'] = "Course name";
$lang['students_manage_item_genre'] = "Genre";
$lang['students_manage_item_lecturer'] = "Lecturer";
$lang['students_manage_item_period'] = "Period";
$lang['students_manage_button_new_students'] = "Add Students";
$lang['students_manage_list_item_userid'] = "User ID";
$lang['students_manage_list_item_nickname'] = "Nickname";
$lang['students_manage_button_release'] = "Remove";
$lang['students_manage_button_cancel'] = "Cancel";
$lang['students_manage_release_confim_title'] = "Remove confim";
$lang['students_manage_release_confim'] = "Are you sure you want to remove the students?";
