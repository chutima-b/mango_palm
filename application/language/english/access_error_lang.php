<?php
/*
 * アクセスエラー画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['access_error_header_pagetitle'] = "Access Error page";
$lang['access_error_pagetitle'] = "Access Error";
$lang['access_error_errormsg'] = "It isn't possible to access this screen.";
