<?php
/*
 * 課題管理画面言語ファイル
 *
 * @author Kazu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['taskmanager_header_pagetitle'] = "Task Manager";
$lang['taskmanager_pagetitle'] = "Task Manager";
$lang['taskmanager_participants'] = " participants";
$lang['taskmanager_label_msg_attended'] = "Attended";
$lang['taskmanager_label_msg_warning'] = "Feedback";
$lang['taskmanager_label_msg_danger'] = "Expired";
$lang['taskmanager_label_msg_closed'] = "Closed";
$lang['taskmanager_label_msg_publicend'] = "Public end";
$lang['taskmanager_btn_reports'] = "Reports";
