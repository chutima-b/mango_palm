<?php
/*
 * システム管理（設定）画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['settings_header_pagetitle'] = "System Manager(Settings)";
$lang['settings_pagetitle'] = "System Manager(Settings)";
$lang['settings_headertitle'] = "System Manager";
$lang['settings_link_usermanager'] = "User Manager";
$lang['settings_link_genremanager'] = "Curriculum Manager";
$lang['settings_link_settings'] = "Settings";
$lang['settings_item_title'] = "Title";
$lang['settings_item_introduction'] = "Introduction";
$lang['settings_item_background'] = "Background";
$lang['settings_checkbox_delete_logo'] = "Delete logo image";
$lang['settings_checkbox_use_background_img'] = "Use Wallpaper";
$lang['settings_btn_logo_img'] = "Logo";
$lang['settings_btn_background_img'] = "Wallpaper";
$lang['settings_btn_regist'] = "Save";
$lang['settings_err_msg_regist'] = "An error occurred at the time of registration of a settings.";
$lang['settings_script_err_msg_backgroundimg_required'] = "Please choose wallpaper.";
$lang['settings_script_err_msg_title_maxlength'] = " characters max length.";
$lang['settings_complete_msg_regist'] = "It was registered.";
