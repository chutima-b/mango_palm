<?php
/*
 * 動画再生画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['materialvideo_restart'] = "Restart";
$lang['materialvideo_play'] = "Play";
$lang['materialvideo_rew'] = "Rewind";
$lang['materialvideo_fastfwd'] = "Forward";
