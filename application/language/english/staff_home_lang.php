<?php
/*
 * ホーム画面（管理者）言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['staff_home_header_pagetitle'] = "Staff Home";
$lang['staff_home_pagetitle'] = "Home";
$lang['staff_home_new_notice'] = "Post News";
$lang['staff_home_noticetitle'] = "News";
$lang['staff_home_notice_col_date'] = "Date";
$lang['staff_home_notice_col_title'] = "Title";
$lang['staff_home_notice_col_content'] = "Content";
$lang['staff_home_notice_col_course'] = "Curriculum";
$lang['staff_home_notice_col_course_option_default'] = "Please choose.";
$lang['staff_home_notice_col_course_option_all'] = "All";
$lang['staff_home_notice_col_calendar_use'] = "Show calendar";
$lang['staff_home_notice_col_color'] = "Calendar";
$lang['staff_home_notice_none'] = "There are no announcements.";
$lang['staff_home_notice_button_edit'] = "Edit";
$lang['staff_home_notice_button_delete'] = "Delete";
$lang['staff_home_notice_button_entry'] = "Post";
$lang['staff_home_notice_button_cancel'] = "Cancel";
$lang['staff_home_notice_confim_title'] = "Delete confirmation";
$lang['staff_home_notice_delete_confim'] = "Are you sure you want to delete the specified announcement?";
$lang['staff_home_new_course'] = "Add Course";
$lang['staff_home_course_count_prefix'] = "total";
$lang['staff_home_course_count'] = "courses";
$lang['staff_home_course_col_course_code'] = "Course ID";
$lang['staff_home_course_col_course_name'] = "Course name";
$lang['staff_home_course_col_lecturer'] = "Lecturer";
$lang['staff_home_course_col_theme_count'] = "Number of themes";
$lang['staff_home_course_col_students_count'] = "Number of students";
$lang['staff_home_course_col_period'] = "Period";
$lang['staff_home_course_button_edit'] = "Edit";
$lang['staff_home_course_button_staff'] = "Staff";
$lang['staff_home_course_button_students'] = "Students";
$lang['staff_home_course_button_task'] = "Evaluation";
$lang['staff_home_script_err_msg_noticetitle_required'] = "Please enter a title.";
$lang['staff_home_script_err_msg_noticetitle_maxlength'] = " characters max length.";
$lang['staff_home_script_err_msg_noticecontent_required'] = "Please enter a content.";
$lang['staff_home_script_err_msg_noticetargetdt_required'] = "Please enter a date.";
$lang['staff_home_script_err_msg_noticecourse_required'] = "Please choose the course.";
$lang['staff_home_script_err_msg_noticecolor_required'] = "Please choose the calendar color.";
