<?php
/*
 * フォーラム管理画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['forum_manage_header_pagetitle'] = "Manage Forum";
$lang['forum_manage_pagetitle'] = "Manage Forum";
$lang['forum_manage_newforum'] = "New Forum";
$lang['forum_manage_forumname'] = "New Forum";
$lang['forum_manage_owner'] = "Owner";
$lang['forum_manage_posts'] = "Number of posts";
$lang['forum_manage_latest'] = "Latest";
$lang['forum_manage_button_stop'] = "Stop";
$lang['forum_manage_button_resumption'] = "Resumption";
$lang['forum_manage_entry_title'] = "Forum Name";
$lang['forum_manage_entry_input_defalut'] = "Please enter a forum name.";
$lang['forum_manage_textarea_default_message'] = "Message";
$lang['forum_manage_button_cancel'] = "Cancel";
$lang['forum_manage_button_registration'] = "Registration";
$lang['forum_manage_confim_title'] = "Delete confirmation";
$lang['forum_manage_delete_confim'] = "Are you sure you want to delete the specified form?";
$lang['forum_manage_button_delete'] = "Delete";
$lang['forum_manage_err_msg_forumname_regist'] = "An error occurred at the time of registration of forum management information.";
$lang['forum_manage_script_err_msg_forumname_required'] = "Please input a Forum Name.";
$lang['forum_manage_script_err_msg_forumname_maxlength'] = " characters max length.";
$lang['forum_manage_script_err_msg_postmsg_required'] = "Please input a message.";
$lang['forum_manage_script_err_msg_postmsg_maxlength'] = " characters max length.";
