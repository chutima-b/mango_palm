<?php
/*
 * システム管理（ユーザー管理）画面言語ファイル
 *
 * @author Kazu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['usermanager_header_pagetitle'] = "System Manager(User)";
$lang['usermanager_pagetitle'] = "System Manager(User)";
$lang['usermanager_headertitle'] = "System Manager";
$lang['usermanager_link_usermanager'] = "User Manager";
$lang['usermanager_link_genremanager'] = "Curriculum Manager";
$lang['usermanager_link_settings'] = "Settings";
$lang['usermanager_search_userid'] = "User ID";
$lang['usermanager_search_surname'] = "Surname";
$lang['usermanager_search_firstname'] = "Firstname";
$lang['usermanager_btn_search'] = "Search";
$lang['usermanager_btn_upload'] = "Upload";
$lang['usermanager_btn_newuser'] = "Add User";
$lang['usermanager_link_before'] = "Before";
$lang['usermanager_link_next'] = "Next";
$lang['usermanager_users'] = "users";
$lang['usermanager_table_userid'] = "User ID";
$lang['usermanager_table_surname'] = "Surname";
$lang['usermanager_table_firstname'] = "Firstname";
$lang['usermanager_table_nickname'] = "Nickname";
$lang['usermanager_table_course_count'] = "Cours count";
$lang['usermanager_table_login_at'] = "Last login";
$lang['usermanager_btn_stop'] = "Stop";
$lang['usermanager_btn_restart'] = "Resumption";
$lang['usermanager_btn_edit'] = "Edit";
$lang['usermanager_btn_download'] = "Download";
$lang['usermanager_dialog_userid'] = "User ID";
$lang['usermanager_dialog_passwd'] = "Password";
$lang['usermanager_dialog_passwd_confirm'] = "Retype password";
$lang['usermanager_dialog_nickname'] = "Nickname";
$lang['usermanager_dialog_surname'] = "Surname";
$lang['usermanager_dialog_firstname'] = "Firstname";
$lang['usermanager_dialog_placeholder_passwd'] = "A password isn't indicated.";
$lang['usermanager_dialog_course'] = "Course";
$lang['usermanager_btn_cancel'] = "Cancel";
$lang['usermanager_btn_regist'] = "Regist";
$lang['usermanager_confirm_msg_title_stop'] = "Stop confirmation";
$lang['usermanager_confirm_msg_body_stop'] = " user is suspended. Would that be OK?";
$lang['usermanager_confirm_msg_title_restart'] = "Restart confirmation";
$lang['usermanager_confirm_msg_body_restart'] = " user is restarted. Would that be OK?";
$lang['usermanager_confirm_msg_title_delete'] = "Elimination confirmation";
$lang['usermanager_confirm_msg_body_delete'] = " user is eliminated. Would that be OK?";
$lang['usermanager_confirm_btn_cancel'] = "CANCEL";
$lang['usermanager_confirm_btn_ok'] = "OK";
$lang['usermanager_already_used_userid'] = "A User ID is used already.";
$lang['usermanager_alpha_numeric_passwd'] = "Please input a password by a alphanumeric symbol.";
$lang['usermanager_err_msg_regist'] = "An error occurred at the time of registration of a user.";
$lang['usermanager_err_msg_update'] = "An error occurred at the time of update of a user.";
$lang['usermanager_err_msg_update_picture_file'] = "An error occurred at the time of update picture file of a user.";
$lang['usermanager_err_msg_stop'] = "An error occurred at the time of stopping of a user.";
$lang['usermanager_err_msg_restart'] = "An error occurred at the time of restarting of a user.";
$lang['usermanager_err_msg_delete'] = "An error occurred at the time of eliminating of a user.";
$lang['usermanager_err_msg_upload'] = "An error occurred at the time of uploading of a csv file.";
$lang['usermanager_err_msg_upload_format'] = "The format of the CSV file is unjust.";
$lang['usermanager_err_msg_upload_userid_length'] = "The number of characters of the user ID is unjust.";
$lang['usermanager_err_msg_upload_userid_alpha_numeric'] = "Be alphanumerical and be a symbol and please describe a user ID.";
$lang['usermanager_err_msg_upload_passwd_length'] = "The number of characters of the password is unjust.";
$lang['usermanager_err_msg_upload_passwd_alpha_numeric'] = "Be alphanumerical and be a symbol and please describe a password.";
$lang['usermanager_err_msg_upload_surname_length'] = "The number of characters of the surname is unjust.";
$lang['usermanager_err_msg_upload_firstname_length'] = "The number of characters of the firstname is unjust.";
$lang['usermanager_script_err_msg_userid_required'] = "Please input a userid.";
$lang['usermanager_script_err_msg_userid_alpha_numeric'] = "Please input by an alphanumeric.";
$lang['usermanager_script_err_msg_surname_required'] = "Please input a surname.";
$lang['usermanager_script_err_msg_firstname_required'] = "Please input a firstname.";
$lang['usermanager_script_err_msg_nickname_required'] = "Please input a nickname.";
$lang['usermanager_script_err_msg_passwd_required'] = "Please input a password.";
$lang['usermanager_script_err_msg_passwd_confirm_required'] = "Please input a Retype password.";
$lang['usermanager_script_err_msg_passwd_confirm_equalto'] = "Please enter the same password as above.";
$lang['usermanager_script_err_msg_maxlength'] = " characters max length.";
$lang['usermanager_script_err_msg_minlength'] = " characters min length.";
$lang['usermanager_csvdata_validityflg_true'] = "usable";
$lang['usermanager_csvdata_validityflg_false'] = "unusable";
$lang['usermanager_guide_title'] = "About upload fle.";
$lang['usermanager_guide_item1'] = "UTF-8(without BOM) or Shift_JIS";
$lang['usermanager_guide_item2'] = "User ID（Up to 50 alphanumeric characters）,Password（Alphanumeric symbol 6 characters or more）,Last Name（Up to 50 characters）,First Name（Up to 50 characters）[New line]";
$lang['usermanager_guide_item3'] = "The same user ID will be overwritten.";
$lang['usermanager_tooltip_guide'] = "Guide";
