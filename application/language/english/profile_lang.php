<?php
/*
 * プロフィール画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['profile_header_pagetitle'] = "Profile";
$lang['profile_pagetitle'] = "Profile";
$lang['profile_nickname'] = "Nickname";
$lang['profile_introduction'] = "About me";
$lang['profile_introduction_description'] = " character";
$lang['profile_chat_use'] = "Use chat";
$lang['profile_passwd'] = "Present password";
$lang['profile_new_passwd'] = "New password";
$lang['profile_new_passwd_confirm'] = "Retype new password";
$lang['profile_regist'] = "Save";
$lang['profile_wrong_passwd'] = "The Present password is wrong.";
$lang['profile_alpha_numeric_new_passwd'] = "Please input a Retype password by a alphanumeric symbol.";
$lang['profile_script_err_msg_nickname_required'] = "Please input a nickname.";
$lang['profile_script_err_msg_nickname_maxlength'] = " characters max length.";
$lang['profile_script_err_msg_introduction_maxlength'] = " characters max length.";
$lang['profile_script_err_msg_passwd_required'] = "Please input a Present password.";
$lang['profile_script_err_msg_new_passwd_confirm_required'] = "Please input a Retype new password.";
$lang['profile_script_err_msg_new_passwd_confirm_equalto'] = "Please enter the same new password as above.";
$lang['profile_script_err_msg_maxlength'] = " characters max length.";
$lang['profile_script_err_msg_minlength'] = " characters min length.";
