<?php
/*
 * ログイン画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['login_header_pagetitle'] = "Login page";
$lang['login_pagetitle'] = "Login";
$lang['login_userid'] = "User ID";
$lang['login_passwd'] = "Password";
$lang['login_continue'] = "Remember me";
$lang['login_btn_login'] = "Sign in";
$lang['login_btn_login_guest'] = "Sign in as a guest";
$lang['login_err_msg_login'] = "It's different in a user ID or a password.";
