<?php
/*
 * チャット画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['chat_header_pagetitle'] = "Chat";
$lang['chat_pagetitle'] = "Chat";
$lang['chat_message_placeholder'] = "message...";
$lang['chat_btn_submit'] = "Send";
$lang['chat_dialog_error_title'] = "ERROR";
$lang['chat_dialog_error_body_empty_message'] = "Please input a message.";
$lang['chat_dialog_error_body_str_over_message'] = "Please input within 256 character.";
