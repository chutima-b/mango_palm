<?php
/*
 * フォーラム画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['forum_header_pagetitle'] = "Forum";
$lang['forum_pagetitle'] = "Forum";
$lang['forum_button_title_edit'] = "Edit";
$lang['forum_button_title_delete'] = "Delete";
$lang['forum_button_title_post'] = "Post";
$lang['forum_button_title_cancel'] = "Cancel";
$lang['forum_button_title_send'] = "Post to Forum";
$lang['forum_textarea_default_message'] = "Message";
$lang['forum_confim_title'] = "Delete confirmation";
$lang['forum_delete_confim'] = "Are you sure you want to delete the specified post?";
$lang['forum_script_err_msg_postmsg_required'] = "Please input a message.";
$lang['forum_script_err_msg_postmsg_maxlength'] = " characters max length.";
