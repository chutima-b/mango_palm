<?php
/*
 * 課題画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['task_header_pagetitle'] = "Task";
$lang['task_pagetitle'] = "Task";
$lang['task_btn_file'] = "File";
$lang['task_btn_noanswer'] = "No answer";
$lang['task_btn_submit'] = "Answer";
$lang['task_valid_msg_require_choose'] = "Please choose.";
$lang['task_valid_msg_require_text'] = "Please input.";
