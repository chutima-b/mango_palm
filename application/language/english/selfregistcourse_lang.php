<?php
/*
 * 講座申込画面（受講講座自己登録）言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['selfregistcourse_header_pagetitle'] = "Staff Home";
$lang['selfregistcourse_pagetitle'] = "Home";
$lang['selfregistcourse_course_count_prefix'] = "total";
$lang['selfregistcourse_course_count'] = "courses";
$lang['selfregistcourse_col_course_code'] = "Course ID";
$lang['selfregistcourse_col_course_name'] = "Course name";
$lang['selfregistcourse_col_lecturer'] = "Lecturer";
$lang['selfregistcourse_col_theme_count'] = "Themes";
$lang['selfregistcourse_col_students_count'] = "Students";
$lang['selfregistcourse_col_period'] = "Period";
$lang['selfregistcourse_btn_course_info'] = "Course information";
$lang['selfregistcourse_modal_lecturer'] = "Lecturer";
$lang['selfregistcourse_modal_course_description'] = "Course description";
$lang['selfregistcourse_modal_btn_cancel'] = "Cancel";
$lang['selfregistcourse_modal_btn_attend'] = "Attend";
$lang['selfregistcourse_err_msg_regist'] = "An error occurred at the time of attendance course registration.";
