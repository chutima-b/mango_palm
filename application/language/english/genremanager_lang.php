<?php
/*
 * システム管理（ジャンル）画面言語ファイル
 *
 * @author Kazu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['genremanager_header_pagetitle'] = "System Manager(Curriculum)";
$lang['genremanager_pagetitle'] = "System Manager(Curriculum)";
$lang['genremanager_containertitle'] = "System Manager";
$lang['genremanager_tab_usermanager'] = "User Manager";
$lang['genremanager_tab_genremanager'] = "Curriculum Manager";
$lang['genremanager_tab_settings'] = "Settings";
$lang['genremanager_item_genre_count'] = "Curriculum Count";
$lang['genremanager_item_course_count'] = "Course Count";
$lang['genremanager_button_new_genre'] = "Add Curriculum";
$lang['genremanager_list_column_icon'] = "Icon";
$lang['genremanager_list_column_genreid'] = "Curriculum ID";
$lang['genremanager_list_column_genrename'] = "Curriculum Name";
$lang['genremanager_list_column_course_count'] = "Course Count";
$lang['genremanager_button_edit'] = "Edit";
$lang['genremanager_regist_item_genreid'] = "Curriculum ID";
$lang['genremanager_regist_item_genrename'] = "Curriculum Name";
$lang['genremanager_regist_item_icon'] = "Icon";
$lang['genremanager_regist_item_delete_icon'] = "Delete icon";
$lang['genremanager_button_regist'] = "Create";
$lang['genremanager_button_delete'] = "Delete";
$lang['genremanager_button_cancel'] = "Cancel";
$lang['genremanager_delete_confim_title'] = "Delete Confim";
$lang['genremanager_delete_confim'] = "Are you sure you want to delete the specified curriculum?";
$lang['genremanager_script_err_msg_genreid_required'] = "Please input a Curriculum ID";
$lang['genremanager_script_err_msg_genreid_maxlength'] = " characters max length.";
$lang['genremanager_script_err_msg_genrename_required'] = "Please input a Curriculum Name";
$lang['genremanager_script_err_msg_genrename_maxlength'] = " characters max length.";
$lang['genremanager_error_msg_duplication_1'] = "Curriculum ID is duplicated";
$lang['genremanager_error_msg_duplication_2'] = "Curriculum name is duplicated";
$lang['genremanager_error_msg_duplication_3'] = "Curriculum ID and the curriculum name is duplicated";
$lang['genremanager_error_msg_update_icon_file'] = "An error occurred at the time of update icon file of a curriculum.";
