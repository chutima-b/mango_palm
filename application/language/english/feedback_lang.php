<?php
/*
 * フィードバック画面言語ファイル
 *
 * @author Kazu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['feedback_header_pagetitle'] = "Feedback";
$lang['feedback_pagetitle'] = "Feedback";
$lang['feedback_item_userid'] = "User ID";
$lang['feedback_item_nickname'] = "Nickname";
$lang['feedback_material_header'] = "Material";
$lang['feedback_input_points'] = "Evaluation points";
$lang['feedback_input_feedback'] = "Comment";
$lang['feedback_button_download'] = "Download";
$lang['feedback_button_draft_save'] = "Draft saved";
$lang['feedback_button_feedback'] = "Feedback";
$lang['feedback_complete_msg'] = "A feedback was sent.";
$lang['feedback_script_err_msg_point_maxlength'] = " characters max length.";
$lang['feedback_script_err_msg_feedbackmessage_maxlength'] = " characters max length.";
$lang['feedback_script_err_msg_feedbackpoints_required'] = "Please input the ".$lang['feedback_input_points'];
$lang['feedback_script_err_msg_feedbackpoints_maxvalue_1'] = "Please enter at ";
$lang['feedback_script_err_msg_feedbackpoints_maxvalue_2'] = " points below";
$lang['feedback_script_err_msg_feedbackpoints_minvalue'] = "Please enter at greater than or equal to 0.";
