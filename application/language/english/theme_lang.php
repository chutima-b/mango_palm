<?php
/*
 * テーマ画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['theme_header_pagetitle'] = "Theme";
$lang['theme_pagetitle'] = "Theme";
$lang['theme_time_limit'] = "Time limit";
$lang['theme_submit_at'] = "Submit";
$lang['theme_feedback_at'] = "Feedback";
$lang['theme_examination'] = "Examination";
$lang['theme_evaluation'] = "Evaluation";
$lang['theme_teaching_materialtitle'] = "Teaching material";
$lang['theme_task_comp'] = "Completion";
$lang['theme_task_incomp'] = "Incomplete";
$lang['theme_task_item_points'] = "Evaluation points";
$lang['theme_task_item_feedback'] = "Feedback";
$lang['theme_task_item_comment'] = "Comment";
$lang['theme_task_button_answer'] = "Answer";
$lang['theme_task_button_submit'] = "Hand in Assignment";
$lang['theme_task_button_attended'] = "Attended";
$lang['theme_task_link_forum'] = "Forum";
$lang['theme_task_link_chat'] = "Chat";
$lang['theme_task_link_diary'] = "Timeline";
$lang['theme_contents_diary'] = "Timeline";
$lang['theme_contents_diary_link'] = "more...";
$lang['theme_contents_forum'] = "Forum";
$lang['theme_complete_msg_submit'] = "A task has been submitted.";
$lang['theme_complete_msg_attend'] = "Attendance has been ended.";
$lang['theme_error_msg_submit'] = "An error occurred at the time of submission of a task.";
$lang['theme_error_msg_not_success_material_videos'] = "The next teaching materials were filled at prescribed watching time, it can't be submitted because I don't have that.";
$lang['theme_guide_task'] = "When a date and a time limit are set as a theme, an answer of a task and submission will be only in the period.";
$lang['theme_label_close'] = "Close";
