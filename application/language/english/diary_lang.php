<?php
/*
 * 日記画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['diary_header_pagetitle'] = "Timeline";
$lang['diary_pagetitle'] = "Timeline";
$lang['diary_button_title_edit'] = "Edit";
$lang['diary_button_title_diarylist'] = "Timeline setting";
$lang['diary_button_title_diarywrite'] = "Post to Timeline";
$lang['diary_button_title_cancel'] = "Cancel";
$lang['diary_button_title_send'] = "Post to Timeline";
$lang['diary_button_title_delete'] = "Delete";
$lang['diary_link_like_add'] = "Like!";
$lang['diary_link_like_del'] = "Cancel Like!";
$lang['diary_confim_title'] = "Delete confirmation";
$lang['diary_delete_confim'] = "Are you sure you want to delete the specified timeline?";
$lang['diary_textarea_default_message'] = "Message";
$lang['diary_script_err_msg_postmsg_required'] = "Please input a message.";
$lang['diary_script_err_msg_postmsg_maxlength'] = " characters max length.";
