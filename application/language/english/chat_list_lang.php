<?php
/*
 * チャット一覧画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['chat_list_header_pagetitle'] = "Chat List";
$lang['chat_list_pagetitle'] = "Chat List";
$lang['chat_list_auth_student'] = "Student";
$lang['chat_list_auth_teacher'] = "Teacher";
$lang['chat_list_auth_op_manager'] = "Operations Manager";
$lang['chat_list_auth_sys_manager'] = "System Manager";
