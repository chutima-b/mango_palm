<?php
/*
 * ホーム画面（受講者）言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['students_home_header_pagetitle'] = "Students Home";
$lang['students_home_pagetitle'] = "Home";
$lang['students_home_noticetitle'] = "News";
$lang['students_home_notice_none'] = "There are no announcements.";
$lang['students_home_coursetitle'] = "Curriculum";
$lang['students_home_noeventmsg'] = "No events";
$lang['students_home_diary'] = "Timeline";
$lang['students_home_diary_link'] = "more...";
$lang['students_home_forum'] = "Forum";
$lang['students_home_cal_intro_switch_cal'] = "Calendar";
$lang['students_home_cal_intro_switch_intro'] = "Introduction";
$lang['students_home_btn_self_regist_course'] = "Regist course";
