<?php
/*
 * 講師運用管理者登録画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['staffmanager_header_pagetitle'] = "Staff Manager";
$lang['staffmanager_pagetitle'] = "Staff manager";
$lang['staffmanager_teachername'] = "Teacher";
$lang['staffmanager_op_managername'] = "Operations manager";
$lang['staffmanager_unregistered'] = "(Unregistered)";
$lang['staffmanager_btn_regist_teacher'] = "Add Teacher";
$lang['staffmanager_btn_regist_op_manager'] = "Add Manager";
