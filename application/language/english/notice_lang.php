<?php
/*
 * お知らせ画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['notice_header_pagetitle'] = "News";
$lang['notice_pagetitle'] = "News";
