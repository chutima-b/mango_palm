<?php
/*
 * 講座画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['course_header_pagetitle'] = "Course";
$lang['course_pagetitle'] = "Course";
$lang['course_teacher'] = "Teacher";
$lang['course_description'] = "Course description";
$lang['course_label_style_info'] = "Submitted";
$lang['course_label_style_warning'] = "Feedback";
$lang['course_label_style_primary'] = "Attended";
$lang['course_label_style_danger'] = "Ended";
$lang['course_label_close'] = "Close";
$lang['course_button_attend_primary'] = "Attended";
$lang['course_button_attend_warning'] = "Feedback";
$lang['course_button_attend_info'] = "Submitted";
$lang['course_button_attend_info_yet_submit'] = "Attend";
$lang['course_link_forum'] = "Forum";
$lang['course_link_chat'] = "Chat";
$lang['course_link_diary'] = "Timeline";
