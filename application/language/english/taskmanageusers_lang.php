<?php
/*
 * 受講者課題画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['taskmanageusers_header_pagetitle'] = "Task manage user list";
$lang['taskmanageusers_pagetitle'] = "Task manage user list";
$lang['taskmanageusers_participants'] = "participants";
$lang['taskmanageusers_time_limit'] = "Time limit";
$lang['taskmanageusers_table_userid'] = "User ID";
$lang['taskmanageusers_table_lastname'] = "Last name";
$lang['taskmanageusers_table_firstname'] = "First name";
$lang['taskmanageusers_table_attend_at'] = "Attended";
$lang['taskmanageusers_table_feedback_at'] = "Feedback";
$lang['taskmanageusers_table_feedback_points'] = "Points";
$lang['taskmanageusers_btn_feedback'] = "Feedback";
$lang['taskmanageusers_btn_download'] = "Download";
$lang['taskmanageusers_csv_item_correct'] = "Correct";
$lang['taskmanageusers_csv_item_incorrect'] = "Incorrect";
$lang['taskmanageusers_csv_column_name_userid'] = "User id";
$lang['taskmanageusers_csv_column_name_nickname'] = "Nickname";
$lang['taskmanageusers_csv_column_name_lastname'] = "Lastname";
$lang['taskmanageusers_csv_column_name_firstname'] = "Firstname";
$lang['taskmanageusers_csv_column_name_genreid'] = "Curriculum ID";
$lang['taskmanageusers_csv_column_name_genrename'] = "Curriculum";
$lang['taskmanageusers_csv_column_name_coursecode'] = "Course id";
$lang['taskmanageusers_csv_column_name_coursename'] = "Course name";
$lang['taskmanageusers_csv_column_name_themename'] = "Theme name";
$lang['taskmanageusers_csv_column_name_themedate'] = "Theme date";
$lang['taskmanageusers_csv_column_name_submitdate'] = "Submit date";
$lang['taskmanageusers_csv_column_name_themelimit'] = "Theme limit";
$lang['taskmanageusers_csv_column_name_feedbackdate'] = "Feedback date";
$lang['taskmanageusers_csv_column_name_moviecnt'] = "Number of movies";
$lang['taskmanageusers_csv_column_name_moviename'] = "Movie name";
$lang['taskmanageusers_csv_column_name_movietotal'] = "Total time of Movie";
$lang['taskmanageusers_csv_column_name_movieplaytime'] = "Play time of Movie";
$lang['taskmanageusers_csv_column_name_total_feedbackpoint'] = "Total of feedback point";
$lang['taskmanageusers_csv_column_name_total_evaluationpoint'] = "Total of evaluation point";
$lang['taskmanageusers_csv_column_name_taskcnt'] = "Number of tasks";
$lang['taskmanageusers_csv_column_name_taskname'] = "Task name";
$lang['taskmanageusers_csv_column_name_taskdescription'] = "Task description";
$lang['taskmanageusers_csv_column_name_feedbackpoints'] = "Feedback point";
$lang['taskmanageusers_csv_column_name_quizresult'] = "Result Of quiz";
$lang['taskmanageusers_csv_column_name_answer'] = "Answer";
$lang['taskmanageusers_csv_column_name_feedbackmessage'] = "Feedback message";
$lang['taskmanageusers_csv_column_name_downloaddate'] = "Download date";
$lang['taskmanageusers_guide_download'] = "The theme to which a task has not been submitted can't be downloaded.";
