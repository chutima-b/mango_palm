<?php
/*
 * 共通ヘッダー画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['header_link_back'] = "Back";
$lang['header_link_home'] = "Home";
$lang['header_link_sysmanage'] = "System Manager";
$lang['header_link_home_students'] = "Students";
$lang['header_link_home_staff'] = "Manager";
$lang['header_link_profile'] = "Profile";
$lang['header_link_logout'] = "Logout";
