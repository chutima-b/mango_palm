<?php
/*
 * ホーム画面（受講者）言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['students_home_header_pagetitle'] = "受講者ホーム画面";
$lang['students_home_pagetitle'] = "ホーム";
$lang['students_home_noticetitle'] = "お知らせ";
$lang['students_home_notice_none'] = "お知らせはありません。";
$lang['students_home_coursetitle'] = "カテゴリー";
$lang['students_home_noeventmsg'] = "予定はありません";
$lang['students_home_diary'] = "日記";
$lang['students_home_diary_link'] = "もっと見る...";
$lang['students_home_forum'] = "フォーラム";
$lang['students_home_cal_intro_switch_cal'] = "カレンダー";
$lang['students_home_cal_intro_switch_intro'] = "イントロ";
$lang['students_home_btn_self_regist_course'] = "講座申込";
