<?php
/*
 * 講座編集画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['courseedit_header_pagetitle'] = "Information編集画面";
$lang['courseedit_pagetitle'] = "Information編集";
$lang['courseedit_courseid'] = "InformationID";
$lang['courseedit_coursename'] = "Contents名";
$lang['courseedit_genre'] = "ジャンル";
$lang['courseedit_genre_default'] = "選択して下さい";
$lang['courseedit_teachername'] = "管理";
$lang['courseedit_op_managername'] = "運用管理者";
$lang['courseedit_unregistered'] = "（未登録）";
$lang['courseedit_btn_regist_teacher'] = "管理登録";
$lang['courseedit_btn_regist_op_manager'] = "運用管理者登録";
$lang['courseedit_description'] = "Contents紹介";
$lang['courseedit_use_forum'] = "フォーラムを利用する";
$lang['courseedit_use_schedule'] = "掲載の期間を設定する";
$lang['courseedit_permit_guest'] = "ゲストの受講を許可する";
$lang['courseedit_startdate'] = "開始日";
$lang['courseedit_enddate'] = "終了日";
$lang['courseedit_calendar_color'] = "カレンダー";
$lang['courseedit_btn_copy'] = "Informationコピー";
$lang['courseedit_btn_regist'] = "登録";
$lang['courseedit_btn_delete'] = "削除";
$lang['courseedit_btn_new_theme'] = "新しいContents";
$lang['courseedit_info_title_theme'] = "Contents";
$lang['courseedit_theme_label_close'] = "非公開";
$lang['courseedit_theme_label_end'] = "公開終了";
$lang['courseedit_theme_label_over_limit'] = "期限切れ";
$lang['courseedit_theme_label_attended'] = "閲覧済";
$lang['courseedit_err_msg_courseid_required'] = "InformationIDを入力してください";
$lang['courseedit_err_msg_courseid_maxlength'] = "50文字以内で入力してください";
$lang['courseedit_err_msg_coursename_required'] = "Information名を入力してください";
$lang['courseedit_err_msg_coursename_maxlength'] = "50文字以内で入力してください";
$lang['courseedit_err_msg_genre_required'] = "ジャンルを選択してください";
$lang['courseedit_err_msg_description_required'] = "Information紹介を入力してください";
$lang['courseedit_err_msg_startdate_required'] = "開始日を入力してください";
$lang['courseedit_err_msg_enddate_required'] = "終了日を入力してください";
$lang['courseedit_err_msg_copy_courseid_required'] = $lang['courseedit_err_msg_courseid_required'];
$lang['courseedit_err_msg_copy_courseid_maxlength'] = $lang['courseedit_err_msg_courseid_maxlength'];
$lang['courseedit_err_msg_copy_coursename_required'] = $lang['courseedit_err_msg_coursename_required'];
$lang['courseedit_err_msg_copy_coursename_maxlength'] = $lang['courseedit_err_msg_coursename_maxlength'];
$lang['courseedit_dialog_copy_courseid'] = $lang['courseedit_courseid'];
$lang['courseedit_dialog_copy_coursename'] = $lang['courseedit_coursename'];
$lang['courseedit_dialog_copy_btn_cancel'] = "キャンセル";
$lang['courseedit_dialog_copy_btn_ok'] = "コピー";
$lang['courseedit_dialog_delete_title'] = "削除確認";
$lang['courseedit_dialog_delete_body'] = "Informationを削除します。よろしいですか？";
$lang['courseedit_dialog_delete_btn_cancel'] = "キャンセル";
$lang['courseedit_dialog_delete_btn_ok'] = "削除";
$lang['courseedit_err_msg_already_used_coursecode'] = "InformationIDは既に使用されています";
$lang['courseedit_err_msg_copy_table_course'] = "Information情報テーブルのコピー時にエラーが発生しました。";
$lang['courseedit_err_msg_copy_image_course_description'] = "Information紹介画像のコピー時にエラーが発生しました。";
$lang['courseedit_err_msg_copy_update_course_description'] = "Information紹介画像の更新時にエラーが発生しました。";
$lang['courseedit_err_msg_copy_table_theme'] = "Contentsテーブルのコピー時にエラーが発生しました。";
$lang['courseedit_err_msg_copy_image_theme_description'] = "Contents説明画像のコピー時にエラーが発生しました。";
$lang['courseedit_err_msg_copy_table_materials'] = "教材テーブルのコピー時にエラーが発生しました。";
$lang['courseedit_err_msg_copy_materials'] = "教材ファイルのコピー時にエラーが発生しました。";
$lang['courseedit_err_msg_copy_table_task'] = "課題情報テーブルのコピー時にエラーが発生しました。";
$lang['courseedit_err_msg_copy_image_task_description'] = "課題「問題」画像のコピー時にエラーが発生しました。";
$lang['courseedit_err_msg_copy_table_students'] = "受講者情報テーブルのコピー時にエラーが発生しました。";
$lang['courseedit_err_msg_regist'] = "Informationの登録時にエラーが発生しました。";
$lang['courseedit_err_msg_update'] = "Informationの更新時にエラーが発生しました。";
$lang['courseedit_err_msg_guest_regist'] = "ゲストユーザーの登録時にエラーが発生しました。";
$lang['courseedit_err_msg_guest_delete'] = "ゲストユーザーの削除時にエラーが発生しました。";
$lang['courseedit_err_msg_startdate_theme_date'] = "開始日はテーマの日付に設定した日以前の日付を指定してください。";
$lang['courseedit_err_msg_past_enddate'] = "終了日は開始日以降の日付を指定してください。";
$lang['courseedit_err_msg_enddate_theme_limit'] = "終了日はテーマの期限に設定した日以降の日付を指定してください。";
$lang['courseedit_err_msg_delete_table_course'] = "講座情報テーブルの削除時に失敗しました。";
$lang['courseedit_err_msg_delete_table_theme'] = "テーマテーブルの削除時に失敗しました。";
$lang['courseedit_err_msg_delete_table_material_play'] = "教材再生時間保持テーブルの削除時に失敗しました。";
$lang['courseedit_err_msg_delete_table_material'] = "教材テーブルの削除時に失敗しました。";
$lang['courseedit_err_msg_delete_table_task'] = "課題情報テーブルの削除時に失敗しました。";
$lang['courseedit_err_msg_delete_table_task_result'] = "課題解答情報テーブルの削除時に失敗しました。";
$lang['courseedit_err_msg_delete_table_students'] = "受講者情報テーブルの削除時に失敗しました。";
$lang['courseedit_err_msg_delete_table_diary'] = "日記テーブルの削除時に失敗しました。";
$lang['courseedit_err_msg_delete_table_diaryhide'] = "日記非表示情報テーブルの削除時に失敗しました。";
$lang['courseedit_err_msg_delete_table_diarylike'] = "日記LIKEテーブルの削除時に失敗しました。";
$lang['courseedit_err_msg_delete_table_forum'] = "フォーラムテーブルの削除時に失敗しました。";
$lang['courseedit_err_msg_delete_table_forummanage'] = "フォーラム管理テーブルの削除時に失敗しました。";
$lang['courseedit_complete_msg_regist'] = "登録しました。";
$lang['courseedit_tooltip_guide'] = "ガイド";
$lang['courseedit_use_schedule_guide1'] = "講座の期間を設定し、各テーマに日付と期限を設定すると、受講者は設定した期間にテーマを受講できます";
$lang['courseedit_use_schedule_guide2'] = "設定の変更は、すべてのテーマが非公開で、テーマの日付、期限がひとつも設定されていない時に変更が可能です";
$lang['courseedit_use_schedule_guide3'] = "講座情報はテーマを作成する前に、十分に検討してから登録してください";
$lang['courseedit_permit_guest_guide'] = "テーマが登録されている場合は変更できません";
