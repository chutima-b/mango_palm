<?php
/*
 * アクセスエラー画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['access_error_header_pagetitle'] = "アクセスエラー画面";
$lang['access_error_pagetitle'] = "アクセスエラー";
$lang['access_error_errormsg'] = "この画面にアクセスすることは出来ません。";
