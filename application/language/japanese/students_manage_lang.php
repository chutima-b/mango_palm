<?php
/*
 * 受講者管理画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['students_manage_header_pagetitle'] = "受講者管理画面";
$lang['students_manage_pagetitle'] = "受講者管理";
$lang['students_manage_item_courseid'] = "講座ID";
$lang['students_manage_item_coursename'] = "講座名";
$lang['students_manage_item_genre'] = "ジャンル";
$lang['students_manage_item_lecturer'] = "講師名";
$lang['students_manage_item_period'] = "期間";
$lang['students_manage_button_new_students'] = "新しい受講者";
$lang['students_manage_list_item_userid'] = "ユーザーID";
$lang['students_manage_list_item_nickname'] = "ニックネーム";
$lang['students_manage_button_release'] = "解除";
$lang['students_manage_button_cancel'] = "キャンセル";
$lang['students_manage_release_confim_title'] = "解除確認";
$lang['students_manage_release_confim'] = "指定の受講者を解除してもよろしいですか？";
