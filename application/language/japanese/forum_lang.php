<?php
/*
 * フォーラム画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['forum_header_pagetitle'] = "フォーラム画面";
$lang['forum_pagetitle'] = "フォーラム";
$lang['forum_button_title_edit'] = "編集";
$lang['forum_button_title_delete'] = "削除";
$lang['forum_button_title_post'] = "投稿";
$lang['forum_button_title_cancel'] = "キャンセル";
$lang['forum_button_title_send'] = "送信";
$lang['forum_textarea_default_message'] = "メッセージ";
$lang['forum_confim_title'] = "削除確認";
$lang['forum_delete_confim'] = "指定の投稿を削除してもよろしいですか？";
$lang['forum_script_err_msg_postmsg_required'] = "メッセージを入力してください";
$lang['forum_script_err_msg_postmsg_maxlength'] = "文字以内で入力してください";
