<?php
/*
 * 課題画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['task_header_pagetitle'] = "課題画面";
$lang['task_pagetitle'] = "課題";
$lang['task_btn_file'] = "ファイル";
$lang['task_btn_noanswer'] = "無解答";
$lang['task_btn_submit'] = "解答";
$lang['task_valid_msg_require_choose'] = "選択してください";
$lang['task_valid_msg_require_text'] = "入力してください";
