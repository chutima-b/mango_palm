<?php
/*
 * 課題編集画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['task_edit_header_pagetitle'] = "課題編集画面";
$lang['task_edit_pagetitle'] = "課題編集";
$lang['task_edit_task_name'] = "タイトル";
$lang['task_edit_description'] = "問題";
$lang['task_edit_task_type'] = "解答の形式";
$lang['task_edit_evaluation_points'] = "評価点";
$lang['task_edit_select_un_selected'] = "選択してください";
$lang['task_edit_select_radio'] = "単一選択";
$lang['task_edit_select_checkbox'] = "複数選択";
$lang['task_edit_select_text'] = "テキスト";
$lang['task_edit_select_file'] = "ファイル";
$lang['task_edit_select_quiz_radio'] = "クイズ（単一選択）";
$lang['task_edit_select_survey_radio'] = "アンケート（単一選択）";
$lang['task_edit_select_survey_checkbox'] = "アンケート（複数選択）";
$lang['task_edit_select_survey_text'] = "アンケート（テキスト）";
$lang['task_edit_choices_textarea'] = "選択肢入力エリア";
$lang['task_edit_quiz_answer'] = "正解";
$lang['task_edit_comment'] = "解説";
$lang['task_edit_btn_regist'] = "登録";
$lang['task_edit_err_msg_taskname_required'] = $lang['task_edit_task_name']."を入力してください";
$lang['task_edit_err_msg_taskname_maxlength'] = "文字以内で入力してください";
$lang['task_edit_err_msg_description_required'] = $lang['task_edit_description']."を入力してください";
$lang['task_edit_err_msg_description_maxlength'] = "文字以内で入力してください";
$lang['task_edit_err_msg_task_type_required'] = $lang['task_edit_task_type']."を選択してください";
$lang['task_edit_err_msg_evaluation_points'] = $lang['task_edit_evaluation_points']."を入力してください";
$lang['task_edit_err_msg_choices_textarea_required'] = "選択肢を改行して入力してください";
$lang['task_edit_err_msg_evaluation_points_over_100'] = "100以下の数値を入力してください";
$lang['task_edit_err_msg_comment_maxlength'] = "文字以内で入力してください";
$lang['task_edit_err_msg_quiz_answer_required'] = "クイズの正解を選択してください";
$lang['task_edit_err_msg_regist'] = "課題の登録時にエラーが発生しました。";
$lang['task_edit_err_msg_update'] = "課題の更新時にエラーが発生しました。";
