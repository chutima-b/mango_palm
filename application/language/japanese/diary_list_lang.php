<?php
/*
 * 日記一覧画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['diary_list_header_pagetitle'] = "日記一覧画面";
$lang['diary_list_pagetitle'] = "日記一覧";
