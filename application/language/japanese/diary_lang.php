<?php
/*
 * 日記画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['diary_header_pagetitle'] = "日記画面";
$lang['diary_pagetitle'] = "日記";
$lang['diary_button_title_edit'] = "編集";
$lang['diary_button_title_diarylist'] = "日記一覧";
$lang['diary_button_title_diarywrite'] = "日記を書く";
$lang['diary_button_title_cancel'] = "キャンセル";
$lang['diary_button_title_send'] = "送信";
$lang['diary_button_title_delete'] = "削除";
$lang['diary_link_like_add'] = "Like!";
$lang['diary_link_like_del'] = "Like!を取り消す";
$lang['diary_confim_title'] = "削除確認";
$lang['diary_delete_confim'] = "指定の日記を削除してもよろしいですか？";
$lang['diary_textarea_default_message'] = "メッセージ";
$lang['diary_script_err_msg_postmsg_required'] = "メッセージを入力してください";
$lang['diary_script_err_msg_postmsg_maxlength'] = "文字以内で入力してください";
