<?php
/*
 * システム管理（ジャンル）画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['genremanager_header_pagetitle'] = "システム管理（ジャンル）画面";
$lang['genremanager_pagetitle'] = "システム管理（ジャンル）";
$lang['genremanager_containertitle'] = "システム管理";
$lang['genremanager_tab_usermanager'] = "ユーザー管理";
$lang['genremanager_tab_genremanager'] = "ジャンル";
$lang['genremanager_tab_settings'] = "設定";
$lang['genremanager_item_genre_count'] = "ジャンル数";
$lang['genremanager_item_course_count'] = "講座数";
$lang['genremanager_button_new_genre'] = "新しいジャンル";
$lang['genremanager_list_column_icon'] = "アイコン";
$lang['genremanager_list_column_genreid'] = "ジャンルID";
$lang['genremanager_list_column_genrename'] = "ジャンル名";
$lang['genremanager_list_column_course_count'] = "講座数";
$lang['genremanager_button_edit'] = "編集";
$lang['genremanager_regist_item_genreid'] = "ジャンルID";
$lang['genremanager_regist_item_genrename'] = "ジャンル名";
$lang['genremanager_regist_item_icon'] = "アイコン";
$lang['genremanager_regist_item_delete_icon'] = "アイコンを削除する";
$lang['genremanager_button_regist'] = "登録";
$lang['genremanager_button_delete'] = "削除";
$lang['genremanager_button_cancel'] = "キャンセル";
$lang['genremanager_delete_confim_title'] = "削除確認";
$lang['genremanager_delete_confim'] = "指定のジャンルを削除してもよろしいですか？";
$lang['genremanager_script_err_msg_genreid_required'] = "ジャンルIDを入力してください";
$lang['genremanager_script_err_msg_genreid_maxlength'] = "文字以内で入力してください";
$lang['genremanager_script_err_msg_genrename_required'] = "ジャンル名を入力してください";
$lang['genremanager_script_err_msg_genrename_maxlength'] = "文字以内で入力してください";
$lang['genremanager_error_msg_duplication_1'] = "ジャンルIDが重複しています";
$lang['genremanager_error_msg_duplication_2'] = "ジャンル名が重複しています";
$lang['genremanager_error_msg_duplication_3'] = "ジャンルIDとジャンル名が重複しています";
$lang['genremanager_error_msg_update_icon_file'] = "アイコンファイル更新時にエラーが発生しました。";
