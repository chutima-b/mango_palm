<?php
/*
 * フィードバック画面言語ファイル
 *
 * @author Kazu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['feedback_header_pagetitle'] = "フィードバック画面";
$lang['feedback_pagetitle'] = "フィードバック";
$lang['feedback_item_userid'] = "ユーザーID";
$lang['feedback_item_nickname'] = "ニックネーム";
$lang['feedback_material_header'] = "教材";
$lang['feedback_input_points'] = "評価点";
$lang['feedback_input_feedback'] = "解説";
$lang['feedback_button_download'] = "ダウンロード";
$lang['feedback_button_draft_save'] = "下書き保存";
$lang['feedback_button_feedback'] = "フィードバック";
$lang['feedback_complete_msg'] = "フィードバックを送信しました";
$lang['feedback_script_err_msg_point_maxlength'] = "文字以内で入力してください";
$lang['feedback_script_err_msg_feedbackmessage_maxlength'] = "文字以内で入力してください";
$lang['feedback_script_err_msg_feedbackpoints_required'] = "評価点を入力してください";
$lang['feedback_script_err_msg_feedbackpoints_maxvalue_1'] = "評価最大の";
$lang['feedback_script_err_msg_feedbackpoints_maxvalue_2'] = "ポイント以下で入力してください";
$lang['feedback_script_err_msg_feedbackpoints_minvalue'] = "0以上で入力してください";
