<?php
/*
 * プロフィール画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['profile_header_pagetitle'] = "プロフィール画面";
$lang['profile_pagetitle'] = "プロフィール";
$lang['profile_nickname'] = "ニックネーム";
$lang['profile_introduction'] = "自己紹介";
$lang['profile_introduction_description'] = "文字まで";
$lang['profile_chat_use'] = "チャットを利用する";
$lang['profile_passwd'] = "現在のパスワード";
$lang['profile_new_passwd'] = "新しいパスワード";
$lang['profile_new_passwd_confirm'] = "新しいパスワード確認";
$lang['profile_regist'] = "登録";
$lang['profile_wrong_passwd'] = "現在のパスワードが間違っています";
$lang['profile_alpha_numeric_new_passwd'] = "新しいパスワードは半角英数記号で入力してください";
$lang['profile_script_err_msg_nickname_required'] = "ニックネームを入力してください";
$lang['profile_script_err_msg_nickname_maxlength'] = "文字以内で入力してください";
$lang['profile_script_err_msg_introduction_maxlength'] = "文字以内で入力してください";
$lang['profile_script_err_msg_passwd_required'] = "パスワードを入力してください";
$lang['profile_script_err_msg_new_passwd_confirm_required'] = "新しいパスワード確認を入力してください";
$lang['profile_script_err_msg_new_passwd_confirm_equalto'] = "新しいパスワードと新しいパスワード確認が一致しません";
$lang['profile_script_err_msg_maxlength'] = "文字以内で入力してください";
$lang['profile_script_err_msg_minlength'] = "文字以上で入力してください";
