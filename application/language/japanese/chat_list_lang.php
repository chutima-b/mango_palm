<?php
/*
 * チャット一覧画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['chat_list_header_pagetitle'] = "チャット一覧画面";
$lang['chat_list_pagetitle'] = "チャット一覧";
$lang['chat_list_auth_student'] = "メンバー";
$lang['chat_list_auth_teacher'] = "管理";
$lang['chat_list_auth_op_manager'] = "運用管理者";
$lang['chat_list_auth_sys_manager'] = "システム管理者";
