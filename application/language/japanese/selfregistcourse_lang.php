<?php
/*
 * 講座申込画面（受講講座自己登録）言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['selfregistcourse_header_pagetitle'] = "講座申込画面";
$lang['selfregistcourse_pagetitle'] = "講座申込";
$lang['selfregistcourse_course_count_prefix'] = "全";
$lang['selfregistcourse_course_count'] = "講座";
$lang['selfregistcourse_col_course_code'] = "講座ID";
$lang['selfregistcourse_col_course_name'] = "講座名";
$lang['selfregistcourse_col_lecturer'] = "講師";
$lang['selfregistcourse_col_theme_count'] = "テーマ";
$lang['selfregistcourse_col_students_count'] = "受講者";
$lang['selfregistcourse_col_period'] = "期間";
$lang['selfregistcourse_btn_course_info'] = "講座を見る";
$lang['selfregistcourse_modal_lecturer'] = "講師";
$lang['selfregistcourse_modal_course_description'] = "講座紹介";
$lang['selfregistcourse_modal_btn_cancel'] = "キャンセル";
$lang['selfregistcourse_modal_btn_attend'] = "受講する";
$lang['selfregistcourse_err_msg_regist'] = "受講講座登録時にエラーが発生しました";
