<?php
/*
 * 参加者登録画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['students_regist_header_pagetitle_students'] = "受講者登録画面";
$lang['students_regist_header_pagetitle_teacher'] = "講師登録画面";
$lang['students_regist_header_pagetitle_op_manager'] = "運用管理者登録画面";
$lang['students_regist_pagetitle_students'] = "受講者登録";
$lang['students_regist_pagetitle_teacher'] = "講師登録";
$lang['students_regist_pagetitle_op_manager'] = "運用管理者登録";
$lang['students_regist_saerch_userid'] = "ユーザーID";
$lang['students_regist_search_familyname'] = "姓";
$lang['students_regist_search_firstname'] = "名";
$lang['students_regist_button_search'] = "検索";
$lang['students_regist_list_userid'] = "ユーザーID";
$lang['students_regist_list_familyname'] = "姓";
$lang['students_regist_list_firstname'] = "名";
$lang['students_regist_list_nickname'] = "ニックネーム";
$lang['students_regist_button_regist'] = "登録";
$lang['students_regist_script_err_msg_userid_maxlength'] = "文字以内で入力してください";
$lang['students_regist_script_err_msg_family_maxlength'] = "文字以内で入力してください";
$lang['students_regist_script_err_msg_first_maxlength'] = "文字以内で入力してください";
$lang['students_regist_error_msg_none_students'] = "有効なユーザーが存在しません。";
