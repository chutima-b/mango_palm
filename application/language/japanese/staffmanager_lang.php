<?php
/*
 *  講師運用管理者登録画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['staffmanager_header_pagetitle'] = "講師運用管理者登録画面";
$lang['staffmanager_pagetitle'] = "講師運用管理者登録";
$lang['staffmanager_teachername'] = "講師";
$lang['staffmanager_op_managername'] = "運用管理者";
$lang['staffmanager_unregistered'] = "（未登録）";
$lang['staffmanager_btn_regist_teacher'] = "講師登録";
$lang['staffmanager_btn_regist_op_manager'] = "運用管理者登録";
