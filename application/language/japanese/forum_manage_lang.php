<?php
/*
 * フォーラム管理画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['forum_manage_header_pagetitle'] = "フォーラム管理画面";
$lang['forum_manage_pagetitle'] = "フォーラム管理";
$lang['forum_manage_newforum'] = "新しいフォーラム";
$lang['forum_manage_forumname'] = "フォーラム名";
$lang['forum_manage_owner'] = "オーナー";
$lang['forum_manage_posts'] = "投稿数";
$lang['forum_manage_latest'] = "最新";
$lang['forum_manage_button_stop'] = "停止";
$lang['forum_manage_button_resumption'] = "再開";
$lang['forum_manage_entry_title'] = "フォーラム名";
$lang['forum_manage_entry_input_defalut'] = "フォーラム名を入力して下さい。";
$lang['forum_manage_textarea_default_message'] = "メッセージ";
$lang['forum_manage_button_cancel'] = "キャンセル";
$lang['forum_manage_button_registration'] = "登録";
$lang['forum_manage_confim_title'] = "削除確認";
$lang['forum_manage_delete_confim'] = "指定のフォーラムを削除してもよろしいですか？";
$lang['forum_manage_button_delete'] = "削除";
$lang['forum_manage_err_msg_forumname_regist'] = "フォーラム管理情報の登録時にエラーが発生しました。";
$lang['forum_manage_script_err_msg_forumname_required'] = "フォーラム名を入力してください";
$lang['forum_manage_script_err_msg_forumname_maxlength'] = "文字以内で入力してください";
$lang['forum_manage_script_err_msg_postmsg_required'] = "メッセージを入力してください";
$lang['forum_manage_script_err_msg_postmsg_maxlength'] = "文字以内で入力してください";
