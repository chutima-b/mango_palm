<?php
/*
 * テーマ画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['theme_header_pagetitle'] = "テーマ画面";
$lang['theme_pagetitle'] = "テーマ";
$lang['theme_time_limit'] = "提出期限";
$lang['theme_submit_at'] = "提出日";
$lang['theme_feedback_at'] = "フィードバック";
$lang['theme_examination'] = "出題";
$lang['theme_evaluation'] = "評価";
$lang['theme_teaching_materialtitle'] = "教材";
$lang['theme_task_comp'] = "完了";
$lang['theme_task_incomp'] = "未完了";
$lang['theme_task_item_points'] = "評価点";
$lang['theme_task_item_feedback'] = "フィードバック";
$lang['theme_task_item_comment'] = "解説";
$lang['theme_task_button_answer'] = "解答する";
$lang['theme_task_button_submit'] = "課題の提出";
$lang['theme_task_button_attended'] = "受講終了";
$lang['theme_task_link_forum'] = "フォーラム";
$lang['theme_task_link_chat'] = "チャット";
$lang['theme_task_link_diary'] = "日記";
$lang['theme_contents_diary'] = "日記";
$lang['theme_contents_diary_link'] = "もっと見る...";
$lang['theme_contents_forum'] = "フォーラム";
$lang['theme_complete_msg_submit'] = "課題を提出しました";
$lang['theme_complete_msg_attend'] = "受講終了しました";
$lang['theme_error_msg_submit'] = "課題の提出時にエラーが発生しました";
$lang['theme_error_msg_not_success_material_videos'] = "次の教材が規定された視聴時間に満たないため提出できません";
$lang['theme_guide_task'] = "テーマに日付、期限が設定されている場合、課題の解答、提出はその期間内だけになります";
$lang['theme_label_close'] = "非公開";
