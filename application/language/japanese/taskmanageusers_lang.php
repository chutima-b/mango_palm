<?php
/*
 * 受講者課題画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['taskmanageusers_header_pagetitle'] = "受講者課題画面";
$lang['taskmanageusers_pagetitle'] = "受講者課題";
$lang['taskmanageusers_participants'] = "受講者";
$lang['taskmanageusers_time_limit'] = "提出期限";
$lang['taskmanageusers_table_userid'] = "ユーザーID";
$lang['taskmanageusers_table_lastname'] = "姓";
$lang['taskmanageusers_table_firstname'] = "名";
$lang['taskmanageusers_table_attend_at'] = "受講日";
$lang['taskmanageusers_table_feedback_at'] = "フィードバック";
$lang['taskmanageusers_table_feedback_points'] = "評価";
$lang['taskmanageusers_btn_feedback'] = "フィードバック";
$lang['taskmanageusers_btn_download'] = "ダウンロード";
$lang['taskmanageusers_csv_item_correct'] = "正解";
$lang['taskmanageusers_csv_item_incorrect'] = "不正解";
$lang['taskmanageusers_csv_column_name_userid'] = "ユーザーID";
$lang['taskmanageusers_csv_column_name_nickname'] = "ニックネーム";
$lang['taskmanageusers_csv_column_name_lastname'] = "姓";
$lang['taskmanageusers_csv_column_name_firstname'] = "名";
$lang['taskmanageusers_csv_column_name_genreid'] = "ジャンルID";
$lang['taskmanageusers_csv_column_name_genrename'] = "ジャンル";
$lang['taskmanageusers_csv_column_name_coursecode'] = "講座ID";
$lang['taskmanageusers_csv_column_name_coursename'] = "講座名";
$lang['taskmanageusers_csv_column_name_themename'] = "テーマ";
$lang['taskmanageusers_csv_column_name_themedate'] = "日付";
$lang['taskmanageusers_csv_column_name_submitdate'] = "提出日";
$lang['taskmanageusers_csv_column_name_themelimit'] = "提出期限";
$lang['taskmanageusers_csv_column_name_feedbackdate'] = "フィードバック日";
$lang['taskmanageusers_csv_column_name_moviecnt'] = "動画教材数";
$lang['taskmanageusers_csv_column_name_moviename'] = "動画教材名";
$lang['taskmanageusers_csv_column_name_movietotal'] = "教材動画の総時間";
$lang['taskmanageusers_csv_column_name_movieplaytime'] = "動画教材の再生時間";
$lang['taskmanageusers_csv_column_name_total_feedbackpoint'] = "実際の評価点合計";
$lang['taskmanageusers_csv_column_name_total_evaluationpoint'] = "出題した評価点合計";
$lang['taskmanageusers_csv_column_name_taskcnt'] = "課題数";
$lang['taskmanageusers_csv_column_name_taskname'] = "課題名";
$lang['taskmanageusers_csv_column_name_taskdescription'] = "課題の説明";
$lang['taskmanageusers_csv_column_name_feedbackpoints'] = "評価点";
$lang['taskmanageusers_csv_column_name_quizresult'] = "正解/不正解";
$lang['taskmanageusers_csv_column_name_answer'] = "解答";
$lang['taskmanageusers_csv_column_name_feedbackmessage'] = "解説";
$lang['taskmanageusers_csv_column_name_downloaddate'] = "ダウンロード日時";
$lang['taskmanageusers_guide_download'] = "課題が提出されていないテーマはダウンロードできません";
