<?php
/*
 * 動画再生画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['materialvideo_restart'] = "先頭";
$lang['materialvideo_play'] = "再生";
$lang['materialvideo_rew'] = "戻し";
$lang['materialvideo_fastfwd'] = "送り";
