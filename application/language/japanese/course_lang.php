<?php
/*
 * 講座画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['course_header_pagetitle'] = "Information画面";
$lang['course_pagetitle'] = "Information";
$lang['course_teacher'] = "管理";
$lang['course_description'] = "Contents紹介";
$lang['course_label_style_info'] = "提出済";
$lang['course_label_style_warning'] = "フィードバック";
$lang['course_label_style_primary'] = "閲覧済";
$lang['course_label_style_danger'] = "終了しました";
$lang['course_label_close'] = "非公開";
$lang['course_button_attend_primary'] = "もう一度読む";
$lang['course_button_attend_warning'] = "フィードバック";
$lang['course_button_attend_info'] = "提出済";
$lang['course_button_attend_info_yet_submit'] = "閲覧する";
$lang['course_link_forum'] = "フォーラム";
$lang['course_link_chat'] = "チャット";
$lang['course_link_diary'] = "日記";
