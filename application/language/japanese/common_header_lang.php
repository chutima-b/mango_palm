<?php
/*
 * 共通ヘッダー部言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['header_link_back'] = "戻る";
$lang['header_link_home'] = "ホーム";
$lang['header_link_sysmanage'] = "システム管理";
$lang['header_link_home_students'] = "メンバー";
$lang['header_link_home_staff'] = "管理者";
$lang['header_link_profile'] = "プロフィール";
$lang['header_link_logout'] = "ログアウト";
