<?php
/*
 * テーマ編集画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['themeedit_header_pagetitle'] = "テーマ編集画面";
$lang['themeedit_pagetitle'] = "テーマ編集";
$lang['themeedit_input_theme'] = "テーマ";
$lang['themeedit_input_goal'] = "目的";
$lang['themeedit_input_date'] = "日付";
$lang['themeedit_input_description'] = "テーマ説明";
$lang['themeedit_input_time_limit'] = "期限";
$lang['themeedit_input_evaluation_use'] = "課題を作成する";
$lang['themeedit_theme_item_evaluation_points'] = "出題";
$lang['themeedit_input_quiz'] = "クイズ形式にする";
$lang['themeedit_comment_form_header'] = "テーマ、日付、期限、目的、テーマ説明の各入力項目について";
$lang['themeedit_comment_form1'] = "公開中や公開終了のテーマは設定を変更することはできません";
$lang['themeedit_comment_form2'] = "すでに受講済の課題があるテーマは設定を変更することはできません";
$lang['themeedit_comment_evaluation_use1'] = "公開中や公開終了のテーマは設定を変更することはできません";
$lang['themeedit_comment_evaluation_use2'] = "すでに受講済の課題があるテーマは設定を変更することはできません";
$lang['themeedit_comment_evaluation_use3'] = "講座編集でゲストの受講を許可している講座は課題を作成することはできません";
$lang['themeedit_comment_quiz1'] = "課題をクイズ形式にすると、受講者が提出した課題は講師がフィードバックすることなくすぐに結果がわかります";
$lang['themeedit_comment_quiz2'] = "受講済の受講者がいるテーマは設定を変更することはできません";
$lang['themeedit_comment_btn_reg_del'] = "受講済の受講者がいるテーマは登録、削除することはできません";
$lang['themeedit_comment_btn_open1'] = "課題を作成するテーマで、一つも課題が登録されていないテーマは公開できません";
$lang['themeedit_comment_btn_open2'] = "期間を設定している講座は、テーマの日付、期限が登録されていないと公開できません";
$lang['themeedit_comment_btn_open3'] = "非公開のテーマは公開終了にできません";
$lang['themeedit_comment_btn_open4'] = "公開終了にすると再び公開することはできません";
$lang['themeedit_comment_btn_new_material1'] = "新しいテーマは一度登録後、教材の登録が可能になります";
$lang['themeedit_comment_btn_new_material2'] = "受講済の受講者がいるテーマは教材を登録、削除することはできません";
$lang['themeedit_comment_btn_new_material3'] = "公開中や公開終了のテーマは教材を登録、削除することはできません";
$lang['themeedit_comment_btn_new_task1'] = "新しいテーマは一度登録後、課題の登録が可能になります";
$lang['themeedit_comment_btn_new_task2'] = "受講済の受講者がいるテーマは課題を登録、編集、削除することはできません";
$lang['themeedit_comment_btn_new_task3'] = "公開中や公開終了のテーマは課題を登録、編集、削除することはできません";
$lang['themeedit_theme_open'] = "公開する";
$lang['themeedit_theme_close'] = "非公開";
$lang['themeedit_theme_end'] = "公開終了";
$lang['themeedit_input_err_msg_theme'] = "テーマを入力して下さい";
$lang['themeedit_input_err_msg_goal'] = "目的を入力して下さい";
$lang['themeedit_input_err_msg_date'] = "日付を入力して下さい";
$lang['themeedit_input_err_msg_description'] = "テーマ説明を入力して下さい";
$lang['themeedit_input_err_msg_timelimit'] = "期限を入力して下さい";
$lang['themeedit_input_err_msg_date_startdate'] = "日付は講座開始日以降の日付を設定してください。";
$lang['themeedit_input_err_msg_date_enddate'] = "日付は講座終了日以前の日付を設定してください。";
$lang['themeedit_input_err_msg_timelimit_startdate'] = "期限は講座開始日以降の日付を設定してください。";
$lang['themeedit_input_err_msg_timelimit_past_enddate'] = "期限は日付に設定した日以降の日付を設定してください。";
$lang['themeedit_input_err_msg_timelimit_enddate'] = "期限は講座終了日以前の日付を設定してください。";
$lang['themeedit_title_material'] = "教材";
$lang['themeedit_button_entry'] = "登録";
$lang['themeedit_button_new_material'] = "新しい教材";
$lang['themeedit_button_new_task'] = "新しい課題";
$lang['themeedit_button_reset_task_result'] = "提出された課題の削除";
$lang['themeedit_button_edit'] = "編集";
$lang['themeedit_button_delete'] = "削除";
$lang['themeedit_button_cancel'] = "キャンセル";
$lang['themeedit_button_yes'] = "はい";
$lang['themeedit_button_no'] = "いいえ";
$lang['themeedit_delete_confim_title'] = "削除確認";
$lang['themeedit_themedelete_confim'] = "削除すると、教材、課題のデータなど、このテーマに関連するデータがすべて削除されます。<br>テーマを削除してもよろしいですか？";
$lang['themeedit_themedelete_confim_for_manager'] = "このテーマは公開中、または公開終了しているか、すでに受講されているテーマです。<br>削除すると、教材、課題、課題の解答データなど、このテーマに関連するデータがすべて削除されます。<br>テーマを削除してもよろしいですか？";
$lang['themeedit_task_item_evaluation_points'] = "評価点";
$lang['themeedit_task_item_btn_file'] = "ファイル";
$lang['themeedit_taskdelete_confim'] = "指定の課題を削除してもよろしいですか？";
$lang['themeedit_materialdelete_confim'] = "指定の教材を削除してもよろしいですか？";
$lang['themeedit_material_resource'] = "リソース";
$lang['themeedit_material_description'] = "リンク文字列";
$lang['themeedit_material_file_select'] = "ファイル選択";
$lang['themeedit_material_overlap_error'] = "同一講座の同じテーマで同一の教材説明は設定できません。";
$lang['themeedit_material_guide1'] = "アップロードできるファイル";
$lang['themeedit_material_guide2'] = ".mp4 .mov";
$lang['themeedit_material_guide3'] = ".jpg .jpeg .png .pdf";
$lang['themeedit_material_guide4'] = "外部リンクを指定する場合、http://またはhttps://から入力してください。";
$lang['themeedit_material_play_end'] = "最後まで再生する";
$lang['themeedit_material_button_regist'] = "登録";
$lang['themeedit_script_err_msg_theme_maxlength'] = "文字以内で入力してください";
$lang['themeedit_script_err_msg_goal_maxlength'] = "文字以内で入力してください";
$lang['themeedit_material_script_err_msg_description_maxlength'] = "文字以内で入力してください";
$lang['themeedit_material_script_err_msg_description_required'] = "説明を入力して下さい";
$lang['themeedit_material_script_err_msg_resource_maxlength'] = "文字以内で入力してください";
$lang['themeedit_material_script_err_msg_resource_required'] = "リソースを入力して下さい";
$lang['themeedit_err_msg_hasnt_task'] = "課題が作成されていないため公開できません。";
$lang['themeedit_complete_msg_regist'] = "登録しました。";
$lang['themeedit_complete_msg_update'] = "更新しました。";
$lang['themeedit_modal_msg_change_evaluation_yes'] = "「".$lang['themeedit_button_entry']."」ボタンを押してテーマの変更を保存してください";
$lang['themeedit_modal_msg_change_quiz'] = "「".$lang['themeedit_input_quiz']."」の状態を変更しました。<br>「".$lang['themeedit_button_entry']."」ボタンを押下して状態を保存するまで課題の作成は出来ません。";
$lang['themeedit_modal_msg_reset_task_result'] = "すべての受講者が提出した課題が削除されます<br>よろしいですか？";
$lang['themeedit_tooltip_guide'] = "ガイド";
