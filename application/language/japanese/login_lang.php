<?php
/*
 * ログイン画面言語ファイル
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['login_header_pagetitle'] = "ログイン画面";
$lang['login_pagetitle'] = "ログイン";
$lang['login_userid'] = "ユーザーID";
$lang['login_passwd'] = "パスワード";
$lang['login_continue'] = "ログインしたままにする。";
$lang['login_btn_login'] = "ログイン";
$lang['login_btn_login_guest'] = "ゲストとしてログイン";
$lang['login_err_msg_login'] = "ユーザーIDかパスワードが異なります。";
