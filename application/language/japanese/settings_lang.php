<?php
/*
 * システム管理（設定）画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['settings_header_pagetitle'] = "システム管理（設定）画面";
$lang['settings_pagetitle'] = "システム管理（設定）";
$lang['settings_headertitle'] = "システム管理";
$lang['settings_link_usermanager'] = "ユーザー管理";
$lang['settings_link_genremanager'] = "ジャンル";
$lang['settings_link_settings'] = "設定";
$lang['settings_item_title'] = "タイトル";
$lang['settings_item_introduction'] = "イントロダクション";
$lang['settings_item_background'] = "背景";
$lang['settings_checkbox_delete_logo'] = "ロゴ画像を削除する";
$lang['settings_checkbox_use_background_img'] = "壁紙を使う";
$lang['settings_btn_logo_img'] = "ロゴ画像";
$lang['settings_btn_background_img'] = "壁紙";
$lang['settings_btn_regist'] = "登録";
$lang['settings_err_msg_regist'] = "登録時にエラーが発生しました";
$lang['settings_script_err_msg_backgroundimg_required'] = "壁紙を選択してください";
$lang['settings_script_err_msg_title_maxlength'] = "文字以内で入力してください";
$lang['settings_complete_msg_regist'] = "登録しました。";
