<?php
/*
 * 課題管理画面言語ファイル
 *
 * @author Kazu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['taskmanager_header_pagetitle'] = "課題管理画面";
$lang['taskmanager_pagetitle'] = "課題管理";
$lang['taskmanager_participants'] = "人の受講者";
$lang['taskmanager_label_msg_attended'] = "受講済";
$lang['taskmanager_label_msg_warning'] = "フィードバック";
$lang['taskmanager_label_msg_danger'] = "期限切れ";
$lang['taskmanager_label_msg_closed'] = "非公開";
$lang['taskmanager_label_msg_publicend'] = "公開終了";
$lang['taskmanager_btn_reports'] = "レポート";
