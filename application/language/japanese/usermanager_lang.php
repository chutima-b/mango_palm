<?php
/*
 * システム管理（ユーザー管理）画面言語ファイル
 *
 * @author Kazu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['usermanager_header_pagetitle'] = "システム管理（ユーザー管理）画面";
$lang['usermanager_pagetitle'] = "システム管理（ユーザー管理）";
$lang['usermanager_headertitle'] = "システム管理";
$lang['usermanager_link_usermanager'] = "ユーザー管理";
$lang['usermanager_link_genremanager'] = "ジャンル";
$lang['usermanager_link_settings'] = "設定";
$lang['usermanager_search_userid'] = "ユーザーID";
$lang['usermanager_search_surname'] = "姓";
$lang['usermanager_search_firstname'] = "名";
$lang['usermanager_btn_search'] = "検索";
$lang['usermanager_btn_upload'] = "アップロード";
$lang['usermanager_btn_newuser'] = "新しいユーザー";
$lang['usermanager_link_before'] = "前";
$lang['usermanager_link_next'] = "次";
$lang['usermanager_users'] = "件";
$lang['usermanager_table_userid'] = "ユーザーID";
$lang['usermanager_table_surname'] = "姓";
$lang['usermanager_table_firstname'] = "名";
$lang['usermanager_table_nickname'] = "ニックネーム";
$lang['usermanager_table_course_count'] = "講座数";
$lang['usermanager_table_login_at'] = "最終ログイン日時";
$lang['usermanager_btn_stop'] = "停止";
$lang['usermanager_btn_restart'] = "再開";
$lang['usermanager_btn_edit'] = "編集";
$lang['usermanager_btn_download'] = "ダウンロード";
$lang['usermanager_dialog_userid'] = "ユーザーID";
$lang['usermanager_dialog_passwd'] = "パスワード";
$lang['usermanager_dialog_passwd_confirm'] = "パスワード確認";
$lang['usermanager_dialog_nickname'] = "ニックネーム";
$lang['usermanager_dialog_surname'] = "姓";
$lang['usermanager_dialog_firstname'] = "名";
$lang['usermanager_dialog_placeholder_passwd'] = "パスワードは表示されません";
$lang['usermanager_dialog_course'] = "受講中の講座";
$lang['usermanager_btn_cancel'] = "キャンセル";
$lang['usermanager_btn_regist'] = "登録";
$lang['usermanager_confirm_msg_title_stop'] = "停止確認";
$lang['usermanager_confirm_msg_body_stop'] = " ユーザーを停止します。よろしいですか？";
$lang['usermanager_confirm_msg_title_restart'] = "再開確認";
$lang['usermanager_confirm_msg_body_restart'] = " ユーザーを再開します。よろしいですか？";
$lang['usermanager_confirm_msg_title_delete'] = "削除確認";
$lang['usermanager_confirm_msg_body_delete'] = " ユーザーを削除します。よろしいですか？";
$lang['usermanager_confirm_btn_cancel'] = "キャンセル";
$lang['usermanager_confirm_btn_ok'] = "実行";
$lang['usermanager_already_used_userid'] = "ユーザーIDは既に使われております";
$lang['usermanager_alpha_numeric_passwd'] = "パスワードは半角英数記号で入力してください";
$lang['usermanager_err_msg_regist'] = "ユーザー登録時にエラーが発生しました。";
$lang['usermanager_err_msg_update'] = "ユーザー更新時にエラーが発生しました。";
$lang['usermanager_err_msg_update_picture_file'] = "画像ファイル更新時にエラーが発生しました";
$lang['usermanager_err_msg_stop'] = "停止時にエラーが発生しました";
$lang['usermanager_err_msg_restart'] = "再開時にエラーが発生しました";
$lang['usermanager_err_msg_delete'] = "削除時にエラーが発生しました";
$lang['usermanager_err_msg_upload'] = "アップロード時にエラーが発生しました";
$lang['usermanager_err_msg_upload_format'] = "CSVファイルのフォーマットが不正です";
$lang['usermanager_err_msg_upload_userid_length'] = "ユーザーIDの文字数が不正です";
$lang['usermanager_err_msg_upload_userid_alpha_numeric'] = "ユーザーIDは半角英数で記述してください";
$lang['usermanager_err_msg_upload_passwd_length'] = "パスワードの文字数が不正です";
$lang['usermanager_err_msg_upload_passwd_alpha_numeric'] = "パスワードは半角英数記号で記述してください";
$lang['usermanager_err_msg_upload_surname_length'] = "姓の文字数が不正です";
$lang['usermanager_err_msg_upload_firstname_length'] = "名の文字数が不正です";
$lang['usermanager_script_err_msg_userid_required'] = "ユーザーIDを入力してください";
$lang['usermanager_script_err_msg_userid_alpha_numeric'] = "半角英数字で入力して下さい";
$lang['usermanager_script_err_msg_surname_required'] = "姓を入力してください";
$lang['usermanager_script_err_msg_firstname_required'] = "名を入力してください";
$lang['usermanager_script_err_msg_nickname_required'] = "ニックネームを入力してください";
$lang['usermanager_script_err_msg_passwd_required'] = "パスワードを入力してください";
$lang['usermanager_script_err_msg_passwd_confirm_required'] = "パスワード確認を入力してください";
$lang['usermanager_script_err_msg_passwd_confirm_equalto'] = "パスワードとパスワード確認が一致しません";
$lang['usermanager_script_err_msg_maxlength'] = "文字以内で入力してください";
$lang['usermanager_script_err_msg_minlength'] = "文字以上で入力してください";
$lang['usermanager_csvdata_validityflg_true'] = "有効";
$lang['usermanager_csvdata_validityflg_false'] = "無効";
$lang['usermanager_guide_title'] = "アップロードファイルについて";
$lang['usermanager_guide_item1'] = "UTF-8（BOMなし）またはシフトJIS";
$lang['usermanager_guide_item2'] = "ユーザーID（英数50文字まで）,パスワード（英数記号6文字以上）,姓（50文字まで）,名（50文字まで）[改行]";
$lang['usermanager_guide_item3'] = "同じユーザーIDは上書きされます";
$lang['usermanager_tooltip_guide'] = "ガイド";
