<?php
/*
 * チャット画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['chat_header_pagetitle'] = "チャット画面";
$lang['chat_pagetitle'] = "チャット";
$lang['chat_message_placeholder'] = "メッセージ...";
$lang['chat_btn_submit'] = "送信";
$lang['chat_dialog_error_title'] = "エラー";
$lang['chat_dialog_error_body_empty_message'] = "メッセージを入力してください";
$lang['chat_dialog_error_body_str_over_message'] = "256文字以内で入力してください";
