<?php
/*
 * チャット管理画面言語ファイル
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['chatmanager_header_pagetitle'] = "チャット管理画面";
$lang['chatmanager_pagetitle'] = "チャット管理";
$lang['chatmanager_label_students'] = "メンバー";
$lang['chatmanager_select_msg'] = "選択してください";
$lang['chatmanager_dialog_error_title'] = "エラー";
