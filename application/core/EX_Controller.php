<?php
/*
 * ベースコントローラー
 *
 * ログイン画面以外の、各画面はこのコントローラーを継承する。
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class EX_Controller extends CI_Controller {
	/** コントローラー種別. */
	private $controller_kind = 0;
	/** 講座権限. */
	private $course_auth = false;

	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		parent::__construct();

		// ログインチェック
		if (!$this->session->userdata('is_logged_in')) {
			redirect($this->commonlib->baseUrl().'login');
		}
	}

	/**
	 * 講座権限取得
	 *
	 * 開いている画面の講座での自分の権限を取得する
	 *
	 * @return mixed 講座権限（権限が取得できない場合はfalse）
	 */
	public function getCourseAuth()
	{
		if (!is_numeric($this->course_auth)) {
			return false;
		}
		return intval($this->course_auth);
	}

	/**
	 * 初期化メソッド
	 *
	 * @param int $param_controller_kind コントローラー種別
	 */
	protected function initialize($param_controller_kind)
	{
		$this->controller_kind = $param_controller_kind;

		$_view_data = array('controller_kind' => $this->controller_kind);
		$this->load->vars($_view_data);

		$this->accessJudgement();
	}

	/**
	 * 画面アクセス判定
	 */
	private function accessJudgement()
	{
		if($this->controller_kind === $this->config->item('CONTROLLER_KIND_ACCESS_ERROR')) {
			// アクセスエラー画面は何もしない
			return;
		}

		// 権限取得
		$_authorityInfo = $this->session->userdata('authority');
		$_authority = $_authorityInfo[0]->authority;

		// システム管理者は全画面アクセス可能
		if ($_authority == $this->config->item('AUTH_SYS_MANAGER')) {
			$this->course_auth = $this->config->item('AUTH_SYS_MANAGER');
			return;
		}

		// コントローラーが、画面アクセス判定定義の対象の権限に定義されているか判定
		$_access_config = $this->config->item('ACCESS_JUDGEMENT')[$_authority];
		if (!in_array($this->controller_kind, $_access_config, true)) {
			redirect($this->commonlib->baseUrl().'accesserror');
		}

		if ($this->controller_kind == $this->config->item('CONTROLLER_KIND_SELF_REGIST_COURSE')
		 && $this->config->item('USE_SELF_REGIST_COURSE') == 0) {
			// コントローラーが講座申込画面の場合で、
			// 受講講座自己登録機能使用設定が使用しないに設定されている場合は
			// アクセスエラー
			redirect($this->commonlib->baseUrl().'accesserror');
		}

		// 講座画面アクセス判定対象のコントローラか判定
		$_cource_access_config = $this->config->item('COURSE_ACCESS_JUDGEMENT');
		if (in_array($this->controller_kind, $_cource_access_config, true)) {
			// パラメータから講座IDを取得
			$_course_id = $this->input->get_post('cid', TRUE);
			if (is_null($_course_id)) {
				// 講座IDが無い場合はアクセスエラー
				redirect($this->commonlib->baseUrl().'accesserror');
			}
			// 受講者情報テーブルに、該当講座へ登録されているか判定
			$this->load->model("Students_Model", 'students_model', true);
			$this->course_auth = $this->students_model->getAuthority($_course_id, $_authorityInfo[0]->id);
			if ($this->course_auth === false) {
				// 登録が無い場合はアクセスエラー
				redirect($this->commonlib->baseUrl().'accesserror');
			}
			// 該当講座で受講者権限の場合
			if ($this->course_auth == $this->config->item('AUTH_STUDENT')) {
				// 編集権限が無ければアクセスできない画面の場合
				$_cource_edit_config = $this->config->item('COURSE_EDIT_JUDGEMENT');
				if (in_array($this->controller_kind, $_cource_edit_config, true)) {
					// アクセスエラーにする
					redirect($this->commonlib->baseUrl().'accesserror');
				}
			}
		}
	}

	/**
	 * 画面タイトル設定
	 *
	 * @param string $title
	 */
	protected function setPageTitle($title)
	{
		$_view_data = array('page_title' => $title);
		$this->load->vars($_view_data);
	}

	/**
	 * CSSファイル追加
	 *
	 * @param string $add_css
	 */
	protected function addCssFiles($add_css)
	{
		$_view_data = array('add_css' => $add_css);
		$this->load->vars($_view_data);
	}

	/**
	 * JavaScriptファイル追加
	 *
	 * @param string $add_scripts
	 */
	protected function addScriptFiles($add_scripts)
	{
		$_view_data = array('add_scripts' => $add_scripts);
		$this->load->vars($_view_data);
	}

	/**
	 * 戻り先画面URL設定
	 *
	 * @param string $backUrl
	 */
	protected function setBackUrl($backUrl)
	{
		$_view_data = array('culture_back_url' => $backUrl);
		$this->load->vars($_view_data);
	}

	/**
	 * ホーム画面URL取得.
	 *
	 * 権限に応じたホーム画面のURLを返す.
	 */
	public function homeUrl() {
		if ($this->session->userdata('authority')) {
			$_home_link = $this->commonlib->baseUrl();
			$_authorityInfo = $this->session->userdata('authority');
			if ($_authorityInfo[0]->authority == $this->config->item('AUTH_TEACHER')
			 || $_authorityInfo[0]->authority == $this->config->item('AUTH_OP_MANAGER')
			 || $_authorityInfo[0]->authority == $this->config->item('AUTH_SYS_MANAGER')) {
				$_home_link .= 'staffhome';
			} else {
				$_home_link .= 'studentshome';
			}
			return $_home_link;
		}
		return null;
	}

}
