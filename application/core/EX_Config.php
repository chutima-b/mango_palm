<?php
/*
 * 拡張定義ファイル読み込みクラス
 *
 * CodeIgniterの定義読み込みクラス（CI_Config）を拡張子、当システム独自の定義ファイル読み込み処理を実施する
 *
 * @author Takahiro Kimura
 * @version 1.0.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class EX_Config extends CI_Config {
	/**
	 * コンストラクター
	 */
	public function __construct()
	{
		parent::__construct();

		// 初期化処理実行
		$this->initialize();
	}

	/**
	 * 初期化処理
	 */
	private function initialize()
	{
		// 言語依存設定ファイル読み込み
		// config.phpの$config['language']の設定に応じて、
		// 読み込むべき言語ディレクトリからslms_language.phpを読み込む
		$_lang_dir = empty($this->config['language']) ? 'english' : $this->config['language'];
		$this->load('slms/language/'.$_lang_dir.'/slms_language');
	}
}
