<?php
/*
 * テーマ画面ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('theme_lang');

$theme = $theme_data;
if (!is_null($theme)) {
?>
	<h1 class="sr-only"><?=$this->lang->line('theme_pagetitle')?></h1>
	<div class="container">
		<!--<div class="panel panel-default blur">-->
			<!--<div class="panel-heading"><?php if (!is_null($theme['icon'])) : ?><img class="navpic" src="<?=$theme['icon']?>">&nbsp;<?php endif; ?><?=htmlspecialchars($theme['course_name'])?></div>-->
			<!--<div class="panel-body">-->
<?php
	if (!is_null($complete_msg)) {
?>
				<div class="alert alert-success" role="alert"><?=$complete_msg?></div>
<?php
	}
?>
<?php
	if (!is_null($error_msg)) {
?>
				<div class="alert alert-danger" role="alert"><?=$error_msg?></div>
<?php
	}
?>
				<!--<div class="row">-->
					<!--<div class="col-sm-9">-->
						<div class="panel panel-default blur">
							<!-- mangop.net追加 -->
							<ol class="breadcrumb">
								<li><a href="http://mangop.net/studentshome">ホーム</a></li>
								<li><a href="<?=$culture_back_url?>"><?=htmlspecialchars($theme['course_name'])?></a></li>
								<li class="active"><?=htmlspecialchars($theme['name'])?></li>
							</ol>
							<!-- mangop.net追加 -->
							<div class="panel-heading">
								<?=$theme['theme_date']?> <?=htmlspecialchars($theme['name'])?><?php if ($theme['open'] == 0) : ?>&nbsp;<span class="label label-default"><?=$this->lang->line('theme_label_close')?></span><?php endif; ?>
							</div>
							<div class="panel-body">
								<p><?=nl2br(htmlspecialchars($theme['goal']))?></p>
								<hr>
								<p><?=$theme['description']?></p>
<?php
	if ($theme['evaluation_use'] == 1) {
		// 「課題を作成する」がYESの場合
?>								<hr>
								<dl class="dl-horizontal">
<?php
		if (!$is_quiz) {
			// クイズ以外の場合は「提出期限」を表示する
?>									<dt><?=$this->lang->line('theme_time_limit')?></dt>
									<dd><?=$theme['time_limit']?></dd>
<?php
		}
?>									<dt><?=$this->lang->line('theme_submit_at')?></dt>
									<dd><?=$taskfeedback_data->submit_at?></dd>
<?php
		if (!$is_quiz) {
		// クイズ以外の場合は「フィードバック」を表示する
?>									<dt><?=$this->lang->line('theme_feedback_at')?></dt>
									<dd><?=$taskfeedback_data->feedback_at?></dd>
<?php
		}

		// 「出題」の項目を表示
		// ※クイズ以外の場合
		// 　このテーマの課題の評価点合計の評価点を表示
		// 　クイズの場合
		// 　出題数を表示
		$_evaluation_point_item = $this->lang->line('theme_examination');
		if ($is_feedback) {
			// フィードバック済みの場合は項目名を「評価」に変更
			// ※クイズ以外の場合
			// 　実際の“評価点/評価点”を表示
			// 　クイズの場合
			// 　“クイズの正解数/出題数”を表示
			$_evaluation_point_item = $this->lang->line('theme_evaluation');
		}
?>
									<dt><?=$_evaluation_point_item?></dt>
									<dd><?=$task_point?></dd>
								</dl>
<?php
	}
?>
							</div>
							<!-- mongop.net追加 -->
							<a class="btn btn-default" href="<?=$culture_back_url?>" role="button"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> 戻る</a>
							<!-- mongop.net<div class="panel-footer">削除 -->
						</div>
<?php
	if (count($materials_data) > 0) {
?>
						<div class="panel panel-default blur">
							<div class="panel-heading"><?=$this->lang->line('theme_teaching_materialtitle')?></div>
							<div class="panel-body">
<?php
		foreach ($materials_data as $r) :
?>
								<p>
									<a href="<?=$r['link_path']?>" target="_blank">
<?php
			if ($r['material_type'] == $this->config->item('MATERIAL_TYPE_EXTERNAL')) {
				// 外部リンク
?>
										<span class="label label-primary"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></span>
<?php
			} elseif ($r['material_type'] == $this->config->item('MATERIAL_TYPE_PICTURE')) {
				// 写真
?>
										<span class="label label-primary"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></span>
<?php
			} elseif ($r['material_type'] == $this->config->item('MATERIAL_TYPE_MOVIE')) {
				// 動画
?>
										<span class="label label-primary"><span class="glyphicon glyphicon-film" aria-hidden="true"></span></span>
<?php
			} elseif ($r['material_type'] == $this->config->item('MATERIAL_TYPE_PDF')) {
				// PDF
?>
										<span class="label label-primary"><span class="glyphicon glyphicon-file" aria-hidden="true"></span></span>
<?php
			}
?>
									<?=htmlspecialchars($r['material_description'])?></a>
								</p>
<?php
		endforeach;
?>
							</div>
						</div>
<?php
	}
	if ($task_data !== null and !empty($task_data)) {
?>
						<p class="guide"><?=$this->lang->line('theme_guide_task')?></p>
<?php
		foreach($task_data as $t) :
?>
						<div class="panel panel-default blur">
							<div id="task_<?=$t['id']?>" class="panel-heading"><?=htmlspecialchars($t['task_name'])?></div>
							<div class="panel-body">
<?php
			if (isset($t['is_correct_quiz'])) {
				// クイズの正答情報がある場合
				// 正解/不正解のラベルを表示する
				$_css_correct_color = "correctflag";
				$_css_correct_quiz = "circle";
				if (!$t['is_correct_quiz']) {
					$_css_correct_color = "incorrectflag";
					$_css_correct_quiz = "cross";
				}
?>
								<div class="row">
									<div class="col-xs-12 text-center"><div class="inner <?=$_css_correct_color?>"><span class="inner <?=$_css_correct_quiz?>"></span></div></div>
								</div>
<?php
			}
?>
								<p><?=$t['description']?></p>
								<?=$t['answer']?>
<?php
			if (!is_null($students_data) && intval($students_data['authority']) === $this->config->item('AUTH_STUDENT')) {
				// 該当講座に受講者権限で登録されている場合のみ表示
?>
								<hr>
								<dl class="dl-horizontal">
<?php
				if ($theme['evaluation_use'] == 1 && $theme['quiz'] == 0) {
?>
									<dt><?=$this->lang->line('theme_task_item_points')?></dt>
									<dd><?=$t['points']?></dd>
<?php
				}

				$_item_name_feedback = $this->lang->line('theme_task_item_feedback');
				if ($is_quiz) {
					// クイズの場合は項目名を「フィードバック」から「解説」に変更する
					$_item_name_feedback = $this->lang->line('theme_task_item_comment');
				}
?>
									<dt><?=$_item_name_feedback?></dt>
									<dd><?=nl2br(htmlspecialchars($t['feedback_message']))?></dd>
								</dl>
<?php
				if ($theme['submission_use'] == 1 and !$is_submit) {
?>
								<p><a href="<?=$this->commonlib->baseUrl()?>task?cid=<?=$theme['course_id']?>&thid=<?=$theme['id'] ?>&taid=<?=$t['id'] ?>"><button class="btn btn-success"><?=$this->lang->line('theme_task_button_answer')?></button></a></p>
<?php
				}
			}
?>
							</div>
						</div>
<?php
		endforeach;
	}
	if (!is_null($task_data) && count($task_data) > 0) {
		// 課題データがある場合は「課題の提出」ボタンの表示判定処理を行う
		if (!is_null($students_data) && intval($students_data['authority']) === $this->config->item('AUTH_STUDENT')
		 && $theme['submission_use'] == 1 && !$is_submit) {
			// 該当講座に受講者権限で登録されている場合のみ
			// かつ
			// テーマ日付け～期限内
			// かつ
			// 未提出の場合のみ表示
			$_submit_disabled = '';
			if (!$is_all_answer) {
				// 全ての課題が解答済みとなっていない場合は
				// 課題の提出ボタンを無効にする。
				$_submit_disabled = ' disabled';
			}
?>
						<div class="pull-right">
							<form name="theme_submit" action="<?=$this->commonlib->baseUrl()?>theme/submit?cid=<?=$theme['course_id']?>&thid=<?=$theme['id']?>&pre=<?=$pre?>" method="post">
								<button type="submit" class="btn btn-primary"<?=$_submit_disabled?>><?=$this->lang->line('theme_task_button_submit')?></button>
								<input type="hidden" name="quiz" value="<?=$theme['quiz']?>">
							</form>
						</div>
						<div id="theme_main_bottom_spacer"><!--  --></div>
<?php
		}
	} else {
		// 課題が無い場合は、「受講終了」ボタンを表示
		// 受講終了ボタンの押下で、提出済みにする。
		if (!is_null($students_data) && intval($students_data['authority']) === $this->config->item('AUTH_STUDENT')
		&& $theme['submission_use'] == 1 && !$is_submit) {
?>
						<div class="pull-right">
							<form name="theme_submit" action="<?=$this->commonlib->baseUrl()?>theme/submit?cid=<?=$theme['course_id']?>&thid=<?=$theme['id']?>&pre=<?=$pre?>&attend=true" method="post">
								<button type="submit" class="btn btn-primary"><?=$this->lang->line('theme_task_button_attended')?></button>
							</form>
						</div>
						<div id="theme_main_bottom_spacer"><!--  --></div>
<?php
		}
	}
?>
					<!--</div>-->

<!-- mangop.net日記、フォーラム削除 -->

				<!--</div>-->
			<!--</div>-->
		<!--</div>-->
	</div>
<?php
}
?>
	<?=$script?>
