<?php
/*
 * 教材動画再生画面ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('materialvideo_lang');

// 壁紙
$_wallpaper = '';
if (!is_null($this->session->userdata('wallpaper'))) {
	$_wallpaper = $this->session->userdata('wallpaper');
} else if (isset($tmp_wallpaper)) {
	$_wallpaper = $tmp_wallpaper;
}

// 背景色
$_backcolor = '';
if (!is_null($this->session->userdata('backcolor'))) {
	$_backcolor = $this->session->userdata('backcolor');
} else if (isset($tmp_backcolor)) {
	$_backcolor = $tmp_backcolor;
}

// キャッシュさせない
header('Expires: Thu, 01 Jan 1970 00:00:00 GMT');
header('Last-Modified: '.gmdate( 'D, d M Y H:i:s' ).' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
<?php
$_ua = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
$_is_ios9under = false;
if((strpos($_ua, 'iPhone') !== false)
|| (strpos($_ua, 'iPad') !== false)
|| (strpos($_ua, 'iPod') !== false)) {
	if (preg_match('/OS ([0-9_]+)/', $_ua, $matches)) {
		$_major_ver = explode('_', $matches[1])[0];
		if (is_numeric($_major_ver) && 10 < $_major_ver) {
			$_is_ios9under = true;
		}
	}
}
?>
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
	<link rel="stylesheet" href="<?=$this->commonlib->baseUrl()?>libs/bootstrap-3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=$this->commonlib->baseUrl()?>css/common.css?v=1.0.0">
	<link rel="stylesheet" href="<?=$this->commonlib->baseUrl()?>css/materialvideo.css?v=1.0.0">
	<style>
		body {
			background-color: <?=$_backcolor?>;
		}
<?php
if ($_wallpaper) {
?>
		body,
		.blur::before {
			background: url(<?=$_wallpaper?>) 0 / cover fixed;
		}
<?php
}
?>
	</style>
	<title><?=$title?></title>
	<script src="<?=$this->commonlib->baseUrl()?>libs/jquery/jquery-2.2.4.min.js"></script>
	<script src="<?=$this->commonlib->baseUrl()?>libs/bootstrap-3.3.6/js/bootstrap.min.js"></script>
	<script>
		var videoSrc = '<?=$src?>';
		var playTime = <?=$play_time?>;
		var sendTime = playTime;
		var evaluationUse = <?=$evaluation_use?>;
		var isWatchEnd = <?=$is_watch_end ? 'true' : 'false'?>;
		var isStudents = <?=$is_students ? 'true' : 'false'?>;
		var isPreview = <?=$is_preview ? 'true' : 'false'?>;
		var totalTime = 0;
	</script>
	<script src="<?=$this->commonlib->baseUrl()?>js/materialvideo_common.js?v=1.0.3"></script>
<?php
if ($_is_ios9under) {
?>
	<script src="<?=$this->commonlib->baseUrl()?>js/materialvideo_ios9under.js?v=1.0.0"></script>
<?php
} else {
?>
	<script src="<?=$this->commonlib->baseUrl()?>js/materialvideo.js?v=1.0.2"></script>
<?php
}
?>
</head>
<body>
<?php
if ($_is_ios9under) {
?>
	<canvas id="canvas"></canvas>
	<div class="container-fluid">
<?php
} else {
?>
	<div class="container">
		<p class="embed-responsive embed-responsive-16by9">
			<video id="video" type="<?=$mime?>" webkit-playsinline playsinline autoplay></video>
		</p>
<?php
}
?>
		<div class="form-inline">
			<button id="restart" class="btn btn-default" data-toggle="tooltip" title="<?=$this->lang->line('materialvideo_restart')?>" data-placement="bottom"><span class="glyphicon glyphicon-fast-backward" aria-hidden="true"></span></button>
			<button id="play" class="btn btn-default" data-toggle="tooltip" title="<?=$this->lang->line('materialvideo_play')?>" data-placement="bottom"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></button>
			<div class="btn-group">
				<button id="rew" data-skip="-3" class="btn btn-default" data-toggle="tooltip" title="<?=$this->lang->line('materialvideo_rew')?>" data-placement="bottom"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span></button>
				<button id="fastFwd" data-skip="3" class="btn btn-default" data-toggle="tooltip" title="<?=$this->lang->line('materialvideo_fastfwd')?>" data-placement="bottom"><span class="glyphicon glyphicon-forward" aria-hidden="true"></span></button>
			</div>
			<p class="pull-right"><span id="timeDisplay"></span> / <span id="totalTime"></span></p>
		</div>
	</div>
</body>
</html>
