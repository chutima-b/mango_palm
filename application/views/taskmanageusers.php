<?php
/*
 * 受講者課題画面ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('taskmanageusers_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('taskmanageusers_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=htmlspecialchars($coursename)?></div>
			<div class="panel-body">
				<div class="panel panel-default blur">
					<div class="panel-heading">
						<h4 class="panel-title">
							<?=htmlspecialchars($theme_data->name)?>
						</h4>
					</div>
					<div class="panel-body">
						<dl class="dl-horizontal">
							<dt><?=$this->lang->line('taskmanageusers_time_limit')?></dt>
							<dd><?=$theme_data->time_limit?></dd>
							<dt><?=$this->lang->line('taskmanageusers_participants')?></dt>
							<dd><?=$participants?></dd>
						</dl>
<?php
		$_taskmng_btn_download_disabled = " disabled"; // ダウンロードボタンはデフォルト無効にしておく
		if ($theme_data->enable_download == 1) {
			// 「課題を作成する」がYES
			// かつ
			// 公開中または、公開終了、または、非公開であっても１件以上課題が提出されている場合
			// ダウンロードボタンを有効にする
			// ※enable_downloadはSQLにて設定
			$_taskmng_btn_download_disabled = '';
		}
?>						<hr>
						<div class="text-right">
							<button id="taskmng_btn_download_<?=$cid?>_<?=$thid?>" class="btn btn-info"<?=$_taskmng_btn_download_disabled?>><?=$this->lang->line('taskmanageusers_btn_download')?></button>
							<p class="guide"><?=$this->lang->line('taskmanageusers_guide_download')?></p>
						</div>
						<div id="page_link">
<?php
if ($before_page):
?>
							<a id="before_page" href="<?=$before_page?>"><?=$this->lang->line('usermanager_link_before')?></a>
<?php
endif;
if ($next_page):
?>
							<a id="next_page"  href="<?=$next_page?>"><?=$this->lang->line('usermanager_link_next')?></a>
<?php
endif;
?>						</div>
						<div class="table-responsive touch-scroll">
							<table id="user_list" class="table table-hover">
								<tr>
									<th>&nbsp</th>
									<th><?=$this->lang->line('taskmanageusers_table_userid')?></th>
<?php
		// 姓、名の表示を定義に従い、生成する
		$_name_item_column = $this->commonlib->createFullName(
				"<th>".$this->lang->line('taskmanageusers_table_firstname')."</th>",
				"<th>".$this->lang->line('taskmanageusers_table_lastname')."</th>");
?>									<?=$_name_item_column?>
									<th><?=$this->lang->line('taskmanageusers_table_attend_at')?></th>
									<th><?=$this->lang->line('taskmanageusers_table_feedback_at')?></th>
									<th><?=$this->lang->line('taskmanageusers_table_feedback_points')?></th>
									<th>&nbsp</th>
								</tr>
<?php
		if (!is_null($theme_data->taskmanage_list)) {
			foreach ($theme_data->taskmanage_list as $_taskmanage) {
?>
								<tr>
									<td><img class="img-responsive taskmanageusers_user_img" src="<?=$_taskmanage->picture_file?>"></td>
									<td><?=htmlspecialchars($_taskmanage->userid)?></td>
<?php
			// 姓、名の表示を定義に従い、生成する
			$_name_column = $this->commonlib->createFullName(
					"<td>".htmlspecialchars($_taskmanage->firstname)."</td>",
					"<td>".htmlspecialchars($_taskmanage->surname)."</td>");
?>									<?=$_name_column?>
									<td><?=$_taskmanage->submit_at?></td>
									<td><?=$_taskmanage->feedback_at?></td>
									<td><?=$_taskmanage->points?></td>
									<td>
<?php
				if (strcmp($_taskmanage->submit_at, '-') !== 0
				 && $theme_data->evaluation_use != $this->config->item('EVALUATION_USE_FLAG_OFF')
				 && $theme_data->open == $this->config->item('THEME_OPEN')) {
?>
										<button id="taskmng_btn_feedback_<?=$cid?>_<?=$thid?>_<?=$_taskmanage->uid?>_<?=$page?>" class="btn btn-warning"><?=$this->lang->line('taskmanageusers_btn_feedback')?></button>
<?php
				}
?>									</td>
								</tr>
<?php
			}
		}
?>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?=$script?>
