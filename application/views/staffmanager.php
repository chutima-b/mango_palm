<?php
/*
 * 講師運用管理者登録画面ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

$userid = "";
if (isset($_POST['userid'])) {
	$userid = $_POST['userid'];
}

// 言語ファイル読み込み
$this->lang->load('staffmanager_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('staffmanager_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=htmlspecialchars($course->course_name)?></div>
			<div class="panel-body">
				<form class="form-horizontal">
					<div class="form-group">
						<label for="staffmanager_teachername" class="col-md-4 control-label"><?=$this->lang->line('staffmanager_teachername')?></label>
						<div class="col-md-2">
							<p class="form-control-static"><?=htmlspecialchars($course->teacher_name)?></p>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-4 col-md-8">
							<img class="img-responsive" src="<?=$course->picture_file?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-4 col-md-8">
							<button id="staffmanager_btn_regist_teacher" class="btn btn-primary" role="button"><?=$this->lang->line('staffmanager_btn_regist_teacher')?></button>
						</div>
					</div>
					<div class="form-group">
						<label for="staffmanager_op_managername" class="col-md-4 control-label"><?=$this->lang->line('staffmanager_op_managername')?></label>
						<div class="col-md-2">
							<p class="form-control-static"><?=htmlspecialchars($course->op_mng_name)?></p>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-4 col-md-8">
							<img class="img-responsive" src="<?=$course->op_mng_picture_file?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-4 col-md-8">
<?php
$_btn_regist_op_manager_disabled = "";
if ($authority != $this->config->item('AUTH_SYS_MANAGER')) {
	// システム管理者以外は「運用管理者登録」ボタンを無効にする。
	$_btn_regist_op_manager_disabled = " disabled";
}
?>
							<button id="staffmanager_btn_regist_op_manager" class="btn btn-primary" role="button"<?=$_btn_regist_op_manager_disabled?>><?=$this->lang->line('staffmanager_btn_regist_op_manager')?></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?=$script?>
