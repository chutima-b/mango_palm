<?php
/*
 * 講座申込画面（受講講座自己登録）ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('selfregistcourse_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('selfregistcourse_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading">
				<?=$this->lang->line('selfregistcourse_pagetitle')?>
			</div>
			<div class="panel-body">
<?php
if (!is_null($error_msg)) {
	// エラーメッセージがある場合は表示
?>
				<div class="alert alert-danger" role="alert"><?=$error_msg?></div>
<?php
}
?>
				<h5><?=$this->lang->line('selfregistcourse_course_count_prefix')?><?=$course_count?><?=$this->lang->line('selfregistcourse_course_count')?></h5>
<?php
if ($genre_course_count !== null and !empty($genre_course_count)) {
	if ($course_data !== null and !empty($course_data)) {
?>
				<div class="panel-group" id="accordion">
<?php
		foreach($genre_course_count as $_genre) :
?>
					<div class="panel panel-info blur">
						<div class="panel-heading">
							<h4 class="panel-title">
								<!--
								data-toggle : Collapseを起動させる
								data-parent : アコーディオン風に閉じたり開いたりするためのもの
								href : 指定した場所のパネルを開く
								-->
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $_genre->id ?>">
									<?php if (!is_null($_genre->icon)) : ?><img id="genre_icon_<?=$_genre->id?>" class="navpic" src="<?=$_genre->icon?>">&nbsp;<?php endif; ?><?=$_genre->genrename?>&nbsp;<span class="badge"><?=$_genre->cnt?></span>
								</a>
							</h4>
						</div>
						<div id="collapse<?=$_genre->id?>" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="table-responsive touch-scroll">
									<table class="table">
										<thead>
<?php
			if (0 < $_genre->cnt) {
?>
											<tr>
												<th><?=$this->lang->line('selfregistcourse_col_course_code')?></th>
												<th><?=$this->lang->line('selfregistcourse_col_course_name')?></th>
												<th><?=$this->lang->line('selfregistcourse_col_lecturer')?></th>
												<th><?=$this->lang->line('selfregistcourse_col_theme_count')?></th>
												<th><?=$this->lang->line('selfregistcourse_col_students_count')?></th>
												<th><?=$this->lang->line('selfregistcourse_col_period')?></th>
												<th>&nbsp;</th>
											</tr>
<?php
			}
?>
										</thead>
										<tbody>
<?php
			foreach($course_data as $_course) :
				if ($_genre->id == $_course->genre) {
					$_fullname = $this->commonlib->createFullName($_course->t_firstname, $_course->t_surname);
?>
											<tr>
												<td><?=htmlspecialchars($_course->course_code) ?></td>
												<td id="course_name_<?=$_course->id?>"><?=htmlspecialchars($_course->course_name) ?></td>
												<td id="course_teacher_name_<?=$_course->id?>"><?=htmlspecialchars($_fullname) ?></td>
												<td><?=$_course->th_cnt?></td>
												<td><?=$_course->u_cnt?></td>
												<td>
<?php
					if ($_course->schedule_use == 1) {
?>
													<?=$_course->start_date?> - <?=$_course->end_date?>
<?php
					}
?>
												</td>
												<td>
<?php
					if ($_course->is_registered == 0) {
?>													<button id="btn_course_info_<?=$_course->id?>" class="btn btn-success"><?=$this->lang->line('selfregistcourse_btn_course_info')?></button>
<?php
					}
?>													<input id="course_genre_<?=$_course->id?>" type="hidden" value="<?=$_course->genre?>">
													<input id="course_teacher_picture_<?=$_course->id?>" type="hidden" value="<?=$_course->t_picture?>">
													<div id="course_description_<?=$_course->id?>" style="display:none;"><?=htmlspecialchars($_course->course_description)?></div>
												</td>
											</tr>
<?php
				}
			endforeach;
?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
<?php
		endforeach;
?>
				</div>
<?php
	}
}
?>
			</div>
		</div>
	</div>
	<!-- 「講座申込」ダイアログ -->
	<div class="modal fade bs-example-modal-lg" id="modal_regist_course" tabindex="-1">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form id="form_regist_course" name="form_regist_course" action="<?=$this->commonlib->baseUrl()?>selfregistcourse/regist" method="post">
					<div class="modal-header">
						<img id="modal_genre_icon" class="navpic">&nbsp;<span id="modal_course_name"></span>
					</div>
	        		<div class="modal-body">
						<dl class="dl-horizontal">
							<dt><?=$this->lang->line('selfregistcourse_modal_lecturer')?></dt>
							<dd id="modal_teacher"></dd>
							<dd><img id="modal_teacher_picture" class="img-responsive"></dd>
							<dt><?=$this->lang->line('selfregistcourse_modal_course_description')?></dt>
							<dd id="modal_course_description"></dd>
						</dl>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('selfregistcourse_modal_btn_cancel')?></button>
							<button id="modal_btn_regist_course" type="submit" class="btn btn-warning"><?=$this->lang->line('selfregistcourse_modal_btn_attend')?></button>
						</div>
					</div>
					<input id="modal_cid" type="hidden" name="cid">
				</form>
			</div>
		</div>
	</div>
