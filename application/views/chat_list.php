<?php
/*
 * チャット一覧画面ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('chat_list_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('chat_list_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=htmlspecialchars($course_name)?></div>
			<div class="panel-body">
<?php
// 無限スクロール用“前ページ”リンク
if ($before_page):
?>
				<a id="before_page" href="<?=$before_page?>"></a>
<?php
endif;

// 無限スクロール用“次ページ”リンク
if ($next_page):
?>
				<a id="next_page" href="<?=$next_page?>"></a>
<?php
endif;
?>
				<div id="chat_list">
<?php
if (!is_null($chat_users)) {
	$_has_list = false;
	foreach ($chat_users as $user) {
		if ($_has_list) {
?>
					<hr>
<?php
		}
?>
					<div class="row">
						<div class="col-xs-3 col-md-1">
							<p><img class="img-responsive img-circle" src="<?=$user->picture_file?>"></p>
						</div>
						<div class="col-xs-9 col-md-10">
							<p>
								<a class="link_underline" id="chatlist_chatlink_<?=$user->uid?>" href="javascript:void(0);"><?=htmlspecialchars($user->nickname)?></a>
								<span class="label label-default"><?=$user->authority?></span>
							</p>
							<p><?=nl2br(htmlspecialchars($user->introduction))?></p>
						</div>
					</div>
<?php
		$_has_list = true;
	}
}
?>
				</div>
				<input type="hidden" id="cid" name="cid" value="<?=$cid?>">
			</div>
		</div>
	</div>
	<?=$script?>
