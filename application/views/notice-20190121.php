<?php
/*
 * お知らせ画面ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('notice_lang');

$n = $notice_data[0];
?>
	<h1 class="sr-only"><?=$this->lang->line('notice_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=$this->lang->line('notice_pagetitle')?></div>
			<div class="panel-body">
<?php
if ($notice_data !== null and !empty($notice_data)) {
?>
				<div class="media">
					<div class="media-left">
<?php
	if (!is_null($n['icon'])) {
?>
						<img src="<?=$n['icon']?>">
<?php
	}
?>
					</div>
					<div class="media-body">
<?php
	if(!is_null($n['calendar_color'])) {
?>
						<style>
							.cource {
								background-color: <?=$n['calendar_color']?>;
							}
						</style>
<?php
	}
?>
						<h4 class="media-heading"><?=htmlspecialchars($n['title'])?></h4>
						<h5><?=$n['created_at']?>&nbsp;<span class="label label-default cource"><?=htmlspecialchars($n['course_name'])?></span></h5>
						<hr>
						<p><?=$n['content']?></p>
					</div>
				</div>
<?php
}
?>
			</div>
		</div>
	</div>
