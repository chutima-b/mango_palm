<?php
/*
 * 課題管理画面ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('taskmanager_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('taskmanager_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=htmlspecialchars($coursename)?></div>
			<div class="panel-body">
				<p class="lead"><?=$participants?><?=$this->lang->line('taskmanager_participants')?></p>
				<div class="panel-group" id="accordion">
<?php
if (!is_null($theme_list)) {
	foreach ($theme_list as $_theme) {
?>
					<div class="panel panel-default blur">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a href="<?=$this->commonlib->baseUrl()?>taskmanageusers?cid=<?=$cid?>&thid=<?=$_theme->id?>">
									<?=$_theme->name?>
								</a>
<?php
		if ($_theme->open == $this->config->item('THEME_CLOSE')) {
?>
								&nbsp;<span class="label label-default"><?=$this->lang->line('taskmanager_label_msg_closed')?></span>
<?php
		} else if ($_theme->open == $this->config->item('THEME_PUBLIC_END')) {
?>
								&nbsp;<span class="label label-default"><?=$this->lang->line('taskmanager_label_msg_publicend')?></span>
<?php
		}
		if ($_theme->label_message) {
?>
								&nbsp;<span class="label <?=$_theme->label_style?>"><?=$_theme->label_message?></span>
<?php
		}
?>
							</h4>
						</div>
					</div>
<?php
	}
}
?>
				</div>
<!--
				<div class="pull-right">
					<a href="/images/REPORTS/index.html"><button class="btn btn-primary"><?=$this->lang->line('taskmanager_btn_reports')?></button></a>
				</div>
-->
			</div>
		</div>
	</div>
