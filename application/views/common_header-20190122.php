<?php

/*
 * HTML共通ヘッダー部ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('common_header_lang');

// 基準URL
$_base_url = $this->commonlib->baseUrl();

// タイトル
$_system_title = '';
if (!is_null($this->session->userdata('system_title'))) {
	$_system_title = $this->session->userdata('system_title');
} else if (isset($tmp_system_title)) {
	$_system_title = $tmp_system_title;
}
// ロゴ
$_logo = '';
if (!is_null($this->session->userdata('logo'))) {
	$_logo = $this->session->userdata('logo');
} else if (isset($tmp_logo)) {
	$_logo = $tmp_logo;
}

// 壁紙
$_wallpaper = '';
if (!is_null($this->session->userdata('wallpaper'))) {
	$_wallpaper = $this->session->userdata('wallpaper');
} else if (isset($tmp_wallpaper)) {
	$_wallpaper = $tmp_wallpaper;
}

// 背景色
$_backcolor = '';
if (!is_null($this->session->userdata('backcolor'))) {
	$_backcolor = $this->session->userdata('backcolor');
} else if (isset($tmp_backcolor)) {
	$_backcolor = $tmp_backcolor;
}

// キャッシュさせない
header("Expires: -1");
header("Last-Modified: ". gmdate("D, d M Y H:i:s"). " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="<?=$this->config->item('HTML_LANG')?>">
<head>
	<base href="<?=$this->commonlib->baseUrl()?>">
	<meta charset="utf-8">
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
	<meta name="viewport" content="width=device-width">
	<link rel="stylesheet" href="<?=$_base_url?>libs/bootstrap-3.3.6/css/bootstrap.min.css">
	<style>
		body {
			background-color: <?=$_backcolor?>;
		}
<?php
if ($_wallpaper) {
?>
		body,
		.blur::before {
			background: url(<?=$_wallpaper?>) 0 / cover fixed;
		}
<?php
}
?>
	</style>
<?php
	if (isset($add_css)) {
		// 追加CSSファイル表示
		echo $add_css;
	}

	// 通常は共通CSSのあとに独自のCSSをロードするが、
	// 各種ライブラリ関連のCSSの絡みでcommon.cssをあとからあてたいとの事で
	// 最後にcommon.cssを配置する。
	// 各画面では、common.cssの定義に被らない様にcssを定義する。
?>
	<link rel="stylesheet" href="<?=$_base_url?>css/common.css?v=1.0.0">
<?php
	if (isset($page_title)) {
		// タイトル表示
?>
	<title><?=$page_title?></title>
<?php
	}
?>
	<script src="<?=$_base_url?>libs/jquery/jquery-2.2.4.min.js"></script>
	<script src="<?=$_base_url?>libs/bootstrap-3.3.6/js/bootstrap.min.js"></script>
<?php
	if (isset($add_scripts)) {
		// 追加JavaScriptファイル表示
		echo $add_scripts;
	}
?>
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#slms-navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
<?php
// ログイン画面、ホーム画面以外は、「戻る」リンクを表示
if ($controller_kind != $this->config->item('CONTROLLER_KIND_LOGIN')
 && $controller_kind != $this->config->item('CONTROLLER_KIND_STUDENTS_HOME')
 && $controller_kind != $this->config->item('CONTROLLER_KIND_STAFF_HOME')
 && $controller_kind != $this->config->item('CONTROLLER_KIND_NOTICE')
 && $controller_kind != $this->config->item('CONTROLLER_KIND_COURSE')
 && $controller_kind != $this->config->item('CONTROLLER_KIND_THEME')
) {
	$_back_url = '#';
	if (isset($culture_back_url)) {
		$_back_url = $culture_back_url;
	}
?>
				<a class="navbar-brand" href="<?=$_back_url?>"><span class="hidden-xs"><?=$this->lang->line('header_link_back')?></span><span class="visible-xs glyphicon glyphicon-menu-left" aria-hidden="true"></span></a>
<?php
}
if ($_logo) {
?>
				<a class="navbar-brand" href="<?=$_base_url?>">
				    <img class="navpic" src="<?=$_logo?>">
				</a>
<?php
}
if (!is_null($_system_title) && strlen($_system_title)) {
?>
				<p class="navbar-text"><?=htmlspecialchars($_system_title)?></p>
<?php
}
?>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="slms-navbar">
				<ul class="nav navbar-nav">
				</ul>
				<ul class="nav navbar-nav navbar-right">
<?php /*
					<li>
						<a href="/images/STORE/">BOOK SHOP</a>
					</li>
*/ ?>
<?php
// 受講者、管理者を判別してホーム画面のリンクを設定する
$_is_staff = false; // 管理者フラグ
$_authorityInfo = null;
$_profile_img = '';
$_nickname = '';
if($controller_kind != $this->config->item('CONTROLLER_KIND_LOGIN')) {
	if ($this->session->userdata('authority')) {
		$_authorityInfo = $this->session->userdata('authority');
		$_profile_img = $this->commonlib->baseUrl()."displayfile?tbl=".$this->config->item('TABLE_KEY_USER')."&id=".$_authorityInfo[0]->id;
		$_nickname = $_authorityInfo[0]->nickname;
		if ($_authorityInfo[0]->authority == $this->config->item('AUTH_TEACHER')
		 || $_authorityInfo[0]->authority == $this->config->item('AUTH_OP_MANAGER')
		 || $_authorityInfo[0]->authority == $this->config->item('AUTH_SYS_MANAGER')) {
			$_is_staff = true;
		}
	}
	// 受講者以外
	if ($_is_staff) {
		// システム管理者の場合のみシステム管理のリンクを表示
		if ($_authorityInfo[0]->authority == $this->config->item('AUTH_SYS_MANAGER')) {
?>
					<li>
						<a href="<?=$_base_url?>usermanager"><?=$this->lang->line('header_link_sysmanage')?></a>
					</li>
<?php
		}
?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$this->lang->line('header_link_home')?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?=$_base_url?>studentshome"><?=$this->lang->line('header_link_home_students')?></a></li>
							<li><a href="<?=$_base_url?>staffhome"><?=$this->lang->line('header_link_home_staff')?></a></li>
						</ul>
					</li>
<?php
	}

	// 受講者かつ、ホーム画面以外
	if (!$_is_staff && $controller_kind != $this->config->item('CONTROLLER_KIND_STUDENTS_HOME')) {
?>
					<li><a href="<?=$_base_url?>studentshome"><?=$this->lang->line('header_link_home')?></a></li>
<?php
	}
?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
<?php
	if (!is_null($_authorityInfo) && $_authorityInfo[0]->authority != $this->config->item('AUTH_GUEST')) {
		// ゲストはユーザー画像を表示しない
?>
							<img class="navpic" src="<?=$_profile_img?>">
<?php
	}
?>							<?=htmlspecialchars($_nickname)?><span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
<?php
	if (!is_null($_authorityInfo) && $_authorityInfo[0]->authority != $this->config->item('AUTH_GUEST')) {
		// ゲストはプロフィール画面のリンクを表示しない
?>
							<li><a href="<?=$_base_url?>profile"><?=$this->lang->line('header_link_profile')?></a></li>
<?php
	}
?>
							<li><a href="<?=$_base_url?>login/logout"><?=$this->lang->line('header_link_logout')?></a></li>
						</ul>
					</li>
<?php
}
?>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
