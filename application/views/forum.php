<?php
/*
 * フォーラム画面ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('forum_lang');

$c = $course_data[0];
$fm = $forummanage_data[0];
?>
	<h1 class="sr-only"><?=$this->lang->line('forum_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=htmlspecialchars($c->course_name)?></div>
			<div class="panel-body">
				<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
<?php
if (!is_null($error_msg)) {
?>
				<div class="alert alert-danger" role="alert"><?=$error_msg?></div>
<?php
}

// 無限スクロール用“前ページ”リンク
if ($before_page):
?>
				<a id="before_page" href="<?=$before_page?>"></a>
<?php
endif;

// 無限スクロール用“次ページ”リンク
if ($next_page):
?>
				<a id="next_page" href="<?=$next_page?>"></a>
<?php
endif;

// ゲスト以外の場合は投稿ボタンを表示
if ($auth != $this->config->item('AUTH_GUEST')) {
?>
					<p class="pull-right">
<?php
	$_disabled = "";
	if ($fm['use'] == $this->config->item('FORUM_USE_FLAG_OFF')) {
		// フォーラム利用フラグがOFFの場合は投稿ボタンを無効にする
		$_disabled = " disabled";
	}
?>
						<button id="add" class="btn btn-primary" data-toggle="modal" data-target="#new_post_modal" data-pid="" data-postmsg=""<?=$_disabled?>><?=$this->lang->line('forum_button_title_post')?></button>
					</p>
<?php
}
?>
				<div id="forum_list">
<?php
$cnt = 0;
foreach ($forum_data as $f) :
?>
					<div class="row">
						<div class="col-xs-3 col-md-1">
							<img class="img-responsive img-circle" src="<?=$f['picture_file']?>">
						</div>
						<div class="col-xs-9 col-md-11">
							<div class="panel panel-default blur">
<?php
	if ($cnt == 0) {
?>
								<div id="forum_name" class="panel-heading"><?=htmlspecialchars($fm['forum_name'])?></div>
								<input type="hidden" id="page" name="page" value="<?=$page?>">
<?php
	}
?>
								<div class="panel-body">
									<div class="row">
										<div class="col-xs-12">
											<div><?=htmlspecialchars($f['nickname'])?></div>
											<div><?=$f['post_time']?></div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
<?php
		if (!is_null($f['image_file'])) {
?>
											<p><img class="pull-left col-xs-4 diarypic" src="<?=$f['image_file']?>"></p>
<?php
		}
?>
											<p><?=nl2br(htmlspecialchars($f['message']))?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<div class="pull-right">
<?php
	// 自分の投稿または、講師、運用管理者、システム管理者の場合は、編集・削除ボタンを表示する。
	if ($f['my_post'] == 1
	 || $auth == $this->config->item('AUTH_TEACHER')
	 || $auth == $this->config->item('AUTH_OP_MANAGER')
	 || $auth == $this->config->item('AUTH_SYS_MANAGER')) {
?>
												<button class="btn btn-success" data-toggle="modal" data-target="#new_post_modal" data-pid="<?=$f['id']?>" data-postmsg="<?=htmlspecialchars($f['message'])?>" data-img="<?=$f['image_file']?>"><?=$this->lang->line('forum_button_title_edit')?></button>
												<button class="btn btn-warning" data-toggle="modal" data-target="#delete_post_modal" data-pid="<?=$f['id']?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
<?php
	}
?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
<?php
	if ($cnt == 0) {
?>
					<hr id="forum_first_separation">
<?php
	}
	$cnt++;
endforeach;
?>
				</div>
			</div>
		</div>
	</div>

	<!-- 編集、投稿ダイアログ -->
	<div class="modal fade" id="new_post_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="post_regist" name="post_regist" method="post" enctype="multipart/form-data" action="<?=$this->commonlib->baseUrl()?>forum/edit">
					<div class="modal-body">
						<div class="form-group">
							<textarea class="form-control" rows="5" name="postmsg" id="postmsg" placeholder="<?=$this->lang->line('forum_textarea_default_message')?>" required></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<div class="pull-left" id="upload_image_btn_area">
							<label class="btn btn-success" for="upload_imagefile">
								<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
								<input type="file" name="upload_imagefile" id="upload_imagefile" accept="image/gif,image/png,image/jpeg" style="display:none;">
							</label>
							<input type="hidden" name="cid" id="cid" value="<?=$c->id?>">
							<input type="hidden" name="pre" id="pre" value="<?=$pre?>">
							<input type="hidden" name="thid" id="thid" value="<?=$thid?>">
							<input type="hidden" id="fid" name="fid" value="<?=$fid?>">
							<input type="hidden" id="pid" name="pid" value="">
							<input type="hidden" id="edit_page" name="page" value="<?=$page?>">
						</div>
						<img id="upload_imagefile_thumbnail" class="img-responsive" src="">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('forum_button_title_cancel')?></button>
						<button type="submit" class="btn btn-primary"><?=$this->lang->line('forum_button_title_send')?></button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- 削除確認ダイアログ -->
	<div class="modal fade" id="delete_post_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-inline" name="forum_delete" method="post" action="<?=$this->commonlib->baseUrl()?>forum/delete">
					<div class="modal-body">
						<div class="form-group">
							<label for="confim_title"><?=$this->lang->line('forum_confim_title')?></label><br>
							<label for="delete_confim"><?=$this->lang->line('forum_delete_confim')?></label>
							<input type="hidden" name="cid" id="cid" value="<?=$c->id?>">
							<input type="hidden" name="pre" id="pre" value="<?=$pre?>">
							<input type="hidden" name="thid" id="thid" value="<?=$thid?>">
							<input type="hidden" id="fid" name="fid" value="<?=$fid?>">
							<input type="hidden" id="pid" name="pid" value="">
							<input type="hidden" id="del_page" name="page" value="<?=$page?>">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('forum_button_title_cancel')?></button>
						<button type="submit" class="btn btn-primary"><?=$this->lang->line('forum_button_title_delete')?></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?=$script?>
