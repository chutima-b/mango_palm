<?php
/*
 * 講座画面ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('course_lang');

$_base_url = $this->commonlib->baseUrl();
?>
	<h1 class="sr-only"><?=$this->lang->line('course_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?php if (!is_null($course->icon)) : ?><img class="navpic" src="<?=$course->icon?>">&nbsp;<?php endif; ?><?=htmlspecialchars($course->course_name)?></div>
			<div class="panel-body">
				<dl class="dl-horizontal">
					<dt><?=$this->lang->line('course_teacher')?></dt>
					<dd><?=htmlspecialchars($course->teacher_name)?></dd>
					<dd><img class="img-responsive" src="<?=$course->picture_file?>"></dd>
					<dt><?=$this->lang->line('course_description')?></dt>
					<dd><?=$course->course_description?></dd>
				</dl>
				<p><hr></p>
<?php
if (isset($theme)) {
?>
<div class="panel-group" id="accordion">
<?php
	foreach ($theme as $themedata) {
		// パネルヘッダー、受講ボタンのスタイル設定（デフォルトは未受講・未提出）
		$_label_style = '';
		$_label_message = '';
		$_button_style = 'btn-info';
		$_button_title = $this->lang->line('course_button_attend_info_yet_submit');
		if ($themedata->is_submit) {
			// 提出済みの場合
			$_label_style = 'label-info';
			$_label_message = $this->lang->line('course_label_style_info');
			$_button_style = 'btn-info';
			$_button_title = $this->lang->line('course_button_attend_info');

			if ($themedata->is_feedback) {
				// フィードバック済みの場合
				$_label_style = 'label-warning';
				$_label_message = $this->lang->line('course_label_style_warning');
				$_button_style = 'btn-warning';
				$_button_title = $this->lang->line('course_button_attend_warning');
			}

			if ($themedata->is_confirmed) {
				// 受講者確認済みの場合
				// ※「課題を作成する」がYESのテーマはフィードバック日時、受講者確認日時ともにセットされた場合であるが、
				// 　「課題を作成する」がNOのテーマはフィードバック日時が無く、受講者確認日時がセットされた状態。
				$_label_style = 'label-primary';
				$_label_message = $this->lang->line('course_label_style_primary');
				$_button_style = 'btn-primary';
				$_button_title = $this->lang->line('course_button_attend_primary');
			}
		} else {
			// 未提出の場合
			if ($themedata->is_over_timelimit) {
				// 期限切れの場合は期限切れにする
				$_label_style = 'label-danger';
				$_label_message = $this->lang->line('course_label_style_danger');
			}
		}
?>
					<div class="panel panel-default blur">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$themedata->id?>">
									<?php if (!is_null($themedata->theme_date)) :?><?=$themedata->theme_date?>&nbsp;&nbsp;&nbsp;<?php endif;?><?=htmlspecialchars($themedata->name)?>
								</a>
<?php
		if (strlen($_label_message) > 0) {
?>
								&nbsp;<span class="label <?=$_label_style?>"><?=$_label_message?></span>
<?php
		}
		if ($themedata->open == 0) {
?>
								&nbsp;<span class="label label-default"><?=$this->lang->line('course_label_close')?></span>
<?php
		}
?>
							</h4>
						</div>
						<div id="collapse<?=$themedata->id?>" class="panel-collapse collapse">
							<div class="panel-body">
								<p><?=nl2br(htmlspecialchars($themedata->goal))?></p>
<?php
		if (!$themedata->is_over_timelimit) {
			// 期限切れ以外の場合のみ、テーマ画面への遷移ボタンを表示する
?>
								<div class="row">
									<div class="col-md-2">
										<a class="btn <?=$_button_style?>" role="button" href="<?=$_base_url?>theme?cid=<?=$cid?>&thid=<?=$themedata->id?>"><?=$_button_title?></a>
									</div>
								</div>
<?php
		}
?>
							</div>
						</div>
					</div>
<?php
	}
?>
				</div>
<?php
}
?>
			</div>
			<div class="panel-footer">
<?php
if ($course->forum_use == 1) {
?>
				<a href="<?=$_base_url?>forummanage?cid=<?=$cid?>"><?=$this->lang->line('course_link_forum')?></a>&nbsp;&nbsp;|&nbsp;&nbsp;
<?php
}
if ($course_auth == $this->config->item('AUTH_SYS_MANAGER')
	|| ($course_auth != $this->config->item('AUTH_GUEST') && $chat_use == 1)) {
?>
				<a href="<?=$_base_url?>chatlist?cid=<?=$cid?>"><?=$this->lang->line('course_link_chat')?></a>&nbsp;&nbsp;|&nbsp;&nbsp;
<?php
}
?>
				<a href="<?=$_base_url?>diary?cid=<?=$cid?>"><?=$this->lang->line('course_link_diary')?></a>
			</div>
		</div>
	</div>