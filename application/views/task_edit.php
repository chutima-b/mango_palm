<?php
/*
 * 課題編集画面ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('task_edit_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('task_edit_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=htmlspecialchars($theme_name)?></div>
			<div class="panel-body">
				<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
<?php
if (!is_null($error_msg)) {
?>
				<div class="alert alert-danger" role="alert"><?=$error_msg?></div>
<?php
}
?>
				<?=form_open($form_regist_action_url, array('class' => 'form-horizontal', 'id' => 'taskedit_form', 'name' => 'taskedit_form', 'accept-charset' => 'utf-8'))?>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="taskedit_name"><?=$this->lang->line('task_edit_task_name')?></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="taskedit_name" name="taskedit_name" value="<?=set_value('taskedit_name', $task->task_name)?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="taskedit_description"><?=$this->lang->line('task_edit_description')?></label>
						<div class="col-sm-10">
							<textarea class="form-control" id="taskedit_description" name="taskedit_description" rows="10"><?=set_value('taskedit_description', $task->task_description)?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="taskedit_type"><?=$this->lang->line('task_edit_task_type')?></label>
						<div class="col-sm-10">
							<select class="form-control" id="taskedit_type" name="taskedit_type">
								<option value=""><?=$this->lang->line('task_edit_select_un_selected')?></option>
<?php
// 課題のタイプのプルダウン生成
foreach ($task_types as $_key=>$_value) {
	$_default_selected = false;
	if ($task->task_type !=='' && $_key == $task->task_type) {
		$_default_selected = true;
	}
?>
								<option value="<?=$_key?>"<?=set_select('taskedit_type', $_key, $_default_selected)?>><?=$_value?></option>
<?php
}
?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="taskedit_choices">　</label>
						<div class="col-sm-10">
<?php
$_choices_disabled = "";
if ($task->task_type == null || $task->task_type === ''
	|| (intval($task->task_type) !== $this->config->item('TASK_TYPE_SINGLE_SELECTION')
		&& intval($task->task_type) !== $this->config->item('TASK_TYPE_MULTIPLE_CHOICE')
		&& intval($task->task_type) !== $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')
		&& intval($task->task_type) !== $this->config->item('TASK_TYPE_SURVEY_SINGLE_SELECTION')
		&& intval($task->task_type) !== $this->config->item('TASK_TYPE_SURVEY_MULTIPLE_CHOICE')
		)
) {
	$_choices_disabled = " disabled";
}
?>
							<textarea class="form-control" id="taskedit_choices" name="taskedit_choices" rows="3"<?=$_choices_disabled?>><?=set_value('taskedit_choices', $task->choices)?></textarea>
						</div>
					</div>
<?php
if (!$quiz) {
	// クイズ以外の場合は「評価点」を表示する
?>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="taskedit_evaluation_points"><?=$this->lang->line('task_edit_evaluation_points')?></label>
						<div class="col-sm-2">
<?php
	$_evaluation_points_disabled = "";
	if (!$evaluation_use) {
		// 「評価を有効にする」が“NO”の場合は「評価点」を無効にする
		$_evaluation_points_disabled = " disabled";
	}
?>
							<input type="text" class="form-control" id="taskedit_evaluation_points" name="taskedit_evaluation_points" value="<?=set_value('taskedit_evaluation_points', $task->evaluation_points)?>"<?=$_evaluation_points_disabled?>>
						</div>
					</div>
<?php
} else {
	// クイズの時は、「正解」を表示する
	// ※ただし、クイズ（単一選択）以外の場合は表示しない
	$_quiz_answer_display_none = '';
	if (intval($task->task_type) !== $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION')) {
		$_quiz_answer_display_none = ' style="display:none;"';
	}
?>
					<div id="taskedit_quiz_answer_form_group" class="form-group"<?=$_quiz_answer_display_none?>>
						<label class="col-sm-2 control-label" for="taskedit_quiz_answer"><?=$this->lang->line('task_edit_quiz_answer')?></label>
						<div class="col-sm-10">
							<div id="taskedit_quiz_answer_area"></div>
							<span id="taskedit_quiz_answer_error"></span>
						</div>
					</div>
<?php
}
?>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="taskedit_comment"><?=$this->lang->line('task_edit_comment')?></label>
						<div class="col-sm-10">
							<textarea class="form-control" id="taskedit_comment" name="taskedit_comment" rows="3"><?=set_value('taskedit_comment', $task->comment)?></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-11 col-sm-1">
							<button type="button" class="btn btn-primary" id="taskedit_btn_regist"><?=$this->lang->line('task_edit_btn_regist')?></button>
						</div>
					</div>
					<input type="hidden" id="taskedit_evaluation_use" name="taskedit_evaluation_use" value="<?=set_value('taskedit_evaluation_use', $evaluation_use)?>">
					<input type="hidden" id="taskedit_quiz" name="taskedit_quiz" value="<?=set_value('taskedit_quiz', $quiz)?>">
				</form>
			</div>
		</div>
	</div>
	<?=$script?>
