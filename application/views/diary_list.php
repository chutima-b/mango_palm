<?php
/*
 * 日記一覧画面ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('diary_list_lang');

$c = $course_data[0];
?>
	<h1 class="sr-only"><?=$this->lang->line('diary_list_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=htmlspecialchars($c->course_name)?></div>
			<div class="panel-body">
<?php
// 無限スクロール用“前ページ”リンク
if ($before_page):
?>
				<a id="before_page" href="<?=$before_page?>"></a>
<?php
endif;

// 無限スクロール用“次ページ”リンク
if ($next_page):
?>
				<a id="next_page" href="<?=$next_page?>"></a>
<?php
endif;
?>
				<div id="diary_list">
<?php
// 日記参加者一覧表示
$cnt = 0;
foreach ($diarylist_data as $dl) :
	if (0 < $cnt) {
?>
					<hr>
<?php
	}
	$cnt++;

	$_checked = false;
	if ($dl['on_flg'] == 1) {
		$_checked = true;
	}
?>
					<div class="row">
						<div class="col-xs-3 col-md-1">
							<img class="img-responsive img-circle" src="<?=$dl['picture_file']?>">
						</div>
						<div class="col-xs-5 col-md-9">
							<p><?=htmlspecialchars($dl['nickname'])?> <span class="label label-default"><?=$dl['auth']?></span></p>
							<p><?=nl2br(htmlspecialchars($dl['introduction']))?><p>
						</div>
						<div class="col-xs-4 col-md-2">
							<form id="diarylist_hide_edit_<?=$dl['uid']?>" name="diarylist_hide_edit_<?=$dl['uid']?>" method="post" action="<?=$this->commonlib->baseUrl()?>diarylist/edit">
								<input id="diarylist_display_check_<?=$dl['uid']?>" class="diarylist_display_check" name="display_check" type="checkbox"  value="1" <?=set_checkbox('display_check', '1', $_checked)?>>
								<input type="hidden" name="cid" id="cid" value="<?=$c->id?>" />
								<input type="hidden" name="pre" id="pre" value="<?=$pre?>" />
								<input type="hidden" name="thid" id="thid" value="<?=$thid?>" />
								<input type="hidden" name="page" id="page" value="<?=$page?>" />
								<input type="hidden" name="dpage" id="dpage" value="<?=$dpage?>" />
								<input type="hidden" name="uid" id="uid" value="<?=$dl['uid']?>" />
								<input type="hidden" name="hid" id="hid" value="<?=$dl['hid']?>" />
							</form>
						</div>
					</div>
<?php
endforeach
?>
				</div>
			</div>
		</div>
	</div>
	<?=$script?>