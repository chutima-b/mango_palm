<?php
/*
 * テーマ編集画面ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('themeedit_lang');

$c = $course_data[0];

$_title_readonly = "";
$_themedate_readonly = "";
$_time_limit_readonly = "";
$_goal_readonly = "";
$_description_readonly = "";
$_btn_update_theme_disabled = "";
$_btn_delete_theme_disabled = "";
$_btn_new_material_disabled = "";
$_btn_delete_material_disabled = "";
$_btn_new_task_disabled = "";
$_btn_edit_task_disabled = "";
$_btn_delete_task_disabled = "";
$_evaluation_use_readonly = "";
if ($theme_data['open'] == $this->config->item('THEME_OPEN')
 || $theme_data['open'] == $this->config->item('THEME_PUBLIC_END')) {
	// 公開中または、公開終了の場合は、
	// 「新しい教材＋」、教材の「削除」、「新しい課題＋」、課題の「編集」、課題の「削除」
	// の各ボタンをdisabledにする
	$_title_readonly = ' readonly';
	$_themedate_readonly = ' readonly';
	$_time_limit_readonly = ' readonly';
	$_goal_readonly = ' readonly';
	$_description_readonly = ' readonly';
	$_btn_new_material_disabled = ' disabled';
	$_btn_delete_material_disabled = ' disabled';
	$_btn_new_task_disabled = ' disabled';
	$_btn_edit_task_disabled = ' disabled';
	$_btn_delete_task_disabled = ' disabled';
	// 公開中または、公開終了の場合は、「課題を作成する」を変更させない
	$_evaluation_use_readonly = " readonly";
}
if ($is_attended) {
	// 一人でも受講済みのデータがある（課題フィードバックテーブルにデータがある）場合、
	// 編集操作は全てdisabled
	$_title_readonly = ' readonly';
	$_themedate_readonly = ' readonly';
	$_time_limit_readonly = ' readonly';
	$_goal_readonly = ' readonly';
	$_description_readonly = ' readonly';
	$_btn_update_theme_disabled = ' disabled';
	$_btn_delete_theme_disabled = ' disabled';
	$_btn_new_material_disabled = ' disabled';
	$_btn_delete_material_disabled = ' disabled';
	$_btn_new_task_disabled = ' disabled';
	$_btn_edit_task_disabled = ' disabled';
	$_btn_delete_task_disabled = ' disabled';
}
?>
	<h1 class="sr-only"><?=$this->lang->line('themeedit_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?= $c->course_name ?></div>
			<div class="panel-body">
				<aside id="error"></aside>
				<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
				<?php if (isset($complete_msg)) { echo $complete_msg; }?>
				<?php if (isset($error_msg)) { echo $error_msg; }?>
				<form class="form-horizontal" id="themeedit" name="themeedit" enctype="multipart/form-data" method="post" action="<?=$this->commonlib->baseUrl()?>themeedit/themeedit">
					<input type="hidden" name="cid" value="<?=$cid?>">
					<input type="hidden" name="thid" value="<?=$thid?>">
					<div id="themeedit_form_guide" class="form-group" hidden>
						<div class="col-md-offset-2 col-md-10">
							<div class="alert alert-info help" role="alert">
								<p><strong><?=$this->lang->line('themeedit_comment_form_header')?></strong></p>
								<ul>
									<li><?=$this->lang->line('themeedit_comment_form1')?></li>
									<li><?=$this->lang->line('themeedit_comment_form2')?></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="courseid" class="col-md-2 control-label"><?=$this->lang->line('themeedit_input_theme')?></label>
						<div class="col-md-9">
							<input class="form-control" type="text" id="title" name="title" value="<?=set_value('title', $theme_data['title'])?>"<?=$_title_readonly?> required>
						</div>
						<div class="col-md-1">
							<span id="themeedit_form_guide_help" data-toggle="tooltip" title="<?=$this->lang->line('themeedit_tooltip_guide')?>" data-placement="top" class="guide_icon glyphicon glyphicon-question-sign" aria-hidden="true"></span>
						</div>
					</div>
					<div class="form-group">
						<label for="themedate" class="col-md-2 control-label"><?=$this->lang->line('themeedit_input_date')?></label>
						<div class="col-md-2">
<?php
$_date_disabled = "";
if (!$c->schedule_use) {
	// スケジュールを利用しない場合は「日付」は無効にする
	$_date_disabled = " disabled";
}
?>
							<input class="form-control" type="text" id="themedate" name="themedate" value="<?=set_value('themedate', $theme_data['theme_date'])?>" <?=$_date_disabled?><?=$_themedate_readonly?> required>
						</div>
					</div>
					<div class="form-group">
						<label for="time_limit" class="col-md-2 control-label"><?=$this->lang->line('themeedit_input_time_limit')?></label>
						<div class="col-md-2">
<?php
$_time_limit_disabled = "";
if (!$c->schedule_use) {
	// スケジュールを利用しない場合は「期限」は無効にする
	$_time_limit_disabled = " disabled";
}
?>
							<input class="form-control" type="text" id="time_limit" name="time_limit" value="<?=set_value('time_limit', $theme_data['time_limit'])?>" <?=$_time_limit_disabled?><?=$_time_limit_readonly?> required>
						</div>
					</div>
					<div class="form-group">
						<label for="theme_goal" class="col-md-2 control-label"><?=$this->lang->line('themeedit_input_goal')?></label>
						<div class="col-md-10">
							<textarea class="form-control" rows="5" id="goal" name="goal"<?=$_goal_readonly?> required><?=set_value('goal', $theme_data['goal'])?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="themedescription" class="col-md-2 control-label"><?=$this->lang->line('themeedit_input_description')?></label>
						<div class="col-md-10">
							<textarea class="form-control" rows="5" id="theme_description" name="description"<?=$_description_readonly?> required><?=set_value('description', $theme_data['description'])?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="evaluation_use" class="col-md-2 control-label"><?=$this->lang->line('themeedit_input_evaluation_use')?></label>
						<div class="col-md-10">
<?php
$_quiz_readonly = "";
if (!is_null($task_data) && count($task_data) > 0) {
	// 課題がある場合は、「課題を作成する」を変更させない
	$_evaluation_use_readonly = " readonly";
	// 課題がある場合は、「クイズ形式にする」を変更させない
	$_quiz_readonly = ' readonly';
}
if ($is_attended) {
	// 一人でも受講済みのデータがある（課題フィードバックテーブルにデータがある）場合は、
	// 「課題を作成する」を変更させない
	$_evaluation_use_readonly = " readonly";
}
if ($c->permitted_guest == 1) {
	// 講座情報テーブルの“ゲスト許可”の値が“許可する”の場合、
	// “課題を作成する”のスイッチは“NO”固定にする
	// ※$theme_data['evaluation_use']=falseの処理はコントローラ側にて実施
	$_evaluation_use_readonly = " readonly";
}
?>
							<input class="themeedit_switch" name="evaluation_use" id="evaluation_use" type="checkbox" data-on-text="Yes" data-off-text="No"  value="1" <?=set_checkbox('evaluation_use', '1', $theme_data['evaluation_use'])?><?=$_evaluation_use_readonly?>>
							<span id="themeedit_evaluation_use_guide_help" data-toggle="tooltip" title="<?=$this->lang->line('themeedit_tooltip_guide')?>" data-placement="top" class="guide_icon glyphicon glyphicon-question-sign" aria-hidden="true"></span>
							<p>
								<div id="themeedit_evaluation_use_guide" class="alert alert-info help" role="alert" hidden>
									<ul>
										<li><?=$this->lang->line('themeedit_comment_evaluation_use1')?></li>
										<li><?=$this->lang->line('themeedit_comment_evaluation_use2')?></li>
										<li><?=$this->lang->line('themeedit_comment_evaluation_use3')?></li>
									</ul>
								</div>
							</p>
						</div>
					</div>
					<div class="form-group evaluation_points_area">
						<label for="quiz" class="col-md-2 control-label"><?=$this->lang->line('themeedit_input_quiz')?></label>
						<div class="col-md-10">
							<input class="themeedit_switch" name="quiz" id="quiz" type="checkbox" data-on-text="Yes" data-off-text="No" value="1" <?=set_checkbox('quiz', '1', $theme_data['quiz'])?><?=$_quiz_readonly?>>
							<span id="themeedit_quiz_guide_help" data-toggle="tooltip" title="<?=$this->lang->line('themeedit_tooltip_guide')?>" data-placement="top" class="guide_icon glyphicon glyphicon-question-sign" aria-hidden="true"></span>
							<p>
								<div id="themeedit_quiz_guide" class="alert alert-info help" role="alert" hidden>
									<ul>
										<li><?=$this->lang->line('themeedit_comment_quiz1')?></li>
										<li><?=$this->lang->line('themeedit_comment_quiz2')?></li>
									</ul>
								</div>
							</p>
						</div>
					</div>
					<div class="form-group evaluation_points_area">
						<label for="evaluation_points" class="col-md-2 control-label"><?=$this->lang->line('themeedit_theme_item_evaluation_points')?></label>
						<div class="col-md-10">
							<p class="form-control-static"><?=$theme_data['evaluation_points']?></p>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
<?php
$_is_manager_delete_theme = false;
$_is_new = '0';
if (is_null($thid) || empty($thid)) {
	$_is_new = '1';
	// 新規登録時は、「新しい教材」「新しい課題」ボタンを無効にする。
	$_btn_new_material_disabled = " disabled";
	$_btn_new_task_disabled = " disabled";
	// 新規登録の場合は「登録」ボタンを表示
?>
							<button id="themeedit_btn_entry" class="btn btn-primary" type="submit"><?=$this->lang->line('themeedit_button_entry')?></button>
<?php
} else {
	// 新規登録以外は「登録」ボタンおよび、ゴミ箱ボタンを表示

	// 運用管理者またはシステム管理者の場合、
	// 削除ボタンが無効になるべき状態であっても
	// 警告メッセージを表示の上、削除できるようにする。
	$_is_manager_delete_theme = false;
	if (($course_auth == $this->config->item('AUTH_OP_MANAGER')
		|| $course_auth == $this->config->item('AUTH_SYS_MANAGER'))
	 && strlen($_btn_delete_theme_disabled) > 0) {
		$_btn_delete_theme_disabled = '';
		$_is_manager_delete_theme = true;
	}
?>
							<button id="themeedit_btn_entry" class="btn btn-primary" type="submit"<?=$_btn_update_theme_disabled?>><?=$this->lang->line('themeedit_button_entry')?></button>
							<button id="btn_delete_theme" class="btn btn-warning" type="button" data-toggle="modal" data-target="#delete_theme_modal"<?=$_btn_delete_theme_disabled?>><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
<?php
}
?>
							<span id="themeedit_btn_reg_del_guide_help" data-toggle="tooltip" title="<?=$this->lang->line('themeedit_tooltip_guide')?>" data-placement="top" class="guide_icon glyphicon glyphicon-question-sign" aria-hidden="true"></span>
							<p>
								<div id="themeedit_btn_reg_del_guide" class="alert alert-info help" role="alert" hidden>
									<ul>
										<li><?=$this->lang->line('themeedit_comment_btn_reg_del')?></li>
									</ul>
								</div>
							</p>
						</div>
					</div>
<?php
$_btn_open_disabled = "";
$_btn_end_disabled = "";
if (!is_null($thid) && !empty($thid)) {
	// 新規登録以外は公開・公開停止のボタンを表示
	// デフォルトは「公開」ボタン
	$_btn_open_name = $this->lang->line('themeedit_theme_open');
	$_btn_open_style= 'btn-info';
	if ($theme_data['open'] == $this->config->item('THEME_OPEN')) {
		// 公開中は「公開停止」ボタン
		$_btn_open_name = $this->lang->line('themeedit_theme_close');
		$_btn_open_style= 'btn-default';
	} else if ($theme_data['open'] == $this->config->item('THEME_CLOSE')) {
		// 非公開の場合
		if ($theme_data['evaluation_use'] == 1
		 && (is_null($task_data) || count($task_data) == 0)) {
			// 「課題を作成する」がYESで、
			// 課題が作成されてない場合は公開ボタンを無効にする
			$_btn_open_disabled = " disabled";
		}
		// 非公開の場合は公開終了ボタンは無効にする
		$_btn_end_disabled = " disabled";
		// スケジュールを利用するばあいで、
		// テーマの「日付」、「期限」が未設定の場合は
		// 公開ボタンを無効にする
		if ($c->schedule_use
		 && (empty($theme_data['theme_date']) || empty($theme_data['time_limit']))) {
			$_btn_open_disabled = " disabled";
		}
	} else {
		// 公開終了の場合
		// 公開ボタンを無効にする
		$_btn_open_disabled = " disabled";
		// 公開終了ボタンは無効にする
		$_btn_end_disabled = " disabled";
	}
?>
					<hr>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<p>
								<button id="btn_open" class="btn <?=$_btn_open_style?>" type="button"<?=$_btn_open_disabled?>><?=$_btn_open_name?></button>
								<button id="btn_end" class="btn btn-warning" type="button"<?=$_btn_end_disabled?>><?=$this->lang->line('themeedit_theme_end')?></button>
								<span id="themeedit_btn_open_guide_help" data-toggle="tooltip" title="<?=$this->lang->line('themeedit_tooltip_guide')?>" data-placement="top" class="guide_icon glyphicon glyphicon-question-sign" aria-hidden="true"></span>
							</p>
							<p>
								<div id="themeedit_btn_open_guide" class="alert alert-info help" role="alert" hidden>
									<ul>
										<li><?=$this->lang->line('themeedit_comment_btn_open1')?></li>
										<li><?=$this->lang->line('themeedit_comment_btn_open2')?></li>
										<li><?=$this->lang->line('themeedit_comment_btn_open3')?></li>
										<li><?=$this->lang->line('themeedit_comment_btn_open4')?></li>
									</ul>
								</div>
							</p>
						</div>
					</div>
<?php
}
?>
					<input type="hidden" id="is_new" name="is_new" value="<?=$_is_new?>">
				</form>
<?php
if (!is_null($thid) && !empty($thid)) {
	// 新規登録以外は公開する/非公開、公開終了のフォームを表示
?>
				<form id="themeopen" name="themeopen" method="post" action="<?=$this->commonlib->baseUrl()?>themeedit/open?cid=<?=$cid?>&thid=<?=$thid?>">
					<input type="hidden" name="cid" value="<?=$cid?>">
					<input type="hidden" name="thid" value="<?=$thid?>">
					<input type="hidden" name="open" value="<?=$theme_data['open']?>">
				</form>
				<form id="theme_public_end" name="theme_public_end" method="post" action="<?=$this->commonlib->baseUrl()?>themeedit/end?cid=<?=$cid?>&thid=<?=$thid?>">
					<input type="hidden" name="cid" value="<?=$cid?>">
					<input type="hidden" name="thid" value="<?=$thid?>">
				</form>
<?php
}
?>
				<hr>
				<p class="text-right">
					<button class="btn btn-primary" data-toggle="modal" data-target="#new_material_modal" data-description="" data-resource=""<?=$_btn_new_material_disabled?>>
				    <?=$this->lang->line('themeedit_button_new_material')?> <span class="glyphicon glyphicon-plus" aria-hidden="true">
					</button>
					<span id="themeedit_btn_new_material_guide_help" data-toggle="tooltip" title="<?=$this->lang->line('themeedit_tooltip_guide')?>" data-placement="top" class="guide_icon glyphicon glyphicon-question-sign" aria-hidden="true"></span>
					<p>
						<div id="themeedit_btn_new_material_guide" class="alert alert-info help" role="alert" hidden>
							<ul>
								<li><?=$this->lang->line('themeedit_comment_btn_new_material1')?></li>
								<li><?=$this->lang->line('themeedit_comment_btn_new_material2')?></li>
								<li><?=$this->lang->line('themeedit_comment_btn_new_material3')?></li>
							</ul>
						</div>
					</p>
				</p>
<?php
if (count($materials_data) > 0) {
	// 教材の一覧を表示
?>
				<div class="panel panel-default blur">
					<div class="panel-heading"><?=$this->lang->line('themeedit_title_material')?></div>
					<div class="panel-body">
<?php
	foreach ($materials_data as $r) :
?>
						<div class="row">
							<div class="col-sm-10">
								<a href="<?=$r['link_path']?>" target="_blank">
<?php
		if ($r['material_type'] == $this->config->item('MATERIAL_TYPE_EXTERNAL')) {
			// 外部リンク
?>
								<span class="label label-primary"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></span>
<?php
		} elseif ($r['material_type'] == $this->config->item('MATERIAL_TYPE_PICTURE')) {
			// 写真
?>
								<span class="label label-primary"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></span>
<?php
		} elseif ($r['material_type'] == $this->config->item('MATERIAL_TYPE_MOVIE')) {
			// 動画
?>
								<span class="label label-primary"><span class="glyphicon glyphicon-film" aria-hidden="true"></span></span>
<?php
		} elseif ($r['material_type'] == $this->config->item('MATERIAL_TYPE_PDF')) {
			// PDF
?>
								<span class="label label-primary"><span class="glyphicon glyphicon-file" aria-hidden="true"></span></span>
<?php
		}
?>
								<?=htmlspecialchars($r['material_description'])?></a>
							</div>
							<div class="col-sm-2">
								<p class="text-right"><button class="btn btn-warning" data-toggle="modal" data-target="#delete_material_modal" data-mid="<?=$r['id']?>"<?=$_btn_delete_material_disabled?>><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></p>
							</div>
						</div>
<?php
	endforeach;
?>
					</div>
				</div>
<?php
}
?>
				<hr>
				<p class="text-right">
<?php
$_new_task_url = $this->commonlib->baseUrl().'taskedit?cid='.$cid.'&thid='.$thid;
if ($theme_data['evaluation_use'] == 0) {
	// 「課題を作成する」がOFFの場合は「新しい課題」ボタンを無効にする。
	$_btn_new_task_disabled = ' disabled';
}
if (!empty($_btn_new_task_disabled)) {
	// 「新しい課題」ボタンが無効の場合は、ボタンのリンクも無効にしておく
	$_new_task_url = 'javascript:void(0);';
}
?>
					<a href="<?=$_new_task_url?>" id="btn_new_task" class="btn btn-primary"<?=$_btn_new_task_disabled?>><?=$this->lang->line('themeedit_button_new_task')?> <span class="glyphicon glyphicon-plus" aria-hidden="true"></a>
<?php
// 運用管理者またはシステム管理者の場合は、
// 「課題のリセット」ボタンを表示
if ($course_auth == $this->config->item('AUTH_OP_MANAGER')
		|| $course_auth == $this->config->item('AUTH_SYS_MANAGER')) {
	$_btn_reset_task_result_disabled = '';
	if (is_null($thid) || empty($thid)) {
		// 新規登録時は、「課題のリセット」ボタンを無効にする。
		$_btn_reset_task_result_disabled = ' disabled';
	}
?>
					<button id="btn_reset_task_result" class="btn btn-warning" type="button"<?=$_btn_reset_task_result_disabled?>><?=$this->lang->line('themeedit_button_reset_task_result')?>&nbsp;<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
<?php
}
?>					<span id="themeedit_btn_new_task_guide_help" data-toggle="tooltip" title="<?=$this->lang->line('themeedit_tooltip_guide')?>" data-placement="top" class="guide_icon glyphicon glyphicon-question-sign" aria-hidden="true"></span>
					<p>
						<div id="themeedit_btn_new_task_guide" class="alert alert-info help" role="alert" hidden>
							<ul>
								<li><?=$this->lang->line('themeedit_comment_btn_new_task1')?></li>
								<li><?=$this->lang->line('themeedit_comment_btn_new_task2')?></li>
								<li><?=$this->lang->line('themeedit_comment_btn_new_task3')?></li>
							</ul>
						</div>
					</p>
				</p>
<?php
if ($task_data !== null and !empty($task_data)) {
	// 課題の一覧を表示
?>
<?php
	foreach($task_data as $t) :
?>
				<div class="panel panel-default blur">
					<div class="panel-heading"><?=htmlspecialchars($t['task_name'])?></div>
					<div class="panel-body">
						<p><?=$t['description']?></p>
						<?=$t['answer']?>
						<hr>
<?php
		if (!$theme_data['quiz']) {
?>
						<dl class="dl-horizontal evaluation_points_area">
							<dt><?=$this->lang->line('themeedit_task_item_evaluation_points')?></dt>
							<dd><?=$t['evaluation_points']?></dd>
						</dl>
<?php
		}
?>
						<div class="pull-right">
							<a href="<?=$this->commonlib->baseUrl()?>taskedit?cid=<?=$cid?>&thid=<?=$thid?>&taid=<?=$t['id']?>"><button class="btn btn-success"<?=$_btn_edit_task_disabled?>><?=$this->lang->line('themeedit_button_edit')?></button></a>&nbsp;&nbsp;<button class="btn btn-warning" data-toggle="modal" data-target="#delete_task_modal" data-taid="<?=$t['id']?>"<?=$_btn_delete_task_disabled?>><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
						</div>
					</div>
				</div>
<?php
	endforeach;
}
?>
			</div>
		</div>
	</div>

	<!-- 新しい教材ダイアログ -->
	<div class="modal fade" id="new_material_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-horizontal" id="add_material_form" name="add_material_form" method="post" enctype="multipart/form-data" action="<?=$this->commonlib->baseUrl()?>themeedit/materialregist">
					<div class="modal-body">
						<div class="form-group">
							<label for="description" class="col-md-3 control-label"><?=$this->lang->line('themeedit_material_description')?></label>
							<div class="col-md-9">
								<input class="form-control" id="description" name="description" type="text" value="" required>
							</div>
						</div>
						<div class="form-group">
							<label for="file_name" class="col-md-3 control-label"><?=$this->lang->line('themeedit_material_resource')?></label>
							<div class="col-md-6">
								<input class="form-control" id="resource" name="resource" type="text" value="" required>
							</div>
							<div class="col-md-3">
								<div class="pull-right">
									<label class="btn btn-success" for="upload_file">
										<?=$this->lang->line('themeedit_material_file_select')?>
										<input type="file" name="upload_file" id="upload_file" accept="video/mp4,video/mpeg4,video/quicktime,image/jpg,image/jpeg,image/png,application/pdf" style="display:none;">
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-3 col-md-9">
								<dl class="guide">
									<dt><?=$this->lang->line('themeedit_material_guide1')?></dt>
									<dd>
										<ul>
											<li><?=$this->lang->line('themeedit_material_guide2')?></li>
											<li><?=$this->lang->line('themeedit_material_guide3')?></li>
										</ul>
									</dd>
									<dt><?=$this->lang->line('themeedit_material_guide4')?></dt>
								</dl>
							</div>
						</div>
						<div id="play_end_area" class="form-group">
							<label class="col-sm-3 control-label"><?=$this->lang->line('themeedit_material_play_end')?></label>
							<div class="col-sm-9">
								<input class="themeedit_switch" name="play_end" id="play_end" type="checkbox" data-on-text="Yes" data-off-text="No" value="1" checked>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('themeedit_button_cancel')?></button>
						<button type="submit" class="btn btn-primary"><?=$this->lang->line('themeedit_material_button_regist')?></button>
					</div>
					<input type="hidden" name="cid" id="cid" value="<?=$c->id?>">
					<input type="hidden" name="thid" id="thid" value="<?=$thid?>">
					<input type="hidden" name="start_date" id="start_date" value="<?=$c->start_date?>">
					<input type="hidden" name="end_date" id="end_date" value="<?=$c->end_date?>">
				</form>
			</div>
		</div>
	</div>

	<!-- テーマ削除確認ダイアログ -->
	<div class="modal fade" id="delete_theme_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-inline" name="theme_delete_form" method="post" action="<?=$this->commonlib->baseUrl()?>themeedit/themedelete">
					<div class="modal-body">
						<div class="form-group">
<?php
$_msg_delete_theme = $this->lang->line('themeedit_themedelete_confim');
if ($_is_manager_delete_theme) {
	$_msg_delete_theme = $this->lang->line('themeedit_themedelete_confim_for_manager');
}
?>
							<label for="confim_title"><?=$this->lang->line('themeedit_delete_confim_title')?></label><br>
							<label for="delete_confim"><?=$_msg_delete_theme?></label>
							<input type="hidden" id="cid" name="cid" value="<?=$cid?>">
							<input type="hidden" id="thid" name="thid" value="<?=$thid?>">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('themeedit_button_cancel')?></button>
						<button id="btn_modal_delete_theme" type="submit" class="btn btn-primary"><?=$this->lang->line('themeedit_button_delete')?></button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- 課題削除確認ダイアログ -->
	<div class="modal fade" id="delete_task_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-inline" id="task_delete_form" name="task_delete_form" method="post" action="<?=$this->commonlib->baseUrl()?>themeedit/taskdelete">
					<div class="modal-body">
						<div class="form-group">
							<label for="confim_title"><?=$this->lang->line('themeedit_delete_confim_title')?></label><br>
							<label for="delete_confim"><?=$this->lang->line('themeedit_taskdelete_confim')?></label>
							<input type="hidden" id="cid" name="cid" value="<?=$cid?>">
							<input type="hidden" id="thid" name="thid" value="<?=$thid?>">
							<input type="hidden" id="taid" name="taid" value="">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('themeedit_button_cancel')?></button>
						<button type="submit" class="btn btn-primary"><?=$this->lang->line('themeedit_button_delete')?></button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- 教材削除確認ダイアログ -->
	<div class="modal fade" id="delete_material_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-inline" name="material_delete_form" method="post" action="<?=$this->commonlib->baseUrl()?>themeedit/materialdelete">
					<div class="modal-body">
						<div class="form-group">
							<label for="confim_title"><?=$this->lang->line('themeedit_delete_confim_title')?></label><br>
							<label for="delete_confim"><?=$this->lang->line('themeedit_materialdelete_confim')?></label>
							<input type="hidden" id="cid" name="cid" value="<?=$cid?>">
							<input type="hidden" id="thid" name="thid" value="<?=$thid?>">
							<input type="hidden" id="mid" name="mid" value="">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('themeedit_button_cancel')?></button>
						<button type="submit" class="btn btn-primary"><?=$this->lang->line('themeedit_button_delete')?></button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- 課題を作成するに変更後のメッセージダイアログ -->
	<div class="modal fade" id="evaluation_use_yes_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<?=$this->lang->line('themeedit_modal_msg_change_evaluation_yes')?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
				</div>
			</div>
		</div>
	</div>

	<!-- 「クイズ形式にする」のチェックボックス変更後のメッセージダイアログ -->
	<div class="modal fade" id="quiz_change_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<?=$this->lang->line('themeedit_modal_msg_change_quiz')?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
				</div>
			</div>
		</div>
	</div>

	<!-- 「課題のリセット」の確認ダイアログ -->
	<div class="modal fade" id="reset_task_result_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<form name="reset_task_result_form" method="post" action="<?=$this->commonlib->baseUrl()?>themeedit/resettaskresult">
					<div class="modal-body">
						<?=$this->lang->line('themeedit_modal_msg_reset_task_result')?>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary"><?=$this->lang->line('themeedit_button_yes')?></button>
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('themeedit_button_no')?></button>
					</div>
					<input type="hidden" id="cid" name="cid" value="<?=$cid?>">
					<input type="hidden" id="thid" name="thid" value="<?=$thid?>">
				</form>
			</div>
		</div>
	</div>

	<?=$script?>
