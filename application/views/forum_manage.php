<?php
/*
 * フォーラム管理画面ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('forum_manage_lang');

$c = $course_data[0];
?>
	<h1 class="sr-only"><?=$this->lang->line('forum_manage_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=htmlspecialchars($c->course_name)?></div>
			<div class="panel-body">
				<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
<?php
if (!is_null($error_msg)) {
?>
				<div class="alert alert-danger" role="alert"><?=$error_msg?></div>
<?php
}
if ($auth != $this->config->item('AUTH_GUEST')) {
	// ゲスト以外は「新しいフォーラム」ボタンを表示
?>
				<p class="text-right">
					<button class="btn btn-primary" data-toggle="modal" data-target="#new_forum_modal" data-pre="<?=$pre?>" data-thid="<?=$thid?>"><?=$this->lang->line('forum_manage_newforum')?> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
				</p>
<?php
}
?>				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th><?=$this->lang->line('forum_manage_forumname')?></th>
								<th><?=$this->lang->line('forum_manage_owner')?></th>
								<th><?=$this->lang->line('forum_manage_posts')?></th>
								<th><?=$this->lang->line('forum_manage_latest')?></th>
								<th>　</th>
								<th>　</th>
							</tr>
						</thead>
						<tbody>
<?php
if ($forummanage_data !== null and !empty($forummanage_data)) {
	foreach($forummanage_data as $f) :
?>
							<tr>
								<td>
<?php
		if ($f['use'] == $this->config->item('FORUM_USE_FLAG_ON')) {
?>
									<a class="link_underline" href="<?=$this->commonlib->baseUrl()?>forum?cid=<?=$c->id?>&pre=<?=$pre?>&thid=<?=$thid?>&fid=<?=$f['id']?>">
<?php
		}
?>
										<?=htmlspecialchars($f['forum_name'])?>
<?php
		if ($f['use'] == $this->config->item('FORUM_USE_FLAG_ON')) {
?>
									</a>
<?php
		}
?>
								</td>
								<td><?=$f['nickname']?></td>
								<td><?=$f['cnt']?></td>
								<td><?=$f['lastest_post']?></td>
								<td>
<?php
		// 自分の投稿または、講師、運用管理者、システム管理者の場合は、停止・再開ボタンを表示する。
		if ($f['owner_flg'] == 1
		 || $auth == $this->config->item('AUTH_TEACHER')
		 || $auth == $this->config->item('AUTH_OP_MANAGER')
		 || $auth == $this->config->item('AUTH_SYS_MANAGER')) {
?>
									<form class="form-horizontal" name="forum_manage" method="post" action="<?=$this->commonlib->baseUrl()?>forummanage/edit">
<?php
			if ($f['use'] == $this->config->item('FORUM_USE_FLAG_ON')) {
?>
										<button class="btn btn-success" type="submit" name="action" value="stop"><?=$this->lang->line('forum_manage_button_stop')?></button>
<?php
			} else {
?>
										<button class="btn btn-default" type="submit" name="action" value="resumption"><?=$this->lang->line('forum_manage_button_resumption')?></button>
<?php
			}
?>
										<input type="hidden" name="cid" id="cid" value="<?=$c->id?>">
										<input type="hidden" name="fid" id="fid" value="<?=$f['id'] ?>">
										<input type="hidden" name="pre" id="pre" value="<?=$pre?>">
										<input type="hidden" name="thid" id="thid" value="<?=$thid?>">
									</form>
<?php
		}
?>
								</td>
								<td>
<?php
		// 自分の投稿または、講師、運用管理者、システム管理者の場合は、削除ボタンを表示する。
		if ($f['owner_flg'] == 1
		 || $auth == $this->config->item('AUTH_TEACHER')
		 || $auth == $this->config->item('AUTH_OP_MANAGER')
		 || $auth == $this->config->item('AUTH_SYS_MANAGER')) {
?>
									<button class="btn btn-warning" data-toggle="modal" data-target="#delete_forum_modal" data-fid="<?=$f['id']?>">
										<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
									</button>
<?php
		}
?>
								</td>
							</tr>
<?php
	endforeach;
}
?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- 新しいフォーラムダイアログ -->
	<div class="modal fade" id="new_forum_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="forum_registration" name="forum_registration" method="post" enctype="multipart/form-data" action="<?=$this->commonlib->baseUrl()?>forummanage/registration">
					<div class="modal-body">
						<div class="form-group">
							<input type="text" class="form-control" name="forum_name" id="forum_name" placeholder="<?=$this->lang->line('forum_manage_entry_input_defalut')?>" required>
							<input type="hidden" name="cid" id="cid" value="<?=$c->id?>">
							<input type="hidden" name="pre" id="pre" value="<?=$pre?>">
							<input type="hidden" name="thid" id="thid" value="<?=$thid?>">
						</div>
						<div class="form-group">
							<textarea class="form-control" rows="5" name="postmsg" id="postmsg" placeholder="<?=$this->lang->line('forum_manage_textarea_default_message')?>" required></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<div class="pull-left" id="upload_image_btn_area">
							<label class="btn btn-success" for="upload_imagefile">
								<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
								<input type="file" name="upload_imagefile" id="upload_imagefile" accept="image/gif,image/png,image/jpeg" style="display:none;">
							</label>
						</div>
						<img id="upload_imagefile_thumbnail" class="img-responsive" src="">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('forum_manage_button_cancel')?></button>
						<button type="submit" class="btn btn-primary"><?=$this->lang->line('forum_manage_button_registration')?></button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- 削除確認ダイアログ -->
	<div class="modal fade" id="delete_forum_modal" tabindex="-2">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-inline" name="forum_delete" method="post" action="<?=$this->commonlib->baseUrl()?>forummanage/delete">
					<div class="modal-body">
						<div class="form-group">
							<label for="confim_title"><?=$this->lang->line('forum_manage_confim_title')?></label><br>
							<label for="delete_confim"><?=$this->lang->line('forum_manage_delete_confim')?></label>
							<input type="hidden" name="cid" id="cid" value="<?=$c->id?>">
							<input type="hidden" name="pre" id="pre" value="<?=$pre?>">
							<input type="hidden" name="thid" id="thid" value="<?=$thid?>">
							<input type="hidden" id="fid" name="fid">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('forum_manage_button_cancel')?></button>
						<button type="submit" class="btn btn-primary"><?=$this->lang->line('forum_manage_button_delete')?></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?=$script?>
