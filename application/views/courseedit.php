<?php
/*
 * 講座編集画面ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('courseedit_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('courseedit_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=$this->lang->line('courseedit_pagetitle')?></div>
			<div class="panel-body">
				<aside id="error"></aside>
				<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
<?php
if (!is_null($complete_msg)) {
	// 処理完了メッセージがある場合は表示
?>
				<div class="alert alert-success" role="alert"><?=$complete_msg?></div>
<?php
}

if (!is_null($error_msg)) {
	// エラーメッセージがある場合は表示
?>
				<div class="alert alert-danger" role="alert"><?=$error_msg?></div>
<?php
}
?>
				<?=form_open($this->commonlib->baseUrl().'courseedit/regist?cid='.$cid, array('class' => 'form-horizontal', 'id' => 'courseedit_form', 'name' => 'courseedit_form', 'accept-charset' => 'utf-8'))?>
					<div class="form-group">
						<label for="courseedit_courseid" class="col-md-2 control-label"><?=$this->lang->line('courseedit_courseid')?></label>
						<div class="col-md-2">
							<input class="form-control" type="text" id="courseedit_courseid" name="courseedit_courseid" value="<?=set_value('courseedit_courseid', $course->course_code)?>">
						</div>
					</div>
					<div class="form-group">
						<label for="courseedit_coursename" class="col-md-2 control-label"><?=$this->lang->line('courseedit_coursename')?></label>
						<div class="col-md-4">
							<input class="form-control" type="text" id="courseedit_coursename" name="courseedit_coursename" value="<?=set_value('courseedit_coursename', $course->course_name)?>">
						</div>
					</div>
					<div class="form-group">
						<label for="courseedit_genre" class="col-md-2 control-label"><?=$this->lang->line('courseedit_genre')?></label>
						<div class="col-md-4">
							<select id="courseedit_genre" name="courseedit_genre" class="form-control">
								<option value="" ><?=$this->lang->line('courseedit_genre_default')?></option>
<?php
if (!is_null($genre_list)) {
	// ジャンルのプルダウンを生成
	foreach ($genre_list as $_genre) {
		$_default_selected = false;
		if ($_genre->id == $course->genre) {
			$_default_selected = true;
		}
?>
								<option value="<?=$_genre->id?>"<?=set_select('courseedit_genre', $_genre->id, $_default_selected)?>><?=$_genre->genrename?></option>
<?php
	}
}
?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="courseedit_description" class="col-md-2 control-label"><?=$this->lang->line('courseedit_description')?></label>
						<div class="col-md-10">
							<textarea class="form-control" rows="5" id="courseedit_description" name="courseedit_description"><?=set_value('courseedit_description', $course->course_description)?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="courseedit_teachername" class="col-md-2 control-label"><?=$this->lang->line('courseedit_teachername')?></label>
						<div class="col-md-2 courseedit_teachername"><?=$course->teacher_name?></div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-2 col-md-10">
							<img class="img-responsive" src="<?=$course->picture_file?>">
						</div>
					</div>
					<div class="form-group">
						<label for="mgr" class="col-md-2 control-label"><?=$this->lang->line('courseedit_op_managername')?></label>
						<div class="col-md-2">
							<p class="form-control-static"><?=$course->op_mng_name?></p>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-2 col-md-10">
							<img class="img-responsive" src="<?=$course->op_mng_picture_file?>">
						</div>
					</div>
					<div class="form-group">
						<label for="courseedit_use_forum" class="col-md-2 control-label"><?=$this->lang->line('courseedit_use_forum')?></label>
						<div class="col-md-8">
							<input id="courseedit_use_forum" name="courseedit_use_forum" type="checkbox" data-on-text="Yes" data-off-text="No" value="1" <?=set_checkbox('courseedit_use_forum', '1', $course->forum_use)?>>
						</div>
					</div>
<?php
$_use_schedule_readonly = '';
if ($disable_use_schedule
 && !is_null($theme_list)
 && count($theme_list) > 0) {
	// テーマが存在する場合は、
	// 一つでも公開または公開終了のテーマがある場合
	// または、一つでもテーマの日付け、期限が設定されている場合は、
	// 「講座の期間を設定する」を変更させない
	// （すべてのテーマが非公開かつ、テーマの日付、期限がまだひとつも設定されていない場合は、変更が可能）
	// ※もともとテーマが存在する場合は「講座の期間を設定する」を変更させない仕様であったが、
	// 　講座のコピー時に期間をクリアして作成する仕様となったため、
	// 　講座配下のテーマが全て非公開かつ、日付、期限が未設定の場合は後から変更できるようにする為に
	// 　このような仕様に変更となった。
	$_use_schedule_readonly = ' readonly';
}
?>
					<div class="form-group">
						<label for="courseedit_use_schedule" class="col-md-2 control-label"><?=$this->lang->line('courseedit_use_schedule')?></label>
						<div class="col-md-10">
							<input id="courseedit_use_schedule" name="courseedit_use_schedule" type="checkbox" data-on-text="Yes" data-off-text="No" value="1" <?=set_checkbox('courseedit_use_schedule', '1', $course->schedule_use)?><?=$_use_schedule_readonly?>>
							<span id="courseedit_use_schedule_guide_help" data-toggle="tooltip" title="<?=$this->lang->line('courseedit_tooltip_guide')?>" data-placement="top" class="guide_icon glyphicon glyphicon-question-sign" aria-hidden="true"></span>
							<p>
								<div id="courseedit_use_schedule_guide" class="alert alert-info help" role="alert" hidden>
									<ul>
										<li><?=$this->lang->line('courseedit_use_schedule_guide1')?></li>
										<li><?=$this->lang->line('courseedit_use_schedule_guide2')?></li>
										<li><?=$this->lang->line('courseedit_use_schedule_guide3')?></li>
									</ul>
								</div>
							</p>
						</div>
					</div>
					<div class="form-group">
						<label for="courseedit_startdate" class="col-md-2 control-label"><?=$this->lang->line('courseedit_startdate')?></label>
						<div class="col-md-2">
							<input class="form-control" type="text" id="courseedit_startdate" name="courseedit_startdate" value="<?=set_value('courseedit_startdate', $course->start_date)?>">
						</div>
					</div>
					<div class="form-group">
						<label for="courseedit_enddate" class="col-md-2 control-label"><?=$this->lang->line('courseedit_enddate')?></label>
						<div class="col-md-2">
							<input class="form-control" type="text" id="courseedit_enddate" name="courseedit_enddate" value="<?=set_value('courseedit_enddate', $course->end_date)?>">
						</div>
					</div>
					<div class="form-group">
						<label for="color" class="col-md-2 control-label"><?=$this->lang->line('courseedit_calendar_color')?></label>
						<div class="col-md-2">
							<select id="courseedit_calendar_color" name="courseedit_calendar_color" class="form-control">
<?php
foreach ($colorpicker_colors as $color_code=>$color_name) {
	$_color_selected = "";
	if (strcasecmp($color_code, $course->calendar_color) === 0) {
		$_color_selected = " selected";
	}
?>
								<option value="<?=$color_code?>"<?=$_color_selected?>><?=$color_name?></option>
<?php
}
?>
							</select>
						</div>
					</div>
<?php
// ゲスト機能を使用する場合、「ゲストの受講を許可する」のスイッチを表示
if ($this->config->item('USE_GUEST') == 1) {
	$_permit_guest_readonly = '';
	if (!is_null($theme_list) && count($theme_list) > 0) {
		// テーマが存在する場合は、「ゲストの受講を許可する」を変更させない
		$_permit_guest_readonly = ' readonly';
	}
?>
					<div class="form-group">
						<label for="courseedit_permit_guest" class="col-md-2 control-label"><?=$this->lang->line('courseedit_permit_guest')?></label>
						<div class="col-md-10">
							<input id="courseedit_permit_guest" name="courseedit_permit_guest" type="checkbox" data-on-text="Yes" data-off-text="No" value="1" <?=set_checkbox('courseedit_permit_guest', '1', $course->permitted_guest)?><?=$_permit_guest_readonly?>>
							<span id="courseedit_permit_guest_guide_help" data-toggle="tooltip" title="<?=$this->lang->line('courseedit_tooltip_guide')?>" data-placement="top" class="guide_icon glyphicon glyphicon-question-sign" aria-hidden="true"></span>
							<p>
								<div id="courseedit_permit_guest_guide" class="alert alert-info help" role="alert" hidden>
									<ul>
										<li><?=$this->lang->line('courseedit_permit_guest_guide')?></li>
									</ul>
								</div>
							</p>
						</div>
					</div>
<?php
}
?>					<div class="form-group">
						<div class="col-md-offset-2 col-md-8">
<?php
$_btn_copy_disabled = "";
$_btn_delete_disabled = "";
if (is_null($cid) || empty($cid)) {
	// 新規作成（cidが無い場合）は「講座コピー」、「削除」ボタンを無効にする。
	$_btn_copy_disabled = " disabled";
	$_btn_delete_disabled = " disabled";
}
?>
							<button id="courseedit_btn_regist" class="btn btn-primary" type="button"><?=$this->lang->line('courseedit_btn_regist')?></button>
<?php
if ($authority == $this->config->item('AUTH_SYS_MANAGER')) {
	// システム管理者の場合は削除ボタン、講座コピーボタンを表示を表示
?>
							<button id="courseedit_btn_delete" class="btn btn-warning" type="button"<?=$_btn_delete_disabled?>><?=$this->lang->line('courseedit_btn_delete')?></button>
							<button class="btn btn-default" data-toggle="modal" data-target="#courseedit_modal_copy" type="button"<?=$_btn_copy_disabled?>><?=$this->lang->line('courseedit_btn_copy')?></button>
<?php
}
?>
						</div>
					</div>
				</form>
				<hr>
				<p class="text-right">
<?php
$_btn_new_theme_disabled = "";
if (is_null($cid) || empty($cid)) {
	// 新規作成（cidが無い場合）は「新しいテーマ」ボタンは無効にする。
	$_btn_new_theme_disabled = " disabled";
}
?>
					<button id="courseedit_btn_new_theme" class="btn btn-primary" type="button"<?=$_btn_new_theme_disabled?>><?=$this->lang->line('courseedit_btn_new_theme')?> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
				</p>
				<div class="list-group">
				  <p class="list-group-item list-group-item-header"><?=$this->lang->line('courseedit_info_title_theme')?></p>
<?php
if (!is_null($theme_list)) {
	// この講座のテーマの一覧を表示
	foreach ($theme_list as $theme) {
		$_label_close = '';
		if ($theme->open == $this->config->item('THEME_CLOSE')) {
			$_label_close = ' <span class="label label-default">'.$this->lang->line('courseedit_theme_label_close').'</span>';
		} else if ($theme->open == $this->config->item('THEME_PUBLIC_END')) {
			$_label_close = ' <span class="label label-default">'.$this->lang->line('courseedit_theme_label_end').'</span>';
		}
		$_label_over_limit = '';
		if ($theme->over_limit == 1) {
			$_label_over_limit = ' <span class="label label-danger">'.$this->lang->line('courseedit_theme_label_over_limit').'</span>';
		}
		$_label_attended = '';
		if ($theme->attended == 1) {
			$_label_attended = ' <span class="label label-primary">'.$this->lang->line('courseedit_theme_label_attended').'</span>';
		}
?>
				  <a href="<?=$this->commonlib->baseUrl()?>themeedit?cid=<?=$cid?>&thid=<?=$theme->id?>" class="list-group-item"><?=$theme->theme_date?> <?=$theme->name?><?=$_label_close?><?=$_label_over_limit?><?=$_label_attended?></a>
<?php
	}
}
?>
				</div>
			</div>
		</div>
	</div>
	<!-- 講座コピーダイアログ -->
	<div class="modal fade" id="courseedit_modal_copy" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form id="courseedit_form_copy" class="form-horizontal" name="courseedit_form_copy" method="post" action="<?=$this->commonlib->baseUrl()?>courseedit/copy?cid=<?=$cid?>">
						<div class="form-group">
							<label for="courceid" class="col-md-3 control-label"><?=$this->lang->line('courseedit_dialog_copy_courseid')?></label>
							<div class="col-md-9">
								<input class="form-control" id="courseedit_copy_courceid" name="courseedit_copy_courceid" type="text">
							</div>
						</div>
						<div class="form-group">
							<label for="courcename" class="col-md-3 control-label"><?=$this->lang->line('courseedit_dialog_copy_coursename')?></label>
							<div class="col-md-9">
								<input class="form-control" id="courseedit_copy_courcename" name="courseedit_copy_courcename" type="text">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-3 col-md-9">
								<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('courseedit_dialog_copy_btn_cancel')?></button>
								<button id="courseedit_modal_copy_btn_ok" type="submit" class="btn btn-primary"><?=$this->lang->line('courseedit_dialog_copy_btn_ok')?></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- 講座削除ダイアログ -->
	<div class="modal fade" id="courseedit_modal_delete" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
					<h4 class="modal-title"><?=$this->lang->line('courseedit_dialog_delete_title')?></h4>
				</div>
				<div class="modal-body"><?=$course->course_name?> <?=$this->lang->line('courseedit_dialog_delete_body')?></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal"><?=$this->lang->line('courseedit_dialog_delete_btn_cancel')?></button>
					<button id="courseedit_modal_delete_btn_ok" type="button" class="btn btn-default"><?=$this->lang->line('courseedit_dialog_delete_btn_ok')?></button>
				</div>
			</div>
		</div>
	</div>
	<?=$script?>
