<?php
/*
 * 受講者管理画面ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('students_manage_lang');

$course = $course_data[0];
?>
	<h1 class="sr-only"><?=$this->lang->line('students_manage_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=$this->lang->line('students_manage_pagetitle')?></div>
			<div class="panel-body">
				<dl class="dl-horizontal">
					<dt><?=$this->lang->line('students_manage_item_courseid')?></dt>
					<dd><?=htmlspecialchars($course['course_code'])?></dd>
					<dt><?=$this->lang->line('students_manage_item_coursename')?></dt>
					<dd><?=htmlspecialchars($course['course_name'])?></dd>
					<dt><?=$this->lang->line('students_manage_item_genre')?></dt>
					<dd><?=htmlspecialchars($course['genrename'])?></dd>
					<dt><?=$this->lang->line('students_manage_item_lecturer')?></dt>
<?php
// 姓、名の表示を定義に従い、生成する
$_name_column = $this->commonlib->createFullName($course['firstname'], $course['surname']);
?>
					<dd><?=htmlspecialchars($_name_column)?></dd>
					<dd><img class="img-responsive" src="<?=$course['picture_file']?>"></dd>
<?php
if ($course['schedule_use'] == 1) {
?>
					<dt><?=$this->lang->line('students_manage_item_period')?></dt>
					<dd><?=$course['start_date']?> - <?=$course['end_date']?></dd>
<?php
}
?>
				</dl>
				<div class="pull-right studentsmanage_adduser_btn_area">
					<a class="btn btn-primary" href="<?=$this->commonlib->baseUrl()?>studentsregist?cid=<?=$cid?>&target=<?=$auth_students?>"><?=$this->lang->line('students_manage_button_new_students')?> <span class="glyphicon glyphicon-plus" aria-hidden="true"></a>
				</div>
				<div id="page_link">
<?php
// 無限スクロール用“前ページ”リンク
if ($page_data['before_page']):
?>
					<a id="before_page" href="<?=$page_data['before_page']?>"></a>
<?php
endif;

// 無限スクロール用“次ページ”リンク
if ($page_data['next_page']):
?>
					<a id="next_page" href="<?=$page_data['next_page']?>"></a>
<?php
endif;
?>
				</div>
				<table class="table table-hover studentsmanage_table_userinfo">
					<thead>
						<tr>
							<th>　</th>
							<th><?=$this->lang->line('students_manage_list_item_userid')?></th>
							<th><?=$this->lang->line('students_manage_list_item_nickname')?></th>
							<th>　</th>
						</tr>
					</thead>
					<tbody>
<?php
foreach ($students_data AS $std) :
?>
						<tr>
							<td><img class="img-responsive center-block" src="<?=$std['picture_file']?>"></td>
							<td><?=htmlspecialchars($std['userid'])?></td>
							<td><?=htmlspecialchars($std['nickname'])?></td>
							<td>
								<div class="pull-right">
									<button class="btn btn-warning" data-toggle="modal" data-target="#release_students_modal" data-stid="<?=$std['stid']?>"><?=$this->lang->line('students_manage_button_release')?></button>
								</div>
							</td>
						</tr>
<?php
endforeach;
?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<!-- 受講者解除確認ダイアログ -->
	<div class="modal fade" id="release_students_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-inline" name="students_release_form" method="post" action="<?=$this->commonlib->baseUrl()?>studentsmanage/release">
					<div class="modal-body">
						<div class="form-group">
							<label for="confim_title"><?=$this->lang->line('students_manage_release_confim_title')?></label><br>
							<label for="delete_confim"><?=$this->lang->line('students_manage_release_confim')?></label>
							<input type="hidden" id="cid" name="cid" value="<?=$cid?>">
							<input type="hidden" id="page" name="page" value="<?=$page_data['page']?>">
							<input type="hidden" id="stid" name="stid" value="">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('students_manage_button_cancel')?></button>
						<button type="submit" class="btn btn-primary"><?=$this->lang->line('students_manage_button_release')?></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?=$page_data['script']?>