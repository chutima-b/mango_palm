<?php
/*
 * プロフィール画面ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('profile_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('profile_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=$this->lang->line('profile_pagetitle')?></div>
			<div class="panel-body">
				<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
				<?php if (isset($error_msg)) { echo $error_msg; }?>
				<?=form_open_multipart($this->commonlib->baseUrl().'profile/regist', array('class' => 'form-horizontal', 'id' => 'profile_form', 'name' => 'profile_form', 'accept-charset' => 'utf-8'))?>
					<div class="form-group">
						<label class="col-sm-4 control-label" for="profile_nickname"><?=$this->lang->line('profile_nickname')?></label>
						<div class="col-sm-8">
							<input id="profile_nickname" class="form-control" name="profile_nickname" type="text" value="<?=set_value('profile_nickname', $profile->nickname)?>" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" for="profile_imgupload">　</label>
						<div class="col-sm-8">
							<div class="pull-left">
								<img id="thumbnail" class="img-responsive" src="<?=$profile->picture_file?>">
							</div>
							<button class="btn btn-primary profile_btn_imgupload" id="profile_btn_imgupload"><span id="profile_icon_imgupload" class="glyphicon glyphicon-camera" aria-hidden="true"></span></button>
							<input type="file" id="profile_imgupload" name="profile_imgupload" style="display:none;" accept="image/*">
						</div>
					</div>
<!--
					<div class="form-group">
						<label class="col-sm-4 control-label" for="profile_introduction"><?=$this->lang->line('profile_introduction')?><p class="guide"><?=$introduction_max?><?=$this->lang->line('profile_introduction_description')?></p></label>
						<div class="col-sm-8">
							<textarea id="profile_introduction" class="form-control" name="profile_introduction" cols="50" rows="5"><?=set_value('profile_introduction', $profile->introduction)?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label" for="profile_chat_use"><?=$this->lang->line('profile_chat_use')?></label>
						<div class="col-sm-8">
							<input id="profile_chat_use" name="profile_chat_use" type="checkbox" data-on-text="Yes" data-off-text="No" value="1" <?=set_checkbox('profile_chat_use', '1', $profile->chat_use)?>>
						</div>
					</div>
-->
					<div class="form-group">
						<label class="col-md-4 control-label" for="profile_passwd"><?=$this->lang->line('profile_passwd')?></label>
						<div class="col-md-8">
							<input id="profile_passwd" name="profile_passwd" class="form-control" type="password" value="<?=set_value('profile_passwd', '')?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="profile_new_passwd"><?=$this->lang->line('profile_new_passwd')?></label>
						<div class="col-md-8">
							<input id="profile_new_passwd" name="profile_new_passwd" class="form-control" type="password" value="<?=set_value('profile_new_passwd', '')?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="profile_new_passwd_confirm"><?=$this->lang->line('profile_new_passwd_confirm')?></label>
						<div class="col-md-8">
							<input id="profile_new_passwd_confirm" name="profile_new_passwd_confirm" class="form-control" type="password" value="<?=set_value('profile_new_passwd_confirm', '')?>">
						</div>
					</div>
					<div class="pull-right profile_btn_regist_area">
						<button class="btn btn-primary center-block" id="profile_btn_regist" type="submit"><?=$this->lang->line('profile_regist')?></button>
					</div>
				</form>
			</div>
		</div>
	</div>
<?=$script?>
