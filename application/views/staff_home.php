<?php
/*
 * ホーム画面（管理者）ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('staff_home_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('staff_home_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading">
				<?=$this->lang->line('staff_home_pagetitle')?>
			</div>
			<div class="panel-body">
				<aside id="error"></aside>
				<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
						<p class="text-right">
<?php
if ($auth == $this->config->item('AUTH_SYS_MANAGER')
 || $auth == $this->config->item('AUTH_OP_MANAGER'))
{
?>
							<button class="btn btn-primary" data-toggle="modal" data-target="#new_post_modal" data-nid="" data-targetdt="" data-title="" data-content="" data-courseid="" data-calendaruse="" data-color=""><?=$this->lang->line('staff_home_new_notice')?>&nbsp;<span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
<?php
}
?>
						</p>
				<div class="panel panel-default blur">
					<div class="panel-heading">
						<?=$this->lang->line('staff_home_noticetitle')?>
					</div>
					<div class="panel-body">
<?php
if ($notice_data !== null and !empty($notice_data)) {
?>
						<div class="table-responsive touch-scroll">
							<table class="table">
								<thead>
									<tr>
										<th><?=$this->lang->line('staff_home_notice_col_date')?></th>
										<th><?=$this->lang->line('staff_home_notice_col_title')?></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
<?php
	foreach($notice_data as $n) :
?>
									<tr>
										<td><?=$n['created_at']?></td>
										<td>
											<a href="<?=$this->commonlib->baseUrl()?>notice?nid=<?=$n['id']?>&pre=staff"><?=htmlspecialchars($n['title'])?><?php if (!is_null($n['is_new'])) :?>&nbsp;<span class="span_new_mark">NEW</span><?php endif;?></a>
										</td>
										<td>
<?php
		$is_op_notice_edit = false;
		if ($auth == $this->config->item('AUTH_OP_MANAGER')) {
			// 運用管理者の場合は、自分が運用管理者として登録されているお知らせのみ編集/削除を可能とする。
			if (!is_null($notice_course_list)) {
				foreach ($notice_course_list as $notice_course) {
					if ($notice_course->id == $n['course_id']) {
						$is_op_notice_edit = true;
						break;
					}
				}
			}
		}
		if ($is_op_notice_edit
		// システム管理者はすべてのお知らせの編集/削除が可能
		 || $auth == $this->config->item('AUTH_SYS_MANAGER'))
		{
?>
											<button class="btn btn-success" data-toggle="modal" data-target="#new_post_modal" data-nid="<?=$n['id']?>" data-targetdt="<?=$n['target_dt']?>" data-title="<?=htmlspecialchars($n['title'])?>" data-content="<?=htmlspecialchars($n['content'])?>" data-courseid="<?=$n['course_id']?>" data-calendaruse="<?=$n['calendar_use']?>" data-color="<?=$n['calendar_color']?>"><?=$this->lang->line('staff_home_notice_button_edit')?></button>
											<button class="btn btn-warning" data-toggle="modal" data-target="#delete_post_modal" data-nid=<?=$n['id']?>><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
<?php
		}
?>
										</td>
									</tr>
<?php
	endforeach;
?>
								</tbody>
							</table>
						</div>
<?php
} else {
?>
						<hr>
						<div class="alert alert-info" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>&nbsp<?=$this->lang->line('staff_home_notice_none')?>
						</div>
<?php
}
?>
					</div>
				</div>
				<p class="text-right">
<?php
if ($auth == $this->config->item('AUTH_SYS_MANAGER')) {
?>
					<a href="<?=$this->commonlib->baseUrl()?>courseedit"><button class="btn btn-primary"><?=$this->lang->line('staff_home_new_course')?>&nbsp;<span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button></a>
<?php
}
?>
				</p>
<?php
if ($auth == $this->config->item('AUTH_SYS_MANAGER')) {
?>
				<h5><?=$this->lang->line('staff_home_course_count_prefix')?><?=$course_count['cnt']?><?=$this->lang->line('staff_home_course_count')?></h5>
<?php
}
?>
<?php
if ($genre_course_count !== null and !empty($genre_course_count)) {
	if ($course_data !== null and !empty($course_data)) {
?>
				<div class="panel-group" id="accordion">
<?php
		foreach($genre_course_count as $g) :
?>
					<div class="panel panel-default blur">
						<div class="panel-heading">
							<h4 class="panel-title">
								<!--
								data-toggle : Collapseを起動させる
								data-parent : アコーディオン風に閉じたり開いたりするためのもの
								href : 指定した場所のパネルを開く
								-->
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $g['id'] ?>">
									<?php if (!is_null($g['icon'])) : ?><img class="navpic" src="<?=$g['icon']?>">&nbsp;<?php endif; ?><?=$g['genrename']?><?php if ($auth == $this->config->item('AUTH_SYS_MANAGER')) { ?>&nbsp;<span class="badge"><?=$g['cnt']?></span><?php } ?>
								</a>
							</h4>
						</div>
						<div id="collapse<?=$g['id']?>" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="table-responsive touch-scroll">
									<table class="table">
										<thead>
<?php
			if (0 < $g['cnt']) {
?>
											<tr>
												<th><?=$this->lang->line('staff_home_course_col_course_code')?></th>
												<th><?=$this->lang->line('staff_home_course_col_course_name')?></th>
												<th><?=$this->lang->line('staff_home_course_col_lecturer')?></th>
												<th><?=$this->lang->line('staff_home_course_col_theme_count')?></th>
												<th><?=$this->lang->line('staff_home_course_col_students_count')?></th>
												<th><?=$this->lang->line('staff_home_course_col_period')?></th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
											</tr>
<?php
			}
?>
										</thead>
										<tbody>
<?php
			foreach($course_data as $c) :
				if ($g['id'] == $c['genre']) {
					$_fullname = $this->commonlib->createFullName($c['firstname'], $c['surname']);
?>
											<tr>
												<td><?=htmlspecialchars($c['course_code']) ?></td>
												<td><?=htmlspecialchars($c['course_name']) ?></td>
												<td><?=htmlspecialchars($_fullname) ?></td>
												<td><?=$c['ccnt']?></td>
												<td><?=$c['ucnt']?></td>
												<td>
<?php
					if ($c['schedule_use'] == 1) {
?>
													<?=$c['start_date']?> - <?=$c['end_date']?>
<?php
					}
?>
												</td>
												<td><a href="<?=$this->commonlib->baseUrl()?>courseedit?cid=<?=$c['id']?>"><button class="btn btn-success"><?=$this->lang->line('staff_home_course_button_edit')?></button></a></td>
<?php
					if ($c['authority'] == $this->config->item('AUTH_SYS_MANAGER') or $c['authority'] == $this->config->item('AUTH_OP_MANAGER')) {
?>
												<td><a href="<?=$this->commonlib->baseUrl()?>staffmanager?cid=<?=$c['id']?>"><button class="btn btn-warning"><?=$this->lang->line('staff_home_course_button_staff')?></button></a></td>
<?php
						if ($c['nickname'] == null or $c['nickname'] == "") {
?>
												<td><button class="btn btn-primary" disabled><?=$this->lang->line('staff_home_course_button_students')?></button></td>
<?php
						} else {
?>
												<td><a href="<?=$this->commonlib->baseUrl()?>studentsmanage?cid=<?=$c['id']?>"><button class="btn btn-primary"><?=$this->lang->line('staff_home_course_button_students')?></button></a></td>
<?php
						}
?>
<?php
					}
?>
												<td><a href="<?=$this->commonlib->baseUrl()?>taskmanager?cid=<?=$c['id']?>"><button class="btn btn-info"><?=$this->lang->line('staff_home_course_button_task')?></button></a></td>
<?php
					if ($c['authority'] != $this->config->item('AUTH_SYS_MANAGER') && $c['authority'] != $this->config->item('AUTH_OP_MANAGER')) {
?>
												<td></td><td></td>
<?php
					}
?>											</tr>
<?php
				}
			endforeach;
?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
<?php
		endforeach;
?>
				</div>
<?php
	}
}
?>
			</div>
		</div>
	</div>

	<!-- 新しいお知らせ、編集ダイアログ -->
	<div class="modal fade" id="new_post_modal" tabindex="-1">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-body">
					<aside id="error_notice_edit"></aside>
					<form class="form-horizontal" id="notice_edit_form" name="notice_edit_form" method="post" action="<?=$this->commonlib->baseUrl()?>staffhome/noticeedit">
						<div class="form-group">
							<label for="title" class="col-md-3 control-label"><?=$this->lang->line('staff_home_notice_col_title')?></label>
							<div class="col-md-9">
								<input class="form-control" id="title" name="title" type="text" required>
							</div>
						</div>
						<div class="form-group">
							<label for="content" class="col-md-3 control-label"><?=$this->lang->line('staff_home_notice_col_content')?></label>
							<div class="col-md-9">
								<textarea class="form-control" id="notice_content" name="content" rows="5"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="course" class="col-md-3 control-label"><?=$this->lang->line('staff_home_notice_col_course')?></label>
							<div class="col-md-9">
								<select id="course" name="course" class="form-control">
									<option value="" ><?=$this->lang->line('staff_home_notice_col_course_option_default')?></option>
<?php
if ($c['authority'] == $this->config->item('AUTH_SYS_MANAGER')) {
?>
									<option value="0" ><?=$this->lang->line('staff_home_notice_col_course_option_all')?></option>
<?php
}
if (!is_null($genre_course_count) && !is_null($notice_course_list)) {
	foreach ($genre_course_count as $genre) {
		if ($genre['cnt'] == 0) {
			continue;
		}
		$_is_display_grouptag = false;
		$_course_display_cnt = 0;
		foreach ($notice_course_list as $notice_course) {
			if ($genre['id'] != $notice_course->genre) {
				continue;
			}
			if (!$_is_display_grouptag) {
?>
									<optgroup label="<?=$genre['genrename']?>">
<?php
				$_is_display_grouptag = true;
			}
?>
										<option value="<?=$notice_course->id?>" ><?=htmlspecialchars($notice_course->course_name)?></option>
<?php
			$_course_display_cnt++;
			if ($_course_display_cnt == $genre['cnt']) {
?>
									</optgroup>
<?php
			}
		}
	}
}
?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="calendar_use" class="col-md-3 control-label"><?=$this->lang->line('staff_home_notice_col_calendar_use')?></label>
							<div class="col-md-9">
								<input id="calendar_use" name="calendar_use" type="checkbox" value="1" >
							</div>
						</div>
						<div id="calendar_use_items">
							<div class="form-group">
								<label for="date" class="col-md-3 control-label"><?=$this->lang->line('staff_home_notice_col_date')?></label>
								<div class="col-md-2">
									<input class="form-control" id="targetdt" name="targetdt" type="text" required>
								</div>
							</div>
							<div class="form-group">
								<label for="color" class="col-md-3 control-label"><?=$this->lang->line('staff_home_notice_col_color')?></label>
								<div class="col-md-9">
									<select id="color" name="color" class="form-control">
<?php
foreach ($colorpicker_colors as $color_code=>$color_name) {
?>
										<option value="<?=$color_code?>"><?=$color_name?></option>
<?php
}
?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-3 col-md-9">
								<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('staff_home_notice_button_cancel')?></button>
								<button id="notice_entry" type="submit" class="btn btn-primary"><?=$this->lang->line('staff_home_notice_button_entry')?></button>
							</div>
						</div>
						<input type="hidden" name="nid" id="nid" value="">
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- 削除確認ダイアログ -->
	<div class="modal fade" id="delete_post_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-inline" name="notice_delete_form" method="post" action="<?=$this->commonlib->baseUrl()?>staffhome/noticedelete">
					<div class="modal-body">
						<div class="form-group">
							<label for="confim_title"><?=$this->lang->line('staff_home_notice_confim_title')?></label><br>
							<label for="delete_confim"><?=$this->lang->line('staff_home_notice_delete_confim')?></label>
							<input type="hidden" id="nid" name="nid" value="">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('staff_home_notice_button_cancel')?></button>
						<button type="submit" class="btn btn-primary"><?=$this->lang->line('staff_home_notice_button_delete')?></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?=$script?>
