<?php
/*
 * チャット管理画面ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('chatmanager_lang');

// 壁紙
$_wallpaper = '';
if (!is_null($this->session->userdata('wallpaper'))) {
	$_wallpaper = $this->session->userdata('wallpaper');
} else if (isset($tmp_wallpaper)) {
	$_wallpaper = $tmp_wallpaper;
}

// 背景色
$_backcolor = '';
if (!is_null($this->session->userdata('backcolor'))) {
	$_backcolor = $this->session->userdata('backcolor');
} else if (isset($tmp_backcolor)) {
	$_backcolor = $tmp_backcolor;
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<link rel="stylesheet" href="<?=$this->commonlib->baseUrl()?>libs/bootstrap-3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=$this->commonlib->baseUrl()?>css/common.css?v=1.0.0">
	<link rel="stylesheet" href="<?=$this->commonlib->baseUrl()?>css/slms_loading.css?v=1.0.0">
	<link rel="stylesheet" href="<?=$this->commonlib->baseUrl()?>css/chatmanager.css?v=1.0.0">
	<style>
		body {
			background-color: <?=$_backcolor?>;
		}
<?php
if ($_wallpaper) {
?>
		body,
		.blur::before {
			background: url(<?=$_wallpaper?>) 0 / cover fixed;
		}
<?php
}
?>
	</style>
	<title><?=$this->lang->line('chatmanager_header_pagetitle')?></title>
	<script src="<?=$this->commonlib->baseUrl()?>libs/jquery/jquery-2.2.4.min.js"></script>
	<script src="<?=$this->commonlib->baseUrl()?>libs/bootstrap-3.3.6/js/bootstrap.min.js"></script>
	<script src="<?=$this->commonlib->baseUrl()?>js/slms_loading.js?v=1.0.0"></script>
	<script src="<?=$this->commonlib->baseUrl()?>js/chatmanager.js?v=1.0.1"></script>

</head>
<body>
	<h1 class="sr-only"><?=$this->lang->line('chatmanager_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=htmlspecialchars($tk_profile->nickname)?></div>
			<div class="panel-body">
				<div id="chatarea">
<?php
$_first_id = 0;
$_last_id = 0;
if (!is_null($chat_data)) {
	foreach ($chat_data as $_data) {
		if (!$_first_id) {
			$_first_id = $_data->id; // 先頭のID設定
		}
		$_last_id = $_data->id; // 最終のID更新
		if ($_data->is_self == 1) { // 自身の投稿
?>
					<div class="chat_box_right">
						<div class="chat_area_right">
							<div class="chat_hukidashi_right">
								<?=htmlspecialchars($_data->post)?>
<?php
			if (!is_null($_data->image_file)) {
?>
								<img class="img-responsive center-block" src="<?=$_data->image_file?>">
<?php
			}
?>
							</div>
						</div>
						<div class="chat_area_right">
							<div class="chat_info chat_info_right">
								<?=$_data->nickname?>&nbsp;<?=$_data->created_at?>
							</div>
						</div>
					</div>
<?php
		} else { // チャット相手の投稿
?>
					<div class="chat_box_left">
						<div class="chat_face_left">
							<img class="img-responsive img-circle" src="<?=$_data->profile_img?>">
						</div>
						<div class="chat_area_left">
							<div class="chat_hukidashi_left">
								<?=htmlspecialchars($_data->post)?>
<?php
			if (!is_null($_data->image_file)) {
?>
								<img class="img-responsive center-block" src="<?=$_data->image_file?>">
<?php
			}
?>
							</div>
						</div>
						<div class="chat_area_left">
							<div class="chat_info chat_info_left">
								<?=$_data->nickname?>&nbsp;<?=$_data->created_at?>
							</div>
						</div>
					</div>
<?php
		}
	}
}
?>
				</div>
				<form id="chat_form" class="form-horizontal" method="post" action="<?=$this->commonlib->baseUrl()?>chatmanager?cid=<?=$cid?>&tkid=<?=$tkid?>">
					<div class="form-group">
						<div class="col-xs-offset-1 col-xs-2">
							<div class="chat_student_label">
								<label class="control-label" for="chatmanager_slct_students"><?=$this->lang->line('chatmanager_label_students')?></label>
							</div>
						</div>
						<div class="col-xs-9">
							<select id="chatmanager_slct_students" class="form-control" name="uid">
								<option value=""<?=set_select('uid', '', true)?>><?=$this->lang->line('chatmanager_select_msg')?></option>
<?php
if (!is_null($user_list)) {
	foreach ($user_list as $_user) :
?>
								<option value="<?=$_user->uid?>"<?=set_select('uid', $_user->uid)?>><?=$_user->nickname?></option>
<?php
	endforeach;
}
?>
							</select>
						</div>
					</div>
					<input type="hidden" id="chat_firstid" name="chat_firstid" value="<?=$_first_id?>">
					<input type="hidden" id="chat_lastid" name="chat_lastid" value="<?=$_last_id?>">
				</form>
			</div>
		</div>
	</div>
	<button id="chat_btn_modal" type="button" data-toggle="modal" data-target="#chat_modal" style="display:none;">モーダル</button>
	<div class="modal fade" id="chat_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
					<h4 class="modal-title" id="chat_modal_title"></h4>
				</div>
				<div class="modal-body" id="chat_modal_body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
				</div>
			</div>
		</div>
	</div>
	<?=$script?>
</body>
</html>
