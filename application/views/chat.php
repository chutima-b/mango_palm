<?php
/*
 * チャット画面ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('chat_lang');

// 壁紙
$_wallpaper = '';
if (!is_null($this->session->userdata('wallpaper'))) {
	$_wallpaper = $this->session->userdata('wallpaper');
} else if (isset($tmp_wallpaper)) {
	$_wallpaper = $tmp_wallpaper;
}

// 背景色
$_backcolor = '';
if (!is_null($this->session->userdata('backcolor'))) {
	$_backcolor = $this->session->userdata('backcolor');
} else if (isset($tmp_backcolor)) {
	$_backcolor = $tmp_backcolor;
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<link rel="stylesheet" href="<?=$this->commonlib->baseUrl()?>libs/bootstrap-3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=$this->commonlib->baseUrl()?>css/common.css?v=1.0.0">
	<link rel="stylesheet" href="<?=$this->commonlib->baseUrl()?>css/slms_loading.css?v=1.0.0">
	<link rel="stylesheet" href="<?=$this->commonlib->baseUrl()?>css/chat.css?v=1.0.0">
	<style>
		body {
			background-color: <?=$_backcolor?>;
		}
<?php
if ($_wallpaper) {
?>
		body,
		.blur::before {
			background: url(<?=$_wallpaper?>) 0 / cover fixed;
		}
<?php
}
?>
	</style>
	<title><?=$this->lang->line('chat_header_pagetitle')?></title>
	<script src="<?=$this->commonlib->baseUrl()?>libs/jquery/jquery-2.2.4.min.js"></script>
	<script src="<?=$this->commonlib->baseUrl()?>libs/bootstrap-3.3.6/js/bootstrap.min.js"></script>
	<script src="<?=$this->commonlib->baseUrl()?>js/slms_loading.js?v=1.0.0"></script>
	<script src="<?=$this->commonlib->baseUrl()?>js/chat.js?v=1.0.2"></script>

</head>
<body>
	<h1 class="sr-only"><?=$this->lang->line('chat_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=htmlspecialchars($tk_profile->nickname)?></div>
			<div class="panel-body">
				<div id="chatarea">
<?php
$_first_id = 0;
$_last_id = 0;
if (!is_null($chat_data)) {
	foreach ($chat_data as $_data) {
		if (!$_first_id) {
			$_first_id = $_data->id; // 先頭のID設定
		}
		$_last_id = $_data->id; // 最終のID更新
		if ($_data->is_self == 1) { // 自身の投稿
?>
					<div class="chat_box_right">
						<div class="chat_area_right">
							<div class="chat_hukidashi_right">
								<?=htmlspecialchars($_data->post)?>
<?php
			if (!is_null($_data->image_file)) {
?>
								<img class="img-responsive center-block" src="<?=$_data->image_file?>">
<?php
			}
?>
							</div>
						</div>
						<div class="chat_area_right">
							<div class="chat_info chat_info_right">
								<?=$_data->nickname?>&nbsp;<?=$_data->created_at?>
							</div>
						</div>
					</div>
<?php
		} else { // チャット相手の投稿
?>
					<div class="chat_box_left">
						<div class="chat_face_left">
							<img class="img-responsive img-circle" src="<?=$_data->profile_img?>">
						</div>
						<div class="chat_area_left">
							<div class="chat_hukidashi_left">
								<?=htmlspecialchars($_data->post)?>
<?php
			if (!is_null($_data->image_file)) {
?>
								<img class="img-responsive center-block" src="<?=$_data->image_file?>">
<?php
			}
?>
							</div>
						</div>
						<div class="chat_area_left">
							<div class="chat_info chat_info_left">
								<?=$_data->nickname?>&nbsp;<?=$_data->created_at?>
							</div>
						</div>
					</div>
<?php
		}
	}
}
?>
				</div>
				<form id="chat_form" class="form">
					<div class="row">
						<div class="col-xs-12">
							<div class="input-group">
								<span class="input-group-btn">
									<label class="btn btn-success" for="chat_upload_imagefile">
										 <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
										<input type="file" id="chat_upload_imagefile" name="chat_upload_imagefile" accept="image/gif,image/png,image/jpeg" style="display:none;">
									</label>
								</span>
								<input type="text" class="form-control" id="chat_message" name="chat_message" placeholder="<?=$this->lang->line('chat_message_placeholder')?>">
								<span class="input-group-btn">
									<button type="button" id="chat_submit" class="btn btn-primary"><?=$this->lang->line('chat_btn_submit')?></button>
								</span>
								<input type="hidden" id="chat_firstid" name="chat_firstid" value="<?=$_first_id?>">
								<input type="hidden" id="chat_lastid" name="chat_lastid" value="<?=$_last_id?>">
							</div><!-- /input-group -->
							<br>
							<img id="upload_imagefile_thumbnail" class="img-responsive" src="">
						</div><!-- /.col-lg-6 -->
					</div><!-- /.row -->
				</form>
			</div>
		</div>
	</div>
	<button id="chat_btn_modal" type="button" data-toggle="modal" data-target="#chat_modal" style="display:none;">モーダル</button>
	<div class="modal fade" id="chat_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
					<h4 class="modal-title" id="chat_modal_title"></h4>
				</div>
				<div class="modal-body" id="chat_modal_body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
				</div>
			</div>
		</div>
	</div>
	<?=$script?>
</body>
</html>
