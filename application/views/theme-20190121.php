<?php
/*
 * テーマ画面ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('theme_lang');

$theme = $theme_data;
if (!is_null($theme)) {
?>
	<h1 class="sr-only"><?=$this->lang->line('theme_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?php if (!is_null($theme['icon'])) : ?><img class="navpic" src="<?=$theme['icon']?>">&nbsp;<?php endif; ?><?=htmlspecialchars($theme['course_name'])?></div>
			<div class="panel-body">
<?php
	if (!is_null($complete_msg)) {
?>
				<div class="alert alert-success" role="alert"><?=$complete_msg?></div>
<?php
	}
?>
<?php
	if (!is_null($error_msg)) {
?>
				<div class="alert alert-danger" role="alert"><?=$error_msg?></div>
<?php
	}
?>
				<div class="row">
					<div class="col-sm-9">
						<div class="panel panel-default blur">
							<div class="panel-heading">
								<?=$theme['theme_date']?> <?=htmlspecialchars($theme['name'])?><?php if ($theme['open'] == 0) : ?>&nbsp;<span class="label label-default"><?=$this->lang->line('theme_label_close')?></span><?php endif; ?>
							</div>
							<div class="panel-body">
								<p><?=nl2br(htmlspecialchars($theme['goal']))?></p>
								<hr>
								<p><?=$theme['description']?></p>
<?php
	if ($theme['evaluation_use'] == 1) {
		// 「課題を作成する」がYESの場合
?>								<hr>
								<dl class="dl-horizontal">
<?php
		if (!$is_quiz) {
			// クイズ以外の場合は「提出期限」を表示する
?>									<dt><?=$this->lang->line('theme_time_limit')?></dt>
									<dd><?=$theme['time_limit']?></dd>
<?php
		}
?>									<dt><?=$this->lang->line('theme_submit_at')?></dt>
									<dd><?=$taskfeedback_data->submit_at?></dd>
<?php
		if (!$is_quiz) {
		// クイズ以外の場合は「フィードバック」を表示する
?>									<dt><?=$this->lang->line('theme_feedback_at')?></dt>
									<dd><?=$taskfeedback_data->feedback_at?></dd>
<?php
		}

		// 「出題」の項目を表示
		// ※クイズ以外の場合
		// 　このテーマの課題の評価点合計の評価点を表示
		// 　クイズの場合
		// 　出題数を表示
		$_evaluation_point_item = $this->lang->line('theme_examination');
		if ($is_feedback) {
			// フィードバック済みの場合は項目名を「評価」に変更
			// ※クイズ以外の場合
			// 　実際の“評価点/評価点”を表示
			// 　クイズの場合
			// 　“クイズの正解数/出題数”を表示
			$_evaluation_point_item = $this->lang->line('theme_evaluation');
		}
?>
									<dt><?=$_evaluation_point_item?></dt>
									<dd><?=$task_point?></dd>
								</dl>
<?php
	}
?>
							</div>
							<div class="panel-footer">
<?php
	if ($theme['forum_use'] == $this->config->item('FORUM_USE_FLAG_ON')) {
?>
								<a href="<?=$this->commonlib->baseUrl()?>forummanage?cid=<?=$theme['course_id']?>&thid=<?=$theme['id']?>&pre=theme"><?=$this->lang->line('theme_task_link_forum')?></a>&nbsp;&nbsp;|&nbsp;&nbsp;
<?php
	}
	if (intval($students_data['authority']) === $this->config->item('AUTH_SYS_MANAGER')
		|| (intval($students_data['authority']) !== $this->config->item('AUTH_GUEST') && $students_data['chat_use'] == 1)) {
?>
								<a href="<?=$this->commonlib->baseUrl()?>chatlist?cid=<?=$theme['course_id']?>"><?=$this->lang->line('theme_task_link_chat')?></a>&nbsp;&nbsp;|&nbsp;&nbsp;
<?php
	}
?>
								<a href="<?=$this->commonlib->baseUrl()?>diary?cid=<?=$theme['course_id']?>&thid=<?=$theme['id'] ?>&pre=theme"><?=$this->lang->line('theme_task_link_diary')?></a>
							</div>
						</div>
<?php
	if (count($materials_data) > 0) {
?>
						<div class="panel panel-default blur">
							<div class="panel-heading"><?=$this->lang->line('theme_teaching_materialtitle')?></div>
							<div class="panel-body">
<?php
		foreach ($materials_data as $r) :
?>
								<p>
									<a href="<?=$r['link_path']?>" target="_blank">
<?php
			if ($r['material_type'] == $this->config->item('MATERIAL_TYPE_EXTERNAL')) {
				// 外部リンク
?>
										<span class="label label-primary"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></span>
<?php
			} elseif ($r['material_type'] == $this->config->item('MATERIAL_TYPE_PICTURE')) {
				// 写真
?>
										<span class="label label-primary"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></span>
<?php
			} elseif ($r['material_type'] == $this->config->item('MATERIAL_TYPE_MOVIE')) {
				// 動画
?>
										<span class="label label-primary"><span class="glyphicon glyphicon-film" aria-hidden="true"></span></span>
<?php
			} elseif ($r['material_type'] == $this->config->item('MATERIAL_TYPE_PDF')) {
				// PDF
?>
										<span class="label label-primary"><span class="glyphicon glyphicon-file" aria-hidden="true"></span></span>
<?php
			}
?>
									<?=htmlspecialchars($r['material_description'])?></a>
								</p>
<?php
		endforeach;
?>
							</div>
						</div>
<?php
	}
	if ($task_data !== null and !empty($task_data)) {
?>
						<p class="guide"><?=$this->lang->line('theme_guide_task')?></p>
<?php
		foreach($task_data as $t) :
?>
						<div class="panel panel-default blur">
							<div id="task_<?=$t['id']?>" class="panel-heading"><?=htmlspecialchars($t['task_name'])?></div>
							<div class="panel-body">
<?php
			if (isset($t['is_correct_quiz'])) {
				// クイズの正答情報がある場合
				// 正解/不正解のラベルを表示する
				$_css_correct_color = "correctflag";
				$_css_correct_quiz = "circle";
				if (!$t['is_correct_quiz']) {
					$_css_correct_color = "incorrectflag";
					$_css_correct_quiz = "cross";
				}
?>
								<div class="row">
									<div class="col-xs-12 text-center"><div class="inner <?=$_css_correct_color?>"><span class="inner <?=$_css_correct_quiz?>"></span></div></div>
								</div>
<?php
			}
?>
								<p><?=$t['description']?></p>
								<?=$t['answer']?>
<?php
			if (!is_null($students_data) && intval($students_data['authority']) === $this->config->item('AUTH_STUDENT')) {
				// 該当講座に受講者権限で登録されている場合のみ表示
?>
								<hr>
								<dl class="dl-horizontal">
<?php
				if ($theme['evaluation_use'] == 1 && $theme['quiz'] == 0) {
?>
									<dt><?=$this->lang->line('theme_task_item_points')?></dt>
									<dd><?=$t['points']?></dd>
<?php
				}

				$_item_name_feedback = $this->lang->line('theme_task_item_feedback');
				if ($is_quiz) {
					// クイズの場合は項目名を「フィードバック」から「解説」に変更する
					$_item_name_feedback = $this->lang->line('theme_task_item_comment');
				}
?>
									<dt><?=$_item_name_feedback?></dt>
									<dd><?=nl2br(htmlspecialchars($t['feedback_message']))?></dd>
								</dl>
<?php
				if ($theme['submission_use'] == 1 and !$is_submit) {
?>
								<p><a href="<?=$this->commonlib->baseUrl()?>task?cid=<?=$theme['course_id']?>&thid=<?=$theme['id'] ?>&taid=<?=$t['id'] ?>"><button class="btn btn-success"><?=$this->lang->line('theme_task_button_answer')?></button></a></p>
<?php
				}
			}
?>
							</div>
						</div>
<?php
		endforeach;
	}
	if (!is_null($task_data) && count($task_data) > 0) {
		// 課題データがある場合は「課題の提出」ボタンの表示判定処理を行う
		if (!is_null($students_data) && intval($students_data['authority']) === $this->config->item('AUTH_STUDENT')
		 && $theme['submission_use'] == 1 && !$is_submit) {
			// 該当講座に受講者権限で登録されている場合のみ
			// かつ
			// テーマ日付け～期限内
			// かつ
			// 未提出の場合のみ表示
			$_submit_disabled = '';
			if (!$is_all_answer) {
				// 全ての課題が解答済みとなっていない場合は
				// 課題の提出ボタンを無効にする。
				$_submit_disabled = ' disabled';
			}
?>
						<div class="pull-right">
							<form name="theme_submit" action="<?=$this->commonlib->baseUrl()?>theme/submit?cid=<?=$theme['course_id']?>&thid=<?=$theme['id']?>&pre=<?=$pre?>" method="post">
								<button type="submit" class="btn btn-primary"<?=$_submit_disabled?>><?=$this->lang->line('theme_task_button_submit')?></button>
								<input type="hidden" name="quiz" value="<?=$theme['quiz']?>">
							</form>
						</div>
						<div id="theme_main_bottom_spacer"><!--  --></div>
<?php
		}
	} else {
		// 課題が無い場合は、「受講終了」ボタンを表示
		// 受講終了ボタンの押下で、提出済みにする。
		if (!is_null($students_data) && intval($students_data['authority']) === $this->config->item('AUTH_STUDENT')
		&& $theme['submission_use'] == 1 && !$is_submit) {
?>
						<div class="pull-right">
							<form name="theme_submit" action="<?=$this->commonlib->baseUrl()?>theme/submit?cid=<?=$theme['course_id']?>&thid=<?=$theme['id']?>&pre=<?=$pre?>&attend=true" method="post">
								<button type="submit" class="btn btn-primary"><?=$this->lang->line('theme_task_button_attended')?></button>
							</form>
						</div>
						<div id="theme_main_bottom_spacer"><!--  --></div>
<?php
		}
	}
?>
					</div>
					<div class="col-sm-3">
						<div class="panel panel-default blur">
							<div class="panel-heading">
								<?=$this->lang->line('theme_contents_diary')?>
							</div>
							<div class="panel-body">
<?php
	if (!is_null($diary_data)) {
		$_derim = '';
		foreach ($diary_data as $d) :
			// 定義に指定された文字数以上の場合は切り取って“...”を表示
			// ※文字数のカウントは改行コードのCRとLFがそれぞれ1文字としてカウントされるので
			// 　文中に、CR+LFの改行がある場合は、改行ごとに2文字カウントされる
			if (mb_strlen($d->post, 'utf-8') > $this->config->item('THEME_DIARY_LENGTH')) {
				$d->post = mb_substr($d->post, 0, $this->config->item('THEME_DIARY_LENGTH'), 'utf-8') ."...";
			}
?>
								<?=$_derim?>
								<div class="row">
									<div class="col-xs-4">
										<p><img class="img-responsive img-circle" src="<?=$d->picture_file?>"></p>
									</div>
									<div class="col-xs-8">
<?php
			$_backcolor = '';
			if (!is_null($d->calendar_color)) {
				$_backcolor = ' style="background-color:'.$d->calendar_color.';"';
			}
?>
										<p><div class="label_cource"<?=$_backcolor?>><?=htmlspecialchars($d->course_name)?></div></p>
										<div><?=htmlspecialchars($d->nickname)?></div>
										<div><?=$d->created_at?></div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
<?php
			if (!is_null($d->image_file)) {
?>
										<img class="pull-right col-xs-6 diarypic" src="<?=$d->image_file?>">
<?php
			}
?>
										<p><?=nl2br(htmlspecialchars($d->post))?></p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<p class="text-right"><a href="<?=$this->commonlib->baseUrl()?>diary?cid=<?=$d->cid?>&pre=theme&thid=<?=$theme['id']?>"><small><?=$this->lang->line('theme_contents_diary_link')?></small></a></p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
<?php
			if ($d->uid == $uid) {
				// 自分の投稿の場合は「Like!」機能を無効にする
?>
										<span class="glyphicon glyphicon-heart-empty diary_heart_empty" aria-hidden="true"></span>&nbsp;<span class="diary_heart_empty"><?=$d->diary_like_cnt?></span>
<?php
			} else {
				// 自分以外の投稿は「Like!」機能を有効にする
				$_like_mode = "1";
				$_diary_like_tooltip_title = $this->lang->line('diary_link_like_add');
				$_diary_like_glyphicon_class = "glyphicon glyphicon-heart-empty diary_heart_empty";
				$_diary_like_cnt_class = "diary_heart_empty";
				if ($d->diary_like == 1) {
					// 既に「Like!」している場合は、「Like!を取り消す」に差し替える
					$_like_mode = "0";
					$_diary_like_tooltip_title = $this->lang->line('diary_link_like_del');
					$_diary_like_glyphicon_class = "glyphicon glyphicon-heart diary_heart";
					$_diary_like_cnt_class = "diary_heart";
				}
?>
										<a id="diary_like_<?=$d->id?>" like-mode="<?=$_like_mode?>" cid="<?=$d->cid?>" class="diary_like_link" href="javascript:void(0);" data-toggle="tooltip" title="<?=$_diary_like_tooltip_title?>" data-placement="right"><span id="diary_like_glyphicon_<?=$d->id?>" class="<?=$_diary_like_glyphicon_class?>" aria-hidden="true"></span>&nbsp;<span id="diary_like_cnt_<?=$d->id?>" class="<?=$_diary_like_cnt_class?>"><?=$d->diary_like_cnt?></span></a>
<?php
			}
?>
									</div>
								</div>
<?php
			$_derim = '<hr>';
		endforeach;
	}
?>
							</div>
						</div>
						<div class="panel panel-default blur">
							<div class="panel-heading">
								<?=$this->lang->line('theme_contents_forum')?>
							</div>
							<div class="panel-body">
<?php
	if (!is_null($forum_data)) {
		$_derim = '';
		foreach ($forum_data as $f) :
			// 定義に指定された文字数以上の場合は切り取って“...”を表示
			// ※文字数のカウントは改行コードのCRとLFがそれぞれ1文字としてカウントされるので
			// 　文中に、CR+LFの改行がある場合は、改行ごとに2文字カウントされる
			if (mb_strlen($f->message, 'utf-8') > $this->config->item('THEME_FORUM_LENGTH')) {
				$f->message = mb_substr($f->message, 0, $this->config->item('THEME_FORUM_LENGTH'), 'utf-8') ."...";
			}
?>
								<?=$_derim?>
								<div class="row">
									<div class="col-xs-4">
										<p><img class="img-responsive img-circle" src="<?=$f->picture_file?>"></p>
									</div>
									<div class="col-xs-8">
										<div><a href="<?=$this->commonlib->baseUrl()?>forum?cid=<?=$f->course_id?>&pre=themetop&thid=<?=$theme['id']?>&fid=<?=$f->forum_id?>"><?=htmlspecialchars($f->forum_name)?></a></div>
										<div><?=htmlspecialchars($f->nickname)?></div>
										<div><?=$f->created_at?></div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
<?php
			if (!is_null($f->image_file)) {
?>
										<img class="pull-left col-xs-6 diarypic" src="<?=$f->image_file?>">
<?php
			}
?>
										<p><?=nl2br(htmlspecialchars($f->message))?></p>
									</div>
								</div>
<?php
			$_derim = '<hr>';
		endforeach;
	}
?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
}
?>
	<?=$script?>
