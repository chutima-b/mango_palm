<?php
/*
 * システム管理（ジャンル）ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('genremanager_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('genremanager_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=$this->lang->line('genremanager_containertitle')?></div>
			<ul class="nav nav-tabs">
			  <li role="presentation"><a href="<?=$this->commonlib->baseUrl()?>usermanager"><?=$this->lang->line('genremanager_tab_usermanager')?></a></li>
			  <li role="presentation" class="active"><a href="#"><?=$this->lang->line('genremanager_tab_genremanager')?></a></li>
			  <li role="presentation"><a href="<?=$this->commonlib->baseUrl()?>settings"><?=$this->lang->line('genremanager_tab_settings')?></a></li>
			</ul>
			<div class="panel-body">
				<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
<?php
if (isset($error_msg)) {
?>
				<div class="alert alert-danger" role="alert"><?=$error_msg?></div>
<?php
}
?>
				<dl class="dl-horizontal">
				  <dt><?=$this->lang->line('genremanager_item_genre_count')?></dt>
				  <dd><?=$genre_cnt?></dd>
				  <dt><?=$this->lang->line('genremanager_item_course_count')?></dt>
				  <dd><?=$course_cnt?></dd>
				</dl>
				<p class="text-right">
<?php
if ($before_page):
?>
					<a id="before_page" href="<?=$before_page?>"></a>
<?php
endif;
if ($next_page):
?>
					<a id="next_page"  href="<?=$next_page?>"></a>
<?php
endif;
?>
					<button class="btn btn-primary" data-toggle="modal" data-target="#new_genre_modal" data-isnew="1" data-gid="" data-genreid="" data-genrename="" data-icon="<?=$icon_default_image?>"><?=$this->lang->line('genremanager_button_new_genre')?> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
				</p>
				<div class="table-responsive touch-scroll">
					<table id="genre_list" class="table table-hover">
						<tr>
							<th><?=$this->lang->line('genremanager_list_column_icon')?></th>
							<th><?=$this->lang->line('genremanager_list_column_genreid')?></th>
							<th><?=$this->lang->line('genremanager_list_column_genrename')?></th>
							<th><?=$this->lang->line('genremanager_list_column_course_count')?></th>
							<th>&nbsp</th>
						</tr>
<?php
foreach ($genre_data as $g) :
?>
						<tr>
							<td><img class="img-responsive" src="<?=$g['icon']?>"></td>
							<td><?=htmlspecialchars($g['genreid'])?></td>
							<td><?=htmlspecialchars($g['genrename'])?></td>
							<td><?=$g['cnt']?></td>
							<td>
								<button class="btn btn-success" data-toggle="modal" data-target="#new_genre_modal" data-isnew="0" data-gid="<?=$g['id']?>" data-genreid="<?=htmlspecialchars($g['genreid'])?>" data-genrename="<?=htmlspecialchars($g['genrename'])?>" data-icon="<?=$g['icon']?>"><?=$this->lang->line('genremanager_button_edit')?></button>
<?php
	if ($g['cnt'] == 0) {
?>
								<button class="btn btn-warning" data-toggle="modal" data-target="#delete_genre_modal" data-gid="<?=$g['id']?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
<?php
	}
?>
							</td>
						</tr>
<?php
endforeach;
?>
					</table>
				</div>
			</div>
		</div>
	</div>

	<!-- 新しいジャンル、編集ダイアログ -->
	<div class="modal fade" id="new_genre_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form class="form-horizontal" id="genre_form" name="genre_form" method="post" enctype="multipart/form-data" action="<?=$this->commonlib->baseUrl()?>genremanager/regist">
						<div class="form-group">
							<label for="genreid" class="col-md-3 control-label"><?=$this->lang->line('genremanager_regist_item_genreid')?></label>
							<div class="col-md-9">
								<input class="form-control" id="genreid" name="genreid" type="text">
							</div>
						</div>
				        <div class="form-group">
							<label for="genre_name" class="col-md-3 control-label"><?=$this->lang->line('genremanager_regist_item_genrename')?></label>
							<div class="col-md-9">
								<input class="form-control" id="genrename" name="genrename" type="text">
							</div>
						</div>
						<div class="form-group">
							<label for="icon" class="col-md-3 control-label"><?=$this->lang->line('genremanager_regist_item_icon')?></label>
							<div class="col-md-9">
								<div class="pull-left">
									<img id="icon_thumb" class="img-responsive" src="">
								</div>
								<button class="btn btn-primary" id="btn_icon"><span id="genre_icon_glyphicon" class="glyphicon glyphicon-camera" aria-hidden="true"></span></button>
								<input type="file" id="icon" name="icon" style="display:none;" accept="image/*" />
							</div>
						</div>
						<div id="delete_icon_area" class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div class="checkbox">
									<label>
										<input type="checkbox" id="delete_icon" name="delete_icon">&nbsp;<?=$this->lang->line('genremanager_regist_item_delete_icon')?>
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-3 col-md-9">
								<input type="hidden" id="gid" name="gid">
								<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('genremanager_button_cancel')?></button>
								<button type="submit" class="btn btn-primary"><?=$this->lang->line('genremanager_button_regist')?></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- 削除確認ダイアログ -->
	<div class="modal fade" id="delete_genre_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-inline" id="genre_delete_form" name="genre_delete_form" method="post" action="<?=$this->commonlib->baseUrl()?>genremanager/delete">
					<div class="modal-body">
							<div class="form-group">
								<label for="confim_title"><?=$this->lang->line('genremanager_delete_confim_title')?></label><br>
								<label for="delete_confim"><?=$this->lang->line('genremanager_delete_confim')?></label>
								<input type="hidden" id="gid" name="gid" value="">
							</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('genremanager_button_cancel')?></button>
						<button type="submit" class="btn btn-primary"><?=$this->lang->line('genremanager_button_delete')?></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?=$script?>
