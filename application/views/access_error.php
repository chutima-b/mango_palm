<?php
/*
 * アクセスエラー画面ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('access_error_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('access_error_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=$this->lang->line('access_error_pagetitle')?></div>
			<div class="panel-body">
				<?=$this->lang->line('access_error_errormsg')?>
			</div>
		</div>
	</div>
