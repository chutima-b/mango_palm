<?php
/*
 * ログイン画面ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('login_lang');
?>

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
			<div class="panel panel-default blur">
				<div class="panel-body">
<!-- MANGOP追加 -->
					<div class="loginlogo">
						<img src="<?=$_assets_url?>images/settings/logo/sample_mango_logo_login.png" class="img-responsive center-block">
					</div>
<!-- MANGOP追加 -->
					<h1 class="sr-only"><?=$this->lang->line('login_pagetitle')?></h1>
					<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
					<form id="login_form" class="form-horizontal" name="login" method="post" action="<?=$this->commonlib->baseUrl()?>login/login">
						<div class="form-group">
							<label for="login_userid" class="col-md-4 control-label"><?=$this->lang->line('login_userid')?></label>
							<div class="col-xs-12 col-md-8">
								<input class="form-control" id="login_userid" name="login_userid" type="text" value="<?=$mangopid?>" required>
							</div>
						</div>
						<div class="form-group">
							<label for="login_password" class="col-md-4 control-label"><?=$this->lang->line('login_passwd')?></label>
							<div class="col-xs-12 col-md-8">
								<input class="form-control" id="login_password" name="login_password" type="password" required>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-offset-4 col-md-8">
								<div class="checkbox">
									<input id="login_continue" name="login_continue" type="checkbox" <?=set_checkbox('login_continue', '1', $login_continue)?>>
									<label for="login_continue"><?=$this->lang->line('login_continue')?></label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-offset-4 col-md-8">
								<button class="btn btn-primary" type="submit" id="login_btn_login"><?=$this->lang->line('login_btn_login')?></button>
							</div>
						</div>
<?php
if ($this->config->item('USE_GUEST') == 1) {
?>
						<div class="form-group">
							<div class="col-xs-12 col-md-offset-4 col-md-8">
								<button id="login_btn_guest" class="btn btn-info" type="button" id="login_btn_login"><?=$this->lang->line('login_btn_login_guest')?></button>
							</div>
						</div>
<?php
}
?>
					</form>
<!-- MANGOP追加 -->
					<div class="loginnote">
						<p>本サービスは、Mango Owner's Club メンバー様のみご利用いただけます。</p>
					</div>
<!-- MANGOP追加 -->
				</div><!-- /.panel-body -->
			</div><!-- /.panel panel-default -->
		</div><!-- /.col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6 col-lg-offset-4 col-lg-4 -->
	</div><!-- /.row -->
</div><!-- /.container -->
<?=$script?>
