<?php
/*
 * ホーム画面（受講者）ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('students_home_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('students_home_pagetitle')?></h1>
	<div class="container">
		<?php
		$_hidden_calendar = '';
		$_hidden_intro = '';
		$_hidden_cal_intro_switch = '';
		$_switch_cal_intro_checked = '';
		$_hidden_course_list = '';
		// カレンダー・イントロ表示設定
		if ($cal_intro_display_mode == 0) {
			// カレンダー固定
			$_hidden_intro = ' hidden';
			$_hidden_cal_intro_switch = ' hidden';
		} else if ($cal_intro_display_mode == 1) {
			// イントロ固定
			$_hidden_calendar = ' hidden';
			$_hidden_cal_intro_switch = ' hidden';
		} else {
			// カレンダー/イントロダクション切替
			if ($cal_intro_switchmode_default == 0) {
				// カレンダー/イントロ切替時のデフォルト値がカレンダーの場合
				$_hidden_intro = ' hidden';
				$_switch_cal_intro_checked = ' checked';
			} else {
				// カレンダー/イントロ切替時のデフォルト値がイントロの場合
				$_hidden_calendar = ' hidden';
			}
		}
		// 受講講座表示・非表示設定
		if ($cal_intro_display_course_list == 3) {
			// 「カレンダー/イントロの両方表示しない」
			$_hidden_course_list = ' hidden';
		} else if ($cal_intro_display_course_list == 0) {
			// 「カレンダーの時表示する」
			// （カレンダーが表示されてない時非表示）
			if (strlen($_hidden_calendar) > 0) {
				$_hidden_course_list = ' hidden';
			}
		} else if ($cal_intro_display_course_list == 1) {
			// 「イントロの時表示する」
			// （イントロが表示されてない時非表示）
			if (strlen($_hidden_intro) > 0) {
				$_hidden_course_list = ' hidden';
			}
		}
		?>
<!--
		<div class="panel panel-default blur">
-->
<!--
			<div class="panel-heading">
				<?=$this->lang->line('students_home_pagetitle')?>
			</div>
-->
<!--
			<div class="panel-body">
-->
				<div class="row">
					<div class="col-sm-8">
						<div class="panel panel-default blur">
							<div class="panel-body">
								<p>ここに説明文。ここに説明文。ここに説明文。ここに説明文。</p>
								<div id="course_list" class="list-group"<?=$_hidden_course_list?>>
									<p class="list-group-item list-group-item-header"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>&nbsp;<?=$this->lang->line('students_home_coursetitle')?></p>
<?php
if (!is_null($course_data)) {
	foreach($course_data as $c) :
?>
							<a href="<?=$this->commonlib->baseUrl()?>course?cid=<?=$c['id']?>" class="list-group-item"><?php if (!is_null($c['icon'])) : ?><img src="<?=$c['icon']?>">&nbsp;<?php endif; ?><?=htmlspecialchars($c['course_name'])?></a>
<?php
	endforeach;
}
?>
								</div>
							</div>
						</div>
						<div class="panel panel-default blur">
							<div class="panel-heading">
								<?=$this->lang->line('students_home_noticetitle')?>
							</div>
							<div class="panel-body">
<?php
if ($notice_data !== null and !empty($notice_data)) {
	$_notice_cnt = 0;
	foreach($notice_data as $n) :
		if ($_notice_cnt == 0) {
?>
								<dl class="dl-horizontal">
<?php
		}
?>
									<dt><?=$n['created_at']?></dt>
									<dd><a href="<?=$this->commonlib->baseUrl()?>notice?nid=<?=$n['id']?>&pre=students"><?=htmlspecialchars($n['title'])?><?php if (!is_null($n['is_new'])) :?>&nbsp;<span class="new">NEW</span><?php endif;?></a></dd>
<?php
		if ($_notice_cnt == (count($notice_data) - 1)) {
?>
								</dl>
<?php
		}
		$_notice_cnt++;
	endforeach;
} else {
?>
								<div class="alert alert-info" role="alert">
									<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>&nbsp<?=$this->lang->line('students_home_notice_none')?>
								</div>
<?php
}
?>
							</div>
						</div>
						<p>
							<div id="calendar" class="tgl_cal_intro"<?=$_hidden_calendar?>></div>
						</p>
						<div id="intro" class="panel panel-default blur tgl_cal_intro"<?=$_hidden_intro?>>
							<div class="panel-body">
								<?=$introduction_data?>
							</div>
						</div>
<?php
$_authority_info = $this->session->userdata('authority');
if ($this->config->item('USE_SELF_REGIST_COURSE') === 1
 && intval($_authority_info[0]->authority) !== $this->config->item('AUTH_GUEST')
 && intval($_authority_info[0]->authority) !== $this->config->item('AUTH_SYS_MANAGER')) {
	// 受講講座自己登録機能が使用するに設定されていて、
	// ゲストおよびシステム管理者以外の場合は
	// 「講座申込」ボタンを表示する
?>						<div class="pull-left selfregistcourse_area">
							<a class="btn btn-warning" href="<?=$this->commonlib->baseUrl()?>selfregistcourse"><?=$this->lang->line('students_home_btn_self_regist_course')?></a>
						</div>
<?php
}
?>						<p id="switch_cal_intro_area" class="text-right"<?=$_hidden_cal_intro_switch?>>
							<input id="switch_cal_intro" name="switch_cal_intro" type="checkbox"<?=$_switch_cal_intro_checked?>>
						</p>
					</div>
					<div class="col-sm-4">
						<div class="panel panel-default blur">
							<div class="panel-body">
								<a href="#" target="_blank"><img src="images/banner/banner1.png" class="img-responsive"></a>
							</div>
						</div>
						<div class="panel panel-default blur">
							<div class="panel-body">
								<a href="#" target="_blank"><img src="images/banner/banner2.png" class="img-responsive"></a>
							</div>
						</div>
					</div>
				</div>
<!-- .panel-body
			</div>
-->
<!-- .panel .panel-default .blur
		</div>
-->
	</div>
	<?=$script?>
