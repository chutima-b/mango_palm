<?php
/*
 * システム管理（設定）ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2017, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('settings_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('settings_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=$this->lang->line('settings_headertitle')?></div>
			<ul class="nav nav-tabs">
				<li role="presentation"><a href="<?=$this->commonlib->baseUrl()?>usermanager"><?=$this->lang->line('settings_link_usermanager')?></a></li>
				<li role="presentation"><a href="<?=$this->commonlib->baseUrl()?>genremanager"><?=$this->lang->line('settings_link_genremanager')?></a></li>
				<li role="presentation" class="active"><a href="javascript:void(0);"><?=$this->lang->line('settings_link_settings')?></a></li>
			</ul>
			<div class="panel-body">
				<aside id="error"></aside>
				<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
<?php
if (isset($complete_msg)) {
	// 処理完了メッセージがある場合は表示
?>
				<div class="alert alert-success" role="alert"><?=$complete_msg?></div>
<?php
}
if (isset($error_msg)) {
?>
				<div class="alert alert-danger" role="alert"><?=$error_msg?></div>
<?php
}
?>
				<form class="form-horizontal" id="setting_form" name="setting_form" method="post" enctype="multipart/form-data" action="<?=$this->commonlib->baseUrl()?>settings/regist">
				    <div class="form-group">
						<label class="col-sm-2 control-label"><?=$this->lang->line('settings_item_title')?></label>
				        <div class="col-sm-4">
							<input type="text" class="form-control" id="title" name="title" value="<?=set_value('title', $settings->title)?>">
				        </div>
					</div>
				    <div class="form-group">
				        <div class="col-sm-offset-2 col-sm-4">
							<div class="checkbox">
								<input type="checkbox" id="delete_logo" name="delete_logo">
								<label for="delete_logo"><?=$this->lang->line('settings_checkbox_delete_logo')?></label>
							</div>
						</div>
					</div>
				    <div class="form-group">
				        <div class="col-sm-offset-2 col-sm-4">
							<img id="thumb_logo" class="img-responsive" src="<?=set_value('logo_img', $settings->logo_img)?>">
							<input type="file" id="logo_img" name="logo_img" style="display:none;" accept="image/*">
						</div>
				    </div>
				    <div class="form-group">
				        <div class="col-sm-offset-2 col-sm-4">
							<button id="btn_logo_imgupload" class="btn btn-primary" type="button">
								<span class="glyphicon glyphicon-picture" aria-hidden="true"></span>
								<?=$this->lang->line('settings_btn_logo_img')?>
							</button>
						</div>
			        </div>
					<hr>
				    <div class="form-group">
						<label class="col-sm-2 control-label"><?=$this->lang->line('settings_item_introduction')?></label>
				        <div class="col-sm-10">
							<textarea class="form-control" rows="5" id="introduction" name="introduction"><?=set_value('introduction', $settings->introduction)?></textarea>
				        </div>
					</div>
					<hr>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?=$this->lang->line('settings_item_background')?></label>
						<div class="col-sm-10">
							<select id="background_color" name="background_color" class="form-control">
<?php
foreach ($colorpicker_colors as $color_code=>$color_name) {
	$_selected = false;
	if (strcasecmp($color_code, $settings->background_color) === 0) {
		// POSTデータが無い場合はデータベースのデータを元に選択状態にする
		$_selected = true;
	}
?>
								<option value="<?=$color_code?>" <?=set_select('background_color', $color_code, $_selected)?>><?=$color_name?></option>
<?php
}
?>
							</select>
						</div>
					</div>
				    <div class="form-group">
						<div class="col-sm-offset-2 col-sm-4">
							<div class="checkbox">
<?php
$_back_img_use = false;
if ($settings->back_img_use == 1) {
	// POSTデータが無い場合はデータベースのデータを元にチェック状態にする
	$_back_img_use = true;
}
?>
								<input type="checkbox" id="use_background_img" name="use_background_img" value="1" <?=set_checkbox('use_background_img', '1', $_back_img_use)?>>
								<label for="use_background_img">
									<?=$this->lang->line('settings_checkbox_use_background_img')?>
								</label>
							</div>
						</div>
					</div>
				    <div class="form-group">
						<div class="col-sm-offset-2 col-sm-4">
							<img class="img-responsive" id="thumb_background" src="<?=set_value('background_img', $settings->background_img)?>">
							<span id="msg_validate_background_img"></span>
							<input type="file" id="background_img" name="background_img" style="display:none;" accept="image/*">
							<input type="hidden" id="validate_background_img" name="validate_background_img" value="<?=$settings->validate_background_img?>">
						</div>
					</div>
					<div class="form-group">
				        <div class="col-sm-offset-2 col-sm-4">
							<div class="pull-left">
								<button id="btn_background_img" class="btn btn-primary" type="button">
									<span class="glyphicon glyphicon-picture" aria-hidden="true"></span>
									<?=$this->lang->line('settings_btn_background_img')?>
								</button>
							</div>
						</div>
					</div>
					<hr>
					<p class="text-right">
						<button id="btn_regist" class="btn btn-primary" type="button"><?=$this->lang->line('settings_btn_regist')?></button>
					</p>
					<input type="hidden" name="backkind" value="<?=$backkind?>">
				</form>
<?=$script?>
