<?php
/*
 * 参加者登録画面ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('students_regist_lang');
?>
	<h1 class="sr-only"><?=$page_title?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=$page_title?></div>
			<div class="panel-body">
				<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
				<form class="form-horizontal" id="students_search_form" name="students_search_form" method="post" action="<?=$this->commonlib->baseUrl()?>studentsregist/search">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="userid"><?=$this->lang->line('students_regist_saerch_userid')?></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="userid" name="userid" value="<?=htmlspecialchars($userid)?>">
						</div>
					</div>
<?php
// 姓、名の表示を定義に従い、生成する
$_usermanage_surname_value = htmlspecialchars($family);
$_search_form_surname = <<<EOT
					<div class="form-group">
						<label class="col-sm-2 control-label" for="family">{$this->lang->line('students_regist_search_familyname')}</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="family" name="family" value="{$_usermanage_surname_value}">
						</div>
					</div>
EOT;

$_usermanage_firstname_value = htmlspecialchars($first);
$_search_form_firstname = <<<EOT
					<div class="form-group">
						<label class="col-sm-2 control-label" for="first">{$this->lang->line('students_regist_search_firstname')}</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="first" name="first" value="{$_usermanage_firstname_value}">
						</div>
					</div>
EOT;

$_search_form_item = $this->commonlib->createFullName($_search_form_firstname, $_search_form_surname);
?>					<?=$_search_form_item?>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-success"><?=$this->lang->line('students_regist_button_search')?></button>
						</div>
					</div>
					<input type="hidden" name="cid" value="<?=$cid?>">
					<input type="hidden" name="target" value="<?=$target?>">
					<input type="hidden" name="page" value="1">
				</form>
				<hr>
				<div id="page_link">
<?php
// 無限スクロール用“前ページ”リンク
if ($page_data['before_page']):
?>
					<a id="before_page" href="<?=$page_data['before_page']?>"></a>
<?php
endif;

// 無限スクロール用“次ページ”リンク
if ($page_data['next_page']):
?>
					<a id="next_page" href="<?=$page_data['next_page']?>"></a>
<?php
endif;
?>
				</div>
				<div class="table-responsive">
					<table id="studentsregist_table_userlist" class="table table-hover">
						<thead>
							<tr>
								<th>　</th>
								<th><?=$this->lang->line('students_regist_list_userid')?></th>
<?php
// 姓、名の表示を定義に従い、生成する
$_name_item_column = $this->commonlib->createFullName(
		"<th>".$this->lang->line('students_regist_list_firstname')."</th>",
		"<th>".$this->lang->line('students_regist_list_familyname')."</th>");
?>								<?=$_name_item_column?>
								<th><?=$this->lang->line('students_regist_list_nickname')?></th>
								<th>　</th>
							</tr>
						</thead>
						<tbody>
<?php
if (0 < $page_data['total']) {
	foreach ($candidate_data AS $cd) :
?>
							<tr>
								<td><img class="studentsregist_img_userimg" src="<?=$cd['picture_file']?>"></td>
								<td><?=htmlspecialchars($cd['userid'])?></td>
<?php
		// 姓、名の表示を定義に従い、生成する
		$_name_column = $this->commonlib->createFullName(
				"<td>".htmlspecialchars($cd['firstname'])."</td>",
				"<td>".htmlspecialchars($cd['surname'])."</td>");
?>
								<?=$_name_column?>
								<td><?=htmlspecialchars($cd['nickname'])?></td>
								<td>
									<form id="studentsregist_form_regist_<?=$cd['uid']?>" class="form-horizontal" method="post" action="<?=$this->commonlib->baseUrl()?>studentsregist/regist">
									<div class="pull-right">
										<button id="studentsregist_btn_regist_<?=$cd['uid']?>" type="submit" class="btn btn-primary studentsregist_btn_regist"><?=$this->lang->line('students_regist_button_regist')?></button>
									</div>
									<input type="hidden" name="cid" value="<?=$cid ?>">
									<input type="hidden" name="target" value="<?=$target ?>">
									<input type="hidden" name="userid" value="<?=htmlspecialchars($userid)?>">
									<input type="hidden" name="family" value="<?=htmlspecialchars($family)?>">
									<input type="hidden" name="first" value="<?=htmlspecialchars($first)?>">
									<input type="hidden" name="page" value="<?=$page_data['page']?>">
									<input type="hidden" name="uid" value="<?=$cd['uid']?>">
									</form>
								</td>
							</tr>
<?php
	endforeach;
} else {
?>
							<tr>
								<td colspan="6">
									&nbsp;&nbsp;&nbsp;&nbsp;<?=$this->lang->line('students_regist_error_msg_none_students')?>
								</td>
							</tr>
<?php
}
?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?=$script?>
