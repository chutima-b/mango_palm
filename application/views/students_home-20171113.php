<?php
/*
 * ホーム画面（受講者）ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('students_home_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('students_home_pagetitle')?></h1>
	<div class="container-fluid">



<!--
		<div class="panel panel-default blur">
			<div class="panel-heading">
				<?=$this->lang->line('students_home_pagetitle')?>
			</div>
			<div class="panel-body">
-->

<?php
$_hidden_calendar = '';
$_hidden_intro = '';
$_hidden_cal_intro_switch = '';
$_switch_cal_intro_checked = '';
$_hidden_course_list = '';
// カレンダー・イントロ表示設定
if ($cal_intro_display_mode == 0) {
	// カレンダー固定
	$_hidden_intro = ' hidden';
	$_hidden_cal_intro_switch = ' hidden';
} else if ($cal_intro_display_mode == 1) {
	// イントロ固定
	$_hidden_calendar = ' hidden';
	$_hidden_cal_intro_switch = ' hidden';
} else {
	// カレンダー/イントロダクション切替
	if ($cal_intro_switchmode_default == 0) {
		// カレンダー/イントロ切替時のデフォルト値がカレンダーの場合
		$_hidden_intro = ' hidden';
		$_switch_cal_intro_checked = ' checked';
	} else {
		// カレンダー/イントロ切替時のデフォルト値がイントロの場合
		$_hidden_calendar = ' hidden';
	}
}
// 受講講座表示・非表示設定
if ($cal_intro_display_course_list == 3) {
	// 「カレンダー/イントロの両方表示しない」
	$_hidden_course_list = ' hidden';
} else if ($cal_intro_display_course_list == 0) {
	// 「カレンダーの時表示する」
	// （カレンダーが表示されてない時非表示）
	if (strlen($_hidden_calendar) > 0) {
		$_hidden_course_list = ' hidden';
	}
} else if ($cal_intro_display_course_list == 1) {
	// 「イントロの時表示する」
	// （イントロが表示されてない時非表示）
	if (strlen($_hidden_intro) > 0) {
		$_hidden_course_list = ' hidden';
	}
}
?>
				<div class="row">
					<div id="intro" class="blur tgl_cal_intro"<?=$_hidden_intro?>>
						<?=$introduction_data?>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-4">
<!--
						<div class="panel panel-default blur">
							<div class="panel-heading">
								<?=$this->lang->line('students_home_noticetitle')?>
							</div>
							<div class="panel-body">
-->
						<?php
if ($notice_data !== null and !empty($notice_data)) {
	$_notice_cnt = 0;
	foreach($notice_data as $n) :
		if ($_notice_cnt == 0) {
?>
								<dl class="dl-horizontal notice">
<?php
		}
?>
									<dt><?=$n['created_at']?></dt>
									<dd><span class="arrow1"></span><a href="<?=$this->commonlib->baseUrl()?>notice?nid=<?=$n['id']?>&pre=students"><?=htmlspecialchars($n['title'])?><?php if (!is_null($n['is_new'])) :?>&nbsp;<span class="new">NEW</span><?php endif;?></a></dd>
<?php
		if ($_notice_cnt == (count($notice_data) - 1)) {
?>
								</dl>
<?php
		}
		$_notice_cnt++;
	endforeach;
} else {
?>
								<div class="alert alert-info" role="alert">
									<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>&nbsp<?=$this->lang->line('students_home_notice_none')?>
								</div>
<?php
}
?>
<!--
							</div>
						</div>
-->
							<p>
							<div id="calendar" class="tgl_cal_intro"<?=$_hidden_calendar?>></div>
						</p>
<?php
$_authority_info = $this->session->userdata('authority');
if ($this->config->item('USE_SELF_REGIST_COURSE') === 1
 && intval($_authority_info[0]->authority) !== $this->config->item('AUTH_GUEST')
 && intval($_authority_info[0]->authority) !== $this->config->item('AUTH_SYS_MANAGER')) {
	// 受講講座自己登録機能が使用するに設定されていて、
	// ゲストおよびシステム管理者以外の場合は
	// 「講座申込」ボタンを表示する
?>						<div class="pull-left selfregistcourse_area">
							<a class="btn btn-warning" href="<?=$this->commonlib->baseUrl()?>selfregistcourse"><?=$this->lang->line('students_home_btn_self_regist_course')?></a>
						</div>
<?php
}
?>						<p id="switch_cal_intro_area" class="text-right"<?=$_hidden_cal_intro_switch?>>
							<input id="switch_cal_intro" name="switch_cal_intro" type="checkbox"<?=$_switch_cal_intro_checked?>>
						</p>
						<div id="course_list" class="list-group course_dmy_box"<?=$_hidden_course_list?>>
							<p class="list-group-item list-group-item-header dmy_box"><?=$this->lang->line('students_home_coursetitle')?></p>
<?php
if (!is_null($course_data)) {
	foreach($course_data as $c) :
?>
							<a href="<?=$this->commonlib->baseUrl()?>course?cid=<?=$c['id']?>" class="list-group-item box dmy_box"><?php if (!is_null($c['icon'])) : ?><img src="<?=$c['icon']?>">&nbsp;<?php endif; ?><?=htmlspecialchars($c['course_name'])?></a>
<?php
	endforeach;
}
?>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="panel panel-default diary_box">
							<div class="panel-heading">
								<?=$this->lang->line('students_home_diary')?>
							</div>
							<div class="panel-body">
<?php
if (!is_null($diary_data)) {
	$_derim = '';
	foreach ($diary_data as $d) :
		// 定義に指定された文字数以上の場合は切り取って“...”を表示
		// ※文字数のカウントは改行コードのCRとLFがそれぞれ1文字としてカウントされるので
		// 　文中に、CR+LFの改行がある場合は、改行ごとに2文字カウントされる
		if (mb_strlen($d->post, 'utf-8') > $this->config->item('STUDENTSHOME_DIARY_LENGTH')) {
			$d->post = mb_substr($d->post, 0, $this->config->item('STUDENTSHOME_DIARY_LENGTH'), 'utf-8') ."...";
		}
?>
								<?=$_derim?>
								<div class="row">
									<div class="col-xs-3">
										<p><img class="img-responsive img-circle" src="<?=$d->picture_file?>"></p>
									</div>
									<div class="col-xs-9">
<?php
		$_backcolor = '';
		if (!is_null($d->calendar_color)) {
			$_backcolor = ' style="background-color:'.$d->calendar_color.';"';
		}
?>
										<p><div class="label_cource"<?=$_backcolor?>><?=htmlspecialchars($d->course_name)?></div></p>
										<div><?=htmlspecialchars($d->nickname)?></div>
										<div><?=$d->created_at?></div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
<?php
		if (!is_null($d->image_file)) {
?>
										<img class="pull-right col-xs-6 diarypic" src="<?=$d->image_file?>">
<?php
		}
?>
										<p><?=nl2br(htmlspecialchars($d->post))?></p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<p class="text-right"><a href="<?=$this->commonlib->baseUrl()?>diary?cid=<?=$d->cid?>&pre=home"><small><?=$this->lang->line('students_home_diary_link')?></small></a></p>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
<?php
		if ($d->uid == $uid) {
			// 自分の投稿の場合は「Like!」機能を無効にする
?>
										<span class="glyphicon glyphicon-heart-empty diary_heart_empty" aria-hidden="true"></span>&nbsp;<span class="diary_heart_empty"><?=$d->diary_like_cnt?></span>
<?php
		} else {
			// 自分以外の投稿は「Like!」機能を有効にする
			$_like_mode = "1";
			$_diary_like_tooltip_title = $this->lang->line('diary_link_like_add');
			$_diary_like_glyphicon_class = "glyphicon glyphicon-heart-empty diary_heart_empty";
			$_diary_like_cnt_class = "diary_heart_empty";
			if ($d->diary_like == 1) {
				// 既に「Like!」している場合は、「Like!を取り消す」に差し替える
				$_like_mode = "0";
				$_diary_like_tooltip_title = $this->lang->line('diary_link_like_del');
				$_diary_like_glyphicon_class = "glyphicon glyphicon-heart diary_heart";
				$_diary_like_cnt_class = "diary_heart";
			}
?>
										<a id="diary_like_<?=$d->id?>" like-mode="<?=$_like_mode?>" cid="<?=$d->cid?>" class="diary_like_link" href="javascript:void(0);" data-toggle="tooltip" title="<?=$_diary_like_tooltip_title?>" data-placement="right"><span id="diary_like_glyphicon_<?=$d->id?>" class="<?=$_diary_like_glyphicon_class?>" aria-hidden="true"></span>&nbsp;<span id="diary_like_cnt_<?=$d->id?>" class="<?=$_diary_like_cnt_class?>"><?=$d->diary_like_cnt?></span></a>
<?php
		}
?>
									</div>
								</div>
<?php
		$_derim = '<hr>';
	endforeach;
}
?>
							</div>
						</div>
					</div>
					
					
					
					
					
					<div class="col-sm-4">
						<div class="panel panel-default diary_box">
							<div class="panel-heading">
								<?=$this->lang->line('students_home_forum')?>
							</div>
							<div class="panel-body">
<?php
if (!is_null($forum_data)) {
	$_derim = '';
	foreach ($forum_data as $f) :
		// 定義に指定された文字数以上の場合は切り取って“...”を表示
		// ※文字数のカウントは改行コードのCRとLFがそれぞれ1文字としてカウントされるので
		// 　文中に、CR+LFの改行がある場合は、改行ごとに2文字カウントされる
		if (mb_strlen($f->message, 'utf-8') > $this->config->item('STUDENTSHOME_FORUM_LENGTH')) {
			$f->message = mb_substr($f->message, 0, $this->config->item('STUDENTSHOME_FORUM_LENGTH'), 'utf-8') ."...";
		}
?>
								<?=$_derim?>
								<div class="row">
									<div class="col-xs-3">
										<p><img class="img-responsive img-circle" src="<?=$f->picture_file?>"></p>
									</div>
									<div class="col-xs-9">
										<div><a href="<?=$this->commonlib->baseUrl()?>forum?cid=<?=$f->course_id?>&pre=home&fid=<?=$f->forum_id?>"><?=htmlspecialchars($f->forum_name)?></a></div>
										<div><?=htmlspecialchars($f->nickname)?></div>
										<div><?=$f->created_at?></div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
<?php
		if (!is_null($f->image_file)) {
?>
										<img class="pull-left col-xs-6 diarypic" src="<?=$f->image_file?>">
<?php
		}
?>
										<p><?=nl2br(htmlspecialchars($f->message))?></p>
									</div>
								</div>
<?php
		$_derim = '<hr>';
	endforeach;
}
?>
							</div>
						</div>
					</div>
					
					
					
					
				</div>
<!--
			</div>
		</div>
-->
	</div>
	<?=$script?>
