<?php
/*
 * フィードバック画面ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('feedback_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('feedback_pagetitle')?></h1>
	<div class="container">
		<!-- フィードバック画面 -->
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=$theme['name']?></div>
			<div class="panel-body">
				<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
<?php
if (!is_null($complete_msg)) {
	// 処理完了メッセージがある場合は表示
?>
				<div class="alert alert-success" role="alert"><?=$complete_msg?></div>
<?php
}
?>
				<dl class="dl-horizontal">
					<dt><?=$this->lang->line('feedback_item_userid')?></dt>
					<dd><?=$user->userid?></dd>
					<dt><?=$this->lang->line('feedback_item_nickname')?></dt>
					<dd><?=$user->nickname?></dd>
					<dd><img class="img-responsive" src="<?=$user->picture_file?>"></dd>
				</dl>
				<div class="panel panel-default blur">
					<div class="panel-heading"><?=$this->lang->line('feedback_material_header')?></div>
					<div class="panel-body">
<?php
// 教材リンク表示
if (!is_null($material_list)) {
	foreach ($material_list as $_material_data) {
		// 教材タイプ別にアイコンを変更
		$_mime_icon = '';
		if ($_material_data->material_type == $this->config->item('MATERIAL_TYPE_EXTERNAL')) {
			// 外部リンク
			$_mime_icon = '<span class="label label-primary"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></span>';
		} elseif ($_material_data->material_type == $this->config->item('MATERIAL_TYPE_PICTURE')) {
			// 写真
			$_mime_icon = '<span class="label label-primary"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></span>';
		} elseif ($_material_data->material_type == $this->config->item('MATERIAL_TYPE_MOVIE')) {
			// 動画
			$_mime_icon = '<span class="label label-primary"><span class="glyphicon glyphicon-film" aria-hidden="true"></span></span>';
		} elseif ($_material_data->material_type == $this->config->item('MATERIAL_TYPE_PDF')) {
			// PDF
			$_mime_icon = '<span class="label label-primary"><span class="glyphicon glyphicon-file" aria-hidden="true"></span></span>';
		}
		$_material_description = htmlspecialchars($_material_data->material_description);
		// リンク部分生成
		$_link_contents = "<a href=\"{$_material_data->link_path}\" target=\"_blank\">{$_mime_icon}&nbsp;{$_material_description}</a>";
?>
						<div class="row">
<?php
		if ($_material_data->material_type == $this->config->item('MATERIAL_TYPE_MOVIE')) {
			// 教材が動画の場合
			// 動画再生時間に応じてプログレスバーのスタイル変更
			$_progress_class = 'progress-bar-danger';
			$_percentage = '0';
			if (!is_null($_material_data->percentage)) {
				$_percentage = $_material_data->percentage;
				if ($_material_data->percentage >= $this->config->item('MATERIAL_MOVIE_PLAYTIME_SUCCESS')) {
					$_progress_class = 'progress-bar-success';
				} else if ($_material_data->percentage >= $this->config->item('MATERIAL_MOVIE_PLAYTIME_WARNING')) {
					$_progress_class = 'progress-bar-warning';
				}
			}
?>
							<div class="col-md-3">
								<p><?=$_link_contents?></p>
							</div>
							<div class="col-md-9">
								<div class="progress">
									<div class="progress-bar <?=$_progress_class?>" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:<?=$_percentage?>%;">
										<?=$_percentage?>%
									</div>
								</div>
							</div>
<?php
		} else {
			// 教材が動画以外の場合
?>
							<div class="col-md-12">
								<p><?=$_link_contents?></p>
							</div>
<?php
		}
?>
						</div>
<?php
	}
}
?>
					</div>
				</div>
				<form class="form-horizontal" id="feedback_form" name="feedback_form" method="post" action="<?=$this->commonlib->baseUrl()?>feedback/update?tmpage=<?=$tmpage?>">
<?php
if (!is_null($task_list)) {
	$_is_feedback = false;
	$_feedback_disabled = "";
	$_feedback_readonly = "";
	if (!is_null($task_feedback->feedback_at)) {
		$_is_feedback = true;
		$_feedback_disabled = " disabled";
		$_feedback_readonly = " readonly";
	}
	foreach ($task_list as $task_data) :
?>
					<div class="panel panel-default blur">
						<div class="panel-heading"><?=htmlspecialchars($task_data->task_name)?></div>
						<div class="panel-body">
<?php
		if ($this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION') == $task_data->task_type
		 && isset($task_data->result_data['is_correct_quiz'])) {
			// クイズの正答情報がある場合
			// 正解/不正解のラベルを表示する
			$_css_correct_color = "correctflag";
			$_css_correct_quiz = "circle";
			if (!$task_data->result_data['is_correct_quiz']) {
				$_css_correct_color = "incorrectflag";
				$_css_correct_quiz = "cross";
			}
?>
								<div class="row">
									<div class="col-xs-12 text-center"><div class="inner <?=$_css_correct_color?>"><span class="inner <?=$_css_correct_quiz?>"></span></div></div>
								</div>
<?php
		}
?>
							<p class="lead"><?=$task_data->task_description?></p>
<?php
		if ($this->config->item('TASK_TYPE_SINGLE_SELECTION') == $task_data->task_type
		 || $this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION') == $task_data->task_type
		 || $this->config->item('TASK_TYPE_SURVEY_SINGLE_SELECTION') == $task_data->task_type
		) {
?>
							<hr>
<?php
				$itemArray = $task_data->result_data['radio_array'];
				foreach ($itemArray as $item) :
?>
							<div class="radio disabled">
								<label>
<?php
					if ($item['select'] == 1) {
?>
									<input type="radio" checked disabled>
<?php
					} else {
?>
									<input type="radio" disabled>
<?php
					}

					if ($this->config->item('TASK_TYPE_QUIZ_SINGLE_SELECTION') == $task_data->task_type
					 && isset($item['correct_item'])
					 && $item['correct_item'] === true) {
						// 正答項目の場合は項目名に色を付ける
						$item['item'] = '<span class="correct">'.$item['item'].'</span>';
					}
?>
									<?=$item['item']?>
								</label>
							</div>
<?php
				endforeach;
?>
							<hr>
<?php
		} elseif ($this->config->item('TASK_TYPE_MULTIPLE_CHOICE') == $task_data->task_type
				|| $this->config->item('TASK_TYPE_SURVEY_MULTIPLE_CHOICE') == $task_data->task_type) {
?>
							<hr>
<?php
			$itemArray = $task_data->result_data['check_array'];
			foreach ($itemArray as $item) :
?>
							<div class="checkbox disabled">
								<label>
<?php
				if ($item['select'] == 1) {
?>
									<input type="checkbox" checked disabled>
<?php
				} else {
?>
									<input type="checkbox" disabled>
<?php
				}
?>
									<?=$item['item']?>
								</label>
							</div>
<?php
			endforeach;
?>
							<hr>
<?php
		} elseif ($this->config->item('TASK_TYPE_TEXT') == $task_data->task_type) {
			if (!is_null($task_data->result_data['result_text'])) {
?>
							<hr>
							<p><?=nl2br($task_data->result_data['result_text'])?></p>
<?php
			}
?>
							<hr>
<?php
		} elseif ($this->config->item('TASK_TYPE_FILE') == $task_data->task_type) {
			if (!is_null($task_data->result_data['result_file'])) {
?>
							<hr>
							<img class="img-responsive" src="<?=$task_data->result_data['result_file']?>">
							<br>
							<p class="pull-right">
								<a class="btn btn-primary" href="<?=$task_data->result_data['result_file']?>" download="<?=$task_data->result_data['result_file_name']?>"><?=$this->lang->line('feedback_button_download')?></a>
							</p>
							<br>
<?php
			}
?>
							<hr>
<?php
		}
?>
<?php
		if ($theme['evaluation_use'] == $this->config->item('EVALUATION_USE_FLAG_ON') && $theme['quiz'] == 0) {
?>
							<div class="form-group">
								<label for="points" class="col-xs-3 control-label"><?=$this->lang->line('feedback_input_points')?></label>
								<div class="col-xs-3">
									<div class="input-group">
										<input class="form-control" id="points_<?=$task_data->id?>" name="points_<?=$task_data->id?>" type="text" value="<?=set_value('points_'.$task_data->id, $task_data->result_data['feedback_points'])?>"<?=$_feedback_disabled?>>
										<span class="input-group-addon">/<?=$task_data->evaluation_points?></span>
										<input type="hidden" id="evaluation_points_<?=$task_data->id?>" name="evaluation_points_<?=$task_data->id?>" value="<?=$task_data->evaluation_points?>">
									</div>
								</div>
					        </div>
<?php
		}
?>
							<div class="form-group">
								<label for="feedback" class="col-xs-3 control-label"><?=$this->lang->line('feedback_input_feedback')?></label>
								<div class="col-xs-9">
									<textarea id="feedback_<?=$task_data->id?>" name="feedback_<?=$task_data->id?>" class="form-control" rows="3"<?=$_feedback_readonly?>><?=set_value('feedback_'.$task_data->id, $task_data->result_data['feedback_message'])?></textarea>
								</div>
							</div>
						</div>
					</div>
<?php
	endforeach;
?>
					<input type="hidden" name="cid" value="<?=$cid?>">
					<input type="hidden" name="thid" value="<?=$thid?>">
					<input type="hidden" name="uid" value="<?=$uid?>">
					<input type="hidden" name="evaluation_use" value="<?=$theme['evaluation_use']?>">
					<input type="hidden" id="draft_flg" name="draft_flg">
					<p class="pull-right">
<?php
	if (!$_is_feedback) {
?>
						<button class="btn btn-primary" id="draft_submit"<?=$_feedback_disabled?>><?=$this->lang->line('feedback_button_draft_save')?></button>
<?php
	}
?>						<button class="btn btn-info" id="feedback_submit"<?=$_feedback_disabled?>><?=$this->lang->line('feedback_button_feedback')?></button>
					</p>
<?php
}
?>
				</form>
			</div>
		</div>
	</div>
	<?=$script?>
