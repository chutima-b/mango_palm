<?php
/*
 * 課題画面ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('task_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('task_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=htmlspecialchars($task->task_name)?></div>
			<div class="panel-body">
				<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
				<?php if (isset($error_msg)) { echo $error_msg; }?>
				<form id="task_form" name="task_form" method="post" action="<?=$this->commonlib->baseUrl()?>task/submission<?=$urlparam?>"<?=$enctype?>>
					<font><?=$task->task_description?></font><br><br>
					<?=$task->answer?>
					<div class="pull-right">
						<button id="btn_noanswer" type="button" class="btn btn-default"><?=$this->lang->line('task_btn_noanswer')?></button>
						<button id="btn_submit" type="button" class="btn btn-primary"><?=$this->lang->line('task_btn_submit')?></button>
					</div>
					<input id="task_is_noanswer" name="task_is_noanswer" type="hidden">
					<input id="task_type" name="task_type" type="hidden" value="<?=$task->task_type?>">
					</form>
			</div>
		</div>
	</div>
<?=$script?>
