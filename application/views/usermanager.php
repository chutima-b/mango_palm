<?php
/*
 * システム管理（ユーザー管理）ビュー
 *
 * @author Takahiro Kimura
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('usermanager_lang');
?>
	<h1 class="sr-only"><?=$this->lang->line('usermanager_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading"><?=$this->lang->line('usermanager_headertitle')?></div>
			<ul class="nav nav-tabs">
			  <li role="presentation" class="active"><a href="javascript:void(0);"><?=$this->lang->line('usermanager_link_usermanager')?></a></li>
			  <li role="presentation"><a href="<?=$this->commonlib->baseUrl()?>genremanager"><?=$this->lang->line('usermanager_link_genremanager')?></a></li>
			  <li role="presentation"><a href="<?=$this->commonlib->baseUrl()?>settings"><?=$this->lang->line('usermanager_link_settings')?></a></li>
			</ul>
			<div class="panel-body">
				<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
<?php
if (isset($error_msg)) {
?>
				<div class="alert alert-danger" role="alert"><?=$error_msg?></div>
<?php
}
?>
				<form class="form-horizontal" id="usermanage_form" name="usermanage_form" method="post" action="<?=$this->commonlib->baseUrl()?>usermanager">
					<div class="form-group">
						<label for="usermanage_userid" class="col-sm-4 control-label"><?=$this->lang->line('usermanager_search_userid')?></label>
						<div class="col-sm-4">
							<input class="form-control" id="usermanage_userid" name="usermanage_userid" type="text" value="<?=set_value('usermanage_userid', $search_userid)?>">
						</div>
					</div>
<?php
// 姓、名の表示を定義に従い、生成する
$_usermanage_surname_value = set_value('usermanage_surname', $search_surname);
$_search_form_surname = <<<EOT
					<div class="form-group">
						<label for="usermanage_surname" class="col-sm-4 control-label">{$this->lang->line('usermanager_search_surname')}</label>
						<div class="col-sm-4">
							<input class="form-control" id="usermanage_surname" name="usermanage_surname" type="text" value="{$_usermanage_surname_value}">
						</div>
					</div>
EOT;

$_usermanage_firstname_value = set_value('usermanage_firstname', $search_firstname);
$_search_form_firstname = <<<EOT
					<div class="form-group">
						<label for="usermanage_firstname" class="col-sm-4 control-label">{$this->lang->line('usermanager_search_firstname')}</label>
						<div class="col-sm-4">
							<input class="form-control" id="usermanage_firstname" name="usermanage_firstname" type="text" value="{$_usermanage_firstname_value}">
						</div>
					</div>
EOT;

$_search_form_item = $this->commonlib->createFullName($_search_form_firstname, $_search_form_surname);
?>
					<?=$_search_form_item?>
					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-8">
							<button id ="usermanage_btn_search" class="btn btn-success" type="submit"><?=$this->lang->line('usermanager_btn_search')?></button>
						</div>
					</div>
					<input type="hidden" id="usermanage_page" name="usermanage_page" value="<?=set_value('usermanage_page', $page)?>">
				</form>
				<hr>
				<form id="usermanage_upload_form" name="usermanage_upload_form" method="post" enctype="multipart/form-data" action="<?=$upload_action_url?>">
					<div class="bs-group">
						<span id="usermanage_btn_guide_help" data-toggle="tooltip" title="<?=$this->lang->line('usermanager_tooltip_guide')?>" data-placement="top" class="guide_icon glyphicon glyphicon-question-sign" aria-hidden="true"></span>
						<label class="btn btn-success" for="usermanage_file_upload">
						<input id="usermanage_file_upload" name="usermanage_file_upload" type="file" accept="text/csv" style="display:none;">
						<?=$this->lang->line('usermanager_btn_upload')?>
						</label>
<?php
$_btn_download_disabled = "";
if (is_null($user_list)) {
	$_btn_download_disabled = " disabled";
}
?>						<button id="usermanager_btn_download" class="btn btn-info"<?=$_btn_download_disabled?>><?=$this->lang->line('usermanager_btn_download')?></button>
						<button type="button" id="usermanager_btn_newuser" class="btn btn-primary" data-toggle="modal" data-target="#user_edit_modal"><?=$this->lang->line('usermanager_btn_newuser')?> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
					</div>
					<span id="usermanage_file_name"></span>
					<p>
						<div id="usermanage_btn_guide" class="alert alert-info help" role="alert" hidden>
							<p><strong><?=$this->lang->line('usermanager_guide_title')?></strong></p>
							<ul>
								<li><?=$this->lang->line('usermanager_guide_item1')?>
								<li><?=$this->lang->line('usermanager_guide_item2')?>
								<li><?=$this->lang->line('usermanager_guide_item3')?>
							</ul>
						</div>
					</p>
				</form>
				<p class="text-right">
					<?=$user_cnt?>&nbsp;<?=$this->lang->line('usermanager_users')?>
<?php
if ($before_page):
?>
					<a id="before_page" href="<?=$before_page?>"><?=$this->lang->line('usermanager_link_before')?></a>
<?php
endif;
if ($next_page):
?>
					<a id="next_page"  href="<?=$next_page?>"><?=$this->lang->line('usermanager_link_next')?></a>
<?php
endif;
?>
				</p>
				<div class="table-responsive touch-scroll">
					<table id="user_list" class="table table-hover">
						<tr>
							<th>&nbsp</th>
							<th><?=$this->lang->line('usermanager_table_userid')?></th>
<?php
// 姓、名の表示を定義に従い、生成する
$_name_item_column = $this->commonlib->createFullName(
		"<th>".$this->lang->line('usermanager_table_firstname')."</th>",
		"<th>".$this->lang->line('usermanager_table_surname')."</th>");
?>
							<?=$_name_item_column?>
							<th><?=$this->lang->line('usermanager_table_nickname')?></th>
							<th><?=$this->lang->line('usermanager_table_course_count')?></th>
							<th><?=$this->lang->line('usermanager_table_login_at')?></th>
							<th>&nbsp</th>
						</tr>
<?php
if (!is_null($user_list)) {
	// ユーザー一覧表示
	foreach($user_list as $user) {
		$_course_cnt = '0';
		$_course_names = '';
		if (isset($user->course_info)) {
			$_course_cnt = count($user->course_info);
			$_is_add_course = false;
			foreach($user->course_info as $_course) {
				// 編集ボタン押下時に表示されるダイアログに
				// 受講中の講座名を表示させるための情報を生成
				if ($_is_add_course) {
					$_course_names .= $this->config->item('USERMANAGE_COURSENAME_DELIMITER');
				}
				$_course_names .= htmlspecialchars($_course->course_name);
				$_is_add_course = true;
			}
		}
?>
						<tr>
							<td><img id="usermanage_list_image_<?=$user->id?>" class="img-responsive usermanage_list_image" src="<?=$user->picture_file?>"></td>
							<td id="usermanage_list_userid_<?=$user->id?>"><?=htmlspecialchars($user->userid)?></td>
<?php
		// 姓、名の表示を定義に従い、生成する
		$_name_column = $this->commonlib->createFullName(
				"<td id=\"usermanage_list_firstname_".$user->id."\">".htmlspecialchars($user->firstname)."</td>",
				"<td id=\"usermanage_list_surname_".$user->id."\">".htmlspecialchars($user->surname)."</td>");
?>
							<?=$_name_column?>
							<td id="usermanage_list_nickname_<?=$user->id?>"><?=htmlspecialchars($user->nickname)?></td>
							<td><?=$_course_cnt?></td>
							<td><?=$user->login_at?></td>
							<td class="usermanage_btn_td">
								<a id="usermanager_btn_edit_<?=$user->id?>" class="btn btn-success"><?=$this->lang->line('usermanager_btn_edit')?></a>
								<a id="<?=$user->id_validity?>" class="btn <?=$user->btn_validity_class?>"><?=$user->btn_validity?></a>
								<a id="usermanager_btn_delete_<?=$user->id?>" class="btn btn-warning"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
								<input id="usermanager_coursenames_<?=$user->id?>" type="hidden" value="<?=$_course_names?>">
							</td>
						</tr>
<?php
	}
}
?>
					</table>
				</div>
			</div>
		</div>
	</div>

	<!-- 新しいユーザー、編集ダイアログ -->
	<div class="modal fade" id="user_edit_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<form class="form-horizontal" id="usermanage_regist_form" name="usermanage_regist_form" method="post" enctype="multipart/form-data" action="<?=$regist_action_url?>">
						<div class="form-group">
							<label for="usermanage_regist_userid" class="col-md-3 control-label"><?=$this->lang->line('usermanager_dialog_userid')?></label>
							<div class="col-md-9">
								<input class="form-control" id="usermanage_regist_userid" name="usermanage_regist_userid" type="text">
							</div>
						</div>
						<div class="form-group">
							<label for="usermanage_regist_password" class="col-md-3 control-label"><?=$this->lang->line('usermanager_dialog_passwd')?></label>
							<div class="col-md-9">
								<input class="form-control" id="usermanage_regist_password" name="usermanage_regist_password" type="password">
							</div>
						</div>
						<div class="form-group">
							<label for="usermanage_regist_password_confirm" class="col-md-3 control-label"><?=$this->lang->line('usermanager_dialog_passwd_confirm')?></label>
							<div class="col-md-9">
								<input class="form-control" id="usermanage_regist_password_confirm" name="usermanage_regist_password_confirm" type="password">
							</div>
						</div>
						<div class="form-group">
							<label for="usermanage_regist_nickname" class="col-md-3 control-label"><?=$this->lang->line('usermanager_dialog_nickname')?></label>
							<div class="col-md-9">
								<input class="form-control" id="usermanage_regist_nickname" name="usermanage_regist_nickname" type="text">
							</div>
						</div>
						<div class="form-group">
							<label for="usermanage_regist_imgupload" class="col-md-3 control-label">　</label>
							<div class="col-md-9">
								<div class="pull-left">
									<img id="usermanage_regist_thumbnail" class="img-responsive" src="">
								</div>
								<button class="btn btn-primary usermanage_regist_btn_imgupload" id="usermanage_regist_btn_imgupload"><span id="usermanage_regist_icon_imgupload" class="glyphicon glyphicon-camera" aria-hidden="true"></span></button>
								<input type="file" id="usermanage_regist_imgupload" name="usermanage_regist_imgupload" style="display:none;" accept="image/*" />
							</div>
						</div>
<?php
// 姓、名の表示を定義に従い、生成する
$_regist_form_surname = <<<EOT
						<div class="form-group">
							<label for="usermanage_regist_surname" class="col-md-3 control-label">{$this->lang->line('usermanager_dialog_surname')}</label>
							<div class="col-md-9">
								<input class="form-control" id="usermanage_regist_surname" name="usermanage_regist_surname" type="text">
							</div>
						</div>
EOT;

$_regist_form_firstname = <<<EOT
						<div class="form-group">
							<label for="usermanage_regist_firstname" class="col-md-3 control-label">{$this->lang->line('usermanager_dialog_firstname')}</label>
							<div class="col-md-9">
								<input class="form-control" id="usermanage_regist_firstname" name="usermanage_regist_firstname" type="text">
							</div>
						</div>
EOT;

$_regist_form_item = $this->commonlib->createFullName($_regist_form_firstname, $_regist_form_surname);
?>
						<?=$_regist_form_item?>
						<div class="form-group" id="usermanage_course_area">
							<label class="col-md-3 control-label"><?=$this->lang->line('usermanager_dialog_course')?></label>
							<div class="col-md-9">
								<div id="usermanage_course"></div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-3 col-md-9">
								<button class="btn btn-default" type="button" data-dismiss="modal"><?=$this->lang->line('usermanager_btn_cancel')?></button>
								<button class="btn btn-primary" type="submit"><?=$this->lang->line('usermanager_btn_regist')?></button>
							</div>
						</div>
						<input type="hidden" id="usermanage_regist_uid" name="usermanage_regist_uid">
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- 確認ダイアログ -->
	<button id="usermanage_confirm_modal_btn_open" type="button" data-toggle="modal" data-target="#usermanage_confirm_modal" style="display:none;">確認ダイアログ</button>
	<div class="modal fade" id="usermanage_confirm_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
					<h4 class="modal-title" id="usermanage_confirm_modal_title"></h4>
				</div>
				<div class="modal-body" id="usermanage_confirm_modal_body"></div>
				<div class="modal-footer">
					<form class="form-horizontal" id="usermanage_confirm_form" name="usermanage_confirm_form" method="post">
						<button type="button" class="btn btn-primary" data-dismiss="modal"><?=$this->lang->line('usermanager_confirm_btn_cancel')?></button>
						<button id="usermanage_confirm_modal_btn_ok" type="submit" class="btn btn-default"><?=$this->lang->line('usermanager_confirm_btn_ok')?></button>
						<input type="hidden" id="usermanage_confirm_modal_uid" name="usermanage_confirm_modal_uid">
					</form>
				</div>
			</div>
		</div>
	</div>
	<?=$script?>
