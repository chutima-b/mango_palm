<?php
/*
 * 日記画面ビュー
 *
 * @author Mitsuharu Shimizu
 * @version 1.0
 * @copyright Copyright (c) 2016, J's Bros. Co., Ltd.
 */


defined('BASEPATH') OR exit('No direct script access allowed');

// 言語ファイル読み込み
$this->lang->load('diary_lang');

$c = $course_data[0];
?>
	<h1 class="sr-only"><?=$this->lang->line('diary_pagetitle')?></h1>
	<div class="container">
		<div class="panel panel-default blur">
			<div class="panel-heading">
				<?=htmlspecialchars($c->course_name)?>
			</div>
			<div class="panel-body">
				<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>')?>
<?php
if (isset($error_msg)) {
?>
				<?=$error_msg?>
<?php
}
// ゲストの場合「日記を書く」「日記一覧」ボタン、<hr>は非表示
if ($auth != $this->config->item('AUTH_GUEST')) {
?>
				<p class="text-right">
					<a class="btn btn-info" href="<?=$this->commonlib->baseUrl()?>diarylist?cid=<?=$c->id?>&pre=<?=$pre?>&thid=<?=$thid?>&dpage=<?=$page?>"><?=$this->lang->line('diary_button_title_diarylist')?></a>
					<button class="btn btn-primary" data-toggle="modal" data-target="#new_post_modal" data-did="" data-postmsg=""><?=$this->lang->line('diary_button_title_diarywrite')?> <span class="glyphicon glyphicon-plus" aria-hidden="true" style="font-size:1.2em;"></span></button>
				</p>
				<hr>
<?php
}

// 無限スクロール用“前ページ”リンク
if ($before_page):
?>
				<a id="before_page" href="<?=$before_page?>"></a>
<?php
endif;

// 無限スクロール用“次ページ”リンク
if ($next_page):
?>
				<a id="next_page" href="<?=$next_page?>"></a>
<?php
endif;
?>
				<div id="diary_list">
<?php
foreach ($diary_data as $idx => $d) :
	// 一つめの日記は<hr>非表示
	if ($idx != 0) {
?>
					<hr>
<?php
	}
?>
					<div class="row diary_post_area">
						<div class="col-xs-3 col-md-1">
							<img class="img-responsive img-circle" src="<?=$d['picture_file']?>">
						</div>
						<div class="col-xs-9 col-md-11">
							<div class="row">
								<div class="col-xs-12">
									<div><?=htmlspecialchars($d['nickname'])?></div>
									<div><?=$d['post_time']?></div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
<?php
	if (!is_null($d['image_file'])) {
?>
									<p><img class="pull-left col-xs-4 diarypic" src="<?=$d['image_file']?>"></p>
<?php
	}
?>
									<p><?=nl2br(htmlspecialchars($d['post']))?></p>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
<?php
	if ($d['uid'] == $uid) {
		// 自分の投稿の場合は「Like!」機能を無効にする
?>
									<span class="glyphicon glyphicon-heart-empty diary_heart_empty" aria-hidden="true"></span>&nbsp;<span class="diary_heart_empty"><?=$d['diary_like_cnt']?></span>
<?php
	} else {
		// 自分以外の投稿は「Like!」機能を有効にする
		$_like_mode = "1";
		$_diary_like_tooltip_title = $this->lang->line('diary_link_like_add');
		$_diary_like_glyphicon_class = "glyphicon glyphicon-heart-empty diary_heart_empty";
		$_diary_like_cnt_class = "diary_heart_empty";
		if ($d['diary_like'] == 1) {
			// 既に「Like!」している場合は、「Like!を取り消す」に差し替える
			$_like_mode = "0";
			$_diary_like_tooltip_title = $this->lang->line('diary_link_like_del');
			$_diary_like_glyphicon_class = "glyphicon glyphicon-heart diary_heart";
			$_diary_like_cnt_class = "diary_heart";
		}
?>
									<a id="diary_like_<?=$d['id']?>" like-mode="<?=$_like_mode?>" cid="<?=$c->id?>" class="diary_like_link" href="javascript:void(0);" data-toggle="tooltip" title="<?=$_diary_like_tooltip_title?>" data-placement="right"><span id="diary_like_glyphicon_<?=$d['id']?>" class="<?=$_diary_like_glyphicon_class?>" aria-hidden="true"></span>&nbsp;<span id="diary_like_cnt_<?=$d['id']?>" class="<?=$_diary_like_cnt_class?>"><?=$d['diary_like_cnt']?></span></a>
<?php
	}
?>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="pull-right">
<?php
	// 自分の投稿または、講師、運用管理者、システム管理者の場合は
	// 編集・削除ボタンを表示する。
	if ($d['my_post'] == 1
	 || $auth == $this->config->item('AUTH_TEACHER')
	 || $auth == $this->config->item('AUTH_OP_MANAGER')
	 || $auth == $this->config->item('AUTH_SYS_MANAGER')) {
?>
										<button class="btn btn-success" data-toggle="modal" data-target="#new_post_modal" data-did="<?=$d['id']?>" data-postmsg="<?=htmlspecialchars($d['post'])?>" data-img="<?=$d['image_file']?>"><?=$this->lang->line('diary_button_title_edit')?></button>
										<button class="btn btn-warning" data-toggle="modal" data-target="#delete_post_modal" data-did="<?=$d['id']?>">
											<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
										</button>
<?php
	}
?>
									</div>
								</div>
							</div>
						</div>
					</div>
<?php
endforeach;
?>
				</div>
			</div>
		</div>
	</div>
	<!-- 編集、投稿ダイアログ -->
	<div class="modal fade" id="new_post_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="post_edit" name="post_edit" method="post" enctype="multipart/form-data" action="<?=$this->commonlib->baseUrl()?>diary/edit">
					<div class="modal-body">
						<div class="form-group">
							<textarea class="form-control" rows="5" name="postmsg" id="postmsg" placeholder="<?=$this->lang->line('diary_textarea_default_message')?>" required></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<div class="pull-left" id="upload_image_btn_area">
							<label class="btn btn-success" for="upload_imagefile">
								<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
								<input type="file" name="upload_imagefile" id="upload_imagefile" accept="image/gif,image/png,image/jpeg" style="display:none;">
							</label>
							<input type="hidden" name="cid" id="cid" value="<?=$c->id?>" />
							<input type="hidden" name="pre" id="pre" value="<?=$pre?>" />
							<input type="hidden" name="thid" id="thid" value="<?=$thid?>" />
							<input type="hidden" name="page" id="page" value="<?=$page?>" />
							<input type="hidden" name="did" id="did" value="" />
						</div>
						<img id="upload_imagefile_thumbnail" class="img-responsive" src="">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('diary_button_title_cancel')?></button>
						<button type="submit" class="btn btn-primary"><?=$this->lang->line('diary_button_title_send')?></button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- 削除確認ダイアログ -->
	<div class="modal fade" id="delete_post_modal" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<form class="form-inline" name="diary_delete" method="post" action="<?=$this->commonlib->baseUrl()?>diary/delete">
					<div class="modal-body">
						<div class="form-group">
							<label for="confim_title"><?=$this->lang->line('diary_confim_title')?></label><br>
							<label for="delete_confim"><?=$this->lang->line('diary_delete_confim')?></label>
							<input type="hidden" name="cid" id="cid" value="<?=$c->id?>" />
							<input type="hidden" name="pre" id="pre" value="<?=$pre?>" />
							<input type="hidden" name="thid" id="thid" value="<?=$thid?>" />
							<input type="hidden" name="page" id="page" value="<?=$page?>" />
							<input type="hidden" id="did" name="did" value=""/>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal"><?=$this->lang->line('diary_button_title_cancel')?></button>
						<button type="submit" class="btn btn-primary"><?=$this->lang->line('diary_button_title_delete')?></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?=$script?>